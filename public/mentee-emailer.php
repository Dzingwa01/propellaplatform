<?php
ini_set( 'display_errors', 1 );
error_reporting( E_ALL );
$from = "dev@propellaincubator.co.za";
$to = $_POST['email'];
$full_name = $_POST['name'];
/*$reason = $_POST['declined_reason_two'];*/

$subject = 'Your Shadow board Times';

// To send HTML mail, the Content-type header must be set
/*$headers = 'Cc: info@drivethrusa.co.za'. "\r\n";*/
$headers  .= 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Create email headers
$headers .= 'From: '.$from."\r\n".
    'Reply-To: '.$from."\r\n" .
    'X-Mailer: PHP/' . phpversion();

// Compose a simple HTML email message
$message = '<html><body>';
$message .= '<br/>';
$message .= '<p style="font-size:16px;color:black!important">Hi '.$full_name.'<br/>  <p>Your Shadow Board is due to occur in the next week.</p>
    <br>
    <p>Please ensure you have competed your<span><b>3 Monthly Evaluation Form on your Mentor prior to your Shadow Board</b></span> by logging in to : www.thepropella.co.za .</p>
    <br>
    <p>If you have not completed it then please ensure you arrive 10 minutes early for your Shadow Board and complete on our Propella tablet before your Shadow Board starts.</p>
    <br>
    <p>This is important to ensure the success of the Mentoring Relationship and to enable the Mentor to keep you moving forward.</p>
    <br>
    <p>You are required to provide feedback on your Action Items from previous Shadow Board.</p>
    <br>
    <p>The agenda for the Shadow Board is below in case you require it:
    </p>
    <p><b> SHADOW BOARD MEETING</b></p>
    <p>1)	Present: Incubatee / Mentor / Advisor / Incubation Manager / AN Other</p>
    <p>2)	Meets quarterly at Propella</p>
    <p>3)	Incubatee reviews roadmap and discusses goals set for next quarter </p>
    <br>
    <p><b>Agenda</b></p>
    <p> 1)	Strategy & Stakeholders</p>
    <p> 2)	Marketing</p>
    <p> 3)	Sales (achieved and pipeline)</p>
    <p> 4)	Finance (performance against budget)</p>
    <p> 5)	Legal & Compliance (including HR)</p>
    <p> 6)	Operations (Process / Admin)</p>
    <p> 7)	Product / Technology Development</p>
    <p> 8)	Entrepreneur / Personal Development</p>
    <p> 9)	General</p>
    <p> 10)	Dates for the next 3 months</p>
    <p> -	Incubatee / Mentor</p>
    <p> -	Propella Advisor</p>
    <p> -	Next Shadow Board</p>
    <br>
    <br/>
<br/>
Kind regards <br/>
Propella</p>';

$message .= '</body></html>';

if(mail($to,$subject,$message, $headers)) {
    echo "The email message was sent.";
} else {
    echo "The email message was not sent.";
}
?>
