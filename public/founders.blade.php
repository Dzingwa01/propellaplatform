@extends('resources.views.layouts.app')
@section('content')

    <link rel="stylesheet" type="text/css" href="/css/Founder/FounderMain.css"/>
    <!---Link For Used Icons-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!---Links for Social Media-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <br>
    <!--Comment-->
    <!--NEW PROFILE DESKTOP-->
    <div class="row " id="VentureDesktop">
        <div class="" style="margin-left: 70px;">
            <br>
            <h4 style="margin-left: 2em;color: black;text-transform: capitalize"><b>{{$venture->company_name}}</b></h4>
            <div class="row" style="margin-left: 70px;">
                <div class="">
                    @if($venture->smart_city_tags!= null)
                        <a style=" text-transform: lowercase;" class="btn disabled">{{$venture->smart_city_tags}}</a>
                    @endif
                    @if($venture->hub!= null)
                        <a style="text-transform: lowercase;" class="btn disabled">{{$venture->hub}}</a>
                    @endif
                    @if($venture->stage!= null)
                        <a style="text-transform: lowercase;" class="btn disabled">{{$venture->stage}}</a>
                        @elseif(is_null($venture->stage))
                            <a style="text-transform: lowercase;" class="btn disabled" hidden>{{$venture->stage}}</a>
                    @endif
                </div>
            </div>

            <div class="col m6" style="margin-left: 4em">
                <div class="z-depth-3">
                    {{--<img style="width: 100%;height: 100%;" src="/images/Propella_Logo.jpg"/>--}}
                    <img
                        src="/storage/{{isset($venture_upload->company_logo_url)?$venture_upload->company_logo_url:''}}"
                        style="width: 100%;height: 100%;">

                </div>
                <div class="card" style="border-radius: 2%;">
                    <br>
                    <h4 style="margin-left: 1em;" class="standard-header">COMPANY PROFILE</h4>
                    @if($venture->elevator_pitch != null)
                        <p style="margin-left: 2em;margin-right: 2em;">{{$venture->elevator_pitch}}</p>
                    @endif
                    <br>
                    @if($venture->venture_profile_information != null)
                        <p style="margin-left: 2em;margin-right: 2em;">{{$venture->venture_profile_information}}</p>
                    @endif

                    <div class="row" style="margin-left: 1em;margin-right: 1em;">
                        <br/>
                        <br/>
                        @if(isset($venture_upload->infographic_url))
                            <div class="col s4">
                                <a class="waves-effect blue waves-light btn modal-trigger infographicBtn" href="#modal1"
                                   style="width:100%; ">Infographic</a>
                                <!-- Modal Structure -->
                                <div id="modal1" class="modal">
                                    <div class="modal-content">
                                        <div class="row">
                                            <div class="col m12">
                                                <img style="width:100%; height:100%;"
                                                     src="/storage/{{isset($venture_upload->infographic_url)?$venture_upload->infographic_url:''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="">
                            {{--Need to add incubatee testimonials here--}}
                            {{--<div class="col s6">--}}
                            {{--<a class="waves-effect blue waves-light btn modal-trigger"href="#modal3"href="#modal3" style="width:70%;">Testimonials</a>--}}
                            {{--<!-- Modal Structure -->--}}
                            {{--<div id="modal3" class="modal">--}}
                            {{--<div class="modal-content">--}}
                            {{--<h4>List of testimonials to go here.</h4>--}}
                            {{--</div>--}}
                            {{--<div class="modal-footer">--}}
                            {{--<a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            @if(isset($venture->ventureOwnership->ownership) or isset($venture->ventureOwnership->registration_number) or isset($venture->ventureOwnership->BBBEE_level))
                                <div class="col s4">
                                    <a class="waves-effect blue waves-light btn modal-trigger" href="#modal4" href="#"
                                       style="width:100%;">Ownership</a>
                                    <div id="modal4" class="modal">
                                        <div class="modal-content">
                                            <h4>{{$venture->company_name}}'s Ownership</h4>
                                            <p>Company
                                                Ownership: {{isset($current_venture_ownership->ownership)?$current_venture_ownership->ownership:''}}</p>
                                            <p>Company Registration
                                                Number: {{isset($current_venture_ownership->registration_number)?$current_venture_ownership->registration_number:''}}</p>
                                            <p>Company BBBEE
                                                Level: {{isset($current_venture_ownership->BBBEE_level)?$current_venture_ownership->BBBEE_level:''}}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="">
                            @if(isset($venture_upload->crowdfunding_url))
                                <div class="col s4">
                                    <a class="waves-effect blue waves-light btn modal-trigger" href="#modal5" href="#"
                                       style="width:100%;">Crowd Funding</a>
                                    <div id="modal5" class="modal">
                                        <div class="modal-content">
                                            <img
                                                src="/storage/{{isset($venture_upload->crowdfunding_url)?$venture_upload->crowdfunding_url:''}}">
                                        </div>
                                        <div class="modal-footer">
                                            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            {{--Need to add incubatee gallery here--}}
                            {{--<div class="col s6">--}}
                            {{--<a class="waves-effect blue waves-light btn modal-trigger"href="#modalM6"href="#" style="width:70%;">Gallery</a>--}}
                            {{--<div id="modalM6" class="modal">--}}
                            {{--<div class="modal-content">--}}
                            {{--<h4>Incubatee gallery to go here</h4>--}}
                            {{--</div>--}}
                            {{--<div class="modal-footer">--}}
                            {{--<a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="col m4">
                            <a class="waves-effect green waves-light btn modal-trigger" href="#modal7" href="#"
                               style="width:100%;">Invest in us</a>
                            <div id="modal7" class="modal">
                                <div class="modal-content">
                                    <div class="row" style="width: 70%;margin-left: 100px;">
                                        <form id="ContactUsForm">
                                            <div class="input-field">
                                                <input id="name" type="tel"/>
                                                <label for="name">First name</label>
                                            </div>


                                            <div class="input-field">
                                                <select>
                                                    <option value="Subject" disabled selected>Subject</option>
                                                    <option value="1">General Discussion</option>
                                                    <option value="2">Invest</option>
                                                    <option value="3">Entrepreneurs</option>
                                                    <option value="4">Apply</option>
                                                </select>
                                            </div>
                                            <div class="input-field">
                                                <input id="email" type="email"/>
                                                <label for="email">Email</label>
                                            </div>

                                            <div class="input-field">
                                                <input id="Message" type="tel"/>
                                                <label for="Message">Message</label>
                                            </div>

                                            <div class="input-field">
                                                <input id="phone" type="tel"/>
                                                <label for="phone">Phone Number</label>
                                            </div>

                                            <div>
                                                <button class="btn waves-effect waves-light float-right" type="submit"
                                                        name="action">Send
                                                    <i class="material-icons right">send</i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col s7">
                            <div class="row" style="padding-left: 25px;">
                                @if($venture->business_facebook_url != null)
                                    <div class="col s2">
                                        <a href="{{$venture->business_facebook_url}}"><i
                                                class="small fab fa-facebook"></i></a>
                                    </div>
                                @endif

                                @if($venture->business_twitter_url != null)
                                    <div class="col s2">
                                        <a href="{{$venture->business_twitter_url}}"><i
                                                class="small fab fa-twitter"></i></a>
                                    </div>
                                @endif

                                @if($venture->business_linkedIn_url != null)
                                    <div class="col s2">
                                        <a href="{{$venture->business_linkedIn_url}}"><i
                                                class="small fab fa-linkedin"></i></a>
                                    </div>
                                @endif
                                @if($venture->business_instagram_url != null)
                                    <div class="col s2">
                                        <a href="{{$venture->business_instagram_url}}"><i
                                                class="small fab fa-instagram"></i></a>
                                    </div>
                                @endif
                                @if($venture->company_website != null)
                                    <div class="col s2">
                                        <a href="{{$venture->company_website}}"><i class="small material-icons dp48">language</i></a></a>
                                    </div>
                                @endif
                                @if($venture->contact_number != null)
                                    <div class="col s2">
                                        <a href="{{$venture->contact_number}}" href="#modal20"><a
                                                class="waves-effect waves-light  modal-trigger" href="#modal20"><i
                                                    class="small material-icons dp48">local_phone</i></a></a>
                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                    <br>
                </div>

                <!-- Modal Trigger -->

                <!-- Modal Structure -->
                <div id="modal20" class="modal" style="width:20%;margin-top: 50vh;">
                    <div class="modal-content">
                        <p>Phone number:{{$venture->contact_number}}</p>
                    </div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Ok</a>
                    </div>
                </div>
            </div>

            <div class="col s2"></div>
            <div class="row col s4" style="margin-right: 4em;">
                <div class="card" style="border-radius: 2%;margin:0;">
                    <div class="" style="padding-left: 2em;padding-right: 2em;">
                        <br>
                        <h4 class="standard-header">TEAM PROFILE</h4>
                        @foreach($venture->incubatee as $incubatee)
                            <div class="row">
                                <div class="col s5">
                                    <img style="width:100%; height:30%;border-radius: 50%"
                                         src="/storage/{{isset($incubatee->uploads->profile_picture_url)?$incubatee->uploads->profile_picture_url:'images/ProfilePictures/NoProfilePic.jpg'}}"/>
                                    {{--<img style="width:100%; height:30%;border-radius: 50%" src="/images/ProfilePictures/Propella Entrepreneurs/_H2A1052.JPG"/>--}}
                                </div>
                                <div class="col s7">
                                    <div class="row">
                                        <h6 style="margin-left: 1em;">{{$incubatee->user->name}} {{$incubatee->user->surname}}</h6>
                                        <br>
                                        <h6 style="margin-left: 1em;"> {{$incubatee->user->email}}</h6>
                                        <br>
                                        @if($incubatee->personal_facebook_url != null)
                                            <div class="col s3">
                                                <a href="{{$incubatee->personal_facebook_url}}"><i
                                                        class="small fab fa-facebook"></i></a></a>
                                            </div>
                                        @endif

                                        @if($incubatee->personal_twitter_url != null)
                                            <div class="col s3">
                                                <a href="{{$incubatee->personal_twitter_url}}"><i
                                                        class="small fab fa-twitter"></i></a></a>
                                            </div>
                                        @endif

                                        @if($incubatee->personal_linkedIn_url != null)
                                            <div class="col s3">
                                                <a href="{{$incubatee->personal_linkedIn_url}}"><i
                                                        class="small fab fa-linkedin"></i></a></a>
                                            </div>
                                        @endif

                                        @if($incubatee->personal_instagram_url != null)
                                            <div class="col s3">
                                                <a href="{{$incubatee->personal_instagram_url}}"><i
                                                        class="small fab fa-instagram"></i></a>
                                            </div>
                                        @endif
                                    </div>

                                </div>
                            </div>


                            <div class=" ">
                                <h4 class="" style="font-size: 2rem;font-family: Arial;">About Me</h4>
                                <div class="item">
                                    <p>
                                        {{$incubatee->short_bio}}
                                    </p>
                                </div>
                                <hr>
                            </div>
                        @endforeach
                    </div>
                    <br/>
                </div>
            </div>
        </div>
    </div>

    <!--NEW PROFILE Mobile-->
    <div class="row left-align" id="VentureMobile">
        <br>
        <br>
        <div class="row col s12">
            <h6 style="margin-left: 1em;color: black;text-transform: capitalize"><b>{{$venture->company_name}}</b></h6>
            <div class="row" style="margin-left: 1em;">
                <div class="">
                    @if($venture->smart_city_tags!= null)
                        <button>{{$venture->smart_city_tags}}</button>
                    @endif
                    @if($venture->hub!= null)
                        <button>{{$venture->hub}}</button>
                    @endif
                    @if($venture->stage!= null)
                        <button>{{$venture->stage}}</button>
                    @endif
                </div>
            </div>
            <div class="z-depth-3">
                <img style="width: 100%;"
                     src="/storage/{{isset($venture_upload->company_logo_url)?$venture_upload->company_logo_url:''}}"
                     style="height: 50%; width: 100%;">
            </div>
            <br>
            <div class="card" style="width: 340px;margin:0 auto;border-radius: 2%;">
                <div class="" style="padding-left: 2em;padding-right: 2em;">
                    <br>
                    <h4 class="standard-header">TEAM PROFILE</h4>
                    @foreach($venture->incubatee as $incubatee)
                        <div class="row">
                            <div class="col s5">
                                <img style="width:100%; height:30%;border-radius: 50%"
                                     src="/storage/{{isset($incubatee->uploads->profile_picture_url)?$incubatee->uploads->profile_picture_url:'images/ProfilePictures/NoProfilePic.jpg'}}"/>
                                {{--<img style="width:100%; height:30%;border-radius: 50%" src="/images/ProfilePictures/Propella Entrepreneurs/_H2A1052.JPG"/>--}}
                            </div>
                            <div class="col s7">
                                <div class="row">
                                    <h6 style="margin-left: 1em;">{{$incubatee->user->name}} {{$incubatee->user->surname}}</h6>
                                    <br>
                                    {{--<h6 style="margin-left: 1em;"> {{$incubatee->user->email}}</h6>--}}
                                    <br>
                                    @if($incubatee->personal_facebook_url != null)
                                        <div class="col s3">
                                            <a href="{{$incubatee->personal_facebook_url}}"><i
                                                    class="small fab fa-facebook"></i></a>
                                        </div>
                                    @endif

                                    @if($incubatee->personal_twitter_url != null)
                                        <div class="col s3">
                                            <a href="{{$incubatee->personal_twitter_url}}"><i
                                                    class="small fab fa-twitter"></i></a>
                                        </div>
                                    @endif

                                    @if($incubatee->personal_linkedIn_url != null)
                                        <div class="col s3">
                                            <a href="{{$incubatee->personal_linkedIn_url}}"><i
                                                    class="small fab fa-linkedin"></i></a>
                                        </div>
                                    @endif
                                    @if($incubatee->personal_linkedIn_url != null)
                                        <div class="col s3">
                                            <a href="mailto:{{$incubatee->user->email}}"><i
                                                    class="small material-icons dp48">email</i></a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class=" ">
                            <h4 class="" style="font-size: 2rem;font-family: Arial;">About Me</h4>
                            <p>
                                {{$incubatee->short_bio}}
                            </p>
                        </div>
                        <hr>
                    @endforeach
                </div>
                <br/>
            </div>
        </div>
        <div class="col s1"></div>

        <div class="col m6">
            {{--<img style="width: 100%;height: 500%" src="/images/Propella_Logo.jpg"/>--}}
            <br>
            <div class="card" style="width: 340px;margin: 0 auto;border-radius: 2%;">
                <br>
                <h4 class="standard-header" style="margin-left: 1em;margin-right: 1em;">COMPANY PROFILE</h4>
                @if($venture->elevator_pitch != null)
                    <p style="margin-left: 1em;margin-right: 1em;">{{$venture->elevator_pitch}}</p>
                @endif
                <br>
                @if($venture->venture_profile_information != null)
                    <p style="margin-left: 1em;margin-right: 1em;">{{$venture->venture_profile_information}}</p>
                @endif

                <br>
            </div>

            <div class="row">
                <br/>
                <br/>
                @if(isset($venture_upload->infographic_url))
                    <div class="col s12">
                        <a class="waves-effect blue waves-light btn modal-trigger infographicBtn" href="#modal10"
                           style="width:100%; ">Infographic</a>
                        <!-- Modal Structure -->
                        <div id="modal10" class="modal">
                            <div class="modal-content">
                                <div class="row">
                                    <div class="col m4">
                                        <img style="width:120%; height:100%;"
                                             src="/storage/{{isset($venture_upload->infographic_url)?$venture_upload->infographic_url:''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                            </div>
                        </div>
                    </div>
                @endif
                <br>
                <br/>
                <div class="">
                    {{--Need to add incubatee testimonials here--}}
                    {{--<div class="col s6">--}}
                    {{--<a class="waves-effect blue waves-light btn modal-trigger"href="#modal3"href="#modal3" style="width:70%;">Testimonials</a>--}}
                    {{--<!-- Modal Structure -->--}}
                    {{--<div id="modal3" class="modal">--}}
                    {{--<div class="modal-content">--}}
                    {{--<h4>List of testimonials to go here.</h4>--}}
                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                    {{--<a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    @if(isset($current_venture_ownership->ownership) or isset($current_venture_ownership->registration_number) or isset($current_venture_ownership->BBBEE_level))
                        <div class="col s4">
                            <a class="waves-effect blue waves-light btn modal-trigger" href="#modal11" href="#"
                               style="width:342%;">Ownership</a>
                            <div id="modal11" class="modal">
                                <div class="modal-content">
                                    <h6><b>{{$venture->company_name}}'s Ownership</b></h6>
                                    <p>Company
                                        Ownership: {{isset($current_venture_ownership->ownership)?$current_venture_ownership->ownership:''}}</p>
                                    <p>Company Registration
                                        Number: {{isset($current_venture_ownership->registration_number)?$current_venture_ownership->registration_number:''}}</p>
                                    <p>Company BBBEE
                                        Level: {{isset($current_venture_ownership->BBBEE_level)?$current_venture_ownership->BBBEE_level:''}}</p>
                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <br>
                <br/>
                <div class="">
                    @if(isset($venture->ventureUploads->crowdfunding_url))
                        <div class="col s12">
                            <a class="waves-effect blue waves-light btn modal-trigger" href="#modal12" href="#"
                               style="width:100%;">Crowd Funding</a>
                            <div id="modal12" class="modal">
                                <div class="modal-content">
                                    <img
                                        src="/storage/{{isset($venture_upload->crowdfunding_url)?$venture_upload->crowdfunding_url:''}}">
                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                </div>
                            </div>
                        </div>
                    @endif

                    {{--Need to add incubatee gallery here--}}
                    {{--<div class="col s6">--}}
                    {{--<a class="waves-effect blue waves-light btn modal-trigger"href="#modalM6"href="#" style="width:70%;">Gallery</a>--}}
                    {{--<div id="modalM6" class="modal">--}}
                    {{--<div class="modal-content">--}}
                    {{--<h4>Incubatee gallery to go here</h4>--}}
                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                    {{--<a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="col s12">
                    <a class="waves-effect green waves-light btn modal-trigger" href="#modal13" href="#"
                       style="width:100%;">Invest in us</a>
                    <div id="modal13" class="modal">
                        <div class="modal-content">
                            <div class="row" style="width: 70%;margin-left: 2em;">
                                <form id="ContactUsForm">
                                    <div class="input-field">
                                        <input id="name" type="tel"/>
                                        <label for="name">First name</label>
                                    </div>


                                    <div class="input-field">
                                        <select>
                                            <option value="Subject" disabled selected>Subject</option>
                                            <option value="1">General Discussion</option>
                                            <option value="2">Invest</option>
                                            <option value="3">Entrepreneurs</option>
                                            <option value="4">Apply</option>
                                        </select>
                                    </div>
                                    <div class="input-field">
                                        <input id="email" type="email"/>
                                        <label for="email">Email</label>
                                    </div>

                                    <div class="input-field">
                                        <input id="Message" type="tel"/>
                                        <label for="Message">Message</label>
                                    </div>

                                    <div class="input-field">
                                        <input id="phone" type="tel"/>
                                        <label for="phone">Phone Number</label>
                                    </div>

                                    <div>
                                        <button class="btn waves-effect waves-light float-right" type="submit"
                                                name="action">Send
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <br>
        <br>
        <br>


        <div class="row">
            <div class="col s7">
                <div class="row" style="padding-left: 25px;">
                    @if($venture->business_facebook_url != null)
                        <div class="col s2">
                            <a href="{{$venture->business_facebook_url}}"><i class="small fab fa-facebook"></i></a>
                        </div>
                    @endif

                    @if($venture->business_twitter_url != null)
                        <div class="col s2">
                            <a href="{{$venture->business_twitter_url}}"><i class="small fab fa-twitter"></i></a>
                        </div>
                    @endif

                    @if($venture->business_linkedIn_url != null)
                        <div class="col s2">
                            <a href="{{$venture->business_linkedIn_url}}"><i class="small fab fa-linkedin"></i></a>
                        </div>
                    @endif
                    @if($venture->business_instagram_url != null)
                        <div class="col s2">
                            <a href="{{$venture->business_instagram_url}}"><i class="small fab fa-instagram"></i></a>
                        </div>
                    @endif
                    @if($venture->company_website != null)
                        <div class="col s2">
                            <a href="{{$venture->company_website}}"><i
                                    class="small material-icons dp48">language</i></a></a>
                        </div>
                    @endif
                    @if($venture->contact_number != null)
                        <div class="col s2">
                            <a href="{{$venture->contact_number}}" href="#modal20"><a
                                    class="waves-effect waves-light  modal-trigger" href="#modal20"><i
                                        class="small material-icons dp48">local_phone</i></a></a>
                        </div>
                    @endif
                </div>

            </div>
        </div>


    </div>
    </div>


    <script>
        $(document).ready(function () {
            $('.modal').modal();

            // $('.item').each(function(event){
            //
            //     var max_length = 300;
            //
            //     if($(this).html().length > max_length){
            //
            //         var short_content 	= $(this).html().substr(0,max_length);
            //         var long_content	= $(this).html().substr(max_length);
            //
            //         $(this).html(short_content+
            //             '<a href="#" class="read_more"><br/>Read More</a>'+
            //             '<span class="more_text" style="display:none;">'+long_content+'</span>');
            //
            //         $(this).find('a.read_more').click(function(event){
            //
            //             event.preventDefault();
            //             $(this).hide();
            //             $(this).parents('.item').find('.more_text').show();
            //
            //         });
            //
            //     }
            //
            // });
        });
    </script>

@endsection
