@extends('layouts.visitors-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('admin-add-visitors')}}
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="./js/numeric-1.2.6.min.js"></script>
        <script src="./js/bezier.js"></script>
        <script src="./js/jquery.signaturepad.js"></script>
        <script type="text/javascript"
                src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
        <script src="./js/json2.min.js"></script>
    </head>
    <form id="create-visitor-form">
        <div class="card row" style="width: 800px;margin: 0 auto;">
            <br/>
            <a><img style="width: 200px;margin-left: 600px" src="/images/Propella_Logo.jpg" class=""/></a>
            <h4 style="margin-left: 250px;font-size: 2em" id="page-header">Welcome To Propella</h4>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col m6">
                    <select id="title" required>
                        <option value="" disabled selected>Choose Title</option>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Miss</option>
                        <option value="Ms">Ms</option>
                        <option value="Dr">Dr</option>
                    </select>
                    <label>Title</label>
                </div>
                <div class="input-field col s6">
                    <input id="first_name" name="first_name" type="text" class="validate" required>
                    <label for="first_name">First Name</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="last_name" name="last_name" type="text" class="validate" required>
                    <label for="last_name">Last Name</label>
                </div>
                <div class="input-field col s6">
                    <input id="company" name="company" type="text" class="validate" required>
                    <label for="company">Your Company Name</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="designation" name="designation" type="text" class="validate" required>
                    <label for="designation">Designation</label>
                </div>
                <div class="input-field col s6">
                    <input id="email" name="email" type="email" class="validate" required>
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em; margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="cell_number" name="cell_number" type="text" class="validate" required>
                    <label for="cell_number">Cell number</label>
                </div>
                <div class="input-field col s6">
                    <input id="dob" type="date" name="date" class="validate" required>
                    <label for="dob">Date of Birth</label>
                </div>
            </div>

            <div class="row" style="margin-left: 550px;">
                <input type="submit" id="submit-button" class="waves-effect waves-light btn">
            </div>
            <br/>
        </div>
    </form>

    <br/>
    <br/>

    <script>
        $(document).ready(function () {
            $('#create-visitor-form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();

                formData.append('title', $('#title').val());
                formData.append('first_name', $('#first_name').val());
                formData.append('last_name', $('#last_name').val());
                formData.append('company', $('#company').val());
                formData.append('designation', $('#designation').val());
                formData.append('email', $('#email').val());
                formData.append('cell_number', $('#cell_number').val());
                formData.append('land_line', $('#land_line').val());
                formData.append('dob', $('#dob').val());


                let url = "{{route('visitor.store')}}";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#page-header').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/visitors/visitor-login'}, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        $('#page-header').notify(response.message, "error");
                    }
                });
            });
        });

    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection

