@extends('layouts.app')

@section('content')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <br>
    <br>

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white" id="page-header">Visitor Details</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="center">
                <div class="col l12 card">
                    <div class="row" style="margin-left: 2em;margin-right: 2em;">
                        <input hidden disabled id="visitor-id-input" value="{{$visitor->id}}">
                        <div class="input-field col m6">
                            <select id="title">
                                <option value="" disabled selected>Choose Title</option>
                                <option value="Mr" {{$visitor->title=='Mr'?'selected':''}}>Mr</option>
                                <option value="Mrs"{{$visitor->title=='Mrs'?'selected':''}}>Mrs</option>
                                <option value="Miss"{{$visitor->title=='Miss'?'selected':''}}>Miss</option>
                                <option value="Ms"{{$visitor->title=='Ms'?'selected':''}}>Ms</option>
                                <option value="Dr"{{$visitor->title=='Dr'?'selected':''}}>Dr</option>
                            </select>
                            <label>Title</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="first_name" type="text" value="{{$visitor->first_name}}" class="validate">
                            <label for="first_name">First Name</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 2em;margin-right: 2em;">
                        <div class="input-field col s6">
                            <input id="last_name" type="text" value="{{$visitor->last_name}}" class="validate">
                            <label for="last_name">Last Name</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="company" type="text" value="{{$visitor->company}}" class="validate">
                            <label for="company">Your Company Name</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 2em;margin-right: 2em;">
                        <div class="input-field col s6">
                            <input id="designation" type="text" value="{{$visitor->designation}}" class="validate">
                            <label for="designation">Designation</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="email" type="text" value="{{$visitor->email}}" class="validate">
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 2em; margin-right: 2em;">
                        <div class="input-field col s6">
                            <input id="cell_number" value="{{$visitor->cell_number}}" type="text" class="validate">
                            <label for="cell_number">Cell number</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="dob" type="date" value="{{$visitor->dob}}" class="validate">
                            <label for="dob">Date of Birth</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 2em; margin-right: 2em;">
                        <button class="btn right" id="update-basic-info-button">Update Details</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12">
                    <div class="row">
                        <div class="col l6 card hoverable" id="visitor-event-card" style="background: #1a237e">
                            <br>
                            <h5 style="color: white">Events</h5>
                            <br>
                        </div>
                        <div class="col l6 card hoverable" id="visitor-summary-card" style="background: #1B5E20">
                            <br>
                            <h5 style="color: white">Visitor Summary</h5>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $('#update-basic-info-button').on('click', function(){
            let formData = new FormData();

            formData.append('title', $('#title').val());
            formData.append('first_name', $('#first_name').val());
            formData.append('last_name', $('#last_name').val());
            formData.append('company', $('#company').val());
            formData.append('designation', $('#designation').val());
            formData.append('email', $('#email').val());
            formData.append('cell_number', $('#cell_number').val());
            formData.append('dob', $('#dob').val());
            formData.append('visitor_id', $('#visitor-id-input').val());


            let url = "{{route('visitor.updateBasicDetails')}}";
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#page-header').notify(response.message, "success");
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    $('#page-header').notify(response.message, "error");
                }
            });

        });

        $('#visitor-summary-card').on('click', function(){
           location.href = '/visitors/visitor-summary/' + $('#visitor-id-input').val();
        });

        $('#visitor-event-card').on('click', function(){
            location.href = '/visitors/visitor-events/' + $('#visitor-id-input').val();
        });

        $(document).ready(function(){
            let stored_event_id = sessionStorage.getItem('stored_event_id');
            let stored_found_out = sessionStorage.getItem('stored_found_out_via');
            let visitor_id = $("#visitor-id-input").val();

            if(stored_event_id !== null && stored_found_out !== null){
                let formData = new FormData();
                formData.append('event_id', stored_event_id);
                formData.append('visitor_id', visitor_id);
                formData.append('found_out_via', stored_found_out);

                let url = "{{route('visitor.storeEventViaVisitorEventPage')}}";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $.notify(response.message, "info");
                        sessionStorage.setItem('stored_event_id', "");
                        sessionStorage.setItem('stored_found_out_via', "");
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        $.notify(response.message, "error");
                    }
                });
            }
        });

    </script>

@endsection
