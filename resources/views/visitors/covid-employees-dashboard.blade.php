@extends('layouts.app')

@section('content')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <br>
    <br>

    <div class="container">
        <h5><b>Hi</b> {{$companyEmployee->name}} {{$companyEmployee->surname}}</h5>
        <div class="row">
            <div class="">
                <input hidden disabled id="company-employee-id-input" value="{{$companyEmployee->id}}">
                <div class="">
                    <div class=" left-align row">
                        <h5><b>Are you at risk of Covid-19 ?</b></h5>
                        <p>Please answer following questions</p>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="employee_temperature" type="text"  class="validate">
                            <label for="employee_temperature"><span style="color: red">Please enter your Temperature</span></label>
                        </div>
                    </div>
                    <div class="row">
                        @if($companyEmployee->is_vaccinated == 'Yes')
                        @else
                            <div class="row">
                                <h6>Have you been Vaccinated:</h6>
                            </div>
                            <div class="row">
                                <p>
                                    <label>
                                        <input id="yes" name="is_vaccinated" value="Yes" required type="radio"/>
                                        <span>Yes</span>
                                    </label>
                                </p>
                                <p>
                                    <label>
                                        <input id="no" name="is_vaccinated" value="No" type="radio"/>
                                        <span>No</span>
                                    </label>
                                </p>

                            </div>

                        @endif
                    </div>

                    <div class="row">
                        <h6>Have you come into close contact (within 6 feet) with someone who has a
                            laboratory confirmed COVID–19 diagnosis in the past 10 days?</h6>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="row">
                                <p>
                                    <label>
                                        <input id="yes" name="employee_covid_symptoms_one" required value="Yes" type="radio"/>
                                        <span>Yes</span>
                                    </label>
                                </p>
                                <p>
                                    <label>
                                        <input id="no" name="employee_covid_symptoms_one" value="No" type="radio"/>
                                        <span>No</span>
                                    </label>
                                </p>

                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <h6>Do you have any of the following: fever or chills, cough, shortness of breath
                            or difficulty breathing, body aches, headache, new loss of taste or smell, sore throat?
                        </h6>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="row">
                                <p>
                                    <label>
                                        <input id="yes" name="employee_covid_symptoms_two" value="Yes" required type="radio"/>
                                        <span>Yes</span>
                                    </label>
                                </p>
                                <p>
                                    <label>
                                        <input id="no" name="employee_covid_symptoms_two" value="No" type="radio"/>
                                        <span>No</span>
                                    </label>
                                </p>
                            </div>

                        </div>
                    </div>



                    <div class="row">
                        <div class="row">
                            <h6>* Digitally Signed Declaration</h6>
                            <p>By checking this box, you are digitally and legally agreeing to the
                                declarations as stated in this screening form.</p>
                            <p>
                                <label>
                                    <input id="no" name="employee_digitally_signed_declaration" value="Agree" required type="radio"/>
                                    <span>Agree</span>
                                </label>
                            </p>
                        </div>
                    </div>


                    <div class="row" style="margin-left: 2em; margin-right: 2em;">
                        <button class="btn right" id="submit-employee-covid-button">SUBMIT</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

       /* $(".input-select").on("change", checkSelect);
        $("#vaccine-date").hide();

        function checkSelect() {
            if ($('#is_vaccinated').val() == 'Yes') {
                $("#vaccine-date").show();
            }
            if ($('#is_vaccinated').val() == 'No') {
                $("#vaccine-date").hide();
            }
        }*/

        $('#submit-employee-covid-button').on('click', function () {
            let formData = new FormData();

            formData.append('name', $('#name').val());
            formData.append('surname', $('#surname').val());
            formData.append('email', $('#email').val());
            formData.append('employee_temperature', $('#employee_temperature').val());
            formData.append('employee_covid_symptoms_one', $('input[name=employee_covid_symptoms_one]:checked').val());
            formData.append('employee_covid_symptoms_two', $('input[name=employee_covid_symptoms_two]:checked').val());
            formData.append('employee_health_declaration', $('#employee_health_declaration').val());
            formData.append('employee_declaration', $('#employee_declaration').val());
            formData.append('employee_digitally_signed_declaration', $('input[name=employee_digitally_signed_declaration]:checked').val());
            formData.append('company_employee_id', $('#company-employee-id-input').val());
            formData.append('is_vaccinated', $('input[name=is_vaccinated]:checked').val());



            let url = "{{route('store-employee-covid-form')}}";
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-employee-covid-button').notify(response.message, "success");

                    setTimeout(function () {
                        window.location.href = '/employees-covid-login';
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    $('#page-header').notify(response.message, "error");
                }
            });

        });

    </script>

@endsection
