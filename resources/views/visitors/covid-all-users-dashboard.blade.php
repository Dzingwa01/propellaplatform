@extends('layouts.app')

@section('content')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <br>
    <br>

    <div class="container">
        <h5><b>Hi</b> {{$user->name}} {{$user->surname}}</h5>

        <div class="row">
            <div class="row">
                <div class="row">
                    <input hidden disabled id="user-id-input" value="{{$user->id}}">
                </div>
                <div class=" left-align row">
                    <h5><b>Are you at risk of Covid-19 ?</b></h5>
                    <p>Please answer following questions</p>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="staff_temperature" type="text" required class="validate">
                        <label for="staff_temperature"><span style="color: red">Enter your
                            Temperature</span></label>
                    </div>
                </div>
                @if($user->is_vaccinated == 'Yes')
                    @else
                <div class="row">
                    <h6 style="color: #2352aa">Have you been Vaccinated:</h6>
                </div>
                <div class="row">
                    <p>
                        <label>
                            <input id="yes" name="is_vaccinated" value="Yes" required type="radio"/>
                            <span>Yes</span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <input id="no" name="is_vaccinated" value="No" type="radio"/>
                            <span>No</span>
                        </label>
                    </p>

                </div>
                    @endif
            </div>


            <div class="row">
                <h6 style="color: #2352aa">Have you come into close contact (within 6 feet) with someone who has a
                    laboratory confirmed COVID–19 diagnosis in the past 10 days?</h6>
            </div>
            <div class="row">
                <div class="row">
                    <div class="row">
                        <p>
                            <label>
                                <input id="yes" name="staff_covid_symptoms_one" required value="Yes" type="radio"/>
                                <span>Yes</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input id="no" name="staff_covid_symptoms_one" value="No" type="radio"/>
                                <span>No</span>
                            </label>
                        </p>

                    </div>

                </div>
            </div>
            <div class="row">
                <h6 style="color: #2352aa">Do you have any of the following: fever or chills, cough, shortness of breath
                    or difficulty breathing, body aches, headache, new loss of taste or smell, sore throat?
                </h6>
            </div>
            <div class="row">
                <div class="row">
                    <div class="row">
                        <p>
                            <label>
                                <input id="yes" name="staff_covid_symptoms_two" value="Yes" required type="radio"/>
                                <span>Yes</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input id="no" name="staff_covid_symptoms_two" value="No" type="radio"/>
                                <span>No</span>
                            </label>
                        </p>
                    </div>

                </div>
            </div>


            <div class="row">
                <h6 style="color: #2352aa">* Digitally Signed Declaration</h6>
                <p style="color: #2352aa">By checking this box, you are digitally and legally agreeing to the
                    declarations as stated in this screening form.</p>
                <p>
                    <label>
                        <input id="no" name="staff_digitally_signed_declaration" value="Agree" required type="radio"/>
                        <span>Agree</span>
                    </label>
                </p>
            </div>
            <div class="row" style="margin-left: 2em; margin-right: 2em;">
                <button class="btn right" id="submit-visitor-covid-button">SUBMIT</button>
            </div>
        </div>


    </div>



    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });


        $('#submit-visitor-covid-button').on('click', function () {
            let formData = new FormData();

            formData.append('name', $('#name').val());
            formData.append('surname', $('#surname').val());
            formData.append('email', $('#email').val());
            formData.append('staff_temperature', $('#staff_temperature').val());
            formData.append('staff_covid_symptoms_one', $('input[name=staff_covid_symptoms_one]:checked').val());
            formData.append('staff_covid_symptoms_two', $('input[name=staff_covid_symptoms_two]:checked').val());
            formData.append('staff_health_declaration', $('#staff_health_declaration').val());
            formData.append('staff_declaration', $('#staff_declaration').val());
            formData.append('staff_digitally_signed_declaration', $('input[name=staff_digitally_signed_declaration]:checked').val());
            formData.append('user_id', $('#user-id-input').val());
            formData.append('is_vaccinated', $('input[name=is_vaccinated]:checked').val());


            let url = "{{route('store-staff-covid-form')}}";
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-visitor-covid-button').notify(response.message, "success");

                    setTimeout(function () {
                        window.location.href = '/staff-login';
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    $('#page-header').notify(response.message, "error");
                }
            });

        });

    </script>

@endsection
