@extends('layouts.app')

@section('content')

    <br>
    <br>

    <input hidden disabled id="visitor-id-input" value="{{$visitor->id}}">

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white">Visits / Appointments / Events Summary</h5>
                </div>
            </div>
        </div>

        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="visitors-summary-table">
                    <thead>
                    <tr>
                        <th>Date / Time</th>
                        <th>Reason</th>
                        <th>Details</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {

            let visitor_id = $('#visitor-id-input').val();

            $(function () {
                $('#visitors-summary-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/visitors/visitor-get-summary/' + visitor_id,
                    columns: [
                        {data: 'date_in', name: 'date_in'},
                        {data: 'visit_category', name: 'visit_category'},
                        {data: 'visit_reason', name: 'visit_reason'}
                    ]
                });
                $('select[name="visitors-summary-table_length"]').css("display","inline");
            });

        });

    </script>
@endsection