@extends('layouts.clerk-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="./js/numeric-1.2.6.min.js"></script>
        <script src="./js/bezier.js"></script>
        <script src="./js/jquery.signaturepad.js"></script>
        <script type="text/javascript"
                src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
        <script src="./js/json2.min.js"></script>
    </head>
    <br/>
    <br/>
    <form id="clerk-create-visitor-form">
        <div class="card row" style="width: 800px;margin: 0 auto;">
            <br/>
            <a><img style="width: 200px;margin-left: 600px" src="/images/Propella_Logo.jpg" class=""/></a>
            <h4 style="margin-left: 250px;font-size: 2em" id="page-header">Welcome To Propella</h4>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="col s6" id="visit_category" >
                    <p>
                        <label>
                            <input name="visit_category" type="radio" value="Appointment" />
                            <span>Appointment</span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <input name="visit_category" type="radio" value="Event" />
                            <span>Event</span>
                        </label>
                    </p><p>
                        <label>
                            <input name="visit_category" type="radio" value="Supplier" />
                            <span>Supplier</span>
                        </label>
                    </p><p>
                        <label>
                            <input name="visit_category" type="radio" value="Other" />
                            <span>Other</span>
                        </label>
                    </p>
                </div>
                {{--                <div class="col s6">--}}
                {{--                    <select id="visit_category" name="visit_category" required>--}}
                {{--                        <option value="" disabled selected>Reason for visit</option>--}}
                {{--                        <option value="Appointment">Appointment</option>--}}
                {{--                        <option value="Event">Event</option>--}}
                {{--                        <option value="Supplier">Supplier</option>--}}
                {{--                        <option value="Other">Other</option>--}}
                {{--                    </select>--}}
                {{--                </div>--}}


                <div class="col s6" hidden id="appointment-input-div">
                    <input id="appointment-input" name="appointment-input" placeholder="Person You Are Visiting?" type="text" class="validate">
                </div>


                <div class="col s6" hidden id="event-drop-down-div">
                    <p style="color: grey">Choose an Event</p>
                    @foreach($events as $event)
                        @if($today <=$event->end->toDateString())
                            @if($event->type =='Public')
                                <p>
                                    <label>
                                        <input name="event-input" type="radio" value="{{$event->id}}"/>
                                        <span>{{$event->title}}</span>
                                    </label>
                                </p>
                            @endif
                        @endif
                    @endforeach


                    {{--                    <select id="event-input" name="event-input" data-value="">--}}
                    {{--                        <option disabled selected>Choose Event</option>--}}
                    {{--                        @foreach($events as $event)--}}
                    {{--                            @if($today <= $event->end->toDatestring())--}}
                    {{--                                @if($event->type == 'Public')--}}
                    {{--                                    <option value="{{$event->id}}">{{$event->title}}</option>--}}
                    {{--                                @endif--}}
                    {{--                            @endif--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    {{--                    <label for="event-input">Choose Event</label>--}}
                </div>

                <div class="col s6" hidden id="supplier-input-div">
                    <input id="supplier-input" name="supplier-input" type="text" class="validate" placeholder="Supplier?">
                </div>

                <div class="col s6" hidden id="other-input-div">
                    <input id="other-input" name="other-input" type="text" class="validate" placeholder="Reason for visit?">
                    <select id="found-us-via-input" name="found-us-via-input">
                        <option value="" disabled selected>How did you hear about us?</option>
                        <option value="Radio">Radio</option>
                        <option value="Email Campaign">Email Campaign</option>
                        <option value="Facebook">Facebook</option>
                        <option value="Website">Website</option>
                        <option value="LinkedIn">LinkedIn</option>
                        <option value="Twitter">Twitter</option>
                        <option value="Print Media">Print Media</option>
                        <option value="Other">Other</option>
                    </select>
                    <input id="found-us-via-other-input" name="found-us-via-other-input" hidden placeholder="Please specify?" type="text">
                </div>
            </div>

            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col m6">
                    <select id="title" name="title" required>
                        <option value="" disabled selected>Choose Title</option>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Miss</option>
                        <option value="Ms">Ms</option>
                        <option value="Dr">Dr</option>
                    </select>
                    <label>Title</label>
                </div>
                <div class="input-field col s6">
                    <input id="first_name" name="first_name" type="text" class="validate" required>
                    <label for="first_name">First Name</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="last_name" name="last_name" type="text" class="validate" required>
                    <label for="last_name">Last Name</label>
                </div>
                <div class="input-field col s6">
                    <input id="company" name="company" type="text" class="validate" required>
                    <label for="company">Your Company Name</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="designation" name="designation" type="text" class="validate" required>
                    <label for="designation">Designation</label>
                </div>
                <div class="input-field col s6">
                    <input id="email" name="email" type="email" class="validate" required>
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em; margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="cell_number" name="cell_number" type="text" class="validate" required>
                    <label for="cell_number">Cell number</label>
                </div>
                <div class="input-field col s6">
                    <input id="dob" name="dob" type="date" class="validate" required>
                    <label for="dob">Date of Birth</label>
                </div>
            </div>


            <div class="row" style="margin-left: 550px;">
                <input type="submit" class="waves-effect waves-light btn">
            </div>
            <br/>
        </div>
    </form>
    <br/>
    <br/>
    <style>

    </style>

    <script>
        $(document).ready(function () {

            $('#visit_category').change(function(){
                if ($("input[name='visit_category']:checked").val() === 'Appointment') {
                    $('#appointment-input-div').show();
                    $('#event-drop-down-div').hide();
                    $('#supplier-input-div').hide();
                    $('#other-input-div').hide();
                } else if ($("input[name='visit_category']:checked").val() === 'Event') {
                    $('#appointment-input-div').hide();
                    $('#event-drop-down-div').show();
                    $('#supplier-input-div').hide();
                    $('#other-input-div').hide();
                } else if ($("input[name='visit_category']:checked").val() === 'Supplier') {
                    $('#appointment-input-div').hide();
                    $('#event-drop-down-div').hide();
                    $('#supplier-input-div').show();
                    $('#other-input-div').hide();
                } else if ($("input[name='visit_category']:checked").val() === 'Other') {
                    $('#appointment-input-div').hide();
                    $('#event-drop-down-div').hide();
                    $('#supplier-input-div').hide();
                    $('#other-input-div').show();
                }
            });

            $('#found-us-via-input').change(function(){
                if($('#found-us-via-input').val() === 'Other'){
                    $('#found-us-via-other-input').show();
                } else {
                    $('#found-us-via-other-input').hide();
                }
            });

            $('#clerk-create-visitor-form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();

                formData.append('title', $('#title').val());
                formData.append('first_name', $('#first_name').val());
                formData.append('last_name', $('#last_name').val());
                formData.append('company', $('#company').val());
                formData.append('designation', $('#designation').val());
                formData.append('email', $('#email').val());
                formData.append('cell_number', $('#cell_number').val());
                formData.append('land_line', $('#land_line').val());
                formData.append('dob', $('#dob').val());

                if ($("input[name='visit_category']:checked").val() === 'Appointment') {
                    formData.append('visit_category', $("input[name='visit_category']:checked").val());
                    formData.append('visit_reason', $('#appointment-input').val());
                }

                if($('#visit_category').val() === 'Appointment'){
                    formData.append('visit_category', $('#visit_category').val());
                    formData.append('visit_reason', $('#appointment-input').val());
                }else if ($("input[name='visit_category']:checked").val() === 'Event') {
                    formData.append('visit_category', $("input[name='visit_category']:checked").val());
                    formData.append('event_id', $("input[name='event-input']:checked").val());
                } else if ($('#visit_category').val() === 'Supplier'){
                    formData.append('visit_category', $('#visit_category').val());
                    formData.append('visit_reason', $('#supplier-input').val());
                } else if ($('#visit_category').val() === 'Other'){
                    formData.append('visit_category', $('#visit_category').val());
                    formData.append('visit_reason', $('#other-input').val());
                }

                if($('#found-us-via-input').val() === 'Other'){
                    formData.append('found_us_via', $('#found-us-via-other-input').val());
                }


                let url = "{{route('visitor.storeViaClerk')}}";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#page-header').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/visitors/clerk/visitor-login';
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        $('#page-header').notify(response.message, "error");
                    }
                });
            });
        });

    </script>
@endsection
