@extends('layouts.clerk-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <br>
    <br>

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white" id="page-header">Reason for visit</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="center">
                <div class="col l12 card">
                    <div class="row" style="margin-left: 2em; margin-right: 2em;">
                        <div class="col s6">
                            <h6><b>Today's Bootcamp</b></h6>
                        </div>

                        <input hidden disabled value="{{$bootcamper->id}}" id="bootcamper-id-input">

                        <div class="col s6"  id="event-drop-down-div">
                            @if(isset($event) and isset($event->event))
                                <input id="event-input" type="text" hidden disabled value="{{$event->event->id}}"/>
                                <input type="text" disabled value="{{$event->event->title}}"/>
                                @else
                                <p>No events</p>
                                @endif
                        </div>
                    </div>
                    @if($bootcamper->bootcamp_nda_agreement == false)
                        <div class="row" style="margin-left: 2em; margin-right: 2em;">
                            <p class="right">Please view our Non Disclosure Agreement and accept it by clicking <a class="modal-trigger" href="#modal1"> here </a>.</p>
                        </div>
                    @endif
                    <div class="row" style="margin-left: 2em; margin-right: 2em;">
                        <button class="btn right" id="submit-reason-for-visit-button">Attend</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Agreement modal--}}
    <div id="modal1" class="modal">
        <div class="modal-content">
            <img src="/images/Prop agrre.png">
            <div class="col s5" style="margin-left: 15em;margin-right: 15em">
                <h5 class="center-align"><b>Open Communications FutureMakers Multi-Party Nondisclosure Agreement</b></h5>
            </div>
            <p>This multi-party nondisclosure agreement ("Agreement") is entered into and made effective as of the date set forth above among the parties listed at the end of this Agreement and any third party that executes this Agreement after the Effective Date. This Agreement is intended to cover the parties’ confidentiality obligations with respect to disclosures regarding or pertinent to the exploration ways the parties can work together for mutual benefit.</p>
            <p><b>The Parties agree as follows:</b></p>
            <p>1.	Confidential Information. The confidential, proprietary and trade secret information of each party ("Confidential Information") to be disclosed hereunder is that tangible or intangible information which is marked with a "Confidential," "Proprietary", or similar legend and which is clearly identified in writing as provided under this Agreement and includes any information disclosed during discussion sessions. To be considered Confidential Information, non-tangible disclosures must be identified as confidential under the terms and conditions of this Agreement prior to disclosure and reduced to writing, marked as provided above, and delivered to the receiving party within thirty (30) calendar days of the original date of disclosure. Any information received from any other participant in Bootcamp 1 will not be used by any other party.
                Ideas or discussions not forming part of the Disclosing Parties normal course of business and ideas generated during discussions will not be regarded as confidential information.
            </p>
            <p>2.	Obligations of receiving party. Each receiving party will maintain the confidentiality of the Confidential Information for each disclosing party with at least the same degree of care that it uses to protect its own confidential and proprietary information, but no less than a reasonable degree of care under the circumstances. No receiving party will disclose any of a disclosing party's confidential information to any employees or to any third parties except to employees, contractors, and Affiliates (as below defined) of such receiving party who have a need to know and who agree to abide by nondisclosure terms at least as
                comprehensive as those set forth herein; provided such receiving party will be liable for breach of any such entity. The receiving party will not make any copies of the Confidential Information received from a disclosing party except as necessary for their employees and contractors and those employees and contractors of the receiving party's Affiliates with a need to know. Any copies which are made will include the legend and identification statement as supplied by the disclosing party.  The term “Affiliate” of a party, as used herein, shall mean any company, corporation or other business entity which directly or indirectly (i) controls said party or (ii) is controlled by said party or (iii) is controlled by any entity under (i) above, where “to control” an entity shall mean to own more than fifty per cent (50%) of the shares of, or of the ownership interest in, such entity.
            </p>
            <p>3.    Period of confidentiality. The receiving party’s obligation of confidentiality shall be for a period of two (2) years from the date of receipt of the disclosing party’s Confidential Information</p>
            <p>4.    Termination of obligation of confidentiality. No receiving party will be liable for the disclosure of a disclosing party's confidential information which is:</p>
            <p>a)	Published with the consent of the disclosing party or is otherwise rightfully in the public domain other than by a breach of a duty of confidentiality to the disclosing party;</p>
            <p>b)   Rightfully received from a third party without any obligation of confidentiality;</p>
            <p>c)	Rightfully known to the receiving party without any limitation on use or disclosure prior to its receipt from the disclosing party</p>
            <p>d)   Independently developed by employees of the receiving party; or</p>
            <p>e)    Generally made available to third parties by the disclosing party without restriction on disclosure.</p>
            <p>5.    Title. Title or the right to possess Confidential Information as between receiving parties and the disclosing party will remain in the disclosing party.</p>
            <p>6.    No obligation of disclosure; Termination. No party has an obligation to disclose Confidential Information to any other
                party.  Any party may withdraw from this Agreement at any time and without cause upon written notice to the remaining
                parties, provided that such withdrawing party's obligations with respect to Confidential Information of other parties
                disclosed during the term of this Agreement will survive any such termination until the end of the period set forth in
                Section 3. A disclosing party may at any time: (a) cease giving Confidential Information to the other party without any
                liability, and/or (b) request in writing the return or destruction of all or part of its Confidential Information previously
                disclosed, and all copies thereof, and each other party will comply with such request, and certify in writing its compliance
                within sixty (60) calendar days.
            </p>
            <p>7.	No warranty.  All parties acknowledge that all information provided hereunder is provided "AS IS" WITH NO WARRANTIES WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, AND THE PARTIES EXPRESSLY DISCLAIM ANY WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, FITNESS FOR ANY PARTICULAR PURPOSE, OR ANY WARRANTY OTHERWISE ARISING OUT OF ANY PROPOSAL, SPECIFICATION, OR SAMPLE.</p>
            <p>8.    General</p>
            <p>a)	This Agreement is neither intended to, nor will it be construed as creating a joint venture, a partnership or other form of a business association between the parties, nor an obligation to buy or sell products using or incorporating the Confidential Information.</p>
            <p>b)   Each party understands and acknowledges that no license under any patents, copyrights, trademarks, or maskworks is granted to or conferred upon any party or by the disclosure of any Confidential Information by one party to the other party as contemplated hereunder, either expressly, by implication, inducement, estoppel or otherwise, and that any license under such intellectual property rights must be express and in writing.</p>
            <p>c)	The failure of any party to enforce any rights resulting from breach of any provision of this Agreement will not be deemed a waiver of any right relating to a subsequent breach of such provision or of any other right hereunder.</p>
            <p>d)   This Agreement constitutes the entire agreement between the parties with respect to the disclosures of Confidential
                Information, and may not be amended except in a writing signed by a duly authorized representative of the respective parties. e)    Each party shall execute a copy of this Agreement, each of which shall be deemed an original and all of which together shall
                constitute one instrument. A list shall be maintained of all parties that have executed this Agreement and shall be made available to all parties.
            </p>
            <p>e)    Each party shall execute a copy of this Agreement, each of which shall be deemed an original and all of which together shall
                constitute one instrument. A list shall be maintained of all parties that have executed this Agreement and shall be made available to all parties.
            </p>
            <p id="thank-you">f)    This Agreement shall be subject to and governed by the law of South Africa and the place of jurisdiction shall be Port Elizabeth, South Africa.

            </p>
        </div>

        <div class="row" style="margin-left: 2em; margin-right: 2em;">
            <button class="btn right" id="agreement-button">Agree</button>
        </div>
    </div>


    <script>
        $('#submit-reason-for-visit-button').on('click', function () {
            let formData = new FormData();
            formData.append('bootcamper_id', $('#bootcamper-id-input').val());
            //formData.append('event_id', $("input[name='event-input']:checked").val());
            formData.append('event_id', $('#event-input').val());

            let url = "{{route('update-bootcamper-event')}}";


            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    console.log("reponse",response);
                    $('#page-header').notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/join-event';
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    $('#page-header').notify(response.message, "error");
                }
            });
        });

        //Bootcamp agreement
        $('#agreement-button').on('click', function () {
            let bootcamper_id = $('#bootcamper-id-input').val();
            let formData = new FormData();

            let url = '/update-bootcamp-agreement/' + bootcamper_id;

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                success: function (response, a, b) {
                    $('#thank-you').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                    sessionStorage.clear();
                },
            });
        });
    </script>

@endsection
