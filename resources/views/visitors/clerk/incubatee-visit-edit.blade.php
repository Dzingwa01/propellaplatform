@extends('layouts.app')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <br>
    <br>

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white" id="page-header">Reason for visit</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="center">
                <div class="col l12 card">
                    <div class="row" style="margin-left: 2em; margin-right: 2em;">
                        <div class="col s6">
                            <h6><b>Which event / workshop are you here to attend?</b></h6>
                        </div>

                        <input hidden disabled value="{{$incubatee->id}}" id="incubatee-id-input">

                        <div class="col s6"  id="event-drop-down-div">
                            <p style="color: grey">Choose an Event</p>
                            @foreach($events as $event)
                                @if($event->start->toDateString() == $today)
                                <p>
                                    <label>
                                        <input name="event-input" type="radio" value="{{$event->id}}"/>
                                        <span>{{$event->title}}</span>
                                    </label>
                                </p>
                                @endif
                            @endforeach
                        </div>

                    </div>
                    <div class="row" style="margin-left: 2em; margin-right: 2em;">
                        <button class="btn right" id="submit-reason-for-visit-button">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>
        $('#submit-reason-for-visit-button').on('click', function () {
            let formData = new FormData();
            formData.append('incubatee_id', $('#incubatee-id-input').val());
            formData.append('event_id', $("input[name='event-input']:checked").val());
            // formData.append('event_id', $('#event-input').val());

            let url = "{{route('update-incubatee-event')}}";
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    console.log("reponse",response);
                    $('#page-header').notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/join-event';
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    $('#page-header').notify(response.message, "error");
                }
            });
        });
    </script>

@endsection
