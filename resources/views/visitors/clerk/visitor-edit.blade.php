@extends('layouts.clerk-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <br>
    <br>

    <div class="container">
        <form id="clerk-visitor-edit-form">
            <div class="row">
                <div class="center">
                    <div class="col l12 card" style="background: #6c757d">
                        <h5 style="color: white" id="reason-header">Reason for visit</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="center">
                    <div class="col l12 card">
                        <div class="row" style="margin-left: 2em; margin-right: 2em;">
                            <div class="col s6">
                                <p>
                                    <label>
                                        <input name="visit_category" type="radio" value="Appointment" />
                                        <span>Appointment</span>
                                    </label>
                                </p>
                                <p>
                                    <label>
                                        <input name="visit_category" type="radio" value="Event" />
                                        <span>Event</span>
                                    </label>
                                </p><p>
                                    <label>
                                        <input name="visit_category" type="radio" value="Supplier" />
                                        <span>Supplier</span>
                                    </label>
                                </p><p>
                                    <label>
                                        <input name="visit_category" type="radio" value="Other" />
                                        <span>Other</span>
                                    </label>
                                </p>
                            </div>

                            <div class="col s6" hidden id="appointment-input-div">
                                <input id="appointment-input" name="appointment-input" placeholder="Person You Are Visiting?" type="text"
                                       class="validate">
                            </div>

                            <div class="col s6" hidden id="event-drop-down-div">
                                    @foreach($events as $event)
                                        @if($today <= $event->end->toDatestring())
                                            @if($event->type == 'Public')
                                            <p>
                                                <label>
                                                    <input name="event" type="radio" value="{{$event->id}}" />
                                                    <span>{{$event->title}}</span>
                                                </label>
                                            </p>
                                            @endif
                                        @endif
                                    @endforeach
                            </div>

                            <div class="col s6" hidden id="supplier-input-div">
                                <input id="supplier-input" name="supplier-input" type="text" class="validate" placeholder="Supplier?">
                            </div>

                            <div class="col s6" hidden id="other-input-div">
                                <input id="other-input" type="text" name="other-input" class="validate" placeholder="Reason for visit?">
                                <select id="found-us-via-input" name="found-us-via-input">
                                    <option value="" disabled selected>How did you hear about us?</option>
                                    <option value="Radio">Radio</option>
                                    <option value="Email Campaign">Email Campaign</option>
                                    <option value="Facebook">Facebook</option>
                                    <option value="Website">Website</option>
                                    <option value="LinkedIn">LinkedIn</option>
                                    <option value="Twitter">Twitter</option>
                                    <option value="Print Media">Print Media</option>
                                    <option value="Other">Other</option>
                                </select>
                                <input id="found-us-via-other-input" name="found-us-via-other-input" hidden placeholder="Please specify?" type="text">
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em; margin-right: 2em;">
                            <input class="btn right" type="submit">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="container">
        <form id="clerk-update-visitor-details-form">
            <div class="row">
                <div class="center">
                    <div class="col l12 card" style="background: #6c757d">
                        <h5 style="color: white" id="details-header">Visitor Details</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="center">
                    <div class="col l12 card">
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <input hidden disabled id="visitor-id-input" name="visitor-id-input" value="{{$visitor->id}}">
                            <div class="input-field col m6">
                                <select id="title" name="title">
                                    <option value="" disabled selected>Choose Title</option>
                                    <option value="Mr" {{$visitor->title=='Mr'?'selected':''}}>Mr</option>
                                    <option value="Mrs"{{$visitor->title=='Mrs'?'selected':''}}>Mrs</option>
                                    <option value="Miss"{{$visitor->title=='Miss'?'selected':''}}>Miss</option>
                                    <option value="Ms"{{$visitor->title=='Ms'?'selected':''}}>Ms</option>
                                    <option value="Dr"{{$visitor->title=='Dr'?'selected':''}}>Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="first_name" name="first_name" type="text" value="{{$visitor->first_name}}" class="validate">
                                <label for="first_name">First Name</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col s6">
                                <input id="last_name" name="last_name" type="text" value="{{$visitor->last_name}}" class="validate">
                                <label for="last_name">Last Name</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="company" name="company" type="text" value="{{$visitor->company}}" class="validate">
                                <label for="company">Your Company Name</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col s6">
                                <input id="designation" name="designation" type="text" value="{{$visitor->designation}}" class="validate">
                                <label for="designation">Designation</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="email" name="email" type="email" value="{{$visitor->email}}" class="validate">
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em; margin-right: 2em;">
                            <div class="input-field col s6">
                                <input id="cell_number" name="cell_number" value="{{$visitor->cell_number}}" type="text" class="validate">
                                <label for="cell_number">Cell number</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="dob" name="dob" type="date" value="{{$visitor->dob}}" class="validate">
                                <label for="dob">Date of Birth</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em; margin-right: 2em;">
                            <input class="btn right" type="submit">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>



    <script>

        $(document).ready(function () {
            $("input[name='visit_category']").on('change', function(){
                if ($("input[name='visit_category']:checked").val() === 'Appointment') {
                    $('#appointment-input-div').show();
                    $('#event-drop-down-div').hide();
                    $('#supplier-input-div').hide();
                    $('#other-input-div').hide();
                } else if ($("input[name='visit_category']:checked").val() === 'Event') {
                    $('#appointment-input-div').hide();
                    $('#event-drop-down-div').show();
                    $('#supplier-input-div').hide();
                    $('#other-input-div').hide();
                } else if ($("input[name='visit_category']:checked").val() === 'Supplier') {
                    $('#appointment-input-div').hide();
                    $('#event-drop-down-div').hide();
                    $('#supplier-input-div').show();
                    $('#other-input-div').hide();
                } else if ($("input[name='visit_category']:checked").val() === 'Other') {
                    $('#appointment-input-div').hide();
                    $('#event-drop-down-div').hide();
                    $('#supplier-input-div').hide();
                    $('#other-input-div').show();
                }
            });


            $('#found-us-via-input').change(function () {
                if ($('#found-us-via-input').val() === 'Other') {
                    $('#found-us-via-other-input').show();
                } else {
                    $('#found-us-via-other-input').hide();
                }
            });
        });


        $('#clerk-update-visitor-details-form').on('submit', function (e) {
            e.preventDefault();
            let formData = new FormData();

            formData.append('title', $('#title').val());
            formData.append('first_name', $('#first_name').val());
            formData.append('last_name', $('#last_name').val());
            formData.append('company', $('#company').val());
            formData.append('designation', $('#designation').val());
            formData.append('email', $('#email').val());
            formData.append('cell_number', $('#cell_number').val());
            formData.append('dob', $('#dob').val());
            formData.append('visitor_id', $('#visitor-id-input').val());


            let url = "{{route('visitor.updateBasicDetails')}}";
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#details-header').notify(response.message, "success");
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    $('#details-header').notify(response.message, "error");
                }
            });
        });


        $('#clerk-visitor-edit-form').on('submit', function (e) {
            e.preventDefault();
            let formData = new FormData();

            if ($("input[name='visit_category']:checked").val() === 'Appointment') {
                formData.append('visit_category', $("input[name='visit_category']:checked").val());
                formData.append('visit_reason', $('#appointment-input').val());
            } else if ($("input[name='visit_category']:checked").val() === 'Event') {
                formData.append('visit_category', $("input[name='visit_category']:checked").val());
                formData.append('event_id', $("input[name='event']:checked").val());
            } else if ($("input[name='visit_category']:checked").val() === 'Supplier') {
                formData.append('visit_category', $("input[name='visit_category']:checked").val());
                formData.append('visit_reason', $('#supplier-input').val());
            } else if ($("input[name='visit_category']:checked").val() === 'Other') {
                formData.append('visit_category', $("input[name='visit_category']:checked").val());
                formData.append('visit_reason', $('#other-input').val());
            }

            if ($('#found-us-via-input').val() === 'Other') {
                formData.append('found_us_via', $('#found-us-via-other-input').val());
            }

            formData.append('visitor_id', $('#visitor-id-input').val());

            console.log();

            let url = "{{route('visitor-update-visit')}}";
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    console.log("reponse",response);
                    $('#reason-header').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.href = '/visitors/clerk/visitor-login';
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    $('#reason-header').notify(response.message, "error");
                }
            });

        });
    </script>

@endsection
