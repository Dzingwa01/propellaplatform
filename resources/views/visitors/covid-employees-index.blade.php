@extends('layouts.admin-layout')

@section('content')

    <div class="container-fluid" style="margin-top: 10vh">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Covid 19 - ALL COMPANY EMPLOYEES</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="company_employee_covid_forms-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Temperature</th>
                        <th>COVID–19 diagnosis in the past 10 days?</th>
                        <th>Do you have any of the symptoms</th>
                        <th>Signed Declaration</th>
                        <th>Date</th>
                        <th>Vaccine</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>


    </div>

    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                let visitor_id = $('#visitor-id-input').val();
                $('select').formSelect();
                $(function () {
                    $('#company_employee_covid_forms-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        stateSave: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-employees-covid-forms')}}',
                        columns: [
                            {data: 'name'},
                            {data: 'surname'},
                            {data: 'email'},
                            {data: 'employee_temperature'},
                            {data: 'employee_covid_symptoms_one'},
                            {data: 'employee_covid_symptoms_two'},
                            {data: 'employee_digitally_signed_declaration'},
                            {data: 'date'},
                            {data: 'is_vaccinated'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]

                    });
                    $('select[name="company_employee_covid_forms-table_length"]').css("display", "inline");
                });
            });

        </script>
    @endpush

@endsection
