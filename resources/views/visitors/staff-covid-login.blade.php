@extends('layouts.app')

@section('content')
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="/css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="/css/site.css"  media="screen,projection"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="center" style=" margin:auto;
        width: 400px;
        margin-top:5%;">
                <div class="col s12 card">
                    <div class="center">
                        <a href="/">  <img style="width: 60%" src="/images/Propella_Logo.jpg"/></a>
                    </div>
                    <div>
                        <h5 class="center-align" id="page-header">Propella users only</h5>
                    </div>
                    <div class="row" style="padding-top:2em;padding-left: 2em;padding-right: 2em;">
                        <div class="input-field col s12">
                            <input placeholder="Enter email address" name="email" id="email" type="email" class="validate">
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 2em;padding-right: 2em;">
                        <button class="btn right" id="search-covid-staff-email-button">Login</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="/js/materialize.min.js"></script>


    <script>
        $(document).ready(function(){

            $('#search-covid-staff-email-button').on('click', function(){
                let formData = new FormData();

                formData.append('email', $('#email').val());

                let url = '{{route('search-staff')}}';

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        if(response.message === 'Success'){
                            location.href = '/get-all-users-covid-dashboard/' + response.user_id;
                        }else {
                            $('#page-header').notify(response.message, "info");
                        }

                    },

                    error: function (response) {
                        let message = response.responseJSON.message;
                        $('#page-header').notify(response.message, "error");
                    }
                });
            });
        });
    </script>

    </body>
@endsection
