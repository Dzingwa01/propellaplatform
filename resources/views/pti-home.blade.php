@extends('layouts.pti-layout')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
               <div class="card col s4 white-text" style="margin-left: 310px;top: 10vh;background-color: #e9605c;height: 30vh">
                   <h5 style="margin-left: 7em"><b>PTI Applicants</b></h5>
                   <div class="row">
                       <div class="icon-preview col s6 m3">
                           <i class="material-icons dp48 large">assignment_ind</i>
                       </div>
                        <div class="col s6 m3">
                            <h4 style="margin-left: 3em"> {{$pti_applicants_count}}</h4>
                        </div>
                   </div>
                   <hr>
                   <a class="txt"  href="{{ url('pti-application-index') }}" style="margin-left: 180px;font-size: 1.3em;color: #404040"><b>View Applicants</b></a>
               </div>
            </div>
        </div>
    </div>
    <style>
        .txt:hover {
            text-decoration: underline;
        }
    </style>
    @endsection

