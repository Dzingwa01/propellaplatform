@extends('layouts.app')
{{--   <!DOCTYPE html>
<html>
<head>
   <!--Import Google Icon Font-->
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   <!--Import materialize.css-->
   <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
   <link type="text/css" rel="stylesheet" href="css/site.css"  media="screen,projection"/>


   <!--Let browser know website is optimized for mobile-->
   <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>--}}

@section('content')
    <html>

    <div class="" id="desktopLog">
        <div class="row">
            <div class="login-form" class="center" style=" margin:auto; width: 400px;">
                <!-- session message start -->
                @if (session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                <br>
                <form class="col s12 card" method="post" style="height: 60vh">
                    @csrf
                    <div>
                        <h3 class="center-align">Log In</h3>
                    </div>
                    <div class="row" style="padding-top:2em;padding-left: 2em;padding-right: 2em;">
                        <div class="input-field col s12">
                            <input placeholder="Enter email address" name="email" id="email" type="email"
                                   class="validate">
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 2em;padding-right: 2em;">
                        <div class="input-field col s12">
                            <input placeholder="Enter password" name="password" id="password" type="password"
                                   class="validate">
                            <label for="password">Password</label>
                        </div>
                        <span class="eye" onclick="myFunction()" style="margin-top: 6vh">
                           <i id="hide1" class="fa fa-eye"></i>
                           <i id="hide2" class="fas fa-eye-slash"></i>
                        </span>
                    </div>
                    <div class="row" style="padding-left: 2em;padding-right: 2em;">
                        <button class="btn right" type="submit">Login</button>

                        @if (Route::has('password.request'))
                            <a class="page-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
    </html>

    <head>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
              integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
              crossorigin="anonymous" referrerpolicy="no-referrer"/>

    </head>
    <br>
    <br>
    <br>

    <script>
        function myFunction() {
            var x = document.getElementById("password");
            var y = document.getElementById("hide1");
            var z = document.getElementById("hide2");

            if (x.type === 'password') {
                x.type = "text";
                y.style.display = "block";
                z.style.display = "none";
            } else {
                x.type = "password";
                y.style.display = "none";
                z.style.display = "block";
            }
        }
    </script>


    <style>
        .eye{
            position: absolute;
        }
        #hide1{
            display: none;

        }
    </style>

    <div class="container" id="mobileLog">
                <div class="row">
                    <div class="login-form" class="center" style=" margin:auto; width: auto;">
                        <!-- session message start -->
                        @if (session()->has('success_message'))
                            <div class="alert alert-success">
                                {{ session()->get('success_message') }}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="col s12 card" method="post" style="height: 60vh">
                            @csrf
                            <div>
                                <h5 class="center-align">Log In</h5>
                            </div>
                            <div class="row" style="padding-top:2em;padding-left: 2em;padding-right: 2em;">
                                <div class="input-field col s12">
                                    <input placeholder="Enter email address" name="email" id="email" type="email" class="validate">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="row" style="padding-left: 2em;padding-right: 2em;">
                                <div class="input-field col s12">
                                    <input placeholder="Enter password" name="password" id="password" type="password" class="validate">
                                    <label for="password">Password</label>
                                </div>
                                <span class="eye" onclick="myFunction()" style="margin-top: 6vh">
                           <i id="hide1" class="fa fa-eye"></i>
                           <i id="hide2" class="fas fa-eye-slash"></i>
                        </span>
                            </div>
                            <div class="row" style="padding-left: 3em;padding-right: 2em;">
                                <p>
                                    <label>
                                        <input type="checkbox" class="filled-in"  />
                                        <span>Remember Me</span>
                                    </label>
                                </p>
                            </div>
                            <div class="row" style="padding-left: 2em;padding-right: 2em;">
                                <button class="btn right" type="submit">Login</button>
                                @if (Route::has('password.request'))
                                    <a class="page-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    <br>
    <br>
    <br>

    <style>
        .card{
            top:4vh;
        }
    </style>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
@endsection

{{--</body>
</html>--}}
