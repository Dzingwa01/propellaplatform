@extends('layouts.clerk-layout')

@section('content')

<div class="container" style="margin-top: 10vh;">
    <div class="row center">
        <h1 style="color: black;"> This account is <u>only</u> for visitors login. </h1>
        <i class="material-icons large" style="color: red;">do_not_disturb</i>
    </div>
</div>

@endsection
