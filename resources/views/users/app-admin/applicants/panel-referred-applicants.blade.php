@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Panel Referred Applicants</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Contacted Via</th>
                        <th>Date Declined</th>
                        <th>Reason for refer</th>
                        <th>Referred To</th>
                        <th>Category</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#users-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-panel-decision-referred-applicants')}}',
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'contact_number', name: 'contact_number'},
                        {data:'contacted_via',name:'contacted_via'},
                        {data:'created_at',name:'created_at'},
                        {data:'referred_reason',name:'referred_reason'},
                        {data:'referred_company',name:'referred_company'},
                        {data: 'category', name: 'category'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="users-table_length"]').css("display","inline");
            });
        });

        function confirm_delete_declined_applicant(obj){
            var r = confirm("Are you sure want to delete this declined applicant?");
            if (r === true) {
                $.get('/delete-referred-app/' + obj.id, function (data, status) {
                    if (status === 'success') {
                        alert(data.message);
                        window.location.reload();
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }
    </script>
@endsection

