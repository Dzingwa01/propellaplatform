@extends('layouts.admin-layout')

@section('content')

    <div class="section" style="margin-top: 5vh">
        <input id="user-id-input" value="{{$declinedApplicant->id}}" hidden disabled>
        <div class="col s10 card z-depth-4" style="border-radius: 10px;width: 1350px;margin: 0 auto">
            {{--APPPLICATION ACOUNT INFO--}}
            <div class="row" id="applicant-account-show" style="margin-right: 3em; margin-left: 3em;">
                <br>
                <h4 class="center"><b>OVERVIEW OF : {{$declinedApplicant->name}} {{$declinedApplicant->surname}}</b></h4>
                <div class="row " style="margin-left: 10em">
                    <br>
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h6><b>Title : </b> {{$declinedApplicant->title}}</h6>
                            <h6><b>Initial : </b> {{$declinedApplicant->initials}}</h6>
                            <h6><b>Name : </b> {{$declinedApplicant->name}}</h6>
                            <h6><b>Surname : </b> {{$declinedApplicant->name}}</h6>
                            <h6><b>Email : </b> {{$declinedApplicant->email}}</h6>
                            <h6><b>Contact number : </b> {{$declinedApplicant->contact_number}}</h6>
                            <h6><b>ID Number : </b> {{$declinedApplicant->id_number}}</h6>
                            <h6><b>Date of birth: </b> {{$declinedApplicant->dob}}</h6>
                            <h6><b>Age : </b> {{$declinedApplicant->age}}</h6>
                            <h6><b>Race : </b> {{$declinedApplicant->race}}</h6>
                        </div>
                        <div class="col l6 m6 s12">
                            <h6><b>Address One : </b> {{$declinedApplicant->address_one}}</h6>
                            <h6><b>Address Two  </b> {{$declinedApplicant->address_two}}</h6>
                            <h6><b>Address Three : </b> {{$declinedApplicant->address_three}}</h6>
                            <h6><b>Application Created : </b> {{$declinedApplicant->created_at}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s2"></div>
        <div class="row center">
            <h4><b>ACTIONS</b></h4>
        </div>
        <div class="row" style="margin-left: 400px">
            <div class="card col s4" id="application" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">APPLICATION FORM</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s4" id="contact-log" style="background-color:darkgreen;font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">CONTACT LOG</p>
            </div>
        </div>
    </div>

    <div class="section" style="margin-top: 5vh">
        <div class="col s12 card"  id="application-details-row" hidden style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            {{--APPLICATION FORM--}}
            <br>
            <div class="row"   style="margin-right: 3em; margin-left: 3em;">
                <div class="container" id="QuestionsAndAnswers">
                    <div class="row center">
                        <h4><b>APPLICATION DETAILS</b></h4>
                        <br>
                        <hr style="background: darkblue; height: 5px;">
                        <br>
                    </div>
                    <br>
                    <div class="row">
                        @foreach($declinedApplicantQuestionAnswers as $question_answer)
                            <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                            <div class="input-field">
                                <textarea disabled>{{$question_answer->answer_text}}</textarea>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{-- APPLICANT CONTACT LOG--}}
        <div class="row" id="applicant-contact-log" hidden  style="margin-right: 8em; margin-left: 8em;">
            <br>
            <div class="row center">
                <h4><b>CONTACT LOG</b></h4>
            </div>
            <div class="row">
                <div class="col s4">
                    <h5 style="color: grey;">Contact log history</h5>
                    <br>

                    @foreach($declinedApplicant->contactLog->sortByDesc('date') as $user_log)
                        <p><b>Results of contact</b> : {{$user_log->applicant_contact_results}}</p>
                        <p><b>Comment</b> : {{$user_log->comment}}</p>
                        <p><b>Date</b>:{{$user_log->date}}</p>
                        <hr>
                    @endforeach
                </div>
                <div class="col s1"></div>
                <div class="col s7">
                    <h5 style="color: grey;margin-left: 4.5em">Create applicant contact log</h5>
                    <br>
                    <div class="row" style="margin-left: 7em">
                        <div class="input-field col s10">
                            <select id="applicant_contact_results">
                                <option value="Phone Call">Phone Call</option>
                                <option value="Face-to-Face">Face-to-Face</option>
                                <option value="Email">Email</option>
                                <option value="No response to any contact type">No response to any contact type
                                </option>
                                <option value="Invited, but did not arrive">Invited, but did not arrive</option>
                            </select>
                            <label>You contacted {{$declinedApplicant->name}} {{$declinedApplicant->surname}} via</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 7em">
                        <div class="input-field col s10">
                            <input id="comment" type="text" class="validate">
                            <label for="comment">Comment</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 300px;">
                        <div class="col s6 save-contact-log" hidden>
                            <a class="waves-effect waves-light btn" id="contact-log-submit-button">Save contact log</a>
                        </div>
                        <div class="col s6 send-email-to-applicant" hidden>
                            <a class="waves-effect waves-light btn" id="send-email">Send email</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>

        $('#application').on('click', function () {
            $('#application-details-row').show();
            $('#applicant-contact-log').hide();
        });
        $('#contact-log').on('click', function () {
            $('#applicant-contact-log').show();
            $('#application-details-row').hide();
        });

        $(document).ready(function(){
            $('select').formSelect();
            //CONTACT LOG BUTTON TOGLE
            $('#applicant_contact_results').on('change', function(){
                if(this.value === "Email"){
                    $('.save-contact-log').hide();
                    $('.send-email-to-applicant').show();
                }else{
                    $('.save-contact-log').show();
                    $('.send-email-to-applicant').hide();
                }
            });

            //Applicant contact log
            $('#contact-log-submit-button').on('click',function () {
                let declined_applicant_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('applicant_contact_results', $('#applicant_contact_results').val());

                let url = '/store-declined-applicant-contact-log/' + declined_applicant_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#contact-log-submit-button').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/declined-applicant-account-overview/' + declined_applicant_id;
                        }, 3000);
                    },
                });
            });

            //Send email to applicants
            $('#send-email').on('click',function () {
                let declined_applicant_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('applicant_contact_results', $('#applicant_contact_results').val());

                let url = '/store-declined-applicant-contact-log/' + declined_applicant_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#send-email').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/declined-applicant-account-overview/' + declined_applicant_id;
                        }, 3000);
                    },
                });
            });
        });
    </script>
@endsection
