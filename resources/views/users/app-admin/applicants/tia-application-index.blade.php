@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Tia Applicants</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="applicants-table">
                    <thead>
                    <tr>
                        <th>Full name</th>
                        <th>Contact number</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $(function () {
                    $('#applicants-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        stateSave: true,
                        scrollX: 640,
                        pageLength: 100,
                        ajax: '{{route('get-tia-index')}}',
                        columns: [
                            {data: 'full_name'},
                            {data: 'contact_number'},
                            {data: 'email'},
                            {data: 'action', orderable: true, searchable: false}
                        ]

                    });
                    $('select[name="applicants-table_length"]').css("display", "inline");
                });
            });
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>

@endsection
