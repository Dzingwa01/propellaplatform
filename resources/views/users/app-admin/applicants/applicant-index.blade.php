@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    <nav>
        <div class="nav-wrapper">
            <div class="col s12" style="margin-left: 4em">
                <a href='/home' class="breadcrumb">Home</a>
                <a href="#!" class="breadcrumb">Applicants</a>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Applicants</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="applicants-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Category</th>
                        <th>Date/Time Applied</th>
                        <th>Status</th>
                        <th>Form</th>
                        <th>POPI</th>
                        <th>Data Protected</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#applicants-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        stateSave: true,
                        scrollX: 640,
                        pageLength: 100,
                        ajax: '{{route('get-applicants')}}',
                        columns: [
                            {data: 'name'},
                            {data: 'surname'},
                            {data: 'email'},
                            {data: 'phone'},
                            {data:'category'},
                            {data:'date_time_applied'},
                            {data: 'status'},
                            {data: 'completed_form'},
                            {data: 'popi_act_agreement'},
                            {data: 'data_protected'},
                            {data: 'action', orderable: true, searchable: false}
                        ]

                    });
                    $('select[name="applicants-table_length"]').css("display", "inline");
                });
            });
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>

@endsection
