@extends('layouts.app')

@section('content')

    <style>
        .paragraph { margin-top: -20px; }
    </style>
    <!--Desktop-->
    <body>

    <div class="row" style="margin-top: 10vh" id="desktopShowBlog">
        <div id="crouton" style="margin-left: 7em">
            <ul>
                <li><a href="/">Latest info</a></li>
                <li><a href="#">{{$latestInfo->title}}</a></li>
            </ul>
        </div>
        <div class="col m10" style="margin-left: 7em">
            <p style="font-size: 3em;color: darkgreen;"><b>{{$latestInfo->title}}</b></p>
            <p style="color: lightseagreen">{{$latestInfo->description}}</p>
            {!!$latestInfo->latestInfoSummernote->content!!}

            @if($latestInfo->pdf_url != null)
                <a href="{{'/download-latest-info-pdf/'.$latestInfo->id}}" target="_blank">
                    <img style="margin-left: 1em" class="hoverable" src="/images/word doc.png" width="80" height="100">
                </a>
            @elseif(isset($latestInfo->pdf_url))
                <a href="{{'$latestInfo->pdf_url'}}" target="_blank">
                    <img style="margin-left: 1em" class="hoverable" src="/images/word doc.png" width="80" height="100">
                </a>
            @endif
            @if($latestInfo->word_doc_url != null)
                <a href="{{'/download-latest-info-doc/'.$latestInfo->id}}" target="_blank">
                    <img style="margin-left: 1em" class="hoverable" src="/images/word doc.png" width="80" height="100">
                </a>
            @elseif(isset($latestInfo->word_doc_url))
                <a href="{{'$latestInfo->pdf_url'}}" target="_blank">
                    <img style="margin-left: 1em" class="hoverable" src="/images/word doc.png" width="80" height="100">
                </a>
            @endif
        </div>
        <br>
    </div>

    <!--Mobile-->
   {{-- <div class="container-fluid" id="mobileShowBlog">
        <div class="row" style="margin-top: 10vh;margin-left: 1em;margin-right: 1em">
            <div id="crouton">
                <ul>
                    <li><a href="/media#top-blog">Blog</a></li>
                    <li><a href="#">{{$blog->title}}</a></li>
                </ul>
            </div>

            <p style="font-size: 2em;color: orange;"><b>{{$blog->title}}</b></p>
            <p style="color: lightseagreen">{{$blog->description}}</p>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em">
            <p style="width: 10px">{!!$blog->summernote->content!!}</p>
        </div>
    </div>--}}

    <style>
        #crouton ul {
            margin: 0;
            padding: 0;
            overflow: hidden;
            width: 100%;
            list-style: none;
        }

        #crouton li {
            float: left;
            margin: 0 10px;
        }

        #crouton a {
            background: #ddd;
            padding: .7em 1em;
            float: left;
            text-decoration: none;
            color: #444;
            text-shadow: 0 1px 0 rgba(255,255,255,.5);
            position: relative;
        }

        #crouton li:first-child a {
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        #crouton li:last-child a {
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        #crouton a:hover {
            background: #99db76;
        }

        #crouton li:not(:first-child) > a::before {
            content: "";
            position: absolute;
            top: 50%;
            margin-top: -1.5em;
            border-width: 1.5em 0 1.5em 1em;
            border-style: solid;
            border-color: #ddd #ddd #ddd transparent;
            left: -1em;
        }

        #crouton li:not(:first-child) > a:hover::before {
            border-color: #99db76 #99db76 #99db76 transparent;
        }

        #crouton li:not(:last-child) > a::after {
            content: "";
            position: absolute;
            top: 50%;
            margin-top: -1.5em;
            border-top: 1.5em solid transparent;
            border-bottom: 1.5em solid transparent;
            border-left: 1em solid #ddd;
            right: -1em;
        }

        #crouton li:not(:last-child) > a:hover::after {
            border-left-color: #99db76;
        }
        hr {
            position: relative;
            top: 40px;
            border: none;
            height: 6px;
            background: black;
            margin-bottom: 30px;
        }
    </style>


    <script>
        $(document).ready(function () {

            $('.fixed-action-btn').floatingActionButton();

            $('.modal').modal();
            $('.materialboxed').materialbox();


        });
        function navigateBlog(obj){
            let blog_id= obj.getAttribute('data-value');
            window.location.href="/show-blog-summernote/" + blog_id
        }
    </script>
    </body>
@endsection
