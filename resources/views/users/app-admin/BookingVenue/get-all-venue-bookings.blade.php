@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('admin-venue-booking')}}
    <br>
    <br>
    <div class="container-fluid">

        <div class="row">
          <h5 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">
                Venue Bookings Time Slots</h5>
        </div>
        <br><br>


        <input id="venue-id-input" value="{{$venue->id}}" hidden disabled>
        {{--<input id="venue-id-input" value="{{$venue_bookings->user_venue_booking_id}}" hidden>--}}
        {{--<p>{{$venue_bookings->id}}</p>--}}
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <div class="row" style="margin-left: 2em; margin-right: 2em;">
                    <a class="modal-close waves-effect waves-green btn" onclick="goBack()" style="width: 150px">Back<i class="material-icons right">arrow_back</i>
                    </a>
                </div>
                <br>

                <table class="table table-bordered" style="width: 100%!important;" id="booking-table">
                    <thead>
                    <tr>
                        {{--<th>Venue Name</th>--}}
                        <th>Date</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Booking Person</th>
                        <th>Booking Reason</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        {{--<div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Blog" href="{{url('book-venue')}}">
                <i class="large material-icons">add</i>
            </a>
        </div>--}}
    </div>
    <style>
        .exportExcel {
            padding: 5px;
            border: 1px solid grey;
            margin: 5px;
            cursor: pointer;
        }
            nav {
                margin-bottom: 0;
                background-color: grey;
            }
    </style>
    @push('custom-scripts')
        <script type="text/javascript"
                src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>

        <script type="text/javascript" language="javascript"
                src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>


        <script>

            $(document).ready(function (data) {
                let venue_id = $('#venue-id-input').val();
                $(function () {
                    $('#booking-table').DataTable({
                        dom: 'lBfrtip',
                        buttons: [
                            'copy', 'excel', 'pdf'
                        ],
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type: 'get',
                        scrollX: 640,
                        ajax: '/get-venue-all-bookings/' + venue_id,
                        columns: [
                            /*{data: 'venue_name', name: 'venue_name'},*/
                            {data: 'venue_date', name: 'venue_date'},
                            {data: 'venue_start_time', name: 'venue_start_time'},
                            {data: 'venue_end_time', name: 'venue_end_time'},
                            {data: 'booking_person', name: 'booking_person'},
                            {data: 'booking_reason', name: 'booking_reason'},
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="booking-table_length"]').css("display", "inline");
                });
            });
            function goBack() {
                window.history.back();
            }
            function confirm_delete_booking(obj) {
                var r = confirm("Are you sure want to delete this booking?");
                console.log("Check", r);
                if (r) {
                    $.get('/cancel-delete-check/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status == 'success') {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Delete action cancelled');
                }
            }
        </script>
    @endpush
@endsection
