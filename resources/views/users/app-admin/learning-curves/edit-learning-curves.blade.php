@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    <head>
        <meta charset="UTF-8">
        <title>bootstrap4</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
    </head>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <ul class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="/get-learning-curves">All Learning Curves</a></li>
        <li><a href="/edit-learning-curves">Edit Learning Curve</a></li>
    </ul>
    <body>

    <div class="card hoverable" style=";width: 1100px;margin-left: 200px">
        <div class="container">
            <br>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Update learning curve</h6>
            <br>
            <form id="learning-form" class=" col s12" style="margin-top:1em;">
                @csrf
                <input value="{{$propellaLearningCurve->id}}" id="blog_id" hidden>
                <div class="row">
                    <div class="input-field col m5">
                        <input id="title" value="{{$propellaLearningCurve->title}}" type="text" class="validate">
                        <label for="title">Title</label>
                    </div>
                    <div class="input-field col m5">
                        <textarea id="description" class="materialize-textarea">{{$propellaLearningCurve->description}}</textarea>
                        <label for="description">Description</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col m6">
                        <div class="file-field input-field" style="bottom:0px!important;">
                            <div class="btn">
                                <span>Learning Image</span>
                                <input id="learning_image_url" type="file" name="learning_image_url">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path" value="{{isset($propellaLearningCurve->learning_image_url)?$propellaLearningCurve->learning_image_url:''}}" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col offset-m4">
                        <button id="save-learning-details" style="margin-left: 2em"
                                class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
                <br>
                <br>
            </form>
        </div>
    </div>

    <!--create summernote-->
    <div class="container">
        <div class="panel-body">
            <form action="{{url('/update-learning-curve-summernote/'.$propellaLearningCurve->id)}}" method="POST" style="margin-top: 50px" id="summernote-form">
                <div class="form-group">
                    <textarea id="summernote" name="content" class="summernote" ></textarea>
                </div>
                <div class="form-group">
                    <button type="submit">Submit</button>
                    {!!csrf_field()!!}
                </div>
            </form>
        </div>
    </div>

    @push('custom-scripts')
        <script>

            $(document).ready(function () {

                $('#learning-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();

                    formData.append('learning_curve_id', $('#learning_curve_id').val());
                    formData.append('title', $('#title').val());
                    formData.append('description', $('#description').val());
                    formData.append('added_date', $('#added_date').val());
                    formData.append('content', $('.content').val());

                    jQuery.each(jQuery('#learning_image_url')[0].files, function (i, file) {
                        formData.append('learning_image_url', file);
                    });

                    console.log("propellaLearningCurve", formData);
                    let url = '/update-learning-curves/' + '{{$propellaLearningCurve->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response, a, b) {
                            alert(response.message);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
                var content = {!! json_encode($propellaLearningCurve->learningCurveSummernote->content) !!};
                $('.summernote').summernote('code', content);
                $('#summernote').summernote({
                    placeholder: 'Content here ..',
                    height: 700,
                });
            });
        </script>
    @endpush
    </body>

    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

    </head>



    <head>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

        <!-- include summernote css/js-->
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    </head>
    <style>
        ul.breadcrumb {
            padding: 10px 16px;
            list-style: none;
            background-color:grey;
        }
        ul.breadcrumb li {
            display: inline;
            font-size: 18px;
        }
        ul.breadcrumb li+li:before {
            padding: 8px;
            color: white;
            content: ">\00a0";
        }
        ul.breadcrumb li a {
            color: black;
            text-decoration: none;
        }
    </style>
@endsection
