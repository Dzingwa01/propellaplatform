@extends('layouts.admin-layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">RINP Applicant Categories</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="rinp-applicant-categories-table">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <th># of applicants</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Category" href="#rinp-applicant-category-modal">
            <i class="large material-icons">add</i>
        </a>
    </div>

    <div id="rinp-applicant-category-modal" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Add New Category</h4>
            <input type="text" id="rinp-applicant-category-name-input">
            <button class="btn teal" id="submit-rinp-applicant-category-button">Submit</button>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#rinp-applicant-categories-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-rinp-applicant-categories')}}',
                    columns: [
                        {data: 'category_name', name: 'category_name'},
                        {data: 'applicant_count', name: 'applicant_count'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="rinp-applicant-categories-table_length"]').css("display","inline");
            });
        });

        $('#submit-rinp-applicant-category-button').on('click',function(){
            let formData = new FormData();
            formData.append('category_name', $('#rinp-applicant-category-name-input').val());

            $.ajax({
                url: "",
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',

                success: function (response, a, b) {
                    $('#submit-rinp-applicant-category-button').notify('Added successfully', 'success');

                    setTimeout(function(){
                        window.location.reload();
                    }, 1500);
                },
                error: function (response) {
                    let message = response.message;
                    alert(message);
                }
            });
        });
    </script>
@endsection
