@extends('layouts.admin-layout')

@section('content')
    <div class="container" id="QuestionsAndAnswers">
        <br>
        <div class="row center">
            <h4><u>Applicant Details:</u></h4>
            <div class="row">
                <div class="col l6 m6 s12">
                    <h6>{{$rinpApplicant->name}} {{$rinpApplicant->surname}}</h6>
                    <h6>Email: {{$rinpApplicant->email}}</h6>
                    <h6>Contact Number: {{$rinpApplicant->contact_number}}</h6>
                    <h6>ID Number: {{$rinpApplicant->id_number}}</h6>
                    <h6>Age: {{$rinpApplicant->age}}</h6>
                    <h6>Gender: {{$rinpApplicant->gender}}</h6>
                </div>
                <div class="col l6 m6 s12">
                    <h6>Address One: {{$rinpApplicant->address_one}}</h6>
                    <h6>Address Two: {{$rinpApplicant->address_two}}</h6>
                    <h6>Address Three: {{$rinpApplicant->address_three}}</h6>
                    <h6>Application Created: {{$rinpApplicant->created_at->toDateString()}}</h6>
                </div>
            </div>
            <br>
            <hr style="background: darkblue; height: 5px;">
            <br>
        </div>

        <div class="row">
            @if(isset($rinpApplicant->applicantQuestionAnswers))
                @if(count($rinpApplicant->applicantQuestionAnswers) > 0)
                    @foreach($rinpApplicant->applicantQuestionAnswers as $question_answer)
                        <h6>{{ $question_answer->question_text}} </h6>
                        <div class="input-field">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            @else
                <h6>No questions and answers submitted by applicant.</h6>
            @endif

        </div>
    </div>

    <br>
    <div class="row center">
        <button id="print-button"  class="waves-effect waves-light btn" onclick="printContent('QuestionsAndAnswers')">
            <i class="material-icons left">local_printshop</i>Print
        </button>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }
    </script>

@endsection
