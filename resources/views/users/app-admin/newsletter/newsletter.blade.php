@extends('layouts.admin-layout')

@section('content')
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Newsletter</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="careers-table">
                    <thead>
                    <tr>
                        <th>Full name(s)</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
    @push('custom-scripts')

        <script>

            $(document).ready(function (data) {
                $(function () {
                    $('#careers-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '{{route('get-newsletter')}}',
                        columns: [
                            {data: 'fullname', name: 'fullname'},
                            {data: 'email', name: 'email'},
                        ]
                    });
                    $('select[name="careers-table_length"]').css("display","inline");
                });
            });

        </script>
    @endpush
@endsection
