@extends('layouts.admin-layout')

@section('content') <br>
<br>
<div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
    <br>

    <h6 style="margin-left: 15em">Create Category</h6>
    <div class="row" style="margin-left: 7em">
        <div class="input-field col s10">
            <input id="category_name" type="text" class="validate">
            <label for="category_name">Category name</label>
        </div>
    </div>
    <div class="row" style="margin-left: 600px;">
        <div class="col s4">
            <a class="waves-effect waves-light btn" id="question-category-submit-button">Save</a>
        </div>
    </div>
    <br>
</div>

<script>
    $(document).ready(function(){
        $('#question-category-submit-button').on('click',function () {

            let formData = new FormData();
            formData.append('category_name', $('#category_name').val());

            $.ajax({
                url: '{{route('store-workshop-categories')}}',
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                success: function (response, a, b) {
                    $('#question-category-submit-button').notify(response.message, "success");
                    window.location.reload();
                },
                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });
    });
</script>

@endsection
