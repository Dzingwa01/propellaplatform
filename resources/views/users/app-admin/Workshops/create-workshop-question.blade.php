@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    <div class="row" style="width: 950px;padding-left:9%;">
        <div class="card white" style="padding-left:5%;top: 10vh">

            <h5 style="margin-left: 8em">Create Workshop Questions</h5>
            <div class="row">
                <div class="col s5">
                    <div class="input-field">
                        <select id="w_category_id">
                            <option value="" disabled selected>Chose the question category</option>
                            @foreach($workshopCategories as $category)
                                <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col s5">
                    <div class="input-field">
                        <input style="width:90%" id="question_number" type="number">
                        <label for="question_number">Question Number</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s5">
                    <div class="input-field">
                        <input style="width:90%" id="question_text" type="text" class="validate">
                        <label for="question_text">Question text</label>
                    </div>
                </div>
                <div class="col s5">
                    <div class="input-field col s12" >
                        <select id="question_type">
                            <option value="" disabled selected>Choose question type</option>
                            <option value="1_5">1 - 5</option>
                            <option value="Yes_No">Comment</option>
                            <option value="text">Text</option>
                        </select>
                        <label>Question Type</label>
                    </div>

                </div>
            </div>
            <div class="row" style="margin-left: 500px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn" id="question-submit-button">Save</a>
                </div>
            </div>
            <br/>
        </div>
    </div>

    <script>
        $('#question_type').on('change', function(){
            if($('#question_type').val() === "Multiple"){
                $('#add-sub').show();
            }

        });

        $(document).ready(function() {
            $('select').formSelect();

            $('#question-submit-button').on('click',function () {

                let formData = new FormData();
                formData.append('question_text', $('#question_text').val());
                formData.append('question_number', $('#question_number').val());
                formData.append('question_type', $('#question_type').val());
                formData.append('w_category_id', $('#w_category_id').val());

                $.ajax({
                    url: '{{route('store-workshop-questions')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });

    </script>

@endsection
