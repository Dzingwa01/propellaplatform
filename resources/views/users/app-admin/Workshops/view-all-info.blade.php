@extends('layouts.admin-layout')
@section('content')

    <div class="row">
        <div class="card" style="top: 10vh;width: 800px;margin: 0 auto">
            <br>
            <h5 style="margin-left: 150px"><b>WORKSHOP EVALUATION RESULTS</b></h5>
            <br>
            <div class="row" style="margin-left: 3em;margin-right: 3em">
                <h6>Workshop Title : <b>{{$workshopEvaluation->title}}</b></h6>
                <h6>Date : <b>{{$workshopEvaluation->date}}</b></h6>
                <h6>Venue : <b>{{$workshopEvaluation->venue}}</b></h6>
                <h6>Facilitator : <b>{{$workshopEvaluation->facilitator}}</b></h6>
                <br>
                <h6>Please rate the following items on a scale of one to ten, with one being abysmal, five being acceptable, and ten being perfect.</h6>
                <h6>WORKSHOP VENUE : <b>{{$workshopEvaluation->workshop_venue}}</b></h6>
                <h6>Comment : <b>{{$workshopEvaluation->venue_comment}}</b></h6>
                <h6>WORKSHOP FACILITIES : <b>{{$workshopEvaluation->workshop_facilities}}</b></h6>
                <h6>Comment : <b>{{$workshopEvaluation->facility_comment}}</b></h6>
                <h6>INSTRUCTOR KNOWLEDGE :<b>{{$workshopEvaluation->instructor_knowledge}} </b></h6>
                <h6>Comment : <b>{{$workshopEvaluation->instructor_comment}}</b></h6>
                <h6>QUALITY OF PRESENTATION MATERIALS &HANDOUTS : <b>{{$workshopEvaluation->quality_of_presentation}}</b></h6>
                <h6>Comment : <b>{{$workshopEvaluation->quality_comment}}</b></h6>
                <h6>1.	What is your main take away? : <b>{{$workshopEvaluation->main_take_away}}</b></h6>
                <h6>2.	Would you recommend this course to others? Why or why not? : <b>{{$workshopEvaluation->recommendation_of_course}}</b></h6>
                <h6>3.	Other thoughts you would like to share? : <b>{{$workshopEvaluation->thoughts}}</b></h6>
            </div>
            <br>
        </div>
    </div>


    @endsection
