@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('fund-cat')}}
    <div class="container-fluid">
        <div class="row">
           <br/> <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Resource Funding Category</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="resource_categories-table">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add User Role" href="{{url('create-category')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>


    </div>

    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#resource_categories-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('category-name')}}',
                        columns: [
                            {data: 'category_name', name: 'category_name'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]

                    });
                    $('select[name="resource_categories-table_length"]').css("display","inline");
                } );
            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
