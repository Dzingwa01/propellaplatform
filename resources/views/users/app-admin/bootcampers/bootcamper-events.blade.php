@extends('layouts.admin-layout')

@section('content')
    <input id="user-id-input" disabled hidden value="{{$bootcamper->id}}">

    <br><br>
   {{-- {{ Breadcrumbs::render('admin-bootcamper-events',$bootcamper)}}--}}
    <div class="container-fluid" style="margin-top: 2em;">
        <input hidden disabled id="bootcamper-id-input" value="{{$bootcamper->id}}">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Bootcampers</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="bootcamper-event-table">
                    <thead>
                    <tr>
                        <th>Event</th>
                        <th>Date Registered</th>
                        <th>Accepted</th>
                        <th>Attended</th>
                        <th>Declined</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Visitor" href="#event_modal">
                <i class="large material-icons">add</i>
            </a>
        </div>

        <!-- Modal Structure -->
        <div id="event_modal" class="modal" style="background-color: grey;">
            <div class="row center">
                @foreach($events as $event)
                    <div class="col l6 m6 s12">
                        <div class="card">
                            <h5>{{$event->title}}</h5>
                            <p>Start: {{$event->start->toDateString()}}</p>
                            <p>End: {{$event->end->toDateString()}}</p>
                            <a style="color: orange; cursor: pointer;" class="register-bootcamper" data-value="{{$event->id}}">Register</a>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- PSI Events -->
            <div class="row center">
                @foreach($events_psi as $event_psi)
                    <div class="col l6 m6 s12">
                        <div class="card">
                            <h5>{{$event_psi->title}}</h5>
                            <p>Start: {{$event_psi->start->toDateString()}}</p>
                            <p>End: {{$event_psi->end->toDateString()}}</p>
                            <a style="color: orange; cursor: pointer;" class="register-bootcamper" data-value="{{$event_psi->id}}">Register</a>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- RAP EVENTS-->
            <div class="row center">
                @foreach($events_rap as $event_rap)
                    <div class="col l6 m6 s12">
                        <div class="card">
                            <h5>{{$event_rap->title}}</h5>
                            <p>Start: {{$event_rap->start->toDateString()}}</p>
                            <p>End: {{$event_rap->end->toDateString()}}</p>
                            <a style="color: orange; cursor: pointer;" class="register-bootcamper" data-value="{{$event_rap->id}}">Register</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            let bootcamper_id = $('#bootcamper-id-input').val();
            $(function () {
                $('#bootcamper-event-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/get-bootcamper-events/' + bootcamper_id,
                    columns: [
                        {data: 'event', name: 'event'},
                        {data: 'date_registered', name: 'date_registered'},
                        {data: 'accepted', name: 'accepted'},
                        {data: 'attended', name: 'attended'},
                        {data: 'declined', name: 'declined'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="bootcamper-event-table_length"]').css("display","inline");
            });
        });

        $('.register-bootcamper').on('click', function(){
            let event_id = this.getAttribute('data-value');
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('event_id', event_id);
            formData.append('bootcamper_id', bootcamper_id);

            let url = '/admin-register-bootcamper-event';
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('.register-bootcamper').notify(response.message, "success");
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        function confirm_delete_bootcamper_event(obj){
            var r = confirm("Are you sure want to delete this registration?");
            if (r === true) {
                $.get('/bootcamper-event-delete/' + obj.id, function (data, status) {
                    if (status === 'success') {
                        alert(data.message);
                        window.location.reload();
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }

    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
