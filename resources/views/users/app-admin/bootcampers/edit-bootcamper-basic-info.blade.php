@extends('layouts.admin-layout')
@section('content')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <br/>
    <br>
    <br>
    <input hidden disabled id="bootcamper-id-input" value="{{$bootcamper->id}}">
    {{ Breadcrumbs::render('admin-edit-bootcamper-info',$bootcamper)}}
    <div class="container-fluid">
        <form id="basic-info-form">
            <div class="card" style="margin-left: 4em;margin-right: 4em">
                <input id="user-id-input" value="{{$bootcamper->id}}" hidden disabled>
                <br/>
                <div class="row center">
                    <h4>Personal Information</h4>
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s4 active"><a href="#test1">Bootcamper Details</a></li>
                            <li class="tab col s4"><a href="#test2">Bootcamper Uploads</a></li>
                        </ul>
                    </div>
                    <div class="col l12 m12 s12" id="test1">
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <select id="title-desktop" required>
                                    <option value="{{$bootcamper->user->title}}" selected>{{$bootcamper->user->title}}</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Ms</option>
                                    <option value="Miss">Mr</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr">Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="initials-desktop" type="text" class="validate" value="{{$bootcamper->user->initials}}"
                                       required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="first_name-desktop" type="text" value="{{$bootcamper->user->name}}" class="validate"
                                       required>
                                <label for="first_name-desktop">First Name</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="last_name-desktop" type="text" value="{{$bootcamper->user->surname}}" class="validate"
                                       required>
                                <label for="last_name-desktop">Last Name</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="age-desktop" type="number" value="{{$bootcamper->user->age}}" class="validate" required>
                                <label for="age-desktop">Age</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <select id="gender-desktop" required>
                                    <option value="{{$bootcamper->user->gender}}" selected>{{$bootcamper->user->gender}}</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                                <label for="gender-desktop">Gender</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="ID-desktop" type="text" value="{{$bootcamper->user->id_number}}" class="validate"
                                       required>
                                <label for="ID-desktop">ID Number</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="data_cellnumber-desktop" type="text" value="{{$bootcamper->user->data_cellnumber}}" class="validate"
                                       required>
                                <label for="data_cellnumber-desktop">Data cell number</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <select id="service_provider_network-desktop" required>
                                    <option value="{{$bootcamper->user->service_provider_network}}" selected>{{$bootcamper->user->service_provider_network}}</option>
                                    <option value="MTN">MTN</option>
                                    <option value="Vodacom">Vodacom</option>
                                    <option value="Cell C">Cell C</option>
                                    <option value="Telkom">Telkom</option>
                                </select>
                                <label for="service_provider_network-desktop">Service provider</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="email-desktop" type="email"  value="{{$bootcamper->user->email}}"
                                       class="validate" required>
                                <label for="email-desktop">Email address</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="cell_number-desktop" type="text" value="{{$bootcamper->user->contact_number}}"
                                       class="validate" required>
                                <label for="cell_number-desktop">Cell Number</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="address-desktop" type="text" value="{{$bootcamper->user->address_one}}" class="validate"
                                       required>
                                <label for="address-desktop">Physical Address 1</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="address_two-desktop" type="text" value="{{$bootcamper->user->address_two}}"
                                       class="validate" required>
                                <label for="address_two-desktop">Physical Address 2</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="address_three-desktop" type="text" value="{{$bootcamper->user->address_three}}"
                                       class="validate" required>
                                <label for="address_three-desktop">Physical Address 3</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="city-desktop" type="text" value="{{$bootcamper->user->city}}" class="validate" required>
                                <label for="city-desktop">City</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="code-desktop" type="text" value="{{$bootcamper->user->code}}" class="validate" required>
                                <label for="code-desktop">Postal Code</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;" id="empty-div" >
                            <div class="input-field col l6 m6 s12">
                                <select id="race-desktop" required>
                                    <option value="{{$bootcamper->user->race}}" selected>{{$bootcamper->user->race}}</option>
                                    <option value="Black">African</option>
                                    <option value="White">White</option>
                                    <option value="Indian">Indian</option>
                                    <option value="Coloured">Coloured</option>
                                    <option value="Other">Other</option>
                                </select>
                                <label for="race-desktop">Race</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 80%">
                            <input class="btn waves-effect waves-light" type="submit">
                        </div>
                        <br/>
                        <br/>
                    </div>

                    <div class="row center" id="test2">
                        <div class="">
                            <h4 style="color: black">Documents</h4>
                        </div>
                        <div class="col l4 m4 s12">
                            <h5>Pitch Video:</h5>
                            @if(isset($bootcamper->pitch_video_link))
                                <input type="text" id="bootcamper-video-link-input-display" value="{{$bootcamper->pitch_video_link}}">
                                <p>Date Uploaded: {{$bootcamper->pitch_video_date_time}}</p>
                                <button class="btn blue" id="update-pitch-video">Update</button>
                            @else
                                <input type="text" id="bootcamper-video-link-input" placeholder="Upload Pitch Video">
                                <button class="btn blue" id="submit-pitch-video">Submit</button>
                            @endif
                        </div>

                        <!--Second Video-->
                        <div class="col l4 m4 s12">
                            <h5>Second Pitch Video:</h5>
                            @if(isset($bootcamper->pitch_video_link_two))
                                <input type="text" id="bootcamper-second-video-link-input-display" value="{{$bootcamper->pitch_video_link_two}}">
                                <p>Date Uploaded: {{$bootcamper->pitch_video_date_time}}</p>
                                <button class="btn blue" id="update-second-pitch-video">Update</button>
                            @else
                                <input type="text" id="bootcamper-second-video-link-input" placeholder="Upload Second Pitch Video">
                                <button class="btn blue" id="submit-second-pitch-video">Submit</button>
                            @endif
                        </div>

                        <div class="col l4 m4 s12">
                            <h5>ID Document:</h5>
                            @if(isset($bootcamper->id_document_url))
                                <input type="file" id="bootcamper-id-document-input-display" value="{{$bootcamper->id_document_url}}">
                                <p style="color: red;">Only upload another ID document if you wish to update the one you uploaded previously.</p>
                                <button class="btn blue" id="update-id-document">Update</button>
                            @else
                                <input class="file-path validate" id="bootcamper-id-document-input"
                                       type="file" placeholder="Upload ID Document">
                                <button class="btn blue" id="submit-id-document">Submit</button>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();

            <!--Submit first video-->
            $('#submit-pitch-video').on('click', function(){
                let bootcamper_id = $('#bootcamper-id-input').val();

                let formData = new FormData();
                formData.append('pitch_video', $('#bootcamper-video-link-input').val());

                let url = '/bootcamper-upload-pitch-video/' + bootcamper_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#submit-pitch-video').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            <!--Submit second video-->
            $('#submit-second-pitch-video').on('click', function(){
                let bootcamper_id = $('#bootcamper-id-input').val();

                let formData = new FormData();
                formData.append('second_pitch_video', $('#bootcamper-second-video-link-input').val());

                let url = '/bootcamper-upload-second-pitch-video/' + bootcamper_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#submit-second-pitch-video').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            $("#update-pitch-video").on('click', function(){
                let bootcamper_id = $('#bootcamper-id-input').val();

                let formData = new FormData();
                formData.append('pitch_video', $('#bootcamper-video-link-input-display').val());

                let url = '/bootcamper-upload-pitch-video/' + bootcamper_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#update-pitch-video').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            <!--Update second video-->
            $("#update-second-pitch-video").on('click', function(){
                let bootcamper_id = $('#bootcamper-id-input').val();

                let formData = new FormData();
                formData.append('second_pitch_video', $('#bootcamper-second-video-link-input-display').val());

                let url = '/bootcamper-upload-second-pitch-video/' + bootcamper_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#update-second-pitch-video').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            <!--Submit id document-->
            $('#submit-id-document').on('click', function(){
                let bootcamper_id = $('#bootcamper-id-input').val();

                let formData = new FormData();

                jQuery.each(jQuery('#bootcamper-id-document-input')[0].files, function (i, file) {
                    formData.append('id_document', file);
                });

                let url = '/bootcamper-upload-id-document/' + bootcamper_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#submit-id-document').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            <!--Update id document-->
            $('#update-id-document').on('click', function(){
                let bootcamper_id = $('#bootcamper-id-input').val();

                let formData = new FormData();

                jQuery.each(jQuery('#bootcamper-id-document-input-display')[0].files, function (i, file) {
                    formData.append('id_document', file);
                });

                let url = '/bootcamper-upload-id-document/' + bootcamper_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#update-id-document').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });


            $('#basic-info-form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();
                let user_id = $("#user-id-input").val();

                formData.append('name', $('#first_name-desktop').val());
                formData.append('surname', $('#last_name-desktop').val());
                formData.append('id_number', $('#ID-desktop').val());
                formData.append('contact_number', $('#cell_number-desktop').val());
                formData.append('address_one', $('#address-desktop').val());
                formData.append('title', $('#title-desktop').val());
                formData.append('initials', $('#initials-desktop').val());
                formData.append('venture_name', $('#venture_name-desktop').val());
                formData.append('address_two', $('#address_two-desktop').val());
                formData.append('address_three', $('#address_three-desktop').val());
                formData.append('city', $('#city-desktop').val());
                formData.append('code', $('#code-desktop').val());
                formData.append('role_id', $('#role-desktop').val());
                formData.append('age', $('#age-desktop').val());
                formData.append('gender', $('#gender-desktop').val());
                formData.append('race', $('#race-desktop').val());
                formData.append('service_provider_network', $('#service_provider_network-desktop').val());
                formData.append('data_cellnumber', $('#data_cellnumber-desktop').val());


                $.ajax({
                    url: "/bootcamper-update-basic/" + user_id,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#empty-div').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        $('#empty-div').notify(response.message, "error");

                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
    @endsection
