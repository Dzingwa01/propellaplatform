@extends('layouts.admin-layout')
@section('content')
    <br>
    <br>
    <input hidden disabled id="event-id-input" value="{{$event->id}}">
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">{{$event->title}} Visitors</h6>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">{{$event->start->toDateString()}}</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="private-event-incubatee-table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Contact Number</th>
                        <th>Date Registered</th>
                        <th>Attendance</th>
                        <th>Accepted</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            let event_id = $('#event-id-input').val();
            $(function () {
                $('#private-event-incubatee-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/get-private-event-visitors/' + event_id,
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'contact_number', 'name': 'contact_number'},
                        {data: 'date_registered', name: 'date_registered'},
                        {data: 'attendance', name: 'attendance'},
                        {data: 'accepted', name: 'accepted'}
                    ]
                });
                $('select[name="private-event-incubatee-table_length"]').css("display","inline");
            });
        });
    </script>
@endsection
