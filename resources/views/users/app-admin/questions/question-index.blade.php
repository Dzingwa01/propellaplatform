@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('admin-questions')}}
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Questions</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="questions-table">
                    <thead>
                    <tr>
                        <th>Question Number</th>
                        <th>Question text</th>
                        <th>Category name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Upload" href="{{url('upload-questions')}}">
            <i class="large material-icons">add</i>
        </a>
    </div>

    <script>

        $(document).ready(function () {
            $('select').formSelect();
            $(function () {
                $('#questions-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    stateSave: true,
                    pageLength: 100,
                    ajax: '{{route('get-questions')}}',
                    columns: [
                        {data: 'question_number', name: 'question_number'},
                        {data: 'question_text', name: 'question_text'},
                        {data: 'category_name', name: 'category_name'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="questions-table_length"]').css("display","inline");
            });

        });
        function confirm_delete(obj){
            var r = confirm("Are you sure want to delete this question?");
            if (r == true) {
                $.get('/question/delete/'+obj.id,function(data,status){
                    console.log('Data',data);
                    console.log('Status',status);
                    if(status=='success'){
                        alert(data.message);
                        window.location.reload();
                    }

                });
            } else {
                alert('Delete action cancelled');
            }

        }
    </script>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
    @endsection
