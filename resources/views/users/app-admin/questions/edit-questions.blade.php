@extends('layouts.admin-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <br>
    <br>
    <input id="user-id-input" disabled hidden value="{{$Question->id}}">
    {{ Breadcrumbs::render('admin-edit-questions',$Question)}}
    <div class="row" style="width: 850px;padding-left:9%;margin-top: 10vh">
        <div class="card white" style="padding-left:5%;">
                <input value="{{$Question->id}}" id="question_id" hidden>
                <h4>Edit questions</h4>
                <br>
                <div class="input-field" style="width:90%">
                    <select id="category-id">
                        @foreach($questionCategories as $category)
                            <option value="{{$category->id}}" {{$category->id==$category->display_name?'selected':''}}>{{$category->category_name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="input-field">
                    <input style="width:90%" id="question_number" type="number" value="{{$Question->question_number}}">
                    <label for="question_number">Question Number</label>
                </div>

                <div class="input-field">
                    <input style="width:90%" id="question_text" type="text" value="{{$Question->question_text}}" class="validate">
                    <label for="question_text">Question text</label>
                </div>
                <div class="input-field" style="width:90%">
                    <select id="question_type">
                        <option value="" disabled selected>Choose question type - only for Evaluation forms</option>
                        <option value="text">Text</option>
                    </select>
                    <label>Question Type</label>
                </div>

                {{--<div class="input-field subText" id="show-add-sub">--}}
                        {{--<input style="width:90%" id="question_sub_text" type="text" value="{{$question_text->question_sub_text}}" class="validate">--}}
                        {{--<label for="question_sub_text">Question sub text</label>--}}
                {{--</div>--}}

                <!--Sub text-->
                @foreach($Question->questionSubTexts as $question_text)
                    <div class="input-field subText" id="show-add-sub" >
                        <input style="width:90%" id="question_sub_text" type="text" value="{{$question_text->question_sub_text}}">
                        <label for="question_sub_text">Question sub text</label>
                    </div>
                @endforeach


                <div class="row" style="margin-left: 500px;">
                    <div class="col s4">
                        <a class="waves-effect waves-light btn" id="question-submit-button">Save</a>
                    </div>
                </div>
                <br/>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('select').formSelect();

            $('#question-submit-button').on('click',function () {
                let formData = new FormData();
                formData.append('question_text', $('#question_text').val());
                formData.append('question_number', $('#question_number').val());
                formData.append('question_category_id', $('#category-id').val());
                formData.append('question_sub_text', $('#question_sub_text').val());
                formData.append('question_type', $('#question_type').val());

                console.log(formData);

                let url = '/updateQuestion/'+ '{{$Question->id}}';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection

