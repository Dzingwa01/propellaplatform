@extends('layouts.admin-layout')
@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <br>
    <br>
    {{ Breadcrumbs::render('admin-add-questions')}}
    <div class="row" style="width: 850px;padding-left:9%;">
        <div class="card white" style="padding-left:5%;">
            <br/>
            <h4>Create questions</h4>
            <div class="input-field" style="width:90%">
                <select id="category-id">
                    <option value="" disabled selected>Chose the question category</option>
                    @foreach($questionCategories as $category)
                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="input-field">
                <input style="width:90%" id="question_number" type="number">
                <label for="question_number">Question Number</label>
            </div>
            <div class="input-field">
                <input style="width:90%" id="question_text" type="text" class="validate">
                <label for="question_text">Question text</label>
            </div>

            <div class="input-field" style="width:90%">
                <select id="question_type">
                    <option value="" disabled selected>Choose question type - only for Evaluation forms</option>
                    <option value="text">Text</option>
                </select>
                <label>Question Type</label>
            </div>
            <!--Sub text-->
            <div class="row" style="margin-left: 200px;">
                <div class="col s4" id="add-sub">
                    <a class="waves-effect waves-light btn">Add sub text</a>
                </div>
            </div>
            <div class="input-field subText" id="show-add-sub" hidden>
                <input style="width:90%" id="question_sub_text" type="text" class="validate">
                <label for="question_sub_text">Question sub text</label>
            </div>
            <div class="row" id="add-to-list" hidden>
                <button class="btn blue" id="add-sub-text-button">Add</button>
            </div>


            <div class="row" style="margin-left: 500px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn" id="question-submit-button">Save</a>
                </div>
            </div>
            <br/>
        </div>
    </div>

    <script>

        //Add sub text
        let sub_text_array = [];

        $('#add-sub-text-button').on('click', function(){
            let question_sub_text = $('#question_sub_text').val();

            let object = {
                question_sub_text: question_sub_text,
            };

            sub_text_array.push(object);

            $('#question_sub_text').val("");

        });


        $(document).ready(function() {
            $('#add-sub').on('click', function () {
                $('#show-add-sub').show();
                $('#add-to-list').show();
            });
            $('select').formSelect();

            $('#question-submit-button').on('click',function () {

                let formData = new FormData();
                formData.append('question_text', $('#question_text').val());
                formData.append('question_number', $('#question_number').val());
                formData.append('sub_text_array', JSON.stringify(sub_text_array));
                formData.append('question_type', $('#question_type').val());
                formData.append('question_category_id', $('#category-id').val());

                $.ajax({
                    url: '{{route('store-questions')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });

    </script>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection

