@extends('layouts.admin-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <br>
    <br>
    <input id="user-id-input" disabled hidden value="{{$QuestionsCategory->id}}">
    {{ Breadcrumbs::render('admin-edit-category',$QuestionsCategory)}}

    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <form class="col s12 " id="edit-category" method="post">
            <br>
            @csrf
            <input value="{{$QuestionsCategory->id}}" id="question_category_id" hidden>

            <h6 style="margin-left: 15em">Edit Category</h6>
            <div class="row" style="margin-left: 7em">
                <div class="input-field col s10">
                    <input id="category_name" type="text" value="{{$QuestionsCategory->category_name}}" class="validate">
                    <label for="category_name">Category name</label>
                </div>
            </div>
            <div class="row" style="margin-left: 7em">
                <div class="input-field col s10">
                    <input id="category_description" type="text" value="{{$QuestionsCategory->category_description}}" class="validate">
                    <label for="category_description">Category description</label>
                </div>
            </div>
            <div class="row" style="margin-left: 100px">
                <div class="file-field input-field col s10">
                    <div class="btn">
                        <span>Category Image</span>
                        <input id="category_image_url" type="file" name="category_image_url">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path" value="{{isset($QuestionsCategory->category_image_url)?$QuestionsCategory->category_image_url:''}}" type="text">
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 600px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn" id="edit-question-category-submit-button">Save</a>
                </div>
            </div>
            <br>
            <br>
        </form>
    </div>

    <script>
        $(document).ready(function () {
            $('#edit-question-category-submit-button').on('click',function () {
                let formData = new FormData();

                formData.append('question_category_id', $('#question_category_id').val());
                formData.append('category_name', $('#category_name').val());
                formData.append('category_description', $('#category_description').val());
                jQuery.each(jQuery('#category_image_url')[0].files, function (i, file) {
                    formData.append('category_image_url', file);
                });
                console.log(formData);

                let url = '/update-application-category/'+ '{{$QuestionsCategory->id}}';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });

            });
        });

    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
    @endsection
