@extends('layouts.admin-layout')
@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('admin-enquiry-followup')}}
    <div class="section" style="margin-top: 2em;">
        <input id="enquiry-id-input" hidden disabled value="{{$enquiry->id}}">
        <div class="row">
            <div class="row center">
                <h4>Enquiry Follow Up</h4>
            </div>

            <div class="row center" style="width: 50%">
                <label for="assigned-to-list-input">Assign Enquiry</label>
                <select id="assigned-to-list-input" multiple>
                    <option value="" disabled selected>Assign To</option>
                    <option value="grant@propellaincubator.co.za" >Grant Minnie</option>
                    <option value="woosthuizen@engeli.co.za">Wayne Oosthuizen</option>
                    <option value="neohmabunda@gmail.com">Neo Mabunda</option>
                    <option value="zainow1@hotmail.com">Zain Imran</option>
                </select>
                <a class="btn-flat" id="assign-enquiry-button">Submit</a>
            </div>
        </div>
    </div>


    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>

    <script>
        $(document).ready(function(){
            $('select').formSelect();
        });

        $('#assign-enquiry-button').on('click', function(){
            $(this).text("Please wait");
            let assigned_list = $('#assigned-to-list-input').val();
            let enquiry_id = $('#enquiry-id-input').val();
            let final_list = [];

            for(let i = 0; i < assigned_list.length; i++){
                final_list.push(assigned_list[i]);
            }

            let formData = new FormData();
            formData.append('assigned_list', final_list);

            let url = '/admin-store-pre-assigned-enquiry/' + enquiry_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success: function (response, a, b) {
                    $("#assign-enquiry-button").notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/unassigned-enquiry-index';
                    }, 3000);
                },
                error: function (response) {
                    let message = response.message;
                    alert(message);
                }
            });
        });
    </script>
@endsection
