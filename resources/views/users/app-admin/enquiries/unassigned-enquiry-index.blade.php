@extends('layouts.admin-layout')
@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('admin-unassigned-enquiries')}}
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Unassigned Enquiries</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="enquiries-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Enquiry Category</th>
                        <th>Date Enquired</th>
                        <th>Enquiry</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(function () {
                $('#enquiries-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-unassigned-enquiries')}}',
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'contact_number', name: 'contact_number'},
                        {data: 'category', name: 'category'},
                        {data: 'date_enquired', name: 'date_enquired'},
                        {data: 'enquiry_message', name: 'enquiry_message'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="enquiries-table_length"]').css("display","inline");
            });
        });
    </script>
@endsection
