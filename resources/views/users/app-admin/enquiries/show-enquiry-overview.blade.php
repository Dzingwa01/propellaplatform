@extends('layouts.admin-layout')
@section('content')

    <br>
    <br>
    {{ Breadcrumbs::render('admin-enquiry-overview')}}
    <div class="section" style="margin-top: 2em;">
        <div class="row">
            <div class="row center">
                <h4>Enquiry Overview</h4>
            </div>
            <div class="row" style="margin-left: 8em; margin-right: 8em;">
                <div class="col l6 m6 s12">
                    <div class="card hoverable">
                        <div class="card-content">
                            <span>
                                <b>Title:</b> {{$enquiry->title}}
                                <br>
                                <b>Name:</b> {{$enquiry->name}}
                                <br>
                                <b>Surname:</b> {{$enquiry->surname}}
                                <br>
                                <b>Email:</b> {{$enquiry->email}}
                                <br>
                                <b>Contact Number:</b> {{$enquiry->contact_number}}
                                <br>
                                <b>Company:</b> {{$enquiry->company}}
                                <br>
                                <b>City:</b> {{$enquiry->city}}
                                <br>
                                <b>Heard About Us:</b> {{$enquiry->heard_about_us}}
                                <br>
                                <b>Message:</b> {{$enquiry->enquiry_message}}
                                <br>
                            </span>
                        </div>
                    </div>

                </div>
                <div class="col l6 m6 s12">
                    <div class="card hoverable">
                        <div class="card-content">
                            @if($enquiry->is_assigned == true)
                                <span>
                                    <b>Initial Contact Person:</b> {{$enquiry->initial_contact_person}}
                                    <br>
                                    <b>Initial Contact Method:</b> {{$enquiry->initial_contact_method}}
                                    <br>
                                    <b>Initial Report:</b> {{$enquiry->initial_report}}
                                    <br>
                                    <b>Next Step:</b> {{$enquiry->enquiry_next_step}}
                                    <br>
                                    <b>Date Assigned:</b> {{$enquiry->date_assigned}}
                                </span>
                            @else
                                <h6>This enquiry has not been attended to yet.</h6>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>




    <script>
        $(document).ready(function(){
            $('.collapsible').collapsible();
        });
    </script>
@endsection
