@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>

    <div class="container">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;" id="page-header">Registered Events</h6>
        </div>
        <input hidden disabled id="visitor-id-input" value="{{$visitor->id}}">
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="visitor-events-table">
                    <thead>
                    <tr>
                        <th>Event Name</th>
                        <th>De-register Reason</th>
                        <th>Date / Time De-registered</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $('select').formSelect();
            let visitor_id = $('#visitor-id-input').val();

            $(function () {
                $('#visitor-events-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/get-visitor-de-registered-events-via-edit/' + visitor_id,
                    columns: [
                        {data: 'event_name', name: 'event_name'},
                        {data: 'de_register_reason', name: 'de_register_reason'},
                        {data: 'date_time_de_register', name: 'date_time_de_register'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="visitor-events-table_length"]').css("display","inline");
            });
        });

        function deleteConfirm(obj) {
            let r = confirm("Are you sure want to delete this record?");
            if (r === true) {
                $.get('/visitor-deregistered-event-delete/' + obj.id, function (data, status) {
                    if (status === 'success') {
                        $('#page-header').notify(data.message, "success");

                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    }
                });
            } else {
                $('#page-header').notify("Delete action cancelled!", "info");
            }
        }
    </script>
@endsection
