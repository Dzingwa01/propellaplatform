@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>

    <div class="container">
            <div class="row">
                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Registered Events</h6>
            </div>
            <input hidden disabled id="visitor-id-input" value="{{$visitor->id}}">
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="col s12">
                    <table class="table table-bordered" style="width: 100%!important;" id="visitor-events-table">
                        <thead>
                        <tr>
                            <th>Event</th>
                            <th>Date Registered</th>
                            <th>Attended</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
           data-tooltip="Register Event" href="#add-event-to-visitor-modal">
            <i class="large material-icons">add</i>
        </a>
    </div>

    <div id="add-event-to-visitor-modal" class="modal">
        <div class="modal-content">
            <div class="row">
                <div class="col l6 m6">
                    <label for="event-input">Events</label>
                    <select id="event-input">
                        <option value="" disabled selected>Choose an event</option>
                        @foreach($events as $event)
                            <option value="{{$event->id}}">{{$event->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col l6 m6">
                    <label for="attended-input">Attended</label>
                    <select id="attended-input">
                        <option selected disabled>Did he/she attend?</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>
                </div>
            </div>
            <div class="row right">
                <a class="waves-effect waves-light btn blue" id="admin-register-visitor-submit-button">Submit</a>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $('select').formSelect();
            let visitor_id = $('#visitor-id-input').val();

            $(function () {
                $('#visitor-events-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/get-visitor-events-via-edit/' + visitor_id,
                    columns: [
                        {data: 'event', name: 'event'},
                        {data: 'date_registered', name: 'date_registered'},
                        {data: 'attended', name: 'attended'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="visitor-events-table_length"]').css("display","inline");
            });
        });

        $("#admin-register-visitor-submit-button").on('click', function(){
           let visitor_id = $('#visitor-id-input').val();
           let event_id = $('#event-input').val();
           let attended = $('#attended-input').val();

           if(event_id === null || attended === null){
                alert('Please fill in all inputs!');
           } else {
               let formData = new FormData();
               formData.append('visitor_id', visitor_id);
               formData.append('event_id', event_id);
               formData.append('attended', attended);

               let url = '/admin-register-visitor-event';
               $.ajax({
                   url: url,
                   processData: false,
                   contentType: false,
                   data: formData,
                   type: 'post',
                   headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                   success: function (response, a, b) {
                       $("#admin-register-visitor-submit-button").notify(response.message, "success");

                       setTimeout(function(){
                           window.location.reload();
                       }, 3000);
                   },
                   error: function (response) {
                       console.log("error", response);
                       let message = response.responseJSON.message;
                       $("#admin-register-visitor-submit-button").notify(message, "success");
                   }
               });
           }
        });
    </script>
@endsection
