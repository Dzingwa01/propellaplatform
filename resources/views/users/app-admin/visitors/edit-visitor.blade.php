@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>

    <input id="user-id-input" disabled hidden value="{{$visitor->id}}">
    {{ Breadcrumbs::render('admin-visitors-edit',$visitor)}}

    <div class="container">
        <div class="card" style="width: 800px;margin: 0 auto;">
            <div class="row center">
                <h5>Click a button to download the data</h5>
                <div class="col s3 m3 l3">
                    <a class="btn btn-success purple"
                       onclick="exportVisitorDetails()"
                    >Details
                    </a>
                </div>
                <div class="col s3 m3 l3">
                    <a class="btn btn-success purple"
                       onclick="exportVisitorSummary()"
                    >Summary
                    </a>
                </div>
                <div class="col s3 m3 l3">
                    <a class="btn btn-success purple"
                       onclick="exportVisitorEvents()"
                    >Events
                    </a>
                </div>
                <div class="col s3 m3 l3">
                    <a class="btn btn-success purple"
                       onclick="exportVisitorDeregisteredEvents()"
                    >De-Registered Events
                    </a>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <input hidden disabled value="{{$visitor->id}}" id="visitor-id-input">
                <div class="input-field col m6">
                    <select id="title">
                        @if(isset($visitor->title))
                            <option value="{{$visitor->title}}" selected>{{$visitor->title}}</option>
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Miss">Miss</option>
                            <option value="Ms">Ms</option>
                            <option value="Dr">Dr</option>
                        @else
                            <option value="" selected disabled="">Choose Title</option>
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Miss">Miss</option>
                            <option value="Ms">Ms</option>
                            <option value="Dr">Dr</option>
                        @endif
                    </select>
                </div>
                <div class="input-field col s6">
                    <input id="first_name" value="{{$visitor->first_name}}" type="text" class="validate">
                    <label for="first_name">First Name</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="last_name" value="{{$visitor->last_name}}" type="text" class="validate">
                    <label for="last_name">Last Name</label>
                </div>
                <div class="input-field col s6">
                    <input id="company" type="text" value="{{$visitor->company}}" class="validate">
                    <label for="company">Your Company Name</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="designation" type="text" value="{{$visitor->designation}}" class="validate">
                    <label for="designation">Designation</label>
                </div>
                <div class="input-field col s6">
                    <input id="email" type="text" value="{{$visitor->email}}" class="validate">
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em; margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="cell_number" type="text" value="{{$visitor->cell_number}}" class="validate">
                    <label for="cell_number">Cell number</label>
                </div>
                <div class="input-field col s6">
                    <input id="land_line" type="text" value="{{$visitor->land_line}}" class="validate">
                    <label for="land_line">Land line</label>
                </div>

            </div>
            <div class="row" style="margin-left: 2em; margin-right: 2em;">
                <div class="input-field col s6">
                    <input id="dob" type="date" value="{{$visitor->dob}}" class="validate">
                    <label for="dob">Date of Birth</label>
                </div>
            </div>

            <div class="row" style="margin-left: 550px;">
                <a class="waves-effect waves-light btn" id="form-submit-button">Submit</a>
            </div>
            <br/>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12">
                    <div class="row">
                        <div class="col l6 card hoverable" id="visitor-registered-events-card" style="background: #1a237e">
                            <br>
                            <h5 style="color: white">Registered Events</h5>
                            <br>
                        </div>
                        <div class="col l6 card hoverable" id="visitor-de-registered-events-card" style="background: #1B5E20">
                            <br>
                            <h5 style="color: white">De-registered Events</h5>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="center">
                <div class="col l12 card hoverable" id="visitor-summary-card" style="background: dimgray;">
                            <br>
                            <h5 style="color: white">Visitor Summary</h5>
                            <br>
                </div>
            </div>
        </div>
    </div>

    <script>
        let visitor_id = $('#visitor-id-input').val();

        $(document).ready(function(){
            $('select').formSelect();
        });

        $('#form-submit-button').on('click', function () {
            let visitor_id = $('#visitor-id-input').val();
            let formData = new FormData();
            formData.append('title', $('#title').val());
            formData.append('first_name', $('#first_name').val());
            formData.append('last_name', $('#last_name').val());
            formData.append('company', $('#company').val());
            formData.append('designation', $('#designation').val());
            formData.append('email', $('#email').val());
            formData.append('cell_number', $('#cell_number').val());
            formData.append('dob', $('#dob').val());

            let url = '/admin-update-visitor-details/' + visitor_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#form-submit-button').notify(response.message, "success");
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    $('#form-submit-button').notify(message, "error");
                }
            });
        });

        $('#visitor-registered-events-card').on('click', function(){
            window.location.href = '/visitor-registered-event-index/' + visitor_id;
        });

        $('#visitor-de-registered-events-card').on('click', function(){
            window.location.href = '/visitor-de-registered-event-index/' + visitor_id;
        });

        $('#visitor-summary-card').on('click', function(){
            window.location.href = '/visitor-visit-summary-index/' + visitor_id;
        });

        function exportVisitorDetails(){
            let visitor_id = $('#visitor-id-input').val();
            window.location.href = '/export-visitor-details/' + visitor_id;
        }

        function exportVisitorSummary(){
            let visitor_id = $('#visitor-id-input').val();
            window.location.href = '/export-visitor-summary/' + visitor_id;
        }

        function exportVisitorEvents(){
            let visitor_id = $('#visitor-id-input').val();
            window.location.href = '/export-visitor-events/' + visitor_id;
        }

        function exportVisitorDeregisteredEvents(){
            let visitor_id = $('#visitor-id-input').val();
            window.location.href = '/export-visitor-deregistered-events/' + visitor_id;
        }
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
