@extends('layouts.admin-layout')

@section('content')

    <br>
    <br>

    <div class="container-fluid">
        <div class="row">
            <div class="col l6 m6 card">
                <div class="row center">
                    <h5><b>Event Details:</b></h5>
                    <div class="col l6 m6">
                        <p>{{$eventVisitor->event->title}}</p>
                    </div>
                    <div class="col l6 m6">
                        <p>{{$eventVisitor->event->start->toDateString()}}</p>
                    </div>
                </div>
            </div>
            <input hidden disabled id="event-id-input" value="{{$eventVisitor->event->id}}">
            <input hidden disabled id="visitor-id-input" value="{{$eventVisitor->visitor->id}}">
            <input hidden disabled id="visitor-event-id-input" value="{{$eventVisitor->id}}">
            <div class="col l6 m6 card">
                <div class="row center">
                    <h5><b>Visitor Details</b></h5>
                    <div class="col l6 m6">
                        <p>{{$eventVisitor->visitor->first_name}}</p>
                    </div>
                    <div class="col l6 m6">
                        <p>{{$eventVisitor->visitor->last_name}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col l6 m6">
                <label for="registered-input">Registered</label>
                <select id="registered-input">
                    @if($eventVisitor->registered == true)
                        <option value="Yes" selected>Yes</option>
                        <option value="No">No</option>
                    @else
                        <option value="Yes">Yes</option>
                        <option value="No" selected>No</option>
                    @endif
                </select>
            </div>
            <div class="col l6 m6">
                <label for="attended-input">Attended</label>
                <select id="attended-input">
                    @if($eventVisitor->attended == true)
                        <option value="Yes" selected>Yes</option>
                        <option value="No">No</option>
                    @else
                        <option value="Yes">Yes</option>
                        <option value="No" selected>No</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="container" hidden id="deregister-container">
            <div class="row">
                <div class="col l6 m6">
                    <label for="date-time-de-registered-input">Date / Time De-registered</label>
                    <input id="date-time-de-registered-input" type="date">
                </div>
                <div class="col l6 m6">
                    <label for="de-registered-reason-input">De-registered Reason</label>
                    <textarea id="de-registered-reason-input" ></textarea>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col l12 m12">
                    <label for="date-time-registered-input">Date / Time Registered</label>
                    <input id="date-time-registered-input" value="{{$eventVisitor->date_time_registered}}" disabled>
                </div>
            </div>

        <div class="row right">
            <button class="btn blue" id="submit-visitor-event-update-button">Update</button>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('select').formSelect();

            $('#registered-input').on('change', function(){
               if($('#registered-input').val() == 'No'){
                   $('#deregister-container').show();
               } else {
                   $('#deregister-container').hide();
                   $('#de-registered-input').val("");
                    $('#de-registered-reason-input').val("");
                    $('#date-time-de-registered-input').val("");
               }
            });
        });

        $('#submit-visitor-event-update-button').on('click', function(){
           let formData = new FormData();
           let visitor_event_id = $('#visitor-event-id-input').val();
           let visitor_id = $('#visitor-id-input').val();
           let url = '/admin-update-visitor-registered-event/' + visitor_event_id;

           if($('#registered-input').val() == 'Yes'){
               formData.append('registered', $('#registered-input').val());
               formData.append('attended', $('#attended-input').val());
               formData.append('event_id', $('#event-id-input').val());
               formData.append('visitor_id', $('#visitor-id-input').val());

               $.ajax({
                   url: url,
                   processData: false,
                   contentType: false,
                   data: formData,
                   type: 'post',
                   headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                   success: function (response, a, b) {
                       $('#submit-visitor-event-update-button').notify(response.message, "success");

                       setTimeout(function(){
                           window.location.reload();
                       }, 3000);
                   },

                   error: function (response) {
                       console.log("error", response);
                       let message = response.responseJSON.message;
                       $('#submit-visitor-event-update-button').notify(message, "error");
                   }
               });
           } else if ($('#registered-input').val() == 'No'){
               let r = confirm("Are you sure want to de-register this visitor?");
               if (r == true) {
                   formData.append('registered', $('#registered-input').val());
                   formData.append('de_registered-reason', $('#de-registered-reason-input').val());
                   formData.append('de-registered_date_time', $('#date-time-de-registered-input').val());
                   formData.append('event_id', $('#event-id-input').val());
                   formData.append('visitor_id', $('#visitor-id-input').val());

                   $.ajax({
                       url: url,
                       processData: false,
                       contentType: false,
                       data: formData,
                       type: 'post',
                       headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                       success: function (response, a, b) {
                           $('#submit-visitor-event-update-button').notify(response.message, "success");

                           setTimeout(function(){
                               window.location.href = '/visitor-registered-event-index/' + visitor_id;
                           }, 3000);
                       },

                       error: function (response) {
                           console.log("error", response);
                           let message = response.responseJSON.message;
                           $('#submit-visitor-event-update-button').notify(message, "error");
                       }
                   });
               } else {
                   $('#submit-visitor-event-update-button').notify('De-registration cancelled', "info");
               }
           }
        });
    </script>

@endsection
