@extends('layouts.admin-layout')
@section('content')

    <br>
    <br>

    <input hidden disabled id="visitor-id-input" value="{{$visitor->id}}">

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white" id="page-header">Visits / Appointments / Events Summary</h5>
                </div>
            </div>
        </div>

        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="visitors-summary-table">
                    <thead>
                    <tr>
                        <th>Date / Time</th>
                        <th>Reason</th>
                        <th>Details</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {

            let visitor_id = $('#visitor-id-input').val();

            $(function () {
                $('#visitors-summary-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/admin-get-visitor-summary/' + visitor_id,
                    columns: [
                        {data: 'date_in', name: 'date_in'},
                        {data: 'visit_category', name: 'visit_category'},
                        {data: 'visit_reason', name: 'visit_reason'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                $('select[name="visitors-summary-table_length"]').css("display","inline");
            });
        });

        function deleteConfirm(obj) {
            let r = confirm("Are you sure want to delete this record?");
            if (r === true) {
                $.get('/admin-delete-visitor-backup/' + obj.id, function (data, status) {
                    if (status === 'success') {
                        $('#page-header').notify(data.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    }
                });
            } else {
                $('#page-header').notify('Delete action cancelled', "info");
            }
        }

    </script>
@endsection
