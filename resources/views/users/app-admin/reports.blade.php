@extends('layouts.administrator-layout')
@section('content')
<br>
<br>{{ Breadcrumbs::render('admin-app-report')}}
    <div class="row">
        <div class="input-field col m2" style="margin-top: 15vh">
            <select id="roles">
                <option value="" disabled selected>List of Users</option>
                @foreach($roles as $role)
                    <option value="{{$role->display_name}}">{{$role->display_name}}</option>
                @endforeach
            </select>
            <label>Our users</label>
        </div>
        <div class="input-field col m2" id="incubateeCategoryDiv" hidden style="margin-top: 15vh" h>
            <select id="incubateecategory">
                <option value="" disabled selected>Choose Categories</option>
                <option value="ICT" id="ICT">ICT</option>
                <option value="INDUSTRIAL">INDUSTRIAL</option>
                <option value="ALUMNI">ALUMNI</option>
            </select>
            <label>Incubatee Categories</label>
        </div>
        <div class="input-field col m2" id="incubateeDetailsDiv" hidden style="margin-top: 15vh" h>
            <select id="incubateeDetails">
                <option value="" disabled selected>Choose</option>
                <option value="startup_name">Start up name</option>
                <option value="hub">Hub</option>
                <option value="stage">Stage</option>
            </select>
            <label>Details</label>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('select').formSelect();

            $("#roles").change(checkSelect);

            $('#incubateecategory').change( function () {
                if($('#incubateecategory').val() === 'ICT'){
                    $('#incubateeDetailsDiv').show();
                } else if($('#incubateecategory').val() === 'INDUSTRIAL'){
                    $('#incubateeDetailsDiv').show();
                } else if($('#incubateecategory').val() === 'ALUMNI'){
                    $('#incubateeDetailsDiv').show();
                }
            });
        });

        function checkSelect() {
            if ($('#roles').val() === 'Incubatee') {
                $("#incubateeCategoryDiv").show();
            }
        }
    </script>
<style>
    nav {
        margin-bottom: 0;
        background-color: grey;
    }
</style>
@endsection
