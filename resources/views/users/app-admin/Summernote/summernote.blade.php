@extends('layouts.admin-layout')

@section('content')

    <br>

    <head>
        <meta charset="UTF-8">
        <title>bootstrap4</title>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <script src="https://cdn.tiny.cloud/1/h9arxjl10ypkgljjm33ddt16vik05kh57kxh07smgu9b7ftw/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>


        <script>
            tinymce.init({
                selector: 'textarea',
                branding: false,
                placeholder: "Content here ..        P.S For better readability set font Size to 18pt and font to Arial",
                height: 700,
                plugins: 'autolink lists media  table searchreplace preview insertdatetime anchor print charmap emoticons image imagetools help wordcount fullscreen colorpicker',
                toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table advancedlist directionality  insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image paste',
                imagetools_cors_hosts: ['thepropella.co.za', '127.0.0.1:8000'],
                toolbar_mode: 'floating',
                tinycomments_mode: 'embedded',
                tinycomments_author: 'Author name',

                /* enable title field in the Image dialog*/
                image_title: true,
                /* enable automatic uploads of images represented by blob or data URIs*/
                automatic_uploads: true,
                /*
                  URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
                  images_upload_url: 'postAcceptor.php',
                  here we add custom filepicker only to Image dialog
                */
                file_picker_types: 'image',
                /* and here's our custom image picker*/
                file_picker_callback: function (cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    input.onchange = function () {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function () {
                            /*
                              Note: Now we need to register the blob in TinyMCEs image blob
                              registry. In the next release this part hopefully won't be
                              necessary, as we are looking to handle it internally.
                            */
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            /* call the callback and populate the Title field with the file name */
                            cb(blobInfo.blobUri(), { title: file.name });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                },
            });
        </script>
        <style>
            ul.breadcrumb {
                padding: 10px 16px;
                list-style: none;
                background-color:grey;
            }
            ul.breadcrumb li {
                display: inline;
                font-size: 18px;
            }
            ul.breadcrumb li+li:before {
                padding: 8px;
                color: white;
                content: ">\00a0";
            }
            ul.breadcrumb li a {
                color: black;
                text-decoration: none;
            }
        </style>
    </head>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

    </head>
    <br>
    <br>

    <body>


    <ul class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="/blog-index">All Blogs</a></li>
        <li><a href="/edit-blog">Create Blog</a></li>
    </ul>

    <!--Create blog-->
    <div class="" style="margin-top: 10vh">
        <div class="card" style="width: 1070px;margin: 0 auto">
            <br>
            <h4 style="margin-left: 350px">Create Blog</h4>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col m4">
                    <input id="author" type="text" class="validate">
                    <label for="author">Author</label>
                </div>
                <div class="input-field col m4">
                    <input id="title" type="text" class="validate">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col m4">
                    <input id="description" type="text" class="validate">
                    <label for="description">Description</label>
                </div>
            </div>
            <div class="row" style="margin-right: 2em;margin-left: 2em;">
                <div class="file-field input-field col m4">
                    <div class="btn" style="height: 50px;">
                        <span>Blog Image</span>
                        <input type="file" id="blog_image_url">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>

            </div>
            <div class="row"style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col m4">
                    <input id="pdf_description" type="text" class="validate">
                    <label for="pdf_description">PDF Description</label>
                </div>
                <div class="col m6">
                    <div class="file-field input-field" style="bottom:0px!important;">
                        <div class="btn">
                            <span>Upload PDF</span>
                            <input id="pdf_url" type="file" name="pdf_url">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path"
                                   type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 800px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn section" id="blog-upload-submit-button">Save</a>
                </div>
            </div>

            <br>
        </div>
    </div>

    <!--create summernote-->
    <div class="container">
        <div class=>
            <form action="{{route('summernotePersist')}}" method="POST" style="margin-top: 50px" class="card" id="summernote-form">
                <div class="form-group">
                    <textarea id="summernote" name="summernoteInput" class="summernote"></textarea>
                </div>
                <button type="submit" class="waves-effect waves-light btn section ">Submit</button>
                {!!csrf_field()!!}
            </form>

        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            //Create blog
            $('#blog-upload-submit-button').on('click', function () {
                let title = $('#title').val();
                let description = $('#description').val();
                let author = $('#author').val();
                let pdf_description = $('#pdf_description').val();


                if (title === "" || description === "") {
                    alert("Please insert a title and description!");
                } else {
                    let formData = new FormData();
                    formData.append('author', author);
                    formData.append('title', title);
                    formData.append('description', description);
                    formData.append('pdf_description', pdf_description);

                    jQuery.each(jQuery('#pdf_url')[0].files, function (i, file) {
                        formData.append('pdf_url', file);
                    });

                    jQuery.each(jQuery('#blog_image_url')[0].files, function (i, file) {
                        formData.append('blog_image_url', file);
                    });

                    let url = "{{route('blog.store')}}";
                    $.ajax({
                        url: url,
                        data: formData,
                        type: 'post',
                        processData: false,
                        contentType: false,
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            alert(response.message);
                            // blog_id = response.blog.id;-
                            $('.second-row').show();

                            $('#summernote-form').append(
                                '<input name="blog_id" value="'+response.blog_id+'" hidden >'
                            );
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                }
            });
        });

    </script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">







        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>


    </body>



    @endsection
