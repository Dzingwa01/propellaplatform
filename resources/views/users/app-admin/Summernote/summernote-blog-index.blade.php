@extends('layouts.admin-layout')

@section('content')
    <div class="container-fluid">
        <br>
        <br>
        {{ Breadcrumbs::render('blog-index')}}

        <head>

        <div class="row" style="margin-top: 10vh;">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Blogs</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="blog-table">
                    <thead>
                    <tr>
                        <th>Author</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Date of Blog</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New User" href="{{url('/summernote')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>

    </div>
    @push('custom-scripts')

        <script>

            $(document).ready(function (data) {
                $(function () {
                    $('#blog-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '{{route('get-blog')}}',
                        columns: [
                            {data: 'author', name: 'author'},
                            {data: 'title', name: 'title'},
                            {data: 'description', name: 'description'},
                            {data: 'blog_date', name: 'blog_date'},
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="blog-table_length"]').css("display","inline");
                });
            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
