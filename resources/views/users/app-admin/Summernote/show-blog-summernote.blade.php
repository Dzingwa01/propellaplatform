@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Media/media.css"/>
    <link rel="stylesheet" type="text/css" href="/css/Blog/blog-show.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>


        #crouton {
            margin-left: 7em
        }
        #crouton ul {
            margin: 0;
            padding: 0;
            overflow: hidden;
            width: 100%;
            list-style: none;
        }

        #crouton li {
            float: left;
            margin: 0 10px;
        }

        #crouton a {
            background: #ddd;
            padding: .7em 1em;
            float: left;
            text-decoration: none;
            color: #444;
            text-shadow: 0 1px 0 rgba(255,255,255,.5);
            position: relative;
        }

        #crouton li:first-child a {
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        #crouton li:last-child a {
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        #crouton a:hover {
            background: #99db76;
        }

        #crouton li:not(:first-child) > a::before {
            content: "";
            position: absolute;
            top: 50%;
            margin-top: -1.5em;
            border-width: 1.5em 0 1.5em 1em;
            border-style: solid;
            border-color: #ddd #ddd #ddd transparent;
            left: -1em;
        }

        #crouton li:not(:first-child) > a:hover::before {
            border-color: #99db76 #99db76 #99db76 transparent;
        }

        #crouton li:not(:last-child) > a::after {
            content: "";
            position: absolute;
            top: 50%;
            margin-top: -1.5em;
            border-top: 1.5em solid transparent;
            border-bottom: 1.5em solid transparent;
            border-left: 1em solid #ddd;
            right: -1em;
        }

        #crouton li:not(:last-child) > a:hover::after {
            border-left-color: #99db76;
        }

        .blogpost--author {
            width: 50%;
            margin-bottom: 0px;
        }

        .blogpost--author--name {
            display: inline-block;
            vertical-align: middle;
            color: #172b4d;
            -webkit-transition: color .3s ease-in-out;
            -moz-transition: color .3s ease-in-out;
            -ms-transition: color .3s ease-in-out;
            -o-transition: color .3s ease-in-out;
            transition: color .3s ease-in-out
        }
    </style>
    <body>
    <div id="desktop-media">
        <div class="row" style="margin-top: 10vh">
            <div id="crouton">
                <ul>
                    <li><a href="/blogs">Blog</a></li>
                    <li><a href="#">{{$blog->title}}</a></li>
                </ul>
            </div>
        </div>
        <main>
            <div class="n-banner downl_ban" style="background-image: url('/storage/{{$blog->blog_image_url}}'); height: 662px">

            </div>
            <div class="container">
                <div class="row faq">
                    <div style="margin-bottom: 30px;">
                        <div style="color: darkgreen;font-size: 52px;line-height: 1.15em;font-weight: 600;"><b>{{$blog->title}}</b></div>
                        <div style="color: lightseagreen;font-size: 32px;line-height: 1.15em;font-weight: 600;">{{$blog->description}}</div>
                        <div style="margin-right: auto; width: max-content; margin-bottom: 30px;">
                            <b>
                                <span class="blogpost--author--image">
                                </span>
                                <span class="blogpost--author--name" style="color: black">
                                    By
                                    <span style="color: black">
                                        {{$blog->author}}
                                    </span>
                                </span>
                                <span class="blogpost--meta--time">
                                    <span class="blogpost--meta--divider" style="color: #0a97d9">
                                        |
                                    </span>
                                    <span class="blogpost--date" style="color: black">
                                        Published on<span itemprop="datePublished">
                                            {{ date('jS M Y', strtotime($blog->blog_date)) }}
                                        </span>
                                    </span>
                                    <span class="blogpost--meta--divider" style="color: #0a97d9">
                                        |
                                    </span>
                                    <span class="blogpost--readtime" style="color: black">
                                        6 min read
                                    </span>
                                </span>
                            </b>

                        </div><hr>
                    </div>
                    <div class="col-center faq">
                        <p>{!!$blog->summernote->content!!}</p>
                        <summary tabindex="1">
                            <h3 style="color: darkgreen;font-size: 35px;line-height: 1.15em;font-weight: 600;">OTHER POSTS</h3>
                        </summary>
                        @for($i = 0; $i<$blogs->count(); $i++)
                            @if($i <= 2)
                                <div class="col l4 blog card1">
                                    @if($blogs[$i]->blog_image_url != null)
                                        <div class="card" >
                                            <div class="card-image">
                                                <img style="width:100%;height: 40vh;"
                                                     src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                                <div class="overlay">
                                                    <h6 class="center-align"><b>{{$blogs[$i]->title}}</b></h6>
                                                    <p class="center-align" style="font-size: 11px">{{$blogs[$i]->description}}</p>
                                                    <input hidden  class="news_id" data-value="{{$blogs[$i]->slug}}">
                                                </div>
                                                <span class="card-title"
                                                      style="background-color: #1a237e;font-weight: bold;font-size: 1em">BLOG</span>
                                                <input hidden class="blog_id" data-value="{{$blogs[$i]->slug}}">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        @endfor
                    </div>
                </div>
            </div>
        </main>
    </div>
    <div id="mobile-media">
        <div class="row" style="margin-top: 10vh">
            <div id="crouton">
                <ul>
                    <li><a href="/blogs">Blog</a></li>
                    <li><a href="#">{{$blog->title}}</a></li>
                </ul>
            </div>
        </div>
        <main>
            <div class="n-banner downl_ban" style="background-image: url('/storage/{{$blog->blog_image_url}}');">

            </div>
            <div class="container">
                <div class="row faq">
                    <div style="margin-bottom: 30px;">
                        <div style="color: darkgreen;font-size: 28px;line-height: 1em;font-weight: 600;"><b>{{$blog->title}}</b></div>
                        <div style="color: lightseagreen;font-size: 16px;line-height: 1em;font-weight: 600;">{{$blog->description}}</div>
                        <div style="margin-right: auto; margin-bottom: 30px;">
                            <b>
                                <span class="blogpost--author--image">
                                </span>
                                <span class="blogpost--author--name" style="color: black">
                                    By
                                    <span style="color: black">
                                        {{$blog->author}}
                                    </span>
                                </span>
                                <span class="blogpost--meta--time">
                                    <span class="blogpost--meta--divider" style="color: #0a97d9">
                                        |
                                    </span>
                                    <span class="blogpost--date" style="color: black">
                                        Published on<span itemprop="datePublished">
                                            {{ date('jS M Y', strtotime($blog->blog_date)) }}
                                        </span>
                                    </span>
                                    <span class="blogpost--meta--divider" style="color: #0a97d9">
                                        |
                                    </span>
                                    <span class="blogpost--readtime" style="color: black">
                                        6 min read
                                    </span>
                                </span>
                            </b>

                        </div><hr>
                    </div>
                    <div class="col-center faq">
                        <p>{!!$blog->summernote->content!!}</p>
                        <summary tabindex="1">
                            <h3 style="color: darkgreen;font-size: 35px;line-height: 1.15em;font-weight: 600;">OTHER POSTS</h3>
                        </summary>
                        @for($i = 0; $i<$blogs->count(); $i++)
                            @if($i <= 2)
                                <div class="col-12 l4 blog">
                                    @if($blogs[$i]->blog_image_url != null)
                                        <div class="card" >
                                            <div class="card-image">
                                                <img style="width:100%;height: 40vh;"
                                                     src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                                <div class="overlay">
                                                    <h6 class="center-align"><b>{{$blogs[$i]->title}}</b></h6>
                                                    <p class="center-align" style="font-size: 11px">{{$blogs[$i]->description}}</p>
                                                    <input hidden  class="news_id" data-value="{{$blogs[$i]->slug}}">
                                                </div>
                                                <span class="card-title"
                                                      style="background-color: #1a237e;font-weight: bold;font-size: 1em">BLOG</span>
                                                <input hidden class="blog_id" data-value="{{$blogs[$i]->slug}}">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        @endfor
                    </div>
                </div>
            </div>
        </main>
    </div>
    </body>
    <script>
        $(document).ready(function () {
            $('.blog').each(function () {
                let blog_id = $(this).find('.blog_id').attr('data-value');


                $(this).on('click', function () {
                    location.href = '/blog/' + blog_id;
                });
            });
        });
    </script>
@endsection
