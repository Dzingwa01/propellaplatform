@extends('layouts.admin-layout')

@section('content')
<br>
<br>
    {{ Breadcrumbs::render('ques-cat')}}
    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
         <span onclick="window.history.back()"
               class="close-icon right" style="width: auto; height: auto; "><i class="material-icons">close</i></span>
        <br>
        <h5 style="margin-left: 25%">Create Shadow Board Category </h5>
        <br>
        <div class="input-field col s2" style="margin-left: 2em;margin-right: 2em;">
            <input id="category_name" type="text" class="validate">
            <label for="category_name">Category Name</label>
        </div>
        <br>
        <button class="btn waves-effect waves-light" style="margin-left:500px;" id="save-category" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
        <br>
        <br>
    </div>



    <script>
        $(document).ready(function () {
            $('#save-category').on('click',function () {

                let formData = new FormData();
                formData.append('category_name', $('#category_name').val());

                $.ajax({
                    url: '{{route('store')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        console.log("success", response);
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
        .close-icon {
            cursor: pointer;
            display: block;
            right: 32px;
            width: 50px;
            height: 50px;
            opacity: 0.3;
        }
    </style>

@endsection
