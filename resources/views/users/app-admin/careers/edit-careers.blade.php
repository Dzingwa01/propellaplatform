@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>

    <head>
        <meta charset="UTF-8">
        <title>bootstrap4</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
    </head>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>

    <div class="card hoverable" style=";width: 1100px;margin-left: 200px">
        <div class="container">
            <br>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Update Careers</h6>
            <br>
            <form id="careers-form" class=" col s12" style="margin-top:1em;">
                @csrf
                <input value="{{$career->id}}" id="career_id" hidden>
                <div class="row">
                    <div class="input-field col m6">
                        <input id="title" value="{{$career->title}}" type="text" class="validate">
                        <label for="title">Title</label>
                    </div>
                    <div class="input-field col m6">
                        <textarea id="description" class="materialize-textarea">{{$career->description}}</textarea>
                        <label for="description">Description</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col offset-m4">
                        <button id="save-incubatee-employee-details" style="margin-left: 2em"
                                class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
                <br>
                <br>
            </form>
        </div>
    </div>

    <!--create summernote-->
    <div class="container">
        <div class="panel-body">
            <form action="{{url('/update-career-summernote/'.$career->id)}}" method="POST" style="margin-top: 50px" id="summernote-form">
                <div class="form-group">
                    <textarea id="summernote" name="content" class="summernote" ></textarea>
                </div>
                <div class="form-group">
                    <button type="submit">Submit</button>
                    {!!csrf_field()!!}
                </div>
            </form>
        </div>
    </div>



    @push('custom-scripts')
        <script>

            $(document).ready(function () {

                $('#careers-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();

                    formData.append('career_id', $('#career_id').val());
                    formData.append('title', $('#title').val());
                    formData.append('description', $('#description').val());
                    formData.append('content', $('.content').val());

                    console.log("career", formData);
                    let url = '/update-careers/' + '{{$career->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response, a, b) {
                            alert(response.message);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
                var content = {!! json_encode($career->careersSummernote->content) !!};
                $('.summernote').summernote('code', content);
                $('#summernote').summernote({
                    placeholder: 'Content here ..',
                    height: 1500,
                });
            });
        </script>
    @endpush

    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

    </head>



    <head>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

        <!-- include summernote css/js-->
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    </head>

@endsection
