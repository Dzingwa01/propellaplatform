@extends('layouts.admin-layout')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <br/>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Propella Roles Database</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="roles-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add User Role" href="{{url('createRole')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>


    </div>

    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#roles-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-roles')}}',
                        columns: [
                            {data: 'name', name: 'name'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]

                    });
                    $('select[name="roles-table_length"]').css("display","inline");
                } );
            });

            function confirm_delete_roles(obj){
                var r = confirm("Are you sure want to delete this user?");
                if (r == true) {
                    $.get('/role/delete/'+obj.id,function(data,status){
                        console.log('Data',data);
                        console.log('Status',status);
                        if(status=='success'){
                            alert(data.message);
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }
            }

        </script>
    @endpush
    @endsection
