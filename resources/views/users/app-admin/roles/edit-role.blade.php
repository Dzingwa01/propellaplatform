@extends('layouts.admin-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <head>
        <!-- jQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    </head>

    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <br>
        @csrf
        <input value="{{$role->id}}" id="role_id" hidden>
        <h5 class="center-align">Update role</h5>
        <br>
        <div class="input-field col s2" style="margin-left: 2em;margin-right: 2em;">
            <input id="name" type="text" value={{$role->name}} class="validate">
            <label for="name">Role name</label>
        </div>

        <br/>
        <button id="success">Submit</button>
<br/><br/>
    </div>
    <style>
        body {
            font-family: sans-serif;
            text-align: center;
        }
        button {
            background-color: cadetblue;
            color: whitesmoke;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            font-size: 18px;
            font-weight: 500;
            border-radius: 7px;
            padding: 15px 35px;
            cursor: pointer;
            white-space: nowrap;
            margin: 10px;
        }
        img {
            width: 200px;
        }
        input[type="text"] {
            padding: 12px 20px;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 10px;
            box-sizing: border-box;
        }
        h1 {
            border-bottom: solid 2px grey;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(document).on('click', '#success', function(e) {

                let formData = new FormData();
                formData.append('name', $('#name').val());
                formData.append('display_name', $('#display_name').val());
                formData.append('guard_name', $('#guard_name').val());
                formData.append('permissions', $('#permissions').val());

                console.log(formData);

                let url = '/updateRole/' + $('#role_id').val();
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
                swal(
                    'Success',
                    'Role saved <b style="color:green;">Successfully</b>',
                    'success',
                    setTimeout(function(){
                        window.location.href = '{{url('/indexRole')}}';
                    }, 3000)
                )
            });
            // Alert With Custom Icon and Background Image
            $(document).on('click', '#icon', function(event) {
                swal({
                    title: 'Custom icon!',
                    text: 'Alert with a custom image.',
                    imageUrl: 'https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg',
                    imageWidth: 200,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                    animation: false
                })
            });

            $(document).on('click', '#image', function(event) {
                swal({
                    title: 'Custom background image, width and padding.',
                    width: 700,
                    padding: 150,
                    background: '#fff url(https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg)'
                })
            });
        });

        function goBack() {
            window.history.back();
        }
    </script>
    <style>
        .close-icon {
            cursor: pointer;
            display: block;
            right: 32px;
            width: 50px;
            height: 50px;
            opacity: 0.3;
        }
    </style>
@endsection
