@extends('layouts.bootcamper-layout')
@section('content')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <br/>
    <br>
    <br>
    <div class="container-fluid">
        <form id="basic-info-form">
            <div class="card" style="margin-left: 4em;margin-right: 4em">
                <input id="user-id-input" value="{{$user->id}}" hidden disabled>
                <br/>
                <div class="row center">
                    <h4>Personal Information</h4>
                    <div class="col l12 m12 s12">
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <select id="title-desktop" required>
                                    <option value="{{$user->title}}" selected>{{$user->title}}</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Ms</option>
                                    <option value="Miss">Mr</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr">Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="initials-desktop" type="text" class="validate" value="{{$user->initials}}"
                                       required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="first_name-desktop" type="text" value="{{$user->name}}" class="validate"
                                       required>
                                <label for="first_name-desktop">First Name</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="last_name-desktop" type="text" value="{{$user->surname}}" class="validate"
                                       required>
                                <label for="last_name-desktop">Last Name</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="age-desktop" type="number" value="{{$user->age}}" class="validate" required>
                                <label for="age-desktop">Age</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <select id="gender-desktop" required>
                                    <option value="{{$user->gender}}" selected>{{$user->gender}}</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                                <label for="gender-desktop">Gender</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="venture_name-desktop" type="text" value="{{$user->applicant->venture_name}}"
                                       class="validate" required>
                                <label for="venture_name-desktop">Venture Name</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="ID-desktop" type="text" value="{{$user->id_number}}" class="validate"
                                       required>
                                <label for="ID-desktop">ID Number</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="email-desktop" type="email"  value="{{$user->email}}"
                                       class="validate" required>
                                <label for="email-desktop">Email address</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="cell_number-desktop" type="text" value="{{$user->contact_number}}"
                                       class="validate" required>
                                <label for="cell_number-desktop">Cell Number</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="data_cellnumber-desktop" type="text" value="{{$user->data_cellnumber}}" class="validate"
                                       required>
                                <label for="data_cellnumber-desktop">Data cell number</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <select id="service_provider_network-desktop" required>
                                    <option value="{{$user->service_provider_network}}" selected>{{$user->service_provider_network}}</option>
                                    <option value="MTN">MTN</option>
                                    <option value="Vodacom">Vodacom</option>
                                    <option value="Cell C">Cell C</option>
                                    <option value="Telkom">Telkom</option>
                                </select>
                                <label for="service_provider_network-desktop">Service provider</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="address-desktop" type="text" value="{{$user->address_one}}" class="validate"
                                       required>
                                <label for="address-desktop">Physical Address 1</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="address_two-desktop" type="text" value="{{$user->address_two}}"
                                       class="validate" required>
                                <label for="address_two-desktop">Physical Address 2</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="address_three-desktop" type="text" value="{{$user->address_three}}"
                                       class="validate" required>
                                <label for="address_three-desktop">Physical Address 3</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="city-desktop" type="text" value="{{$user->city}}" class="validate" required>
                                <label for="city-desktop">City</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="code-desktop" type="text" value="{{$user->code}}" class="validate" required>
                                <label for="code-desktop">Postal Code</label>
                            </div>
                            <div id="empty-div" class="col l6 m6 s12"></div>
                            <div class="input-field col l6 m6 s12">
                                <select id="race-desktop" required>
                                    <option value="{{$user->race}}" selected>{{$user->race}}</option>
                                    <option value="Black">African</option>
                                    <option value="White">White</option>
                                    <option value="Indian">Indian</option>
                                    <option value="Coloured">Coloured</option>
                                    <option value="Other">Other</option>
                                </select>
                                <label for="race-desktop">Race</label>
                            </div>

                        </div>
                        <div class="row" style="margin-left: 80%">
                            <input class="btn waves-effect waves-light" type="submit">
                        </div>
                        <br/>
                        <br/>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();

            $('#basic-info-form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();
                let user_id = $("#user-id-input").val();

                formData.append('name', $('#first_name-desktop').val());
                formData.append('surname', $('#last_name-desktop').val());
                formData.append('id_number', $('#ID-desktop').val());
                formData.append('contact_number', $('#cell_number-desktop').val());
                formData.append('address_one', $('#address-desktop').val());
                formData.append('title', $('#title-desktop').val());
                formData.append('initials', $('#initials-desktop').val());
                formData.append('venture_name', $('#venture_name-desktop').val());
                formData.append('address_two', $('#address_two-desktop').val());
                formData.append('address_three', $('#address_three-desktop').val());
                formData.append('city', $('#city-desktop').val());
                formData.append('code', $('#code-desktop').val());
                formData.append('role_id', $('#role-desktop').val());
                formData.append('age', $('#age-desktop').val());
                formData.append('gender', $('#gender-desktop').val());
                formData.append('race', $('#race-desktop').val());
                formData.append('email', $('#email-desktop').val());
                formData.append('service_provider_network', $('#service_provider_network-desktop').val());
                formData.append('data_cellnumber', $('#data_cellnumber-desktop').val());


                $.ajax({
                    url: "/bootcamper-update-basic-info/" + user_id,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#empty-div').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        $('#empty-div').notify(response.message, "error");

                    }
                });
            });
        });
    </script>
    @endsection
