@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-mentor-venture',$mentor_user,$venture)}}
    <div class="row center">
        <br>
        <h4>Content between <b>{{$mentor_user->name}} {{$mentor_user->surname}}</b> and <b>{{$venture->company_name}}</b></h4>
    </div>

    <div class="row center">
        <div class="row center" style="background-color: black;">
            <h4 style="color:white;">Shadowboards <i class="material-icons">arrow_downward</i> </h4>
        </div>
        @foreach($venture_shadowboards as $v_shadow)
            <div class="col l4 m4 s12">
                <div class="card center">
                    <div class="card-content">
                        <h5>{{$v_shadow->mentor_name}} {{$v_shadow->mentor_surname}}</h5>
                        <p>Date: {{$v_shadow->shadow_board_date}}</p>
                        <p>Time: {{$v_shadow->shadow_board_time}}</p>
                    </div>
                    <div class="card-action">
                        <a class="btn teal" id="{{$v_shadow->mentor_venture_shadowboard_id}}" onclick="viewResults(this)">Results</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>


    <script>
        function viewResults(obj){
            window.location.href = '/view-mentor-venture-shadowboard-results/' + obj.id;
        }
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
