@extends('layouts.admin-layout')

@section('content')

    <br>
    <br>
    {{ Breadcrumbs::render('users-email')}}
    <br>
    <br>
    <div class="section" style="margin-left: 7em">
        <!!-- Users -->
        <div class="row">
            <div class="card col m5 z-depth-5" style="margin-left: 2em">
                <br>
                <br>
                <h5 style="top: 10vh;margin-left: 4em"><b>Send email to Propella Users</b></h5>

                <div class="input-field col s12 " style="margin-top: 10vh">
                    <select id="users-selected" multiple>
                        <option value="" disabled selected>Please select Users</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}} {{$user->surname}} - {{$user->email}}</option>
                        @endforeach
                    </select>
                    <label>Propella Users</label>
                </div>
                <div class="row" style="margin-left: 1em">
                    <a class="waves-effect waves-light btn " id="send-mail-button">Send mail</a>
                </div>
                <br>
            </div>
            <!!-- Visitors -->
            <div class="card col m5 z-depth-5" style="margin-left: 2em">
                <br>
                <br>
                <h5 style="top: 10vh;margin-left: 4em"><b>Send email to Propella Visitors</b></h5>

                <div class="input-field col s12" style="margin-top: 10vh">
                    <select id="visitors-selected" multiple>
                        <option value="" disabled selected>Please select Visitors</option>
                        @foreach($visitors as $visitor)
                            <option
                                value="{{$visitor->id}}">{{$visitor->first_name}}  {{$visitor->last_name}} {{$visitor->email}}</option>
                        @endforeach
                    </select>
                    <label>Propella Users</label>
                </div>
                <div class="row" style="margin-left: 1em">
                    <a class="waves-effect waves-light btn " id="send-mail-visitor-button">Send mail</a>
                </div>
                <br>
            </div>
        </div>

        <div class="row">
            <div class="card col m5 z-depth-5" style="margin-left: 2em">
                <br>
                <br>
                <h5 style="top: 10vh;margin-left: 4em"><b>Send email to Propella incubatees</b></h5>

                <div class="input-field col s12" style="margin-top: 10vh">
                    <select id="ventures-selected" multiple>
                        <option value="" disabled selected>Please select incubatees</option>
                        @foreach($incubatees as $incubatee)
                            <option value="{{$incubatee->id}}">{{$incubatee->user->email}}</option>
                        @endforeach
                    </select>
                    <label>Propella Users</label>
                </div>
                <div class="row" style="margin-left: 1em">
                    <a class="waves-effect waves-light btn " id="send-venture-mail-button">Send mail</a>
                </div>
                <br>
            </div>

            <div class="card col m5 z-depth-5" style="margin-left: 2em">
                <br>
                <br>
                <h5 style="top: 10vh;margin-left: 4em"><b>Send email to Companies employees</b></h5>

                <div class="input-field col s12" style="margin-top: 10vh">
                    <select id="company-selected" multiple>
                        <option value="" disabled selected>Please select employees</option>
                        @foreach($companiesEmployees as $companiesEmployee)
                            <option value="{{$companiesEmployee->id}}">{{$companiesEmployee->name}} {{$companiesEmployee->surname}} - {{$companiesEmployee->email}}</option>
                        @endforeach
                    </select>
                    <label>Propella Users</label>
                </div>
                <div class="row" style="margin-left: 1em">
                    <a class="waves-effect waves-light btn " id="send-company-mail-button">Send mail</a>
                </div>
                <br>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function () {

            $('select').formSelect();

            $('#send-mail-button').on('click', function(){
                let selected_users = [];
                let formData = new FormData();

                jQuery.each(jQuery('#users-selected').val(), function (i, value) {
                    selected_users.push(value);
                });

                formData.append('selected_users', JSON.stringify(selected_users));

                let url = "/send-email-to-users";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#send-mail-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#send-mail-button').notify(response.message, "error");
                    }
                });
            });

            $('#send-mail-visitor-button').on('click', function(){
                let selected_visitors = [];
                let formData = new FormData();

                jQuery.each(jQuery('#visitors-selected').val(), function (i, value) {
                    selected_visitors.push(value);
                });

                formData.append('selected_visitors', JSON.stringify(selected_visitors));

                let url = "/send-email-to-visitors";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#send-mail-visitor-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#send-mail-visitor-button').notify(response.message, "error");
                    }
                });
            });

            $('#send-venture-mail-button').on('click', function(){
                let selected_ventures = [];
                let formData = new FormData();

                jQuery.each(jQuery('#ventures-selected').val(), function (i, value) {
                    selected_ventures.push(value);
                });

                formData.append('selected_ventures', JSON.stringify(selected_ventures));

                let url = "/send-email-to-venture";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#send-venture-mail-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#send-venture-mail-button').notify(response.message, "error");
                    }
                });
            });

            $('#send-company-mail-button').on('click', function(){
                let selected_company = [];
                let formData = new FormData();

                jQuery.each(jQuery('#company-selected').val(), function (i, value) {
                    selected_company.push(value);
                });

                formData.append('selected_company', JSON.stringify(selected_company));

                let url = "/send-email-to-company";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#send-company-mail-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#send-company-mail-button').notify(response.message, "error");
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
    @endsection
