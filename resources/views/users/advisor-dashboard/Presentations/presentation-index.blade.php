@extends('layouts.advisor-layout')

@section('content')


    <div class="container-fluid" style="margin-top: 10vh">
        <h4 class="center-align"><b>PRESENTATIONS</b></h4>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Upload" href="{{url('create-presentation')}}">
            <i class="large material-icons">add</i>
        </a>
    </div>
    @push('custom-scripts')

        <script>
            //
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#users-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        stateSave: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-presentation')}}',
                        columns: [
                            {data: 'title'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]

                    });
                    $('select[name="users-table_length"]').css("display", "inline");
                });
            });

            function confirm_delete_presentation(obj){
                var r = confirm("Are you sure want to delete this presentation?");
                if (r == true) {
                    $.get('/destroy-presentation/'+obj.id,function(data,status){
                        console.log('Data',data);
                        console.log('Status',status);
                        if(status=='success'){
                            alert('Deleted successfully');
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }
            }
        </script>
    @endpush

@endsection
