@extends('layouts.advisor-layout')

@section('content')

    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <br>
        <h6 style="margin-left: 15em">CREATE EVENT PRESENTATION</h6>
        <div class="row" style="margin-left: 7em">
            <div class="input-field col s10">
                <input id="title" type="text" class="validate">
                <label for="title">Ttitle</label>
            </div>
            <div class="col m10">
                <div class="input-field" style="bottom:0px!important;">
                    <input id="video" type="text">
                    <label for="video">Video link</label>
                </div>
            </div>
            <div class="file-field input-field col s10" hidden>
                <div class="btn">
                    <span>UPLOAD HANDOUT</span>
                    <input  multiple="multiple" type="file" name="handout[]" id="handout">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 100px" hidden>
            <div class="file-field input-field col s10">
                <div class="btn">
                    <span>UPLOAD PRESENTATION</span>
                    <input multiple type="file" id="presentation">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 600px;">
            <div class="col s4">
                <a class="waves-effect waves-light btn" id="question-category-submit-button">Save</a>
            </div>
        </div>
        <br>
    </div>


    <script>
        $(document).ready(function () {
            $('.modal').modal();
            $('select').formSelect();
            $('#question-category-submit-button').on('click', function () {

                let formData = new FormData();
                formData.append('title', $('#title').val());
                formData.append('status', $('#status').val());
                formData.append('video', $('#video').val());
                jQuery.each(jQuery('#handout')[0].files, function (i, file) {
                    formData.append('handout', file);
                });
                jQuery.each(jQuery('#presentation')[0].files, function (i, file) {
                    formData.append('presentation', file);
                });
                /* jQuery.each(jQuery('#audio')[0].files, function (i, file) {
                     formData.append('audio', file);
                 });*/

                $.ajax({
                    url: '{{route('store-presentation')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},



                    success: function (response, a, b) {
                        $('#question-category-submit-button').notify(response.message, "success");
                        setTimeout(function () {
                            window.location.href = '/index-presentation';
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>


@endsection
