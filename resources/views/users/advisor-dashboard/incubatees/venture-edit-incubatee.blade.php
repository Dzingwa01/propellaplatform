@extends('layouts.advisor-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <div class="container">

        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Edit
                Incubatee</h6>
            <div class="row card hoverable">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s6"><a href="#test1" class="active">Personal Details</a></li>
                        <li class="tab col s6"><a href="#test3">Media</a></li>
                    </ul>
                </div>
                <div id="test1" class="col s12">
                    <form id="incubatee-details-update-form" method="post" class="col s12" style="margin-top:1em;"
                          enctype="multipart/form-data">
                        @csrf
                        <input value="{{$current_user->id}}" id="incubatee_id" hidden>
                        <h5>Personal Details</h5>

                        <div class="row">
                            <div class="input-field col m6">
                                <select id="title" name="title">
                                    @if($current_user->title == null)
                                        <option>Choose title</option>
                                    @else
                                        <option value="Mr"{{$current_user->title=='Mr'?'selected':''}}>Mr</option>
                                        <option value="Mrs"{{$current_user->title=='Mrs'?'selected':''}}>Mrs</option>
                                        <option value="Miss"{{$current_user->title=='Miss'?'selected':''}}>Miss</option>
                                        <option value="Ms"{{$current_user->title=='Ms'?'selected':''}}>Ms</option>
                                        <option value="Dr"{{$current_user->title=='Dr'?'selected':''}}>Dr</option>
                                    @endif
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="initials" type="text" class="validate" value="{{$current_user->initials}}"
                                       required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="name" type="text" class="validate" value="{{$current_user->name}}" required>
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="surname" type="text" class="validate" value="{{$current_user->surname}}"
                                       required>
                                <label for="surname">Surname</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="id_number" type="text" class="validate" value="{{$current_user->id_number}}"
                                       required>
                                <label for="id_number">ID Number</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="dob" type="date" class="validate"
                                       value="{{$current_user->dob}}" required>
                                <label for="dob">Date of Birth</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="age" type="text" class="validate" value="{{$current_user->age}}"
                                       placeholder="Age" required>
                            </div>
                            <div class="input-field col m6">
                                <select id="gender" name="gender">
                                    @if($current_user->gender == null)
                                        <option>Choose gender</option>
                                    @else
                                        <option value="Male"{{$current_user->gender=='Male'?'selected':''}}>Male</option>
                                        <option value="Female"{{$current_user->gender=='Female'?'selected':''}}>Female</option>
                                    @endif
                                </select>
                                <label>Gender</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="email" type="email" class="validate" value="{{$current_user->email}}"
                                       required>
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="tel" class="validate"
                                       value="{{$current_user->contact_number}}" required>
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="address_one" type="tel" class="validate"
                                       value="{{$current_user->address_one}}" required>
                                <label for="address_one">Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="address_two" type="tel" class="validate"
                                       value="{{$current_user->address_two}}" required>
                                <label for="address_two">Address 2</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="address_three" type="tel" class="validate"
                                       value="{{$current_user->address_three}}" required>
                                <label for="address_three">Address 3</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="city" type="tel" class="validate"
                                       value="{{$current_user->city}}" required>
                                <label for="city">City</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="code" type="tel" class="validate" value="{{$current_user->code}}" required>
                                <label for=code">Postal Code</label>
                            </div>
                            <div class="input-field col m6">
                                <div class="input-field col m6">
                                    <select id="nmu_graduate" name="nmu_graduate">
                                        @if($incubatee->nmu_graduate== null)
                                            <option>Are you nmu graduate</option>
                                        @else
                                            <option value="Yes"{{$incubatee->nmu_graduate=='Yes'?'selected':''}}>Yes</option>
                                            <option value="No"{{$incubatee->nmu_graduate=='No'?'selected':''}}>No</option>
                                        @endif
                                    </select>
                                    <label for="nmu_graduate">NMU Graduate</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="disabled">
                                    @if($incubatee->disabled== null)
                                        <option>Are you disabled</option>
                                    @else
                                        <option value="Yes"{{$incubatee->disabled=='Yes'?'selected':''}}>Yes</option>
                                        <option value="No"{{$incubatee->disabled=='No'?'selected':''}}>No</option>
                                    @endif
                                </select>
                                <label>Disability</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="home_language">
                                    @if($incubatee->home_language== null)
                                        <option>Select language</option>
                                    @else
                                        <option value="English"{{$incubatee->home_language=='English'?'selected':''}}>English</option>
                                        <option value="IsiXhosa"{{$incubatee->home_language=='IsiXhosa'?'selected':''}}>IsiXhosa</option>
                                        <option value="IsiZulu"{{$incubatee->home_language=='IsiZulu'?'selected':''}}>IsiZulu</option>
                                        <option value="Afrikaans"{{$incubatee->home_language=='Afrikaans'?'selected':''}}>Afrikaans</option>
                                        <option value="IsiNdebele"{{$incubatee->home_language=='IsiNdebele'?'selected':''}}>IsiNdebele</option>
                                        <option value="Sepedi"{{$incubatee->home_language=='Sepedi'?'selected':''}}>Sepedi</option>
                                        <option value="SeTswana"{{$incubatee->home_language=='SeTswana'?'selected':''}}>SeTswana</option>
                                        <option value="TshiVenḓa"{{$incubatee->home_language=='TshiVenḓa'?'selected':''}}>TshiVenḓa</option>
                                        <option value="SiSwati"{{$incubatee->home_language=='SiSwati'?'selected':''}}>SiSwati</option>
                                        <option value="XiTsonga"{{$incubatee->home_language=='XiTsonga'?'selected':''}}>XiTsonga</option>
                                        <option value="SeSotho"{{$incubatee->home_language=='SeSotho'?'selected':''}}>SeSotho</option>
                                    @endif
                                </select>
                                <label>Home Language</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="residence">
                                    @if($incubatee->residence== null)
                                        <option>Select residance</option>
                                    @else
                                        <option value="Urban"{{$incubatee->residence=='Urban'?'selected':''}}>Urban</option>
                                        <option value="Peri-Urban"{{$incubatee->residence=='Peri-Urban'?'selected':''}}>Peri-Urban</option>
                                        <option value="Rural"{{$incubatee->residence=='Rural'?'selected':''}}>Rural</option>
                                    @endif
                                </select>
                                <label>Resides</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_facebook" type="text" value="{{$incubatee->personal_facebook_url}}"
                                       class="validate">
                                <label for="personal_facebook">Facebook</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_linked_in" type="text" value="{{$incubatee->personal_linkedIn_url}}"
                                       class="validate">
                                <label for="personal_linked_in">LinkedIn</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_twitter" type="text" value="{{$incubatee->personal_twitter_url}}"
                                       class="validate">
                                <label for="personal_twitter">Twitter</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_instagram" type="text"
                                       value="{{$incubatee->personal_instagram_url}}" class="validate">
                                <label for="personal_instagram">Instagram</label>
                            </div>
                            <div class="input-field col m6">
                                <textarea id="short_bio" class="materialize-textarea" data-length="120">{{$incubatee->short_bio}}</textarea>
                                <label for="short_bio">About Me</label>
                            </div>
                        </div>
                        <div class="input-field col m6">
                            <select id="status">
                                <option value="Active"{{$incubatee->status=='Active'?'selected':''}}>Active</option>
                                <option value="Alumni"{{$incubatee->status=='Alumni'?'selected':''}}>Alumni</option>
                                <option value="Exit"{{$incubatee->status=='Exit'?'selected':''}}>Exit</option>
                                <option value="Resigned"{{$incubatee->status=='Resigned'?'selected':''}}>Resigned</option>
                                <option value="On Hold"{{$incubatee->status=='On Hold'?'selected':''}}>On Hold</option>
                            </select>
                            <label>Status</label>
                        </div>



                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-personal-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>

                    @if(is_null($incubatee->venture_id))
                        <div class="fixed-action-btn" id="float-create-venture">
                            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
                               data-tooltip="Add New Venture"
                               href="{{url('/incubatee-create-venture/'. $incubatee->id)}}">
                                <i class="large material-icons">add</i>
                            </a>
                        </div>
                    @else
                        <div class="fixed-action-btn" id="float-create-venture">
                            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
                               data-tooltip="Add New Venture"
                               href="{{url('/incubatee-create-venture/'. $incubatee->id)}}">
                                <i class="large material-icons">add</i>
                            </a>
                        </div>
                    @endif

                </div>
                <div id="test3" class="col s12">
                    <form id="incubatee-media-update-form" class="col s12" style="margin-top:1em;" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <input value="{{$current_user->id}}" id="incubatee_id" hidden>
                        {{--Profile Picture--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Profile Picture</span>
                                        <input id="profile_picture_url" type="file" name="profile_picture_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->profile_picture_url)?$incubatee_uploads->profile_picture_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--ID and POA--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>ID Document</span>
                                        <input id="id_document" type="file" name="id_document">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->id_document)?$incubatee_uploads->id_document:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Proof of address</span>
                                        <input id="proof_of_address" type="file" name="proof_of_address">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->proof_of_address)?$incubatee_uploads->proof_of_address:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- DISC assessment--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>DISC assessment</span>
                                        <input id="disc_assessment_report" type="file" name="disc_assessment_report">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->disc_assessment_report)?$incubatee_uploads->disc_assessment_report:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col m6 triple-input">
                                <input id="disk_results" type="text" value="{{$incubatee_uploads->disk_results}}" class="validate">
                                <label for="disk_results">Disk assessment results</label>
                            </div>
                            <div class="row center" style="border: 1px solid black;">
                                <div class="col l4 input-field" style="margin-left: 800px">
                                    <button class="btn blue " id="disc_results_upload_btn">Save</button>
                                </div>
                            </div>
                        </div>
                        {{--Incubatee strength--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Incubatee strength</span>
                                        <input id="strength_upload" type="file" name="strength_upload">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->strength_upload)?$incubatee_uploads->strength_upload:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col m6 triple-input">
                                <input id="strength_results" type="text" value="{{$incubatee_uploads->strength_results}}" class="validate">
                                <label for="strength_results">Strengths</label>
                            </div>
                            <div class="row center" style="border: 1px solid black;">
                                <div class="col l4 input-field" style="margin-left: 800px">
                                    <button class="btn blue " id="strength_results_upload_btn">Save</button>
                                </div>
                            </div>
                        </div>
                        {{-- Talent dynamic upload--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Talent dynamic upload</span>
                                        <input id="talent_dynamics_upload" type="file" name="talent_dynamics_upload">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->talent_dynamics_upload)?$incubatee_uploads->talent_dynamics_upload:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col m6 triple-input">
                                <input id="talent_results" type="text" value="{{$incubatee_uploads->talent_results}}" class="validate">
                                <label for="talent_results">Talent dynamic</label>
                            </div>
                            <div class="row center" style="border: 1px solid black;">
                                <div class="col l4 input-field" style="margin-left: 800px">
                                    <button class="btn blue " id="talent_results_upload_btn">Save</button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m6">
                                @if(isset($incubatee_uploads->stage_2_pitch_video))
                                    <input id="stage_2_pitch_video" type="text" value="{{$incubatee_uploads->stage_2_pitch_video}}" class="validate">
                                    <label for="stage_2_pitch_video">STAGE 2 PITCH VIDEO</label>
                                @else
                                    <input id="stage_2_pitch_video" type="text"  class="validate">
                                    <label for="stage_2_pitch_video">STAGE 2 PITCH VIDEO</label>
                                @endif
                            </div>
                            <div class="input-field col m6">
                                @if(isset($incubatee_uploads->stage_2_pitch_video_title))
                                    <input id="stage_2_pitch_video_title" type="text" value="{{$incubatee_uploads->stage_2_pitch_video_title}}" class="validate">
                                    <label for="stage_2_pitch_video_title">STAGE 2 PITCH VIDEO TITLE</label>
                                @else
                                    <input id="stage_2_pitch_video_title" type="text"  class="validate">
                                    <label for="stage_2_pitch_video_title">STAGE 2 PITCH VIDEO TITLE</label>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                @if(isset($incubatee_uploads->founder_video_shot))
                                    <input id="founder_video_shot" type="text" value="{{$incubatee_uploads->founder_video_shot}}" class="validate">
                                    <label for="founder_video_shot">FOUNDERS VIDEO</label>
                                @else
                                    <input id="founder_video_shot" type="text"  class="validate">
                                    <label for="founder_video_shot">FOUNDERS VIDEO</label>
                                @endif
                            </div>
                            <div class="input-field col m6">
                                @if(isset($incubatee_uploads->founder_video_title))
                                    <input id="founder_video_title" type="text" value="{{$incubatee_uploads->founder_video_title}}" class="validate">
                                    <label for="founder_video_title">FOUNDER VIDEO TITLE</label>
                                @else
                                    <input id="founder_video_title" type="text"  class="validate">
                                    <label for="founder_video_title">FOUNDER VIDEO TITLE</label>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-media" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <style>
        .tabs .tab a:hover, .tabs .tab a.active {
            color: black !important;
            font-weight: bolder;
        }

        nav {
            margin-bottom: 0;
            background-color: grey;
        }
        .tabs .tab a {
            color: black !important;
        }
    </style>
    @push('custom-scripts')
        <script>
            $(document).ready(function () {
                $('#disc_results_upload_btn').on('click', function(e){
                    e.preventDefault();
                    let formData = new FormData();

                    formData.append('disk_results', $('#disk_results').val());

                    let url = '/store-disk-results/' + '{{$current_user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            $('#disc_results_upload_btn').notify(response.message, "success");

                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            $('#disc_results_upload_btn').notify(response.message, "error");
                        }
                    });
                });
                $('#strength_results_upload_btn').on('click', function(e){
                    e.preventDefault();
                    let formData = new FormData();

                    formData.append('strength_results', $('#strength_results').val());

                    let url = '/store-strength-results/' + '{{$current_user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            $('#strength_results_upload_btn').notify(response.message, "success");

                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            $('#strength_results_upload_btn').notify(response.message, "error");
                        }
                    });
                });
                $('#talent_results_upload_btn').on('click', function(e){
                    e.preventDefault();
                    let formData = new FormData();

                    formData.append('talent_results', $('#talent_results').val());

                    let url = '/store-talent-dynamics-results/' + '{{$current_user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            $('#talent_results_upload_btn').notify(response.message, "success");

                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            $('#talent_results_upload_btn').notify(response.message, "error");
                        }
                    });
                });


                $("select").formSelect();
                $('.tooltipped').tooltip();

                //Style block where profile picture is loaded
                function profilePictureIsLoaded(e) {
                    $("#profile_picture_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#profile-picture-preview').attr('src', e.target.result);
                    $('#profile-picture-preview').attr('width', '250px');
                    $('#profile-picture-preview').attr('height', '230px');
                }

                //Style block where company logo is loaded
                function companyLogoIsLoaded(e) {
                    $("#company_logo_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#company-logo-preview').attr('src', e.target.result);
                    $('#company-logo-preview').attr('width', '250px');
                    $('#company-logo-preview').attr('height', '230px');
                }

                //Style block where video shot is loaded
                function videoShotIsLoaded(e) {
                    $("#video-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#video-shot-preview').attr('src', e.target.result);
                    $('#video-shot-preview').attr('width', '250px');
                    $('#video-shot-preview').attr('height', '230px');
                }

                //Style block where infographic is loaded
                function infographicIsLoaded(e) {
                    $("#infographic_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#infographic-preview').attr('src', e.target.result);
                    $('#infographic-preview').attr('width', '250px');
                    $('#infographic-preview').attr('height', '230px');
                }

                //Style block where crowdfunding is loaded
                function crowdfundingIsLoaded(e) {
                    $("#crowdfunding_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#crowdfunding-preview').attr('src', e.target.result);
                    $('#crowdfunding-preview').attr('width', '250px');
                    $('#crowdfunding-preview').attr('height', '230px');
                }

                //Style block where product shot is loaded
                function productShotIsLoaded(e) {
                    $("#product-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#product-shot-preview').attr('src', e.target.result);
                    $('#product-shot-preview').attr('width', '250px');
                    $('#product-shot-preview').attr('height', '230px');
                }

                // Function to preview profile picture after validation
                $(function () {
                    $("#profile_picture_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#profile-picture-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = profilePictureIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview company logo after validation
                $(function () {
                    $("#company_logo_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#company-logo-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = companyLogoIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview video shot after validation
                $(function () {
                    $("#video-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#video-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = videoShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview infographic after validation
                $(function () {
                    $("#infographic_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#infographic-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = infographicIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview crowdfunding after validation
                $(function () {
                    $("#crowdfunding_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#crowdfunding-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = crowdfundingIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview product shot after validation
                $(function () {
                    $("#product-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#product-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = productShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });
                let value = $("#dob").val();
                let dob = new Date(value);
                let today = new Date();
                let age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                if (isNaN(age)) {
                    // will set 0 when value will be NaN
                    age = 0;
                } else {
                    age = age;
                }
                let ages = $('#age').val(age);
                $("#dob").change(function () {
                    let value = $("#dob").val();
                    let dob = new Date(value);
                    let today = new Date();
                    let age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                    if (isNaN(age)) {
                        // will set 0 when value will be NaN
                        age = 0;
                    } else {
                        age = age;
                    }
                    let ages = $('#age').val(age);
                });

                $('#incubatee-details-update-form').on('submit', function (e) {
                    e.preventDefault();
                    let formData = new FormData();
                    formData.append('title', $('#title').val());
                    formData.append('initials', $('#initials').val());
                    formData.append('dob', $('#dob').val());
                    formData.append('age', $('#age').val());
                    formData.append('gender', $('#gender').val());
                    formData.append('id_number', $('#id_number').val());
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('email', $('#email').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('personal_facebook_url', $('#personal_facebook').val());
                    formData.append('personal_linkedIn_url', $('#personal_linked_in').val());
                    formData.append('personal_twitter_url', $('#personal_twitter').val());
                    formData.append('personal_instagram_url', $('#personal_instagram').val());
                    formData.append('address_one', $('#address_one').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    formData.append('city', $('#city').val());
                    formData.append('code', $('#code').val());
                    formData.append('nmu_graduate', $('#nmu_graduate').val());
                    formData.append('disabled', $('#disabled').val());
                    formData.append('residence', $('#residence').val());
                    formData.append('home_language', $('#home_language').val());
                    formData.append('short_bio', $('#short_bio').val());
                    formData.append('status', $('#status').val());

                    /*jQuery.each(jQuery('#id_document')[0].files, function (i, file) {
                        formData.append('id_document', file);
                    });*/

                    console.log("user ", formData);


                    let url = '/incubatee-update-personal/' + '{{$current_user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        /**
                         * @param response
                         * @param a
                         * @param b
                         */

                        success: function (response, a, b) {
                            $("#save-incubatee-personal-details").notify(
                                "You have successfully Update Incubatee", "success",
                                { position:"right" }
                            );
                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });


                $('#incubatee-media-update-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();
                    formData.append('file_description', $('#file_description').val());
                    formData.append('stage_2_pitch_video', $('#stage_2_pitch_video').val());
                    formData.append('founder_video_shot', $('#founder_video_shot').val());
                    formData.append('stage_2_pitch_video_title', $('#stage_2_pitch_video_title').val());
                    formData.append('founder_video_title', $('#founder_video_title').val());

                    jQuery.each(jQuery('#profile_picture_url')[0].files, function (i, file) {
                        formData.append('profile_picture_url', file);
                    });

                    jQuery.each(jQuery('#id_document')[0].files, function (i, file) {
                        formData.append('id_document', file);
                    });

                    jQuery.each(jQuery('#proof_of_address')[0].files, function (i, file) {
                        formData.append('proof_of_address', file);
                    });

                    jQuery.each(jQuery('#disc_assessment_report')[0].files, function (i, file) {
                        formData.append('disc_assessment_report', file);
                    });
                    jQuery.each(jQuery('#strength_upload')[0].files, function (i, file) {
                        formData.append('strength_upload', file);
                    });
                    jQuery.each(jQuery('#talent_dynamics_upload')[0].files, function (i, file) {
                        formData.append('talent_dynamics_upload', file);
                    });

                    let url = '/incubatee-update-media/' + '{{$current_user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            $("#save-incubatee-media").notify(
                                "You have successfully Updated Incubatee Media", "success",
                                { position:"right" }
                            );
                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                            sessionStorage.clear();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection

