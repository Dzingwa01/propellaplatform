@extends('layouts.advisor-layout')
@section('content')


    <div class="row" id="applicant-contact-log" style="margin-right: 3em; margin-left: 3em;">
        <input id="venture-id-input" value="{{$venture->id}}" hidden disabled>
        <br>
        <div class="row center">
            <h4><b>CONTACT LOG</b></h4>
        </div>
        <div class="row">
            <div class="col s4">
                <h5 style="color: grey;">Contact log history</h5>
                <br>
                @foreach($venture->contactLog->sortByDesc('date') as $user_log)
                    <p><b>Results of contact</b> : {{$user_log->venture_contact_results}}</p>
                    @if($user_log->comment != null)
                        <p><b>Comment</b> : {{$user_log->comment}}</p>
                    @else
                    @endif
                    @if($user_log->comment_email != null)
                        <p><b>Comment</b> : {{$user_log->comment_email}}</p>
                    @else
                    @endif
                    <p><b>Date</b>:{{$user_log->date}}</p>
                    <hr>
                @endforeach
            </div>
            <div class="col s1"></div>
            <div class="col s7">
                <h5 style="color: grey;margin-left: 4.5em">Create Venture contact log</h5>
                <br>
                <div class="row" style="margin-left: 7em">
                    <div class="input-field col s10">
                        <select id="venture_contact_results">
                            <option value="Phone Call">Phone Call</option>
                            <option value="Face-to-Face">Face-to-Face</option>
                            <option value="Email">Email</option>
                            <option value="No response to any contact type">No response to any contact type
                            </option>
                            <option value="Invited, but did not arrive">Invited, but did not arrive</option>
                        </select>
                        <label>You contacted {{$venture->company_name}} via</label>
                    </div>
                </div>
                <div class="row" id="comment-section" style="margin-left: 7em">
                    <div class="input-field col s10">
                        <input id="comment" type="text" class="validate">
                        <label for="comment">Comment</label>
                    </div>
                </div>
                <div class="row" id="comment-emailer" hidden style="margin-left: 7em">
                    <div class="input-field col s10">
                        <select id="comment_email">
                            <option value="A reminder to submit your latest M&E information for reporting purposes by close of business today.">A reminder to submit your latest M&E information for reporting purposes by close of business today.</option>
                            <option value="Your M&E information is outstanding.  Please complete urgently on your Propella Platform profile.">Your M&E information is outstanding.  Please complete urgently on your Propella Platform profile.</option>
                            <option value="Your M&E spreadsheets are not up to date, please complete URGENTLY.">Your M&E spreadsheets are not up to date, please complete URGENTLY.</option>
                        </select>
                        <label>Send email to  {{$venture->company_name}}</label>
                    </div>
                </div>
                <div class="row" style="margin-left: 300px;">
                    <div class="col s6 save-contact-log" hidden>
                        <a class="waves-effect waves-light btn" id="contact-log-submit-button">Save contact log</a>
                    </div>
                    <div class="col s6 send-email-to-venture" hidden>
                        <a class="waves-effect waves-light btn" id="send-email">Send email</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#venture_contact_results').on('change', function(){
                if(this.value === "Email"){
                    $('.save-contact-log').hide();
                    $('.send-email-to-venture').show();
                    $('#comment-section').hide();
                    $('#comment-emailer').show();
                }else{
                    $('.save-contact-log').show();
                    $('.send-email-to-venture').hide();
                    $('#comment-emailer').hide();
                    $('#comment-section').show();
                }
            });
            $('select').formSelect();
            $('#contact-log-submit-button').on('click', function () {
                let venture_id = $('#venture-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('venture_contact_results', $('#venture_contact_results').val());

                let url = '/store-venture-contact-log/' + venture_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#contact-log-submit-button').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/venture-contact-log/' + venture_id;
                        }, 3000);
                    },
                });
            });
            //Send email to ventures
            $('#send-email').on('click',function () {
                let venture_id = $('#venture-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('venture_contact_results', $('#venture_contact_results').val());
                formData.append('comment_email', $('#comment_email').val());

                let url = '/store-venture-contact-log/' + venture_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#send-email').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/venture-contact-log/' + venture_id;
                        }, 3000);
                    },
                });
            });
        });
    </script>

@endsection
