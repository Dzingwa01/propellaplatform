@extends('layouts.advisor-layout')

@section('content')
    <!-- If the applicant panelists are set -->
    <br>
    <br>{{ Breadcrumbs::render('admin-prop-ventures-panel')}}
    <div class="" style="margin-top: 10vh">

        <div class="container-fluid" style="margin-left: 5em; margin-right: 5em;">
            <input id="venture-id-input" value="{{$venture->id}}" hidden disabled>
            <div class="row center">
                <div class="col l6 m6 s12">
                    <select id="venture-panel-interview-category-input">
                        <option value="" disabled selected>Choose Panel Interview Category</option>
                        @foreach($venturePanelInterviewCategories as $item)
                            <option value="{{$item->id}}">{{$item->category_name}}</option>
                        @endforeach
                    </select>
                    <label for="venture-panel-interview-category-input">Panel Interview Category</label>
                </div>
                <div class="col l6 m6 s12">
                    <select id="question-category-input">
                        <option value="" disabled selected>Choose Questions</option>
                        @foreach($question_categories as $question_category)
                            <option
                                value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                        @endforeach
                    </select>
                    <label for="question-category-input">Questions Category</label>
                </div>
            </div>


            <div class="section" id="panel-interview-row">
                <div class="row">
                    <div class="col l6 m6 s12">
                        <select id="panelist-input" multiple="multiple">
                            <option value="" disabled selected>Panelists?</option>
                            <option value="grant@propellaincubator.co.za">Grant Minnie</option>
                            <option value="anita@propellaincubator.co.za">Anita Palmer</option>
                            <option value="woosthuizen@engeli.co.za">Wayne Oosthuizen</option>
                            <option value="bwiseman@engeli.co.za">Barry Wiseman</option>
                            <option value="rdames@engeli.co.za">Ricardo Dames</option>
                            <option value="errol@propellaincubator.co.za">Error Wills</option>
                            <option value="linda1@propellaincubator.co.za">Linda Lawrie</option>
                            <option value="daryl.mcwilliams@gmail.com">Daryl McWilliams</option>
                            <option value="Nqobile.Gumede@mandela.ac.za">Nqobile Gumede</option>
                            <option value="Siyanda.Msithini@mandela.ac.za">Siyanda Msithini</option>
                            <option value="nafeesa@propellaincubator.co.za">Nafeesa Dinie</option>
                        </select>

                        <label for="panel-selection-date-input">Date of panel interview</label>
                        <input type="date" id="panel-selection-date-input">

                        <label for="panel-selection-time-input">Time of panel interview</label>
                        <input type="time" id="panel-selection-time-input">
                    </div>
                    <div class="col l6 m6 s12">
                        <button class="btn blue" id="show-guest-panelist-button">Add Guest Panelist</button>
                        <div class="container" id="guest-panelist-container" hidden>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <select id="title-input">
                                        <option value="" disabled selected>Choose Title</option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Dr">Dr</option>
                                    </select>
                                    <label>Title</label>
                                </div>
                                <div class="col l6 m6 s12">
                                    <input id="initial-input" name="initial-input" type="text" class="validate">
                                    <label for="initial-input">Initials</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <input id="name-input" name="name-input" type="text" class="validate">
                                    <label for="name-input">First Name</label>
                                </div>
                                <div class="col l6 m6 s12">
                                    <input id="surname-input" name="surname-input" type="text" class="validate">
                                    <label for="surname-input">Last Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <input id="company-input" name="company-input" type="text" class="validate">
                                    <label for="company-input">Company Name</label>
                                </div>
                                <div class="col l6 m6 s12">
                                    <input id="position-input" name="position-input" type="text" class="validate">
                                    <label for="position-input">Position</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <input id="email-input" name="email-input" type="email" class="validate">
                                    <label for="email-input">Email</label>
                                </div>
                                <div class="col l6 m6 s12">
                                    <input id="contact-number-input" name="contact-number-input" type="text"
                                           class="validate">
                                    <label for="contact-number-input">Contact Number</label>
                                </div>
                            </div>
                            <div class="row">
                                <button class="btn blue" id="add-guest-panelist-button">Add</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <button class="btn blue" id="panel-interview-button">Finalize</button>
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid" style="margin-left: 5em; margin-right: 5em;">
        </div>
    </div>
    <div class="container">
        <table class="table table-hover" id="feederDatabaseTable">
            <thead>
            <tr>
                <th scope="col">Panel selection date</th>
                <th scope="col">Panel selection time</th>
                <th scope="col">Interview Category</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($venture_panel_array as $venture_panel)
                <tr>
                    <td>{{$venture_panel->panel_selection_date}}</td>
                    <td>{{$venture_panel->panel_selection_time}}</td>
                    <td>{{$venture_panel->category_name}}</td>
                    <td>
                        <button type="button" class="btn btn-primary" id="{{$venture_panel->id}}" onclick="UpdatePanel(this)">EDIT</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
    <script>
        function UpdatePanel(obj) {
            let venture_panel_interview_id = obj.id;
            window.location.href = '/edit-venture-panel-interview/' + venture_panel_interview_id;
        }
        let venture_id = $('#venture-id-input').val();
        $(document).ready(function () {
            $('select').formSelect();
        });


        $('#show-guest-panelist-button').on('click', function () {
            $('#show-guest-panelist-button').hide();
            $('#guest-panelist-container').show();
        });

        //Submit Panel Interview Buttons

        //Approve Applicant
        let guest_panelists = [];

        $('#add-guest-panelist-button').on('click', function () {
            let title = $('#title-input').val();
            let initials = $('#initial-input').val();
            let name = $('#name-input').val();
            let surname = $('#surname-input').val();
            let position = $('#position-input').val();
            let company = $('#company-input').val();
            let email = $('#email-input').val();
            let contact_number = $('#contact-number-input').val();

            let object = {
                title: title,
                initials: initials,
                name: name,
                surname: surname,
                position: position,
                company: company,
                email: email,
                contact_number: contact_number
            };

            guest_panelists.push(object);

            $('#title-input').val("");
            $('#initial-input').val("");
            $('#name-input').val("");
            $('#surname-input').val("");
            $('#position-input').val("");
            $('#company-input').val("");
            $('#email-input').val("");
            $('#contact-number-input').val("");
        });

        $('#panel-interview-button').on('click', function () {
            if ($('#venture-panel-interview-category-input').val() === null || $('#question-category-input').val() === null) {
                alert('Please select interview category AND question category');
            } else {
                $(this).text("Loading...");
                let selected_panelists = [];
                let formData = new FormData();

                jQuery.each(jQuery('#panelist-input').val(), function (i, value) {
                    selected_panelists.push(value);
                });

                formData.append('selected_panelists', JSON.stringify(selected_panelists));
                formData.append('guest_panelists', JSON.stringify(guest_panelists));
                formData.append('panel_selection_date', $('#panel-selection-date-input').val());
                formData.append('panel_selection_time', $('#panel-selection-time-input').val());
                formData.append('question_category_id', $('#question-category-input').val());
                formData.append('venture_panel_interview_category_id', $('#venture-panel-interview-category-input').val());

                let url = "/admin-create-venture-panel-interview/" + venture_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#panel-interview-button').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/propella-ventures';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#panel-interview-button').notify(response.message, "error");
                    }
                });
            }
        });

    </script>
@endsection
