@extends('layouts.advisor-layout')
@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-evaluations')}}
    <div class="container-fluid" style="margin-top: 2em;">
        <br>
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">WORKSHOP EVALUATION FORMS</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="workshop_evaluations-table">
                    <thead>
                    <tr>
                        <th>Workshop Title</th>
                        <th>Date</th>
                        <th>Venue</th>
                        <th>Facilitator</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#workshop_evaluations-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-workshop')}}',
                    columns: [
                        {data: 'title', name: 'title'},
                        {data: 'date', name: 'date'},
                        {data: 'venue', name: 'venue'},
                        {data: 'facilitator', name: 'facilitator'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                $('select[name="workshop_evaluations-table_length"]').css("display","inline");
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
