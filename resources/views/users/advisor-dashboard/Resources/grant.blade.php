@extends('layouts.advisor-layout')

@section('content')

    <br>
    <br>
    {{ Breadcrumbs::render('advisor-grant')}}
    <div class="row" style="margin-left: 2em;margin-right: 2em">
        <br>
        <p style="margin-left: 20em;font-size: 2em">{{$resourceCategory->category_name}}</p>
        @foreach($resourceCategory->fundingResource as $funding)
            @if ($funding->closing_date < $date)
                <div class=""></div>
            @else
                <div class="col s12 m4">
                    <div class=" card funding" style="top: 5vh;height: 50vh; cursor: pointer">
                        <div class="col s1"></div>
                        <div class="card-image">
                            @if($funding->image_url != null)
                                <img style="width: 100%; object-fit: cover; height: 15vw"
                                     src="/storage/{{isset($funding->image_url)?$funding->image_url:'Nothing Detected'}}">
                            @endif
                        </div>
                        <div class="card-content">
                            <p style="margin-left: 1em">{{$funding->title}}</p>
                            <p style="margin-left: 1em">Closing Date : {{$funding->closing_date}}</p>
                        </div>
                        <input hidden disabled class="funding_id" data-value="{{$funding->id}}">
                    </div>
                </div>
            @endif
        @endforeach
    </div>

    <script>

        $('.funding').each(function () {
            let funding_id = $(this).find('.funding_id').attr('data-value');

            $(this).on('click', function () {
                location.href = '/show-all-info/' + funding_id;
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection

