@extends('layouts.advisor-layout')

@section('content')
    <br>
    <div class="card" style="width: 1300px;margin: 0 auto;top: 8vh">
        <div class="row" style="margin-left: 4em;margin-right: 4em;">
            <br>
            <div class="row">
                <div class="col s8">
                    <h4>{{$fundingResource->title}}</h4>
                </div>
                <div class="col s4">
                    <p>Closing date : {{$fundingResource->closing_date}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col s8">
                    <p>{!!$fundingResource->resourceSummernote->content!!}</p>
                </div>
                <div class="col s1"></div>
                <div class="col s4">
                    @if($fundingResource->image_url != null)
                        <img style="width: 400px;height: 50vh"
                             src="/storage/{{isset($fundingResource->image_url)?$fundingResource->image_url:'Nothing Detected'}}">
                    @endif
                </div>
            </div>
        </div>
        @foreach($fundingResource->fundingResourceDocuments as $upload)
            <a style="margin-left: 4em" href="{{'/download-file/'.$upload->id}}">Download</a>
        @endforeach
        <br>
        <br>
    </div>



@endsection
