@extends('layouts.advisor-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-funding-edit')}}
    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <br>
        @csrf
        <input value="{{$resourceCategory->id}}" id="category_id" hidden>
        <h5 style="margin-left: 40%">Update role</h5>
        <br>
        <div class="input-field col s2" style="margin-left: 2em;margin-right: 2em;">
            <input id="category_name" type="text" value={{$resourceCategory->category_name}} class="validate">
            <label for="category_name">Category name</label>
        </div>

        <br>
        <button class="btn waves-effect waves-light" style="margin-left:500px;" id="save-category" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
        <br>
        <br>
    </div>

    <script>
        $(document).ready(function () {
            $('#save-category').on('click',function () {

                let formData = new FormData();
                formData.append('category_name', $('#category_name').val());

                console.log(formData);

                let url = '/updateCategory/' + $('#category_id').val();
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    @endsection
