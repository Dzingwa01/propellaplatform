@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-get-venues')}}
    <br>
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Bookings</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="blog-table">
                    <thead>
                    <tr>
                        <th>Venue Name</th>
                        <th>Number of Seats</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>


    </div>
    @push('custom-scripts')

        <script>

            $(document).ready(function (data) {
                $(function () {
                    $('#blog-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '/get-all-venues',
                        columns: [
                            {data: 'venue_name', name: 'venue_name'},
                            {data: 'number_of_seats', name: 'number_of_seats'},
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="blog-table_length"]').css("display","inline");
                });
            });

            function confirm_delete_booking(obj){
                var r = confirm("Are you sure want to delete this Venue?");
                if (r == true) {
                    $.get('/venue-delete-check/'+obj.id,function(data,status){
                        console.log('Data',data);
                        console.log('Status',status);
                        if(status=='success'){
                            $('#blog-table').notify(response.message, "success");
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }

            }
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
