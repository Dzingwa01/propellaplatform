@extends('layouts.advisor-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <br>
    <br>

    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <form class="col s12 " id="edit-video" method="post">
            <br>
            @csrf
            <input value="{{$video->id}}" id="video_id" hidden>

            <p class="center-align"><b>UPLOAD VIDEO</b></p>
            <div class="row" style="margin-left: 2em;margin-right: 2em">
                <div class="col m6">
                    <div class="input-field" style="bottom:0px!important;">
                        <input id="video" type="text" value="{{$video->video}}">
                        <label for="video">Video url</label>
                    </div>
                </div>
                <div class="input-field col m6">
                    <input id="video_title" type="text" class="validate" value="{{$video->video_title}}">
                    <label for="video_title">Video Title</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em">
                <div class="col m6">
                    <div class="input-field" style="bottom:0px!important;">
                        <input id="date" type="date" value="{{$video->date}}">
                        <label for="date">Date</label>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 400px">
                <div class="col s4">
                    <a class="waves-effect waves-light btn section" id="edit-video-upload">Save</a>
                </div>
            </div>
            <br>

        </form>
    </div>

    <script>
        $(document).ready(function () {
            $('#edit-video-upload').on('click',function () {
                let formData = new FormData();

                formData.append('video_id', $('#video_id').val());
                formData.append('video', $('#video').val());
                formData.append('video_title', $('#video_title').val());
                formData.append('date', $('#date').val());

                console.log(formData);

                let url = '/updateVideo/'+ '{{$video->id}}';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#edit-video-upload').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/video-index';
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });

            });
        });

    </script>

@endsection
