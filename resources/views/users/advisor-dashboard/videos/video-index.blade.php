@extends('layouts.advisor-layout')

@section('content')
    <div class="container-fluid">
        <br>
        <br>

        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Propella Videos</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="videos-table">
                    <thead>
                    <tr>
                        <th>Video url</th>
                        <th>video title</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New video" href="{{url('/upload-videos')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>
    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#videos-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-videos')}}',
                        columns: [
                            {data: 'video', name: 'video'},
                            {data: 'video_title', name: 'video_title'},
                            {data: 'date', name: 'date'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="videos-table_length"]').css("display","inline");
                });

            });

        </script>
    @endpush
@endsection
