
@extends('layouts.advisor-layout')

@section('content')
    <br>
<br>
{{ Breadcrumbs::render('advisor-venture-emailer')}}
    <div class="container-fluid">
        <div id="top-section">
            <div class="row " >
                <div style="float: right; margin-top:4em;margin-right: 2em;" >
                    <span id="users-selected-count">No recipients selected</span>
                    <button class="btn waves-effect waves-light"  disabled style="margin-right:2em;" id="send-mail">Send Email
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Ventures</h6>
                <div class="col s12">
                    <table class="table table-bordered" style="width: 100%!important;" id="incubatees-table">
                        <thead>
                        <tr>
                            <th>
                                <button type="button" id="selectAll" class="main">
                                    <span class="sub"></span> Select </button></th>
                            <th>Venture Name</th>
                            <th>Email</th>
                            <th>Hub</th>
                            <th>Stage</th>
                            <th>Status</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div id="bottom-section" hidden  >
            <h6 style="text-transform: uppercase;font-weight: bolder;margin-top:2em;text-align: center;">Mail Send</h6>
            <div class="row" >
                <form id="send-bulk-email-form" class="col s12 card hoverable" style=" margin-right:2em;margin-right: 2em;">
                    @csrf
                    <div class="row">
                        <div class="input-field col m12" style=" margin-right: 2em!important;;">
                            <i class="material-icons prefix">people_outline</i>
                            <textarea id="selected-recipients" class="materialize-textarea"></textarea>
                            {{--<label for="selected-recipients">Selected Recipients</label>--}}
                        </div>

                    </div>
                    <div class="row">
                        <div class="input-field col m12" style="margin-right: 2em!important;;">
                            <i class="material-icons prefix">mode_edit</i>
                            <input id="email-subject" type="text" class="validate">
                            <label for="email-subject">Email Subject</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field" style="bottom:0px!important;margin-left: 1em;margin-right: 2em;">
                            <div class="btn">
                                <span>Attachments</span>
                                <input id="attachments" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                            <span id="message"></span>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 1em;margin-right: 2em;">
                        <label for="editor">Message</label>
                        <div id="editor" >

                        </div>

                        {{--<textarea style="margin-left:1em;" id="summernote" name="message" class="materialize-textarea"></textarea>--}}
                    </div>
                    <div class="row" style="margin-top:2em;">
                        <div class="col offset-m4">
                            <a id="cancel-send" class="waves-effect waves-green btn modal-close">Cancel<i class="material-icons right">close</i> </a>
                            <button class="btn waves-effect waves-light" style="margin-left:2em;" id="send-bulk-email" name="action">Send
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <style>
            .check {
                opacity: 1 !important;
                pointer-events: auto !important;
            }

            table.dataTable tbody tr.selected {
                background-color: #B0BED9;
            }
        </style>

    </div>
    @push('custom-scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.14.2/highlight.min.js"></script>
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>


        <script>
            $(document).ready(function () {
                $('select').formSelect();
                var toolbarOptions = [
                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                    ['blockquote', 'code-block'],

                    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
                    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                    [{ 'direction': 'rtl' }],                         // text direction

                    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                    [{ 'font': [] }],
                    [{ 'align': [] }],
                    ['image'],
                    ['clean']                                         // remove formatting button
                ];

                var quill = new Quill('#editor', {
                    modules: {
                        'history': {          // Enable with custom configurations
                            'delay': 2500,
                            'userOnly': true
                        },
                        'syntax':true,
                        toolbar: toolbarOptions
                    },
                    theme: 'snow'
                });
                $(function () {
                    $('#incubatees-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-allVentures')}}',
                        columns: [
                            {data: 'action', name: 'action', orderable: false, searchable: false},
                            {data:'venture.company_name',name:'venture.company_name'},
                            {data: 'user.email', name: 'user.email'},
                            {data: 'venture.hub', name: 'venture.hub'},
                            {data: 'venture.stage', name: 'venture.stage'},
                            {data: 'venture.status', name: 'venture.status'},

                        ]
                    });
                    $('select[name="incubatees-table_length"]').css("display","inline");
                });
                $('body').on('click', '#selectAll', function () {
                    if ($(this).hasClass('allChecked')) {
                        $('input[type="checkbox"]', '#incubatees-table').prop('checked', false);
                        $(this).toggleClass('selected');
                        count_selected();
                    } else {
                        $('input[type="checkbox"]', '#incubatees-table').prop('checked', true);
                        $(this).toggleClass('selected');
                        count_selected();
                    }
                    $(this).toggleClass('allChecked');
                });
                $('#incubatees-table tbody').on('click', 'tr', function () {
                    $(this).toggleClass('selected');
                    count_selected();
                });

                $('#send-mail').on('click',function(){
                    $('#top-section').hide();
                    $('#bottom-section').show();
                    $('#users-selected-count').empty();
                    let selected_users = $("table input:checkbox:checked").map(function () {
                        return $(this).val();
                    }).get();
                    console.log("Check selected users",selected_users);
                    let incubatees = {!! $incubatees !!};
                    let recipients ="";

                    for(let i=0;i<selected_users.length;i++){
                        let incubatee = incubatees.find(incubatee => incubatee.id === selected_users[i]);
                        console.log('Incubatee',incubatee.user.email);
                        recipients = recipients + incubatee.user.email +";";
                    }
                    $('#selected-recipients').val(recipients);
                    M.textareaAutoResize($('#selected-recipients'));
                });

                $('#cancel-send').on('click',function(){
                    $('#top-section').show();
                    $('#bottom-section').hide();
                });

                $('#send-bulk-email-form').on('submit',function(e){
                    e.preventDefault();
                    console.log("Check",$('#selected-recipients').val());
                    // alert(quill.getText());
                    if(!$('#selected-recipients').val().includes('All')){
                        let users = $("table input:checkbox:checked").map(function () {
                            return $(this).val();
                        }).get();

                        // let data = {selection_type:'multi',users:JSON.stringify(users),
                        //     message:quill.getText(),
                        //     subject:$('#email-subject').val()
                        // };
                        let formData = new FormData();
                        formData.append('selection_type', 'multi');
                        formData.append('users',JSON.stringify(users));
                        formData.append('message', quill.root.innerHTML);
                        formData.append('subject', $('#email-subject').val());

                        jQuery.each(jQuery('#attachments')[0].files, function (i, file) {
                            formData.append('attachment', file);
                        });
                        $.ajax({
                            url: "/send-bulk-email-to-ventures",
                            processData: false,
                            contentType: false,
                            data: formData,
                            type: 'post',
                            success: function (response, a, b) {
                                console.log("success", response);
                                alert(response.message);
                                window.location.reload();
                            },
                            error: function (response) {
                                console.log("error", response);
                                let message = response.responseJSON.message;
                                console.log("error", message);
                                let errors = response.responseJSON.errors;

                                for (var error in   errors) {
                                    console.log("error", error)
                                    if (errors.hasOwnProperty(error)) {
                                        message += errors[error] + "\n";
                                    }
                                }
                                alert(message);
                            }
                        });

                    }else{
                        let users = $("table input:checkbox:checked").map(function () {
                            return $(this).val();
                        }).get();
                        let data = {selection_type:'all',
                            message:$('#summernote').summernote('code'),
                            subject:$('#email-subject').val()};
                        console.log("data",data);
                        $.post('/send-bulk-email-to-ventures',data).done(function (data) {
                            console.log("Check hrere",data);
                            alert(data.message);
                            window.location.reload();
                        }).fail(function(reason){
                            console.log("reason",reason);
                            alert(reason.message);
                        });
                    }

                });
                $('#assign-stock').on('click', function () {
                    $('#selected-products').empty();
                    let selected_users = $("table input:checkbox:checked").map(function () {
                        return $(this).val();
                    }).get();
                    let visitors = {!! $incubatees !!};

                    for(var i=0;i<selected_users.length;i++){
                        let user = users.find(user => user.id == user[i].id);
                        console.log('User',user);
                        // $('#selected-products').append('<tr><td>'+product.barcode+'<td>'+product.product_name+' - '+ product.quantity+' Available</td><td> <input style="width:80px!important;" class="qty" min="0" max="'+product.quantity+'" required id="'+product.id+'" type="number" class="validate"/></td></tr>');
                    }

                });

                function count_selected() {
                    var count = $("table input[type=checkbox]:checked").length;
                    $('#users-selected-count').empty();
                    if (count <= 0) {
                        $('#send-mail').prop("disabled", true);
                        $('#users-selected-count').append(' No Recipients Selected');
                    } else {
                        $('#send-mail').prop("disabled", false);
                        $('#users-selected-count').append(count + ' Recipients Selected');
                    }
                }

                function saveToServer(file) {
                    const fd = new FormData();
                    fd.append('image', file);

                    $.ajax({
                        url: "/embed-image-for-venture",
                        processData: false,
                        contentType: false,
                        data: fd,
                        type: 'post',
                        success: function (response, a, b) {
                            console.log("success", response);
                            const url = response;
                            insertToEditor(url);
                            // window.location.reload();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            console.log("error", message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                }
                function selectLocalImage() {
                    const input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.click();

                    // Listen upload local image and save to server
                    input.onchange = () => {
                        const file = input.files[0];

                        // file type is only image.
                        if (/^image\//.test(file.type)) {
                            saveToServer(file);
                        } else {
                            console.warn('You could only upload images.');
                        }
                    };
                }
                function insertToEditor(url) {
                    // push image url to rich editor.
                    console.log("Checj url",url);
                    const range = quill.getSelection();
                    var getUrl = window.location;
                    var baseUrl = getUrl .protocol + "//" + getUrl.host;
                    var img_url = baseUrl+'/storage/'+url;

                    quill.insertEmbed(range.index, 'image', img_url);
                }
                quill.getModule('toolbar').addHandler('image', () => {
                    selectLocalImage();
                });
            });

        </script>
    @endpush
<style>
    nav {
        margin-bottom: 0;
        background-color: grey;
        padding: 5px 16px;
    }
</style>

@endsection



{{--@extends('layouts.admin-layout')--}}
{{--@section('content')--}}
{{--    <div class="container-fluid">--}}
{{--        <div id="top-section">--}}
{{--            <div class="row " >--}}
{{--                <div style="float: right; margin-top:4em;margin-right: 2em;" >--}}
{{--                    <span id="visitors-selected-count">No recipients selected</span>--}}
{{--                    <button class="btn waves-effect waves-light"  disabled style="margin-right:2em;" id="send-mail">Send Email--}}
{{--                        <i class="material-icons right">send</i>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row" style="margin-left: 2em;margin-right: 2em;">--}}
{{--                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Ventures</h6>--}}
{{--                <div class="col s12">--}}
{{--                    <table class="table table-bordered" style="width: 100%!important;" id="users-table">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>Select</th>--}}
{{--                            <th>Venture Name</th>--}}
{{--                            <th>Email</th>--}}
{{--                            <th>Hub</th>--}}
{{--                            <th>Stage</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}

{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="container">--}}
{{--        <div id="bottom-section" hidden  >--}}
{{--            <h6 style="text-transform: uppercase;font-weight: bolder;margin-top:2em;text-align: center;">Mail Send</h6>--}}
{{--            <div class="row" >--}}
{{--                <form id="send-bulk-email-form" class="col s12 card hoverable" style=" margin-right:2em;margin-right: 2em;">--}}
{{--                    @csrf--}}
{{--                    <div class="row">--}}
{{--                        <div class="input-field col m12" style=" margin-right: 2em!important;;">--}}
{{--                            <i class="material-icons prefix">people_outline</i>--}}
{{--                            <textarea id="selected-recipients" class="materialize-textarea"></textarea>--}}
{{--                            --}}{{--<label for="selected-recipients">Selected Recipients</label>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="row">--}}
{{--                        <div class="input-field col m12" style="margin-right: 2em!important;;">--}}
{{--                            <i class="material-icons prefix">mode_edit</i>--}}
{{--                            <input id="email-subject" type="text" class="validate">--}}
{{--                            <label for="email-subject">Email Subject</label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row">--}}
{{--                        <div class="file-field input-field" style="bottom:0px!important;margin-left: 1em;margin-right: 2em;">--}}
{{--                            <div class="btn">--}}
{{--                                <span>Attachments</span>--}}
{{--                                <input id="attachments" type="file">--}}
{{--                            </div>--}}
{{--                            <div class="file-path-wrapper">--}}
{{--                                <input class="file-path validate" type="text">--}}
{{--                            </div>--}}
{{--                            <span id="message"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row" style="margin-left: 1em;margin-right: 2em;">--}}
{{--                        <label for="editor">Message</label>--}}
{{--                        <div id="editor" >--}}

{{--                        </div>--}}

{{--                        --}}{{--<textarea style="margin-left:1em;" id="summernote" name="message" class="materialize-textarea"></textarea>--}}
{{--                    </div>--}}
{{--                    <div class="row" style="margin-top:2em;">--}}
{{--                        <div class="col offset-m4">--}}
{{--                            <a id="cancel-send" class="waves-effect waves-green btn modal-close">Cancel<i class="material-icons right">close</i> </a>--}}
{{--                            <button class="btn waves-effect waves-light" style="margin-left:2em;" id="send-bulk-email" name="action">Send--}}
{{--                                <i class="material-icons right">send</i>--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <style>--}}
{{--            .check {--}}
{{--                opacity: 1 !important;--}}
{{--                pointer-events: auto !important;--}}
{{--            }--}}

{{--            table.dataTable tbody tr.selected {--}}
{{--                background-color: #B0BED9;--}}
{{--            }--}}
{{--        </style>--}}

{{--    </div>--}}

{{--    @push('custom-scripts')--}}
{{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.14.2/highlight.min.js"></script>--}}
{{--        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">--}}
{{--        <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>--}}


{{--        <script>--}}
{{--            $(document).ready(function () {--}}
{{--                $('select').formSelect();--}}
{{--                var toolbarOptions = [--}}
{{--                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons--}}
{{--                    ['blockquote', 'code-block'],--}}

{{--                    [{ 'header': 1 }, { 'header': 2 }],               // custom button values--}}
{{--                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],--}}
{{--                    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript--}}
{{--                    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent--}}
{{--                    [{ 'direction': 'rtl' }],                         // text direction--}}

{{--                    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown--}}
{{--                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],--}}

{{--                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme--}}
{{--                    [{ 'font': [] }],--}}
{{--                    [{ 'align': [] }],--}}
{{--                    ['image'],--}}
{{--                    ['clean']                                         // remove formatting button--}}
{{--                ];--}}
{{--                var quill = new Quill('#editor', {--}}
{{--                    modules: {--}}
{{--                        'history': {          // Enable with custom configurations--}}
{{--                            'delay': 2500,--}}
{{--                            'userOnly': true--}}
{{--                        },--}}
{{--                        'syntax':true,--}}
{{--                        toolbar: toolbarOptions--}}
{{--                    },--}}
{{--                    theme: 'snow'--}}
{{--                });--}}
{{--                $(function () {--}}
{{--                    $('#users-table').DataTable({--}}
{{--                        processing: true,--}}
{{--                        serverSide: true,--}}
{{--                        paging: true,--}}
{{--                        responsive: true,--}}
{{--                        scrollX: 640,--}}
{{--                        ajax: '{{route('get-allVentures')}}',--}}

{{--                        columns: [--}}
{{--                            {data: 'action', name: 'action', orderable: false, searchable: false},--}}
{{--                            {data:'venture.company_name',name:'venture.company_name'},--}}
{{--                            {data: 'user.email', name: 'user.email'},--}}
{{--                            {data: 'venture.hub', name: 'venture.hub'},--}}
{{--                            {data: 'venture.stage', name: 'venture.stage'},--}}
{{--                        ],--}}

{{--                    });--}}
{{--                    $('select[name="users-table_length"]').css("display","inline");--}}
{{--                });--}}
{{--                $('#users-table tbody').on('click', 'tr', function () {--}}
{{--                    $(this).toggleClass('selected');--}}
{{--                    count_selected();--}}
{{--                });--}}

{{--                $('#send-mail').on('click',function(){--}}
{{--                    $('#top-section').hide();--}}
{{--                    $('#bottom-section').show();--}}
{{--                    $('#visitors-selected-count').empty();--}}
{{--                    let selected_visitors = $("table input:checkbox:checked").map(function () {--}}
{{--                        return $(this).val();--}}
{{--                    }).get();--}}
{{--                    console.log("Check selected visitors",selected_visitors);--}}
{{--                    let visitors = {!! $incubatees !!};--}}
{{--                    let recipients ="";--}}

{{--                    for(var i=0;i<selected_visitors.length;i++){--}}
{{--                        let visitor = visitors.find(visitor => visitor.id == selected_visitors[i]);--}}
{{--                        console.log('Visitor',visitor);--}}
{{--                        recipients = recipients + visitor.email +";";--}}
{{--                    }--}}
{{--                    $('#selected-recipients').val(recipients);--}}
{{--                    M.textareaAutoResize($('#selected-recipients'));--}}
{{--                });--}}
{{--                $('#cancel-send').on('click',function(){--}}
{{--                    $('#top-section').show();--}}
{{--                    $('#bottom-section').hide();--}}
{{--                });--}}
{{--                $('#send-bulk-email-form').on('submit',function(e){--}}
{{--                    e.preventDefault();--}}
{{--                    console.log("Check",$('#selected-recipients').val());--}}
{{--                    // alert(quill.getText());--}}
{{--                    if(!$('#selected-recipients').val().includes('All')){--}}
{{--                        let users = $("table input:checkbox:checked").map(function () {--}}
{{--                            return $(this).val();--}}
{{--                        }).get();--}}

{{--                        // let data = {selection_type:'multi',users:JSON.stringify(users),--}}
{{--                        //     message:quill.getText(),--}}
{{--                        //     subject:$('#email-subject').val()--}}
{{--                        // };--}}
{{--                        let formData = new FormData();--}}
{{--                        formData.append('selection_type', 'multi');--}}
{{--                        formData.append('users',JSON.stringify(users));--}}
{{--                        formData.append('message', quill.root.innerHTML);--}}
{{--                        formData.append('subject', $('#email-subject').val());--}}

{{--                        jQuery.each(jQuery('#attachments')[0].files, function (i, file) {--}}
{{--                            formData.append('attachment', file);--}}
{{--                        });--}}
{{--                        $.ajax({--}}
{{--                            url: "/send-venture-bulk-email",--}}
{{--                            processData: false,--}}
{{--                            contentType: false,--}}
{{--                            data: formData,--}}
{{--                            type: 'post',--}}
{{--                            success: function (response, a, b) {--}}
{{--                                console.log("success", response);--}}
{{--                                alert(response.message);--}}
{{--                                window.location.reload();--}}
{{--                            },--}}
{{--                            error: function (response) {--}}
{{--                                console.log("error", response);--}}
{{--                                let message = response.responseJSON.message;--}}
{{--                                console.log("error", message);--}}
{{--                                let errors = response.responseJSON.errors;--}}

{{--                                for (var error in   errors) {--}}
{{--                                    console.log("error", error)--}}
{{--                                    if (errors.hasOwnProperty(error)) {--}}
{{--                                        message += errors[error] + "\n";--}}
{{--                                    }--}}
{{--                                }--}}
{{--                                alert(message);--}}
{{--                            }--}}
{{--                        });--}}

{{--                    }else{--}}
{{--                        let users = $("table input:checkbox:checked").map(function () {--}}
{{--                            return $(this).val();--}}
{{--                        }).get();--}}
{{--                        let data = {selection_type:'all',--}}
{{--                            message:$('#summernote').summernote('code'),--}}
{{--                            subject:$('#email-subject').val()};--}}
{{--                        console.log("data",data);--}}
{{--                        $.post('/send-bulk-email',data).done(function (data) {--}}
{{--                            console.log("Check hrere",data);--}}
{{--                            alert(data.message);--}}
{{--                            window.location.reload();--}}
{{--                        }).fail(function(reason){--}}
{{--                            console.log("reason",reason);--}}
{{--                            alert(reason.message);--}}
{{--                        });--}}
{{--                    }--}}

{{--                });--}}
{{--                $('#assign-stock').on('click', function () {--}}
{{--                    $('#selected-products').empty();--}}
{{--                    let selected_employees = $("table input:checkbox:checked").map(function () {--}}
{{--                        return $(this).val();--}}
{{--                    }).get();--}}
{{--                    let visitors = {!! $incubatees !!};--}}

{{--                    for(var i=0;i<selected_employees.length;i++){--}}
{{--                        let employee = visitors.find(employee => employee.id == employees[i].id);--}}
{{--                        console.log('Employee',employee);--}}
{{--                        // $('#selected-products').append('<tr><td>'+product.barcode+'<td>'+product.product_name+' - '+ product.quantity+' Available</td><td> <input style="width:80px!important;" class="qty" min="0" max="'+product.quantity+'" required id="'+product.id+'" type="number" class="validate"/></td></tr>');--}}
{{--                    }--}}

{{--                });--}}

{{--                function count_selected() {--}}
{{--                    var count = $("table input[type=checkbox]:checked").length;--}}
{{--                    $('#visitors-selected-count').empty();--}}
{{--                    if (count <= 0) {--}}
{{--                        $('#send-mail').prop("disabled", true);--}}
{{--                        $('#visitors-selected-count').append(' No Recipients Selected');--}}
{{--                    } else {--}}
{{--                        $('#send-mail').prop("disabled", false);--}}
{{--                        $('#visitors-selected-count').append(count + ' Recipients Selected');--}}
{{--                    }--}}
{{--                }--}}

{{--                function saveToServer(file) {--}}
{{--                    const fd = new FormData();--}}
{{--                    fd.append('image', file);--}}

{{--                    $.ajax({--}}
{{--                        url: "/visitor-embed-image",--}}
{{--                        processData: false,--}}
{{--                        contentType: false,--}}
{{--                        data: fd,--}}
{{--                        type: 'post',--}}
{{--                        success: function (response, a, b) {--}}
{{--                            console.log("success", response);--}}
{{--                            const url = response;--}}
{{--                            insertToEditor(url);--}}
{{--                            // window.location.reload();--}}
{{--                        },--}}
{{--                        error: function (response) {--}}
{{--                            console.log("error", response);--}}
{{--                            let message = response.responseJSON.message;--}}
{{--                            console.log("error", message);--}}
{{--                            let errors = response.responseJSON.errors;--}}

{{--                            for (var error in   errors) {--}}
{{--                                console.log("error", error)--}}
{{--                                if (errors.hasOwnProperty(error)) {--}}
{{--                                    message += errors[error] + "\n";--}}
{{--                                }--}}
{{--                            }--}}
{{--                            alert(message);--}}
{{--                        }--}}
{{--                    });--}}
{{--                }--}}
{{--                function selectLocalImage() {--}}
{{--                    const input = document.createElement('input');--}}
{{--                    input.setAttribute('type', 'file');--}}
{{--                    input.click();--}}

{{--                    // Listen upload local image and save to server--}}
{{--                    input.onchange = () => {--}}
{{--                        const file = input.files[0];--}}

{{--                        // file type is only image.--}}
{{--                        if (/^image\//.test(file.type)) {--}}
{{--                            saveToServer(file);--}}
{{--                        } else {--}}
{{--                            console.warn('You could only upload images.');--}}
{{--                        }--}}
{{--                    };--}}
{{--                }--}}
{{--                function insertToEditor(url) {--}}
{{--                    // push image url to rich editor.--}}
{{--                    console.log("Checj url",url);--}}
{{--                    const range = quill.getSelection();--}}
{{--                    var getUrl = window.location;--}}
{{--                    var baseUrl = getUrl .protocol + "//" + getUrl.host;--}}
{{--                    var img_url = baseUrl+'/storage/'+url;--}}

{{--                    quill.insertEmbed(range.index, 'image', img_url);--}}
{{--                }--}}
{{--                quill.getModule('toolbar').addHandler('image', () => {--}}
{{--                    selectLocalImage();--}}
{{--                });--}}
{{--            });--}}

{{--        </script>--}}
{{--    @endpush--}}
{{--@endsection--}}



{{--@extends('layouts.admin-layout')--}}

{{--@section('content')--}}


{{--    <div class="container-fluid">--}}
{{--        <div id="top-section">--}}
{{--            <div class="row " >--}}
{{--                <div style="float: right; margin-top:2em;margin-right: 2em;" >--}}
{{--                    <span id="employees-selected-count">No recipients selected</span>--}}
{{--                    <button class="btn waves-effect waves-light"  disabled style="margin-right:2em;" id="send-mail">Send Email--}}
{{--                        <i class="material-icons right">send</i>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row" style="margin-left: 2em;margin-right: 2em;">--}}
{{--                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Propella Ventures</h6>--}}
{{--                <div class="col s12">--}}
{{--                    <table class="table table-bordered" style="width: 100%!important;" id="venture-table">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th><button type="button" id="selectAll" class="main">--}}
{{--                                <span class="sub"></span> Select All</button></th>--}}
{{--                            <th>Venture Name</th>--}}
{{--                            <th>Contact Number</th>--}}
{{--                            <th>Hub</th>--}}
{{--                            <th>Stage</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}

{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="container">--}}
{{--        <div id="bottom-section" hidden  >--}}
{{--            <h6 style="text-transform: uppercase;font-weight: bolder;margin-top:2em;text-align: center;">Mail Send</h6>--}}
{{--            <div class="row" >--}}
{{--                <form id="send-bulk-email-form" class="col s12 card hoverable" style=" margin-right:2em;margin-right: 2em;">--}}
{{--                    @csrf--}}
{{--                    <div class="row">--}}
{{--                        <div class="input-field col m12" style=" margin-right: 2em!important;">--}}
{{--                            <i class="material-icons prefix">people_outline</i>--}}
{{--                            <textarea id="selected-recipients" class="materialize-textarea"></textarea>--}}
{{--                            --}}{{--<label for="selected-recipients">Selected Recipients</label>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="row">--}}
{{--                        <div class="input-field col m12" style="margin-right: 2em!important;;">--}}
{{--                            <i class="material-icons prefix">mode_edit</i>--}}
{{--                            <input id="email_subject" type="text" class="validate">--}}
{{--                            <label for="email_subject">Email Subject</label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row">--}}
{{--                        <div class="file-field input-field" style="bottom:0px!important;margin-left: 1em;margin-right: 2em!important;;">--}}
{{--                            <div class="btn">--}}
{{--                                <span>Attachments</span>--}}
{{--                                <input id="attachments" type="file">--}}
{{--                            </div>--}}
{{--                            <div class="file-path-wrapper">--}}
{{--                                <input class="file-path validate" type="text">--}}
{{--                            </div>--}}
{{--                            <span id="message"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row" style="margin-left: 1em;margin-right: 2em;">--}}
{{--                        <label for="editor">Message</label>--}}
{{--                        <div id="editor" >--}}

{{--                        </div>--}}

{{--                        --}}{{--<textarea style="margin-left:1em;" id="summernote" name="message" class="materialize-textarea"></textarea>--}}
{{--                    </div>--}}
{{--                    <div class="row" style="margin-top:2em;">--}}
{{--                        <div class="col offset-m4">--}}
{{--                            <a id="cancel-send" class="waves-effect waves-green btn modal-close">Cancel<i class="material-icons right">close</i> </a>--}}
{{--                            <button class="btn waves-effect waves-light" style="margin-left:2em;" id="send-bulk-email" name="action">Save--}}
{{--                                <i class="material-icons right">send</i>--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <style>--}}
{{--            .check {--}}
{{--                opacity: 1 !important;--}}
{{--                pointer-events: auto !important;--}}
{{--            }--}}

{{--            table.dataTable tbody tr.selected {--}}
{{--                background-color: #B0BED9;--}}
{{--            }--}}
{{--        </style>--}}

{{--    </div>--}}
{{--    @push('custom-scripts')--}}
{{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.14.2/highlight.min.js"></script>--}}
{{--        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">--}}
{{--        <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>--}}


{{--        <script>--}}


{{--            $(document).ready(function () {--}}
{{--                $('select').formSelect();--}}
{{--                // $('#summernote').summernote();--}}
{{--                // var Bold = Quill.import('formats/bold');--}}
{{--                // Bold.tagName = 'B';   // Quill uses <strong> by default--}}
{{--                // Quill.register(Bold, true);--}}
{{--                // var FontAttributor = Quill.import('attributors/class/font');--}}
{{--                // FontAttributor.whitelist = [--}}
{{--                //     'sofia', 'slabo', 'roboto', 'inconsolata', 'ubuntu'--}}
{{--                // ];--}}
{{--                // Quill.register(FontAttributor, true);--}}
{{--                // var ColorClass = Quill.import('attributors/class/color');--}}
{{--                // var SizeStyle = Quill.import('attributors/style/size');--}}
{{--                // Quill.register(ColorClass, true);--}}
{{--                // Quill.register(SizeStyle, true);--}}
{{--                var toolbarOptions = [--}}
{{--                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons--}}
{{--                    ['blockquote', 'code-block'],--}}

{{--                    [{ 'header': 1 }, { 'header': 2 }],               // custom button values--}}
{{--                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],--}}
{{--                    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript--}}
{{--                    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent--}}
{{--                    [{ 'direction': 'rtl' }],                         // text direction--}}

{{--                    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown--}}
{{--                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],--}}

{{--                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme--}}
{{--                    [{ 'font': [] }],--}}
{{--                    [{ 'align': [] }],--}}
{{--                    ['image'],--}}
{{--                    ['clean']                                         // remove formatting button--}}
{{--                ];--}}

{{--                var quill = new Quill('#editor', {--}}
{{--                    modules: {--}}
{{--                        'history': {          // Enable with custom configurations--}}
{{--                            'delay': 2500,--}}
{{--                            'userOnly': true--}}
{{--                        },--}}
{{--                        'syntax':true,--}}
{{--                        toolbar: toolbarOptions--}}
{{--                    },--}}
{{--                    theme: 'snow'--}}
{{--                });--}}
{{--                $(function () {--}}
{{--                    let example = $('#venture-table').DataTable({--}}

{{--                        processing: true,--}}
{{--                        serverSide: true,--}}
{{--                        paging: true,--}}
{{--                        responsive: true,--}}
{{--                        scrollX: 640,--}}
{{--                        ajax: {--}}
{{--                            'url':'/get-AllVenture'--}}
{{--                        },--}}
{{--                        columns: [--}}
{{--                            {data: 'action', name: 'action', orderable: false, searchable: false},--}}
{{--                            {data: 'company_name', name: 'company_name'},--}}
{{--                            {data: 'contact_number', name: 'contact_number'},--}}
{{--                            {data: 'hub', name: 'hub'},--}}
{{--                            {data: 'stage', name: 'stage'},--}}
{{--                        ],--}}
{{--                    });--}}
{{--                    $('select[name="venture-table_length"]').css("display","inline");--}}
{{--                });--}}
{{--                $('#selectAll').click(function(e) {--}}
{{--                    if($(this).hasClass('checkedAll')) {--}}
{{--                        $('input').prop('checked', false);--}}
{{--                        $(this).removeClass('checkedAll');--}}
{{--                    } else {--}}
{{--                        $('input').prop('checked', true);--}}
{{--                        $(this).addClass('checkedAll');--}}
{{--                    }--}}
{{--                });--}}

{{--                $('#venture-table tbody').on('click', 'tr', function () {--}}
{{--                    $(this).toggleClass('selected');--}}
{{--                    count_selected();--}}
{{--                });--}}

{{--                $('#send-mail').on('click',function(){--}}
{{--                    $('#top-section').hide();--}}
{{--                    $('#bottom-section').show();--}}
{{--                    $('#employees-selected-count').empty();--}}
{{--                    let selected_employees = $("table input:checkbox:checked").map(function () {--}}
{{--                        return $(this).val();--}}
{{--                    }).get();--}}
{{--                    let employees = {!! $allVentures !!};--}}
{{--                    let recipients ="";--}}

{{--                    for(var i=0;i<selected_employees.length;i++){--}}
{{--                        let employee = employees.find(employee => employee.id == employees[i].id);--}}
{{--                        console.log('Employee',employee);--}}
{{--                        recipients = recipients + employee.email +";";--}}
{{--                    }--}}
{{--                    $('#selected-recipients').val(recipients);--}}
{{--                    M.textareaAutoResize($('#selected-recipients'));--}}
{{--                });--}}

{{--                $('#cancel-send').on('click',function(){--}}
{{--                    $('#top-section').show();--}}
{{--                    $('#bottom-section').hide();--}}
{{--                });--}}

{{--                $('#assign-stock').on('click', function () {--}}
{{--                    $('#selected-products').empty();--}}
{{--                    let selected_employees = $("table input:checkbox:checked").map(function () {--}}
{{--                        return $(this).val();--}}
{{--                    }).get();--}}
{{--                    let employees = {!! $allVentures !!};--}}

{{--                    for(var i=0;i<selected_employees.length;i++){--}}
{{--                        let employee = employees.find(employee => employee.id == employees[i].id);--}}
{{--                        console.log('Employee',employee);--}}
{{--                        // $('#selected-products').append('<tr><td>'+product.barcode+'<td>'+product.product_name+' - '+ product.quantity+' Available</td><td> <input style="width:80px!important;" class="qty" min="0" max="'+product.quantity+'" required id="'+product.id+'" type="number" class="validate"/></td></tr>');--}}
{{--                    }--}}

{{--                });--}}

{{--                function count_selected() {--}}
{{--                    var count = $("table input[type=checkbox]:checked").length;--}}
{{--                    $('#employees-selected-count').empty();--}}
{{--                    if (count <= 0) {--}}
{{--                        $('#send-mail').prop("disabled", true);--}}
{{--                        $('#employees-selected-count').append(' No Recipients Selected');--}}
{{--                    } else {--}}
{{--                        $('#send-mail').prop("disabled", false);--}}
{{--                        $('#employees-selected-count').append(count + ' Recipients Selected');--}}
{{--                    }--}}
{{--                }--}}
{{--            });--}}

{{--        </script>--}}
{{--    @endpush--}}


{{--@endsection--}}
