@extends('layouts.app')

@section('content')

    <link rel="stylesheet" type="text/css" href="/css/Blog/blogShow.css"/>
    <style>
        .paragraph { margin-top: -20px; }
    </style>
    <div class="" style="margin-top: 5vh;">
        <div class="row" style="margin-left: 8em;margin-right: 8em">
            <p style="font-size: 3em;color: orange;"><b>{{$blog->title}}</b></p>
            <p style="color: lightseagreen">{{$blog->description}}</p>
            {!!$blog->summernote->content!!}
        </div>
        <br>
    </div>


    <script>
        $(document).ready(function () {

            $('.fixed-action-btn').floatingActionButton();

            $('.modal').modal();
            $('.materialboxed').materialbox();


        });
        function navigateBlog(obj){
            let blog_id= obj.getAttribute('data-value');
            window.location.href="/Blogs/show-blog/" + blog_id
        }
    </script>

@endsection
