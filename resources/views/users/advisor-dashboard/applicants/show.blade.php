@extends('layouts.advisor-layout')
@section('content')
    <br />
    <br>
    <div id="QuestionsAndAnswers" >
        <div class="">
            <a   href="/" class="brand-logo">
                <img style="margin-left:40%;width: 200px;" src="/images/Propella_Logo.jpg" class="propella-logo"/></a>
            <br />
            <div class="row" style="margin-left: 5%;margin-right: 5%;">
                <input id="event_id" value="{{$user->id}}" hidden>
                <h6><b>Applicant Details || {{$user->name}}  {{$user->surname}}</b></h6>
                <h6></h6>


                @foreach($questionCategories as $category)
                    <option value="{{$category->id}}" {{$category->id==$category->question_category_id?'selected':''}}>{{$category->category_name}}</option>

                @endforeach


                <h6>Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$user->email}}</h6>
                <h6>ID Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$user->id_number}}</h6>
                <h6>Contact Number &nbsp;: {{$user->contact_number}}</h6>
                <h6>Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$user->address_one}}</h6>
                <br>
                <hr>
            </div>
        </div>


        <div class="">
            <div class="row" style="margin-left: 5%;margin-right: 5%;">
                {{--<h4>{{$user->name}}: </h4>--}}
                <h6><b>Questions and Answers</b></h6>
                <br>
                @foreach($object_array as $object)
                    <h6><b>Q{{$object->question_number}} - {{$object->question_text}}</b></h6>
                    <h6 style="margin-left: 32px;">{{$object->user_answer}}</h6>
                    <hr>
                @endforeach
            </div>

            <br />
        </div>
    </div>
    <br>
    <div class="row">
        <button id="print-button"  style="width:10%;margin-left: 1000px;"class="waves-effect waves-light btn" onclick="printContent('QuestionsAndAnswers')">
            <i class="material-icons left">local_printshop</i>Print
        </button>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();

            function getStoredCart(){
                // return stored array and if not existing return an empty array
                return JSON.parse(sessionStorage.getItem('category', obj.id);
            }
        }

        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }
    </script>

@endsection
