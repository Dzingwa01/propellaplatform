@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-next-step')}}
    <br>
    <br>
    <br>

    <div class="section">
        <input hidden disabled id="user-id-input" value="{{$user->id}}">
        <div class="row center">
            <div class="col l6 m6 s12">
                <i class="material-icons large" style="color: saddlebrown;" onclick="approveApplicant()">thumb_up</i>
                <h3>Go?</h3>
            </div>
            <div class="col l6 m6 s12">
                <i class="material-icons large" style="color: saddlebrown;" onclick="declineApplicant()">thumb_down</i>
                <h3>No Go?</h3>
            </div>
        </div>

        <div class="row center" hidden id="go-row">
            <div class="col s12 m6 l6" style="width:50%;">
                <select id="venture_category">
                    <option value="" disabled selected>Choose Venture category</option>
                    @foreach($venture_categories as $v_category)
                        <option value="{{$v_category->id}}">{{$v_category->category_name}}</option>
                    @endforeach
                </select>

                <select id="events" multiple="multiple">
                    <option value="" disabled selected>Assign to event</option>
                    @foreach($events as $event)
                        <option value="{{$event->id}}">{{$event->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col s12 m6 l6" style="width:50%;">
                <select id="incubatee_stage">
                    <option value="" disabled selected>Choose Venture stage</option>
                    @foreach($incubatee_stages as $i_stage)
                        <option value="{{$i_stage->id}}">{{$i_stage->stage_name}}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn blue" id="submit-chosen-category-button">submit</button>
        </div>

        <div class="row center" hidden id="no-go-row">
            <div class="row center">
                <div class="col l6 m6 s12">
                    <select id="contact-input">
                        <option value="" disabled selected>How did you contact {{$user->name}} {{$user->surname}}?</option>
                        <option value="Phone Call">Phone Call</option>
                        <option value="Face-to-Face">Face-to-Face</option>
                        <option value="Email">Email</option>
                        <option value="No response to any contact type">No response to any contact type</option>
                        <option value="Invited, but did not arrive">Invited, but did not arrive</option>

                    </select>
                </div>
                <div class="col l6 m6 s12">
                    <select id="contact-result-input">
                        <option value="" disabled selected>What was the result?</option>
                        <option value="Application Declined">App - Declined</option>
                        <option value="Application Referred">App - Referred</option>
                        <option value="Interview Declined">Interview - Declined</option>
                        <option value="Interview Referred">Interview - Referred</option>
                    </select>
                </div>
            </div>
            <div class="col l6 m6 s12">
                <label for="decline-reason-input">Reason it was declined?</label>
                <textarea id="decline-but-referred-reason-input"></textarea>
                <br>
                <label for="declined-applicant-categories-input">Select category</label>
                <select id="declined-applicant-categories-input">
                    <option value="" disabled selected>Select applicant category</option>
                    @foreach($declined_applicant_categories as $d_category)
                        <option value="{{$d_category->id}}">{{$d_category->category_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col l6 m6 s12">
                <label for="referred-company-input">Who it was referred to?</label>
                <textarea id="referred-company-input"></textarea>
                <button class="btn blue" id="panel-interview-declined-but-referred-button">Submit</button>
            </div>
        </div>
    </div>

    <div class="section standard-section-with-margins" style="margin-top: 5vh">
        <br>
        <div class="row" style="margin-left: 230px">
            <div class="col l5 center-align z-depth-4 viewLogs" style="margin-top: 10vh;width: 300px;cursor: pointer">
                <div class="input-field col s12">
                    <select id="status">
                        <option value="" disabled selected>Choose Applicant status</option>
                        <option value="Pending">Pending</option>
                        <option value="Closed">Closed</option>
                    </select>
                    <label>Applicant status</label>
                </div>
                <div class="row">
                    <div class="col s4">
                        <a class="waves-effect waves-light btn " id="status-submit-button">Save</a>
                    </div>
                </div>
            </div>
            <div class="col l1"></div>

            <div class="col l5 left-align z-depth-4 logCard" style="margin-top: 10vh;width: 300px;cursor: pointer">
                <br>
                <div class="icon-preview col s6 m3"><i style="margin-left: 110px;color: saddlebrown" class="material-icons dp48">announcement</i></div>
                <br>
                <h6 style="margin-left: 2em">APPLICANT CONTACT LOG</h6>
                <a class="waves-effect waves-light modal-trigger" style="color: red;margin-left: 4em" href="#modal1">Click here to log a query</a>
                <br>
                <br>
            </div>
            <div class="col l1"></div>
            <div class="col l5 center-align z-depth-4 viewLogs" style="margin-top: 10vh;width: 300px;cursor: pointer">
                <br>
                <div class="icon-preview col s6 m3"><i style="margin-left: 120px;color: saddlebrown" class="material-icons dp48">remove_red_eye</i></div>
                <br>
                <h6 style="margin-left:25px">VIEW ALL CONTACT LOGS</h6>
                <a class="waves-effect waves-light modal-trigger" style="color: red;margin-left: 2em" href="#modal2">Click here to view contact records</a>
                <br>
                <br>
                <div class="status">
                    <p id="status">Applicant Status : <span style="text-transform: uppercase" ><b>{{$user_applicant->status}}</b></span></p>
                </div>
                <br>
            </div>
        </div>
    </div>


    <!-- Modal Structure for Contact log -->
    <div id="modal1" class="modal" style="width: 300px;margin-top: 20vh">
        <div class="row">
            <h5 style="margin-left: 6em;color: grey">Applicant contact log</h5>
            <div class="row" style="margin-left: 7em">
                <div class="input-field col s10">
                    <input id="comment" type="text" class="validate">
                    <label for="comment">Comment</label>
                </div>
            </div>
            <div class="row" style="margin-left: 7em">
                <div class="input-field col s10">
                    <input id="date" type="date" class="validate">
                    <label for="date">Date/Time</label>
                </div>
            </div>
            <div class="row" style="margin-left: 360px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn" id="contact-log-submit-button">Save</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Structure for all logs-->
    <div id="modal2" class="modal">
        <div class="row" style="margin-left: 5em;margin-right: 5em">
            @foreach($user_applicant->contactLog as $user_log)
                <p><b>Comment</b> : {{$user_log->comment}}</p>
                <p><b>Date</b>:{{$user_log->date}}</p>
                <hr>
            @endforeach
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.status:contains("Closed")').css('color', 'red');
            $('.status:contains("Pending")').css('color', 'green');

            $('.modal').modal();
            $('.logCard').on('click', function () {
                $('#logCardShow').show();
            });
            $('.viewLogs').on('click', function () {
                $('#viewAllLogs').show();
            });

            //Applicant contact log
            $('#contact-log-submit-button').on('click',function () {
                let user_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('date', $('#date').val());

                let url = '/store-contact-log/' + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#date').notify(response.message, "success");

                        setTimeout(function(){
                            window.reload =  $('#modal1').hide();;
                        }, 3000);
                    },
                });
            });

            //Status save
            $('#status-submit-button').on('click',function () {
                let user_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('status', $('#status').val());

                let url = '/update-applicant-status/' + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#status').notify(response.message, "success");

                        setTimeout(function(){
                            window.reload =  $('#logCardShow').hide();;
                        }, 3000);
                    },
                });
            });

            $('select').formSelect();
        });

        function approveApplicant(){
            $('#go-row').show();
            $('#no-go-row').hide();
        }

        function declineApplicant(){
            $('#go-row').hide();
            $('#no-go-row').show();
        }

        $('#submit-chosen-category-button').on('click', function(){
            let category_id = $('#venture_category').val();
            let user_id = $('#user-id-input').val();
            let stage_id = $('#incubatee_stage').val();
            let selected_events = [];
            let r = confirm("Are you sure you want to give this applicant incubatee permissions?");
            if(r === true){
                jQuery.each(jQuery('#events').val(), function (i, value) {
                    selected_events.push(value);
                });
                $('#submit-chosen-category-button').text("loading...");
                let formData = new FormData();
                formData.append('category_id', category_id);
                formData.append('stage_id', stage_id);
                formData.append('events', JSON.stringify(selected_events));

                let url = '/set-applicant-to-incubatee/' + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#submit-chosen-category-button").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/applicants';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        $('#panel-interview-declined-but-referred-button').on('click', function(){
            let r = confirm("Are you sure you want to decline this applicant?");

            if(r === true){
                $('#panel-interview-declined-but-referred-button').text("Loading...");
                let formData = new FormData();
                formData.append('contacted_via', $('#contact-input').val());
                formData.append('result_of_contact', $('#contact-result-input').val());
                formData.append('declined_reason_text', $('#decline-but-referred-reason-input').val());
                formData.append('referred_company', $('#referred-company-input').val());
                formData.append('declined_applicant_category_id', $('#declined-applicant-categories-input').val());

                let user_id = $('#user-id-input').val();
                let url = "/applicant-panel-interview-decline/" + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#decline-but-referred-reason-input').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/applicants';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#decline-but-referred-reason-input').notify(response.message, "error");
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>

@endsection
