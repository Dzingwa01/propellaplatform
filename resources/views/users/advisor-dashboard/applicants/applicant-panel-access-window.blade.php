@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-applicant-access')}}
    <!-- If the applicant panelists are set -->
    <div class="" style="margin-top: 10vh">
        @if(count($applicant->panelists) > 0)
            <div class="card" style="width: 170vh;margin-left: 120px;">
                <br>
                <div class="container-fluid">
                    <input id="user-id-input" value="{{$user->id}}" hidden disabled>

                    <div class="row center" style="margin-left: 2em;margin-right: 2em;">
                        <div class="input-field col m6 s12">
                            <select id="contact-input-display">
                                <option value="{{$applicant->contacted_via}}" selected>{{$applicant->contacted_via}}</option>
                                <option value="Phone Call">Phone Call</option>
                                <option value="Face-to-Face">Face-to-Face</option>
                                <option value="Email">Email</option>
                            </select>
                            <label>You contacted {{$user->name}} {{$user->surname}} via</label>
                        </div>
                        <div class="input-field col l6 m5 s12">
                            <select id="contact-result-input-display">
                                <option value="{{$applicant->result_of_contact}}" selected>{{$applicant->result_of_contact}}</option>
                                <option value="Panel Interview">Panel Interview</option>
                                <option value="Application Declined">Application - Declined</option>
                                <option value="Application Referred">Application - Referred</option>
                                <option value="Interview Declined">Interview - Declined</option>
                                <option value="Interview Referred">Interview - Referred</option>
                            </select>
                            <label>The result of this contact</label>
                        </div>
                    </div>
                </div>
                <div class="row center" style="margin-left: 2em;margin-right: 2em;">
                    <input hidden disabled id="question-category-id" value="{{$applicant->panelists[0]->question_category_id}}">
                    <div class="input-field col s6">
                        <input type="date" id="panel-selection-date-display" value="{{$applicant->panelists[0]->panel_selection_date}}">
                        <label for="">Date of panel interview</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="time" id="panel-selection-time-display" value="{{$applicant->panelists[0]->panel_selection_time}}">
                        <label for="">Time of panel interview</label>
                    </div>
                </div>
                <br>
                <div class="row">
                    <button class="btn blue" id="update-panel-interview-details-button">Update</button>
                </div>
            </div>

            <div class="row">
                <div class="row center" style="background-color: grey;margin-right: 8em;margin-left: 8em;">
                    <h5 style="color: white;" id="panelist-header"><b>Panelists you have chosen</b></h5>
                </div>
                <div class="row" style="margin-left: 7em;margin-right: 7em;">
                    @foreach($applicant_panelists as $applicant_panelist)
                        <div class="col s6">
                            <div class="card" style="background: grey">
                                <div class="card-content">
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <label for="title-display" style="color: white;font-size: 1em"><b>Title</b></label>
                                            <input id="title-display" disabled name="title-display" type="text" value="{{$applicant_panelist->title}}">
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <label for="initial-display" style="color: white;font-size: 1em"><b>Initials</b></label>
                                            <input id="initial-display" disabled name="initial-display" type="text" value="{{$applicant_panelist->initials}}">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <label for="name-display" style="color: white;font-size: 1em"><b>First Name</b></label>
                                            <input id="name-display" disabled name="name-display" type="text" value="{{$applicant_panelist->name}}">
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <label for="surname-display" style="color: white;font-size: 1em"><b>Surname</b></label>
                                            <input id="surname-display" disabled name="surname-display" type="text" value="{{$applicant_panelist->surname}}">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <label for="company-display" style="color: white;font-size: 1em"><b>Company Name</b></label>
                                            <input id="company-display" disabled name="company-display" type="text" value="{{$applicant_panelist->company_name}}">
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <label for="position-display" style="color: white;font-size: 1em"><b>Position</b></label>
                                            <input id="position-display" disabled name="position-display" type="text" value="{{$applicant_panelist->position}}">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <label for="email-display" style="color: white;font-size: 1em"><b>Email</b></label>
                                            <input id="email-display" disabled name="email-input" type="email" value="{{$applicant_panelist->email}}">
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <label for="contact-number-display" style="color: white;font-size: 1em"><b>Contact Number</b></label>
                                            <input id="contact-number-display" disabled name="contact-number-display" type="text" value="{{$applicant_panelist->contact_number}}">
                                        </div>
                                    </div>
                                    <div class="row center" >
                                        <i style="color: red; cursor: pointer;" class="material-icons small" id="{{$applicant_panelist->applicant_panelist_id}}"
                                           onclick="deletePanelist(this)">delete_forever</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="row" style="margin-left: 7em">
                <h5 id="add-panelist-header">Want to add more panelists?</h5>
                <div class="input-field col s4">
                    <input type="email" id="extra-panelist-email-input" class="validate">
                    <label for="email">Enter email address of the person</label>
                    <btn  class="btn blue" onclick="appendPanelist()">Add</btn>
                </div>
            </div>



            <!-- If the applicant panelists are not set -->
        @else
            <div class="container-fluid">
                <input id="user-id-input" value="{{$user->id}}" hidden disabled>
                <div class="row center">
                    <div class="col l6 m6 s12">
                        <select id="contact-input">
                            <option value="" disabled selected>How did you contact {{$user->name}} {{$user->surname}}?</option>
                            <option value="Phone Call">Phone Call</option>
                            <option value="Face-to-Face">Face-to-Face</option>
                            <option value="Email">Email</option>
                            <option value="No response to any contact type">No response to any contact type</option>
                            <option value="Invited, but did not arrive">Invited, but did not arrive</option>
                        </select>
                    </div>
                    <div class="col l6 m6 s12">
                        <select id="contact-result-input">
                            <option value="" disabled selected>What was the result?</option>
                            <option value="Panel Interview">Panel Interview</option>
                            <option value="Application Declined">App - Declined</option>
                            <option value="Application Referred">App - Referred</option>
                            <option value="Interview Declined">Interview - Declined</option>
                            <option value="Interview Referred">Interview - Referred</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="section" id="panel-interview-row" hidden>
                <div class="row">
                    <div class="col l6 m6 s12">
                        <select id="panelist-input" multiple="multiple">
                            <option value="" disabled selected>Panelists?</option>
                            <option value="grant@propellaincubator.co.za">Grant Minnie</option>
                            <option value="anita@propellaincubator.co.za">Anita Palmer</option>
                            <option value="woosthuizen@engeli.co.za">Wayne Oosthuizen</option>
                            <option value="bwiseman@engeli.co.za">Barry Wiseman</option>
                            <option value="rdames@engeli.co.za">Ricardo Dames</option>
                            <option value="errol@propellaincubator.co.za">Error Wills</option>
                            <option value="daryl.mcwilliams@gmail.com">Daryl McWilliams</option>
                            <option value="Nqobile.Gumede@mandela.ac.za">Nqobile Gumede</option>
                            <option value="Mante.Kgaria@mandela.ac.za">Mante Kgaria</option>
                        </select>

                        <label for="panel-selection-date-input">Date of panel interview</label>
                        <input type="date" id="panel-selection-date-input">

                        <label for="panel-selection-time-input">Time of panel interview</label>
                        <input type="time" id="panel-selection-time-input">
                    </div>
                    <div class="col l6 m6 s12">
                        <div class="row">
                            <select id="question-category-input">
                                <option value="" disabled selected>Choose Questions</option>
                                @foreach($question_categories as $question_category)
                                    <option value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                @endforeach
                            </select>
                            <label for="question-category-input">Questions Category</label>
                        </div>
                        <button class="btn blue" id="show-guest-panelist-button">Add Guest Panelist</button>
                        <div class="container" id="guest-panelist-container" hidden>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <select id="title-input">
                                        <option value="" disabled selected>Choose Title</option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Dr">Dr</option>
                                    </select>
                                    <label>Title</label>
                                </div>
                                <div class="col l6 m6 s12">
                                    <input id="initial-input" name="initial-input" type="text" class="validate">
                                    <label for="initial-input">Initials</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <input id="name-input" name="name-input" type="text" class="validate">
                                    <label for="name-input">First Name</label>
                                </div>
                                <div class="col l6 m6 s12">
                                    <input id="surname-input" name="surname-input" type="text" class="validate">
                                    <label for="surname-input">Last Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <input id="company-input" name="company-input" type="text" class="validate">
                                    <label for="company-input">Company Name</label>
                                </div>
                                <div class="col l6 m6 s12">
                                    <input id="position-input" name="position-input" type="text" class="validate">
                                    <label for="position-input">Position</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <input id="email-input" name="email-input" type="email" class="validate">
                                    <label for="email-input">Email</label>
                                </div>
                                <div class="col l6 m6 s12">
                                    <input id="contact-number-input" name="contact-number-input" type="text" class="validate">
                                    <label for="contact-number-input">Contact Number</label>
                                </div>
                            </div>
                            <div class="row">
                                <button class="btn blue" id="add-guest-panelist-button">Add</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <button class="btn blue" id="panel-interview-button">Finalize</button>
                </div>
            </div>


            <div class="row" id="panel-interview-declined-row" hidden>
                <div class="col l6 m6 s12">
                    <label for="decline-reason-input">Reason it was declined?</label>
                    <textarea id="decline-reason-input"></textarea>
                    <br>
                    <label for="declined-applicant-categories-input">Select category</label>
                    <select id="declined-applicant-categories-input">
                        <option value="" disabled selected>Select applicant category</option>
                        @foreach($declined_applicant_categories as $d_category)
                            <option value="{{$d_category->id}}">{{$d_category->category_name}}</option>
                        @endforeach
                    </select>
                    <button class="btn blue" id="panel-interview-declined-button">Submit</button>
                </div>
            </div>

            <div class="row" id="panel-interview-declined-but-referred-row" hidden>
                <div class="col l6 m6 s12">
                    <label for="decline-reason-input">Reason it was declined?</label>
                    <textarea id="decline-but-referred-reason-input"></textarea>
                    <br>
                    <label for="declined-applicant-categories-input">Select category</label>
                    <select id="declined-applicant-categories-input">
                        <option value="" disabled selected>Select applicant category</option>
                        @foreach($declined_applicant_categories as $d_category)
                            <option value="{{$d_category->id}}">{{$d_category->category_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col l6 m6 s12">
                    <label for="referred-company-input">Who it was referred to?</label>
                    <textarea id="referred-company-input"></textarea>
                    <button class="btn blue" id="panel-interview-declined-but-referred-button">Submit</button>
                </div>
            </div>
        @endif

    </div>

    <script>
        let user_id = $('#user-id-input').val();
        $(document).ready(function () {
            $('select').formSelect();
        });

        $('#contact-result-input').on('change', function(){
            if($('#contact-result-input').val() === "Panel Interview"){
                $('#panel-interview-row').show();
                $('#panel-interview-declined-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Declined" || $('#contact-result-input').val() === "Application Declined"){
                $('#panel-interview-declined-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Referred" || $('#contact-result-input').val() === "Application Referred"){
                $('#panel-interview-declined-but-referred-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-row').hide();
            }
        });

        $('#show-guest-panelist-button').on('click', function(){
            $('#show-guest-panelist-button').hide();
            $('#guest-panelist-container').show();
        });

        //Submit Panel Interview Buttons

        //Approve Applicant
        let guest_panelists = [];

        $('#add-guest-panelist-button').on('click', function(){
            let title = $('#title-input').val();
            let initials = $('#initial-input').val();
            let name = $('#name-input').val();
            let surname = $('#surname-input').val();
            let position = $('#position-input').val();
            let company = $('#company-input').val();
            let email = $('#email-input').val();
            let contact_number = $('#contact-number-input').val();

            let object = {
                title: title,
                initials: initials,
                name: name,
                surname: surname,
                position: position,
                company: company,
                email: email,
                contact_number: contact_number
            };

            guest_panelists.push(object);

            $('#title-input').val("");
            $('#initial-input').val("");
            $('#name-input').val("");
            $('#surname-input').val("");
            $('#position-input').val("");
            $('#company-input').val("");
            $('#email-input').val("");
            $('#contact-number-input').val("");
        });

        $('#panel-interview-button').on('click', function(){
            let selected_panelists = [];
            let formData = new FormData();

            jQuery.each(jQuery('#panelist-input').val(), function (i, value) {
                selected_panelists.push(value);
            });

            formData.append('contacted_via', $('#contact-input').val());
            formData.append('result_of_contact', $('#contact-result-input').val());
            formData.append('selected_panelists', JSON.stringify(selected_panelists));
            formData.append('guest_panelists', JSON.stringify(guest_panelists));
            formData.append('panel_selection_date', $('#panel-selection-date-input').val());
            formData.append('panel_selection_time', $('#panel-selection-time-input').val());
            formData.append('question_category_id', $('#question-category-input').val());

            let url = "/applicant-panel-interview-approve/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#panel-interview-button').notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/applicants';
                    }, 3000);
                },

                error: function (response) {
                    $('#panel-interview-button').notify(response.message, "error");
                }
            });
        });

        //Decline Applicant
        $('#panel-interview-declined-button').on('click', function(){
            let formData = new FormData();
            formData.append('contacted_via', $('#contact-input').val());
            formData.append('result_of_contact', $('#contact-result-input').val());
            formData.append('declined_reason_text', $('#decline-reason-input').val());
            formData.append('declined_applicant_category_id', $('#declined-applicant-categories-input').val());

            let url = "/applicant-panel-interview-decline/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#panel-interview-declined-button').notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/applicants';
                    }, 3000);
                },

                error: function (response) {
                    $('#panel-interview-declined-button').notify(response.message, "error");
                }
            });
        });

        //Decline but refer
        $('#panel-interview-declined-but-referred-button').on('click', function(){
            let formData = new FormData();
            formData.append('contacted_via', $('#contact-input').val());
            formData.append('result_of_contact', $('#contact-result-input').val());
            formData.append('declined_reason_text', $('#decline-but-referred-reason-input').val());
            formData.append('referred_company', $('#referred-company-input').val());
            formData.append('declined_applicant_category_id', $('#declined-applicant-categories-input').val());

            let url = "/applicant-panel-interview-decline-but-refer/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#panel-interview-declined-but-referred-button').notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/applicants';
                    }, 3000);
                },

                error: function (response) {
                    $('#panel-interview-declined-but-referred-button').notify(response.message, "error");
                }
            });
        });

        //Update panel interview details
        $('#update-panel-interview-details-button').on('click', function(){
            let formData = new FormData();
            let date = $('#panel-selection-date-display').val();
            let time = $('#panel-selection-time-display').val();
            let contact = $('#contact-input-display').val();
            let contact_result = $('#contact-result-input-display').val();

            formData.append('date', date);
            formData.append('time', time);
            formData.append('contact', contact);
            formData.append('contact_result', contact_result);

            let url = "/admin-update-panel-interview-details/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-panel-interview-details-button').notify(response.message, "success");
                },
                error: function (response) {
                    $('#update-panel-interview-details-button').notify(response.message, "error");
                }
            });

        });

        function deletePanelist(obj){
            var r = confirm("Are you sure want to remove this panelist?");
            if (r === true) {
                $.get('/admin-remove-panelist-from-applicant/'+obj.id,function(data,status){
                    console.log('Data',data);
                    if(status==='success'){
                        $("#panelist-header").notify(data.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }

        function appendPanelist(){
            let email = $('#extra-panelist-email-input').val();
            let user_id = $('#user-id-input').val();
            let url = '/admin-add-industrial-panelist-from-panel-access-window';
            let question_category_id = $('#question-category-id').val();

            let formData = new FormData();
            formData.append('email', email);
            formData.append('user_id', user_id);
            formData.append('panel_selection_date', $('#panel-selection-date-display').val());
            formData.append('panel_selection_time', $('#panel-selection-time-display').val());
            formData.append('question_category_id', question_category_id);

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#add-panelist-header').notify(response.message + ". Page refresshing in 3 seconds.", "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    $('#add-panelist-header').notify(response.message, "error");
                }
            });
        }
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
