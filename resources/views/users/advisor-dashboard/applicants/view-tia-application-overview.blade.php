@extends('layouts.advisor-layout')

@section('content')
    <input id="user-id-input" value="{{$tiaApplication->id}}" hidden disabled>
    <div class="row card" style="margin-top: 10vh;margin-left: 10em;margin-right: 10em">
        <div class="col s8" style="margin-left: 4em">
            <br>
            <h5><b>Application of {{$tiaApplication->full_name}}</b></h5>
            <br>
            <p><b>Full Name : </b>{{$tiaApplication->full_name}}</p>
            <p><b>Contact Number : </b>{{$tiaApplication->contact_number}}</p>
            <p><b>Email : </b>{{$tiaApplication->email}}</p>
            <p><b>Innovation Description (no more than 50 words) : </b>{{$tiaApplication->innovation_description}}</p>
            <p><b>Do you have a proof of concept? : </b>{{$tiaApplication->proof_of_concept}}</p>
            <p><b>Are there competitors and/or similar products already in the market? : </b>{{$tiaApplication->if_yes}}</p>
            <p><b>If yes, what makes your product different? : </b>{{$tiaApplication->competitors}}</p>
            <p><b>What is the current market size? : </b>{{$tiaApplication->market_size}}</p>
            <br>
        </div>
    </div>

@endsection
