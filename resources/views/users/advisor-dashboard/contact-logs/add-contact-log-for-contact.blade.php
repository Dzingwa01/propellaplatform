@extends('layouts.advisor-layout')

@section('content')
    <input hidden disabled id="company-id-input" value="{{$companyEmployee->id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-add-employees-log',$companyEmployee)}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Advisor Add Contact Log</h6>
            {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">

            <form class="col s12">
                @csrf
                <div class="row">
                    <div class="input-field col m6">
                        <input id="contact_date" type="date" class="validate">
                        <label for="contact_date">Contact Date</label>
                    </div>
                    <div class="input-field col m6">
                        <select style="width: 100%" id="contact_type_id">
                            <option>Select Contact Type</option>
                            @foreach($contact_types as $contact_type)
                                <option value="{{$contact_type->id}}">{{$contact_type->type_name}}</option>
                            @endforeach
                        </select>
                        {{--<label for="contact_type_id">Contact Type</label>--}}
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col m6">
                        <textarea id="description" class="materialize-textarea"></textarea>
                        <label for="description">Description</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col m6">
                        <label>Who called</label><br/><br/>
                        <select id="caller_id" style="width: 100%;margin-top:1em;">
                            {{--<option>Select Caller</option>--}}
                            @foreach($callers as $caller)
                                <option value="{{$caller->id}}" {{$user->id==$caller->id?'selected':''}}>{{$caller->name . " ".$caller->surname}}</option>
                            @endforeach
                        </select>
                        {{--<label for="caller_id">Who Called</label>--}}
                    </div>
                    <div class="input-field col m6">
                        <label>Who Received</label><br/><br/>
                        <select style="width: 100%;margin-top:1em;" id="receiver_id">
                            <option value="{{$companyEmployee->id}}">{{$companyEmployee->name." ".$companyEmployee->surname}}</option>
                            {{--@foreach($receivers as $caller)--}}
                            {{--<option value="{{$caller->id}}" {{$user->id==$caller->id?'selected':''}}>{{$caller->name . " ".$caller->surname}}</option>--}}
                            {{--@endforeach--}}
                        </select>
                        {{--<label for="receiver_id">Who Called</label>--}}
                    </div>
                </div>
                <div class="row">
                    <button class="btn waves-effect waves-light center" style="margin-left:2em;" id="save-contact-log" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
        </div>


    </div>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {

                $('#contact_type_id').formSelect();
                $('#caller_id').select2();
                $('#receiver_id').select2();

                $('#save-contact-log').on('click',function(){
                    let formData = new FormData();
                    formData.append('contact_date', $('#contact_date').val());
                    formData.append('description', $('#description').val());
                    formData.append('contact_type_id', $('#contact_type_id').val());
                    formData.append('caller_id', $('#caller_id').val());
                    formData.append('receiver_id', $('#receiver_id').val());

                    console.log("contact-log ", formData);

                    $.ajax({
                        url: "{{ route('contact-logs.store') }}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success",response);
                            alert(response.message);
                            window.location.href = '/company-employee/show/'+response.company_employee.id;
                        },
                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            console.log("error",message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error",error)
                                if( errors.hasOwnProperty(error) ) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            // $("#modal1").close();
                        }
                    });
                });

            });

        </script>
    @endpush
@endsection
