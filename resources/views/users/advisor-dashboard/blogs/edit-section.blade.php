@extends('layouts.advisor-layout')
@section('content')
    <br>
    <br>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <br>
    <div class="card hoverable" style="margin-left: 1em; margin-right: 1em;">
        <div class="container">
            <br>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Update Section
                Details</h6>
            <br>
            <form id="blog-form" class=" col s12" style="margin-top:1em;">
                @csrf
                <input value="{{$blog->id}}" id="blog_section_id" hidden>
                <div class="row">
                    <div class="input-field col m6">
                        <input id="section_number" value="{{$blog->section_number}}" type="number" class="validate">
                        <label for="section_number">Section Number</label>
                    </div>
                    <div class="input-field col m6">
                        <input id="section_header" value="{{$blog->section_header}}" type="text" class="validate">
                        <label for="section_header">Section Header</label>
                    </div>
                </div>
                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Add
                    Media</h6>
                <br>
                <div class="row">
                    <div class="col m6">
                        <div class="file-field input-field" style="bottom:0px!important;">
                            <div class="btn">
                                <span>Section Image</span>
                                <input id="section_image_url" type="file" name="image_url">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path"
                                       value="{{isset($blog->section_image_url)?$blog->section_image_url:''}}"
                                       type="text">
                            </div>
                        </div>
                    </div>
                    <div class="input-field col m6">
                        <input id="section_video_url" value="{{$blog->section_video_url}}" type="text" class="validate">
                        <label for="section_video_url">Section Video</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col offset-m4">
                        <a
                            class="btn waves-effect waves-light" href="{{url('blog')}}">Cancel
                            <i class="material-icons left">arrow_back</i>
                        </a>
                        <button id="save-incubatee-employee-details" style="margin-left: 2em"
                                class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
                <br>
                <br>
            </form>
        </div>
    </div>
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Paragraph</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="section-paragraph-table">
                    <thead>
                    <tr>
                        <th>Paragraph Number</th>
                        <th>Paragraph Title</th>
                        <th>Paragraph Text</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script>
            $(document).ready(function () {
                $('.datepicker').datepicker();
                let blog_section_id = $('#blog_section_id').val();

                $(function () {
                    $('#section-paragraph-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '/get-paragraph/' + blog_section_id,
                        columns: [
                            {data: 'paragraph_number', name: 'paragraph_number'},
                            {data: 'paragraph_title', name: 'paragraph_title'},
                            {data: 'paragraph_text', name: 'paragraph_text'},
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="section-paragraph-table_length"]').css("display","inline");
                });


                $('#blog-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();

                    formData.append('section_number', $('#section_number').val());
                    formData.append('section_header', $('#section_header').val());
                    formData.append('section_video_url', $('#section_video_url').val());
                    jQuery.each(jQuery('#section_image_url')[0].files, function (i, file) {
                        formData.append('section_image_url', file);
                    });

                    console.log("blog", formData);
                    let url = '/update-section/' + '{{$blog->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response, a, b) {
                            alert(response.message);
                            /*window.location.href = 'blog-index';*/
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
