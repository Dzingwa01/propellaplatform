@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <br>
    <!--Create blog-->
    <div class="card" style="width: 1000px;margin: 0 auto">
        <br>
        <div class="row">
            <h4 style="margin-left: 350px">Create Blog</h4>
            <div class="row" style="margin-left: 100px">
                <div class="input-field col m5">
                    <input id="title" type="text" class="validate">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col m5">
                    <input id="description" type="text" class="validate">
                    <label for="description">Description</label>
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 100px">
            <div class="file-field input-field col m5">
                <div class="btn">
                    <span>Blog Image</span>
                    <input type="file" id="blog_image_url">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 800px;">
            <div class="col s4">
                <a class="waves-effect waves-light btn" id="blog-upload-submit-button">Next</a>
            </div>
        </div>

        <br>
    </div>
    <br>

    <!--Section-->
    <div class="card section second-row" hidden style="width: 1000px;margin: 0 auto">
        <div class="row" style="margin-left: 100px">
            <div class="input-field col m5">
                <input id="section_number" type="number" class="validate">
                <label for="section_number">How many sections do you want?</label>
            </div>
        </div>

        <div class="row" style="margin-left: 700px;">
            <div class="row">
                <div class="col s4">
                    <a class="waves-effect waves-light btn" id="blog_section">Next</a>
                </div>
            </div>
        </div>
        <br>
    </div>
    <br>

    <div id="append_section_value_cards">

    </div>

    <!--Section number-->


    <br>
    <!--Paragraph-->



    <!--Paragraph-->
    <div id="append_paragraph">

    </div>

    <div id="append_paragraph_cards">

    </div>

    <div id="append-paragraph-via-section">

    </div>

    <br>


    <script>
        let global_sections_array = [];
        let global_paragraph_section_array = [];
        let blog_id = "";

        $(document).ready(function () {
            $('select').formSelect();

            //Create blog
            $('#blog-upload-submit-button').on('click', function () {
                let title = $('#title').val();
                let description =  $('#description').val();

                if(title === "" || description === ""){
                    alert("Please insert a title and description!");
                } else {
                    let formData = new FormData();
                    formData.append('title', title);
                    formData.append('description', description);
                    jQuery.each(jQuery('#blog_image_url')[0].files, function (i, file) {
                        formData.append('blog_image_url', file);
                    });

                    let url = "{{route('blog.store')}}";
                    $.ajax({
                        url: url,
                        data: formData,
                        type: 'post',
                        processData: false,
                        contentType: false,
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            alert(response.message);
                            blog_id = response.blog.id;
                            $('.second-row').show();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                }
            });

            //Appended Blog section with all the fields
            $('#blog_section').on('click', function () {
                let count = $('#section_number').val();

                if(count === ""){
                    alert('Please insert how many sections you want!');
                } else {
                    for (let i = 0; i < count; i++) {
                        $('#append_section_value_cards').append(
                            '<div class="card sectionNumber" style="width: 1000px;margin: 0 auto">'
                            +
                            '<div class="row" style="margin-left: 100px">'
                            +
                            '<div class="input-field col m5">'
                            +
                            '<input class="section_number" type="number">'
                            +
                            '<label for="section_number">' + 'Section number' + '</label>'
                            +
                            '</div>'
                            +
                            '<div class="input-field col m5">'
                            +
                            '<input class="section_header" type="text" >'
                            +
                            '<label for="section_header">' + 'Section header' + '</label> '

                            +
                            '</div> '
                            +
                            ' </div> '
                            +
                            '<div class="row" style="margin-left: 100px"> '
                            +
                            '<div class="file-field input-field col m5"> '
                            +
                            '<div class="btn"> '
                            +
                            '<span>' + 'Section Image' + '</span> '
                            +
                            '<input type="file" class="section_image_url" multiple> '
                            +
                            ' </div> '
                            +
                            ' <div class="file-path-wrapper"> '
                            +
                            '<input class="file-path validate" type="text"> '
                            +
                            '</div> '
                            +
                            '</div> '
                            +
                            '<div class="col m5"> '
                            +
                            '<div class="file-field input-field" style="bottom:0px!important;"> '
                            +
                            '<input class="section_video_url" type="text"> '
                            +
                            '<label for="video_url">' + 'URL for a video' + '</label> '
                            +
                            ' </div> '
                            +
                            '</div> '
                            +
                            ' </div> '
                            +
                            '<br>'
                            +
                            '</div>'
                            +
                            '<br>'
                        );
                    }
                    $('#append_section_value_cards').append('<button class="waves-effect waves-light btn" onclick="saveSectionDetails()" style="margin-left: 1200px">' + 'Save' + '</button>')

                }
            });

        });

        //Save Sections details
        function saveSectionDetails() {
            let section_array = [];
            $('.sectionNumber').each(function () {
                let object = {
                    section_number: 0,
                    section_header: '',
                    section_video_url:'',
                    section_image_url:''
                };
                let section_header = $(this).find('.section_header').val();
                let section_number = $(this).find('.section_number').val();
                let section_video_url = $(this).find('.section_video_url').val();
                let section_image_url = $(this).find('.section_image_url').val();



                if(section_header === "" || section_number === ""){
                    alert("Please insert information on all rows!");
                } else {
                    object.section_header = section_header;
                    object.section_number = section_number;
                    object.section_video_url = section_video_url;
                    object.section_image_url = section_image_url;
                    section_array.push(object);
                }
            });

            if(section_array === []){
                alert("No values given!");
            } else {
                let formData = new FormData();
                formData.append('sections_array', JSON.stringify(section_array));
                formData.append('blog_id', blog_id);



                let url = "{{route('section.store')}}";
                $.ajax({
                    url: url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        global_sections_array = response.sections;

                        global_sections_array.forEach(function(section){
                            let section_header = section.section_header;
                            let section_number = section.section_number;
                            let section_id = section.id;

                            $('#append_paragraph').append(
                                '<div class="card paragraph" style="width: 1000px;margin: 0 auto" >'
                                +
                                '<br>'
                                +
                                '<input class="section-id-input" value="'+ section_id +'" hidden disabled>'
                                +
                                '<input class="section-number-input" value="'+ section_number +'" hidden disabled>'
                                +
                                '<input class="section-header-input" value="'+ section_header +'" hidden disabled>'
                                +
                                '<div class="row" style="margin-left: 100px">'
                                +
                                '<p>' + section_number + "-" + section_header + '</P>'
                                +
                                '</div>'
                                +
                                '<div class="row" style="margin-left: 100px">'
                                +
                                '<div class="input-field col m5">'
                                +
                                '<input type="text" class="validate paragraph-number-input">'
                                +
                                '<label for="paragraph-number-input">How many paragraphs do you want?</label>'
                                +
                                '</div>'
                                +
                                '</div>'
                                +
                                '<div class="input-field col m6" style="width: 400px;margin-left: 100px">'
                                +
                                '<br/>'
                                +
                                '</div>'
                                +
                                '<br>'
                                +
                                '</div>'
                                +
                                '<br>'
                            );
                        });


                        $('#append_paragraph').append('<button class="waves-effect waves-light btn" onclick="createParagraphDiv()" style="margin-left: 1200px;">' + 'Save' + '</button>'
                        );
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            }
        }

        function createParagraphDiv(){
            $('.paragraph').each(function () {
                let paragraph_object = {
                    section_number: 0,
                    section_header: '',
                    paragraph_count: 0,
                    section_id: ''
                };
                let section_header = $(this).find('.section-header-input').val();
                let section_number = $(this).find('.section-number-input').val();
                let section_id = $(this).find('.section-id-input').val();
                let paragraph_count = $(this).find('.paragraph-number-input').val();

                paragraph_object.section_header = section_header;
                paragraph_object.section_number = section_number;
                paragraph_object.section_id = section_id;
                paragraph_object.paragraph_count = paragraph_count;
                global_paragraph_section_array.push(paragraph_object);
            });

            for(let i = 0; i < global_paragraph_section_array.length; i++){
                console.log('First Loop Paragraph Count: ' + global_paragraph_section_array[i].paragraph_count);
                let temp_paragraph_count = global_paragraph_section_array[i].paragraph_count;
                for(let j = 0; j < temp_paragraph_count; j++){
                    $('#append-paragraph-via-section').append(
                        '<div class="card paragraphNumber" style="width: 1000px;margin: 0 auto">'
                        +
                        '<div class="row">'
                        +
                        '<h4>' + global_paragraph_section_array[i].section_number + "-" + global_paragraph_section_array[i].section_header + '</h4>'
                        +
                        '<input class="paragraph-section-id-input" hidden disabled value="'+ global_paragraph_section_array[i].section_id +'">'
                        +
                        '</div>'
                        +
                        '<div class="row" style="margin-left: 100px">'
                        +
                        '<div class="input-field col m5">'
                        +
                        '<input class="paragraph_number" type="text">'
                        +
                        '<label for="paragraph_number">Paragraph number</label>'
                        +
                        '</div>'
                        +
                        '<div class="input-field col m5">'
                        +
                        '<input class="paragraph_title" type="text" >'
                        +
                        '<label for="paragraph_title">Paragraph title</label>'
                        +
                        '</div>'
                        +
                        '</div>'
                        +
                        '<div class="row" style="margin-left: 100px">'
                        +
                        '<div class="input-field col m5">'
                        +
                        '<input class="paragraph_text" type="text">'
                        +
                        '<label for="paragraph_text">Paragraph text</label>'
                        +
                        '</div>'
                        +
                        '</div>'
                        +
                        '<div class="row" style="margin-left: 800px;">'
                        +
                        '</div>'
                        +
                        '<br>'
                        +
                        '</div>'
                        +
                        '<br>'
                    );
                }
            }

            $('#append-paragraph-via-section').append('<button class="waves-effect waves-light btn" onclick="saveParagraphsAndSection()" style="margin-left: 1200px;">' + 'Save' + '</button>'
            );
        }

        //Save all paragraph fields
        function saveParagraphsAndSection() {
            let paragraph_with_section_array = [];

            $('.paragraphNumber').each(function () {
                let object = {
                    paragraph_title: '',
                    paragraph_text: '',
                    paragraph_number:0,
                    section_id: ''
                };

                let paragraph_title = $(this).find('.paragraph_title').val();
                let paragraph_text = $(this).find('.paragraph_text').val();
                let paragraph_number = $(this).find('.paragraph_number').val();
                let section_id = $(this).find('.paragraph-section-id-input').val();

                object.paragraph_title = paragraph_title;
                object.paragraph_text = paragraph_text;
                object.paragraph_number = paragraph_number;
                object.section_id = section_id;

                paragraph_with_section_array.push(object);
            });


            let formData = new FormData();
            formData.append('paragraph_with_section_array', JSON.stringify(paragraph_with_section_array));

            console.log(formData.get('paragraph_with_section_array'));

            let url = "{{route('paragraph.section.number.store')}}";
            $.ajax({
                url: url,
                data: formData,
                type: 'post',
                processData: false,
                contentType: false,
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    alert(response.message);
                    window.location.reload();
                },
                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        }
    </script>

@endsection
