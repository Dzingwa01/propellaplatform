@extends('layouts.advisor-layout')
@section('content')
    <br>
    <br>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <br>
    <div class="card hoverable" style="margin-left: 1em; margin-right: 1em;">
        <div class="container">
            <br>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Update Paragraph
                Details</h6>
            <br>
            <form id="blog-paragraph-form" class=" col s12" style="margin-top:1em;">
                @csrf
                <input value="{{$blog->id}}" id="blog_section_id" hidden>
                <div class="row">
                    <div class="input-field col m6">
                        <input id="paragraph_number" value="{{$blog->paragraph_number}}" type="number" class="validate">
                        <label for="paragraph_number">Paragraph Number</label>
                    </div>
                    <div class="input-field col m6">
                        <input id="paragraph_title" value="{{$blog->paragraph_title}}" type="text" class="validate">
                        <label for="paragraph_title">Paragraph Title</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col m6">
                        <input id="paragraph_text" value="{{$blog->paragraph_text}}" type="text" class="validate">
                        <label for="paragraph_text">Paragraph Text</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col offset-m4">
                        <a
                            class="btn waves-effect waves-light" href="{{url('blog')}}">Cancel
                            <i class="material-icons left">arrow_back</i>
                        </a>
                        <button id="save-incubatee-employee-details" style="margin-left: 2em"
                                class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
                <br>
                <br>
            </form>
        </div>
    </div>
    @push('custom-scripts')
        <script>
            $('#blog-paragraph-form').on('submit', function (e) {
                e.preventDefault();

                let formData = new FormData();

                /*formData.append('blog_section_id', $('#blog_section_id').val());*/
                formData.append('paragraph_number', $('#paragraph_number').val());
                formData.append('paragraph_number', $('#paragraph_title').val());
                formData.append('paragraph_number', $('#paragraph_text').val());

                console.log("blog", formData);
                let url = '/update-paragraph/' + '{{$blog->id}}';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response, a, b) {
                        alert(response.message);
                        /*window.location.href = 'blog-index';*/
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = error.response.message;
                        let errors = error.response.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);
                    }
                });
            });
        </script>
    @endpush
@endsection
