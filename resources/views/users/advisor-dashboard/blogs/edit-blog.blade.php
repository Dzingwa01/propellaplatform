@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <div class="card hoverable" style="margin-left: 1em; margin-right: 1em;">
        <div class="container">
            <br>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Update Blog
                Details</h6>
            <br>
            <form id="blog-form" class=" col s12" style="margin-top:1em;">
                @csrf
                <input value="{{$blog->id}}" id="blog_id" hidden>
                <div class="row">
                    <div class="input-field col m6">
                        <input id="title" value="{{$blog->title}}" type="text" class="validate">
                        <label for="title">Title</label>
                    </div>
                    <div class="input-field col m6">
                        <textarea id="description" class="materialize-textarea">{{$blog->description}}</textarea>
                        <label for="description">Description</label>
                    </div>
                </div>
                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Add
                    Media</h6>
                <br>
                <div class="row">
                    <div class="col m6">
                        <div class="file-field input-field" style="bottom:0px!important;">
                            <div class="btn">
                                <span>Blog Image</span>
                                <input id="blog_image_url" type="file" name="blog_image_url">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path"
                                       value="{{isset($blog->blog_image_url)?$blog->blog_image_url:''}}"
                                       type="text">
                            </div>
                        </div>
                    </div>
                    {{--<div class="input-field col m6">
                        <input id="video" value="{{isset($blog->blogMedia()->video)}}" type="text" class="validate">
                        <label for="video">Video Url</label>
                    </div>--}}
                </div>
                <div class="row">
                    <div class="col offset-m4">
                        <a
                            class="btn waves-effect waves-light" href="{{url('blog')}}">Cancel
                            <i class="material-icons left">arrow_back</i>
                        </a>
                        <button id="save-incubatee-employee-details" style="margin-left: 2em"
                                class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
                <br>
                <br>
            </form>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Sections</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="blog-section-table">
                    <thead>
                    <tr>
                        <th>Section Number</th>
                        <th>Section Header</th>
                        {{--<th>Date of Blog</th>--}}
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script>
            $(document).ready(function () {
                $('.datepicker').datepicker();
                let blog_id = $('#blog_id').val();

                $(function () {
                    $('#blog-section-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '/get-section/' + blog_id,
                        columns: [
                            {data: 'section_number', name: 'section_number'},
                            {data: 'section_header', name: 'section_header'},
                            /*{data: 'blog_date', name: 'blog_date'},*/
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="blog-section-table_length"]').css("display","inline");
                });


                $('#blog-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();

                    formData.append('blog_id', $('#blog_id').val());
                    formData.append('title', $('#title').val());
                    formData.append('description', $('#description').val());
                    formData.append('body', $('#body').val());
                    formData.append('blog_date', $('#blog_date').val());
                    formData.append('video', $('#video').val());
                    jQuery.each(jQuery('#blog_image_url')[0].files, function (i, file) {
                        formData.append('blog_image_url', file);
                    });

                    console.log("blog", formData);
                    let url = '/update-blog/' + '{{$blog->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response, a, b) {
                            alert(response.message);
                            /*window.location.href = 'blog-index';*/
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
