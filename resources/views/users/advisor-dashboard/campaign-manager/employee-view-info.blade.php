@extends('layouts.advisor-layout')

@section('content')
    <input hidden disabled id="company-id-input" value="{{$companyEmployee->id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-view-employees',$companyEmployee)}}
    <br>
    <br>
    <div class="container-fluid">
        <div class="row" style="margin-left: 2em;margin-right: 2em; margin-top: 2em;">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Advisor Personal Information</h6>
            <table>
                <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Company Name</th>
                    <th>Contact Number</th>
                    <th>Email</th>
                    <th>Company</th>
                    <th>Category</th>
                </tr>
                </thead>
                <tbody>
                <tr><td>{{$companyEmployee->name. ' '.$companyEmployee->surname}}</td>
                    <td> {{$companyEmployee->company->company_name}}</td>
                    <td>{{$companyEmployee->contact_number}}</td>
                    <td><a href="{{"mailto:".$companyEmployee->email}}">{{$companyEmployee->email}}</a></td>
                    <td>{{$companyEmployee->company->company_name}}</td>
                    <td>{{$companyEmployee->categories[0]->category_name}}</td></tr>
                </tbody>
            </table>
        </div>

        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Contact Logs</h6>
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="activity-log-table">
                    <thead>
                    <tr>
                        <th>Ref# </th>
                        <th>Contact Date</th>

                        <th>Contact Type</th>
                        <th>Description</th>
                        <th>Caller</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($companyEmployee->recieved_logs)==0)
                        <tr><td></td><td></td><td>No Contact Logs Found</td><td></td><td></td></tr>
                    @endif
                    @foreach($companyEmployee->recieved_logs as $log)
                        <tr><td>{{$log->reference_number}}</td><td>{{$log->contact_date}}</td><td>{{$log->contact_type->type_name}}</td><td>{{$log->description}}</td><td>{{$log->caller->name.' '.$log->caller->surname}}</td></tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add Contact Log" href="{{url('add-activity-log-for-contact/'.$companyEmployee->id)}}">
                <i class="large material-icons">add</i>
            </a>

        </div>

    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                {{--let company = {!! $company !!}--}}
                {{--$(function () {--}}
                {{--$('#company-employees-table').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--paging: true,--}}
                {{--responsive: true,--}}
                {{--scrollX: 640,--}}
                {{--ajax: {--}}
                {{--'url':'/get-specific-company-employees/'+company.id--}}
                {{--},--}}
                {{--columns: [--}}
                {{--{data: 'name', name: 'name'},--}}
                {{--{data: 'surname', name: 'surname'},--}}
                {{--{data: 'contact_number', name: 'contact_number'},--}}
                {{--{data: 'email', name: 'email'},--}}
                {{--// {data: "company.company_name",name:'company.company_name'},--}}
                {{--{data: "address",name:"address"},--}}
                {{--{data: 'action', name: 'action', orderable: false, searchable: false}--}}
                {{--]--}}
                {{--});--}}
                {{--$('select[name="company-employees-table_length"]').css("display","inline");--}}
                {{--});--}}

                {{--$('#save-company-employee').on('click',function(){--}}
                {{--let formData = new FormData();--}}
                {{--formData.append('name', $('#name').val());--}}
                {{--formData.append('surname', $('#surname').val());--}}
                {{--formData.append('contact_number', $('#contact_number').val());--}}
                {{--formData.append('email', $('#email').val());--}}
                {{--formData.append('company_id', $("#company_id").val());--}}
                {{--formData.append('category_id', $('#category_id').val());--}}
                {{--formData.append('address', $('#address').val());--}}

                {{--console.log("company ", formData);--}}

                {{--$.ajax({--}}
                {{--url: "{{ route('company-employees.store') }}",--}}
                {{--processData: false,--}}
                {{--contentType: false,--}}
                {{--data: formData,--}}
                {{--type: 'post',--}}

                {{--success: function (response, a, b) {--}}
                {{--console.log("success",response);--}}
                {{--alert(response.message);--}}
                {{--window.location.reload();--}}
                {{--},--}}
                {{--error: function (response) {--}}
                {{--console.log("error",response);--}}
                {{--let message = error.response.message;--}}
                {{--let errors = error.response.errors;--}}

                {{--for (var error in   errors) {--}}
                {{--console.log("error",error)--}}
                {{--if( errors.hasOwnProperty(error) ) {--}}
                {{--message += errors[error] + "\n";--}}
                {{--}--}}
                {{--}--}}
                {{--alert(message);--}}
                {{--$("#modal1").close();--}}
                {{--}--}}
                {{--});--}}
                {{--});--}}

            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
