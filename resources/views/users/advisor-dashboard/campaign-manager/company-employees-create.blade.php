@extends('layouts.advisor-layout')

@section('content')
    <input hidden disabled id="company-id-input" value="{{$company->id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-view-employees-create',$company)}}
    <div class="container-fluid" style="margin-top: 2em;">
        <h5 style="text-align: center;">Advisor Add New Employee</h5>
        <div class="row">
            <form class="col s12">
                @csrf
                <div class="row">
                    <div class="input-field col m6">
                        <input id="name" type="text" class="validate">
                        <label for="name">Name</label>
                    </div>
                    <div class="input-field col m6">
                        <input id="surname" type="text" class="validate">
                        <label for="surname">Surname</label>
                    </div>

                </div>
                <div class="row">
                    <div class="input-field col m6">
                        <input id="email" type="email" class="validate">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col m6">
                        <input id="contact_number" type="tel" class="validate">
                        <label for="contact_number">Primary Contact Number</label>
                    </div>
                </div>
                <div class="row">

                    <div class="input-field col m6">
                        <input id="contact_number_two" type="tel" class="validate">
                        <label for="contact_number_two">Secondary Contact Number</label>
                    </div>
                    <div class="input-field col m6">
                        <select id="company_id">
                            <option value="{{$company->id}}">{{$company->company_name}}</option>
                        </select>
                        <label>Company Company</label>
                    </div>
                </div>
                <div class="row">

                    <div class="input-field col m6">
                        <select id="category_id" multiple>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach
                        </select>
                        <label>Person Interests</label>
                    </div>
                    <div class="input-field col m6">
                        <textarea id="address" class="materialize-textarea"></textarea>
                        <label for="address">Primary Address</label>
                    </div>
                </div>
                <div class="row">

                    <div class="input-field col m6">
                        <textarea id="address_two" class="materialize-textarea"></textarea>
                        <label for="address">Address 2</label>
                    </div>

                    <div class="input-field col m6">
                        <textarea id="address_three" class="materialize-textarea"></textarea>
                        <label for="address">Address 3</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col offset-m4">
                        <a id="cancel" class="waves-effect waves-green btn">Cancel<i class="material-icons right">close</i> </a>
                        <button class="btn waves-effect waves-light" style="margin-left:2em;" id="save-company-employee" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $('cancel').on('click',function(){
                    window.history.back();
                });

                let company = {!! $company !!}

                $('#save-company-employee').on('click',function(){
                    let formData = new FormData();
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('contact_number_two', $('#contact_number_two').val());
                    formData.append('email', $('#email').val());
                    formData.append('company_id', $("#company_id").val());
                    formData.append('category_id', $('#category_id').val());
                    formData.append('address', $('#address').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    console.log("company ", formData);

                    $.ajax({
                        url: "{{ route('company-employees.store') }}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success",response);
                            alert(response.message);
                            // window.location.href = '/company-employees';
                            window.history.back();
                        },
                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            console.log("error",message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error",error)
                                if( errors.hasOwnProperty(error) ) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });

            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
