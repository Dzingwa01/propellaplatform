@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-bootcamper')}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Bootcampers</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="bootcampers-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Category</th>
                        <th>Date Joined</th>
                        <th>Pitch Video</th>
                        <th>ID</th>
                        <th>CIPC</th>
                        <th>POA</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(function () {
                $('#bootcampers-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    stateSave: true,
                    pageLength: 100,
                    ajax: '{{route('get-bootcampers')}}',
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'contact_number', name: 'contact_number'},
                        {data:'category_name',name:'category_name'},
                        {data:'date_joined', name: 'date_joined'},
                        {data:'pitch_video_link',name:'pitch_video_link'},
                        {data: 'id_document_url', name: 'id_document_url'},
                        {data: 'cipc', name: 'cipc'},
                        {data: 'proof_of_address', name: 'proof_of_address'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="bootcampers-table_length"]').css("display","inline");
            });

        });
        function confirm_delete_bootcamper(obj) {
            var r = confirm("Are you sure want to delete this bootcamper?");
            if (r === true) {
                $.get('/bootcamper-delete/' + obj.id, function (data, status) {
                    if (status === 'success') {
                        alert(data.message);
                        window.location.reload();
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }
    </script>
@endsection
