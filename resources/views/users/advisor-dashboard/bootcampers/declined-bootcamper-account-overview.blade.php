@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>

    <div class="col s10 card z-depth-4" style="border-radius: 10px;width: 1350px;margin: 0 auto">
        <input id="user-id-input" value="{{$declinedBootcamper->id}}" hidden disabled>
        {{--BOOTCAMPER ACOUNT INFO--}}
        <div class="row" id="bootcamper-account-show" style="margin-right: 3em; margin-left: 3em;">
            <br>
            <h4 class="center"><b>OVERVIEW OF : {{$declinedBootcamper->name}} {{$declinedBootcamper->surname}}</b></h4>
            <div class="row " style="margin-left: 10em">
                <br>
                <div class="row">
                    <div class="col l6 m6 s12">
                        <h6><b>Title : </b> {{$declinedBootcamper->title}}</h6>
                        <h6><b>Initial : </b> {{$declinedBootcamper->initials}}</h6>
                        <h6><b>Full name: </b> {{$declinedBootcamper->name}} {{$declinedBootcamper->surname}}</h6>
                        <h6><b>Email : </b> {{$declinedBootcamper->email}}</h6>
                    </div>
                    <div class="col l6 m6 s12">
                        <h6><b>ID Number : </b> {{$declinedBootcamper->id_number}}</h6>
                        <h6><b>Age : </b> {{$declinedBootcamper->age}}</h6>
                        <h6><b>Gender : </b> {{$declinedBootcamper->gender}}</h6>
                        <h6><b>Race </b> {{$declinedBootcamper->race}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row center">
        <h4><b>ACTIONS</b></h4>
    </div>
    <div class="row" style="margin-left: 10em">
        <div class="card col s2" id="declined-bootcamper-application-details-button" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">APPLICATION </p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="contact-log" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">CONTACT LOG</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="declined-bootcamper-business-details-button" style="background-color:#454242;font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">BUSINESS DETAILS</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="declined-bootcamper-event-details-button" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">EVENT DETAILS</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="declined-bootcamper-panel-details-button" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">PANEL RESULTS</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="declined-bootcamper-uploads" style="background-color:#454242;font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">UPLOADS</p>
        </div>
    </div>
    <div class="section" style="margin-top: 5vh">
        {{--BOOTCAMPER FORM--}}
        <div class="col s12 card" style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <div class="row" id="declined-bootcamper-application-details-row" hidden style="margin-right: 3em; margin-left: 3em;">
                <div class="container" id="QuestionsAndAnswers">
                    <br>
                    <div class="row center">
                        <h4><b>APPLICATION DETAILS</b></h4>

                        <br>
                        <hr style="background: darkblue; height: 5px;">
                        <br>
                    </div>

                    <div class="row">
                        @foreach($sortedDeclinedBootcamperApplicationQuestionAnswersArray as $question_answer)
                            <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                            <div class="input-field">
                                <textarea disabled>{{$question_answer->answer_text}}</textarea>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="row center">
                    <button id="print-button" class="waves-effect waves-light btn"
                            onclick="printContent('QuestionsAndAnswers')">
                        <i class="material-icons left">local_printshop</i>Print
                    </button>
                </div>
            </div>
        </div>
        <div class="col s12 card" style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <div class="row" id="declined-bootcamper-business-details-row" style="margin-right: 3em; margin-left: 3em;" hidden>
                <br>
                <div class="row center">
                    <h4><b>BUSINESS DETAILS</b></h4>

                </div>
                <span>
                       <b> Chosen Category</b>  : {{$declinedBootcamper->chosen_category}}
                        <br>
                        <b>Venture Name</b>  : {{$declinedBootcamper->venture_name}}
                        <br>
                        <b>Pitch Video Links: Link one</b> - {{$declinedBootcamper->pitch_video_link}} | <b> Link two</b> - {{$declinedBootcamper->pitch_video_link_two}}
                        <br>
                        <b>Contacted Via</b>  : {{$declinedBootcamper->contacted_via}}
                        <br>
                        <b>Result of contact</b>  : {{$declinedBootcamper->result_of_contact}}
                        <br>
                        @if($declinedBootcamper->declined == true)
                        <b>Declined</b> : Yes
                        <br>
                        <b>Declined Reason</b> : {{$declinedBootcamper->declined_reason}}
                    @else
                        <b>Declined</b> : No
                    @endif
                        <br>
                        @if($declinedBootcamper->referred == true)
                        <b>Referred</b> : No
                        <br>
                        <b>Referred Company</b> : {{$declinedBootcamper->referred_company}}
                    @else
                        <b>Referred </b>: No
                        <br>
                        <b>Referred Company</b>: None
                    @endif
                        <br>
                    </span>
                <br>
            </div>
        </div>
        <div class="col s12 card" style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <div class="row" id="declined-bootcamper-event-details-row" style="margin-right: 3em; margin-left: 3em;" hidden >
                <div class="row center">
                    <br>
                    <h4><b>EVENT DETAILS</b></h4>
                </div>
                <div class="section" style="margin-bottom: 3em; margin-top: 3em;">
                    <div class="row center">
                        <table id="t01">
                            <tr>
                                <th>Event title</th>
                                <th>Event Date</th>
                                <th>Date Registered</th>
                                <th>Accepted</th>
                                <th>Declined</th>
                                <th>Attended</th>
                            </tr>
                            @foreach($declinedBootcamperEventArray as $event)
                                <tr>
                                    <td>{{$event->event_title}}</td>
                                    <td>{{$event->event_date}}</td>
                                    <td>{{$event->date_registered}}</td>
                                    <td> @if($event->accepted == true)
                                            Yes
                                        @else
                                            No
                                        @endif</td>
                                    <td> @if($event->declined == true)
                                            Yes
                                        @else
                                            No
                                        @endif</td>
                                    <td>@if($event->attended == true)
                                            Yes
                                        @else
                                            No
                                        @endif</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <div class="col s12 card" style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <div class="row" id="declined-bootcamper-panel-details-row" style="margin-right: 3em; margin-left: 3em;" hidden>
                {{--SHOW BOOTCAMPER PANELIST--}}
                <div class="row" id="panel-results-row" hidden style="width: 170vh;margin-left: 120px;">
                    <div class="section" style="background-color: grey; margin-bottom: 3em;">
                        <div class="row center">
                            <h5>Panelists assigned to bootcamper</h5>

                            @foreach($declinedBootcamperPanelInterviewArray as $panelist)
                                <div class="col l3 m3 s12">
                                    <ul class="collapsible">
                                        <li>
                                            <div class="collapsible-header" style="color: #00b0ff">{{$panelist->panelist_details}}</div>
                                            <div class="collapsible-body">
                                                @foreach($panelist->panelist_question_answers as $p_question_answer)
                                                    <span>
                                            <b>{{$p_question_answer->question_number}} - {{$p_question_answer->question_text}}</b>
                                            <br>
                                            <b>Score:</b> {{$p_question_answer->panelist_score}}
                                            <br>
                                            <b>Comment:</b> {{$p_question_answer->panelist_comment}}
                                            <br>
                                            <br>
                                        </span>
                                                @endforeach
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>



                <div class="row center">
                    <br>
                    <h4><b>PANEL DETAILS</b></h4>
                </div>
                <span>
                        Panel Date: {{$bootcamperPanelSelectionDate}}
                        <br>
                        Panel Time: {{$bootcamperPanelSelectionTime}}
                        <br>
                        Panel Score: {{$declinedBootcamper->total_panel_score}}
                        <br>
                        Average Score: {{$declinedBootcamper->average_panel_score}}
                        <br>
                        <br>
                    </span>
                @foreach($declinedBootcamperPanelInterviewArray as $panelist)
                    <div class="col l3 m3 s12">
                        <ul class="collapsible">
                            <li>
                                <div class="collapsible-header" style="color: #00b0ff">{{$panelist->panelist_details}}</div>
                                <div class="collapsible-body">
                                    @foreach($panelist->panelist_question_answers as $p_question_answer)
                                        <span>
                                            <b>{{$p_question_answer->question_number}} - {{$p_question_answer->question_text}}</b>
                                            <br>
                                            <b>Score:</b> {{$p_question_answer->panelist_score}}
                                            <br>
                                            <b>Comment:</b> {{$p_question_answer->panelist_comment}}
                                            <br>
                                            <br>
                                        </span>
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
        {{--CONTACT RESULTS--}}
        <div class=" col s12 card row" id="contact-log-row" hidden
             style="margin-left: 10em;margin-right: 10em;border-radius: 10px;">
            <br>
            <div class="row center">
                <h4><b>CONTACT LOG</b></h4>
            </div>
            <div class="row">
                <div class="col s4" style="margin-left: 2em">
                    <h5 style="color: grey;">Contact log history</h5>
                    <br>
                    @foreach($declinedBootcamper->contactLog->sortByDesc('date') as $user_log)
                        <p><b>Results of contact</b> : {{$user_log->bootcamper_contact_results}}</p>
                        <p><b>Comment</b> : {{$user_log->comment}}</p>
                        <p><b>Date</b>:{{$user_log->date}}</p>
                        <hr>
                    @endforeach
                </div>
                <div class="col s1"></div>

                <div class="col s7">
                    <h5 style="color: grey;margin-left: 4.5em">Create declined bootcamper contact log</h5>
                    <br>
                    <div class="row" style="margin-left: 7em">
                        <div class="input-field col s10">
                            <select id="bootcamper_contact_results">
                                <option value="Phone Call">Phone Call</option>
                                <option value="Face-to-Face">Face-to-Face</option>
                                <option value="Email">Email</option>
                                <option value="No response to any contact type">No response to any contact type
                                </option>
                                <option value="Invited, but did not arrive">Invited, but did not arrive</option>
                            </select>
                            <label>You contacted {{$declinedBootcamper->name}} {{$declinedBootcamper->surname}} via</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 7em">
                        <div class="input-field col s10">
                            <input id="comment" type="text" class="validate">
                            <label for="comment">Comment</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 300px;">
                        <div class="col s6 save-contact-log" hidden>
                            <a class="waves-effect waves-light btn" id="contact-log-submit-button">Save contact log</a>
                        </div>
                        <div class="col s6 send-email-to-applicant" hidden>
                            <a class="waves-effect waves-light btn" id="send-email">Send email</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--Declined bootcamper uploads--}}
        <div class="col s12 card" style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <div class="row" id="declined-bootcamper-upload-row" style="margin-right: 3em; margin-left: 3em;" hidden>
                <br>
                <div class="row center"><h4><b>UPLOADS</b></h4></div>
                <div class="row">
                    <div class="col s4">
                        @if($declinedBootcamper->pitch_video_link == null)
                            <p>Video Pitch: Not uploaded.</p>
                        @else
                            <a href="{{$declinedBootcamper->pitch_video_link}}">Video Pitch Link</a>
                        @endif
                        <br>
                    </div>  <div class="col s4">
                        @if(isset($declinedBootcamper->proof_of_address))
                            @if($declinedBootcamper->proof_of_address == null)
                                <p>Proof of address: Not uploaded.</p>
                            @else
                                <a href="{{'/storage/'.$declinedBootcamper->proof_of_address}}" target="_blank">Proof of address</a>
                            @endif
                        @endif
                    </div>
                    <div class="col s4">
                        @if(isset($declinedBootcamper->cipc))
                            @if($declinedBootcamper->cipc == null)
                                <p>CIPC: Not uploaded.</p>
                            @else
                                <a href="{{'/storage/'.$declinedBootcamper->cipc}}" target="_blank">CIPC</a>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col s4">
                        @if(isset($declinedBootcamper->bootcamper_contract_upload))
                            @if($declinedBootcamper->bootcamper_contract_upload == null)
                                <p>Contract: Not uploaded.</p>
                            @else
                                <a href="{{'/storage/'.$declinedBootcamper->bootcamper_contract_upload}}" target="_blank">Contract</a>
                            @endif
                        @endif
                    </div>
                    <div class="col s4">
                        @if(isset($declinedBootcamper->id_document_url))
                            @if($declinedBootcamper->id_document_url == null)
                                <p>ID Document: Not uploaded.</p>
                            @else
                                <a href="{{'/storage/'.$declinedBootcamper->id_document_url}}" target="_blank">ID Document</a>
                            @endif
                        @endif
                    </div>
                </div>

                <br>
            </div>
        </div>
    </div>


    <style>
        .txt:hover {
            text-decoration: underline;
        }
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>

    <script>
        $('#declined-bootcamper-account-details-button').on('click', function(){
            $('#declined-bootcamper-account-details-row').show();
            $('#declined-bootcamper-business-details-row').hide();
            $('#declined-bootcamper-application-details-row').hide();
            $('#declined-bootcamper-event-details-row').hide();
            $('#declined-bootcamper-panel-details-row').hide();
            $('#contact-log-row').hide();
            $('#declined-bootcamper-upload-row').hide();
        });

        $('#declined-bootcamper-business-details-button').on('click', function(){
            $('#declined-bootcamper-account-details-row').hide();
            $('#declined-bootcamper-business-details-row').show();
            $('#declined-bootcamper-application-details-row').hide();
            $('#declined-bootcamper-event-details-row').hide();
            $('#declined-bootcamper-panel-details-row').hide();
            $('#contact-log-row').hide();
            $('#declined-bootcamper-upload-row').hide();
        });

        $('#declined-bootcamper-application-details-button').on('click', function(){
            $('#declined-bootcamper-account-details-row').hide();
            $('#declined-bootcamper-business-details-row').hide();
            $('#declined-bootcamper-application-details-row').show();
            $('#declined-bootcamper-event-details-row').hide();
            $('#declined-bootcamper-panel-details-row').hide();
            $('#contact-log-row').hide();
            $('#declined-bootcamper-upload-row').hide();
        });

        $('#declined-bootcamper-event-details-button').on('click', function(){
            $('#declined-bootcamper-account-details-row').hide();
            $('#declined-bootcamper-business-details-row').hide();
            $('#declined-bootcamper-application-details-row').hide();
            $('#declined-bootcamper-event-details-row').show();
            $('#declined-bootcamper-panel-details-row').hide();
            $('#contact-log-row').hide();
            $('#declined-bootcamper-upload-row').hide();
        });

        $('#declined-bootcamper-panel-details-button').on('click', function(){
            $('#declined-bootcamper-account-details-row').hide();
            $('#declined-bootcamper-business-details-row').hide();
            $('#declined-bootcamper-application-details-row').hide();
            $('#declined-bootcamper-event-details-row').hide();
            $('#declined-bootcamper-panel-details-row').show();
            $('#contact-log-row').hide();
            $('#declined-bootcamper-upload-row').hide();
        });

        $('#contact-log').on('click', function(){
            $('#contact-log-row').show();
            $('#declined-bootcamper-account-details-row').hide();
            $('#declined-bootcamper-business-details-row').hide();
            $('#declined-bootcamper-application-details-row').hide();
            $('#declined-bootcamper-event-details-row').hide();
            $('#declined-bootcamper-panel-details-row').hide();
            $('#declined-bootcamper-upload-row').hide();
        });
        $('#declined-bootcamper-uploads').on('click', function () {
            $('#declined-bootcamper-upload-row').show();
            $('#contact-log-row').hide();
            $('#declined-bootcamper-account-details-row').hide();
            $('#declined-bootcamper-business-details-row').hide();
            $('#declined-bootcamper-application-details-row').hide();
            $('#declined-bootcamper-event-details-row').hide();
            $('#declined-bootcamper-panel-details-row').hide();
        });

        $(document).ready(function(){
            $('select').formSelect();
            //CONTACT LOG BUTTON TOGLE
            $('#bootcamper_contact_results').on('change', function(){
                if(this.value === "Email"){
                    $('.save-contact-log').hide();
                    $('.send-email-to-applicant').show();
                }else{
                    $('.save-contact-log').show();
                    $('.send-email-to-applicant').hide();
                }
            });

            //Applicant contact log
            $('#contact-log-submit-button').on('click',function () {
                let declined_bootcamper_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('bootcamper_contact_results', $('#bootcamper_contact_results').val());

                let url = '/store-declined-bootcamper-contact-log/' + declined_bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#contact-log-submit-button').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/declined-bootcamper-account-overview/' + declined_bootcamper_id;
                        }, 3000);
                    },
                });
            });

            //Send email to applicants
            $('#send-email').on('click',function () {
                let declined_bootcamper_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('bootcamper_contact_results', $('#bootcamper_contact_results').val());

                let url = '/store-declined-bootcamper-contact-log/' + declined_bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#send-email').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/declined-bootcamper-account-overview/' + declined_bootcamper_id;
                        }, 3000);
                    },
                });
            });
        });
    </script>
@endsection
