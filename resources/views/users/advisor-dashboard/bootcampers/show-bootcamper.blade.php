@extends('layouts.advisor-layout')

@section('content')
    <br><br>
    {{ Breadcrumbs::render('advisor-bootcamper-details')}}
    <br>
    <head>
{{--        <style>--}}
{{--            table {--}}
{{--                width:100%;--}}
{{--            }--}}
{{--            table, th, td {--}}
{{--                border: 1px solid black;--}}
{{--                border-collapse: collapse;--}}
{{--            }--}}
{{--            th, td {--}}
{{--                padding: 15px;--}}
{{--                text-align: left;--}}
{{--            }--}}
{{--            table#t01 tr:nth-child(even) {--}}
{{--                background-color: #eee;--}}
{{--            }--}}
{{--            table#t01 tr:nth-child(odd) {--}}
{{--                background-color: #fff;--}}
{{--            }--}}
{{--            table#t01 th {--}}
{{--                background-color: black;--}}
{{--                color: white;--}}
{{--            }--}}
{{--        </style>--}}
    </head>
    <br>
    <input id="user-id-input" disabled hidden value="{{$bootcamper->id}}">
    <div class="col s10 card z-depth-4" style="border-radius: 10px;width: 1350px;margin: 0 auto">
        {{--BOOTCAMPER ACOUNT INFO--}}
        <div class="row" id="bootcamper-account-show" style="margin-right: 3em; margin-left: 3em;">
            <br>
            <h4 class="center"><b>OVERVIEW OF : {{$bootcamper->user->name}} {{$bootcamper->user->surname}}</b></h4>
            <div class="row " style="margin-left: 10em">
                <br>
                <div class="row">
                    <div class="col l6 m6 s12">
                        <h6><b>Title : </b> {{$bootcamper->user->title}}</h6>
                        <h6><b>Initial : </b> {{$bootcamper->user->initials}}</h6>
                        <h6><b>Full name: </b> {{$bootcamper->user->name}} {{$bootcamper->user->surname}}</h6>
                        <h6><b>Email : </b> {{$bootcamper->user->email}}</h6>
                        <h6><b>WhatsApp Number : </b> {{$bootcamper->user->whatsapp_number}}</h6>
                        <h6><b>Data Cell Number : </b> {{$bootcamper->user->data_cellnumber}}</h6>
                    </div>
                    <div class="col l6 m6 s12">
                        <h6><b>Service Provider : </b> {{$bootcamper->user->service_provider_network}}</h6>
                        <h6><b>ID Number : </b> {{$bootcamper->user->id_number}}</h6>
                        <h6><b>Age : </b> {{$bootcamper->user->age}}</h6>
                        <h6><b>Gender : </b> {{$bootcamper->user->gender}}</h6>
                        <h6><b>Race </b> {{$bootcamper->user->race}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row center">
        <h4><b>ACTIONS</b></h4>
    </div>
    <div class="row" style="margin-left: 9em">
        <div class="card col s2" id="application" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">APPLICATION </p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="contact-log" style="background-color:#454242;font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">CONTACT LOG</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="uploads" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">UPLOADS</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="events" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">EVENTS</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="evaluation" style="background-color:#454242;font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">EVALUATION</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="go-no-go-panel" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">GO / NO GO PANEL</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="panel-results" style="background-color:#454242;font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">PANEL RESULTS</p>
        </div>
        <div class="col s0.1"></div>
        <div class="card col s2 " id="go-no-go-incubatee"
             style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
            <p class="center txt" style="color: white;cursor:pointer">GO / NO GO INCUBATEE</p>
        </div>
    </div>


    <div class="section" style="margin-top: 5vh">
        {{--BOOTCAMPER FORM--}}
        <div class="col s12 card" style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <div class="row" id="application-details-row" hidden style="margin-right: 3em; margin-left: 3em;">
                <div class="container" id="QuestionsAndAnswers">
                    <br>
                    <div class="row center">
                        <h4><b>APPLICATION DETAILS</b></h4>
                        <div class="row">
                            <div class="col l6 m6 s12">
                                <h6>{{$bootcamper->user->name}} {{$bootcamper->user->surname}}</h6>
                                <h6>Initials: {{$bootcamper->user->initials}}</h6>
                                <h6>Email: {{$bootcamper->user->email}}</h6>
                                <h6>Contact Number: {{$bootcamper->user->contact_number}}</h6>
                                <h6>ID Number: {{$bootcamper->user->id_number}}</h6>
                                <h6>Age: {{$bootcamper->user->age}}</h6>
                                <h6>Gender: {{$bootcamper->user->gender}}</h6>
                            </div>
                            <div class="col l6 m6 s12">
                                <h6>Address One: {{$bootcamper->user->address_one}}</h6>
                                <h6>Address Two: {{$bootcamper->user->address_two}}</h6>
                                <h6>Address Three: {{$bootcamper->user->address_three}}</h6>
                            </div>
                        </div>
                        <br>
                        <hr style="background: darkblue; height: 5px;">
                        <br>
                    </div>

                    <div class="row">
                        @foreach($bootcamperQuestionAnswers as $question_answer)
                            <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                            <div class="input-field">
                                <textarea disabled>{{$question_answer->answer_text}}</textarea>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="row center">
                    <button id="print-button" class="waves-effect waves-light btn"
                            onclick="printContent('QuestionsAndAnswers')">
                        <i class="material-icons left">local_printshop</i>Print
                    </button>
                </div>
            </div>
        </div>
        {{--CONTACT RESULTS--}}
        <div class=" col s12 card row" id="contact-log-row" hidden
             style="margin-left: 10em;margin-right: 10em;border-radius: 10px;">
            <br>
            <div class="row center">
                <h4><b>CONTACT LOG</b></h4>
            </div>
            <div class="row">
                <div class="col s4" style="margin-left: 2em">
                    <div class="status-color">
                        <p id="status-color">Bootcamper Status : <span style="text-transform: uppercase;color: green"><b>CLOSED</b></span></p>
                    </div>
                    <h5 style="color: grey;">Bootcamper Contact log history</h5>
                    <br>
                    @foreach($user_contact_log->sortByDesc('date') as $user_log)
                        <p><b>Results of contact</b> : {{$user_log->bootcamper_contact_results}}</p>
                        <p><b>Comment</b> : {{$user_log->comment}}</p>
                        <p><b>Date</b>:{{$user_log->date}}</p>
                        <hr>
                    @endforeach
                    <h5 style="color: grey;">Applicant Contact log history</h5>
                    <br>
                    @foreach($applicant_contact_log->sortByDesc('date') as $user_log)
                        <p><b>Results of contact</b> : {{$user_log->applicant_contact_results}}</p>
                        <p><b>Comment</b> : {{$user_log->comment}}</p>
                        <p><b>Date</b>:{{$user_log->date}}</p>
                        <hr>
                    @endforeach

                </div>
                <div class="col s1"></div>

                <div class="col s7">
                    <div class="row" style="margin-left: 7em">
                        <div class="input-field col s10">
                            <input id="comment" type="text" class="validate">
                            <label for="comment">Comment</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 7em">
                        <div class="input-field col s10">
                            <input id="date" type="date" class="validate">
                            <label for="date">Date/Time</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 7em">
                        <div class="input-field col s10">
                            <select id="bootcamper_contact_results">
                                <option value="Phone Call">Phone Call</option>
                                <option value="Face-to-Face">Face-to-Face</option>
                                <option value="Email">Email (not auto sent to venture)</option>
                                <option value="No response to any contact type">No response to any contact type
                                </option>
                                <option value="Invited, but did not arrive">Invited, but did not arrive</option>
                            </select>
                            <label>You contacted {{$bootcamper->user->name}} {{$bootcamper->user->surname}} via</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 360px;">
                        <div class="col s4">
                            <a class="waves-effect waves-light btn" id="bootcamper-contact-log-submit-button">Save</a>
                        </div>
                    </div>
                    <br>
                    <div class="row" style="margin-left: 7em">
                        <h5 style="color: grey;">Update bootcamper status</h5>
                        <br>
                        <div class="input-field col s10">
                            <select id="status">
                                <option value="{{$bootcamper->status}}" selected>{{$bootcamper->status}}</option>
                                <option value="Pending">Pending</option>
                                <option value="Closed">Closed</option>
                            </select>
                            <label>Bootcamper status</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--BOOTCAMPER UPLOADS--}}
        <div class=" col s12  row" id="bootcamper-uploads-row" hidden
             style="margin-left: 12em;margin-right: 12em;border-radius: 10px;">
            <br>
            <div class="row center">
                <h4><b>BOOTCAMPER UPLOADS</b></h4>
            </div>
            <div class="row" style="margin-left: 5em">
                <div class="row">
                    @if($bootcamper->pitch_video_link == null)
                        <div class="card col s3" style="background-color: red;font-weight: bold">
                            <p class="center txt" style="color: white;cursor:pointer">Video Pitch: Not uploaded.</p>
                        </div>
                    @else
                        <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                            <a href="{{$bootcamper->pitch_video_link}}"><p class="center txt" style="color: white;cursor:pointer">Video Pitch link.</p></a>
                        </div>
                    @endif
                    <div class="col s0.1"></div>
                    @if($bootcamper->pitch_video_link_two == null)
                        <div class="card col s3" style="background-color: red;font-weight: bold">
                            <p  class="center txt" style="color: white;cursor:pointer">Video Pitch 2: Not uploaded.</p>
                        </div>
                    @else
                        <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                            <a href="{{$bootcamper->pitch_video_link_two}}"><p class="center txt" style="color: white;cursor:pointer">Video Pitch 2 link.</p></a>
                        </div>
                    @endif
                    <div class="col s0.1"></div>
                    @if($bootcamper->id_document_url == null)
                        <div class="card col s3" style="background-color: red;font-weight: bold">
                            <p class="center txt" style="color: white;cursor:pointer">ID Document: Not uploaded.</p>
                        </div>
                    @else
                        <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                            <a href="{{'/storage/'.$bootcamper->id_document_url}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">ID Document Link</p></a>
                        </div>
                    @endif
                </div>

                <div class="row">
                    @if($bootcamper->proof_of_address == null)
                        <div class="card col s3" style="background-color: red;font-weight: bold">
                            <p class="center txt" style="color: white;cursor:pointer">Proof of address: Not uploaded.</p>
                        </div>
                    @else
                        <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                            <a href="{{'/storage/'.$bootcamper->proof_of_address}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Proof of address</p></a>
                        </div>
                    @endif
                    <div class="col s0.1"></div>
                    @if($bootcamper->cipc == null)
                        <div class="card col s3" style="background-color: red;font-weight: bold">
                            <p class="center txt" style="color: white;cursor:pointer">CIPC: Not uploaded.</p>
                        </div>
                    @else
                        <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                            <a href="{{'/storage/'.$bootcamper->cipc}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">CIPC Link</p></a>
                        </div>
                    @endif
                    <div class="col s0.1"></div>
                    @if($bootcamper->contract_uploaded == null)
                        <div class="card col s3" style="background-color: red;font-weight: bold">
                            <p class="center txt" style="color: white;cursor:pointer">Contract: Not uploaded.</p>
                        </div>
                    @else
                        <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                            <a href="{{'/storage/'.$bootcamper->contract_uploaded}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">ContractLink</p></a>
                        </div>
                    @endif

                </div>
            </div>
        </div>
        {{--BOOTCAMPER EVENTS--}}
        <div class=" col s12 card row" id="bootcamper-events-row" hidden
             style="margin-left: 10em;margin-right: 10em;border-radius: 10px;">
            <br>
            <div class="row center">
                <h4><b>BOOTCAMPER EVENTS</b></h4>
            </div>
            <div class="container-fluid"  style="margin-top: 2em;">
                <input hidden disabled id="bootcamper-id-input" value="{{$bootcamper->id}}">
                <div class="row" style="margin-left: 2em;margin-right: 2em;">
                    <div class="col s12">
                        <table class="table table-bordered" style="width: 100%!important;" id="bootcamper-event-table">
                            <thead>
                            <tr>
                                <th>Event</th>
                                <th>Date Registered</th>
                                <th>Accepted</th>
                                <th>Attended</th>
                                <th>Declined</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="fixed-action-btn">
                    <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Visitor" href="#event_modal">
                        <i class="large material-icons">add</i>
                    </a>
                </div>

                <!-- Modal Structure -->
                <div id="event_modal" class="modal" style="background-color: grey;">
                    <div class="row center">
                        @foreach($events as $event)
                            <div class="col l6 m6 s12">
                                <div class="card">
                                    <h5>{{$event->title}}</h5>
                                    <p>Start: {{$event->start->toDateString()}}</p>
                                    <p>End: {{$event->end->toDateString()}}</p>
                                    <a style="color: orange; cursor: pointer;" class="register-bootcamper" data-value="{{$event->id}}">Register</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{--BOOTCAMPER PANEL ACCESS WINDOW--}}
        <div class="section" id="go-no-go-panel-row" hidden>
            <input hidden disabled id="bootcamper-id-input" value="{{$bootcamper->id}}">
            <br>
            <div class="row center">
                <h4><b>GO / NO GO PANEL</b></h4>
            </div>
            <div class="row center">
                <div class="col l6 m6 s12">
                    <i class="material-icons large" style="color: saddlebrown;" onclick="approvePanel()">thumb_up</i>
                    <h3>Go?</h3>
                </div>
                <div class="col l6 m6 s12">
                    <i class="material-icons large" style="color: saddlebrown;" onclick="declinePanel()">thumb_down</i>
                    <h3>No Go?</h3>
                </div>
            </div>
            <div class="row" id="go-panel-row" hidden>
                <div class="" style="margin-top: 10vh">
                    <br>
                    @if(count($bootcamper->panelists) > 0)
                        <div class="card" style="width: 170vh;margin-left: 120px;">
                            <br>
                            <div class="container-fluid">
                                <div class="row center">
                                    <h4><b>UPDATE PANEL FOR {{$bootcamper->user->name}} {{$bootcamper->user->surname}} </b></h4>
                                </div>
                                <input id="bootcamper-id-input" value="{{$bootcamper->id}}" hidden disabled>

                                <div class="row center" style="margin-left: 2em;margin-right: 2em;">
                                    <div class="input-field col m6 s12">
                                        <select id="contact-input-display">
                                            <option value="{{$bootcamper->contacted_via}}" selected>{{$bootcamper->contacted_via}}</option>
                                            <option value="Phone Call">Phone Call</option>
                                            <option value="Face-to-Face">Face-to-Face</option>
                                            <option value="Email">Email</option>
                                        </select>
                                        <label>You contacted {{$bootcamper->user->name}} {{$bootcamper->user->surname}} via</label>
                                    </div>
                                    <div class="input-field col l6 m5 s12">
                                        <select id="contact-result-input-display">
                                            <option value="{{$bootcamper->result_of_contact}}" selected>{{$bootcamper->result_of_contact}}</option>
                                            <option value="Panel Interview">Panel Interview</option>
                                            <option value="Application Declined">App - Declined</option>
                                            <option value="Application Referred">App - Referred</option>
                                            <option value="Interview Declined">Interview - Declined</option>
                                            <option value="Interview Referred">Interview - Referred</option>
                                        </select>
                                        <label>The result of this contact</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row center" style="margin-left: 2em;margin-right: 2em;">
                                <input hidden disabled id="question-category-id" value="{{$bootcamper->panelists[0]->question_category_id}}">
                                <div class="input-field col s6">
                                    <input type="date" id="panel-selection-date-display" value="{{$bootcamper->panelists[0]->panel_selection_date}}">
                                    <label for="">Date of panel interview</label>
                                </div>
                                <div class="input-field col s6">
                                    <input type="time" id="panel-selection-time-display" value="{{$bootcamper->panelists[0]->panel_selection_time}}">
                                    <label for="">Time of panel interview</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em">
                                <div class="input-field col s6">
                                    @if(isset($bootcamper->panelists[0]->question_category_id))
                                        <select id="question-category-display">
                                            <option value="{{$bootcamper->panelists[0]->question_category_id}}"
                                                    selected>{{$bootcamper->panelists[0]->question_category}}</option>
                                            @foreach($question_categories as $question_category)
                                                <option value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <select id="question-category-display">
                                            <option value="" disabled selected>Choose Questions</option>
                                            @foreach($question_categories as $question_category)
                                                <option value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                    <label for="question-category-input">Questions Category</label>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row center">
                                <button class="btn blue" id="update-panel-interview-details-button">Update</button>
                            </div>
                            <br>
                            <div class="row">
                                <div class="row center">
                                    <h4><b>PANELIST YOU HAVE CHOOSEN</b></h4>
                                </div>
                                <table id="table" style="width:93%;margin-left: 2em;margin-right: 8em">
                                    <tr>
                                        <th>Name</th>
                                        <th>Surname</th>
                                        <th>Email</th>
                                        <th>Company</th>
                                        <th>Position</th>
                                    </tr>
                                    @foreach($bootcamper_panel as $applicant_panelist)
                                        <tr>
                                            <td>{{$applicant_panelist->name}}</td>
                                            <td>{{$applicant_panelist->surname}}</td>
                                            <td>{{$applicant_panelist->email}}</td>
                                            <td>{{$applicant_panelist->company_name}}</td>
                                            <td>{{$applicant_panelist->position}}</td>
                                        </tr>
                                    @endforeach
                                </table>


                            </div>
                            <br>
                            <div class="row center">
                                <h5 id="add-panelist-header">Want to add more panelists?</h5>
                                <div class="input-field col s4 center" style="margin-left: 450px">
                                    <input type="email" id="extra-panelist-email-input" class="validate">
                                    <label for="email">Enter email address of the person</label>
                                    <btn  class="btn blue" onclick="appendPanelist()">Add</btn>
                                </div>
                            </div>
                            <br>
                        </div>
                    @else
                        <div class="row center">
                            <h4><b>ASSIGN PANEL FOR {{$bootcamper->user->name}} {{$bootcamper->user->surname}} </b></h4>
                        </div>
                        <div class="container-fluid">
                            <input id="bootcamper-id-input" value="{{$bootcamper->id}}" hidden disabled>
                            <div class="row center">
                                <div class="col l6 m6 s12">
                                    <select id="contact-input">
                                        <option value="" disabled selected>How did you contact {{$bootcamper->user->name}} {{$bootcamper->user->surname}}?</option>
                                        <option value="Phone Call">Phone Call</option>
                                        <option value="Face-to-Face">Face-to-Face</option>
                                        <option value="Email">Email</option>
                                        <option value="Pitch video">Pitch video</option>
                                    </select>
                                </div>
                                <div class="col l6 m6 s12">
                                    <select id="contact-result-input">
                                        <option value="" disabled selected>What was the result?</option>
                                        <option value="Panel Interview">Panel Interview</option>
                                        <option value="Application Declined">App - Declined</option>
                                        <option value="Application Referred">App - Referred</option>
                                        <option value="Interview Declined">Interview - Declined</option>
                                        <option value="Interview Referred">Interview - Referred</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="section" id="panel-interview-row" hidden>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <select id="panelist-input" multiple="multiple">
                                        <option value="" disabled selected>Panelists?</option>
                                        <option value="grant@propellaincubator.co.za">Grant Minnie</option>
                                        <option value="anita@propellaincubator.co.za">Anita Palmer</option>
                                        <option value="woosthuizen@engeli.co.za">Wayne Oosthuizen</option>
                                        <option value="bwiseman@engeli.co.za">Barry Wiseman</option>
                                        <option value="rdames@engeli.co.za">Ricardo Dames</option>
                                        <option value="errol@propellaincubator.co.za">Error Wills</option>
                                        <option value="daryl.mcwilliams@gmail.com">Daryl McWilliams</option>
                                        <option value="linda1@propellaincubator.co.za">Linda Lawrie</option>
                                        <option value="Nqobile.Gumede@mandela.ac.za">Nqobile Gumede</option>
                                        <option value="Mante.Kgaria@mandela.ac.za">Mante Kgaria</option>
                                    </select>

                                    <label for="panel-selection-date-input">Date of panel interview</label>
                                    <input type="date" id="panel-selection-date-input">

                                    <label for="panel-selection-time-input">Time of panel interview</label>
                                    <input type="time" id="panel-selection-time-input">
                                </div>
                                <div class="col l6 m6 s12">
                                    <div class="row">
                                        <select id="question-category-input">
                                            <option value="" disabled selected>Choose Questions</option>
                                            @foreach($question_categories as $question_category)
                                                <option value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                            @endforeach
                                        </select>
                                        <label for="question-category-input">Questions Category</label>
                                    </div>
                                    <button class="btn blue" id="show-guest-panelist-button">Add Guest Panelist</button>
                                    <div class="container" id="guest-panelist-container" hidden>
                                        <div class="row">
                                            <div class="col l6 m6 s12">
                                                <select id="title-input">
                                                    <option value="" disabled selected>Choose Title</option>
                                                    <option value="Mr">Mr</option>
                                                    <option value="Mrs">Mrs</option>
                                                    <option value="Miss">Miss</option>
                                                    <option value="Ms">Ms</option>
                                                    <option value="Dr">Dr</option>
                                                </select>
                                                <label>Title</label>
                                            </div>
                                            <div class="col l6 m6 s12">
                                                <input id="initial-input" name="initial-input" type="text" class="validate">
                                                <label for="initial-input">Initials</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col l6 m6 s12">
                                                <input id="name-input" name="name-input" type="text" class="validate">
                                                <label for="name-input">First Name</label>
                                            </div>
                                            <div class="col l6 m6 s12">
                                                <input id="surname-input" name="surname-input" type="text" class="validate">
                                                <label for="surname-input">Last Name</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col l6 m6 s12">
                                                <input id="company-input" name="company-input" type="text" class="validate">
                                                <label for="company-input">Company Name</label>
                                            </div>
                                            <div class="col l6 m6 s12">
                                                <input id="position-input" name="position-input" type="text" class="validate">
                                                <label for="position-input">Position</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col l6 m6 s12">
                                                <input id="email-input" name="email-input" type="email" class="validate">
                                                <label for="email-input">Email</label>
                                            </div>
                                            <div class="col l6 m6 s12">
                                                <input id="contact-number-input" name="contact-number-input" type="text" class="validate">
                                                <label for="contact-number-input">Contact Number</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <button class="btn blue" id="add-guest-panelist-button">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button class="btn blue" id="panel-interview-button">Finalize</button>
                            </div>
                        </div>


                        <div class="row" id="panel-interview-declined-row" hidden>
                            <div class="col l6 m6 s12">
                                <label for="decline-reason-input">Reason it was declined?</label>
                                <textarea id="decline-reason-input"></textarea>
                                <button class="btn blue" id="panel-interview-declined-button">Submit</button>
                            </div>
                        </div>

                        <div class="row" id="panel-interview-declined-but-referred-row" hidden>
                            <div class="col l6 m6 s12">
                                <label for="decline-reason-input">Reason it was declined?</label>
                                <textarea id="decline-but-referred-reason-input"></textarea>
                            </div>
                            <div class="col l6 m6 s12">
                                <label for="referred-company-input">Who it was referred to?</label>
                                <textarea id="referred-company-input"></textarea>
                                <button class="btn blue" id="panel-interview-declined-but-referred-button">Submit</button>
                            </div>
                        </div>
                    @endif

                </div>
            </div>

            <div class="row center" hidden id="no-go-panel-row" style="width: 170vh;margin-left: 120px;">
                <div class="row center">
                    <h4><b>NO GO PANEL</b></h4>
                </div>
                <div class="row center" >
                    <div class="col l6 m6 s12">
                        <select id="panel-contact-input">
                            <option value="" disabled selected>How did you contact {{$bootcamper->user->name}} {{$bootcamper->user->surname}}?</option>
                            <option value="Phone Call">Phone Call</option>
                            <option value="Face-to-Face">Face-to-Face</option>
                            <option value="Email">Email</option>
                        </select>
                    </div>
                    <div class="col l6 m6 s12">
                        <select id="panel-contact-result-input">
                            <option value="" disabled selected>What was the result?</option>
                            <option value="Stage 2 - Declined">Stage 2 - Declined</option>
                            <option value="Stage 2 - Referred">Stage 2 - Referred</option>
                            <option value="Bootcamper invited but did not attend">Bootcamper invited but did not attend</option>
                        </select>
                    </div>
                </div>
                <div class="col l6 m6 s12">
                    <label for="panel-decline-reason-input">Reason it was declined?</label>
                    <textarea id="panel-decline-but-referred-reason-input"></textarea>
                </div>
                <div class="col l6 m6 s12">
                    <label for="panel-referred-company-input">Who it was referred to?</label>
                    <textarea id="panel-referred-company-input"></textarea>
                    <button class="btn blue" id="set-bootcamper-to-declined-bootcamper-button">Submit</button>
                </div>
            </div>
        </div>
        {{--GO/NO GO INCUBATEE--}}
        <div class="section" id="go-no-go-incubatee-row" hidden>
            <input hidden disabled id="bootcamper-id-input" value="{{$bootcamper->id}}">
            <br>
            <div class="row center">
                <h4><b>GO / NO GO INCUBATEE</b></h4>
            </div>
            <div class="row center">
                <div class="col l6 m6 s12">
                    <i class="material-icons large" style="color: saddlebrown;" onclick="approveApplicant()">thumb_up</i>
                    <h3>Go?</h3>
                </div>
                <div class="col l6 m6 s12">
                    <i class="material-icons large" style="color: saddlebrown;" onclick="declineApplicant()">thumb_down</i>
                    <h3>No Go?</h3>
                </div>
            </div>

            <div class="row center" hidden id="go-row" style="width: 170vh;margin-left: 120px;">
                <div class="row center">
                    <h4><b> GO INCUBATEE</b></h4>
                </div>
                <div class="col s12 m6 l6" style="width:50%;">
                    <select id="venture_category">
                        <option value="" disabled selected>Choose Venture category</option>
                        @foreach($venture_categories as $v_category)
                            <option value="{{$v_category->id}}">{{$v_category->category_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col s12 m6 l6" style="width:50%;">
                    <select id="incubatee_stage">
                        <option value="" disabled selected>Choose Venture stage</option>
                        @foreach($incubatee_stages as $i_stage)
                            <option value="{{$i_stage->id}}">{{$i_stage->stage_name}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn blue" id="submit-chosen-category-button">submit</button>
            </div>

            <div class="row center" hidden id="no-go-row" style="width: 170vh;margin-left: 120px;">
                <div class="row center">
                    <h4><b>NO GO INCUBATEE</b></h4>
                </div>
                <div class="row center">
                    <div class="col l6 m6 s12">
                        <select id="incubatee-contact-input">
                            <option value="" disabled selected>How did you contact {{$bootcamper->user->name}} {{$bootcamper->user->surname}}?</option>
                            <option value="Phone Call">Phone Call</option>
                            <option value="Face-to-Face">Face-to-Face</option>
                            <option value="Email">Email</option>
                        </select>
                    </div>
                    <div class="col l6 m6 s12">
                        <select id="incubatee-contact-result-input">
                            <option value="" disabled selected>What was the result?</option>
                            <option value="Stage 2 - Declined">Stage 2 - Declined</option>
                            <option value="Stage 2 - Referred">Stage 2 - Referred</option>
                            <option value="Bootcamper invited but did not attend">Bootcamper invited but did not attend</option>
                        </select>
                    </div>
                </div>
                <div class="col l6 m6 s12">
                    <label for="incubatee-decline-reason-input">Reason it was declined?</label>
                    <textarea id="incubatee-decline-but-referred-reason-input"></textarea>
                </div>
                <div class="col l6 m6 s12">
                    <label for="incubatee-referred-company-input">Who it was referred to?</label>
                    <textarea id="incubatee-referred-company-input"></textarea>
                    <button class="btn blue" id="set-bootcamper-to-declined-bootcamper">Submit</button>
                </div>
            </div>
        </div>
        {{--SHOW BOOTCAMPER PANELIST--}}
        <div class="row" id="panel-results-row" hidden style="width: 170vh;margin-left: 120px;">
            <div class="section" style="background-color: grey; margin-bottom: 3em;">
                <input id="bootcamper-id-input" value="{{$bootcamper->id}}" hidden disabled>
                <div class="row center">
                    <h5>Panelists assigned to Bootcamper</h5>

                    @foreach($final_panelist_question_score_array as $panelist)
                        <div class="col l3 m6 s12 center">
                            <div class="card">
                                <div class="card-content">
                                    <h5>{{$panelist->name}} {{$panelist->surname}}</h5>
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <h6>Question Number</h6>
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <h6>Question Score</h6>
                                        </div>
                                    </div>
                                    @if(isset($panelist->user_questions_scores))
                                        @foreach($panelist->user_questions_scores as $question_answer)
                                            <div class="row">
                                                <div class="col l6 m6 s12" style="background-color: darkgray;">
                                                    <p>{{$question_answer->question_number}}</p>
                                                </div>
                                                <div class="col l6 m6 s12" style="background-color: dimgray;">
                                                    <p>{{$question_answer->question_score}}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <h6>Total Score</h6>
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <h6>{{isset($panelist->panelist_applicant_score) ? $panelist->panelist_applicant_score : 0}}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h5>Total Combined Score: {{$total_panelist_score}}</h5>
                        </div>
                        <div class="col l6 m6 s12">
                            <h5>Total Average Score: {{$average_panelist_score}}</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section" style="margin-bottom: 3em; margin-top: 3em;">
                <div class="row center">
                    <h5> Combined scores per question </h5>
                    <table id="t01">
                        <tr>
                            <th>Question Number</th>
                            <th>Total Score</th>
                        </tr>
                        @foreach($question_scores_array as $question_score)
                            <tr>
                                <td>{{$question_score->question_number}}</td>
                                <td>{{$question_score->question_score}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="section" style="background-color: grey; margin-top: 3em;">
                <div class="row">
                    <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Want to see more?</h6>
                </div>
                <div class="row" style="margin-left: 2em;margin-right: 2em;">
                    <div class="col s12">
                        <table class="table table-bordered" style="width: 100%!important;" id="bootcamper-panelists-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Selection Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{--BOOTCAMPER EVALUATION FORM--}}
        <div class="col s12 card" id="bootcamp-evaluation-row" hidden style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <div class="row" style="margin-right: 3em; margin-left: 3em;">
                <div class="container" >
                    <div class="row">
                        <div class="row center">
                            <br>
                            <h4><b>PRE-BOOTCAMP EVALUATION FORM</b></h4>
                        </div>
                        @if(count($beginning_of_bootcamp_evaluation_question_array) > 0)
                            @foreach($beginning_of_bootcamp_evaluation_question_array as $beginning_of_bootcamp_evaluation)
                                <h6>{{$beginning_of_bootcamp_evaluation->question_number}} - {{ $beginning_of_bootcamp_evaluation->question_text}} </h6>
                                <div class="input-field">
                                    <textarea disabled>{{$beginning_of_bootcamp_evaluation->answer_text}}</textarea>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="row" style="margin-right: 3em; margin-left: 3em;">
                <div class="container" >
                    <div class="row">
                        <div class="row center">
                            <br>
                            <h4><b>POST-BOOTCAMP EVALUATION FORM</b></h4>
                        </div>
                        @if(count($post_of_bootcamp_evaluation_question_array) > 0)
                            @foreach($post_of_bootcamp_evaluation_question_array as $post_of_bootcamp_evaluation_question_arrays)
                                <h6>{{$post_of_bootcamp_evaluation_question_arrays->question_number}} - {{ $post_of_bootcamp_evaluation_question_arrays->question_text}} </h6>
                                <div class="input-field">
                                    <textarea disabled>{{$post_of_bootcamp_evaluation_question_arrays->answer_text}}</textarea>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $('.status-color:contains("Closed")').css('color', 'red');
        $('.status-color:contains("Pending")').css('color', 'green');

        let user_id = $('#bootcamper-id-input').val();
        $('#contact-result-input').on('change', function(){
            if($('#contact-result-input').val() === "Panel Interview"){
                $('#panel-interview-row').show();
                $('#panel-interview-declined-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Declined" || $('#contact-result-input').val() === "Application Declined"){
                $('#panel-interview-declined-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Referred" || $('#contact-result-input').val() === "Application Referred"){
                $('#panel-interview-declined-but-referred-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-row').hide();
            }
        });

        $('#show-guest-panelist-button').on('click', function(){
            $('#show-guest-panelist-button').hide();
            $('#guest-panelist-container').show();
        });

        //panel decision declined

        //Approve Applicant

        $('#add-guest-panelist-button').on('click', function(){
            let title = $('#title-input').val();
            let initials = $('#initial-input').val();
            let name = $('#name-input').val();
            let surname = $('#surname-input').val();
            let position = $('#position-input').val();
            let company = $('#company-input').val();
            let email = $('#email-input').val();
            let contact_number = $('#contact-number-input').val();

            let object = {
                title: title,
                initials: initials,
                name: name,
                surname: surname,
                position: position,
                company: company,
                email: email,
                contact_number: contact_number
            };

            guest_panelists.push(object);

            $('#title-input').val("");
            $('#initial-input').val("");
            $('#name-input').val("");
            $('#surname-input').val("");
            $('#position-input').val("");
            $('#company-input').val("");
            $('#email-input').val("");
            $('#contact-number-input').val("");
        });

        <!--Finalize button-->

        $('#panel-interview-button').on('click', function(){
            let selected_panelists = [];
            let formData = new FormData();

            jQuery.each(jQuery('#panelist-input').val(), function (i, value) {
                selected_panelists.push(value);
            });

            formData.append('contacted_via', $('#contact-input').val());
            formData.append('result_of_contact', $('#contact-result-input').val());
            formData.append('selected_panelists', JSON.stringify(selected_panelists));
            formData.append('guest_panelists', JSON.stringify(guest_panelists));
            formData.append('panel_selection_date', $('#panel-selection-date-input').val());
            formData.append('panel_selection_time', $('#panel-selection-time-input').val());
            formData.append('question_category_id', $('#question-category-input').val());


            let url = "/bootcamper-panel-interview-approve/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#panel-interview-button').notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/bootcampers';
                    }, 3000);
                },

                error: function (response) {
                    $('#panel-interview-button').notify(response.message, "error");
                }
            });
        });

        //Decline Applicant
        $('#panel-interview-declined-button').on('click', function(){
            let formData = new FormData();
            formData.append('contacted_via', $('#contact-input').val());
            formData.append('result_of_contact', $('#contact-result-input').val());
            formData.append('declined_reason_text', $('#decline-reason-input').val());

            let url = "/bootcamper-panel-interview-decline/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#panel-interview-declined-button').notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/applicants';
                    }, 3000);
                },

                error: function (response) {
                    $('#panel-interview-declined-button').notify(response.message, "error");
                }
            });
        });

        //Decline but refer
        $('#panel-interview-declined-but-referred-button').on('click', function(){
            let formData = new FormData();
            formData.append('contacted_via', $('#contact-input').val());
            formData.append('result_of_contact', $('#contact-result-input').val());
            formData.append('declined_reason_text', $('#decline-but-referred-reason-input').val());
            formData.append('referred_company', $('#referred-company-input').val());

            let url = "/applicant-panel-interview-decline-but-refer/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#panel-interview-declined-but-referred-button').notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/applicants';
                    }, 3000);
                },

                error: function (response) {
                    $('#panel-interview-declined-but-referred-button').notify(response.message, "error");
                }
            });
        });

        //Update panel interview details
        $('#update-panel-interview-details-button').on('click', function(){
            let formData = new FormData();
            let date = $('#panel-selection-date-display').val();
            let time = $('#panel-selection-time-display').val();
            let contact = $('#contact-input-display').val();
            let contact_result = $('#contact-result-input-display').val();
            let question_category_id = $('#question-category-display').val();

            formData.append('date', date);
            formData.append('time', time);
            formData.append('contact', contact);
            formData.append('contact_result', contact_result);
            formData.append('question_category_id', question_category_id);

            let url = "/update-panel-interview-details/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-panel-interview-details-button').notify(response.message, "success");
                },
                error: function (response) {
                    $('#update-panel-interview-details-button').notify(response.message, "error");
                }
            });

        });

        function deletePanelist(obj){
            var r = confirm("Are you sure want to remove this panelist?");
            if (r === true) {
                $.get('/admin-remove-panelist-from-ict-applicant/'+obj.id,function(data,status){
                    console.log('Data',data);
                    if(status==='success'){
                        alert(data.message);
                        window.location.reload();
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }

        //Append panel
        function appendPanelist(){
            let email = $('#extra-panelist-email-input').val();
            let bootcamper_id = $('#bootcamper-id-input').val();
            let question_category_id = $('#question-category-id').val();
            let url = '/admin-add-panelist-from-panel-access-window';
            let formData = new FormData();
            formData.append('email', email);
            formData.append('bootcamper_id', bootcamper_id);
            formData.append('panel_selection_date', $('#panel-selection-date-display').val());
            formData.append('panel_selection_time', $('#panel-selection-time-display').val());
            formData.append('question_category_id', question_category_id);

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#add-panelist-header').notify(response.message + ". Page refresshing in 3 seconds.", "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    $('#add-panelist-header').notify(response.message, "error");
                }
            });
        }


        $(document).ready(function () {
            $('select').formSelect();

            $(function () {
                let bootcamper_id = $('#bootcamper-id-input').val();
                $('#bootcamper-panelists-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/get-ict-applicant-panelists/' + bootcamper_id,
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'selection_date', name: 'selection_date'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="bootcamper-panelists-table_length"]').css("display","inline");
            });
        });

        $('#contact-result-input').on('change', function(){
            if($('#contact-result-input').val() === "Panel Interview"){
                $('#panel-interview-row').show();
                $('#panel-interview-declined-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Declined" || $('#contact-result-input').val() === "Application Declined"){
                $('#panel-interview-declined-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Referred" || $('#contact-result-input').val() === "Application Referred"){
                $('#panel-interview-declined-but-referred-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-row').hide();
            }
        });

        $('#show-guest-panelist-button').on('click', function(){
            $('#show-guest-panelist-button').hide();
            $('#guest-panelist-container').show();
        });

        //Submit Panel Interview Buttons

        //Approve Applicant
        let guest_panelists = [];

        $('#add-guest-panelist-button').on('click', function(){
            let title = $('#title-input').val();
            let initials = $('#initial-input').val();
            let name = $('#name-input').val();
            let surname = $('#surname-input').val();
            let position = $('#position-input').val();
            let company = $('#company-input').val();
            let email = $('#email-input').val();
            let contact_number = $('#contact-number-input').val();

            let object = {
                title: title,
                initials: initials,
                name: name,
                surname: surname,
                position: position,
                company: company,
                email: email,
                contact_number: contact_number
            };

            guest_panelists.push(object);

            $('#title-input').val("");
            $('#initial-input').val("");
            $('#name-input').val("");
            $('#surname-input').val("");
            $('#position-input').val("");
            $('#company-input').val("");
            $('#email-input').val("");
            $('#contact-number-input').val("");
        });


        //Update panel interview details
        $('#update-panel-interview-details-button').on('click', function(){
            let formData = new FormData();
            let date = $('#panel-selection-date-display').val();
            let time = $('#panel-selection-time-display').val();
            let contact = $('#contact-input-display').val();
            let contact_result = $('#contact-result-input-display').val();

            formData.append('date', date);
            formData.append('time', time);
            formData.append('contact', contact);
            formData.append('contact_result', contact_result);

            let url = "/update-panel-interview-details/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-panel-interview-details-button').notify(response.message, "success");
                },
                error: function (response) {
                    $('#update-panel-interview-details-button').notify(response.message, "error");
                }
            });

        });

        function deletePanelist(obj){
            var r = confirm("Are you sure want to remove this panelist?");
            if (r === true) {
                $.get('/admin-remove-panelist-from-ict-applicant/'+obj.id,function(data,status){
                    console.log('Data',data);
                    if(status==='success'){
                        alert(data.message);
                        window.location.reload();
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }

        //Append panel
        function appendPanelist(){
            let email = $('#extra-panelist-email-input').val();
            let bootcamper_id = $('#bootcamper-id-input').val();
            let question_category_id = $('#question-category-id').val();
            let url = '/admin-add-panelist-from-panel-access-window';
            let formData = new FormData();
            formData.append('email', email);
            formData.append('bootcamper_id', bootcamper_id);
            formData.append('panel_selection_date', $('#panel-selection-date-display').val());
            formData.append('panel_selection_time', $('#panel-selection-time-display').val());
            formData.append('question_category_id', question_category_id);

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#add-panelist-header').notify(response.message + ". Page refresshing in 3 seconds.", "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    $('#add-panelist-header').notify(response.message, "error");
                }
            });
        }
        $(document).ready(function () {
            //Bootcamper contact log
            $('#bootcamper-contact-log-submit-button').on('click',function () {
                let bootcamper_id =$('#bootcamper-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('date', $('#date').val());
                formData.append('bootcamper_contact_results', $('#bootcamper_contact_results').val());
                console.log(formData);

                let url = '/store-bootcamper-contact-log/' + bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $("#bootcamper-contact-log-submit-button").notify(
                            "You have successfully log a contact", "success",
                            { position:"right" }
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                });
            });

            //Status save
            $('#status').on('change',function () {
                let bootcamper_id =$('#bootcamper-id-input').val();

                let formData = new FormData();
                formData.append('status', $('#status').val());

                let url = '/update-bootcamper-status/' + bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $("#status").notify(
                            "You have successfully updated status", "success",
                            { position:"right" }
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                });
            });

            let bootcamper_id = $('#bootcamper-id-input').val();
            $(function () {
                $('#bootcamper-event-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    // scrollX: 640,
                    ajax: '/get-bootcamper-events/' + bootcamper_id,
                    columns: [
                        {data: 'event', name: 'event'},
                        {data: 'date_registered', name: 'date_registered'},
                        {data: 'accepted', name: 'accepted'},
                        {data: 'attended', name: 'attended'},
                        {data: 'declined', name: 'declined'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="bootcamper-event-table_length"]').css("display","inline");
            });
        });

        $('.register-bootcamper').on('click', function(){
            let event_id = this.getAttribute('data-value');
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('event_id', event_id);
            formData.append('bootcamper_id', bootcamper_id);

            let url = '/admin-register-bootcamper-event';
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('.register-bootcamper').notify(response.message, "success");
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        function confirm_delete_bootcamper_event(obj){
            var r = confirm("Are you sure want to delete this registration?");
            if (r === true) {
                $.get('/bootcamper-event-delete/' + obj.id, function (data, status) {
                    if (status === 'success') {
                        alert(data.message);
                        window.location.reload();
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }


        function printContent(el) {
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }

        function approveApplicant(){
            $('#go-row').show();
            $('#no-go-row').hide();
        }

        function declineApplicant(){
            $('#go-row').hide();
            $('#no-go-row').show();
        }

        function approvePanel(){
            $('#go-panel-row').show();
            $('#no-go-panel-row').hide();
        }

        function declinePanel(){
            $('#go-panel-row').hide();
            $('#no-go-panel-row').show();
        }

        //PANEL CONTACT RESULT INPUT
        $('#panel-contact-result-input').on('change', function(){
            if($('#panel-contact-result-input').val() === "Panel decision declined"){
                $('#panel-decision-declined').show();
                $('#panel-declined-button').show();
                $('#panel-referred-applicants').hide();
                $('#panel-referred-button-section').hide();
            }else if($('#panel-contact-result-input').val() === "Panel decision referred"){
                $('#panel-referred-applicants').show();
                $('#panel-referred-button-section').show();
                $('#panel-decision-declined').hide();
                $('#panel-declined-button').hide();
            }

        });

        $('#submit-chosen-category-button').on('click', function(){
            let category_id = $('#venture_category').val();
            let user_id = $('#user-id-input').val();
            let stage_id = $('#incubatee_stage').val();
            let bootcamper_id =$('#bootcamper-id-input').val();
            let r = confirm("Are you sure you want to give this bootcamper incubatee permissions?");
            if(r === true){
                $('#submit-chosen-category-button').text("loading...");
                let formData = new FormData();
                formData.append('category_id', category_id);
                formData.append('stage_id', stage_id);

                let url = '/set-bootcamper-to-incubatee/' + bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#submit-chosen-category-button").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/bootcampers';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        //PANEL DECLINED BUTTON
        $('#set-bootcamper-to-declined-bootcamper-button').on('click', function(){
            let r = confirm("Are you sure you want to decline this bootcamper?");

            if(r === true){
                $('#set-bootcamper-to-declined-bootcamper-button').text("Please wait");
                let formData = new FormData();
                formData.append('contacted_via', $('#panel-contact-input').val());
                formData.append('result_of_contact', $('#panel-contact-result-input').val());
                formData.append('declined_reason_text', $('#panel-decline-but-referred-reason-input').val());
                formData.append('referred_company', $('#panel-referred-company-input').val());

                let bootcamper_id = $('#bootcamper-id-input').val();
                let url = "/set-bootcamper-to-declined-bootcamper/" + bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#set-bootcamper-to-declined-bootcamper-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/bootcampers';
                        }, 3000);
                    },

                    error: function (response) {
                        let message = response.message;
                        alert(message);
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        //INCUBATEE DECLINED BUTTON
        $('#set-bootcamper-to-declined-bootcamper').on('click', function(){
            let r = confirm("Are you sure you want to decline this bootcamper?");

            if(r === true){
                $('#set-bootcamper-to-declined-bootcamper-button').text("Please wait");
                let formData = new FormData();
                formData.append('contacted_via', $('#incubatee-contact-input').val());
                formData.append('result_of_contact', $('#incubatee-contact-result-input').val());
                formData.append('declined_reason_text', $('#incubatee-decline-but-referred-reason-input').val());
                formData.append('referred_company', $('#incubatee-referred-company-input').val());

                let bootcamper_id = $('#bootcamper-id-input').val();
                let url = "/set-bootcamper-to-declined-bootcamper/" + bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#set-bootcamper-to-declined-bootcamper').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/bootcampers';
                        }, 3000);
                    },

                    error: function (response) {
                        let message = response.message;
                        alert(message);
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        $('#application').on('click', function () {
            $('#application-details-row').show();
            $('#bootcamper-uploads-row').hide();
            $('#bootcamper-events-row').hide();
            $('#go-no-go-incubatee-row').hide();
            $('#panel-results-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#contact-log-row').hide();
            $('#bootcamp-evaluation-row').hide();
        });

        $('#uploads').on('click', function () {
            $('#bootcamper-uploads-row').show();
            $('#application-details-row').hide();
            $('#bootcamper-events-row').hide();
            $('#go-no-go-incubatee-row').hide();
            $('#panel-results-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#contact-log-row').hide();
            $('#bootcamp-evaluation-row').hide();
        });

        $('#events').on('click', function () {
            $('#bootcamper-events-row').show();
            $('#application-details-row').hide();
            $('#bootcamper-uploads-row').hide();
            $('#go-no-go-incubatee-row').hide();
            $('#panel-results-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#contact-log-row').hide();
            $('#bootcamp-evaluation-row').hide();
        });

        $('#go-no-go-incubatee').on('click', function () {
            $('#go-no-go-incubatee-row').show();
            $('#bootcamper-events-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-uploads-row').hide();
            $('#panel-results-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#contact-log-row').hide();
            $('#bootcamp-evaluation-row').hide();
        });

        $('#panel-results').on('click', function () {
            $('#panel-results-row').show();
            $('#go-no-go-incubatee-row').hide();
            $('#bootcamper-events-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-uploads-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#contact-log-row').hide();
            $('#bootcamp-evaluation-row').hide();
        });

        $('#go-no-go-panel').on('click', function () {
            $('#go-no-go-panel-row').show();
            $('#panel-results-row').hide();
            $('#go-no-go-incubatee-row').hide();
            $('#bootcamper-events-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-uploads-row').hide();
            $('#contact-log-row').hide();
            $('#bootcamp-evaluation-row').hide();
        });

        $('#contact-log').on('click', function () {
            $('#contact-log-row').show();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#go-no-go-incubatee-row').hide();
            $('#bootcamper-events-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-uploads-row').hide();
            $('#bootcamp-evaluation-row').hide();
        });
        $('#evaluation').on('click', function () {
            $('#bootcamp-evaluation-row').show();
            $('#contact-log-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#go-no-go-incubatee-row').hide();
            $('#bootcamper-events-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-uploads-row').hide();
        });

    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
