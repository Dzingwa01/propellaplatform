@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>

    {{ Breadcrumbs::render('advisor-bootcamper-to-declined',$bootcamper)}}
    <div class="row center" id="no-go-row">
        <input hidden disabled id="bootcamper-id-input" value="{{$bootcamper->id}}">
        <br>
        <br>


        <div class="row center">
            <div class="col l6 m6 s12">
                <select id="contact-input">
                    <option value="" disabled selected>How did you contact {{$user->name}} {{$user->surname}}?</option>
                    <option value="Phone Call">Phone Call</option>
                    <option value="Face-to-Face">Face-to-Face</option>
                    <option value="Email">Email</option>
                </select>
            </div>
            <div class="col l6 m6 s12">
                <select id="contact-result-input">
                    <option value="" disabled selected>What was the result?</option>
                    <option value="Application Declined">App - Declined</option>
                    <option value="Application Referred">App - Referred</option>
                    <option value="Interview Declined">Interview - Declined</option>
                    <option value="Interview Referred">Interview - Referred</option>
                </select>
            </div>
        </div>
        <div class="col l6 m6 s12">
            <label for="decline-reason-input">Reason it was declined?</label>
            <textarea id="decline-but-referred-reason-input"></textarea>
        </div>
        <div class="col l6 m6 s12">
            <label for="referred-company-input">Who it was referred to?</label>
            <textarea id="referred-company-input"></textarea>
            <button class="btn blue" id="set-bootcamper-to-declined-applicant-button">Submit</button>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

        $('#set-bootcamper-to-declined-applicant-button').on('click', function(){
            let r = confirm("Are you sure you want to set this bootcamper to declined applicant?");

            if(r === true){
                $('#set-bootcamper-to-declined-applicant-button').text("Loading...");
                let formData = new FormData();
                formData.append('contacted_via', $('#contact-input').val());
                formData.append('result_of_contact', $('#contact-result-input').val());
                formData.append('declined_reason_text', $('#decline-but-referred-reason-input').val());
                formData.append('referred_company', $('#referred-company-input').val());

                let bootcamper_id = $('#bootcamper-id-input').val();
                let url = "/set-bootcamper-to-declined-applicant/" + bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#decline-but-referred-reason-input').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/bootcampers';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#decline-but-referred-reason-input').notify(response.message, "error");
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
