@extends('layouts.advisor-layout')

@section('content')
    <div class="container" id="QuestionsAndAnswers">
        <br>
        <div class="row center">
            <h4><u>Applicant Details:</u></h4>
            <div class="row">
                <div class="col l6 m6 s12">
                    <h6>{{$user->name}} {{$user->surname}}</h6>
                    <h6>Category: {{$applicant->chosen_category}}</h6>
                    <h6>Email: {{$user->email}}</h6>
                    <h6>Contact Number: {{$user->contact_number}}</h6>
                    <h6>ID Number: {{$user->id_number}}</h6>
                    <h6>Age: {{$user->age}}</h6>
                    <h6>Gender: {{$user->gender}}</h6>
                    <h6>Race: {{$user->race}}</h6>
                </div>
                <div class="col l6 m6 s12">
                    <h6>Address One: {{$user->address_one}}</h6>
                    <h6>Address Two: {{$user->address_two}}</h6>
                    <h6>Address Three: {{$user->address_three}}</h6>
                    <h6>Application Created: {{$applicant->created_at}}</h6>
                </div>
            </div>
            <br>
            <hr style="background: darkblue; height: 5px;">
            <br>
        </div>

        <div class="row">
            @foreach($userQuestionAnswers as $question_answer)
                <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                <div class="input-field">
                    <textarea disabled>{{$question_answer->answer_text}}</textarea>
                </div>
            @endforeach
        </div>
    </div>

    <br>
    <div class="row center">
        <button id="print-button"  class="waves-effect waves-light btn" onclick="printContent('QuestionsAndAnswers')">
            <i class="material-icons left">local_printshop</i>Print
        </button>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }
    </script>

@endsection
