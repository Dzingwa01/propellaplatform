@extends('layouts.advisor-layout')
@section('content')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <br>
    <br>
    {{ Breadcrumbs::render('advisor-edit-declined-bootcamper')}}
    <br/>
    <br>
    <br>
    <div class="container-fluid">
        <form id="basic-info-form">
            <div class="card" style="margin-left: 4em;margin-right: 4em">
                <input value="{{$declinedBootcamper->id}}" id="d_a_id" hidden>
                <br/>
                <div class="row center">
                    <h4>Edit Personal Information</h4>
                    <div class="col l12 m12 s12">
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <select id="title" required>
                                    <option value="{{$declinedBootcamper->title}}" selected>{{$declinedBootcamper->title}}</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Ms</option>
                                    <option value="Miss">Mr</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr">Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="initials" type="text" class="validate" value="{{$declinedBootcamper->initials}}"
                                       required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="name" type="text" value="{{$declinedBootcamper->name}}" class="validate"
                                       required>
                                <label for="name">First Name</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="surname" type="text" value="{{$declinedBootcamper->surname}}" class="validate"
                                       required>
                                <label for="surname">Last Name</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="age" type="number" value="{{$declinedBootcamper->age}}" class="validate" required>
                                <label for="age">Age</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <select id="gender" required>
                                    <option value="{{$declinedBootcamper->gender}}" selected>{{$declinedBootcamper->gender}}</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                                <label for="gender-desktop">Gender</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="id_number" type="text" value="{{$declinedBootcamper->id_number}}" class="validate"
                                       required>
                                <label for="id_number">ID Number</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <select id="contacted_via" required>
                                    <option value="{{$declinedBootcamper->contacted_via}}" selected>{{$declinedBootcamper->contacted_via}}</option>
                                    <option value="Phone Call">Phone Call</option>
                                    <option value="Face-to-face">Face-to-face</option>
                                    <option value="Email">Email</option>
                                    <option value="No response to any contact type">No response to any contact type</option>
                                    <option value="Invited, but did not arrive">Invited, but did not arrive</option>

                                </select>
                                <label for="contacted_via-desktop">Contacted via</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="email" type="email" disabled value="{{$declinedBootcamper->email}}"
                                       class="validate" required>
                                <label for="email">Email address</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="cell_number" type="text" value="{{$declinedBootcamper->contact_number}}"
                                       class="validate" required>
                                <label for="cell_number">Cell Number</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="address_one" type="text" value="{{$declinedBootcamper->address_one}}" class="validate"
                                       required>
                                <label for="address_one">Physical Address 1</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="address_two" type="text" value="{{$declinedBootcamper->address_two}}"
                                       class="validate" required>
                                <label for="address_two">Physical Address 2</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="address_three" type="text" value="{{$declinedBootcamper->address_three}}"
                                       class="validate" required>
                                <label for="address_three">Physical Address 3</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="city" type="text" value="{{$declinedBootcamper->city}}" class="validate" required>
                                <label for="city">City</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l6 m6 s12">
                                <input id="code" type="text" value="{{$declinedBootcamper->code}}" class="validate" required>
                                <label for="code">Postal Code</label>
                            </div>
                            <div class="input-field col l6 m6 s12">
                                <input id="declined_reason" type="text" value="{{$declinedBootcamper->declined_reason}}" class="validate" required>
                                <label for="declined_reason">Declined Reason</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 600px;">
                            <div class="col s4">
                                <a class="waves-effect waves-light btn" id="edit-question-category-submit-button">Save</a>
                            </div>
                        </div>
                        <br/>
                        <br/>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
            $('.tooltipped').tooltip();

            $('#edit-question-category-submit-button').on('click',function () {
                let formData = new FormData();

                formData.append('d_a_id', $('#d_a_id').val());
                formData.append('name', $('#name').val());
                formData.append('surname', $('#surname').val());
                formData.append('id_number', $('#id_number').val());
                formData.append('contact_number', $('#cell_number').val());
                formData.append('address_one', $('#address_one').val());
                formData.append('title', $('#title').val());
                formData.append('initials', $('#initials').val());
                formData.append('address_two', $('#address_two').val());
                formData.append('address_three', $('#address_three').val());
                formData.append('city', $('#city').val());
                formData.append('code', $('#code').val());
                formData.append('age', $('#age').val());
                formData.append('gender', $('#gender').val());
                formData.append('race', $('#race').val());
                formData.append('declined_reason', $('#declined_reason').val());
                formData.append('contacted_via', $('#contacted_via').val());

                console.log(formData);

                let url = '/update-declined-bootcamper-info/'+ '{{$declinedBootcamper->id}}';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $("#edit-question-category-submit-button").notify(
                            "You have successfully Update Declined Applicant", "success",
                            { position:"right" }
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });

            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
