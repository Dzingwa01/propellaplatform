@extends('layouts.advisor-layout')

@section('content')
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Bootcampers Panel Interviews</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="bootcampers-panel-interviews-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Panel Selection Date</th>
                        <th>Count of Panelists</th>
                        <th>Average Score</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#bootcampers-panel-interviews-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-bootcamper-panel-interviews')}}',
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'panel_selection_date', name: 'panel_selection_date'},
                        {data: 'panelist_count', name: 'panelist_count'},
                        {data: 'average_score', name: 'average_score'}
                    ]
                });
                $('select[name="bootcampers-panel-interviews-table_length"]').css("display","inline");
            });

        });

        function confirm_delete_bootcamper(obj) {
            var r = confirm("Are you sure want to delete this bootcamper?");
            if (r === true) {
                $.get('/bootcamper-delete/' + obj.id, function (data, status) {
                    if (status === 'success') {
                        alert(data.message);
                        window.location.reload();
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }

    </script>
@endsection
