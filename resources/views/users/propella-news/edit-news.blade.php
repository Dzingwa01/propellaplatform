@extends('layouts.admin-layout')

@section('content')
<head>
    <meta charset="UTF-8">
    <title>bootstrap4</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdn.tiny.cloud/1/h9arxjl10ypkgljjm33ddt16vik05kh57kxh07smgu9b7ftw/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            branding: false,
            placeholder: "Content here ..        P.S For better readability set font Size to 18pt and font to Arial",
            height: 700,
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker searchreplace preview insertdatetime anchor print charmap emoticons image imagetools help wordcount fullscreen colorpicker',
            toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table advancedlist directionality  insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image paste',
            imagetools_cors_hosts: ['thepropella.co.za', '127.0.0.1:8000'],
            toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',

            /* enable title field in the Image dialog*/
            image_title: true,
            /* enable automatic uploads of images represented by blob or data URIs*/
            automatic_uploads: true,
            /*
              URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
              images_upload_url: 'postAcceptor.php',
              here we add custom filepicker only to Image dialog
            */
            file_picker_types: 'image',
            /* and here's our custom image picker*/
            file_picker_callback: function (cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                input.onchange = function () {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.onload = function () {
                        /*
                          Note: Now we need to register the blob in TinyMCEs image blob
                          registry. In the next release this part hopefully won't be
                          necessary, as we are looking to handle it internally.
                        */
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);

                        /* call the callback and populate the Title field with the file name */
                        cb(blobInfo.blobUri(), { title: file.name });
                    };
                    reader.readAsDataURL(file);
                };

                input.click();
            },
        });
    </script>
</head>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
<br>
<br>
<div class="" style="margin-top: 10vh">
    <div class="card" style="width: 1070px;margin: 0 auto">
        <br>
        <h4 style="margin-left: 350px">Update News</h4>
        <input value="{{$propellaNew->id}}" id="blog_id" hidden>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="input-field col m6">
                <input id="title" type="text" class="validate" value="{{$propellaNew->title}}">
                <label for="title">Publication</label>
            </div>
            <div class="input-field col m6">
                <input id="description" type="text" class="validate" value="{{$propellaNew->description}}">
                <label for="description">News description</label>
            </div>
        </div>
        <div class="row" style="margin-right: 2em;margin-left: 2em;">
            <div class="file-field input-field col m6">
                <div class="btn" style="height: 50px;">
                    <span>News  Image</span>
                    <input type="file" id="news_image_url">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text" value="{{isset($propellaNew->news_image_url)?$propellaNew->news_image_url:''}}">
                </div>
            </div>
            <div class="file-field input-field col m6">
                <input id="added_date" type="date" class="validate" value="{{$propellaNew->added_date}}">
                <label for="added_date">Date</label>
            </div>

        </div>
        <div class="row" style="margin-left: 800px;">
            <div class="col s4">
                <a class="waves-effect waves-light btn section" id="news-upload-submit-button">Save</a>
            </div>
        </div>

        <br>
    </div>
</div>


<!--create summernote-->
<div class="container">
    <div class="panel-body">
        <form action="{{url('/update-news-content/'.$propellaNew->id)}}" method="POST" style="margin-top: 50px" id="summernote-form">
            <div class="form-group">
                <textarea id="summernote" name="content" class="summernote">{{$propellaNew->propellaNewsContent->content}}</textarea>
            </div>
            <div class="form-group text-center">
                <button class="waves-effect waves-light btn section ">Submit</button>
                {!!csrf_field()!!}
            </div>
        </form>
    </div>
</div>

@push('custom-scripts')
    <script>

        $(document).ready(function () {

            $('#news-upload-submit-button').on('click', function (e) {
                e.preventDefault();

                let formData = new FormData();

                formData.append('propella_news_id', $('#propella_news_id').val());
                formData.append('title', $('#title').val());
                formData.append('description', $('#description').val());
                formData.append('added_date', $('#added_date').val());
                formData.append('content', $('.content').val());

                jQuery.each(jQuery('#news_image_url')[0].files, function (i, file) {
                    formData.append('news_image_url', file);
                });

                console.log("propellaNew", formData);
                let url = '/update-news/' + '{{$propellaNew->id}}';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response, a, b) {
                        alert(response.message);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = error.response.message;
                        let errors = error.response.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);
                    }
                });
            });
            var content = {!! json_encode($propellaNew->propellaNewsContent->content) !!};
            $('.summernote').summernote('code', content);
            $('#summernote').summernote({
                placeholder: 'Content here ..',
                height: 1000,
            });
        });
    </script>
@endpush
</body>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>


</head>



<head>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
</head>

@endsection
