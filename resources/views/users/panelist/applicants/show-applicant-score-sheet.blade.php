@extends('layouts.panelist-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>

    <div class="section" style="margin-top: 2em;">
        <div class="card white" style="width: 1300px;margin: 0 auto">

            <input id="applicant-panelist-id-input" value="{{$applicant_panelist->id}}" hidden disabled>
            <div class="row center" style="margin-top: 5vh;">
                <br>
                <h5><b>Name & Surname : {{$applicant->user->name}} {{$applicant->user->surname}}</b></h5>
                <h5 ><b>{{$question_category->category_name}}</b></h5>
                <p style="color: red">*Once submitted, please wait for and click OK before proceeding</p>
                @if(isset($cur_applicant_panelist->panelistQuestionAnswers))
                    <div class="row">
                        <button class="btn blue" onclick="editScoresheet()">Edit Scoresheet</button>
                    </div>
                @endif
            </div>

            <div class="row" style="margin-left: 4em; margin-right: 4em;">
                <ul>
                    @foreach ($questions as $question)
                        @if(count($question->questionSubTexts) > 0)
                            <li>{{$question->question_number}} - {{ $question->question_text}} </li>
                            <br>
                            <div class="row">
                                @foreach($question->questionSubTexts as $question_sub)
                                    <div class="col l3 m3 s6" style="color: grey;border-style: solid">
                                        <p disabled>{{$question_sub->question_sub_text}}</p>
                                    </div>
                                @endforeach
                            </div>
                            <div class="input-field panel-question-answers-comment">
                                <input id="{{$question->id}}" class="materialize-textarea answer-text-desktop"
                                       style="width:10%" type="number" name="quantity" min="1" max="5">
                                <label for="answer-text-desktop">Score</label>
                                <div class="row">
                                    <textarea class="comments"></textarea>
                                </div>
                            </div>
                        @else
                            <li>{{$question->question_number}} - {{ $question->question_text}} </li>
                            <div class="input-field panel-question-answers-comment">
                                <input id="{{$question->id}}" class="materialize-textarea answer-text-desktop"
                                       style="width:10%" type="number" name="quantity" min="1" max="5">
                                <label for="answer-text-desktop">Score</label>

                                <textarea class="comments"></textarea>
                            </div>
                        @endif
                    @endforeach
                </ul>
            </div>

            <div class="row center-align">
                <div class="col  s12">
                    <p style="color: red; !important;">*Once you are sure of all your answers, click the "Finalize" button, this will submit your
                        scoresheet.</p>
                </div>
            </div>
            <div class="row" style="margin-left: 1100px">
                <div class="col s12">
                    <button class="btn waves-effect waves-light" id="upload-answers-form-desktop"> Finalize <i
                            class="material-icons right">send</i></button>
                </div>
                <br>
                <br>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large tooltipped waves-effect  btn modal-trigger red" data-tooltip="Save your progress" data-position="left" id="save-progress-button">Save your progress</a>
        </div>
    </div>


    <script>

        $(document).ready(function () {
            //Desktop Form
            $('#upload-answers-form-desktop').on('click', function () {
                let answersCommentsArray = [];
                let applicant_panelist_id = $('#applicant-panelist-id-input').val();

                $('.panel-question-answers-comment').each(function (e) {
                    let answer = this.getElementsByClassName('answer-text-desktop');
                    let comment = this.getElementsByClassName('comments');
                    let question_id = answer[0].id;

                    let model = {
                        question_id: question_id,
                        applicant_panelist_id: applicant_panelist_id,
                        answer_score: answer[0].value,
                        comment: comment[0].value
                    };

                    answersCommentsArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers_comments',JSON.stringify(answersCommentsArray));

                //To push the answersArray as data to the controller and save all the answers to the db
                $.ajax({
                    url: "/panelist-save-question-answer-comments",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        $('#upload-answers-form-desktop').notify(response.message, "success");
                    },

                    error: function (response) {
                        $('#upload-answers-form-desktop').notify(response.message, "error");
                    }
                });
            });

            $('#save-progress-button').on('click', function () {
                let answersCommentsArray = [];
                let applicant_panelist_id = $('#applicant-panelist-id-input').val();

                $('.panel-question-answers-comment').each(function (e) {
                    let answer = this.getElementsByClassName('answer-text-desktop');
                    let comment = this.getElementsByClassName('comments');
                    let question_id = answer[0].id;

                    let model = {
                        question_id: question_id,
                        applicant_panelist_id: applicant_panelist_id,
                        answer_score: answer[0].value,
                        comment: comment[0].value
                    };

                    answersCommentsArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers_comments',JSON.stringify(answersCommentsArray));

                //To push the answersArray as data to the controller and save all the answers to the db
                $.ajax({
                    url: "/panelist-save-question-answer-comments",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        $('#upload-answers-form-desktop').notify(response.message, "success");
                    },

                    error: function (response) {
                        $('#upload-answers-form-desktop').notify(response.message, "error");
                    }
                });
            });
        });

        function editScoresheet(){
            let applicant_panelist_id = $('#applicant-panelist-id-input').val();
            window.location.href = '/panelist-edit-scoresheet/' + applicant_panelist_id;
        }
    </script>

@endsection
