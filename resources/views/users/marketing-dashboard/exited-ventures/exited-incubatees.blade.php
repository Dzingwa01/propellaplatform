@extends('layouts.marketing-layout')

@section('content')
    <div class="container-fluid">
        <br>
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Exited Incubatees</h6>
        </div>
        <br>
        <div class="row" style="margin-left: 2em; margin-right: 2em;">
            <a href="/home" class="modal-close waves-effect waves-green btn">Home<i
                    class="material-icons right">home</i>
            </a>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script>
            $(document).ready(function () {
                $(function () {
                    $('#users-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-exited-incubatees')}}',
                        columns: [
                            {data: 'user.name', name: 'user.name'},
                            {data: 'user.surname', name: 'user.surname'},
                            {data: 'user.email', name: 'user.email'},
                            {data: 'user.contact_number', name: 'user.contact_number'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ],

                    });
                    $('select[name="users-table_length"]').css("display", "inline");
                });

            });

        </script>
    @endpush

@endsection
