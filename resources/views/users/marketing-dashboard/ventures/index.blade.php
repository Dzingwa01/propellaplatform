@extends('layouts.marketing-layout')
@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('marketing-venture')}}
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Venture</h6>
        </div>
        <br>
        <div class="row" style="margin-left: 2em; margin-right: 2em;">
            <a href="/home" class="modal-close waves-effect waves btn"><i
                    class="material-icons right">home</i>
            </a>
        </div>

        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="venture-table">
                    <thead>
                    <tr>
                        <th>Venture Name</th>
                        <th>Contact Number</th>
                        <th>Hub</th>
                        <th>Stage</th>
                        <th>Cohort</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
               data-tooltip="Add New Venture" href="{{url('create-venture')}}">
                <i class="large material-icons">add</i>
            </a>
        </div>

    </div>
    @push('custom-scripts')

        <script type="text/javascript"
                src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>

        <script type="text/javascript" language="javascript"
                src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>

        <script>

            $(document).ready(function (data) {
                $(function () {
                    $('#venture-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type: 'get',
                        scrollX: 640,
                        ajax: '{{route('get-venture')}}',
                        columns: [
                            {data: 'company_name', name: 'company_name'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'hub', name: 'hub'},
                            {data: 'stage', name: 'stage'},
                            {data: 'cohort', name: 'cohort'},
                            {data: 'status', name: 'status'},
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="venture-table_length"]').css("display", "inline");
                });
            });


            function confirm_delete_venture(obj) {
                var r = confirm("Are you sure want to delete this application!");
                console.log("Check", r);
                if (r) {
                    $.get('/Venture-delete-check/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status == 'success') {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Delete action cancelled');
                }
            }

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
