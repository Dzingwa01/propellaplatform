@extends('layouts.marketing-layout')

@section('content')

    <div class="section" style="margin-top: 5vh">
        <input id="user-id-input" value="{{$venture->id}}" hidden disabled>
        <div class="col s10 card z-depth-4" style="border-radius: 10px;width: 1350px;margin: 0 auto">
            {{--APPPLICATION ACOUNT INFO--}}
            <div class="row" id="applicant-account-show" style="margin-right: 3em; margin-left: 3em;">
                <br>
                <h4 class="center"><b>OVERVIEW OF : {{$venture->company_name}}</b></h4>
                <div class="row " style="margin-left: 10em">
                    <br>
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h6><b>Hub : </b> {{$venture->hub}}</h6>
                            <h6><b>Stage </b> {{$venture->stage}}</h6>
                            <h6><b>Cohort : </b> {{$venture->cohort}}</h6>
                            <h6><b>Status : </b> {{$venture->status}}</h6>
                            <h6><b>Venture email : </b> {{$venture->venture_email}}</h6>
                        </div>
                        <div class="col l6 m6 s12">
                            <h6><b>Address One : </b> {{$venture->physical_address_one}}</h6>
                            <h6><b>Address Two </b> {{$venture->physical_address_two}}</h6>
                            <h6><b>Address Three : </b> {{$venture->physical_address_two}}</h6>
                            <h6><b>Smart city tag : </b> {{$venture->smart_city_tags}}</h6>
                            <h6><b>Sustainable development goal : </b> {{$venture->sustainable_development_goals}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s2"></div>
        <div class="row center">
            <h4><b>ACTIONS</b></h4>
        </div>
        <div class="row" style="margin-left: 500px">
            <div class="card col s2" id="ownership" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">OWNERSHIP</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2" id="uploads" style="background-color:#454242;font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">UPLOADS</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="shadowboards" style="background-color:rgba(0, 121, 52, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">SHADOWBOARD</p>
            </div>
        </div>
    </div>

    <div class="section" style="margin-top: 5vh">
        <div class="col s12 card" id="venture-ownership" hidden style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <br>
            <!--ownership-->
            <div class="row"  style="margin-right: 3em; margin-left: 3em;">
                <div class="row center">
                    <h5><b>VENTURE OWNERSHIP</b></h5>
                </div>
                <p>Registration Number : {{$venture->ventureOwnership->registration_number}}</p>
                <p>BBBEE Level : {{$venture->ventureOwnership->BBBEE_level}}</p>
                <p>Ownership : {{$venture->ventureOwnership->ownership}}</p>
            </div>
        </div>
        <div class="col s12 card" id="venture-uploads" hidden style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <br>
            <!--uploads-->
            <div class="row"  style="margin-right: 3em; margin-left: 3em;">
                <div class="row center">
                    <h5><b>VENTURE UPLOADS</b></h5>
                </div>
                <div class="row">
                    <div class="col s4">
                        @if($venture->ventureUploads->video_shot_url == null)
                            <p>Video shot: Not uploaded.</p>
                        @else
                            <a href="{{$venture->ventureUploads->video_shot_url}}">Video shot</a>
                        @endif
                    </div>
                    <div class="col s4">
                        @if($venture->ventureUploads->infographic_url == null)
                            <p>Infographic: Not uploaded.</p>
                        @else
                            <a href="{{'/storage/'.$venture->ventureUploads->infographic_url}}" target="_blank">Infographic</a>
                        @endif
                    </div>
                    <div class="col s4">
                        @if($venture_upload->crowdfunding_url == null)
                            <p></p>
                        @else
                            <a href="{{'/storage/'.$venture_upload->crowdfunding_url}}" target="_blank">Crowd funding</a>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col s4">
                        @if(isset($venture_upload->venture_client_contract))
                            @foreach($venture_upload->venture_client_contract as $client_contract)
                                <a class="waves-effect waves-light  modal-trigger" href="#client-contracts">All Client contracts</a>
                            @endforeach
                        @endif
                    </div>
                    <!-- Client Contracts -->
                    <div id="client-contracts" class="modal">
                        <div class="modal-content">
                            <h4>All Clients contracts</h4>
                            <div class="row">
                                <div class="col s4">
                                    @if($client_contract->client_contract_url == null)
                                        <p>Client contract: Not uploaded.</p>
                                    @else
                                        <a href="{{'/storage/'.$client_contract->client_contract_url}}" target="_blank">Client contract</a>
                                    @endif
                                </div>
                                <div class="col s4">
                                    @if($venture->ventureUploads->stage3_client_contract == null)
                                        <p>Stage 3 contract : Not uploaded.</p>
                                    @else
                                        <a href="{{'/storage/'.$venture->ventureUploads->stage3_client_contract}}"
                                           target="_blank">Stage 3 contract</a>
                                    @endif
                                </div>
                                <div class="col s4">
                                    @if($venture->ventureUploads->stage4_client_contract == null)
                                        <p>Stage 4 contract : Not uploaded.</p>
                                    @else
                                        <a href="{{'/storage/'.$venture->ventureUploads->stage4_client_contract}}"
                                           target="_blank">Stage 4 contract</a>
                                    @endif

                                </div>
                            </div>
                            <div class="row">
                                <div class="col s4">
                                    @if($venture->ventureUploads->stage5_client_contract == null)
                                        <p>Stage 5 contract : Not uploaded.</p>
                                    @else
                                        <a href="{{'/storage/'.$venture->ventureUploads->stage5_client_contract}}"
                                           target="_blank">Stage 5 contract</a>
                                    @endif
                                </div>
                                <div class="col s4">
                                    @if($venture->ventureUploads->stage6_client_contract == null)
                                        <p>Stage 6 contract : Not uploaded.</p>
                                    @else
                                        <a href="{{'/storage/'.$venture->ventureUploads->stage6_client_contract}}"
                                           target="_blank">Stage 6 contract</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                        </div>
                    </div>

                    <div class="col s4">
                        @if(isset($venture_upload->venture_tax_clearances))
                            @foreach($venture_upload->venture_tax_clearances as $tax_clearances)
                                @if($tax_clearances->tax_clearance_url == null)
                                    <p>Tax clearance: Not uploaded.</p>
                                @else
                                    <a href="{{'/storage/'.$tax_clearances->tax_clearance_url}}" target="_blank">Tax clearance</a>
                                @endif
                            @endforeach
                        @endif
                    </div>
                    <div class="col s4">
                        @if(isset($venture_upload->venture_bbbee_certificates))
                            @foreach($venture_upload->venture_bbbee_certificates as $bbbee_certificate)
                                @if($bbbee_certificate->bbbee_certificate_url == null)
                                    <p>BBBEE Certificate : Not uploaded.</p>
                                @else
                                    <a href="{{'/storage/'.$bbbee_certificate->bbbee_certificate_url}}" target="_blank">BBBEE
                                        Certificate</a>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col s4">
                        @if(isset($venture_upload->venture_ck_documents))
                            @foreach($venture_upload->venture_ck_documents as $ck_documents)
                                @if($ck_documents->ck_documents_url == null)
                                    <p>CK Document : Not uploaded.</p>
                                @else
                                    <a href="{{'/storage/'.$ck_documents->ck_documents_url}}" target="_blank">CK
                                        Document</a>
                                @endif
                            @endforeach
                        @endif

                    </div>
                    <div class="col s4">
                        @if($venture->ventureUploads->cipc_document_url == null)
                            <p>CIPC Document : Not uploaded.</p>
                        @else
                            <a href="{{'/storage/'.$venture->ventureUploads->cipc_document_url}}" target="_blank">CIPC
                                Document</a>
                        @endif
                    </div>
                    <div class="col s4">
                        @if(isset($venture_upload->venture_dormant_affidavits))
                            @foreach($venture_upload->venture_dormant_affidavits as $dormant_affidavits)
                                @if($dormant_affidavits->dormant_affidavit_url == null)
                                    <p>Dormant affidavit : Not uploaded.</p>
                                @else
                                    <a href="{{'/storage/'.$dormant_affidavits->dormant_affidavit_url}}"
                                       target="_blank">Dormant
                                        affidavit</a>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col s4">
                        @if($venture->ventureUploads->market_research_upload == null)
                            <p>Market research : Not uploaded.</p>
                        @else
                            <a href="{{'/storage/'.$venture->ventureUploads->market_research_upload}}" target="_blank">Market
                                research</a>
                        @endif
                    </div>
                    <div class="col s4">
                        @if($venture->ventureUploads->project_plan_upload == null)
                            <p>Project plan : Not uploaded.</p>
                        @else
                            <a href="{{'/storage/'.$venture->ventureUploads->project_plan_upload}}" target="_blank">Project
                                plan</a>
                        @endif
                    </div>
                    <div class="col s4">
                        @if($venture->ventureUploads->business_plan == null)
                            <p>Business plan : Not uploaded.</p>
                        @else
                            <a href="{{'/storage/'.$venture->ventureUploads->business_plan}}" target="_blank">Business
                                plan</a>
                        @endif
                    </div>

                </div>

            </div>
        </div>
        <div class="col s12 card" id="venture-shadowboard" hidden style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <br>
            <div class="row">
                <div class="row center">
                    <h5><b>VENTURE SHADOWBOARDS</b></h5>
                </div>
                <table id="table" style="width:93%;margin-left: 2em;margin-right: 8em">
                    <tr>
                        <th>Venture Name</th>
                        <th>Shadowboard Date</th>
                        <th>Shadowboard Time</th>
                    </tr>
                    @foreach($shadow_array as $shadow_arrays)
                        <tr>
                            <td>{{$shadow_arrays->company_name}}</td>
                            <td>{{$shadow_arrays->shadow_board_date}}</td>
                            <td>{{$shadow_arrays->shadow_board_time}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

    <script>
        $('#ownership').on('click', function () {
            $('#venture-ownership').show();
            $('#venture-uploads').hide();
            $('#venture-shadowboard').hide();
        });
        $('#uploads').on('click', function () {
            $('#venture-uploads').show();
            $('#venture-ownership').hide();
            $('#venture-shadowboard').hide();
        });
        $('#shadowboards').on('click', function () {
            $('#venture-shadowboard').show();
            $('#venture-ownership').hide();
            $('#venture-uploads').hide();
        });
    </script>
@endsection
