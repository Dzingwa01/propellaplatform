@extends('layouts.marketing-layout')

@section('content')

    <div class="row" style="margin-left: 2em;margin-right: 2em">
        <br>
        <p style="margin-left: 20em;font-size: 2em">{{$resourceCategory->category_name}}</p>
        @foreach($resourceCategory->fundingResource as $funding)
            @if ($funding->closing_date < $date)
                <div class=""></div>
            @else
                <div class="col s12 m4">
                    <div class=" card funding" style="top: 5vh;height: 30vh;border-radius: 10%;cursor: pointer" >
                        <br>
                        <div class="row">
                            <div class="col s6">
                                <p style="margin-left: 1em">{{$funding->title}}</p>
                                <p style="margin-left: 1em">Closing Date : {{$funding->closing_date}}</p>
                            </div>
                            <div class="col s1"></div>
                            <div class="col s5" style="margin-right: 10px">
                                @if($funding->image_url != null)
                                    <img style="width: 200px; height: 150px"
                                         src="/storage/{{isset($funding->image_url)?$funding->image_url:'Nothing Detected'}}">
                                @endif
                            </div>

                        </div>
                        <input hidden disabled class="funding_id" data-value="{{$funding->id}}">
                    </div>
                </div>
            @endif
        @endforeach
    </div>

    <script>

        $('.funding').each(function () {
            let funding_id = $(this).find('.funding_id').attr('data-value');

            $(this).on('click', function () {
                location.href = '/show-all-info/' + funding_id;
            });
        });
    </script>
@endsection


