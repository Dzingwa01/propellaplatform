@extends('layouts.marketing-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <input id="venture-id-input" disabled hidden value="{{$incubatee->venture_id}}">
    <input id="incubatee-id-input" disabled hidden value="{{$incubatee->id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('marketing-edit-venture-inc',$incubatee,$incubatee->venture_id)}}
    <div class="container">

        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Edit
                Incubatee</h6>
            <div class="row card hoverable">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s6"><a href="#test1" class="active">Personal Details</a></li>
                        {{--<li class="tab col s4"><a href="#test2">Start Up Details</a></li>--}}
                        <li class="tab col s6"><a href="#test3">Media</a></li>
                    </ul>
                </div>
                <div id="test1" class="col s12">
                    <form id="incubatee-details-update-form" method="post" class="col s12" style="margin-top:1em;"
                          enctype="multipart/form-data">
                        @csrf

                        <input value="{{isset($user->incubatee->id)}}" id="incubatee_id" hidden>
                        <h5>Personal Details</h5>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="title">
                                    {{--<option value="" disabled selected>Choose Title</option>--}}
                                    <option value="Mr" {{$user->title=='Mr'?'selected':''}}>Mr</option>
                                    <option value="Mrs"{{$user->title=='Mrs'?'selected':''}}>Mrs</option>
                                    <option value="Miss"{{$user->title=='Miss'?'selected':''}}>Miss</option>
                                    <option value="Ms"{{$user->title=='Ms'?'selected':''}}>Ms</option>
                                    <option value="Dr"{{$user->title=='Dr'?'selected':''}}>Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="initials" type="text" class="validate" value="{{$user->initials}}"
                                       required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="name" type="text" class="validate" value="{{$user->name}}" required>
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="surname" type="text" class="validate" value="{{$user->surname}}"
                                       required>
                                <label for="surname">Surname</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="id_number" type="text" class="validate" value="{{$user->id_number}}"
                                       required>
                                <label for="id_number">ID Number</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="dob" type="date" class="validate"
                                       value="{{$user->dob/*->toDateString()*/}}" required>
                                <label for="dob">Date of Birth</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="age" type="text" class="validate" value="{{$user->age}}"
                                       placeholder="Age" required>
                                {{--<label for="age">Age</label>--}}
                            </div>
                            <div class="input-field col m6">
                                <select id="gender">
                                    {{--<option value="" disabled selected>Choose Gender</option>--}}
                                    <option value="Male" {{$user->type=='Male'?'selected':''}}>Male</option>
                                    <option value="Female" {{$user->type=='Female'?'selected':''}}>Female
                                    </option>
                                </select>
                                <label>Gender</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="email" type="email" class="validate" value="{{$user->email}}"
                                       required>
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="tel" class="validate"
                                       value="{{$user->contact_number}}" required>
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="address_one" type="tel" class="validate"
                                       value="{{$user->address_one}}" required>
                                <label for="address_one">Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="address_two" type="tel" class="validate"
                                       value="{{$user->address_two}}" required>
                                <label for="address_two">Address 2</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="address_three" type="tel" class="validate"
                                       value="{{$user->address_three}}" required>
                                <label for="address_three">Address 3</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="city" type="tel" class="validate"
                                       value="{{$user->city}}" required>
                                <label for="city">City</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="city" type="tel" class="validate"
                                       value="{{$user->code}}" required>
                                <label for="city">Postal Code</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="nmu_graduate">
                                    {{--<option value="" disabled selected>Are you an NMU Graduate?</option>--}}
                                    <option value="Yes"{{$user->nmu_graduate=='Yes'?'selected':''}}>Yes</option>
                                    <option value="No"{{$user->nmu_graduate=='No'?'selected':''}}>No</option>
                                </select>
                                <label>NMU Graduate</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="disabled">
                                    {{--<option value="" disabled selected>Do you have Disabilities?</option>--}}
                                    <option value="Yes"{{$user->disabled=='Yes'?'selected':''}}>Yes</option>
                                    <option value="No"{{$user->disabled=='No'?'selected':''}}>No</option>
                                </select>
                                <label>Disability</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="home_language">
                                    {{--<option value="" disabled selected>Choose your Home Language?</option>--}}
                                    <option value="English"{{$user->home_language=='English'?'selected':''}}>English
                                    </option>
                                    <option value="IsiXhosa"{{$user->home_language=='IsiXhosa'?'selected':''}}>
                                        IsiXhosa
                                    </option>
                                    <option value="IsiZulu"{{$user->home_language=='IsiZulu'?'selected':''}}>IsiZulu
                                    </option>
                                    <option value="Afrikaans"{{$user->home_language=='Afrikaans'?'selected':''}}>
                                        Afrikaans
                                    </option>
                                    <option value="IsiNdebele"{{$user->home_language=='IsiNdebele'?'selected':''}}>
                                        IsiNdebele
                                    </option>
                                    <option value="Sepedi"{{$user->home_language=='Sepedi'?'selected':''}}>Sepedi
                                    </option>
                                    <option value="SeTswana"{{$user->home_language=='SeTswana'?'selected':''}}>
                                        SeTswana
                                    </option>
                                    <option value="TshiVenḓa"{{$user->home_language=='TshiVenḓa'?'selected':''}}>
                                        TshiVenḓa
                                    </option>
                                    <option value="SiSwati"{{$user->home_language=='SiSwati'?'selected':''}}>SiSwati
                                    </option>
                                    <option value="XiTsonga"{{$user->home_language=='XiTsonga'?'selected':''}}>
                                        XiTsonga
                                    </option>
                                    <option value="SeSotho"{{$user->home_language=='SeSotho'?'selected':''}}>SeSotho
                                    </option>
                                </select>
                                <label>Home Language</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="residence">
                                    {{--<option value="" disabled selected>Choose Resident</option>--}}
                                    <option value="Urban"{{$user->incubatee->residence=='Urban'?'selected':''}}>Urban
                                    </option>
                                    <option
                                        value="Peri-Urban"{{$user->incubatee->residence=='Peri-Urban'?'selected':''}}>
                                        Peri-Urban
                                    </option>
                                    <option value="Rural"{{$user->incubatee->residence=='Rural'?'selected':''}}>Rural
                                    </option>
                                </select>
                                <label>Resides</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_facebook" type="text"
                                       value="{{$user->incubatee->personal_facebook_url}}"
                                       class="validate">
                                <label for="personal_facebook">Facebook</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_linked_in" type="text"
                                       value="{{$user->incubatee->personal_linkedIn_url}}"
                                       class="validate">
                                <label for="personal_linked_in">LinkedIn</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_twitter" type="text"
                                       value="{{$user->incubatee->personal_twitter_url}}"
                                       class="validate">
                                <label for="personal_twitter">Twitter</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_instagram" type="text"
                                       value="{{$user->incubatee->personal_instagram_url}}" class="validate">
                                <label for="personal_instagram">Instagram</label>
                            </div>
                            <div class="input-field col m6">
                                <textarea id="short_bio" class="materialize-textarea" data-length="120">{{$user->incubatee->short_bio}}</textarea>
                                <label for="short_bio">About Me</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-personal-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>

                    @if(is_null($user->incubatee->venture_id))
                        {{--create venture--}}
                        <div class="fixed-action-btn" id="float-create-venture">
                            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
                               data-tooltip="Add New Venture"
                               href="{{url('users/incubatees/admin-incubatees/incubatee-create-venture', $user->id)}}">
                                <i class="large material-icons">add</i>
                            </a>
                        </div>
                    @endif
                </div>

                <div id="test3" class="col s12">
                    <form id="incubatee-media-update-form" class="col s12" style="margin-top:1em;" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <input value="{{$user->incubatee->id}}" id="incubatee_id" hidden>
                        {{--{{$user->incubatee->id}}--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Profile Picture</span>
                                        <input id="profile_picture_url" type="file" name="profile_picture_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($user->incubatee->uploads->profile_picture_url)?$user->incubatee->uploads->profile_picture_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <img src="" id="profile-picture-preview">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>ID Document</span>
                                        <input id="id_document" type="file" name="id_document">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($user->incubatee->uploads->id_document)?$user->incubatee->uploads->id_document:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <img src="" id="profile-picture-preview">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-media" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <style>
        .tabs .tab a:hover, .tabs .tab a.active {
            color: black !important;
            font-weight: bolder;
        }

        .tabs .tab a {
            color: black !important;
        }
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    @push('custom-scripts')
        <script>

            $(document).ready(function () {
                $("select").formSelect();
                $('.tooltipped').tooltip();


                //Style block where profile picture is loaded
                function profilePictureIsLoaded(e) {
                    $("#profile_picture_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#profile-picture-preview').attr('src', e.target.result);
                    $('#profile-picture-preview').attr('width', '250px');
                    $('#profile-picture-preview').attr('height', '230px');
                }

                //Style block where company logo is loaded
                function companyLogoIsLoaded(e) {
                    $("#company_logo_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#company-logo-preview').attr('src', e.target.result);
                    $('#company-logo-preview').attr('width', '250px');
                    $('#company-logo-preview').attr('height', '230px');
                }

                //Style block where video shot is loaded
                function videoShotIsLoaded(e) {
                    $("#video-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#video-shot-preview').attr('src', e.target.result);
                    $('#video-shot-preview').attr('width', '250px');
                    $('#video-shot-preview').attr('height', '230px');
                }

                //Style block where infographic is loaded
                function infographicIsLoaded(e) {
                    $("#infographic_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#infographic-preview').attr('src', e.target.result);
                    $('#infographic-preview').attr('width', '250px');
                    $('#infographic-preview').attr('height', '230px');
                }

                //Style block where crowdfunding is loaded
                function crowdfundingIsLoaded(e) {
                    $("#crowdfunding_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#crowdfunding-preview').attr('src', e.target.result);
                    $('#crowdfunding-preview').attr('width', '250px');
                    $('#crowdfunding-preview').attr('height', '230px');
                }

                //Style block where product shot is loaded
                function productShotIsLoaded(e) {
                    $("#product-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#product-shot-preview').attr('src', e.target.result);
                    $('#product-shot-preview').attr('width', '250px');
                    $('#product-shot-preview').attr('height', '230px');
                }

                // Function to preview profile picture after validation
                $(function () {
                    $("#profile_picture_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#profile-picture-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = profilePictureIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview company logo after validation
                $(function () {
                    $("#company_logo_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#company-logo-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = companyLogoIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview video shot after validation
                $(function () {
                    $("#video-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#video-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = videoShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview infographic after validation
                $(function () {
                    $("#infographic_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#infographic-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = infographicIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview crowdfunding after validation
                $(function () {
                    $("#crowdfunding_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#crowdfunding-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = crowdfundingIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview product shot after validation
                $(function () {
                    $("#product-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#product-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = productShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });
                let value = $("#dob").val();
                let dob = new Date(value);
                let today = new Date();
                let age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                if (isNaN(age)) {
                    // will set 0 when value will be NaN
                    age = 0;
                } else {
                    age = age;
                }
                let ages = $('#age').val(age);
                $("#dob").change(function () {
                    let value = $("#dob").val();
                    let dob = new Date(value);
                    let today = new Date();
                    let age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                    if (isNaN(age)) {
                        // will set 0 when value will be NaN
                        age = 0;
                    } else {
                        age = age;
                    }
                    let ages = $('#age').val(age);
                });

                $('#incubatee-details-update-form').on('submit', function (e) {
                    e.preventDefault();
                    let formData = new FormData();
                    formData.append('title', $('#title').val());
                    formData.append('initials', $('#initials').val());
                    formData.append('dob', $('#dob').val());
                    formData.append('age', $('#age').val(ages));
                    formData.append('gender', $('#gender').val());
                    formData.append('id_number', $('#id_number').val());
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('email', $('#email').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('personal_facebook_url', $('#personal_facebook').val());
                    formData.append('personal_linkedIn_url', $('#personal_linked_in').val());
                    formData.append('personal_twitter_url', $('#personal_twitter').val());
                    formData.append('personal_instagram_url', $('#personal_instagram').val());
                    formData.append('address_one', $('#address_one').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    formData.append('city', $('#city').val());
                    formData.append('code', $('#code').val());
                    formData.append('nmu_graduate', $('#nmu_graduate').val());
                    formData.append('disabled', $('#disabled').val());
                    formData.append('residence', $('#residence').val());
                    formData.append('home_language', $('#home_language').val());
                    formData.append('short_bio', $('#short_bio').val());

                    console.log("user ", formData);


                    let url = '/venture-incubatee-update-personal/' + '{{$user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            /*console.log("success", response);
                            alert(response.message);*/
                            $("#save-incubatee-personal-details").notify(
                                "You have successfully Update Incubatee", "success",
                                { position:"right" }
                            );
                            /*$('#save-incubatee-personal-details').notify(response.message, "success");*/

                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });


                $('#incubatee-media-update-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();

                    jQuery.each(jQuery('#profile_picture_url')[0].files, function (i, file) {
                        formData.append('profile_picture_url', file);
                    });

                    jQuery.each(jQuery('#id_document')[0].files, function (i, file) {
                        formData.append('id_document', file);
                    });


                    let url = '/venture-incubatee-update-media/' + '{{$user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            /*console.log("success", response);
                            alert(response.message);*/
                            $("#save-incubatee-media").notify(
                                "You have successfully Updated Incubatee Media", "success",
                                { position:"right" }
                            );
                            /*$('#save-incubatee-media').notify(response.message, "success");*/

                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                            sessionStorage.clear();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection

