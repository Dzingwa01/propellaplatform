@extends('layouts.marketing-layout')

@section('content')


    <br>
    <br>
    {{ Breadcrumbs::render('marketing-incubatees')}}
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Marketing Incubatees</h6>
            {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Category</th>
                        <th>Stage</th>
                        <th>Status</th>
                        <th>Date added</th>
                        <th>POPI</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Incubatee" href="{{url('create-incubatee')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>
        {{--<button class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example" type="button"><span>Excel</span></button>--}}
    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $(function () {
                    $('#users-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-incubatees')}}',

                        columns: [
                            {data: 'name', name: 'name'},
                            {data: 'surname', name: 'surname'},
                            {data: 'email', name: 'email'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'category_name', name: 'category_name'},
                            {data: 'stage', name: 'stage'},
                            {data: 'status', name: 'status'},
                            {data: 'date_joined', name: 'date_joined'},
                            {data: 'incubatee_popi_act_agreement' ,name: 'incubatee_popi_act_agreement'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ],

                    });
                    $('select[name="users-table_length"]').css("display","inline");
                });

            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection

