@extends('layouts.marketing-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <div class="container">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Create
                Venture</h6>
            <div class="row card hoverable">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s6"><a href="#test1">Venture Details</a></li>
                        <li class="tab col s6"><a href="#test2">Media</a></li>
                    </ul>
                </div>
                <div id="test1" class="card col s12">
                    <br>
                    <br>

                    <form id="start_details_form" class="col s12" style="margin-top:1em;">
                        @csrf
                        <input value="{{$incubatee->id}}" id="incubatee_id" hidden>

                        <h6 style="padding-left: 50px;"><b>START UP DETAILS</b></h6>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <input id="company_name" type="text" class="validate">
                                <label for="company_name">Company Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="text" class="validate">
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <input id="ownership" type="text" class="validate">
                                <label for="ownership">Ownership (fill in percentage)</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="registration_number" type="text" class="validate">
                                <label for="registration_number">Registration Number</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <select id="BBBEE_Level">
                                    <option value="" disabled selected>Choose BBBEE Level</option>
                                    <option value="1"> Level 01</option>
                                    <option value="2"> Level 02</option>
                                    <option value="3"> Level 03</option>
                                    <option value="4"> Level 04</option>
                                    <option value="5"> Level 05</option>
                                    <option value="6"> Level 06</option>
                                    <option value="7"> Level 07</option>
                                </select>
                            </div>
                            <div class="input-field col m6">
                                <select id="venture_type">
                                    <option value="" disabled selected>Choose Venture Type</option>
                                    <option value="Start up">Start up</option>
                                    <option value="Dormant">Dormant</option>
                                    <option value="Existing">Existing</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <br>
                        <h6 style="padding-left: 40px;"><b>ONBOARDING INFO</b></h6>
                        <br>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <select id="hub">
                                    <option value="" disabled selected>Choose hub</option>
                                    @foreach($venture_categories as $v_category)
                                        <option value="{{$v_category->category_name}}">{{$v_category->category_name}}</option>
                                    @endforeach
                                </select>
                                <label>Hub</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="stage">
                                    <option value="" disabled selected>Choose Stage</option>
                                    @foreach($incubatee_stages as $i_stage)
                                        <option value="{{$i_stage->stage_name}}">{{$i_stage->stage_name}}</option>
                                    @endforeach
                                </select>
                                <label>Stage</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <select id="business_type">
                                    <option value="" disabled selected>Choose option</option>
                                    <option value="Agriculture">Agriculture; plantations; rural sectors</option>
                                    <option value="Basic Metal Product">Basic Metal Product</option>
                                    <option value="Chemical industries">Chemical industries</option>
                                    <option value="Commerce">Commerce</option>
                                    <option value="Construction">Construction</option>
                                    <option value="Education">Education</option>
                                    <option value="Financial service">Financial and professional services</option>
                                    <option value="Food">Food; drink; tobacco</option>
                                    <option value="Forestry">Forestry; wood; pulp and paper</option>
                                    <option value="Health service">Health service</option>
                                    <option value="Touring">Tourism; (hotels; catering)</option>
                                    <option value="Mining">Mining (coal; other mining)</option>
                                    <option value="Mechanical and electrical engineering">Mechanical and electrical
                                        engineering
                                    </option>
                                    <option value="Oil and gas production">Oil and gas production</option>
                                    <option value="Postal and telecommunications services">Postal and telecommunications
                                        services
                                    </option>
                                    <option value="Public service">Public service</option>
                                    <option value="Fishing">Fishing (ports; fisheries; inland waterways)</option>
                                    <option value="Transport">Transport (including civil aviation; railways; road
                                        transport)
                                    </option>
                                    <option value="Clothing">Clothing (Textiles; clothing; footwear)</option>
                                    <option value="Utilities">Utilities (waters; gas; electricity)</option>
                                </select>
                                <label>Business Category</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="smart_city_tags" multiple>
                                    <option value="" disabled selected>Choose Tags</option>
                                    <option value="Smart Business">Smart Business</option>
                                    <option value="Smart Community">Smart Community</option>
                                    <option value="Smart Healthcare">Smart Healthcare</option>
                                    <option value="Smart Living">Smart Living</option>
                                    <option value="Smart Mobility">Smart Mobility</option>
                                    <option value="Smart Building">Smart Building</option>
                                    <option value="Smart Infrastructure">Smart Infrastructure</option>
                                    <option value="Smart Governance">Smart Governance</option>
                                    <option value="Smart Environment">Smart Environment</option>
                                    <option value="Smart Agriculture">Smart Agriculture</option>
                                </select>
                                <label>Smart City Tags
                                </label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <select id="virtual_or_physical">
                                    <option value="" disabled selected>Choose your Availability</option>
                                    <option id="Physical" value="Physical">Physical</option>
                                    <option id="Virtual" value="Virtual">Virtual</option>
                                </select>
                                <label>Physical or Virtual</label>
                            </div>
                            <div class="input-field col m6">
                                <label for="advisor">Advisor Name</label>
                                <input id="advisor" type="text" class="validate">
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <label for="mentor">Mentor Name</label>
                                <input id="mentor" type="text" class="validate">
                            </div>
                            <div class="input-field col m6">
                                <select id="status" class="input-select">
                                    <option value="" disabled selected>Choose Status</option>
                                    <option value="Active">Active</option>
                                    <option value="Alumni">Alumni</option>
                                    <option value="Exit">Exit</option>
                                    <option value="Resigned">Resigned</option>
                                    <option value="On Hold">On Hold</option>
                                </select>
                                <label>Status</label>
                            </div>
                        </div>
                        <div class="row" id="alumniDate" style="margin-left: 2em;">
                            <div class="input-field col m6">
                                <input id="alumni_date" type="text" class="datepicker">
                                <label for="alumni_date">Alumni Date</label>
                            </div>
                        </div>
                        <div class="row" id="exitDate">
                            <div class="input-field col m6">
                                <input id="exit_date" type="text" class="datepicker">
                                <label for="exit_date">Exit Date</label>
                            </div>
                        </div>
                        <div class="row" id="resignDate">
                            <div class="input-field col m6">
                                <input id="resigned_date" type="text" class="datepicker">
                                <label for="resigned_date">Resigned Date</label>
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>GOVERNANCE</b></h6>

                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="file-field input-field col l6">
                                <div class="btn">
                                    <span>Client Contract </span>
                                    <input type="file" id="client_contract" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="input-field col l6">
                                {{--<label for="date_of_signature">Date of Signature</label>--}}
                                <input type="date" id="date_of_signature">
                            </div>
                        </div>

                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="file-field input-field col l6">
                                <div class="btn">
                                    <span>Tax Clearance</span>
                                    <input type="file" id="tax_clearance_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="input-field col l6">
                                <input type="date" id="date_of_clearance_tax">
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="file-field input-field col l6">
                                <div class="btn">
                                    <span>BBBEE Certificate</span>
                                    <input type="file" id="bbbee_certificate_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="input-field col m6">
                                <input type="date" id="date_of_bbbee">
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="file-field input-field col l6">
                                <div class="btn">
                                    <span>CK Documents</span>
                                    <input type="file" id="ck_documents_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="file-field input-field col l6">
                                <div class="btn">
                                    <span>Dormant Affidavit</span>
                                    <input type="file" id="dormant_affidavit_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="file-field input-field col l6">
                                <div class="btn">
                                    <span>Management Account</span>
                                    <input type="file" id="management_account_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="file-field input-field col l6">
                                <div class="file-path-wrapper">
                                    <label for="vat_registration_number_url">Vat Number</label>
                                    <input class="validate" id="vat_registration_number_url" type="text" placeholder="Vat Number">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row" id="precinct_info" style="margin-left: 2em;margin-right: 2em;">
                            <h6 style="padding-left: 50px;"><b>PRECINCT INFO</b></h6>

                            <div class="input-field col m6">
                                <input id="pricinct_telephone_code" type="text" class="validate">
                                <label for="pricinct_telephone_code">Telephone code</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="pricinct_keys">
                                    <option value="" disabled selected>Do You Have Keys</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <label>Keys</label>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col m6">
                                    <input id="pricinct_office_position" type="text" class="validate">
                                    <label for="pricinct_office_position">Office Position</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="pricinct_printing_code" type="text" class="validate">
                                    <label for="pricinct_printing_code">Printing Code</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="pricinct_warehouse_position" type="text" class="validate">
                                    <label for="pricinct_warehouse_position">Warehouse Position</label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;">
                            <div class="input-field col m6">
                                <input id="cohort" type="text" class="validate">
                                <label for="cohort">Cohort</label>
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>VENTURE FUNDING</b></h6>
                        <br>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <select id="venture_funding">
                                    <option value="" disabled selected>Who is Funding Your Venture?</option>
                                    <option value="TIA Seed Fund">TIA Seed Fund</option>
                                    <option value="Propella BDS">Propella BDS</option>
                                </select>
                                <label>Venture Funding</label>
                            </div>
                        </div>
                        <br>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <input id="date_awarded" type="date" {{--class="datepicker"--}}>
                                <label for="date_awarded">Date Awarded</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="fund_value" type="text" class="validate">
                                <label for="fund_value">Amount</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <input id="date_closedout" type="date" {{--class="datepicker"--}}>
                                <label for="date_closedout">Due Date</label>
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>RENT TURNOVER</b></h6>
                        <br>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <select id="rent_turnover" form="start_details_form" class="inputs-select">
                                    <option value="" disabled selected>Deposited Rent?</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <label>Rent Turnover</label>
                            </div>
                        </div>
                        <br>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6" id="rentDate">
                                <input id="rent_date" type="date">
                                <label for="rent_date">Due Date</label>
                            </div>
                            <div class="input-field col m6" id="rentAmount">
                                <input id="rent_amount" type="text" class="validate" placeholder="Amount">
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>ADDRESS DETAILS</b></h6>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <input id="physical_address_one" type="text" class="validate">
                                <label for="physical_address_one">Physical Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="physical_address_two" type="text" class="validate">
                                <label for="physical_address_two">Physical Address 2</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <input id="physical_address_three" type="text" class="validate">
                                <label for="physical_address_three">Physical Address 3</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="physical_city" type="text" class="validate">
                                <label for="physical_city">City</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <input id="physical_postal_code" type="text" class="validate">
                                <label for="physical_code">Code</label>
                            </div>
                            <p>
                                <label>
                                    <input type="checkbox" id="address_check"/>
                                    <span>Select if physical address is the same as postal address</span>
                                </label>
                            </p>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <input id="postal_address_one" type="text" class="validate"
                                       placeholder="Postal Address 1">
                            </div>
                            <div class="input-field col m6">
                                <input id="postal_address_two" type="text" class="validate"
                                       placeholder="Postal Address 2">
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col m6">
                                <input id="postal_address_three" type="text" class="validate"
                                       placeholder="Postal Address 3">
                            </div>
                            <div class="input-field col m6">
                                <input id="postal_city" type="text" class="validate" placeholder="City">
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;">
                            <div class="input-field col m6">
                                <input id="postal_code" type="text" class="validate" placeholder="postal code">
                            </div>

                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('venture')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-startup-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="test2" class="card col s12">
                    <form id="media_form" class="col s12" style="margin-top:1em;">
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Profile Picture</span>
                                        <input id="profile_picture_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <img src="" id="previewing">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <textarea id="venture_profile_information" class="materialize-textarea"></textarea>
                                <label for="venture_profile_information">Venture Profile Information (100 words
                                    max)</label>
                            </div>
                            <div class="input-field col m6">
                                <label for="elevator_pitch">Elevator Pitch (40 - 50 words max)</label>
                                <textarea id="elevator_pitch" name="elevator_pitch"
                                          class="materialize-textarea"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="business_facebook_url" type="text" class="validate">
                                <label for="business_facebook_url">Facebook</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="business_linkedin_url" type="text" class="validate">
                                <label for="business_linkedin_url">LinkedIn</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="business_twitter_url" type="text" class="validate">
                                <label for="business_twitter_url">Twitter</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="business_instagram_url" type="text" class="validate">
                                <label for="business_instagram_url">Instagram</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <label for="company_website"> Company Website</label>
                                <input id="company_website" type="text" class="validate">
                            </div>
                            <div class="input-field col m6">
                                <label for="venture_email">Venture Email</label>
                                <input id="venture_email" type="text" class="validate">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Company Logo</span>
                                        <input id="company_logo_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <div class="input-field" style="bottom:0px!important;">
                                    <input id="video-shot" type="text">
                                    <label for="video-shot">Video Shot</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Infographic</span>
                                        <input id="infographic_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Crowdfunding</span>
                                        <input id="crowdfunding_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Product Shot</span>
                                        <input id="product-shot" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('venture')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-media" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <style>
        .tabs .tab a:hover, .tabs .tab a.active {
            color: black !important;
            font-weight: bolder;
        }

        .tabs .tab a {
            color: black !important;
        }
    </style>

    @push('custom-scripts')
        <script>

            let venture_id;
            let incubatee = {};
            $(document).ready(function () {
                $('.datepicker').datepicker();
                $('select').formSelect();
                $('.tabs').tabs();

                $('#short_bio').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 120) {
                        alert('Word Limit Reached!');
                    }
                });
                $('#elevator_pitch').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 50) {
                        alert('Word Limit Reached!');
                    }
                });
                $('#venture_profile_information').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 100) {
                        alert('Word Limit Reached!');
                    }
                });

                $(".input-select").on("change", checkSelect);
                $("#alumniDate").hide();
                $("#exitDate").hide();
                $("#resignDate").hide();


                function checkSelect() {
                    if ($('#status').val() == 'Alumni') {
                        $("#alumniDate").show();
                        $("#exitDate").hide();
                        $("#resignDate").hide();
                    }
                    if ($('#status').val() == 'Exit') {
                        $("#exitDate").show();
                        $("#alumniDate").hide();
                        $("#resignDate").hide();
                    }
                    if ($('#status').val() == 'Resigned') {
                        $("#resignDate").show();
                        $("#alumniDate").hide();
                        $("#exitDate").hide();
                    }
                    if ($('#status').val() == 'Active') {
                        $("#alumniDate").hide();
                        $("#exitDate").hide();
                        $("#resignDate").hide();
                    }
                }

                $("#rent_amount").val() == "";
                $("#rent_date").val() == "";
                $("#rentDate").hide();
                $("#rentAmount").hide();
                let now = new Date();
                let day = ("0" + now.getDate()).slice(-2);
                let month = ("0" + (now.getMonth() + 1)).slice(-2);
                let today = now.getFullYear() + "-" + (month) + "-" + (day);

                $(".inputs-select").on("change", rentSelect)

                function rentSelect() {
                    if ($('#rent_turnover').val() == 'No') {
                        $("#rentDate").hide();
                        $("#rent_date").val(today).show();
                        $("#rentAmount").hide();
                        $("#rent_amount").val() == 0;
                    }
                    if ($('#rent_turnover').val() == 'Yes') {
                        $("#rentDate").show();
                        $("#rentAmount").show();
                        $("#rent_amount").val() == "";
                        $("#rent_date").val() == "";

                    }
                }
            });

            // Function to preview image after validation
            $(function () {
                $("#profile_picture_url").change(function () {
                    $("#message").empty(); // To remove the previous error message
                    var file = this.files[0];
                    var imagefile = file.type;
                    var match = ["image/jpeg", "image/png", "image/jpg"];
                    if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                        $('#previewing').attr('src', 'noimage.png');
                        $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                        return false;
                    } else {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        reader.readAsDataURL(this.files[0]);
                    }
                });
            });

            function imageIsLoaded(e) {
                $("#profile_picture_url").css("color", "green");
                $('#image_preview').css("display", "block");
                $('#previewing').attr('src', e.target.result);
                $('#previewing').attr('width', '250px');
                $('#previewing').attr('height', '230px');
            }

            $('#start_details_form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();
                let incubatee_id = $('#incubatee_id').val();

                formData.append('company_name', $('#company_name').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('physical_address_one', $('#physical_address_one').val());
                formData.append('physical_address_two', $('#physical_address_two').val());
                formData.append('physical_address_three', $('#physical_address_three').val());
                formData.append('physical_city', $('#physical_city').val());
                formData.append('physical_postal_code', $('#physical_postal_code').val());
                formData.append('postal_address_one', $('#postal_address_one').val());
                formData.append('postal_address_two', $('#postal_address_two').val());
                formData.append('postal_address_three', $('#postal_address_three').val());
                formData.append('postal_city', $('#postal_city').val());
                formData.append('postal_code', $('#postal_code').val());
                formData.append('hub', $('#hub').val());
                formData.append('smart_city_tags', $('#smart_city_tags').val());
                formData.append('business_type', $('#business_type').val());
                formData.append('cohort', $('#cohort').val());
                formData.append('stage', $('#stage').val());
                formData.append('virtual_or_physical', $('#virtual_or_physical').val());
                formData.append('ownership', $('#ownership').val());
                formData.append('registration_number', $('#registration_number').val());
                formData.append('BBBEE_level', $('#BBBEE_Level').val());
                formData.append('date_of_signature', $('#date_of_signature').val());
                formData.append('date_of_clearance_tax', $('#date_of_clearance_tax').val());
                formData.append('date_of_bbbee', $('#date_of_bbbee').val());
                formData.append('pricinct_telephone_code', $('#pricinct_telephone_code').val());
                formData.append('pricinct_keys', $('#pricinct_keys').val());
                formData.append('pricinct_office_position', $('#pricinct_office_position').val());
                formData.append('pricinct_printing_code', $('#pricinct_printing_code').val());
                formData.append('pricinct_warehouse_position', $('#pricinct_warehouse_position').val());
                formData.append('rent_turnover', $('#rent_turnover').val());
                formData.append('rent_date', $('#rent_date').val());
                formData.append('rent_amount', $('#rent_amount').val());
                formData.append('status', $('#status').val());
                formData.append('alumni_date', $('#alumni_date').val());
                formData.append('exit_date', $('#exit_date').val());
                formData.append('resigned_date', $('#resigned_date').val());
                formData.append('mentor', $('#mentor').val());
                formData.append('advisor', $('#advisor').val());
                formData.append('venture_funding', $('#venture_funding').val());
                formData.append('date_awarded', $('#date_awarded').val());
                formData.append('fund_value', $('#fund_value').val());
                formData.append('date_closedout', $('#date_closedout').val());
                formData.append('venture_type', $('#venture_type').val());


                if(jQuery('#client_contract')[0].files !== null){
                    jQuery.each(jQuery('#client_contract')[0].files, function (i, file) {
                        formData.append('client_contract', file);
                    });
                }

                if(jQuery('#tax_clearance_url')[0].files !== null){
                    jQuery.each(jQuery('#tax_clearance_url')[0].files, function (i, file) {
                        formData.append('tax_clearance_url', file);
                    });
                }

                if(jQuery('#bbbee_certificate_url')[0].files !== null){
                    jQuery.each(jQuery('#bbbee_certificate_url')[0].files, function (i, file) {
                        formData.append('bbbee_certificate_url', file);
                    });
                }

                if(jQuery('#ck_documents_url')[0].files){
                    jQuery.each(jQuery('#ck_documents_url')[0].files, function (i, file) {
                        formData.append('ck_documents_url', file);
                    });
                }

                if(jQuery('#vat_registration_number_url')[0].files){
                    jQuery.each(jQuery('#vat_registration_number_url')[0].files, function (i, file) {
                        formData.append('vat_registration_number_url', file);
                    });
                }

                if(jQuery('#management_account_url')[0].files){
                    jQuery.each(jQuery('#management_account_url')[0].files, function (i, file) {
                        formData.append('management_account_url', file);
                    });
                }

                if(jQuery('#dormant_affidavit_url')[0].files){
                    jQuery.each(jQuery('#dormant_affidavit_url')[0].files, function (i, file) {
                        formData.append('dormant_affidavit_url', file);
                    });
                }



                let url = '/venture-store-business/' + incubatee_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',

                    success: function (response, a, b) {
                        alert(response.message);
                        sessionStorage.setItem('venture_id', response.venture['id']);
                    },
                    error: function (response) {
                        alert(response.message);
                    }
                });
            });


            $('#address_check').click(function () {
                if ($(this).is(':checked')) {
                    let physical_address_one = $('#physical_address_one').val();
                    let physical_address_two = $('#physical_address_two').val();
                    let physical_address_three = $('#physical_address_three').val();
                    let physical_city = $('#physical_city').val();
                    let physical_postal_code = $('#physical_postal_code').val();

                    $('#postal_address_one').val(physical_address_one);
                    $('#postal_address_two').val(physical_address_two);
                    $('#postal_address_three').val(physical_address_three);
                    $('#postal_city').val(physical_city);
                    $('#postal_code').val(physical_postal_code);
                } else {
                    formData.append('postal_address_one', $('#postal_address_one').val());
                    formData.append('postal_address_two', $('#postal_address_two').val());
                    formData.append('postal_address_three', $('#postal_address_three').val());
                    formData.append('postal_city', $('#postal_city').val());
                    formData.append('postal_code', $('#postal_code').val());
                }
            });


            $(function () {
                $("#virtual_or_physical").change(function () {
                    if ($("#Physical").is(":selected")) {
                        $("#precinct_info").show();

                    } else {
                        $("#precinct_info").hide();

                    }
                }).trigger('change');
            });

            $('#media_form').on('submit', function (e) {
                e.preventDefault();

                let venture_id = sessionStorage.getItem('venture_id');

                let formData = new FormData();
                formData.append('venture_id', venture_id);
                formData.append('video_shot_url', $('#video-shot').val());
                formData.append('company_website', $('#company_website').val());
                formData.append('elevator_pitch', $('#elevator_pitch').val());
                formData.append('venture_profile_information', $('#venture_profile_information').val());
                formData.append('venture_email', $('#venture_email').val());
                formData.append('business_facebook_url', $('#business_facebook').val());
                formData.append('business_twitter_url', $('#business_twitter').val());
                formData.append('business_linkedin_url', $('#business_linkedin').val());
                formData.append('business_instagram_url', $('#business_instagram').val());

                jQuery.each(jQuery('#profile_picture_url')[0].files, function (i, file) {
                    formData.append('profile_picture_url', file);
                });

                jQuery.each(jQuery('#company_logo_url')[0].files, function (i, file) {
                    formData.append('company_logo_url', file);
                });

                jQuery.each(jQuery('#infographic_url')[0].files, function (i, file) {
                    formData.append('infographic_url', file);
                });

                jQuery.each(jQuery('#crowdfunding_url')[0].files, function (i, file) {
                    formData.append('crowdfunding_url', file);
                });

                jQuery.each(jQuery('#product-shot')[0].files, function (i, file) {
                    formData.append('product_shot_url', file);
                });


                let url = '/venture-store-media/' + venture_id;

                console.log(url);

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        alert(response.message);
                        sessionStorage.clear();
                    },
                    error: function (response) {
                        alert(response.message);
                    }
                });
            });
        </script>
    @endpush
@endsection

