@extends('layouts.marketing-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <br>
    <br>
    {{ Breadcrumbs::render('marketing-create-incubatees')}}

    <div class="container">
        <br>
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Marketing Create
                Incubatee</h6>
            <div class="row card hoverable">
                <div class="col s12">
                    <ul class="tabs">
                        {{--<li class="tab col s4"><a href="#test1" class="active">Start Up Details</a></li>--}}
                        <li class="tab col s6"><a href="#test2">Personal Details</a></li>
                        <li class="tab col s6"><a href="#test3">Media</a></li>
                    </ul>
                </div>
                <div id="test2" class="col s12">
                    <form id="incubatee-form" class="col s12" style="margin-top:1em;" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="title">
                                    <option value="" disabled selected>Choose Title</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Miss">Miss</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr">Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="initials" type="text" class="validate">
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="name" type="text" class="validate">
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="surname" type="text" class="validate">
                                <label for="surname">Surname</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="id_number" type="text" class="validate">
                                <label for="id_number">ID Number</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="dob" type="date" class="validate">
                                <label for="dob">Date of Birth</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="age" type="text" class="validate" placeholder="Age">
                                {{--<label for="age">Age</label>--}}
                            </div>
                            <div class="input-field col m6">
                                <select id="gender">
                                    <option value="" disabled selected>Choose Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                                <label>Gender</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="email" type="email" class="validate">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="tel" class="validate">
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="address_one" type="text" class="validate">
                                <label for="address_one">Physical Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="address_two" type="text" class="validate">
                                <label for="address_two">Physical Address 2</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="address_three" type="text" class="validate">
                                <label for="address_three">Physical Address 3</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="city" type="text" class="validate">
                                <label for="city">City</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="code" type="text" class="validate">
                                <label for="code">Postal Code</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="nmu_graduate">
                                    <option value="" disabled selected>Are you an NMU Graduate?</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <label>NMU Graduate</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="disabled">
                                    <option value="" disabled selected>Do you have Disabilities?</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <label>Disability</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="home_language">
                                    <option value="" disabled selected>Choose your Home Language?</option>
                                    <option value="Xhosa">English</option>
                                    <option value="IsiXhosa">IsiXhosa</option>
                                    <option value="IsiZulu">IsiZulu</option>
                                    <option value="Afrikaans">Afrikaans</option>
                                    <option value="IsiNdebele">IsiNdebele</option>
                                    <option value=" Sepedi"> Sepedi</option>
                                    <option value="SeTswana">SeTswana</option>
                                    <option value="TshiVenḓa">TshiVenḓa</option>
                                    <option value="SiSwati">SiSwati</option>
                                    <option value="XiTsonga">XiTsonga</option>
                                    <option value="SeSotho">SeSotho</option>
                                </select>
                                <label>Home Language</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="residence">
                                    <option value="" disabled selected>Choose Resident</option>
                                    <option value="Urban">Urban</option>
                                    <option value="Peri-Urban">Peri-Urban</option>
                                    <option value="Rural">Rural</option>
                                </select>
                                <label>Resides</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_facebook" type="text" class="validate">
                                <label for="personal_facebook">Facebook</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_linked_in" type="text" class="validate">
                                <label for="personal_linked_in">LinkedIn</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_twitter" type="text" class="validate">
                                <label for="personal_twitter">Twitter</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_instagram" type="text" class="validate">
                                <label for="personal_instagram">Instagram</label>
                            </div>
                            <div class="input-field col m6">
                                <textarea id="short_bio" class="materialize-textarea" data-length="120"></textarea>
                                <label for="short_bio">About Me</label>
                            </div>
                            {{--<div class="input-field col m6">
                                <input id="short_bio" type="text" class="validate">
                                <label for="short_bio">About Me</label>
                            </div>--}}
                        </div>
                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-personal-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="test3" class="col s12">
                    <form id="media_form" class="col s12" style="margin-top:1em;">
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Profile Picture</span>
                                        <input id="profile_picture_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <img src="" id="previewings">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Copy of ID Document</span>
                                        <input id="id_document" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <img src="" id="previewing">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-media" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
        .tabs .tab a:hover, .tabs .tab a.active {
            color: black !important;
            font-weight: bolder;
        }

        .tabs .tab a {
            color: black !important;
        }
    </style>
    @push('custom-scripts')
        <script>
            let formData = new FormData();
            let incubatee = {};
            $(document).ready(function () {
                $('.datepicker').datepicker();
                $('select').formSelect();
                $('.tooltipped').tooltip();


                $(".input-select").on("change", checkSelect);
                $("#alumniDate").hide();
                $("#exitDate").hide();
                $("#resignDate").hide();

                function checkSelect() {
                    if ($('#status').val() == 'Alumni') {
                        $("#alumniDate").show();
                        $("#exitDate").hide();
                        $("#resignDate").hide();
                    }
                    if ($('#status').val() == 'Exit') {
                        $("#exitDate").show();
                        $("#alumniDate").hide();
                        $("#resignDate").hide();
                    }
                    if ($('#status').val() == 'Resigned') {
                        $("#resignDate").show();
                        $("#alumniDate").hide();
                        $("#exitDate").hide();
                    }
                    if ($('#status').val() == 'Active') {
                        $("#alumniDate").hide();
                        $("#exitDate").hide();
                        $("#resignDate").hide();
                    }
                }

                $("#rent_amount").val() == "";
                $("#rent_date").val() == "";
                $("#rentDate").hide();
                $("#rentAmount").hide();
                let now = new Date();
                let day = ("0" + now.getDate()).slice(-2);
                let month = ("0" + (now.getMonth() + 1)).slice(-2);
                let today = now.getFullYear() + "-" + (month) + "-" + (day);

                $(".inputs-select").on("change", rentSelect)

                function rentSelect() {
                    if ($('#rent_turnover').val() == 'No') {
                        $("#rentDate").hide();
                        $("#rent_date").val(today).show();
                        $("#rentAmount").hide();
                        $("#rent_amount").val() == 0;
                    }
                    if ($('#rent_turnover').val() == 'Yes') {
                        $("#rentDate").show();
                        $("#rentAmount").show();
                        $("#rent_amount").val() == "";
                        $("#rent_date").val() == "";

                    }

                }


            });
            // Function to preview image after validation
            $(function () {
                $("#id_document").change(function () {
                    $("#message").empty(); // To remove the previous error message
                    var file = this.files[0];
                    var imagefile = file.type;
                    var match = ["image/jpeg", "image/png", "image/jpg"];
                    if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                        $('#previewing').attr('src', 'noimage.png');
                        $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                        return false;
                    } else {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        reader.readAsDataURL(this.files[0]);
                    }
                });
            });

            $(function () {
                $("#profile_picture_url").change(function () {
                    $("#message").empty(); // To remove the previous error message
                    var file = this.files[0];
                    var imagefile = file.type;
                    var match = ["image/jpeg", "image/png", "image/jpg"];
                    if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                        $('#previewing').attr('src', 'noimage.png');
                        $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                        return false;
                    } else {
                        var reader = new FileReader();
                        reader.onload = imageIsLoadedd;
                        reader.readAsDataURL(this.files[0]);
                    }
                });
            });

            function imageIsLoaded(e) {
                $("#id_document").css("color", "green");
                $('#image_preview').css("display", "block");
                $('#previewing').attr('src', e.target.result);
                $('#previewing').attr('width', '250px');
                $('#previewing').attr('height', '230px');
            }

            function imageIsLoadedd(e) {
                $("#profile_picture_url").css("color", "green");
                $('#image_preview').css("display", "block");
                $('#previewings').attr('src', e.target.result);
                $('#previewings').attr('width', '250px');
                $('#previewings').attr('height', '230px');
            }


            $("#dob").change(function () {
                let value = $("#dob").val();
                let dob = new Date(value);
                let today = new Date();
                let age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                if (isNaN(age)) {
                    // will set 0 when value will be NaN
                    age = 0;
                } else {
                    age = age;
                }
                let ages = $('#age').val(age);

                $('#incubatee-form').on('submit', function (e) {

                    let venture_id = sessionStorage.getItem('venture_id');

                    e.preventDefault();
                    formData.append('title', $('#title').val());
                    formData.append('initials', $('#initials').val());
                    formData.append('dob', $('#dob').val());
                    formData.append('age', $('#age').val(ages));
                    formData.append('gender', $('#gender').val());
                    formData.append('id_number', $('#id_number').val());
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('email', $('#email').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('personal_facebook_url', $('#personal_facebook').val());
                    formData.append('personal_linkedIn_url', $('#personal_linked_in').val());
                    formData.append('personal_twitter_url', $('#personal_twitter').val());
                    formData.append('personal_instagram_url', $('#personal_instagram').val());
                    formData.append('address_one', $('#address_one').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    formData.append('city', $('#city').val());
                    formData.append('code', $('#code').val());
                    formData.append('nmu_graduate', $('#nmu_graduate').val());
                    formData.append('disabled', $('#disabled').val());
                    formData.append('residence', $('#residence').val());
                    formData.append('home_language', $('#home_language').val());
                    formData.append('short_bio', $('#short_bio').val());

                    jQuery.each(jQuery('#id_document')[0].files, function (i, file) {
                        formData.append('id_document', file);
                    });

                    console.log("user ", formData);

                    $.ajax({
                        url: "{{ route('incubatee.store') }}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            /*console.log("success", response);
                            alert(response.message);*/
                            $("#save-incubatee-personal-details").notify(
                                "You have successfully Saved Incubatee", "success",
                                {position: "right"}
                            );
                            incubatee = response.incubatee;
                            sessionStorage.setItem('incubatee_id', response.incubatee['id']);


                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });

            });


            $('#address_check').click(function () {
                if ($(this).is(':checked')) {
                    let physical_address_1 = $('#physical_address_1').val();
                    let physical_address_2 = $('#physical_address_2').val();
                    let physical_address_3 = $('#physical_address_3').val();
                    let physical_city = $('#physical_city').val();
                    let physical_code = $('#physical_code').val();

                    $('#postal_address_1').val(physical_address_1);
                    $('#postal_address_2').val(physical_address_2);
                    $('#postal_address_3').val(physical_address_3);
                    $('#postal_city').val(physical_city);
                    $('#postal_code').val(physical_code);
                } else {
                    formData.append('postal_address_1', $('#postal_address_1').val());
                    formData.append('postal_address_2', $('#postal_address_2').val());
                    formData.append('postal_address_3', $('#postal_address_3').val());
                    formData.append('postal_city', $('#postal_city').val());
                    formData.append('postal_code', $('#postal_code').val());
                }
            });


            $(function () {
                $("#Virtual_or_Physical").change(function () {
                    if ($("#Physical").is(":selected")) {
                        $("#precinct_info").show();

                    } else {
                        $("#precinct_info").hide();

                    }
                }).trigger('change');
            });

            $('#media_form').on('submit', function (e) {
                e.preventDefault();

                let incubatee_id = sessionStorage.getItem('incubatee_id');


                let formData = new FormData();
                formData.append('incubatee_id', incubatee_id);

                jQuery.each(jQuery('#profile_picture_url')[0].files, function (i, file) {
                    formData.append('profile_picture_url', file);
                });

                jQuery.each(jQuery('#id_document')[0].files, function (i, file) {
                    formData.append('id_document', file);
                });

                let url = 'incubatee-store-media/' + incubatee_id;

                console.log(url);

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        /*console.log("success", response);
                        alert(response.message);*/
                        $("#save-incubatee-media").notify(
                            "You have successfully Saved Incubatee Media", "success",
                            {position: "right"}
                        );
                        window.location.href = 'incubatees';
                        sessionStorage.clear();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = error.response.message;
                        let errors = error.response.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);
                    }
                });
            });
        </script>
    @endpush
@endsection

