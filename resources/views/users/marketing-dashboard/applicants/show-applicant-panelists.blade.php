@extends('layouts.marketing-layout')

@section('content')
    <head>
        <style>
            table {
                width:100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
                text-align: left;
            }
            table#t01 tr:nth-child(even) {
                background-color: #eee;
            }
            table#t01 tr:nth-child(odd) {
                background-color: #fff;
            }
            table#t01 th {
                background-color: white;
                color: black;
            }
            nav {
                margin-bottom: 0;
                background-color: grey;
                padding: 5px 16px;
            }
        </style>
    </head>

    <br>
    <br>
    {{ Breadcrumbs::render('marketing-applicants-next')}}
    <div class="section" style="margin-top: 1em;">
        <input id="applicant-id-input" value="{{$applicant->id}}" hidden disabled>
        <div class="row center">
            <div class="row center" style="background-color: #123c24;">
                <h5 style="color: white;">Panelists scores <i class="material-icons">arrow_downward</i> </h5>
            </div>

            <div class="row" style="margin-left: 3em; margin-right: 3em;">
                @foreach($final_panelist_question_score_array as $panelist)
                    <div class="col l3 m6 s12 center">
                        <ul class="collapsible">
                            <li>
                                <div class="collapsible-header"><h5>{{$panelist->name}} {{$panelist->surname}}'s score</h5></div>
                                <div class="collapsible-body">
                        <span>
                            @if(count($panelist->user_questions_scores) > 0)
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Question Number</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>Question Score</h6>
                                </div>
                            </div>
                                @if(isset($panelist->user_questions_scores))
                                    @foreach($panelist->user_questions_scores as $question_answer)
                                        <div class="row">
                                        <div class="col l6 m6 s12" style="background-color: darkgray;">
                                            <p>{{$question_answer->question_number}}</p>
                                        </div>
                                        <div class="col l6 m6 s12" style="background-color: dimgray;">
                                            <p>{{$question_answer->question_score}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Total Score</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>{{isset($panelist->panelist_applicant_score) ? $panelist->panelist_applicant_score : 0}}</h6>
                                </div>
                            </div>
                            @else
                                <div class="row center">
                                    <h6>The panelist has not completed the score sheet.</h6>
                                </div>
                            @endif
                        </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row center">
            <h5>Total combined score: <b>{{$total_panelist_score}}</b></h5>
            <h5>Total average score: <b>{{$average_panelist_score}}</b></h5>
                <button class="btn green" id="{{$applicant->id}}" onclick="downloadExcel(this)">Excel</button>
        </div>
    </div>

    <div class="section" style="margin-bottom: 3em; margin-top: 3em;">
        <div class="row center">
            <div class="row center" style="background-color: #1a237e;">
                <h5 style="color: white;"> Combined scores per question <i class="material-icons">arrow_downward</i></h5>
            </div>

            <div class="row" style="margin-right: 3em; margin-left: 3em;">
                <table id="t01">
                    <tr>
                        <th>Question Number</th>
                        <th>Total Score</th>
                    </tr>
                    @foreach($question_scores_array as $question_score)
                        <tr>
                            <td>{{$question_score->question_number}}</td>
                            <td>{{$question_score->question_score}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

    <div class="section">
        <input hidden disabled id="applicant-id-input" value="{{$applicant->id}}">
        <div class="row center" style="background-color: #323232">
            <h5 style="color: white;"> Want to see more details? <i class="material-icons">arrow_downward</i></h5>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="applicant-panelists-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Selection Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.collapsible').collapsible();
            $(function () {
                let applicant_id = $('#applicant-id-input').val();
                $('#applicant-panelists-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/get-applicant-panelists/' + applicant_id,
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'selection_date', name: 'selection_date'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="applicant-panelists-table_length"]').css("display","inline");
            });
        });

        function downloadExcel(obj){
            let applicant_id = obj.id;
            window.location.href = '/extract-applicant-panelists-data/' + applicant_id;
        }

    </script>
@endsection
