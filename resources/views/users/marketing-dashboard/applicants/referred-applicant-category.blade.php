@extends('layouts.marketing-layout')

@section('content')
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Referred Applicant Categories</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="referred-applicant-categories-table">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <th># of Applicants</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
           data-tooltip="Add New Category"
           href="#declined-applicant-category-modal">
            <i class="large material-icons">add</i>
        </a>
    </div>

    <!-- Modal Structure -->
    <div id="declined-applicant-category-modal" class="modal">
        <div class="modal-content">
            <h4>Enter Category Name</h4>
            <input type="text" id="referred-applicant-category-name-input">
        </div>
        <div class="modal-footer">
            <a class="btn-flat" id="referred-applicant-category-name-button">Submit</a>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#referred-applicant-categories-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-referred-applicant-categories')}}',
                    columns: [
                        {data: 'category_name', name: 'category_name'},
                        {data: 'applicant_count', name: 'applicant_count'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="referred-applicant-categories-table_length"]').css("display","inline");
            });
        });

        $('#referred-applicant-category-name-button').on('click', function(){
            $(this).text("Loading...");
            let url = '/create-referred-applicant-category';
            let formData = new FormData();
            formData.append('category_name', $('#referred-applicant-category-name-input').val());
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success: function (response, a, b) {
                    $("#referred-applicant-category-name-input").notify(response.message, "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },
                error: function (response) {
                    let message = response.message;
                    alert(message);
                }
            });
        });
    </script>
@endsection

