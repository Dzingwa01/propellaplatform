@extends('layouts.marketing-layout')

@section('content')

    <br>
    <div class="section" style="margin-top: 5vh">
        <input id="user-id-input" value="{{$declinedApplicant->id}}" hidden disabled>
        <div class="col s10 card z-depth-4" style="border-radius: 10px;width: 1350px;margin: 0 auto">
            {{--APPPLICATION ACOUNT INFO--}}
            <div class="row" id="applicant-account-show" style="margin-right: 3em; margin-left: 3em;">
                <br>
                <h4 class="center"><b>OVERVIEW OF : {{$declinedApplicant->name}} {{$declinedApplicant->surname}}</b></h4>
                <div class="row " style="margin-left: 10em">
                    <br>
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h6><b>Title : </b> {{$declinedApplicant->title}}</h6>
                            <h6><b>Initial : </b> {{$declinedApplicant->initials}}</h6>
                            <h6><b>Name : </b> {{$declinedApplicant->name}}</h6>
                            <h6><b>Surname : </b> {{$declinedApplicant->name}}</h6>
                            <h6><b>Email : </b> {{$declinedApplicant->email}}</h6>
                            <h6><b>Contact number : </b> {{$declinedApplicant->contact_number}}</h6>
                            <h6><b>ID Number : </b> {{$declinedApplicant->id_number}}</h6>
                            <h6><b>Date of birth: </b> {{$declinedApplicant->dob}}</h6>
                            <h6><b>Age : </b> {{$declinedApplicant->age}}</h6>
                        </div>
                        <div class="col l6 m6 s12">
                            <h6><b>Address One : </b> {{$declinedApplicant->address_one}}</h6>
                            <h6><b>Address Two  </b> {{$declinedApplicant->address_two}}</h6>
                            <h6><b>Address Three : </b> {{$declinedApplicant->address_three}}</h6>
                            <h6><b>Application Created : </b> {{$declinedApplicant->created_at}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s2"></div>
        <div class="row center">
            <h4><b>ACTIONS</b></h4>
        </div>
        <div class="row" style="margin-left: 600px">
            <div class="card col s4" id="application" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">APPLICATION FORM</p>
            </div>
        </div>
    </div>

    <div class="section" style="margin-top: 5vh">
        <div class="col s12 card"  id="application-details-row" hidden style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            {{--APPLICATION FORM--}}
            <br>
            <div class="row"   style="margin-right: 3em; margin-left: 3em;">
                <div class="container" id="QuestionsAndAnswers">
                    <div class="row center">
                        <h4><b>APPLICATION DETAILS</b></h4>
                        <br>
                        <hr style="background: darkblue; height: 5px;">
                        <br>
                    </div>
                    <br>
                    <div class="row">
                        @foreach($declinedApplicantQuestionAnswers as $question_answer)
                            <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                            <div class="input-field">
                                <textarea disabled>{{$question_answer->answer_text}}</textarea>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $('#application').on('click', function () {
            $('#application-details-row').show();
        });

        $(document).ready(function(){

        });
    </script>
@endsection
