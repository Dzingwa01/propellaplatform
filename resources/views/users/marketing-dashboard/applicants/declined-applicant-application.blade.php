@extends('layouts.marketing-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('marketing-declined-applicant-app')}}
    <div class="container" id="QuestionsAndAnswers">
        <br>
        <div class="row center">
            <h4><u>Declined Applicant Details:</u></h4>
            <div class="row">
                <div class="col l6 m6 s12">
                    <h6>Name & Surname : {{$declinedApplicant->name}} {{$declinedApplicant->surname}}</h6>
                    <h6>Category: {{$declinedApplicant->chosen_category}}</h6>
                    <h6>Email: {{$declinedApplicant->email}}</h6>
                    <h6>Contact Number: {{$declinedApplicant->contact_number}}</h6>
                    <h6>ID Number: {{$declinedApplicant->id_number}}</h6>
                    <h6>Age: {{$declinedApplicant->age}}</h6>
                    <h6>Gender: {{$declinedApplicant->gender}}</h6>
                </div>
                <div class="col l6 m6 s12">
                    <h6>Address One: {{$declinedApplicant->address_one}}</h6>
                    <h6>Address Two: {{$declinedApplicant->address_two}}</h6>
                    <h6>Address Three: {{$declinedApplicant->address_three}}</h6>
                </div>
            </div>
            <br>
            <hr style="background: darkblue; height: 5px;">
            <br>
        </div>
        <div class="row">
            @foreach($d_a_QuestionAnswersArray as $question_answer)
                <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                <div class="input-field">
                    <textarea disabled>{{$question_answer->answer_text}}</textarea>
                </div>
            @endforeach
        </div>
    </div>

    <br>
    <div class="row center">
        <button id="print-button"  class="waves-effect waves-light btn" onclick="printContent('QuestionsAndAnswers')">
            <i class="material-icons left">local_printshop</i>Print
        </button>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }
    </script>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
