@extends('layouts.app')

@section('content')

    <br>
    <br>
    <div class="row">
        <h4 class="center-align"><b>Learning Curves</b></h4>
        @for($i = 0; $i<count($learning_curves); $i++)
            @if($i === 0)
                <div class="col s12 m3">
                    <div class="card hoverable learningCurve z-depth-5 " style="cursor: pointer;background-color: lightgray">
                        <a href="#user">
                            <img class="fullscreen" style="height: 200px;width: 358px;"
                                 src="{{isset($learning_curves[$i]->learning_image_url)?'/storage/'.$learning_curves[$i]->learning_image_url:''}}">
                        </a>
                        <div class="circle">
                            <div class="card-content black-text " style="height: 200px;">
                                <div class="user-view">
                                </div>
                                <div class="center-align">
                                    <h6 class=""> {{$learning_curves[$i]->title}}</h6>
                                    <h6 class="top-blog-description"> {{$learning_curves[$i]->description}} </h6>
                                    <h6 class="date" style="font-style: italic;color: gray">{{$learning_curves[$i]->added_date}}</h6>
                                    <input hidden disabled class="learning_curve_id" data-value="{{$learning_curves[$i]->id}}">
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            @else
                <div class="col s12 m3">
                    <div class="card hoverable learningCurve z-depth-4" style="cursor: pointer;background-color: lightgray">
                        <a href="#user">
                            <img class="fullscreen" style="height: 200px;width: 358px;"
                                 src="{{isset($learning_curves[$i]->learning_image_url)?'/storage/'.$learning_curves[$i]->learning_image_url:''}}">
                        </a>
                        <div class="circle">
                            <div class="card-content black-text " style="height: 200px;">
                                <div class="user-view" align="center">
                                </div>
                                <div class="center-align">
                                    <h6 class=""> {{$learning_curves[$i]->title}}</h6>
                                    <h6 class="bottom-blog-description"> {{$learning_curves[$i]->description}} </h6>
                                    <h6 class="date" style="font-style: italic;color: gray">{{$learning_curves[$i]->added_date}}</h6>
                                    <input hidden disabled class="learning_curve_id" data-value="{{$learning_curves[$i]->id}}">
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            @endif
        @endfor
    </div>

    <script>
        $(document).ready(function () {

            //Card onclick
            $('.learningCurve').each(function () {
                let learning_curve_id = $(this).find('.learning_curve_id').attr('data-value');

                $(this).on('click', function () {
                    location.href = '/get-detailed-learning-curve/' + learning_curve_id;
                });
            });
            $('.date').each(function () {
                var max_length = 10;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<i href="#" class="more_horiz material-icons small"></i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });

            $('.top-blog-description').each(function () {
                var max_length = 40;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<br><br><i href="#" class="more_horiz material-icons small">more_horiz</i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });

            $('.bottom-blog-description').each(function () {
                var max_length = 40;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<br><br><i href="#" class="more_horiz material-icons small">more_horiz</i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });
        });
    </script>
@endsection
