@extends('layouts.admin-layout')

@section('content')
    <head>
        <meta charset="UTF-8">
        <title>bootstrap4</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
    </head>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <body>

    <!--Create latest info-->
    <div class="" style="margin-top: 10vh">
        <div class="card" style="width: 1070px;margin: 0 auto">
            <br>
            <h4 style="margin-left: 350px">Create latest info</h4>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col m6">
                    <input id="title" type="text" class="validate">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col m6">
                    <input id="description" type="text" class="validate">
                    <label for="description">Description</label>
                </div>
            </div>
            <div class="row" style="margin-right: 2em;margin-left: 2em;">
                <div class="file-field input-field col m6">
                    <div class="btn" style="height: 50px;">
                        <span>Image</span>
                        <input type="file" id="info_image_url">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>
                <div class="col m6">
                    <div class="file-field input-field" style="bottom:0px!important;">
                        <div class="btn">
                            <span>Upload PDF</span>
                            <input id="pdf_url" type="file" name="pdf_url">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path"
                                   type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="col m6">
                    <div class="file-field input-field" style="bottom:0px!important;">
                        <div class="btn">
                            <span>Upload Word doc</span>
                            <input id="word_doc_url" type="file" name="word_doc_url">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path"
                                   type="text">
                        </div>
                    </div>
                </div>
                <div class="input-field col m6">
                    <input id="date" type="date" class="validate">
                    <label for="date">Date</label>
                </div>
            </div>
            <div class="row" style="margin-left: 800px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn section" id="latest-info-upload-submit-button">Save</a>
                </div>
            </div>

            <br>
        </div>
    </div>

    <!--Create Learning curve summernote-->
    <div class="container">
        <div class="panel-body">
            <form action="{{route('store-latest-info-summernote')}}" method="POST" style="margin-top: 50px" id="summernote-form">
                <div class="form-group">
                    <textarea id="summernote" name="summernoteInput" class="summernote"></textarea>
                </div>

                <div class="form-group">
                    <button type="submit">Submit</button>
                    {!!csrf_field()!!}

                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#latest-info-upload-submit-button').on('click', function () {
                let title = $('#title').val();
                let description = $('#description').val();
                let date = $('#date').val();

                if (title === "" || description === "" || date === "") {
                    alert("Please insert a title and description!");
                } else {
                    let formData = new FormData();
                    formData.append('title', title);
                    formData.append('description', description);
                    formData.append('date', date);

                    jQuery.each(jQuery('#info_image_url')[0].files, function (i, file) {
                        formData.append('info_image_url', file);
                    });
                    jQuery.each(jQuery('#pdf_url')[0].files, function (i, file) {
                        formData.append('pdf_url', file);
                    });
                    jQuery.each(jQuery('#word_doc_url')[0].files, function (i, file) {
                        formData.append('word_doc_url', file);
                    });

                    let url = "{{route('store-latest-info')}}";
                    $.ajax({
                        url: url,
                        data: formData,
                        type: 'post',
                        processData: false,
                        contentType: false,
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            alert(response.message);
                            $('.second-row').show();

                            $('#summernote-form').append(
                                '<input name="latest_info_id" value="'+response.latest_info_id+'" hidden >'
                            );
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                }
            });
        });

        $('#summernote').summernote({
            placeholder: 'Content here ..',
            height: 700,
        });
    </script>
    </body>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

    </head>



    <head>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

        <!-- include summernote css/js-->
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    </head>


@endsection
