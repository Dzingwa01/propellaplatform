@extends('layouts.marketing-layout')

@section('content')
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Marketing Activity Logs</h6>
            {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="contact-logs-table">
                    <thead>
                    <tr>
                        <th>Ref# </th>
                        <th>Contact Date</th>
                        <th>Contact Info</th>
                        <th>Contact Type</th>
                        <th>Description</th>
                        <th>Caller</th>
                        <th>Receiver</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Contact Log" href="{{url('add-activity-log')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>

    </div>
    <style>
        th{
            text-transform: uppercase!important;
        }
    </style>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {

                $(function () {
                    $('#contact-logs-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-contact-logs')}}',
                        columns: [
                            {data: 'reference_number', name: 'reference_number'},
                            {data: 'contact_date', name: 'contact_date'},
                            {data: 'receiver.contact_number', name: 'receiver.contact_number'},
                            {data: 'contact_type[0].type_name', name: 'contact_type[0].type_name'},
                            {data: "description",name:"description"},
                            {data: "caller.name", name:"caller.name"},
                            {data: "receiver.name", name:"receiver_name"},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="contact-logs-table_length"]').css("display","inline");

                });
                $('#save-contact-log').on('click',function(){
                    let formData = new FormData();
                    formData.append('company_name', $('#company_name').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('address_one', $('#address_one').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    formData.append('city', $('#city').val());

                    formData.append('postal_code', $('#postal_code').val());
                    formData.append('website_url', $('#website_url').val());
                    formData.append('category_id', $('#category_id').val());
                    console.log("company ", formData);

                    $.ajax({
                        url: "{{ route('companies.store') }}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success",response);
                            alert(response.message);
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            console.log("error",message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error",error)
                                if( errors.hasOwnProperty(error) ) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });

            });

        </script>
    @endpush
@endsection
