@extends('layouts.marketing-layout')
@section('content')
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Assigned Enquiries</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="assigned-enquiries-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Enquiry Category</th>
                        <th>Date Enquired</th>
                        <th>Enquiry</th>
                        <th>Date Assigned</th>
                        <th>Assigned To</th>
                        <th>Next Step</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#assigned-enquiries-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-assigned-enquiries')}}',
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'contact_number', name: 'contact_number'},
                        {data: 'category', name: 'category'},
                        {data: 'date_enquired', name: 'date_enquired'},
                        {data: 'enquiry_message', name: 'enquiry_message'},
                        {data: 'date_assigned', name: 'date_assigned'},
                        {data: 'enquiry_assigned_person', name: 'enquiry_assigned_person'},
                        {data: 'enquiry_next_step', name: 'enquiry_next_step'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="assigned-enquiries-table_length"]').css("display","inline");
            });
        });
    </script>
@endsection
