@extends('layouts.marketing-layout')

@section('content')

    <br>

    <head>
        <meta charset="UTF-8">
        <title>bootstrap4</title>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <script src="https://cdn.tiny.cloud/1/h9arxjl10ypkgljjm33ddt16vik05kh57kxh07smgu9b7ftw/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>


        <script>
            tinymce.init({
                selector: 'textarea',
                branding: false,
                placeholder: 'Content here ..',
                height: 700,
                plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker searchreplace preview insertdatetime anchor print charmap emoticons image imagetools help wordcount fullscreen colorpicker',
                toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table advancedlist directionality  insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image paste',
                imagetools_cors_hosts: ['thepropella.co.za', '127.0.0.1:8000'],
                toolbar_mode: 'floating',
                tinycomments_mode: 'embedded',
                tinycomments_author: 'Author name',

                /* enable title field in the Image dialog*/
                image_title: true,
                /* enable automatic uploads of images represented by blob or data URIs*/
                automatic_uploads: true,
                /*
                  URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
                  images_upload_url: 'postAcceptor.php',
                  here we add custom filepicker only to Image dialog
                */
                file_picker_types: 'image',
                /* and here's our custom image picker*/
                file_picker_callback: function (cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    input.onchange = function () {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function () {
                            /*
                              Note: Now we need to register the blob in TinyMCEs image blob
                              registry. In the next release this part hopefully won't be
                              necessary, as we are looking to handle it internally.
                            */
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            /* call the callback and populate the Title field with the file name */
                            cb(blobInfo.blobUri(), { title: file.name });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                },
            });
        </script>

        <style>
            ul.breadcrumb {
                padding: 10px 16px;
                list-style: none;
                background-color:grey;
            }
            ul.breadcrumb li {
                display: inline;
                font-size: 18px;
            }
            ul.breadcrumb li+li:before {
                padding: 8px;
                color: white;
                content: ">\00a0";
            }
            ul.breadcrumb li a {
                color: black;
                text-decoration: none;
            }
        </style>
    </head>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

    </head>
    <br>
    <br>
    <body>
    <ul class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="/blog-index">All Blogs</a></li>
        <li><a href="/edit-blog">Edit Blog</a></li>
    </ul>
    <!--Create blog-->
    <div class="" style="margin-top: 10vh">
        <form id="blog-form" class=" col s12" style="margin-top:1em;">
            @csrf
            <div class="card" style="width: 1070px;margin: 0 auto">
                <br>
                <h4 class="text-center">Update Blog</h4>
                <input data-value="{{$blog->id}}" id="blog_id" hidden>
                <div class="row" style="margin-left: 2em;margin-right: 2em;">
                    <div class="input-field col m4">
                        <input id="author" type="text" class="validate" value="{{$blog->author}}"  >
                        <label for="author">Author</label>
                    </div>
                    <div class="input-field col m4">
                        <input id="title" type="text" class="validate" value="{{$blog->title}}">
                        <label for="title">Title</label>
                    </div>
                    <div class="input-field col m4">
                        <input id="description" type="text" class="validate" value="{{$blog->description}}">
                        <label for="description">Description</label>
                    </div>
                </div>
                <div class="row" style="margin-right: 2em;margin-left: 2em;">
                    <div class="file-field input-field col m4">
                        <div class="btn" style="height: 50px;">
                            <span>Blog Image</span>
                            <input type="file" id="blog_image_url" >
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" value="{{$blog->blog_image_url}}">
                        </div>
                    </div>
                </div>
                <div class="row"style="margin-left: 2em;margin-right: 2em;">
                    <div class="input-field col m4">
                        <input id="pdf_description" type="text" class="validate" value="{{$blog->pdf_description}}">
                        <label for="pdf_description">PDF Description</label>
                    </div>
                    <div class="col m6">
                        <div class="file-field input-field" style="bottom:0px!important;">
                            <div class="btn">
                                <span>Upload PDF</span>
                                <input id="pdf_url" type="file" name="pdf_url" value="{{isset($blog->pdf_url)?$blog->pdf_url:''}}">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path"
                                       type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col offset-m4">
                        <a
                            class="btn waves-effect waves-light" href="/blog-index">Cancel
                            <i class="material-icons left">arrow_back</i>
                        </a>
                        <button id="blog-upload-submit-button" style="margin-left: 2em"
                                class="btn waves-effect waves-light" type="submit" name="action">Update
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
                <br>
            </div>
        </form>
    </div>

    <!--create summernote-->
    <div class="container">
        <div class=>
            <form action="{{url('/update-blog/'.$blog->id)}}" method="POST" style="margin-top: 50px" class="card" id="summernote-form">
                <div class="form-group">
                    <textarea id="summernote" name="content" class="summernote">{{$blog->summernote->content}}</textarea>
                </div>
                <button type="submit" class="waves-effect waves-light btn section ">Submit</button>
                @csrf
            </form>

        </div>
    </div>

    @push('custom-scripts')
        <script>

            $(document).ready(function () {

                $('#blog-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();

                    formData.append('blog_id', $('#blog_id').val());
                    formData.append('author', $('#author').val());
                    formData.append('title', $('#title').val());
                    formData.append('description', $('#description').val());
                    formData.append('body', $('#body').val());
                    formData.append('blog_date', $('#blog_date').val());
                    formData.append('video', $('#video').val());
                    formData.append('content', $('.content').val());
                    formData.append('pdf_description', $('#pdf_description').val());

                    jQuery.each(jQuery('#pdf_url')[0].files, function (i, file) {
                        formData.append('pdf_url', file);
                    });

                    jQuery.each(jQuery('#blog_image_url')[0].files, function (i, file) {
                        formData.append('blog_image_url', file);
                    });

                    console.log("blog", formData);
                    let url = '/update-blog-one/' + '{{$blog->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response, a, b) {
                            alert(response.message);
                            /*window.location.href = 'blog-index';*/
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
                var content = {!! json_encode($blog->summernote->content) !!};
                $('.summernote').summernote('code', content);
                $('#summernote').summernote({
                    placeholder: 'Content here ..',
                    height: 700,
                });
            });
        </script>
    @endpush
    </body>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>


    </head>



    <head>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

        <!-- include summernote css/js-->

    </head>
@endsection
