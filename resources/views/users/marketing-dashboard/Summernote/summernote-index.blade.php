@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Blog/blog.css"/>

    <style>
        .container {
            position: relative;
        }

        .text-block {
            position: absolute;
            bottom: 20px;
            margin-left: 2em;
        }
    </style>z

    <div class="blogDesktop" >
        <div class="section ">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/why_are_blogs_such_a_big_deal.jpg" style="width: 100%;height: 40vh;">
                    </div>
                </div>
            </div>
        </div>
        <div class="section " style="margin-right: 10px; margin-left: 10px;">
            <div class="row timeline" style="margin-left: 5em;margin-right: 5em;">
                @for($i = 0; $i<count($blogs); $i++)
                    @if($i === 0)
                        <br>
                        <br>
                        <div class="container col s12 m6 blog ">
                            @if($blogs[$i]->blog_image_url != null)
                                {{--                            <img src="/images/why_are_blogs_such_a_big_deal.jpg" style="width:100%;height: 40vh;border: 5px solid orange;filter: opacity(70%)">--}}
                                <img style="width:100%;height: 45vh;border: 5px solid orange;filter: opacity(70%)" src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                <div class="text-block">
                                    <h4><b>{{$blogs[$i]->title}}</b></h4>
                                    <p class="top-blog-description"><b>{{$blogs[$i]->description}}</b></p>
                                    <h6 class="date">{{$blogs[$i]->blog_date}}</h6>
                                    <input hidden disabled class="blog_id" data-value="{{$blogs[$i]->slug}}">
                                </div>
                            @endif
                        </div>
                    @else
                        <div class="container col l6 blog card1  ">
                            @if($blogs[$i]->blog_image_url != null)
                                <img style="width:100%;height: 45vh;border: 5px solid orange;filter: opacity(70%)" src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                <div class="text-block">
                                    <h4><b>{{$blogs[$i]->title}}</b></h4>
                                    <p class="top-blog-description"><b>{{$blogs[$i]->description}}</b></p>
                                    <input hidden disabled class="blog_id" data-value="{{$blogs[$i]->slug}}">
                                    <h6 class="date">{{$blogs[$i]->blog_date}}</h6>
                                </div>
                            @endif
                        </div>

                    @endif
                @endfor
            </div>
        </div>

    </div>

    <div class="blogMobile" >
        <div class="section ">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/why_are_blogs_such_a_big_deal.jpg" style="width: 100%;height: 100%;">
                    </div>
                </div>
            </div>
        </div>
        <div class="section ">
            <div class="row timeline">
                @for($i = 0; $i<count($blogs); $i++)
                    @if($i === 0)
                        <br>
                        <br>
                        <div class="container col  m8 blog " style="margin-left: 1em">
                            @if($blogs[$i]->blog_image_url != null)
                                {{--                            <img src="/images/why_are_blogs_such_a_big_deal.jpg" style="width:100%;height: 40vh;border: 5px solid orange;filter: opacity(70%)">--}}
                                <img style="width:100%;height: 45vh;border: 5px solid orange;filter: opacity(70%)" src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                <div class="text-block">
                                    <h6> {{$blogs[$i]->blog_date}}</h6>
                                    <h4 style="color: green"><b>{{$blogs[$i]->title}}</b></h4>
                                    <p class="top-blog-description"><b>{{$blogs[$i]->description}}</b></p>
                                    <input hidden disabled class="blog_id" data-value="{{$blogs[$i]->slug}}">
                                </div>
                            @endif
                        </div>
                    @else
                        <div class="container col m8 blog card1 " style="margin-left: 1em">
                            @if($blogs[$i]->blog_image_url != null)
                                <img  style="width:100%;height: 45vh;border: 5px solid orange;filter: opacity(70%)" src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                <div class="text-block">
                                    <h6> {{$blogs[$i]->blog_date}}</h6>
                                    <h4 style="color: green"><b>{{$blogs[$i]->title}}</b></h4>
                                    <p class="top-blog-description"><b>{{$blogs[$i]->description}}</b></p>
                                    <input hidden disabled class="blog_id" data-value="{{$blogs[$i]->slug}}">
                                </div>
                            @endif
                        </div>

                    @endif
                @endfor
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function () {

            //Card onclick
            $('.blog').each(function () {
                let blog_id = $(this).find('.blog_id').attr('data-value');

                $(this).on('click', function () {
                    location.href = '/show-blog-summernote/' + blog_id;
                });
            });
            $('.date').each(function () {
                var max_length = 10;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<i href="#" class="more_horiz material-icons small"></i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });

            $('.top-blog-description').each(function () {
                var max_length = 70;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<br><br><br><i href="#" class="more_horiz material-icons small">more_horiz</i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });

            $('.bottom-blog-description').each(function () {
                var max_length = 70;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<br><br><br><i href="#" class="more_horiz material-icons small">more_horiz</i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });
        });
    </script>
@endsection
