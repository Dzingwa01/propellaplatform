@extends('layouts.marketing-layout')

@section('content')

    <br>

    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
    </head>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

    </head>

    <body>

    <!--Create blog-->
    <div class="" style="margin-top: 10vh">
        <div class="card" style="width: 1070px;margin: 0 auto">
            <br>
            <h4 style="margin-left: 350px">Create Careers</h4>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col m4">
                    <input id="title" type="text" class="validate">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col m4">
                    <input id="description" type="text" class="validate">
                    <label for="description">Description</label>
                </div>
            </div>

            <div class="row" style="margin-left: 800px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn section" id="career-upload-submit-button">Save</a>
                </div>
            </div>

            <br>
        </div>
    </div>

    <!--create summernote-->
    <div class="container">
        <div class="panel-body">
            <form action="{{route('store-career-summernote')}}" method="POST" style="margin-top: 50px" id="summernote-form">
                <div class="form-group">
                    <textarea id="summernote" name="summernoteInput" class="summernote"></textarea>
                </div>

                <div class="form-group">
                    <button type="submit">Submit</button>
                    {!!csrf_field()!!}

                </div>
            </form>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function () {

            //Create blog
            $('#career-upload-submit-button').on('click', function () {
                let title = $('#title').val();
                let description = $('#description').val();

                if (title === "" || description === "") {
                    alert("Please insert a title and description!");
                } else {
                    let formData = new FormData();
                    formData.append('title', title);
                    formData.append('description', description);


                    let url = "{{route('store-career')}}";
                    $.ajax({
                        url: url,
                        data: formData,
                        type: 'post',
                        processData: false,
                        contentType: false,
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            alert(response.message);
                            $('.second-row').show();

                            $('#summernote-form').append(
                                '<input name="career_id" value="'+response.career_id+'" hidden >'
                            );
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                }
            });
        });

        $('#summernote').summernote({
            placeholder: 'Content here ..',
            height: 700,
        });
    </script>
    </body>

    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

    </head>



    <head>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

        <!-- include summernote css/js-->
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    </head>

@endsection
