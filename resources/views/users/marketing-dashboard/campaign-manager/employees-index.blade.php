@extends('layouts.marketing-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('marketing-company-employee')}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Marketing All Contacts</h6>
            {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="company-employees-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Company</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                        <th>Interest(Primary)</th>
                        {{--<th>Address</th>--}}
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div id="modal1" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Add New Employee</h4>
                <div class="row">
                    <form class="col s12">
                        @csrf
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="name" type="text" class="validate">
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="surname" type="text" class="validate">
                                <label for="surname">Surname</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col m12">
                                <input id="email" type="email" class="validate">
                                <label for="email">Email</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m6">
                                <input id="contact_number" type="tel" class="validate">
                                <label for="contact_number">Contact Number</label>
                            </div>

                            <div class="input-field col m6">
                                <input id="contact_number_two" type="tel" class="validate">
                                <label for="contact_number_two">Secondary Contact Number</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m6">
                                <select id="company_id">
                                    <option>Select Company</option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}">{{$company->company_name}}</option>
                                    @endforeach
                                </select>
                                <label>Company</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="category_id" multiple>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                                <label>Person Interests</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <textarea id="address" class="materialize-textarea"></textarea>
                                <label for="address">Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <textarea id="address_two" class="materialize-textarea"></textarea>
                                <label for="address">Address 2</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <textarea id="address_three" class="materialize-textarea"></textarea>
                                <label for="address">Address 3</label>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn">Cancel<i class="material-icons right">close</i> </a>
                <button class="btn waves-effect waves-light" style="margin-left:2em;" id="save-company-employee" name="action">Submit
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Company Employee" href="#modal1">
                <i class="large material-icons">add</i>
            </a>

        </div>
        <style>
            th{
                text-transform: uppercase!important;
            }
        </style>
    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#company-employees-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-company-employees')}}',
                        columns: [
                            {data: 'name', name: 'name'},
                            {data: 'surname', name: 'surname'},
                            {data: "company.company_name",name:'company.company_name'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'email', name: 'email'},
                            {data: "category[0].category_name",name:"category[0].category_name"},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="company-employees-table_length"]').css("display","inline");
                });
                $('#save-company-employee').on('click',function(){
                    let formData = new FormData();
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('contact_number_two', $('#contact_number_two').val());
                    formData.append('email', $('#email').val());
                    formData.append('company_id', $('#company_id').val());
                    formData.append('category_id', $('#category_id').val());
                    formData.append('address', $('#address').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    console.log("company ", formData);
                    $.ajax({
                        url: "{{ route('company-employees.store') }}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success",response);
                            alert(response.message);
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            console.log("error",message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error",error)
                                if( errors.hasOwnProperty(error) ) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });

            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
