@extends('layouts.marketing-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('marketing-companies')}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Marketing Companies</h6>
            {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="companies-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Contact Number</th>
                        {{--<th>City</th>--}}
                        {{--<th>Postal Code</th>--}}
                        <th>Website</th>
                        <th>Address 1</th>
                        <th>Interests</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        {{--<div id="modal1" class="modal modal-fixed-footer">--}}
        {{--<div class="modal-content">--}}
        {{--<h4>Add New Company</h4>--}}
        {{--<div class="row">--}}
        {{--<form class="col s12">--}}
        {{--@csrf--}}
        {{--<div class="row">--}}
        {{--<div class="input-field col m6">--}}
        {{--<input id="company_name" type="text" class="validate">--}}
        {{--<label for="company_name">Company Name</label>--}}
        {{--</div>--}}
        {{--<div class="input-field col m6">--}}
        {{--<input id="contact_number" type="tel" class="validate">--}}
        {{--<label for="contact_number">Contact Number</label>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row">--}}
        {{--<div class="input-field col m6">--}}
        {{--<input id="website_url" type="text" class="validate">--}}
        {{--<label for="website_url">Website URL</label>--}}
        {{--</div>--}}
        {{--<div class="input-field col m6">--}}
        {{--<select id="category_id">--}}
        {{--<option>Select Category</option>--}}
        {{--@foreach($categories as $category)--}}
        {{--<option value="{{$category->id}}">{{$category->category_name}}</option>--}}
        {{--@endforeach--}}
        {{--</select>--}}
        {{--<label>Company Primary Interest</label>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}

        {{--<div class="input-field col m6">--}}
        {{--<select id="secondary_category_id" multiple>--}}
        {{--<option>Select Category</option>--}}
        {{--@foreach($categories as $category)--}}
        {{--<option value="{{$category->id}}">{{$category->category_name}}</option>--}}
        {{--@endforeach--}}
        {{--</select>--}}
        {{--<label>Company Secondary Interests</label>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row">--}}
        {{--<div class="input-field col m6">--}}
        {{--<textarea id="address_one" class="materialize-textarea"></textarea>--}}
        {{--<label for="address_one">Address 1</label>--}}
        {{--</div>--}}
        {{--<div class="input-field col m6">--}}
        {{--<textarea id="address_two" class="materialize-textarea"></textarea>--}}
        {{--<label for="address_two">Address 2</label>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
        {{--<div class="input-field col m6">--}}
        {{--<textarea id="address_three" class="materialize-textarea"></textarea>--}}
        {{--<label for="address_three">Address 3</label>--}}
        {{--</div>--}}
        {{--<div class="input-field col m6">--}}
        {{--<input id="city" type="text" class="validate">--}}
        {{--<label for="city">City</label>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}

        {{--<div class="input-field col m6">--}}
        {{--<input id="postal_code" type="text" class="validate">--}}
        {{--<label for="postal_code">Postal Code</label>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</form>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="modal-footer">--}}
        {{--<a href="#!" class="modal-close waves-effect waves-green btn">Cancel<i class="material-icons right">close</i> </a>--}}
        {{--<button class="btn waves-effect waves-light" style="margin-left:2em;" id="save-company" name="action">Submit--}}
        {{--<i class="material-icons right">send</i>--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn " data-position="left" data-tooltip="Add New Company" href="/company-create">
                <i class="large material-icons">add</i>
            </a>

        </div>
        <style>
            th{
                text-transform: uppercase!important;
            }
            nav {
                margin-bottom: 0;
                background-color: grey;
                padding: 5px 16px;
            }
        </style>
    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#companies-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-companies')}}',
                        columns: [
                            {data: 'company_name', name: 'company_name'},
                            {data: 'contact_number', name: 'contact_number'},
                            // {data: 'address_two', name: 'address_two'},
                            // {data: "address_three",name:'address_three'},
                            // {data: "city",name:"city"},
                            // {data: "postal_code", name:"postal_code"},
                            {data: "website_url", name:"website_url"},
                            {data: 'address_one', name: 'address_one'},

                            {data:'category.category_name',name:'category.category_name'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="companies-table_length"]').css("display","inline");
                });
                $('#save-company').on('click',function(){
                    let formData = new FormData();
                    formData.append('company_name', $('#company_name').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('address_one', $('#address_one').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    formData.append('city', $('#city').val());

                    formData.append('postal_code', $('#postal_code').val());
                    formData.append('website_url', $('#website_url').val());
                    formData.append('category_id', $('#category_id').val());
                    formData.append('secondary_category_id', $('#secondary_category_id').val());
                    console.log("company ", formData);

                    $.ajax({
                        url: "{{ route('companies.store') }}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success",response);
                            alert(response.message);
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            console.log("error",message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error",error)
                                if( errors.hasOwnProperty(error) ) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }

                    });
                });

            });

        </script>
    @endpush
@endsection
