@extends('layouts.marketing-layout')

@section('content')
    <div class="container-fluid">

        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Mentors</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>System Role</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Mentor" href="#modal1">
                <i class="large material-icons">add</i>
            </a>

        </div>
        <div id="modal1" class="modal modal-fixed-footer">
            <div class="modal-content">
                <form class="col s12" id="add-user" method="post">
                    <br />
                    <br />
                    <h4 style="font-size: 2em;font-family: Arial;">Add new User</h4>
                    @csrf
                    <div class="row"style="margin-left: 95px">
                        <div class="input-field col m5">
                            <input id="name" type="text" class="validate">
                            <label for="name">Name</label>
                        </div>
                        <div class="input-field col m5">
                            <input id="surname" type="text" class="validate">
                            <label for="surname">Surname</label>
                        </div>
                    </div>
                    <div class="row"style=" margin-left: 95px">
                        <div class="input-field col m5">
                            <input id="email" type="email" class="validate">
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field col m5">
                            <input id="contact_number" type="tel" class="validate">
                            <label for="contact_number">Contact Number</label>
                        </div>
                    </div>
                    <div class="row"style="margin-left: 95px">
                        <div class="input-field col m5">
                            <select id="role_id">
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->display_name}}</option>
                                @endforeach
                            </select>
                            <label>System Role</label>
                        </div>
                        <div class="input-field col m5">
                            <textarea id="address_one" class="materialize-textarea"></textarea>
                            <label for="address_one">Address</label>
                        </div>
                    </div>

                    <a href="#!" class="modal-close waves-effect waves-green btn" >Cancel<i class="material-icons right">close</i> </a>

                    <button class="btn waves-effect waves-light" style="margin-left:300px;" id="save-user" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button>
                </form>

            </div>

        </div>
    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#users-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-mentors')}}',
                        columns: [
                            {data: 'name', name: 'name'},
                            {data: 'surname', name: 'surname'},
                            {data: 'email', name: 'email'},
                            {data: 'contact_number', name: 'contact_number'},
//                            {data: 'job_title', name: 'job_title'},
                            {data:'roles[0].name',name:'roles[0].name'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="users-table_length"]').css("display","inline");
                });

            });
            $('#add-user').on('submit', function (e) {

                e.preventDefault();
                let formData = new FormData();
                formData.append('name', $('#name').val());
                formData.append('surname', $('#surname').val());
                formData.append('email', $('#email').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('role_id', $('#role_id').val());
                formData.append('address_one', $('#address_one').val());
                console.log(formData);

                $.ajax({
                    url: '{{route('add-users')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        console.log("success", response);
                        $('#save-user').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
            });
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }

    </style>
@endsection
