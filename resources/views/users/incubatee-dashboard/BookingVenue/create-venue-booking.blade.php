@extends('layouts.incubatee-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <input id="user-id-input" disabled hidden value="{{$users->id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('inc-add-venue-booking',$users,$venue)}}
    <br>
    <div class="section" id="createDesktop">
        <div class="card hoverable center" style="width: 900px; margin-left: 350px">
            <form class="col s12" id="add-booking" method="post">
                <br/>
                <br/>
                <h4 style="font-size: 2em;font-family: Arial;">Add New Venue</h4>
                @csrf
                <div class="row" style="margin-left: 55px;margin-right: 55px;">
                    <input id="user_id" value="{{$users->id}}" hidden>
                    {{--<p>{{$users->id}}</p>--}}
                    <div class="input-field col m6">
                        <input id="venue_id" data-value="{{$venue->id}}" value="{{$venue->venue_name}}" disabled>
                    </div>
                    <div class="input-field col m6">
                        <input id="venue_date" type="date" class="validate" required>
                        <label for="venue_date">Venue Date</label>
                    </div>
                </div>
                <div class="row"style=" margin-left: 55px; margin-right: 55px;">
                    <div class="input-field col m6">
                        <input id="venue_start_time" type="time" class="validate" min="8:00" max="18:00" required>
                        <label for="venue_start_time">Start Time</label>
                    </div>
                    <div class="input-field col m6">
                        <input id="venue_end_time" type="time" class="validate" min="8:00" max="18:00" required>
                        <label for="venue_end_time">End Time</label>
                    </div>
                </div>
                <div class="row"style=" margin-left: 55px; margin-right: 55px;">
                    <div class="input-field col m6">
                        <input id="booking_person" disabled value="{{$users->name}}" type="text" class="validate" required>
                        <label for="booking_person">Booking Person</label>
                    </div>
                    <div class="input-field col m6">
                        <input id="booking_reason" type="text" class="validate" required>
                        <label for="booking_reason">Booking Reason</label>
                    </div>
                </div>
                <a href="#!" class="modal-close waves-effect waves-green btn">Cancel<i class="material-icons right">close</i>
                </a>

                <button class="btn waves-effect waves-light" style="margin-left:300px;" id="save-user" name="action">
                    Submit
                    <i class="material-icons right">send</i>
                </button>
            </form>
            <br/>
            <br/>
        </div>
    </div>
    @push('custom-scripts')
        <script>
            $(document).ready(function () {
                $('.tooltipped').tooltip();
                $('select').formSelect();

                $('#add-booking').on('submit', function (e) {

                    e.preventDefault();
                    let formData = new FormData();
                    formData.append('venue_id', $('#venue_id').attr('data-value'));
                    formData.append('venue_date', $('#venue_date').val());
                    formData.append('venue_start_time', $('#venue_start_time').val());
                    formData.append('venue_end_time', $('#venue_end_time').val());
                    formData.append('booking_reason', $('#booking_reason').val());
                    formData.append('booking_person', $('#booking_person').val());
                    formData.append('user_id', $('#user_id').val());

                    console.log(formData);
                    let venue_id = formData.get('venue_id');

                    $.ajax({
                        url: "/venue-book-store",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                        success: function (response, a, b) {
                            console.log("success", response);
                            $('#save-user').notify(response.message, "success");

                            setTimeout(function(){
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
