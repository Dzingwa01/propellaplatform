@extends('layouts.incubatee-layout')

@section('content')
    {{--<input id="user-id-input" disabled hidden value="{{$user->id}}">--}}
    <br>
    <br>
    <br>
    <br/>
    {{ Breadcrumbs::render('inc-book-venue',$user)}}
    <br>
    <br>

    <div class="container" >
        <div class="row">
            <div class="center">
                <div class="col l12 s12 card" style="background: #6c757d">
                    <h5 style="color: white">Book A Venue Here!</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="center">
                @foreach($venues as $venue)
                    <div class="col s12 m4">
                        <div class="card white hoverable center" style="border-radius: 20px;">
                            <div class="circle">
                                <div class="card-content black-text" style="height: 10%;">
                                    <div class="user-view" align="center">
                                        {{--<img class="fullscreen materialboxed" style="width: 100%; height: 200px;" src="{{isset($event_list->event->event_image_url)?'/storage/'.$event_list->event->event_image_url:''}}">--}}
                                    </div>
                                </div>
                                <p class=""><b>{{$venue->venue_name}}</b></p>
                                <p class="">{{$venue->number_of_seats}}</p>
                            </div>
                            <br>
                            <br>
                            <div class="card-action">
                                <div class="row booking" style="margin-left: 0.5em;">
                                    <a href="{{url('/venue-booking/'.$venue->id)}}"
                                       class="booking-button"
                                       style="color: orange;">Book A Venue
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="container ">
        <div class="row">
            <div class="center">
                <div class="col l12 s12 m12 card" style="background: #6c757d">
                    <h5 style="color: white" id="registered-header">Venues you have booked for.</h5>
                </div>
            </div>
        </div>
        <div class="col l12 s12 m12">
            <div class="row">
                <div style="width: 100%">

                    <table style="width:100%;margin-right: 2em">
                        <tr>
                            <th>Venue</th>
                            <th>Venue Date</th>
                            <th>Start time</th>
                            <th>End time</th>
                            <th>Booking Reason</th>
                            <th>Update</th>
                            <th>Cancel</th>

                        </tr>
                        @foreach($user_bookings as $user_booking)
                            <tr>
                                <td>{{$user_booking->venue_name}}</td>
                                <td>{{$user_booking->venue_date}}</td>
                                <td>{{$user_booking->venue_start_time}}</td>
                                <td>{{$user_booking->venue_end_time}}</td>
                                <td>{{$user_booking->booking_reason}}</td>
                                <th><a class="booking-button modal-trigger"
                                       href="{{url('/edit-venue-booking/'.$user_booking->id)}}"
                                       style="color: orange;">Update Booking
                                    </a>
                                </th>
                                <th><a id="{{$user_booking->id}}"
                                       style="color: orange; cursor: pointer;" onclick="deleteBooking(this)">Cancel Booking
                                    </a></th>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


    @push('custom-scripts')
        <script>

            function deleteBooking(obj) {
                var r = confirm("Are you sure want to delete this booking?");
                console.log("Check", r);
                if (r) {
                    $.get('/cancel-delete-check/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status == 'success') {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Delete action cancelled');
                }
            }
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
