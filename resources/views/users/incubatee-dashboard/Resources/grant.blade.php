@extends('layouts.incubatee-layout')
<link rel="stylesheet" href="/css/platform_styles.css"/>
@section('content')

    <br>
    <br>
    {{ Breadcrumbs::render('inc-grant')}}
    <div class="row" style="margin-left: 2em;margin-right: 2em" id="desktopLog">
        <br>
        <p style="margin-left: 2em;font-size: 2em">{{$resourceCategory->category_name}}</p>
        @foreach($resourceCategory->fundingResource as $funding)
            @if ($funding->closing_date < $date)
                <div class=""></div>
            @else
                <div class="col s12 m4">
                            <div class=" card funding" style="top: 5vh;height: 50vh; cursor: pointer">
                                <div class="col s1"></div>
                                <div class="card-image">
                                    @if($funding->image_url != null)
                                        <img style="width: 100%; object-fit: cover; height: 15vw"
                                             src="/storage/{{isset($funding->image_url)?$funding->image_url:'Nothing Detected'}}">
                                    @endif
                                </div>
                                <div class="card-content">
                                    <p style="margin-left: 1em">{{$funding->title}}</p>
                                    <p style="margin-left: 1em">Closing Date : {{$funding->closing_date}}</p>
                                </div>
                                <input hidden disabled class="funding_id" data-value="{{$funding->id}}">
                            </div>
                        </div>
            @endif
        @endforeach
    </div>
    <div class="row" id="mobileLog">
        <br>
        <p style="margin-left: 2em;font-size: 2em">{{$resourceCategory->category_name}}</p>
        @foreach($resourceCategory->fundingResource as $funding)
            <div class="col s11 m4">
                <div class=" card funding" style="top: 5vh;height: 30vh;border-radius: 10%;cursor: pointer">
                    <br>
                    <div class="row">
                        <div class="col s6">
                            <p style="margin-left: 1em">{{$funding->title}}</p>
                            <p style="margin-left: 1em">Closing Date : {{$funding->closing_date}}</p>
                        </div>
                        <div class="col s1"></div>
                        <div class="col s5" style="margin-right: 5px">
                            @if($funding->image_url != null)
                                <img style="width: 200px"
                                     src="/storage/{{isset($funding->image_url)?$funding->image_url:'Nothing Detected'}}">
                            @endif
                        </div>

                    </div>
                    <input hidden disabled class="funding_id" data-value="{{$funding->id}}">
                </div>
            </div>
        @endforeach
    </div>
    <script>

        $('.funding').each(function () {
            let funding_id = $(this).find('.funding_id').attr('data-value');

            $(this).on('click', function () {
                location.href = '/show-all-info/' + funding_id;
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection

