@extends('layouts.incubatee-layout')

@section('content')

    <div class="" style="margin-top: 10vh">
        @if(isset($presentations->title))
            <h4 class="center-align"><b>{{$presentations->title}}</b></h4>
        @endif
        <div class="row center-align">
            <div class="col s4">
                @if($presentations->video!=null)
                    <iframe style="margin-left: 1em" src="{{isset($presentations->video)?$presentations->video:''}}" width="300" height="200" allow="autoplay;" allowfullscreen></iframe>
                @endif
            </div>
            @if(isset($presentations->handouts_uploads))
                @foreach($presentations->handouts_uploads as $handout)
                    <div class="col s2">
                        <a href="{{'/storage/'.$handout->handout_url}}" target="_blank">
                            <img style="margin-left: 1em;" class="hoverable" src="/images/pdf_image.png" width="80"
                                 height="100">
                        </a>
                        <p>HANDOUT</p>
                    </div>
                @endforeach
            @endif
            @if(isset($presentations->presentation_uploads))
                @foreach($presentations->presentation_uploads as $presentation)
                    <div class="col s2">
                        <a href="{{'/storage/'.$presentation->presentation_url}}" target="_blank">
                            <img style="margin-left: 1em;" class="hoverable" src="/images/pdf_image.png" width="80"
                                 height="100">
                        </a>
                        <p>PRESENTATION</p>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

@endsection
