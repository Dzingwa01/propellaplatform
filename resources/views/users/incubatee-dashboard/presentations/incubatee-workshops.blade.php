@extends('layouts.incubatee-layout')

@section('content')

    <div class="container-fluid" style="margin-top: 10vh">
        <h4 class="center-align"><b>PRESENTATIONS</b></h4>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Date added</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>


    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#users-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        stateSave: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-incubatee-workshops')}}',
                        columns: [
                            {data: 'title'},
                            {data: 'date'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]

                    });
                    $('select[name="users-table_length"]').css("display", "inline");
                });
            });


        </script>
    @endpush

@endsection
