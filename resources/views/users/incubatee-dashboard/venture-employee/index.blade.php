@extends('layouts.incubatee-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('inc-employees',$venture)}}
    <br>
    <div class="row">
        <input id="venture-id-input" value="{{$venture->id}}" hidden disabled>
        <h4 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">{{$venture->company_name}}
            Employees </h4>
    </div>
    <br>
    <div class="row" style="margin-left: 2em;">

        <a class="btn waves-effect waves-light" href="{{url('create-employee',$venture->id)}}">ADD EMPLOYEE
            <i class="material-icons left">person_add</i>
        </a>
    </div>
    <br>
    <div class="row">
        <div class="center">
            @foreach($venture->ventureEmployee as $venture_employee)
                <div class="col s12 m3">
                    {{-- <p>{{$venture->id}}</p>--}}
                    <div class="card hoverable center" style="border-radius: 10px;">
                        <br>
                        <div class="circle">
                            {{--<p>{{$user_booking->id}}</p>--}}
                            <h5 class=""><b>{{$venture_employee->title}}</b></h5>
                            <h5 class="">EMPLOYEE NAME: <b>{{$venture_employee->employee_name}}</b></h5>
                            <h5 class="">POSITION: <b>{{$venture_employee->position}}</b></h5>
                        </div>
                        <div class="card-action">
                            <div class="row booking" style="margin-left: 0.5em; border-radius: 30px;">
                                <a class="booking-button modal-trigger"
                                   href="{{url('/edit-employee/'.$venture_employee->id)}}"
                                   style="color: orange;">Update Employee
                                </a>
                            </div>
                            {{--<div class="row booking" style="margin-left: 0.5em; border-radius: 20px;">
                                <a id="{{$venture_employee->id}}"
                                   style="color: orange; cursor: pointer;" onclick="confirm_delete_venture(this)">Delete
                                    Employee
                                </a>
                            </div>--}}
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
    @push('custom-scripts')

        <script>

            function confirm_delete_venture(obj) {
                var r = confirm("Are you sure want to delete this employee!");
                console.log("Check", r);
                if (r) {
                    $.get('/delete-employee/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status == 'success') {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Delete action cancelled');
                }
            }

            $(document).ready(function () {
                let formData = new FormData();

                formData.append('venture-id-input', $('#venture-id-input').val());




            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
