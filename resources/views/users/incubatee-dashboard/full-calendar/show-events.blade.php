@extends('layouts.incubatee-layout')

@section('content')


    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="/css/Incubatee/incubateeHome.css">
    </head>
    <br>
    <br>
    <br/>
    <br/>
    {{ Breadcrumbs::render('inc-events',$incubatee)}}

    <br>
    <br>
    {{--    This is where the user sees their events--}}

    <input id="incubatee-id-input" disabled hidden value="{{$incubatee->id}}">

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white">Register for upcoming events.</h5>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="center">
                @foreach($all_events_array as $event)
                    @if($event->event->end > new DateTime())
                        <div class="col l4 s6 m4">
                            <div class="card white hoverable center" style="border-radius: 20px;">
                                <div class="circle">
                                    <div class="card-content black-text" style="height: 100%;">
                                        <div class="user-view" align="center">
                                            <a href="#user">
                                                <img class="fullscreen" style="height: 100px;width: 250px;"
                                                     src="{{isset($event->event->event_image_url)?'/storage/'.$event->event->event_image_url:''}}"
                                                     alt="Event Info">
                                            </a>
                                        </div>
                                        <br>
                                        <h6 class=""> {{$event->event->title}}</h6>
                                        <h6 class=""> {{$event->event->start->toDateString()}} </h6>
                                        <h6 class=""> {{$event->event_start_time}} </h6>
                                        <h6 class=""> {{$event->event->venue_types}} </h6>
                                        <h6 class="">{{$event->event->type}} </h6>
                                    </div>
                                    <div class="card-action">
                                        @if($event->event->end > new DateTime())
                                            <div class="row register" style="margin-left: 0.5em;">
                                                <a onclick="storeCurrentEventAndIncubatee(this)" href="#register-modal"
                                                   class="register-button-top-events-row modal-trigger"
                                                   data-visitor="{{$incubatee->id}}" data-event="{{$event->event->id}}"
                                                   data-type="{{$event->event->type}}"
                                                   style="color: orange;">Register
                                                </a>
                                            </div>
                                        @endif
                                        <div class="row" style="margin-left: 0.5em;">
                                            <a href="#modal1" class="modal-trigger" style="color: orange;">Event Details</a>
                                            <!-- Modal Structure -->
                                            <div id="modal1" class="modal">
                                                <div class="modal-content">
                                                    <div class="row">
                                                        <div class="col m12">
                                                            <img style="width:100%; height:100%;"
                                                                 src="/storage/{{isset($event->event->invite_image_url)?$event->event->invite_image_url:''}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#!"
                                                       class="modal-close waves-effect waves-green btn-flat">Close</a>
                                                </div>
                                            </div>
                                        </div>
                                        @if(new DateTime() > $event->event->end)
                                            <div class="row" style="margin-left: 0.5em;">
                                                <div class="view_media"
                                                     style="width:100%;">
                                                    <a style="color: orange;"
                                                       href="{{url('/full_calendar/show-gallery/'.$event->event->id)}}"
                                                       onclick="viewMedia(this)">View Gallery</a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white">Events you have registered for.</h5>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="center">
                @foreach($incubatee_events as $incubatee_event)
                    @if(isset($incubatee_event->incubatee_event_registered))
                        @if($incubatee_event->incubatee_event_registered->registered == true)
                            <div class="col l4 s6 m4">
                                <div class="card white hoverable event-cards-top-row" style="border-radius: 20px">
                                    <div class="card-content black-text circle" style="height: 100%">
                                        <div class="center">
                                            <img class="fullscreen" style="height: 100px;width: 250px;"
                                                 src="{{isset($incubatee_event->incubatee_events_by_event_id->event_image_url)?'/storage/'.$incubatee_event->incubatee_events_by_event_id->event_image_url:''}}"
                                                 alt="Event Info">
                                        </div>
                                        <h6 class=""> {{$incubatee_event->incubatee_events_by_event_id->title}}</h6>
                                        <h6 class=""> {{$incubatee_event->incubatee_events_by_event_id->start->toDateString()}} </h6>
                                        <h6 class=""> {{$incubatee_event->incubatee_event_start_time}} </h6>
                                        <h6 class=""> {{$incubatee_event->incubatee_events_by_event_id->venue_types}} </h6>
                                        <h6 class="">{{$incubatee_event->incubatee_events_by_event_id->type}} </h6>
                                    </div>
                                    <br>
                                    <div class="card-action">
                                        @if($incubatee_event->incubatee_event_registered->de_registered == true or $incubatee_event->incubatee_event_registered->attended == true)
                                            <div class="row" style="margin-left: 0.5em;">
                                                <a href="#deregister-modal" class="modal-trigger register" disabled style="color: orange;">Deregister</a>
                                            </div>
                                        @elseif(new DateTime() > $incubatee_event->incubatee_events_by_event_id->end)
                                            <div class="row" style="margin-left: 0.5em;">
                                                <a href="#deregister-modal" class="modal-trigger register" disabled style="color: orange;">Deregister</a>
                                            </div>
                                        @else
                                            <div class="row" style="margin-left: 0.5em;">
                                                <a href="#deregister-modal" class="modal-trigger register"
                                                   data-value="{{$incubatee_event->incubatee_events_by_event_id->id}}"
                                                   onclick="storeEventId(this)"
                                                   style="color: orange;"><b>Deregister</b></a>
                                            </div>
                                        @endif
                                        <div class="row" style="margin-left: 0.5em;">
                                            <a href="#modal1" class="modal-trigger" style="color: orange;">Event
                                                Details</a>
                                            <!-- Modal Structure -->
                                            <div id="modal1" class="modal">
                                                <div class="modal-content">
                                                    <div class="row">
                                                        <div class="col m12">
                                                            <img style="width:100%; height:100%;"
                                                                 src="/storage/{{isset($incubatee_event->incubatee_events_by_event_id->invite_image_url)?$incubatee_event->incubatee_events_by_event_id->invite_image_url:''}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#!"
                                                       class="modal-close waves-effect waves-green btn-flat">Close</a>
                                                </div>
                                            </div>
                                        </div>
                                        @if(new DateTime() > $incubatee_event->incubatee_events_by_event_id->end)
                                            <div class="row" style="margin-left: 0.5em;">
                                                <div class="view_media"
                                                     style="width:100%;">
                                                    <a style="color: orange;"
                                                       href="{{url('/full_calendar/show-gallery/'.$incubatee_event->incubatee_events_by_event_id->id)}}"
                                                       onclick="viewMedia(this)">View Gallery</a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div id="deregister-modal" class="modal">
        <div class="modal-content">
            <h6>Sorry to see you go :(</h6>

            <input id="deregister-input" type="text" placeholder="What is your reason for deregister?">

        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat" onclick="deregisterIncubatee()"
               id="submit-deregister-info-button">Submit</a>
        </div>
    </div>

    <div id="register-modal" class="modal">
        <div class="modal-content">
            <h5>One more step!</h5>

            <select id="found-us-via-input">
                <option value="" disabled selected>How did you hear about us?</option>
                <option value="Radio">Radio</option>
                <option value="Email Campaign">Email Campaign</option>
                <option value="Facebook">Facebook</option>
                <option value="Website">Website</option>
                <option value="LinkedIn">LinkedIn</option>
                <option value="Twitter">Twitter</option>
                <option value="Print Media">Print Media</option>
                <option value="Print Media">On programme</option>
                <option value="Other">Other</option>
            </select>
            <input id="found-us-via-other-input" hidden placeholder="Please specify?" type="text">

        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat" onclick="doRegistration()">Submit</a>
        </div>
    </div>








    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script>

        $(document).ready(function () {
            $(document).ready(function() {
                $('select').formSelect();
                let incubatee_id = $('#incubatee-id-input').val();

                $(function () {
                    $('#incubatee-registered-events-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '/get-incubatee-registered-events-via-edit/' + incubatee_id,
                        columns: [
                            {data: 'event', name: 'event'},
                            {data: 'date_registered', name: 'date_registered'},
                            {data: 'attended', name: 'attended'},
                            {data: 'eventStatus', name: 'eventStatus'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="incubatee-registered-events-table_length"]').css("display","inline");
                });
            });

            $('.modal').modal();
            $('select').formSelect();


            $('#found-us-via-input').change(function () {
                if ($('#found-us-via-input').val() === 'Other') {
                    $('#found-us-via-other-input').show();
                } else {
                    $('#found-us-via-other-input').hide();
                }
            });
        });

        let event_id;

        function storeEventId(obj) {
            event_id = obj.getAttribute('data-value');
        }

        function deregisterIncubatee() {
            let incubatee_id = $('#incubatee-id-input').val();
            let formData = new FormData();
            formData.append('incubatee_id', incubatee_id);
            formData.append('deregister_reason', $('#deregister-input').val());
            formData.append('event_id', event_id);

            if ($('#deregister-input').val() === "") {
                alert('Please fill in a reason.');
            } else {
                let url = "{{route('incubatee.deregister')}}";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            }
        }

        function storeCurrentEventAndIncubatee(obj) {
            let event_id = obj.getAttribute('data-event');
            let incubatee_id = obj.getAttribute('data-visitor');

            sessionStorage.setItem('current_incubatee', incubatee_id);
            sessionStorage.setItem('current_event', event_id);
        }

        function doRegistration() {
            let event_id = sessionStorage.getItem('current_event');
            let incubatee_id = sessionStorage.getItem('current_incubatee');
            let formData = new FormData();

            formData.append('event_id', event_id);
            formData.append('incubatee_id', incubatee_id);
            if ($('#found-us-via-input').val() === 'Other') {
                formData.append('found_out_via', $('#found-us-via-other-input').val());
            } else {
                formData.append('found_out_via', $('#found-us-via-input').val());
            }

            let url = "{{route('incubatee.storeEventViaIncubateeEventPage')}}";
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    alert(response.message);
                    sessionStorage.setItem('current_visitor', "");
                    sessionStorage.setItem('current_event', "");
                    window.location.reload();
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        }
    </script>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
