@extends('layouts.incubatee-layout')

@section('content')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <br>
    <br>

    <input id="visitor-id-input" disabled hidden value="{{$visitor->id}}">

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white">Register for upcoming events.</h5>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="center">
                @foreach($events as $event)
                    <div class="col l4">
                        <div class="card white hoverable event-cards-top-row">
                            <div class="card-content black-text ">
                                <h6 class=""> {{$event->title}}</h6>
                                <h6 class=""> Date : {{$event->start->toDateString()}} </h6>
                                <h6 class=""> End : {{$event->end->toDateString()}} </h6>
                            </div>
                            <br>
                            <div class="register" style=" width:100%;">
                                <a href="#register-modal" data-event="{{$event->id}}"
                                   data-visitor="{{$visitor->id}}" style="color: black;" onclick="storeCurrentEventAndVisitor(this)" class="register-button-top-events-row modal-trigger">
                                    <b>Register</b>
                                </a>
                            </div>
                            <div class="view_media" style="width:100%;">
                                <a style="color: black;" data-value="{{$event->id}}" onclick="viewMedia(this)"><b>View
                                        Media</b></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white">Events you have registered for.</h5>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="center">
                @foreach($visitor_events as $visitor_event)
                    @if(isset($visitor_event->visitor_event_registered))
                        @if($visitor_event->visitor_event_registered->registered == true)
                            <div class="col l4">
                                <div class="card white hoverable">
                                    <div class="card-content black-text ">
                                        <h6 class=""> {{$visitor_event->visitor_events_by_event_id->title}}</h6>
                                        <h6 class=""> Date
                                            : {{$visitor_event->visitor_events_by_event_id->start->toDateString()}} </h6>
                                        <h6 class=""> End
                                            : {{$visitor_event->visitor_events_by_event_id->end->toDateString()}} </h6>
                                    </div>
                                    <br>
                                    @if($visitor_event->visitor_event_registered->de_registered == true or $visitor_event->visitor_event_registered->attended == true)
                                        <div class="register-disabled" style=" width:100%;">
                                            <a disabled="" style="color: black;"><b>Deregister</b></a>
                                        </div>
                                    @else
                                        <div class="register" style=" width:100%;">
                                            <a href="#deregister-modal" class="modal-trigger register" data-value="{{$visitor_event->visitor_events_by_event_id->id}}" onclick="storeEventId(this)" style="color: black;"><b>Deregister</b></a>
                                        </div>
                                    @endif

                                    <div class="view_media" style="width:100%;">
                                        <a style="color: black;"
                                           data-value="{{$visitor_event->visitor_events_by_event_id->id}}"
                                           onclick="viewMedia(this)"><b>View
                                                Media</b></a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div id="deregister-modal" class="modal">
        <div class="modal-content">
            <h6>Sorry to see you go :(</h6>

            <input id="deregister-input" type="text" placeholder="What is your reason for deregister?">

        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat" onclick="deregisterVisitor()" id="submit-deregister-info-button">Submit</a>
        </div>
    </div>

    <div id="register-modal" class="modal">
        <div class="modal-content">
            <h5>One more step!</h5>

            <select id="found-us-via-input">
                <option value="" disabled selected>How did you hear about us?</option>
                <option value="Radio">Radio</option>
                <option value="Email Campaign">Email Campaign</option>
                <option value="Facebook">Facebook</option>
                <option value="Website">Website</option>
                <option value="LinkedIn">LinkedIn</option>
                <option value="Twitter">Twitter</option>
                <option value="Print Media">Print Media</option>
                <option value="Other">Other</option>
            </select>
            <input id="found-us-via-other-input" hidden placeholder="Please specify?" type="text">

        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat" onclick="doRegistration()">Submit</a>
        </div>
    </div>


    <style>
        .register {
            color: #fff !important;
            text-transform: uppercase;
            background: lightgray;
            padding: 20px;
        }

        .register-disabled{
            color: #9F9F9F !important;
            text-transform: uppercase;
            background: lightgray;
            padding: 20px;
        }

        .register:hover {
            background: blue;
            letter-spacing: 1px;
            -webkit-box-shadow: 0px 5px 40px -10px rgba(0, 0, 0, 0.57);
            -moz-box-shadow: 0px 5px 40px -10px rgba(0, 0, 0, 0.57);
            box-shadow: 5px 40px -10px rgba(0, 0, 0, 0.57);
            transition: all 0.4s ease 0s;
        }

        .view_media {
            color: #fff !important;
            text-transform: uppercase;
            background: lightgray;
            padding: 20px;
        }

        .view_media:hover {
            background: green;
            letter-spacing: 1px;
            -webkit-box-shadow: 0px 5px 40px -10px rgba(0, 0, 0, 0.57);
            -moz-box-shadow: 0px 5px 40px -10px rgba(0, 0, 0, 0.57);
            box-shadow: 5px 40px -10px rgba(0, 0, 0, 0.57);
            transition: all 0.4s ease 0s;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script>

        $(document).ready(function(){
            $('.modal').modal();

            $('#found-us-via-input').change(function(){
                if($('#found-us-via-input').val() === 'Other'){
                    $('#found-us-via-other-input').show();
                } else {
                    $('#found-us-via-other-input').hide();
                }
            });
        });

        let event_id;

        function storeEventId(obj){
            event_id = obj.getAttribute('data-value');
        }

        function deregisterVisitor(){
            let visitor_id = $('#visitor-id-input').val();
            let formData = new FormData();
            formData.append('visitor_id', visitor_id);
            formData.append('deregister_reason', $('#deregister-input').val());
            formData.append('event_id', event_id);

            if($('#deregister-input').val() === ""){
                alert('Please fill in a reason.');
            } else {
                let url = "{{route('visitor.deregister')}}";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            }
        }

        function storeCurrentEventAndVisitor(obj){
            let event_id = obj.getAttribute('data-event');
            let visitor_id = obj.getAttribute('data-visitor');

            sessionStorage.setItem('current_visitor', visitor_id);
            sessionStorage.setItem('current_event', event_id);
        }

        function doRegistration() {
            let event_id = sessionStorage.getItem('current_event');
            let visitor_id = sessionStorage.getItem('current_visitor');
            let formData = new FormData();

            formData.append('event_id', event_id);
            formData.append('visitor_id', visitor_id);
            if($('#found-us-via-input').val() === 'Other'){
                formData.append('found_out_via', $('#found-us-via-other-input').val());
            } else {
                formData.append('found_out_via', $('#found-us-via-input').val());
            }

            let url = "{{route('visitor.storeEventViaVisitorEventPage')}}";
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    alert(response.message);
                    sessionStorage.setItem('current_visitor', "");
                    sessionStorage.setItem('current_event', "");
                    window.location.reload();
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        }
    </script>

@endsection
