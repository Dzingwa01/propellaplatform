@extends('layouts.incubatee-layout')

@section('content')
    <br>
    <br>
    <br>
    <div class="container" style="margin-right: 4em;margin-left: 4em;">

        <div class="container">
            <div class="">
                <div class="row">
                    @if(isset($event->EventsMedia->media_description))
                        <h6 style="font-size: 1.5em; ">{{$event->media_description}}</h6>
                    @endif
                </div>
                <div class="col s12 m3">
                    @foreach($event->EventsMedia as $event_media)
                        @if(isset($event_media->event_media_url))
                            <div class="card">
                                <div class="card-image">
                                    <img style="width:100%; height:100%;"
                                         src="/storage/{{isset($event_media->event_media_url)?$event_media->event_media_url:''}}">
                                    {{--<span class="card-title">Card Title</span>--}}
                                </div>
                                <div class="card-action">
                                    <a href="/storage/{{isset($event_media->event_media_url)}}"
                                       download="image">Download Image</a>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
