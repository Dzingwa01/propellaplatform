@extends('layouts.incubatee-layout')
@section('content')

    <br>
    <br>
    <br>
    <div class="row">
        <a style="margin-left: 50px;" class="btn-floating btn-large waves-effect waves-light blue"
           href="/full_calendar/view-events"><i class="material-icons">arrow_back</i></a>
    </div>
    <div class="row">
        <div class="col s12">
            <ul class="tabs">

                <li class="tab col s6 active"><a href="#test1">Update Events</a></li>
                <li class="tab col s6"><a class="" href="#test2">Events Media</a></li>
            </ul>
        </div>
        <div class="card hoverable col s12" id="test1"
             style="width: 1500px; margin-left: 90px; /*margin-right: 10em;*/">
            <div class="">
                <div class="container">
                    <br>
                    <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Update
                        Events
                        Details</h6>
                    <br>

                    <form class="col s12" id="Edit-Event" style="margin-top:1em;" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <input type="text" id="event_id" hidden value="{{$event->id}}">
                            <div class="input-field col m6">
                                <input type="number" id="participants" name="participants"
                                       value="{{$event->participants}}"
                                       min="00" max="100">
                                <label for="participants">Participants (00-100)</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="title" name="title" value="{{$event->title}}" type="text" class="validate">
                                <label for="title">Title</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="start" type="date" value="{{$event->start->toDateString()}}"
                                       class="validate">
                                <label for="start">start date</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="end" type="date" value="{{$event->end->toDateString()}}" class="validate">
                                <label for="end">end date</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="all_day" value="{{$event->all_day}}" class="input-select">
                                    <option value="Yes" {{$event->type=='Yes'?'selected':''}}>Yes</option>
                                    <option value="No" {{$event->type=='No'?'selected':''}}>No</option>

                                </select>
                                <label>Occupy all day</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="type">
                                    <option value="Public" {{$event->type=='Public'?'selected':''}}>Public</option>
                                    <option value="Private" {{$event->type=='Private'?'selected':''}}>Private</option>
                                    <option value="Team Only" {{$event->type=='Team Only'?'selected':''}}>Team Only
                                    </option>
                                </select>
                                <label for="type">Public/Private</label>
                            </div>
                        </div>

                        <div class="row col m6" id="select_time" hidden>
                            <div class="input-field col m6">
                                <input type="time" value="{{$event->start_time}}" id="start_time" min="8:00"
                                       max="18:00">
                                <label for="end">Start Time</label>
                            </div>
                            <div class="input-field col m6">
                                <input type="time" value="{{$event->end_time}}" id="end_time" min="8:00" max="18:00">
                                <label for="end">End Time</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m6">
                        <textarea id="description" name="description"
                                  class="materialize-textarea">{{$event->description}}</textarea>
                                <label for="description">Description</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="venue_types">
                                    @if(isset($event->EventVenue->id))
                                        <option value="{{$event->EventVenue->id}}" selected>{{$event->EventVenue->venue_name}}</option>
                                        @foreach($venues as $venue)
                                            <option value="{{$venue->id}}">{{$venue->venue_name}}</option>
                                        @endforeach
                                    @else
                                        @foreach($venues as $venue)
                                            <option value="{{$venue->id}}">{{$venue->venue_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <label for="venue_type">Choose venue</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field  col m6">
                                <div class="btn">
                                    <span>Event Gallery</span>
                                    <input type="file" id="event_media_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" id="event_media_url"
                                           onchange="appendImagePreviews()"
                                           name="event_media_url"
                                           type="text" placeholder="Upload one or more image">
                                </div>
                            </div>
                            <div class="input-field col m6">
                                @if(isset($event->EventsMedia->media_description))
                                    <input id="media_description" name="media_description"
                                           value="{{$event->EventsMedia->media_description}}"
                                           type="text" class="validate">
                                    <label for="media_description">Gallery Description</label>
                                @else
                                    <input id="media_description"
                                           type="text" class="validate">
                                    <label for="media_description">Image Gallery Description</label>
                                @endif
                            </div>
                            <div class="row" id="image-preview">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Event Image</span>
                                        <input id="event_image_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Invite Image</span>
                                        <input id="invite_image_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col m6">
                                <input id="color" value="{{$event->color}}" style="width: 50%" type="color"
                                       class="validate">
                                <label for="color">Choose Colour</label>
                            </div>
                        </div>
                        <div class="input-field col l6 m6 s12">
                            <select id="eventStatus">
                                <option value="Active" {{$event->eventStatus=='Active'?'selected':''}}>Active</option>
                                <option value="Inactive" {{$event->eventStatus=='Inactive'?'selected':''}}>Inactive</option>
                            </select>
                            <label for="eventStatus">Status</label>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col offset-m4">
                                <a href="#!" class="modal-close waves-effect waves-green btn">Cancel<i
                                        class="material-icons right">close</i>
                                </a>
                                <button class="btn waves-effect waves-light" type="submit" style="margin-left:2em;"
                                        id="save-event"
                                        name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>

                        <br>
                        <br>
                    </form>
                </div>
            </div>
        </div>

        <div class="card hoverable col s12" id="test2"
             style="width: 1500px; margin-left: 90px; /*margin-right: 10em;*/">

            <div class="container" style="width: 100%;" id="image_card">
                <div class="row">
                    <h4 id="event-media-header">{{$event->title}}</h4>
                    @if(isset($event->EventsMedia->media_description))
                        <p>{{$event->EventsMedia->media_description}}</p>
                    @endif
                </div>
                <div class="row center" style="width: 100%;">
                    @foreach($event_media_images as $event_media_image)
                        @if(isset($event_media_image->event_media_url))
                            <div class="col s12 m6 l3" style="height: 250px; margin-bottom: 2em;">
                                <div style="border-style: groove; height: 100%; width: 100%; ">
                                    <img style="width:100%; height:100%;" class="materialboxed"
                                         src="/storage/{{isset($event_media_image->event_media_url)?$event_media_image->event_media_url:''}}">
                                    <a style="color: red; cursor: pointer;" id="{{$event_media_image->id}}"
                                       onclick="deleteImage(this)"
                                    ><i class="material-icons">delete_forever</i></a>
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>


        @push('custom-scripts')
            <script>
                let images_array = [];
                let videos_array = [];
                let pdf_array = [];
                let image_array_count = 0;
                let pdf_array_count = 0;
                let video_array_count = 0;
                let image_counter = 0;
                let preview_attr = "";

                $(document).ready(function () {
                    $('select').formSelect();

                    $(".input-select").on("change", checkSelect);

                    function checkSelect() {
                        if ($('#all_day').val() === 'No') {
                            $("#select_time").show();

                        }
                        if ($('#all_day').val() === 'Yes') {
                            $('#start_time').val("08:00") && $('#end_time').val("18:00")
                        }
                    }


                    $('#Edit-Event').on('submit', function (e) {

                        e.preventDefault();
                        let formData = new FormData();

                        formData.append('event_id', $('#event_id').val());
                        formData.append('title', $('#title').val());
                        formData.append('start', $('#start').val());
                        formData.append('end', $('#end').val());
                        formData.append('all_day', $('#all_day').val());
                        formData.append('type', $('#type').val());
                        formData.append('description', $('#description').val());
                        formData.append('color', $('#color').val());
                        formData.append('start_time', $('#start_time').val());
                        formData.append('end_time', $('#end_time').val());
                        formData.append('event_venue_id', $('#event_venue_id').val());
                        formData.append('participants', $('#participants').val());
                        formData.append('venue_types', $('#venue_types').val());
                        formData.append('media_description', $('#media_description').val());
                        formData.append('eventStatus', $('#eventStatus').val());

                        images_array.forEach(function (item, i) {
                            formData.append('image_' + i, item.image);
                            image_array_count += 1;
                        });
                        formData.append('image_array_count', image_array_count);

                        jQuery.each(jQuery('#event_image_url')[0].files, function (i, file) {
                            formData.append('event_image_url', file);
                        });

                        jQuery.each(jQuery('#invite_image_url')[0].files, function (i, file) {
                            formData.append('invite_image_url', file);
                        });

                        let url = '/update-event-edit/' + '{{$event->id}}';
                        $.ajax({
                            url: url,
                            processData: false,
                            contentType: false,
                            data: formData,
                            type: 'post',
                            headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                            success: function (response, a, b) {
                                $('#save-event').notify(response.message, "success");
                                // setTimeout(function () {
                                //     window.location.reload();
                                // }, 3000);
                            },

                            error: function (response) {
                                console.log("error", response);
                                let message = response.responseJSON.message;
                                alert(message);
                            }
                        });
                    });
                });

                function deleteImage(obj) {
                    var r = confirm("Are you sure want to delete this image?");
                    if (r) {
                        $.get('/delete-events-images/' + obj.id, function (data, status) {
                            console.log('Data', data);
                            console.log('Status', status);
                            if (status == 'success') {
                                $('#event-media-header').notify(data.message, "success");

                                setTimeout(function(){
                                    window.location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        alert('Delete action cancelled');
                    }
                }

                function appendImagePreviews() {
                    $("#message").empty(); // To remove the previous error message
                    //Getting the selected file. Only one is being selected at a time in this case. You can do multiple, but its ugly
                    let fileType = jQuery('#event_media_url')[0].files[0];
                    let imagefile = fileType.type;



                    jQuery.each(jQuery('#event_media_url')[0].files, function (i, image) {
                        image_counter++;
                        //Variables for dynamic adding and removal of cards with images
                        let id = "image_" + image_counter;
                        let card_id = "card_" + image_counter;
                        let card_action = "action_" + image_counter;
                        preview_attr = id;
                        let img = new Image;
                        img.src = URL.createObjectURL(image);


                        //Appending a card to the image preview area
                        $("#image-preview").append(' <div class="col s12 m6">\n' +
                            '      <div class="card" id=' + card_id + ' value=' + image.name + '>\n' +
                            '        <div class="card-image">\n' +
                            '          <img src=' + img.src +'> ' +
                            '        </div>\n' +
                            '        <div class="card-action">\n' +
                            '          <a href="#" id=' + card_action + ' onclick="removePreview(' + card_id + ')">Delete</a>\n' +
                            '        </div>\n' +
                            '      </div>');

                        let object = {
                            card_id: card_id,
                            image: image

                        };
                        images_array.push(object);
                    });
                }

                function removePreview(card_id) {
                    $("#" + card_id.id).remove();

                    for(let i in images_array){
                        if(card_id.id === images_array[i].card_id){
                            images_array.splice(i, 1);
                        }
                    }
                }

            </script>
    @endpush
@endsection
