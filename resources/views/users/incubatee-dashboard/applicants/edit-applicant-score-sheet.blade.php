@extends('layouts.incubatee-layout')

@section('content')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>

    <input id="applicant-panelist-id-input" value="{{$applicantPanelist->id}}" hidden disabled>

    <div class="section" style="margin-top: 2em;">
        <div class="card white" style="width: 1300px;margin: 0 auto">

            <div class="row center">
                <h5> Here you can update your score / comments if you wish to </h5>
            </div>

            <div class="row" style="margin-left: 4em; margin-right: 4em;">
                <ul>
                    @foreach ($question_answers_array as $question_answer)
                            <li>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </li>
                            <div class="input-field panel-question-answers-comment">
                                <input id="{{$question_answer->question_id}}" class="materialize-textarea answer-text-desktop"
                                       style="width:10%" type="number" name="quantity" min="1" max="5" value="{{$question_answer->score}}">
                                <label for="answer-text-desktop">Score</label>

                                <textarea class="comments">{{$question_answer->comment}}</textarea>
                            </div>
                    @endforeach
                </ul>
            </div>

            <div class="row" style="margin-left: 1100px">
                <div class="col s12">
                    <button class="btn waves-effect waves-light" id="update-answers-form-desktop"> Update <i
                            class="material-icons right">send</i></button>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>


    <script>

        $(document).ready(function () {
            //Desktop Form
            $('#update-answers-form-desktop').on('click', function () {
                let answersCommentsArray = [];
                let applicant_panelist_id = $('#applicant-panelist-id-input').val();

                $('.panel-question-answers-comment').each(function (e) {
                    let answer = this.getElementsByClassName('answer-text-desktop');
                    let comment = this.getElementsByClassName('comments');
                    let question_id = answer[0].id;

                    let model = {
                        question_id: question_id,
                        applicant_panelist_id: applicant_panelist_id,
                        answer_score: answer[0].value,
                        comment: comment[0].value
                    };

                    answersCommentsArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers_comments', JSON.stringify(answersCommentsArray));

                //To push the answersArray as data to the controller and save all the answers to the db
                $.ajax({
                    url: "/panelist-update-question-answer-comments",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},
                    success: function (response, a, b) {
                        $('#update-answers-form-desktop').notify(response.message, "success");
                    },

                    error: function (response) {
                        $('#update-answers-form-desktop').notify(response.message, "error");
                    }
                });
            });
        });
    </script>


@endsection
