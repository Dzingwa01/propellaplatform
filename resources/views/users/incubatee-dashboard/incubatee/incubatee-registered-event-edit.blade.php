@extends('layouts.incubatee-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <input id="eventincubatee-id-input" disabled hidden value="{{$eventIncubatee->id}}">
    <input id="incubatee-id-input" disabled hidden value="{{$incubatee->incubatee_id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('inc-edit-incubatee-event',$incubatee->incubatee_id,$eventIncubatee)}}

    <div class="card" style="margin-top: 10vh;width: 800px;margin-left: 400px">
        <br>
        <form class="col s12" id="Edit-event-incubatee" enctype="multipart/form-data">
            <input id="event_incubatee_id" value="{{$eventIncubatee->id}}" hidden>
            <div class="">
                <h5 class="center-align">Edit Incubatee Event</h5>

                <div class="row" style="margin-left:2em;margin-right: 2em">
                    <div class="input-field col s6">
                        <input id="title" type="text" value="{{$eventIncubatee->events->title}}" class="validate"
                               disabled>
                        <label for="title">Event Tittle</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="start" type="text" value="{{$eventIncubatee->events->start->toDateString()}}"
                               class="validate" disabled>
                        <label for="start">Event Date Registered</label>
                    </div>
                </div>

                <div class="row" style="margin-left: 2em;margin-right: 2em">
                    <div class="input-field col s6">
                        <select id="attended">
                            <option value="1" {{$eventIncubatee->attended=='1'?'selected':''}}>Yes</option>
                            <option value="0" {{$eventIncubatee->attended=='0'?'selected':''}}>No</option>
                        </select>
                        <label for="type">Attended</label>
                    </div>
                    <div class="input-field col s6">
                        <select id="registered">
                            <option value="1" {{$eventIncubatee->registered=='1'?'selected':''}}>Yes</option>
                            <option value="0" {{$eventIncubatee->registered=='0'?'selected':''}}>No</option>
                        </select>
                        <label for="type">Registered</label>
                    </div>
                </div>
                <div class="container" hidden id="deregister-container">
                    <div class="row">
                        <div class="col l6 m6">
                            <label for="date_time_de_register">Date / Time De-registered</label>
                            <input id="date_time_de_register" type="date">
                        </div>
                        <div class="col l6 m6">
                            <label for="de_register_reason">De-registered Reason</label>
                            <textarea id="de_register_reason"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <button class="btn waves-effect waves-light" style="margin-left:650px;" id="save-event-bootcamper"
                    name="action">Submit
                <i class="material-icons right">send</i>
            </button>
        </form>

        <br>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
            $('#attended').on('change', function(){
                if($('#attended').val() === "0"){
                    $('#deregister-container').show();
                }else {
                    $('#deregister-container').hide();
                }
            });
            $('#Edit-event-incubatee').on('submit', function (e) {

                e.preventDefault();
                let formData = new FormData();
                if ($('#registered').val() == '1') {
                    formData.append('attended', $('#attended').val());
                    formData.append('registered', $('#registered').val());
                    formData.append('date_time_de_register', $('#date_time_de_register').val());
                    formData.append('de_register_reason', $('#de_register_reason').val());


                    console.log(formData);

                    let url = '/updateIncubateeEvent/' + '{{$eventIncubatee->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            alert(response.message);
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                }
            });

        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
