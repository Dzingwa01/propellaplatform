@extends('layouts.incubatee-layout')

@section('content')

    <div class="card" style="width: 1000px;margin: 0 auto;top: 10vh">
        <br>
        <div class="row" style="margin-left: 6em; margin-right: 6em;">
            <input hidden disabled id="venture-id-input" value="{{$venture->id}}">
            <input hidden disabled id="mentor-venture-shadowboard-id" value="{{$mentorVentureShadowboard->id}}">

            <p style="font-size: 2em"><b>Venture Evaluation of Mentor</b></p>
            <p><b>Venture</b> : {{$venture->company_name}} </p>
            <p><b>Mentor</b> : {{$mentor->user->name}} </p>
            <p><b>Date</b> : {{$mentorVentureShadowboard->shadow_board_date}} </p>
            <br>
            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-5), with one being bad, five being perfect.</b></p>

            @if(count($ventureQuestionsAnswers) > 0)
                <ol>
                    @foreach ($ventureQuestionsAnswers as $vqa)
                        <li value="{{$vqa->question_number}}">  {{$vqa->question_text}} </li>
                        @if($vqa->question_type == "1_5")
                            <div class="input-field">
                                <input type="number" min="0" max="5" id="{{$vqa->id}}"
                                       class="materialize-textarea answer-text-desktop"
                                       style="width:20%" value="{{$vqa->answer_text}}">
                            </div>

                        @elseif($vqa->question_type == "Multiple")
                            @if(count($vqa->question_sub_texts) > 0)
                                <input id="{{$vqa->id}}" type="text" value="{{$vqa->answer_text}}" disabled style="width:80%">
                                @foreach($vqa->question_sub_texts as $sub)
                                    <p class="testing-checkbox">
                                        <label>
                                            <input id="{{$vqa->id}}" value="{{$sub->question_sub_text}}" type="checkbox"/>
                                            <span>{{$sub->question_sub_text}}</span>
                                        </label>
                                    </p>
                                @endforeach
                            @endif

                        @elseif($vqa->question_type == "Yes_No")
                            <div class="row">
                                <div class="input-field col l3 s3">
                                    <select id="{{$vqa->id}}" class="answer-text-desktop">
                                        <option value="{{$vqa->answer_text}}" selected>{{$vqa->answer_text}}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                        @elseif($vqa->question_type == "text")
                            <div class="input-field ">
                                <input id="{{$vqa->id}}" type="text" value="{{$vqa->answer_text}}" class="validate answer-text-desktop" style="width:80%">
                            </div>
                        @elseif($vqa->question_type == "feedback_text")
                            <div class="input-field ">
                                <input id="{{$vqa->id}}" value="{{$vqa->answer_text}}" type="text" class="validate answer-text-desktop" style="width:100%">
                            </div>
                        @endif
                    @endforeach
                </ol>
                <br>
                <div class="col l6 m5 s12">
                    <button class="btn waves-effect waves-light" id="answers-update"> Save <i
                            class="material-icons right">send</i></button>
                </div>

                @else
                <ol>
                    @foreach ($questions as $question)
                        <li value="{{$question->question_number}}">  {{ $question->question_text}} </li>
                        @if($question->question_type == "1_5")
                            <div class="input-field">
                                <input type="number" min="0" max="5" id="{{$question->id}}"
                                       class="materialize-textarea answer-text-desktop"
                                       style="width:20%">
                            </div>
                        @elseif($question->question_type == "Multiple")
                            @foreach($question->shadowBoardQuestionSubQuestionText as $sub)
                                <p class="testing-checkbox">
                                    <label>
                                        <input id="{{$question->id}}" value="{{$sub->question_sub_text}}" type="checkbox"/>
                                        <span>{{$sub->question_sub_text}}</span>
                                    </label>
                                </p>
                            @endforeach

                        @elseif($question->question_type == "Yes_No")
                            <div class="row">
                                <div class="input-field col l3 s3">
                                    <select id="{{$question->id}}" class="answer-text-desktop">
                                        <option value="" disabled selected>Yes / No</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                        @elseif($question->question_type == "text")
                            <div class="input-field ">
                                <input id="{{$question->id}}" type="text" class="validate answer-text-desktop" style="width:100%">
                            </div>
                        @elseif($question->question_type == "feedback_text")

                            <div class="input-field ">
                                <input id="{{$question->id}}" type="text" class="validate answer-text-desktop" style="width:100%">
                            </div>

                        @endif
                    @endforeach

                </ol>
                <br>

                <div class="col l6 m5 s12">
                    <button class="btn waves-effect waves-light" id="answers-store"> Save <i
                            class="material-icons right">send</i></button>
                </div>
            @endif
        </div>
        <br>
    </div>
    <br>
    <br>

    <script>

        $(document).ready(function () {
            $('.tooltipped').tooltip();
            $('select').formSelect();

            $('#answers-store').on('click', function () {

                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#venture-id-input').val();
                let mentorVentureShadowboard = $('#mentor-venture-shadowboard-id').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        shadow_board_question_id: this.id,
                        venture_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                $(".testing-checkbox input[type=checkbox]:checked").each(function () {
                    let model = {
                        shadow_board_question_id: this.id,
                        venture_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));
                formData.append('mentorVentureShadowboard', mentorVentureShadowboard);

                $.ajax({
                    url: "/storeVentureShadowboardAnswers",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        $('#answers-store').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '';
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            $('#answers-update').on('click', function () {
                $(this).text("please wait...");

                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#venture-id-input').val();
                let mentorVentureShadowboard = $('#mentor-venture-shadowboard-id').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        shadow_board_question_id: this.id,
                        venture_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                $(".testing-checkbox input[type=checkbox]:checked").each(function () {
                    let model = {
                        shadow_board_question_id: this.id,
                        venture_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));
                formData.append('mentorVentureShadowboard', mentorVentureShadowboard);

                $.ajax({
                    url: "/storeVentureShadowboardAnswers",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        $('#answers-store').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '';
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>
    @endsection
