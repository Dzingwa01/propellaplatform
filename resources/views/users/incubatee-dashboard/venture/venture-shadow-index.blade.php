@extends('layouts.incubatee-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('inc-shadowboard',$venture)}}
    <div class="container-fluid">
        <input hidden disabled value="{{$venture->id}}" id="venture-id-input">
    </div>
    <br>

    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">{{$venture->company_name}} Shadow board meeting dates</h6>
        </div>
        <br>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="venture-table">
                    <thead>
                    <tr>
                        <th>Mentor</th>
                        <th>Shadow board date</th>
                        <th>Shadow board time</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>




    <script>

        $(document).ready(function () {
            $('select').formSelect();
            $(function () {
                let venture_id = $('#venture-id-input').val();
                let url = '/get-venture-shadowboards/' + venture_id;
                $('#venture-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    type: 'get',
                    scrollX: 640,
                    ajax: url,
                    columns: [
                        {data: 'mentor', name: 'mentor'},
                        {data: 'shadow_board_date', name: 'shadow_board_date'},
                        {data: 'shadow_board_time', name: 'shadow_board_time'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},

                    ]
                });
                $('select[name="venture-table_length"]').css("display", "inline");
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    @endsection
