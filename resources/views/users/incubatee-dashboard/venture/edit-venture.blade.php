@extends('layouts.incubatee-layout')
<link rel="stylesheet" href="/css/platform_styles.css"/>
@section('content')
    <br>
    <br>
    <head>
        <!-- jQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    </head>
    {{ Breadcrumbs::render('inc-venture',$venture)}}
    <div class="container" style="width: auto">
        <br>
        <br>
        <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Update
            Venture</h6>
        <br>
        <div class="row card hoverable">
            <div class="col s12">
                <ul class="tabs">
                    <li class="tab col s4"><a href="#test1">Venture Details</a></li>
                    <li class="tab col s4"><a href="#test2">Media</a></li>
                    <li class="tab col s4"><a href="#test3">M + E</a></li>
                </ul>
            </div>
            <div id="test1" class="card col s12">
                <br>
                <br>
                <form id="start_details_update_form" class="col s12"
                      style="margin-top:1em; margin-right: 2em;">
                    <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">
                        Update Venture</h6>

                    @csrf

                    <input value="{{$venture->id}}" id="venture_id" hidden>
                    <h6 style="padding-left: 50px;"><b>START UP DETAILS</b></h6>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="company_name" value="{{$venture->company_name}}" type="text"
                                   class="validate">
                            <label for="company_name">Company Name</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="contact_number" value="{{$venture->contact_number}}" type="text"
                                   class="validate">
                            <label for="contact_number">Contact Number</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            @if(isset($venture->ventureOwnership->ownership))
                                <input id="ownership"
                                       value="{{$venture->ventureOwnership->ownership}}"
                                       type="text" class="validate">
                                <label for="ownership">Ownership (fill in percentage)</label>
                            @else
                                <input id="ownership"
                                       type="text" class="validate">
                                <label for="ownership">Ownership (fill in percentage)</label>
                            @endif

                        </div>
                        <div class="input-field col m6">
                            @if(isset($venture->ventureOwnership->registration_number))
                                <input id="registration_number"
                                       value="{{$venture->ventureOwnership->registration_number}}"
                                       type="text" class="validate">
                                <label for="registration_number">Registration Number</label>
                            @else
                                <input id="registration_number"
                                       type="text" class="validate">
                                <label for="registration_number">Registration Number</label>
                            @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <select id="BBBEE_Level">
                                <option
                                    value="1" {{isset($venture->ventureOwnership->BBBEE_level)=='1'?$venture->ventureOwnership->BBBEE_level:''}}>Level 01
                                </option>
                                <option
                                    value="2" {{isset($venture->ventureOwnership->BBBEE_level)=='2'?$venture->ventureOwnership->BBBEE_level:''}}>Level 02
                                </option>
                                <option
                                    value="3" {{isset($venture->ventureOwnership->BBBEE_level)=='3'?$venture->ventureOwnership->BBBEE_level:''}}>Level 03
                                </option>
                                <option
                                    value="4" {{isset($venture->ventureOwnership->BBBEE_level)=='4'?$venture->ventureOwnership->BBBEE_level:''}}>Level 04
                                </option>
                                <option
                                    value="5" {{isset($venture->ventureOwnership->BBBEE_level)=='5'?$venture->ventureOwnership->BBBEE_level:''}}>Level 05
                                </option>
                                <option
                                    value="6" {{isset($venture->ventureOwnership->BBBEE_level)=='6'?$venture->ventureOwnership->BBBEE_level:''}}>Level 06
                                </option>
                                <option
                                    value="7" {{isset($venture->ventureOwnership->BBBEE_level)=='7'?$venture->ventureOwnership->BBBEE_level:''}}>Level 07
                                </option>
                            </select>
                            <label for="BBBEE_Level">BBBEE Level</label>
                        </div>
                        <div class="input-field col m6">
                            <select id="business_type">
                                <option
                                    value="Agriculture" {{$venture->business_type=='Agriculture'?'selected':''}}>Agriculture; plantations; rural sectors
                                </option>
                                <option
                                    value="Basic Metal Product" {{$venture->business_type=='Basic Metal Product'?'selected':''}}>Basic Metal Product
                                </option>
                                <option
                                    value="Chemical industries" {{$venture->business_type=='Chemical industries'?'selected':''}}>Chemical industries
                                </option>
                                <option value="Commerce" {{$venture->business_type=='Commerce'?'selected':''}}>Commerce
                                </option>
                                <option
                                    value="Construction" {{$venture->business_type=='Construction'?'selected':''}}>Construction
                                </option>
                                <option value="Education"{{$venture->business_type=='Education'?'selected':''}}>Education
                                </option>
                                <option
                                    value="Financial service"{{$venture->business_type=='Financial service'?'selected':''}}>Financial and professional services
                                </option>
                                <option value="Food" {{$venture->business_type=='Food'?'selected':''}}>Food; drink; tobacco
                                </option>
                                <option value="Forestry" {{$venture->business_type=='Forestry'?'selected':''}}>Forestry; wood; pulp and paper
                                </option>
                                <option
                                    value="Health service" {{$venture->business_type=='Health service'?'selected':''}}>Health service
                                </option>
                                <option value="Touring" {{$venture->business_type=='Touring'?'selected':''}}>Tourism; (hotels; catering)
                                </option>
                                <option value="Mining" {{$venture->business_type=='Mining'?'selected':''}}>Mining (coal; other mining)
                                </option>
                                <option
                                    value="Mechanical and electrical engineering"{{$venture->business_type=='Mechanical and electrical engineering'?'selected':''}}>Mechanical and electrical engineering
                                </option>
                                <option
                                    value="Oil and gas production" {{$venture->business_type=='Oil and gas production'?'selected':''}}>Oil and gas production
                                </option>
                                <option
                                    value="Postal and telecommunications services" {{$venture->business_type=='Postal and telecommunications services'?'selected':''}}>Postal and telecommunications services
                                </option>
                                <option
                                    value="Public service" {{$venture->business_type=='Public service'?'selected':''}}>Public service
                                </option>
                                <option value="Fishing" {{$venture->business_type=='Fishing'?'selected':''}}>Fishing (ports; fisheries; inland waterways)
                                </option>
                                <option value="Transport" {{$venture->business_type=='Transport'?'selected':''}}>Transport (including civil aviation; railways; road transport)
                                </option>
                                <option value="Clothing" {{$venture->business_type=='Clothing'?'selected':''}}>Clothing (Textiles; clothing; footwear)
                                </option>
                                <option value="Utilities" {{$venture->business_type=='Utilities'?'selected':''}}>Utilities (waters; gas; electricity)
                                </option>
                            </select>
                            <label>Business-Type</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <p>{{$venture->smart_city_tags}}</p>
                            <select id="smart_city_tags" multiple>
                                <option value="" disabled selected>Choose more tags
                                </option>
                                <option
                                    value="Smart Business">Smart Business
                                </option>
                                <option
                                    value="Smart Community">Smart Community
                                </option>
                                <option
                                    value="Smart Healthcare">Smart Healthcare
                                </option>
                                <option
                                    value="Smart Living">Smart Living
                                </option>
                                <option
                                    value="Smart Mobility">Smart Mobility
                                </option>
                                <option
                                    value="Smart Building">Smart Building
                                </option>
                                <option
                                    value="Smart Infrastructure">Smart Infrastructure
                                </option>
                                <option
                                    value="Smart Governance">Smart Governance
                                </option>
                                <option
                                    value="Smart Environment">Smart Environment
                                </option>
                                <option
                                    value="Smart Agriculture">Smart Agriculture
                                </option>
                            </select>
                            <label>Smart City Tags
                            </label>
                        </div>
                        <div class="input-field col m6">
                            <p>{{$venture->sustainable_development_goals}}</p>
                            <select id="sustainable_development_goals" multiple>
                                <option value="" disabled selected>
                                    Choose more sustainable development goals tags
                                </option>
                                <option
                                    value="No poverty">
                                    No poverty
                                </option>
                                <option
                                    value="Zero hunger">
                                    Zero hunger
                                </option>
                                <option
                                    value="Good Health and well-being">
                                    Good Health and well-being
                                </option>
                                <option
                                    value="Quality education">
                                    Quality education
                                </option>
                                <option
                                    value="Gender quality">
                                    Gender quality
                                </option>
                                <option
                                    value=" Clean water & sanitation">
                                    Clean water & sanitation
                                </option>
                                <option
                                    value="Affordable & clean energy">
                                    Affordable & clean energy
                                </option>
                                <option
                                    value="Decent work & economic growth">
                                    Decent work & economic growth
                                </option>
                                <option
                                    value="Industry , innovation & infastructure">
                                    Industry , innovation & infastructure
                                </option>
                                <option
                                    value="Reduced inequalities">
                                    Reduced inequalities
                                </option>
                                <option
                                    value="Sustainable cities and communities">
                                    Sustainable cities and communities
                                </option>
                                <option
                                    value="Responsible consumption and production">
                                    Responsible consumption and production
                                </option>
                                <option
                                    value="Climate action">
                                    Climate action
                                </option>
                                <option
                                    value="Life below water">
                                    Life below water
                                </option>
                                <option
                                    value="Life on land">
                                    Life on land
                                </option>
                                <option
                                    value="Promote just, peaceful and inclusive societies">
                                    Promote just, peaceful and inclusive societies
                                </option>
                                <option
                                    value="Patnership for the goals">
                                    Patnership for the goals
                                </option>
                            </select>
                            <label>Sustainable Development Goals Tags
                            </label>
                        </div>
                    </div>
                    <div class="row">

                    </div>
                    <br>
                    <h6 style="padding-left: 50px;"><b>ADDRESS DETAILS</b></h6>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="physical_address_one" value="{{$venture->physical_address_one}}" type="text"
                                   class="validate">
                            <label for="physical_address_one">Physical Address 1</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="physical_address_two" value="{{$venture->physical_address_two}}" type="text"
                                   class="validate">
                            <label for="physical_address_two">Physical Address 2</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="physical_address_three" value="{{$venture->physical_address_three}}"
                                   type="text" class="validate">
                            <label for="physical_address_three">Physical Address 3</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="physical_city" value="{{$venture->physical_city}}" type="text"
                                   class="validate">
                            <label for="physical_city">City</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="physical_postal_code" value="{{$venture->physical_postal_code}}" type="text"
                                   class="validate">
                            <label for="physical_code">Code</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="postal_address_one" value="{{$venture->postal_address_one}}" type="text"
                                   class="validate"
                                   placeholder="Postal Address 1">
                            <label for="postal_address_1">Postal Address 1</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="postal_address_two" value="{{$venture->postal_address_two}}" type="text"
                                   class="validate"
                                   placeholder="Postal Address 2">
                            <label for="postal_address_2">Postal Address 2</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="postal_address_three" value="{{$venture->postal_address_three}}" type="text"
                                   class="validate"
                                   placeholder="Postal Address 3">
                            <label for="postal_address_3">Postal Address 3</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="postal_city" value="{{$venture->postal_city}}" type="text" class="validate"
                                   placeholder="City">
                            <label for="postal_city">City</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="postal_code" value="{{$venture->postal_code}}" type="text" class="validate"
                                   placeholder="postal code">
                            <label for="postal_code">Code</label>
                        </div>

                    </div>
                    <br>
                    <br>
                    <div class="onboarding-row" hidden>
                    <h6 style="padding-left: 50px;"><b>ONBOARDING INFO</b></h6>

                    <div class="row">
                        <div class="input-field col m6">
                            <select id="hub">
                                <option value="ICT" {{$venture->hub=='ICT'?'selected':''}}>ICT</option>
                                <option value="Industrial"{{$venture->hub=='Industrial'?'selected':''}}>Industrial
                                </option>
                                <option value="Creative" {{$venture->hub=='Creative'?'selected':''}}>Creative</option>
                            </select>
                            <label>Hub</label>
                        </div>
                        <div class="input-field col m6">
                            <select id="stage">
                                <option value="" disabled selected>Choose Stage</option>
                                <option value="Stage 1"{{$venture->stage=='Stage 1'?'selected':''}}>Stage 1</option>
                                <option value="Stage 2"{{$venture->stage=='Stage 2'?'selected':''}}>Stage 2</option>
                                <option value="Stage 3"{{$venture->stage=='Stage 3'?'selected':''}}>Stage 3</option>
                                <option value="Stage 4"{{$venture->stage=='Stage 4'?'selected':''}}>Stage 4</option>
                                <option value="Stage 5"{{$venture->stage=='Stage 5'?'selected':''}}>Stage 5</option>
                                <option value="Stage 6"{{$venture->stage=='Stage 6'?'selected':''}}>Stage 6</option>
                                <option value="Alumni"{{$venture->stage=='Alumni'?'selected':''}}>Alumni</option>
                            </select>
                            <label>Stage</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <p>{{$venture->sustainable_development_goals}}</p>
                            <select id="sustainable_development_goals" multiple>
                                <option value="" disabled selected>
                                    Choose more sustainable development goals tags
                                </option>
                                <option
                                    value="No poverty">
                                    No poverty
                                </option>
                                <option
                                    value="Zero hunger">
                                    Zero hunger
                                </option>
                                <option
                                    value="Good Health and well-being">
                                    Good Health and well-being
                                </option>
                                <option
                                    value="Quality education">
                                    Quality education
                                </option>
                                <option
                                    value="Gender quality">
                                    Gender quality
                                </option>
                                <option
                                    value=" Clean water & sanitation">
                                    Clean water & sanitation
                                </option>
                                <option
                                    value="Affordable & clean energy">
                                    Affordable & clean energy
                                </option>
                                <option
                                    value="Decent work & economic growth">
                                    Decent work & economic growth
                                </option>
                                <option
                                    value="Industry , innovation & infastructure">
                                    Industry , innovation & infastructure
                                </option>
                                <option
                                    value="Reduced inequalities">
                                    Reduced inequalities
                                </option>
                                <option
                                    value="Sustainable cities and communities">
                                    Sustainable cities and communities
                                </option>
                                <option
                                    value="Responsible consumption and production">
                                    Responsible consumption and production
                                </option>
                                <option
                                    value="Climate action">
                                    Climate action
                                </option>
                                <option
                                    value="Life below water">
                                    Life below water
                                </option>
                                <option
                                    value="Life on land">
                                    Life on land
                                </option>
                                <option
                                    value="Promote just, peaceful and inclusive societies">
                                    Promote just, peaceful and inclusive societies
                                </option>
                                <option
                                    value="Patnership for the goals">
                                    Patnership for the goals
                                </option>
                            </select>
                            <label>Sustainable Development Goals Tags
                            </label>
                        </div>
                        <div class="input-field col m6">
                            <select id="business_type">
                                <option
                                    value="Agriculture" {{$venture->business_type=='Agriculture'?'selected':''}}>Agriculture; plantations; rural sectors
                                </option>
                                <option
                                    value="Basic Metal Product" {{$venture->business_type=='Basic Metal Product'?'selected':''}}>Basic Metal Product
                                </option>
                                <option
                                    value="Chemical industries" {{$venture->business_type=='Chemical industries'?'selected':''}}>Chemical industries
                                </option>
                                <option value="Commerce" {{$venture->business_type=='Commerce'?'selected':''}}>Commerce
                                </option>
                                <option
                                    value="Construction" {{$venture->business_type=='Construction'?'selected':''}}>Construction
                                </option>
                                <option value="Education"{{$venture->business_type=='Education'?'selected':''}}>Education
                                </option>
                                <option
                                    value="Financial service"{{$venture->business_type=='Financial service'?'selected':''}}>Financial and professional services
                                </option>
                                <option value="Food" {{$venture->business_type=='Food'?'selected':''}}>Food; drink; tobacco
                                </option>
                                <option value="Forestry" {{$venture->business_type=='Forestry'?'selected':''}}>Forestry; wood; pulp and paper
                                </option>
                                <option
                                    value="Health service" {{$venture->business_type=='Health service'?'selected':''}}>Health service
                                </option>
                                <option value="Touring" {{$venture->business_type=='Touring'?'selected':''}}>Tourism; (hotels; catering)
                                </option>
                                <option value="Mining" {{$venture->business_type=='Mining'?'selected':''}}>Mining (coal; other mining)
                                </option>
                                <option
                                    value="Mechanical and electrical engineering"{{$venture->business_type=='Mechanical and electrical engineering'?'selected':''}}>Mechanical and electrical engineering
                                </option>
                                <option
                                    value="Oil and gas production" {{$venture->business_type=='Oil and gas production'?'selected':''}}>Oil and gas production
                                </option>
                                <option
                                    value="Postal and telecommunications services" {{$venture->business_type=='Postal and telecommunications services'?'selected':''}}>Postal and telecommunications services
                                </option>
                                <option
                                    value="Public service" {{$venture->business_type=='Public service'?'selected':''}}>Public service
                                </option>
                                <option value="Fishing" {{$venture->business_type=='Fishing'?'selected':''}}>Fishing (ports; fisheries; inland waterways)
                                </option>
                                <option value="Transport" {{$venture->business_type=='Transport'?'selected':''}}>Transport (including civil aviation; railways; road transport)
                                </option>
                                <option value="Clothing" {{$venture->business_type=='Clothing'?'selected':''}}>Clothing (Textiles; clothing; footwear)
                                </option>
                                <option value="Utilities" {{$venture->business_type=='Utilities'?'selected':''}}>Utilities (waters; gas; electricity)
                                </option>
                            </select>
                            <label>Business-Type</label>
                        </div>
                        <div class="input-field col m6">
                            <p>{{$venture->smart_city_tags}}</p>
                            <select id="smart_city_tags" multiple>
                                <option value="" disabled selected>Choose more tags
                                </option>
                                <option
                                    value="Smart Business">Smart Business
                                </option>
                                <option
                                    value="Smart Community">Smart Community
                                </option>
                                <option
                                    value="Smart Healthcare">Smart Healthcare
                                </option>
                                <option
                                    value="Smart Living">Smart Living
                                </option>
                                <option
                                    value="Smart Mobility">Smart Mobility
                                </option>
                                <option
                                    value="Smart Building">Smart Building
                                </option>
                                <option
                                    value="Smart Infrastructure">Smart Infrastructure
                                </option>
                                <option
                                    value="Smart Governance">Smart Governance
                                </option>
                                <option
                                    value="Smart Environment">Smart Environment
                                </option>
                                <option
                                    value="Smart Agriculture">Smart Agriculture
                                </option>
                            </select>
                            <label>Smart City Tags
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <select id="virtual_or_physical">
                                <option value="Physical" {{$venture->virtual_or_physical=='Physical'?'selected':''}}>Physical
                                </option>
                                <option value="Virtual" {{$venture->virtual_or_physical=='Virtual'?'selected':''}}>Virtual
                                </option>
                            </select>
                            <label>Business-Type</label>
                        </div>

                        <div class="input-field col m6">
                            <label for="advisor">Advisor Name</label>
                            <input id="advisor" value="{{$venture->advisor}}" type="text" class="validate">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <label for="mentor">Mentor Name</label>
                            <input id="mentor" value="{{$venture->mentor}}" type="text" class="validate">
                        </div>
                        <div class="input-field col m6">
                            <select id="status" class="input-select">
                                <option value="Active"{{$venture->status=='Active'?'selected':''}}>Active</option>
                                <option value="Alumni{{$venture->status=='Alumni'?'selected':''}}">Alumni</option>
                                <option value="Exit"{{$venture->status=='Exit'?'selected':''}}>Exit</option>
                                <option value="Resigned"{{$venture->status=='Resigned'?'selected':''}}>Resigned
                                </option>
                            </select>
                            <label>Status</label>
                        </div>
                    </div>


                    <!--All these must be hidden-->
                    <div class="row" id="exitDate">
                        <div class="input-field col m6">
                            <input id="exit_date" value="{{$venture->exit_date}}"
                                   type="date">
                            <label for="exit_date">Exit Date</label>
                        </div>
                    </div>
                    <div class="row" id="resignDate">
                        <div class="input-field col m6">
                            <input id="resigned_date" value="{{$venture->resigned_date}}"
                                   type="date">
                            <label for="resigned_date">Resigned Date</label>
                        </div>
                    </div>
                    <div class="row" id="reasonForLeaving">
                        <div class="input-field col m6">
                            <label for="reason_for_leaving">Reason for leaving</label>
                            <input id="reason_for_leaving" value="{{$venture->reason_for_leaving}}" type="text"
                                   class="validate">
                        </div>
                    </div>
                    <div class="row" id="alumniDate">
                        <div class="input-field col m6">
                            <input id="alumni_date" value="{{$venture->alumni_date}}"
                                   type="date">
                            <label for="alumni_date">Alumni Date</label>
                        </div>
                    </div>
                    </div>
                    <!---->

                    <br>
                    <div class="row center">
                        <h6><b>GOVERNANCE</b></h6>
                    </div>

                    <div class="row">
                        <div class="input-field col l6">
                            <div class="file-path-wrapper">
                                <span>VAT registration number</span>
                                <input class="validate"
                                       value="{{isset($venture_upload->vat_registration_number_url)?$venture_upload->vat_registration_number_url:''}}"
                                       type="text"
                                       id="vat_registration_number_url">
                            </div>
                        </div>
                    </div>

                    <div class="row center">
                        <h6> Upload a client contract </h6>
                        <div class="file-field input-field col l4">
                            <div class="btn">
                                <span>Client Contract </span>
                                <input type="file" id="client_contract_url" multiple>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate"
                                       type="text" placeholder="Upload in PDF">
                            </div>
                        </div>
                        <div class="input-field col l4">
                            <input type="date" id="date_of_signature"
                                   value="{{$venture_upload->date_of_signature}}">
                            <label for="date_of_signature">Date of Signature (yyyy-mm-dd)</label>
                        </div>
                        <div class="col l4 input-field">
                            <button class="btn blue" id="client_contract_upload_btn">Upload</button>
                        </div>
                    </div>

                    {{--Set Client Contracts--}}
                    @if(isset($venture_upload->venture_client_contract))
                        <div class="row center" style="border: 1px solid black;">
                            <h6>Venture Client Contracts</h6>
                            <br>
                            @foreach($venture_upload->venture_client_contract as $client_contract)
                                <div class="row">
                                    <div class="col l4">
                                        <input class="file-path validate" disabled value="{{$client_contract->client_contract_url}}"
                                               type="text" placeholder="Upload in PDF">
                                    </div>
                                    <div class="col l4">
                                        <input type="text" disabled id="date_of_signature_display" value="{{$client_contract->date_of_signature}}">
                                        <label for="date_of_signature_display">Date of Signature</label>
                                    </div>
                                    <div class="col l4">
                                        <a style="margin-left: 1em; cursor: pointer;" href="{{'/download-venture-document/'.$client_contract->id}}"
                                           download>Download</a>

                                        <a style="margin-left: 1em; cursor: pointer;" href="{{'/storage/'.$client_contract->client_contract_url}}" target="_blank">View</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    <hr>

                    <div class="row center">
                        <h6>Upload venture tax clearance</h6>
                        <div class="file-field input-field col l4">
                            <div class="btn">
                                <span>Tax Clearance</span>
                                <input type="file" id="tax_clearance_url" multiple>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate"
                                       type="text" placeholder="Upload in PDF">
                            </div>
                        </div>
                        <div class="input-field col l4">
                            <input type="date" id="date_of_clearance_tax"
                                   value="{{$venture_upload->date_of_clearance_tax}}">
                            <label for="date_of_clearance_tax">Date of Tax Clearance (yyyy-mm-dd)</label>
                        </div>
                        <div class="col l4 input-field">
                            <button class="btn blue" id="tax_clearance_upload_btn">Upload</button>
                        </div>
                    </div>

                    {{--Set Tax Clearances--}}
                    @if(isset($venture_upload->venture_tax_clearances))
                        <div class="row center" style="border: 1px solid black;">
                            <h6>Venture Tax Clearances</h6>
                            <br>
                            @foreach($venture_upload->venture_tax_clearances as $tax_clearances)
                                <div class="row">
                                    <div class="col l4">
                                        <input class="file-path validate" disabled value="{{$tax_clearances->tax_clearance_url}}"
                                               type="text" placeholder="Upload in PDF">
                                    </div>
                                    <div class="col l4">
                                        <input type="text" disabled id="date_of_tax_clearance_display" value="{{$tax_clearances->date_of_tax_clearance}}">
                                        <label for="date_of_tax_clearance_display">Date of Tax Clearance</label>
                                    </div>
                                    <div class="col l4">
                                        <a style="margin-left: 1em; cursor: pointer;" href="{{'/download-venture-document/'.$tax_clearances->id}}"
                                           download>Download</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    <hr>

                    <div class="row center">
                        <h6>Upload venture bbbee certificate</h6>
                        <div class="file-field input-field col l4">
                            <div class="btn">
                                <span>BBBEE Certificate</span>
                                <input type="file" id="bbbee_certificate_url" multiple>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate"
                                       type="text" placeholder="Upload in PDF">
                            </div>
                        </div>
                        <div class="input-field col l4">
                            <input type="date" id="date_of_bbbee"
                                   value="{{$venture_upload->date_of_bbbee}}">
                            <label for="date_of_bbbee">Date of BBBEE (yyyy-mm-dd)</label>
                        </div>
                        <div class="col l4 input-field">
                            <button class="btn blue" id="bbbee_certificate_upload_btn">Upload</button>
                        </div>
                    </div>

                    {{--Set BBBEE Certificates--}}
                    @if(isset($venture_upload->venture_bbbee_certificates))
                        <div class="row center" style="border: 1px solid black;">
                            <h6>Venture BBBEE Certificates</h6>
                            <br>
                            @foreach($venture_upload->venture_bbbee_certificates as $bbbee_certificate)
                                <div class="row">
                                    <div class="col l4">
                                        <input class="file-path validate" disabled value="{{$bbbee_certificate->bbbee_certificate_url}}"
                                               type="text" placeholder="Upload in PDF">
                                    </div>
                                    <div class="col l4">
                                        <input type="text" disabled id="date_of_tax_clearance_display" value="{{$bbbee_certificate->date_of_bbbee_certificate}}">
                                        <label for="date_of_tax_clearance_display">Date of BBBEE Certificate</label>
                                    </div>
                                    <div class="col l4">
                                        <a style="margin-left: 1em; cursor: pointer;" href="{{'/download-venture-document/'.$bbbee_certificate->id}}"
                                           download>Download</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    <hr>


                    <div class="row center">
                        <h6>Upload CK/CIPC Documents</h6>
                        <div class="file-field input-field col l6">
                            <div class="btn">
                                <span>CK Document</span>
                                <input type="file" id="ckDocument_url" multiple>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate"
                                       type="text" placeholder="Upload in PDF">
                            </div>
                        </div>
                        <div class="col l6 input-field">
                            <button class="btn blue" id="ck_document_upload_btn">Upload</button>
                        </div>
                    </div>

                    {{--Set CK Documents--}}
                    @if(isset($venture_upload->venture_ck_documents))
                        <div class="row center" style="border: 1px solid black;">
                            <h6>Venture CK Documents</h6>
                            <br>
                            @foreach($venture_upload->venture_ck_documents as $ck_documents)
                                <div class="row">
                                    <div class="col l6">
                                        <input class="file-path validate" disabled value="{{$ck_documents->ck_documents_url}}"
                                               type="text" placeholder="Upload in PDF">
                                    </div>
                                    <div class="col l6">
                                        <a style="margin-left: 1em; cursor: pointer;" href="{{'/download-venture-document/'.$ck_documents->id}}"
                                           download>Download</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    <div class="row center">
                        <h6> Uplaod Management Account</h6>
                        <div class="file-field input-field col l6">
                            <div class="btn">
                                <span>Management Account</span>
                                <input type="file" id="management_account_url" multiple>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate"
                                       type="text" placeholder="Upload in PDF">
                            </div>
                        </div>

                        <div class="col l6 input-field">
                            <button class="btn blue" id="management_account_upload_btn">Upload</button>
                        </div>
                    </div>

                    {{--Set Management Accounts--}}
                    @if(isset($venture_upload->venture_management_accounts))
                        <div class="row center" style="border: 1px solid black;">
                            <h6>Venture Management Accounts</h6>
                            <br>
                            @foreach($venture_upload->venture_management_accounts as $management_accounts)
                                <div class="row">
                                    <div class="col l6">
                                        <input class="file-path validate" disabled value="{{$management_accounts->management_account_url}}"
                                               type="text" placeholder="Upload in PDF">
                                    </div>
                                    <div class="col l6">
                                        <a style="margin-left: 1em; cursor: pointer;" href="{{'/download-venture-document/'.$management_accounts->id}}"
                                           download>Download</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    <div class="row center">
                        <h6>Upload Dormant Affidavit</h6>
                        <div class="file-field input-field col l6">
                            <div class="btn">
                                <span>Dormant Affidavit</span>
                                <input type="file" id="dormant_affidavit_url" multiple>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate"
                                       type="text" placeholder="Upload in PDF">
                            </div>
                        </div>

                        <div class="col l6 input-field">
                            <button class="btn blue" id="dormant_affodavit_upload_btn">Upload</button>
                        </div>
                    </div>

                    {{--Set Dormant Affidavit--}}
                    @if(isset($venture_upload->venture_dormant_affidavits))
                        <div class="row center" style="border: 1px solid black;">
                            <h6>Venture Dormant Affidavits</h6>
                            <br>
                            @foreach($venture_upload->venture_dormant_affidavits as $dormant_affidavits)
                                <div class="row">
                                    <div class="col l6">
                                        <input class="file-path validate" disabled value="{{$dormant_affidavits->dormant_affidavit_url}}"
                                               type="text" placeholder="Upload in PDF">
                                    </div>
                                    <div class="col l6">
                                        <a style="margin-left: 1em; cursor: pointer;" href="{{'/download-venture-document/'.$dormant_affidavits->id}}"
                                           download>Download</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif


                    <br>
                    <div class="row" id="precinct_info">
                        <h6 style="padding-left: 50px;"><b>PRECINCT INFO</b></h6>

                        <div class="input-field col m6">
                            <input id="pricinct_telephone_code" value="{{$venture->pricinct_telephone_code}}"
                                   type="text" class="validate">
                            <label for="pricinct_telephone_code">Telephone code</label>
                        </div>
                        <div class="input-field col m6">
                            <select id="pricinct_keys">
                                <option value="" disabled selected>Do You Have Keys</option>
                                <option value="Yes"{{$venture->pricinct_keys=='Yes'?'selected':''}}>Yes</option>
                                <option value="No"{{$venture->pricinct_keys=='No'?'selected':''}}>No</option>
                            </select>
                            <label>Keys</label>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="pricinct_office_position" value="{{$venture->pricinct_office_position}}"
                                       type="text" class="validate">
                                <label for="pricinct_office_position">Office Position</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="pricinct_printing_code" value="{{$venture->pricinct_printing_code}}"
                                       type="text" class="validate">
                                <label for="pricinct_printing_code">Printing Code</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="pricinct_warehouse_position"
                                       value="{{$venture->pricinct_warehouse_position}}" type="text"
                                       class="validate">
                                <label for="pricinct_warehouse_position">Warehouse Position</label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row" hidden>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="cohort" value="{{$venture->cohort}}" type="text" class="validate">
                            <label for="cohort">Cohort</label>
                        </div>
                    </div>
                    <br>
                    <h6 style="padding-left: 50px;"><b>VENTURE FUNDING</b></h6>
                    <br>
                    <div class="row">
                        <div class="input-field col m6">
                            <select id="venture_funding">
                                <option value="" disabled selected>Who is Funding Your Venture?</option>
                                <option
                                    value="TIA Seed Fund"{{$venture->venture_funding=='TIA Seed Fund'?'selected':''}}>TIA Seed Fund
                                </option>
                                <option
                                    value="Propella BDS"{{$venture->venture_funding=='Propella BDS'?'selected':''}}>Propella BDS
                                </option>
                            </select>
                            <label>Venture Funding</label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="date_awarded" value="{{$venture->date_awarded}}"
                                   type="text">
                            <label for="date_awarded">Date Awarded (yyyy-mm-dd)</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="fund_value" value="{{$venture->fund_value}}" type="text" class="validate">
                            <label for="fund_value">Amount</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="date_closedout" value="{{$venture->date_closedout}}"
                                   type="text">
                            <label for="date_closedout">Due Date (yyyy-mm-dd)</label>
                        </div>
                    </div>
                    <br>
                    <h6 style="padding-left: 50px;"><b>RENT TURNOVER</b></h6>
                    <br>
                    <div class="row">
                        <div class="input-field col m6">
                            <select id="rent_turnover" form="start_details_form" class="inputs-select">
                                <option value="" disabled selected>Deposited Rent?</option>
                                <option value="Yes"{{$venture->rent_turnover=='Yes'?'selected':''}}>Yes</option>
                                <option value="No"{{$venture->rent_turnover=='No'?'selected':''}}>No</option>
                            </select>
                            <label>Rent Turnover</label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="input-field col m6" id="rentDate">
                            <input id="rent_date" value="{{$venture->rent_date}}"
                                   type="text">
                            <label for="rent_date">Due Date (yyyy-mm-dd)</label>
                        </div>
                        <div class="input-field col m6" id="rentAmount">
                            <input id="rent_amount" value="{{$venture->rent_amount}}" type="text"
                                   placeholder="Amount">
                            <label for="rent_amount">Amount</label>
                        </div>
                    </div>
                    </div>

                    <div class="row">
                        <div class="col offset-m4">
                            <a class="btn waves-effect waves-light" href="{{url('venture')}}">Cancel
                                <i class="material-icons left">arrow_back</i>
                            </a>
                            <button id="save-incubatee-startup-details" style="margin-left: 2em"
                                    class="btn waves-effect waves-light" type="submit" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
                @if(is_null($venture->incubatee_id))
                    <div class="fixed-action-btn">
                        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
                           data-tooltip="Add New Incubatee" href="{{url('venture-incubatee', $venture->id)}}">
                            <i class="large material-icons">add</i>
                        </a>
                    </div>
                @endif
            </div>

            <div id="test2" class="col s12">
                <form id="venture-media-update-form" class="col s12" style="margin-top:1em;" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <input value="{{$venture->id}}" id="incubatee_id" hidden>
                    <div class="row">
                        <div class="input-field col m6">
                            <label for="elevator_pitch">Elevator Pitch</label>
                            <textarea id="elevator_pitch" name="elevator_pitch"
                                      class="materialize-textarea">{{$venture->elevator_pitch}}</textarea>
                        </div>
                        <div class="input-field col m6">
                                <textarea id="venture_profile_information"
                                          class="materialize-textarea">{{$venture->venture_profile_information}}</textarea>
                            <label for="venture_profile_information">Venture Profile Information</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="business_facebook" type="text" value="{{$venture->business_facebook_url}}"
                                   class="validate">
                            <label for="business_facebook">Facebook</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="business_linkedin_url" type="text" value="{{$venture->business_linkedin_url}}"
                                   class="validate">
                            <label for="business_linkedin_url">LinkedIn</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="business_twitter" type="text" value="{{$venture->business_twitter_url}}"
                                   class="validate">
                            <label for="business_twitter">Twitter</label>
                        </div>

                        <div class="input-field col m6">
                            <input id="business_instagram" type="text"
                                   value="{{$venture->business_instagram_url}}" class="validate">
                            <label for="business_instagram">Instagram</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="company_website" type="text" value="{{$venture->company_website}}"
                                   class="validate">
                            <label for="company_website"> Company Website</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="venture_email" value="{{$venture->venture_email}}" type="text"
                                   class="validate">
                            <label for="venture_email">Venture Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Company Logo</span>
                                    <input id="company_logo_url" type="file" name="company_logo_url">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path"
                                           value="{{$venture_upload->company_logo_url?$venture_upload->company_logo_url:''}}"
                                           type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col m6">
                            <div class="input-field" style="bottom:0px!important;">
                                <input id="video_shot_url"
                                       value="{{$venture_upload->video_shot_url?$venture_upload->video_shot_url:''}}"
                                       type="text">
                                <label for="video-shot">Video Shot</label>
                            </div>
                        </div>
                        <div class="input-field col m6">
                            <input id="video_title" type="text" class="validate">
                            <label for="video_title">Video Title</label>
                        </div>
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Infographic</span>
                                    <input id="infographic_url" type="file" name="infographic_url">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path"
                                           value="{{$venture_upload->infographic_url?$venture_upload->infographic_url:''}}"
                                           type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Crowdfunding</span>
                                    <input id="crowdfunding_url" type="file" name="crowdfunding_url">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path"
                                           value="{{$venture_upload->crowdfunding_url?$venture_upload->crowdfunding_url:''}}"
                                           type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Marketing Assets</span>
                                    <input id="product_shot_url" type="file" name="product-shot">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path"
                                           value="{{$venture_upload->product_shot_url?$venture_upload->product_shot_url:''}}"
                                           type="text">
                                </div>
                            </div>
                        </div>
                        {{--Market research--}}
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Market research</span>
                                    <input id="market_research_upload" type="file" name="market_research_upload-shot">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path"
                                           value="{{$venture_upload->market_research_upload?$venture_upload->market_research_upload:''}}"
                                           type="text">
                                </div>
                            </div>
                        </div>
                        {{--Project plan--}}
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Project plan</span>
                                    <input id="project_plan_upload" type="file" name="project_plan_upload">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path"
                                           value="{{$venture_upload->project_plan_upload?$venture_upload->project_plan_upload:''}}"
                                           type="text">
                                </div>
                            </div>
                        </div>
                        {{--Business plan--}}
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Business plan</span>
                                    <input id="business_plan" type="file" name="business_plan">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path"
                                           value="{{$venture_upload->business_plan?$venture_upload->business_plan:''}}"
                                           type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col m6">
                            <div class="input-field" style="bottom:0px!important;">
                                <input id="stage_two_to_three_video_url"
                                       value="{{$venture_upload->stage_two_to_three_video_url?$venture_upload->stage_two_to_three_video_url:''}}"
                                       type="text">

                                <label for="video-shot">Stage 2 to 3 Video Shot</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m6">
                            <div class="input-field" style="bottom:0px!important;">
                                <input id="stage_three_to_four_video"
                                       value="{{$venture_upload->stage_three_to_four_video?$venture_upload->stage_three_to_four_video:''}}"
                                       type="text">
                                <label for="stage_three_to_four_video">STAGE 3 TO 4 PITCH VIDEO</label>
                            </div>
                        </div>
                        <div class="col m6">
                            <div class="input-field" style="bottom:0px!important;">
                                <input id="stage_four_to_five_video"
                                       value="{{$venture_upload->stage_four_to_five_video?$venture_upload->stage_four_to_five_video:''}}"
                                       type="text">
                                <label for="stage_four_to_five_video">STAGE 4 TO 5 PITCH VIDEO</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m6">
                            <div class="input-field" style="bottom:0px!important;">
                                <input id="product_video_url"
                                       value="{{$venture_upload->product_video_url?$venture_upload->product_video_url:''}}"
                                       type="text">
                                <label for="product_video_url">PRODUCT VIDEO</label>
                            </div>
                        </div>
                    </div>
                    <div class="row" hidden>
                        {{--Stage 3 client contract--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Stage 3 client contract</span>
                                        <input id="stage3_client_contract" type="file" name="stage3_client_contract">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{$venture_upload->stage3_client_contract?$venture_upload->stage3_client_contract:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <input type="date"  id="stage3_date_of_signature"
                                       value="{{$venture_upload->stage3_date_of_signature}}">
                                <label for="stage3_date_of_signature">Stage 3 Date of Signature</label>
                            </div>
                        </div>
                        {{--Stage 4 client contract--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Stage 4 client contract</span>
                                        <input id="stage4_client_contract" type="file" name="stage4_client_contract">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{$venture_upload->stage4_client_contract?$venture_upload->stage4_client_contract:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <input type="date"  id="stage4_date_of_signature"
                                       value="{{$venture_upload->stage4_date_of_signature}}">
                                <label for="stage4_date_of_signature">Stage 4 Date of Signature</label>
                            </div>
                        </div>

                        {{--Stage 5 client contract--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Stage 5 client contract</span>
                                        <input id="stage5_client_contract" type="file" name="stage5_client_contract">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{$venture_upload->stage5_client_contract?$venture_upload->stage5_client_contract:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <input type="date"  id="stage5_date_of_signature"
                                       value="{{$venture_upload->stage5_date_of_signature}}">
                                <label for="stage5_date_of_signature">Stage 5 Date of Signature</label>
                            </div>
                        </div>

                        {{--Stage 6 client contract--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Stage 6 client contract</span>
                                        <input id="stage6_client_contract" type="file" name="stage6_client_contract">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{$venture_upload->stage6_client_contract?$venture_upload->stage6_client_contract:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <input type="date"  id="stage6_date_of_signature"
                                       value="{{$venture_upload->stage6_date_of_signature}}">
                                <label for="stage5_date_of_signature">Stage 6 Date of Signature</label>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col offset-m4">
                            <a class="btn waves-effect waves-light" href="{{url('venture')}}">Cancel
                                <i class="material-icons left">arrow_back</i>
                            </a>
                            <button id="save-incubatee-media" style="margin-left: 2em"
                                    class="btn waves-effect waves-light" type="submit" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <div id="test3" class="col s12">
                <form id="venture-media-update-form" class="col s12" style="margin-top:1em;" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <input value="{{$venture->id}}" id="incubatee_id" hidden>
                    <div class="row">
                    </div>
                    {{--Set Client Contracts--}}
                    @if(isset($venture_upload->venture_spreadsheet))
                        <div class="row center">
                            <h6>M + E Spreadsheet</h6>
                            <p style="color: red">Note : After updating the spreadsheet please upload it.</p>
                            <br>
                            @if(isset($venture_upload->venture_spreadsheet))
                                <div class="row center" style="border: 1px solid black;">
                                    <h6>M + E Spreadsheet</h6>
                                    <br>
                                    @foreach($venture_upload->venture_spreadsheet as $client_contract)
                                        <div class="row">
                                            <div class="col l6">
                                                <input class="file-path validate" disabled value="{{$client_contract->spreadsheet_upload}}" type="text" placeholder="Upload in PDF">
                                            </div>
                                            <div class="col l3">
                                                <input class="center-align" type="text"  id="year" value="{{$client_contract->month}}">
                                                <label for="year">Year + Month</label>
                                            </div>
                                            <div class="col l3">
                                                <a href="{{'/storage/'.$client_contract->spreadsheet_upload}}" target="_blank">Download</a>
                                            </div>
                                        </div>

                                    @endforeach
                                </div>
                            <br>
                                <h6>Updated M + E Spreadsheets </h6>
                                <table>
                                    <tr>
                                        <th>M + E Spreadsheet</th>
                                        <th>Year + Month</th>
                                        <th>Date uploaded</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($venture_upload->incubatee_venture_spreadsheet as $client_contract)
                                    <tr>
                                        <td>{{$client_contract->spreadsheet_upload}}</td>
                                        <td>{{$client_contract->month}}</td>
                                        <td>{{$client_contract->date_submitted}}</td>
                                        <td>
                                            <a href="{{'/storage/'.$client_contract->spreadsheet_upload}}" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                        @endforeach

                                </table>

                                <div class="row center">
                                    <h6>Upload updated M + E Spreadsheet here </h6>
                                    <br>
                                    <div class="file-field input-field col l6">
                                        <div class="btn">
                                            <span>M + E Spreadsheet </span>
                                            <input type="file" id="spreadsheet_upload">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate"
                                                   type="text" placeholder="Upload in Excel">
                                        </div>
                                    </div>
                                    <div class="col l3">
                                        <input class="center-align" type="text"  id="year">
                                        <label for="year">Enter Year</label>
                                    </div>
                                    <div class="col l3">
                                        <input class="center-align" type="month"  id="month">
                                        <label for="month">Choose month</label>
                                    </div>
                                    <div class="col l4 input-field">
                                        <button style="margin-left: 440px" id="success">Submit</button>
                                    </div>
                                </div>
                            @endif

                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
    <style>
        button {
            background-color: cadetblue;
            color: whitesmoke;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            font-size: 18px;
            font-weight: 500;
            border-radius: 7px;
            padding: 15px 35px;
            cursor: pointer;
            white-space: nowrap;
            margin: 10px;
        }
        img {
            width: 200px;
        }
        input[type="text"] {
            padding: 12px 20px;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 10px;
            box-sizing: border-box;
        }
        h1 {
            border-bottom: solid 2px grey;
        }
        #success {
            background: green;
        }
        #error {
            background: red;
        }
        #warning {
            background: coral;
        }
        #info {
            background: cornflowerblue;
        }
        #question {
            background: grey;
        }
        th{
            text-transform: uppercase!important;
        }
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }


        .tabs .tab a:hover, .tabs .tab a.active {
            color: black !important;
            font-weight: bolder;
        }

        .tabs .tab a {
            color: black !important;
        }
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    @push('custom-scripts')
        <script>
            let venture_id = $('#venture_id').val();
            $('#venture-form').on('click',function (){
                window.location.href='/venture-shadow/'+ venture_id
            });

            $('#deleteCompanyLogo').click(function () {
                $('#compony_log_hide').hide();
            });
            $('#deleteInfographic_url').click(function () {
                $('#infographic_url_hide').hide();
            });
            $('#deletCrowdfunding_url').click(function () {
                $('#crowdfunding_url_hide').hide();
            });
            $('#deletRroduct_shot_url').click(function () {
                $('#product_shot_url_hide').hide();
            });

            function clearCompanyLogo() {

                $('#company_logo_url').val("");/*.reset();*/

            }

            function clearProductShot() {

                $('#product_shot_url').val().reset();

            }

            function clearCrowdfunding() {

                $('#crowdfunding_url').val().reset();

            }

            function clearInfographic() {

                $('#infographic_url').val().reset();
            }

            $(function () {
                $("#virtual_or_physical").change(function () {
                    if ($("#Physical").is(":selected")) {
                        $("#precinct_info").show();

                    } else {
                        $("#precinct_info").hide();

                    }
                }).trigger('change');
            });

            $(document).ready(function () {
                $(document).on('click', '#success', function(e) {
                    e.preventDefault();
                    let formData = new FormData();
                    let venture_id = $('#venture_id').val();

                    formData.append('year', $('#year').val());
                    formData.append('month', $('#month').val());
                    jQuery.each(jQuery('#spreadsheet_upload')[0].files, function (i, file) {
                        formData.append('spreadsheet_upload', file);
                    });

                    let url = '/incubatee-upload-venture-spreadsheet/' + venture_id;

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            swal(
                                'Success',
                                'M + E Spreadsheet saved <b style="color:green;">Successfully</b>',
                                'success',
                            )

                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            swal(
                                'Error',
                                'M + E Spreadsheet  <b style="color:red;">not saved, you can only upload next month</b>',
                                'success',
                            )

                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        }
                    });
                });
                $(document).on('click', '#success-mobile', function(e) {
                    e.preventDefault();
                    let formData = new FormData();
                    let venture_id = $('#venture_id').val();

                    formData.append('year', $('#year-mobile').val());
                    formData.append('month', $('#month-mobile').val());
                    jQuery.each(jQuery('#spreadsheet_upload-mobile')[0].files, function (i, file) {
                        formData.append('spreadsheet_upload', file);
                    });

                    let url = '/incubatee-upload-venture-spreadsheet/' + venture_id;

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            swal(
                                'Success',
                                'M + E Spreadsheet saved <b style="color:green;">Successfully</b>',
                                'success',
                            )

                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            swal(
                                'Error',
                                'M + E Spreadsheet  <b style="color:red;">not saved, you can only upload next month</b>',
                                'success',
                            )

                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        }
                    });
                });
                // Alert With Custom Icon and Background Image
                $(document).on('click', '#icon', function(event) {
                    swal({
                        title: 'Custom icon!',
                        text: 'Alert with a custom image.',
                        imageUrl: 'https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg',
                        imageWidth: 200,
                        imageHeight: 200,
                        imageAlt: 'Custom image',
                        animation: false
                    })
                });

                $(document).on('click', '#image', function(event) {
                    swal({
                        title: 'Custom background image, width and padding.',
                        width: 700,
                        padding: 150,
                        background: '#fff url(https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg)'
                    })
                });

                let venture_id = $('#venture_id').val();
                let url = '/get-venture-incubatees/' + venture_id;

                $("select").formSelect();
                $('.tooltipped').tooltip();


                //Style block where profile picture is loaded
                function profilePictureIsLoaded(e) {
                    $("#profile_picture_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#profile-picture-preview').attr('src', e.target.result);
                    $('#profile-picture-preview').attr('width', '250px');
                    $('#profile-picture-preview').attr('height', '230px');
                }

                //Style block where company logo is loaded
                function companyLogoIsLoaded(e) {
                    $("#company_logo_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#company-logo-preview').attr('src', e.target.result);
                    $('#company-logo-preview').attr('width', '250px');
                    $('#company-logo-preview').attr('height', '230px');
                }

                //Style block where video shot is loaded
                function videoShotIsLoaded(e) {
                    $("#video-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#video-shot-preview').attr('src', e.target.result);
                    $('#video-shot-preview').attr('width', '250px');
                    $('#video-shot-preview').attr('height', '230px');
                }

                //Style block where infographic is loaded
                function infographicIsLoaded(e) {
                    $("#infographic_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#infographic-preview').attr('src', e.target.result);
                    $('#infographic-preview').attr('width', '250px');
                    $('#infographic-preview').attr('height', '230px');
                }

                //Style block where crowdfunding is loaded
                function crowdfundingIsLoaded(e) {
                    $("#crowdfunding_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#crowdfunding-preview').attr('src', e.target.result);
                    $('#crowdfunding-preview').attr('width', '250px');
                    $('#crowdfunding-preview').attr('height', '230px');
                }

                //Style block where product shot is loaded
                function productShotIsLoaded(e) {
                    $("#product-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#product-shot-preview').attr('src', e.target.result);
                    $('#product-shot-preview').attr('width', '250px');
                    $('#product-shot-preview').attr('height', '230px');
                }

                // Function to preview profile picture after validation
                $(function () {
                    $("#profile_picture_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#profile-picture-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = profilePictureIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview company logo after validation
                $(function () {
                    $("#company_logo_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#company-logo-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = companyLogoIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview video shot after validation
                $(function () {
                    $("#video-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#video-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = videoShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview infographic after validation
                $(function () {
                    $("#infographic_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#infographic-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = infographicIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview crowdfunding after validation
                $(function () {
                    $("#crowdfunding_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#crowdfunding-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = crowdfundingIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview product shot after validation
                $(function () {
                    $("#product-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#product-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = productShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });

                $(".input-select").on("change", checkSelect);
                $("#exitDate").hide();
                $("#resignDate").hide();
                $("#alumniDate").hide();
                $("#reasonForLeaving").hide();


                function checkSelect() {
                    if ($('#status').val() == 'Alumni') {
                        $("#exitDate").hide();
                        $("#resignDate").hide();
                        $("#alumniDate").show();
                        $("#reasonForLeaving").hide();
                    }
                    if ($('#status').val() == 'Exit') {
                        $("#exitDate").show();
                        $("#resignDate").hide();
                        $("#alumniDate").hide();
                        $("#reasonForLeaving").show();
                    }
                    if ($('#status').val() == 'Resigned') {
                        $("#exitDate").hide();
                        $("#resignDate").show();
                        $("#alumniDate").hide();
                        $("#reasonForLeaving").show();
                    }
                    if ($('#status').val() == 'Active') {
                        $("#exitDate").hide();
                        $("#resignDate").hide();
                        $("#alumniDate").hide();
                        $("#reasonForLeaving").hide();
                    }
                }

                let now = new Date();
                let day = ("0" + now.getDate()).slice(-2);
                let month = ("0" + (now.getMonth() + 1)).slice(-2);
                let today = now.getFullYear() + "-" + (month) + "-" + (day);

                $(".inputs-select").on("change", rentSelect)

                function rentSelect() {
                    if ($('#rent_turnover').val() == 'No') {
                        $("#rentDate").show();
                        $("#rent_date").val(today).show();
                        $("#rentAmount").show();
                        $("#rent_amount").val() == 0;
                    }
                    if ($('#rent_turnover').val() == 'Yes') {
                        $("#rentDate").show();
                        $("#rentAmount").show();
                        $("#rent_amount").val() == "";
                        $("#rent_date").val() == "";

                    }

                }


                $('#start_details_update_form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();
                    formData.append('company_name', $('#company_name').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('physical_address_one', $('#physical_address_one').val());
                    formData.append('physical_address_two', $('#physical_address_two').val());
                    formData.append('physical_address_three', $('#physical_address_three').val());
                    formData.append('physical_city', $('#physical_city').val());
                    formData.append('physical_postal_code', $('#physical_postal_code').val());
                    formData.append('postal_address_one', $('#postal_address_one').val());
                    formData.append('postal_address_two', $('#postal_address_two').val());
                    formData.append('postal_address_three', $('#postal_address_three').val());
                    formData.append('postal_city', $('#postal_city').val());
                    formData.append('postal_code', $('#postal_code').val());
                    formData.append('hub', $('#hub').val());
                    formData.append('smart_city_tags', $('#smart_city_tags').val());
                    formData.append('business_type', $('#business_type').val());
                    formData.append('cohort', $('#cohort').val());
                    formData.append('stage', $('#stage').val());
                    formData.append('virtual_or_physical', $('#virtual_or_physical').val());
                    formData.append('ownership', $('#ownership').val());
                    formData.append('registration_number', $('#registration_number').val());
                    formData.append('BBBEE_level', $('#BBBEE_Level').val());
                    formData.append('date_of_signature', $('#date_of_signature').val());
                    formData.append('date_of_clearance_tax', $('#date_of_clearance_tax').val());
                    formData.append('date_of_bbbee', $('#date_of_bbbee').val());
                    formData.append('pricinct_telephone_code', $('#pricinct_telephone_code').val());
                    formData.append('pricinct_keys', $('#pricinct_keys').val());
                    formData.append('pricinct_office_position', $('#pricinct_office_position').val());
                    formData.append('pricinct_printing_code', $('#pricinct_printing_code').val());
                    formData.append('pricinct_warehouse_position', $('#pricinct_warehouse_position').val());
                    formData.append('rent_turnover', $('#rent_turnover').val());
                    formData.append('rent_date', $('#rent_date').val());
                    formData.append('rent_amount', $('#rent_amount').val());
                    formData.append('status', $('#status').val());
                    formData.append('alumni_date', $('#alumni_date').val());
                    formData.append('exit_date', $('#exit_date').val());
                    formData.append('resigned_date', $('#resigned_date').val());
                    formData.append('mentor', $('#mentor').val());
                    formData.append('advisor', $('#advisor').val());
                    formData.append('venture_funding', $('#venture_funding').val());
                    formData.append('date_awarded', $('#date_awarded').val());
                    formData.append('fund_value', $('#fund_value').val());
                    formData.append('date_closedout', $('#date_closedout').val());
                    formData.append('venture_type', $('#venture_type').val());
                    formData.append('reason_for_leaving', $('#reason_for_leaving').val());
                    formData.append('sustainable_development_goals', $('#sustainable_development_goals').val());


                    jQuery.each(jQuery('#client_contract')[0].files, function (i, file) {
                        formData.append('client_contract', file);
                    });

                    jQuery.each(jQuery('#tax_clearance_url')[0].files, function (i, file) {
                        formData.append('tax_clearance_url', file);
                    });

                    jQuery.each(jQuery('#bbbee_certificate_url')[0].files, function (i, file) {
                        formData.append('bbbee_certificate_url', file);
                    });

                    jQuery.each(jQuery('#ckDocument_url')[0].files, function (i, file) {
                        formData.append('ckDocument_url', file);
                    });

                    jQuery.each(jQuery('#vat_registration_number_url')[0].files, function (i, file) {
                        formData.append('vat_registration_number_url', file);
                    });

                    jQuery.each(jQuery('#management_account_url')[0].files, function (i, file) {
                        formData.append('management_account_url', file);
                    });

                    jQuery.each(jQuery('#dormant_affidavit_url')[0].files, function (i, file) {
                        formData.append('dormant_affidavit_url', file);
                    });


                    let url = '/venture-update/' + '{{$venture->id}}';

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            $("#save-incubatee-startup-details").notify(
                                "You have successfully Upadated Venture", "success",
                                {position: "right"}
                            );
                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });

                $('#venture-media-update-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();
                    formData.append('video_shot_url', $('#video_shot_url').val());
                    formData.append('company_website', $('#company_website').val());
                    formData.append('elevator_pitch', $('#elevator_pitch').val());
                    formData.append('venture_profile_information', $('#venture_profile_information').val());
                    formData.append('venture_email', $('#venture_email').val());
                    formData.append('business_facebook_url', $('#business_facebook').val());
                    formData.append('business_twitter_url', $('#business_twitter').val());
                    formData.append('business_linkedin_url', $('#business_linkedin_url').val());
                    formData.append('business_instagram_url', $('#business_instagram').val());
                    formData.append('video_title', $('#video_title').val());
                    formData.append('stage_two_to_three_video_url', $('#stage_two_to_three_video_url').val());
                    formData.append('stage3_date_of_signature', $('#stage3_date_of_signature').val());
                    formData.append('stage4_date_of_signature', $('#stage4_date_of_signature').val());
                    formData.append('stage5_date_of_signature', $('#stage5_date_of_signature').val());
                    formData.append('stage6_date_of_signature', $('#stage6_date_of_signature').val());
                    formData.append('stage_three_to_four_video', $('#stage_three_to_four_video').val());
                    formData.append('stage_four_to_five_video', $('#stage_four_to_five_video').val());
                    formData.append('product_video_url', $('#product_video_url').val());

                    jQuery.each(jQuery('#company_logo_url')[0].files, function (i, file) {
                        formData.append('company_logo_url', file);
                    });

                    jQuery.each(jQuery('#infographic_url')[0].files, function (i, file) {
                        formData.append('infographic_url', file);
                    });

                    jQuery.each(jQuery('#crowdfunding_url')[0].files, function (i, file) {
                        formData.append('crowdfunding_url', file);
                    });

                    jQuery.each(jQuery('#product_shot_url')[0].files, function (i, file) {
                        formData.append('product_shot_url', file);
                    });

                    jQuery.each(jQuery('#market_research_upload')[0].files, function (i, file) {
                        formData.append('market_research_upload', file);
                    });

                    jQuery.each(jQuery('#project_plan_upload')[0].files, function (i, file) {
                        formData.append('project_plan_upload', file);
                    });

                    jQuery.each(jQuery('#business_plan')[0].files, function (i, file) {
                        formData.append('business_plan', file);
                    });

                    jQuery.each(jQuery('#stage3_client_contract')[0].files, function (i, file) {
                        formData.append('stage3_client_contract', file);
                    });

                    jQuery.each(jQuery('#stage4_client_contract')[0].files, function (i, file) {
                        formData.append('stage4_client_contract', file);
                    });

                    jQuery.each(jQuery('#stage5_client_contract')[0].files, function (i, file) {
                        formData.append('stage5_client_contract', file);
                    });

                    jQuery.each(jQuery('#stage6_client_contract')[0].files, function (i, file) {
                        formData.append('stage6_client_contract', file);
                    });


                    console.log('Company Logo: ' + formData.get('company_logo_url'));


                    let url = '/venture-update-media/' + '{{$venture->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            $("#save-incubatee-media").notify(
                                "You have successfully Upadated Venture Media", "success",
                                {position: "right"}
                            );
                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
            });

            function confirm_delete_incubatee_in_venture(obj) {
                var r = confirm("Are you sure want to delete this application!");
                if (r == true) {
                    $.get('/incubatee-delete/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status == 'success') {
                            alert(data.message);
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }

            }

            $('#client_contract_upload_btn').on('click', function(e){
                e.preventDefault();
                let formData = new FormData();
                let venture_id = $('#venture_id').val();

                formData.append('date_of_signature', $('#date_of_signature').val());

                jQuery.each(jQuery('#client_contract_url')[0].files, function (i, file) {
                    formData.append('client_contract_url', file);
                });

                let url = '/upload-venture-client-contract/' + venture_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $('#client_contract_upload_btn').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        $('#client_contract_upload_btn').notify(response.message, "error");
                    }
                });
            });

            $('#tax_clearance_upload_btn').on('click', function(e){
                e.preventDefault();
                let formData = new FormData();
                let venture_id = $('#venture_id').val();

                formData.append('date_of_clearance_tax', $('#date_of_clearance_tax').val());

                jQuery.each(jQuery('#tax_clearance_url')[0].files, function (i, file) {
                    formData.append('tax_clearance_url', file);
                });

                let url = '/upload-venture-tax-clearance/' + venture_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $('#tax_clearance_upload_btn').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        $('#tax_clearance_upload_btn').notify(response.message, "error");
                    }
                });
            });

            $('#bbbee_certificate_upload_btn').on('click', function(e){
                e.preventDefault();
                let formData = new FormData();
                let venture_id = $('#venture_id').val();

                formData.append('date_of_bbbee', $('#date_of_bbbee').val());

                jQuery.each(jQuery('#bbbee_certificate_url')[0].files, function (i, file) {
                    formData.append('bbbee_certificate_url', file);
                });

                let url = '/upload-venture-bbbee-certificate/' + venture_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $('#bbbee_certificate_upload_btn').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        $('#bbbee_certificate_upload_btn').notify(response.message, "error");
                    }
                });
            });

            $("#ck_document_upload_btn").on('click', function(e){
                e.preventDefault();
                let formData = new FormData();
                let venture_id = $('#venture_id').val();

                jQuery.each(jQuery('#ckDocument_url')[0].files, function (i, file) {
                    formData.append('ckDocument_url', file);
                });

                let url = '/upload-venture-ck-document/' + venture_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $('#ck_document_upload_btn').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        $('#ck_document_upload_btn').notify(response.message, "error");
                    }
                });
            });

            $('#management_account_upload_btn').on('click', function(e){
                e.preventDefault();
                let formData = new FormData();
                let venture_id = $('#venture_id').val();

                jQuery.each(jQuery('#management_account_url')[0].files, function (i, file) {
                    formData.append('management_account_url', file);
                });

                let url = '/upload-venture-management-account/' + venture_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $('#management_account_upload_btn').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        $('#management_account_upload_btn').notify(response.message, "error");
                    }
                });
            });

            $('#dormant_affodavit_upload_btn').on('click', function(e){
                e.preventDefault();
                let formData = new FormData();
                let venture_id = $('#venture_id').val();

                jQuery.each(jQuery('#dormant_affidavit_url')[0].files, function (i, file) {
                    formData.append('dormant_affidavit_url', file);
                });

                let url = '/upload-venture-dormant-affidavit/' + venture_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $('#dormant_affodavit_upload_btn').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        $('#dormant_affodavit_upload_btn').notify(response.message, "error");
                    }
                });
            });
        </script>
    @endpush
@endsection
