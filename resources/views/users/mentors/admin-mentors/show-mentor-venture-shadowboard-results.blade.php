@extends('layouts.admin-layout')

@section('content')
    <div class="row center">
        <br>
        <h4>Shadowboard result between <b>{{$mentor_user->name}} {{$mentor_user->surname}}</b> and <b>{{$venture->company_name}}</b></h4>
    </div>


    <div class="row">
        <div class="col l6 m6 s12" id="MentorQuestionsAndAnswers">
            <div class="row center" style="background-color: black">
                <h4 style="color: white;">Mentor Answers</h4>
            </div>
            @foreach($mentorComment as $mentorComments)
                @if($mentorComments->comment_section != null)
                    <p>Comment</p>
                    <textarea rows="7" cols="110" name="closing_report"
                              id="closing_report">{{$mentorComments->comment_section}}</textarea><br>
                @endif
            @endforeach
            @if(count($mentorShadowboardAnswers) > 0)
                <div class="card">
                    <div class="card-content">
                        @foreach($mentorShadowboardAnswers as $m_sa)
                            <h5>{{$m_sa->question_number}} - {{$m_sa->question_text}}</h5>
                            <input type="text" disabled value="{{$m_sa->answer_text}}">
                        @endforeach
                    </div>
                </div>
            @else
                <div class="card center">
                    <div class="card-content">
                        <p>{{$mentor_user->name}} {{$mentor_user->surname}} still needs to submit their answers.</p>
                    </div>
                </div>
            @endif
            <div class="row center">
                <button id="print-button"  class="waves-effect waves-light btn" onclick="printContent('MentorQuestionsAndAnswers')">
                    <i class="material-icons left">local_printshop</i>Print
                </button>
            </div>
        </div>
        <div class="col l6 m6 s12" id="VentureQuestionsAndAnswers">
            <div class="row center" style="background-color: black">
                <h4 style="color: white;">Venture Answers</h4>
            </div>
            @if(count($ventureShadowboardAnswers) > 0)
                <div class="card">
                    <div class="card-content">
                        @foreach($ventureShadowboardAnswers as $v_sa)
                            <h5>{{$v_sa->question_number}} - {{$v_sa->question_text}}</h5>
                            <input type="text" disabled value="{{$v_sa->answer_text}}">
                        @endforeach
                    </div>
                </div>
            @else
                <div class="card center">
                    <div class="card-content">
                        <p>{{$venture->company_name}} still needs to submit their answers.</p>
                    </div>
                </div>
            @endif
            <div class="row center">
                <button id="print-button"  class="waves-effect waves-light btn" onclick="printContent('VentureQuestionsAndAnswers')">
                    <i class="material-icons left">local_printshop</i>Print
                </button>
            </div>
        </div>
    </div>
    <script>
        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }
    </script>
@endsection

