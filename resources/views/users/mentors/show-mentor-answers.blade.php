@extends('layouts.admin-layout')

@section('content')


    <div class="card" style="width: 1000px;top: 10vh;margin: 0 auto">
        <br>
        <br>
        <h5 style="margin-left: 2em"><b>Mentor Feedback</b></h5>

        <h5 style="margin-left: 2em"><b>{{$mentor->user->name}} {{$mentor->user->surname}}</b></h5>

        <br>

        @foreach($userQuestionAnswers as $question_answer)
            <div class="row" style="margin-left: 3em;">
                <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                <div class="input-field" style="margin-right: 2em">
                    <textarea> {{$question_answer->answer_text}}</textarea>
                </div>
            </div>

        @endforeach
    <br>
    </div>

    @endsection
