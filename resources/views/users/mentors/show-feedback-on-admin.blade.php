@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>

    <div id="QuestionsAndAnswers" style="width: 1000px;top: 10vh;margin: 0 auto">
        @foreach($venture_shadow as $ven)
            <div class="row">
                <div class="col s12">
                    <div class="card  shadow modal-trigger" href="#modal1">
                        <br>
                        <h5 style="margin-left: 3em"><b>Mentor Feedback</b></h5>
                        <h5 style="margin-left: 3em"><b>Venture Name: {{$venture->company_name}}</b></h5>
                        <div class="card-content black-text">
                            <p style="margin-left: 3em;"><b>Date : </b>{{$ven->shadow_board_date}}</p>
                            <p style="margin-left: 3em;"><b>Time : </b>{{$ven->shadow_board_time}}</p>
                            @foreach($ven->ventureQuestionAnswers as $question_answer)
                                <div class="row" style="margin-left: 3em;">
                                    <h6>{{$question_answer->question_number}}
                                        - {{ $question_answer->question_text}} </h6>
                                    <div class="input-field" style="margin-right: 2em">
                                        <textarea>{{$question_answer->answer_text}}</textarea>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
        @endforeach
    </div>
    <br>
    <div class="row" style="margin-left: 300px">
        <button id="print-button"  style="width:10%;margin-left: 380px;" class="waves-effect waves-light btn" onclick="printContent('QuestionsAndAnswers')">
            <i class="material-icons left">local_printshop</i>PRINT</button>
    </div>

    <script>
        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }
    </script>
    @endsection
