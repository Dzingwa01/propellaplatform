@extends('layouts.tenant-layout')
@section('content')
    <br>
    <br>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>


    <h5 style="padding-top: 5px;" align="center"><b>PROPELLA EVENT CALENDAR</b></h5>
    <br/>
    {{--This is where the problem was--}}
    <div class="container" id="calendar-desktop" style="height: 900px; width: 1000px;">
        <div class="row">


            <a href="/full_calendar/view-events" data-tooltip="View Events" class="btn tooltipped purple"
               data-position="top" style="border-radius: 10px; height: 60px; width: 80px; padding-top: 8px;"
               id="view-events"><i class="large material-icons">view_list</i></a>
        </div>
        {!! $calendar->calendar() !!}
        {!! $calendar->script() !!}
    </div>

    <div class="row" id="calender-event-information-desktop">

    </div>


    <!-- Modal Structure -->
    <div id="modal2" class="modal">
        {{--<div class="modal-content">
            <a class="btn btn-primary" href="/full_calendar/view-media">View pictures</a>
        </div>--}}
        <div class="modal-footer">

        </div>
    </div>

    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">

            <h4>Add New Event</h4>
            <div class="row">
                <form class="col s12" id="AddEvent" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="input-field col m6">
                            <input type="number" id="participants" name="participants"
                                   min="00" max="100">
                            <label for="participants">Participants (00-100)</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="title" name="title" type="text" class="validate">
                            <label for="title">Title</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="start" type="date" class="validate">
                            <label for="start">start date</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="end" type="date" class="validate">
                            <label for="end">end date</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <select id="type">
                                <option value="" disabled selected>Select type of events</option>
                                <option value="Public">Public</option>
                                <option value="Private">Private</option>
                                <option value="Team Only">Team Only</option>
                            </select>
                            <label for="type">Type of events</label>
                        </div>
                        <div class="input-field col m6">
                            <select id="all_day" class="input-select">
                                <option value="" disabled selected>Occupy all day</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                            <label>Occupy all day</label>
                        </div>
                    </div>
                    <div class="row col m6" id="select_time" hidden>
                        <div class="input-field col m6">
                            <input type="time" id="start_time" min="8:00" max="18:00">
                            <label for="end">Start Time</label>
                        </div>
                        <div class="input-field col m6">
                            <input type="time" id="end_time" min="8:00" max="18:00">
                            <label for="end">End Time</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col m6">
                            <textarea id="description" name="description" class="materialize-textarea"></textarea>
                            <label for="description">Description</label>
                        </div>
                        <div class="input-field col m6">
                            <select id="venue_types" >
                                <option value="" disabled selected>Please Choose venue</option>
                                <option value="Propella Meeting Room 1">Propella Meeting Room 1</option>
                                <option value="Propella Meeting Room 2">Propella Meeting Room 2</option>
                                <option value="Propella Training Room">Propella Training Room</option>
                                <option value="Propella Office Space">Propella Office Space</option>
                                <option value="Propella Rec Room">Propella Rec Room</option>
                            </select>
                            <label>Choose venue</label>
                            {{--<select id="event_venue_id">
                                <option value="" disabled selected>Choose venue</option>
                                @foreach($venueName as $venue)
                                    <option value="{{$venue->id}}">{{$venue->venue_name}}</option>
                                @endforeach
                            </select>--}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Event Image</span>
                                    <input id="event_image_url" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Invite Image</span>
                                    <input id="invite_image_url" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col m6">
                            <input id="color" style="width: 50%" type="color" class="validate">
                            <label for="color">Choose Colour</label>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="col offset-m4">
                        <a href="#!" class="modal-close waves-effect waves-green btn">Cancel<i
                                class="material-icons right">close</i>
                        </a>
                        <button class="btn waves-effect waves-light" type="submit" style="margin-left:2em;"
                                id="save-event" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    @push('custom-scripts')
        {{--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--}}
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
        <script src="{{ url('_asset/fullcalendar') }}/fullcalendar.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

        <script>

            //Get the collection of events
            //$('#calendar').fullCalendar('clientEvents')


            $(document).ready(function () {
                $('select').formSelect();
                //Select option for all day selectors
                $(".input-select").on("change", checkSelect);

                function checkSelect() {
                    if ($('#all_day').val() === 'No') {
                        $("#select_time").show();

                    }
                    if ($('#all_day').val() === 'Yes') {
                        $('#start_time').val("08:00") && $('#end_time').val("18:00")
                    }
                }


                $(function () {
                    $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay,listMonth'
                        },
                        editable: false,
                        eventLimit: true, // allow "more" link when too many events
                        events: {
                            url: base_url + '/api',
                            error: function() {
                                alert("cannot load json");
                            }
                        }

                    });
                });


                $('#AddEvent').on('submit', function (e) {

                    e.preventDefault();
                    let formData = new FormData();
                    formData.append('title', $('#title').val());
                    formData.append('start', $('#start').val());
                    formData.append('end', $('#end').val());
                    formData.append('all_day', $('#all_day').val());
                    formData.append('color', $('#color').val());
                    formData.append('description', $('#description').val());
                    formData.append('type', $('#type').val());
                    formData.append('start_time', $('#start_time').val());
                    formData.append('end_time', $('#end_time').val());
                    /*formData.append('event_venue_id', $('#event_venue_id').val());*/
                    formData.append('participants', $('#participants').val());
                    formData.append('venue_types', $('#venue_types').val());

                    jQuery.each(jQuery('#event_image_url')[0].files, function (i, file) {
                        formData.append('event_image_url', file);
                    });

                    jQuery.each(jQuery('#invite_image_url')[0].files, function (i, file) {
                        formData.append('invite_image_url', file);
                    });

                    console.log(formData);

                    $.ajax({
                        url: "/full_calendar/create",
                        processData: false,
                        contentType: false,
                        type: "POST",
                        data: formData,
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            console.log("success", response);
                            /*alert(response.message);*/
                            $('#save-event').notify(response.message, "success");
                            setTimeout(function(){
                                /*window.location.reload();*/
                                window.location.href = '/full_calendar/events';
                                $("#modal1").close();
                            }, 3000);

                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                            $("#modal1").close();
                        }


                    });
                });
            });
        </script>
    @endpush
@endsection
