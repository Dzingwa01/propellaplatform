@extends('layouts.app')

@section('content')
    <br>
    <br>
    <br>
    <br>

    <div class="container" style="height: 850px; width: 1000px;">
        <div class="row">
            <a class="waves-effect waves-light btn" href="/show-events"><i class="material-icons left">remove_red_eye</i>View Events</a>
        </div>
        <div class="row">

            {!! $calendar->calendar() !!}
            {!! $calendar->script() !!}
        </div>
    </div>

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    @push('custom-scripts')
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

        <script>
            $(document).ready(function () {
                $(function () {
                    $('#calendar').fullCalendar({


                        editable: true,
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay,listMonth'
                        },
                        eventClick: function(info){
                            alert('Please click on the view events button to view ' + info.event.title);
                        }
                    });
                });
            });

            function alertMessage(info) {
                alert('Please click on "View Events" to view ' + info.title + "'s" + ' details');
            }
        </script>
    @endpush

@endsection
