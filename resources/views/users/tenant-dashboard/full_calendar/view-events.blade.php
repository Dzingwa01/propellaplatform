@extends('layouts.tenant-layout')

@section('content')
<br/>
<br>
<div class="container-fluid">

    <div class="row">
        <h5 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">CALENDAR EVENTS</h5>
        <a style="margin-left: 50px;" class="btn-floating btn-large waves-effect waves-light blue" href="/full_calendar/events"><i class="material-icons">arrow_back</i></a>
        {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
    </div>
    <div class="row" style="margin-left: 2em;margin-right: 2em;">
        <div class="col s12">
            <table class="table table-bordered" style="width: 100%!important;" id="calendar-events">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>All Day</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Color</th>
                    {{--<th>Description</th>--}}
                    <th>Type of Event</th>
                    <th>Status</th>
                    {{--<th>Venue</th>--}}
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
@push('custom-scripts')
    <script>
        $(document).ready(function (data) {
            $(function () {
                $('#calendar-events').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    type : 'get',
                    scrollX: 640,
                    ajax: '{{route('get-events')}}',
                    "columnDefs": [
                        /*{ "width": "0%", "targets": 7 }*/
                    ],
                    columns: [
                        {data: 'title', name: 'title'},
                        {data: 'start', name: 'start'},
                        {data: 'end', name: 'end'},
                        {data: 'all_day', name: 'all_day'},
                        {data: 'start_time', name: 'start_time'},
                        {data: 'end_time', name: 'end_time'},
                        {data: 'color', name: 'color'},
                        /*{data: 'description', name: 'description'},*/
                        {data: 'type', name: 'type'},
                        {data: 'eventStatus', name: 'eventStatus'},/*
                        {data: 'EventVenue.venue_name', name: 'EventVenue.venue_name'},*/
                        {data: 'action', name: 'action', orderable: true, searchable: true}
                    ]
                });
                $('select[name="calendar-events_length"]').css("display","inline");
            });
        });
        function confirm_delete_events(obj){
            var r = confirm("Are you sure want to delete this Event?!");
            if (r == true) {
                $.get('/event-delete-check/'+obj.id,function(data,status){
                    console.log('Data',data);
                    console.log('Status',status);
                    if(status=='success'){
                        alert(data.message);
                        window.location.reload();
                    }

                });
            } else {
                alert('Delete action cancelled');
            }

        }

    </script>
@endpush
@endsection
