@extends('layouts.app')
@section('content')
    <br/>
    <br/>
    <br/>
    <a style="margin-left: 50px;" class="btn-floating btn-large waves-effect waves-light blue"
       href="/full_calendar/events"><i class="material-icons">arrow_back</i></a>

    <h1>This is media</h1>
    <br>
    <br>
    <br>

    <div class="row">
        @foreach ($event->EventsMedia as $even)
            <div class="col s12 m3">
                <div class="card white hoverable">
                    <div class="circle">
                        <div class="card-content black-text " style="height: 280px;">

                            <div class="user-view" align="left">
                                <div class="row col m6" style="height: 50px; width: 50px;">

                                    <img class="materialboxed" id="event_media_preview" width="650" src={{'/storage/'.$even->event_media_url}}>
                                </div>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="input-field col m6">
                                        <input id="media_description" disabled value="{{isset($even->media_description)?$even->media_description:''}}" name="media_description" type="text"
                                               class="validate">
                                        {{--<label for="media_description">Description</label>--}}

                                    </div>
                                </div>
                            </div>
                              {{--<div class="card-action"style="background-color: lightgray;border-radius: 15px;width:100%;">

                              </div>--}}
                        </div>
                    </div>
                </div>
            </div>

            {{--===================================================================================================================================================--}}

        @endforeach
    </div>
@endsection
