@extends('layouts.app')

@section('content')
    <br>
    <div class="container" style="width: 100%;">
        <div class="row">
            <h4>{{$event->title}}</h4>
            @if(isset($event->EventsMedia->media_description))
                <p>{{$event->EventsMedia->media_description}}</p>
            @endif
        </div>
        <div class="row center" style="width: 100%;">
            @foreach($event_media_images as $event_media_image)
                @if(isset($event_media_image->event_media_url))
                    <div class="col s12 m6 l3" style="height: 250px; margin-bottom: 2em;">
                        <div style="border-style: hidden; height: 100%; width: 100%; ">
                            <img style="width:100%; height:100%;" class="materialboxed"
                                 src="/storage/{{isset($event_media_image->event_media_url)?$event_media_image->event_media_url:''}}">
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

    </div>
    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();
        });
    </script>
@endsection
