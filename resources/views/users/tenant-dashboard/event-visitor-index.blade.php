@extends('layouts.tenant-layout')
@section('content')
    <br>
    <br>
    <input hidden disabled id="event-id-input" value="{{$event->id}}">
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Event Visitors</h6>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">{{$event->start->toDateString()}}</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="event-visitor-and-incubatee-table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Date Registered</th>
                        <th>Attended</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            let event_id = $('#event-id-input').val();
            $(function () {
                $('#event-visitor-and-incubatee-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/get-event-visitors-and-incubatees/' + event_id,
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'date_registered', name: 'date_registered'},
                        {data: 'attended', name: 'attended'}
                    ]
                });
                $('select[name="event-visitor-and-incubatee-table_length"]').css("display","inline");
            });
        });
    </script>
@endsection
