@extends('layouts.admin-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <br>
    <br>
    <div class="section" id="createDesktop">
        <div class="card hoverable center" style="width: 800px; margin-left: 350px">
            <form class="col s12" id="add-booking" method="post">
                <br/>
                <br/>
                <h4 style="font-size: 2em;font-family: Arial;">Update Venue</h4>
                @csrf
                <div class="row" style="margin-left: 2em; margin-right: 2em;">
                    <div class="input-field col m6">
                        <input type="text" id="event_id" hidden value="{{$propellaVenue->id}}">
                        <input id="venue_name" value="{{$propellaVenue->venue_name}}" type="text" class="validate">
                        <label for="venue_name">Propella Venue Name</label>
                    </div>
                    <div class="input-field col m6">
                        <input type="number" id="number_of_seats" value="{{$propellaVenue->number_of_seats}}" name="number_of_seats"
                               min="00" max="100">
                        <label for="number_of_seats">Number of Seates</label>
                    </div>
                </div>
                <div class="row" style="margin-left: 2em; margin-right: 2em;">

                    <a href="#!" class="modal-close waves-effect waves-green btn">Cancel<i class="material-icons right">close</i>
                    </a>

                    <button class="btn waves-effect waves-light" style="margin-left:300px;" id="save-user" name="action">
                        Submit
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
            <br/>
            <br/>
        </div>
    </div>
    @push('custom-scripts')
        <script>
            $(document).ready(function () {
                $('.tooltipped').tooltip();
                $('select').formSelect();

                $('#add-booking').on('submit', function (e) {

                    e.preventDefault();
                    let formData = new FormData();
                    formData.append('venue_name', $('#venue_name').val());
                    formData.append('number_of_seats', $('#number_of_seats').val());
                    console.log(formData);

                    let url = '/update-edit-venue/' + '{{$propellaVenue->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                        success: function (response, a, b) {
                            console.log("success", response);
                            $('#save-user').notify(response.message, "success");
                            setTimeout(function(){
                                window.location.href = '{{url('/get-venues')}}';
                            }, 3000);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection

