@extends('layouts.tenant-layout')

@section('content')
    <br>
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Venue Bookings Time Slots</h6>
        </div>
        <input id="venue-id-input" value="{{$venue->id}}" hidden disabled>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="venue-table">
                    <thead>
                    <tr>
                        <th>Venue Name</th>
                        <th>Date</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Booking Person</th>
                        <th>Booking Reason</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Blog" onclick="bookVenue()">
                <i class="large material-icons">add</i>
            </a>

        </div>

    </div>
    @push('custom-scripts')
        <script>
            let venue_id = $('#venue-id-input').val();

            $(document).ready(function (data) {
                $(function () {
                    $('#venue-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '/get-venue-bookings/' + venue_id,
                        columns: [
                            {data: 'venue_name', name: 'venue_name'},
                            {data: 'venue_date', name: 'venue_date'},
                            {data: 'venue_start_time', name: 'venue_start_time'},
                            {data: 'venue_end_time', name: 'venue_end_time'},
                            {data: 'booking_person', name: 'booking_person'},
                            {data: 'booking_reason', name: 'booking_reason'}
                        ]
                    });
                    $('select[name="venue-table_length"]').css("display","inline");
                });
            });

            function bookVenue(){
                window.location.href = '/book-venue/' + venue_id;
            }
        </script>
    @endpush
@endsection
