@extends('layouts.admin-layout')

@section('content')

    <br>
    <br>
    <div class="card" style="width: 1000px;margin: 0 auto">
        <br>
        <div class="row" style="margin-left: 6em; margin-right: 6em;">
            <input hidden disabled id="venture-id-input" value="{{$venture->id}}">

            <p style="font-size: 2em"><b>Incubatee Evaluation of Mentor</b></p>
            <p><b>Mentor</b> : {{$venture->company_name}}</p>
            <p><b>Mentor</b> : </p>
            <p><b>Date</b></p>
            <br>
            <ol>
                @foreach ($question as $question)
                    <li value="{{$question->question_number}}">  {{ $question->question_text}} </li>
                    @if($question->question_type == "1_5")
                        <div class="input-field">
                            <input type="number" min="0" max="5" id="{{$question->id}}"
                                   class="materialize-textarea answer-text-desktop"
                                   style="width:20%">
                        </div>
                    @elseif($question->question_type == "Multiple")
                        @foreach($question->shadowBoardQuestionSubQuestionText as $sub)
                            <p>
                                <label>
                                    <input id="{{$question->id}}" class="answer-text-desktop" type="checkbox"/>
                                    <span>{{$sub->question_sub_text}}</span>
                                </label>
                            </p>
                        @endforeach

                    @elseif($question->question_type == "Yes_No")
                        <p>
                            <label>
                                <input id="{{$question->id}}" class="answer-text-desktop" type="checkbox"/>
                                <span>Yes</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input id="{{$question->id}}" class="answer-text-desktop" type="checkbox"/>
                                <span>No</span>
                            </label>
                        </p>

                    @elseif($question->question_type == "text")
                        <div class="input-field ">
                            <input id="{{$question->id}}" type="text" class="validate answer-text-desktop" style="width:20%">
                        </div>
                    @elseif($question->question_type == "feedback_text")

                        <div class="input-field ">
                            <input id="{{$question->id}}" type="text" class="validate answer-text-desktop" style="width:20%">
                        </div>

                    @endif
                @endforeach

            </ol>
            <br>

            <div class="col l6 m5 s12">
                <button class="btn waves-effect waves-light" id="answers-store"> Save <i
                        class="material-icons right">send</i></button>
            </div>
        </div>
        <br>

    </div>

    <script>

        $(document).ready(function () {
            $('.tooltipped').tooltip();


            $('#answers-store').on('click', function () {

                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#venture-id-input').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        shadow_board_question_id: this.id,
                        venture_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));

                $.ajax({
                    url: "/shadowVentureQuestionAnswer",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        alert("Answers saved successfully");

                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>


@Endsection
