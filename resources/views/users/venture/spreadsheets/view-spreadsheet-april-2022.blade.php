@extends('layouts.admin-layout')
@section('content')

    <div class="row center-align" style="margin-top: 10vh">
        <div class="row center">
            <h5><b>VENTURE M + E</b></h5>
        </div>
        <div class="col s12">
            @if(isset($venture_upload->incubatee_venture_spreadsheet))
                @foreach($venture_upload->incubatee_venture_spreadsheet as $spreadsheet)
                    @if($spreadsheet->month == '2022-04')
                        <div class="card col s4" style="background-color: lightgray">
                            <br>
                            <a href="{{'/storage/'.$spreadsheet->spreadsheet_upload}}" target="_blank">Download M + E - {{$spreadsheet->month}}</a>                            <p>Date submitted : {{$spreadsheet->date_submitted}}</p>
                            <p>Date submitted : {{$spreadsheet->date_submitted}}</p>
                        </div>
                        <div class="col s0.1"></div>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
@endsection
