@extends('layouts.admin-layout')
@section('content')

    <div class="row" style="margin-left: 2em;margin-right: 2em;margin-top: 10vh">
        <h5 class="center-align"><b>M + E NOVEMBER 2021</b></h5>
        <div class="col s12">
            <table class="table table-bordered" style="width: 100%!important;" id="venture-table">
                <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Cohort</th>
                    <th>Stage</th>
                    <th>Email</th>
                    <th>Contact number</th>
                    <th>Date submitted</th>
                    <th>Spreadsheet</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(function () {
                $('#venture-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    type: 'get',
                    scrollX: 640,
                    stateSave: true,
                    pageLength: 100,
                    ajax: '{{route('get-venture-nov-2021')}}',
                    columns: [
                        {data: 'company_name'},
                        {data: 'cohort'},
                        {data: 'stage'},
                        {data: 'email'},
                        {data: 'contact_number'},
                        {data: 'nov_date_submitted'},
                        {data: 'nov_spreadsheet_uploaded'},
                        {data: 'action', name: 'action', orderable: true, searchable: true}
                    ]
                });
                $('select[name="venture-table_length"]').css("display", "inline");
            });

        });

    </script>


@endsection
