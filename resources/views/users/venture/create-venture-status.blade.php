@extends('layouts.admin-layout')

@section('content')
    <br><br>
{{--    {{ Breadcrumbs::render('administrator-add-status')}}--}}
    <div class="section">
        <div class="row center">
            <h4>Add a venture status</h4>
            <input type="text" style="width:50%;" id="venture-status-name-input" placeholder="Enter status here">
            <br>
            <button class="btn blue" id="submit-venture-status-button">Save</button>
        </div>
    </div>

    <script>
        //js function to store entered venture status
        $(document).ready(function(){
            $('#submit-venture-status-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('status_name', $('#venture-status-name-input').val());

                let url = 'store-venture-status';

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    //js to receive system feedback
                    success: function (response, a, b) {
                        $("#venture-status-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = 'venture-status';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
