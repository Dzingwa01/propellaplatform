@extends('layouts.admin-layout')

@section('content')

    <div class="card" style="margin-top: 10vh;width: 1000px;margin-left: 200px">
        <input value="{{$venture->id}}" id="venture_id" hidden>
        <br>
        <h4 class="center-align"> Update a client contract </h4>
        <br>
        <div class="row" style="margin-left: 2em;margin-right: 2em">
            <div class="file-field input-field col s6" style="bottom:0px!important;">
                <div class="btn">
                    <span>Client Contract</span>
                    <input id="client_contract_url" type="file" name="client_contract_url">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path" value="{{$venture->client_contract_url?$venture->client_contract_url:''}}"
                           type="text">
                </div>
            </div>
            <div class="input-field col l6">
                <input type="date" id="date_of_signature"
                       value="{{$venture->date_of_signature}}">
                <label for="date_of_signature">Date of Signature (yyyy-mm-dd)</label>
            </div>
        </div>
        <div class="row" style="margin-left: 2em">
            <div class="input-field col m6">
                <select id="contract_stage">
                    <option value="contract_stage" disabled selected>Choose Stage</option>
                    <option value="Stage 1" {{$venture->contract_stage =='Stage 1'?'selected':''}}>Stage 1</option>
                    <option value="Stage 2" {{$venture->contract_stage =='Stage 2'?'selected':''}}>Stage 2</option>
                    <option value="Stage 3" {{$venture->contract_stage =='Stage 3'?'selected':''}}>Stage 3</option>
                    <option value="Stage 4" {{$venture->contract_stage =='Stage 4'?'selected':''}}>Stage 4</option>
                    <option value="Stage 5" {{$venture->contract_stage =='Stage 5'?'selected':''}}>Stage 5</option>
                    <option value="Stage 6" {{$venture->contract_stage =='Stage 6'?'selected':''}}>Stage 6</option>
                </select>
                <label>Stage</label>
            </div>
        </div>
        <div class="col s6 save-contact-log" style="margin-left: 400px">
            <a class="waves-effect waves-light btn" id="submit-contract-upload">Save</a>
        </div>
        <br>
    </div>

    @push('custom-scripts')
        <script>
            $(document).ready(function () {
                $('select').formSelect();

                $('#submit-contract-upload').on('click', function () {
                    let venture_id = $('#venture_id').val();

                    let formData = new FormData();
                    formData.append('date_of_signature', $('#date_of_signature').val());
                    formData.append('contract_stage', $('#contract_stage').val());

                    jQuery.each(jQuery('#client_contract_url')[0].files, function (i, file) {
                        formData.append('client_contract_url', file);
                    });

                    let url = '/update-venture-client-contract-stage/' + venture_id;

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            $('#submit-contract-upload').notify(response.message, "success");

                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        },
                    });
                });


            });
        </script>
    @endpush
@endsection
