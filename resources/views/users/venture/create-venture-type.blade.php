@extends('layouts.admin-layout')

@section('content')
    <br><br>
    <div class="section">
        <div class="row center">
            <h4>Add a venture type</h4>
            <input type="text" style="width:50%;" id="venture-type-name-input" placeholder="Enter venture type here">
            <br>
            <button class="btn blue" id="submit-venture-type-button">Save</button>
        </div>
    </div>

    <script>
        //js function to store entered venture type
        $(document).ready(function(){
            $('#submit-venture-type-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('type-name', $('#venture-type-name-input').val());

                let url = 'store-venture-type';

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    //js to receive system feedback
                    success: function (response, a, b) {
                        $("#venture-type-name-input").notify(response.message, "success");

                        setTimeout(function(){
                             window.location.href = 'venture-type';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
