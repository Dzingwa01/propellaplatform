@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
{{--    {{ Breadcrumbs::render('admin-venture-status')}}--}}
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Venture Statuses</h6>
        </div>
        <br>

        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="venture-statuses-table">
                    <thead>
                    <tr>
                        <th>Status Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
               data-tooltip="Add New Venture Status" href="{{url('create-venture-status')}}">
                <i class="large material-icons">add</i>
            </a>
        </div>

    </div>
    @push('custom-scripts')

        <script type="text/javascript"
                src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>

        <script type="text/javascript" language="javascript"
                src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>

        <script>
            //table displaying all records
            $(document).ready(function (data) {
                $(function () {
                    $('#venture-statuses-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type: 'get',
                        scrollX: 640,
                        ajax: '{{route('get-venture-status')}}',
                        columns: [
                            {data: 'status_name', name: 'status_name'},
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="venture-statuses-table_length"]').css("display", "inline");
                });
            });

            //delete selected record
            function confirm_delete_venture_status(obj) {
                var r = confirm("Are you sure want to delete this category?");
                if (r) {
                    $.get('/venture-status-delete-check/' + obj.id, function (data, status) {
                        if (status === 'success') {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Delete action cancelled');
                }
            }

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
