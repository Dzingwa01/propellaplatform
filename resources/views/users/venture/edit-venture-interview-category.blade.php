@extends('layouts.admin-layout')

@section('content')
    <input id="user-id-input" disabled hidden value="{{$venturePanelInterviewCategory->id}}">
    <div class="section">
        <div class="row center">
            <input hidden disabled id="venture-category-id-input" value="{{$venturePanelInterviewCategory->id}}">
            <h4>Edit panel interview category</h4>
            <input type="text" style="width:50%;" id="venture-category-name-input" value="{{$venturePanelInterviewCategory->category_name}}">
            <br>
            <button class="btn blue" id="update-venture-category-button">Update</button>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#update-venture-category-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('category_name', $('#venture-category-name-input').val());

                let venture_category_id = $('#venture-category-id-input').val();
                let url = '/update-panel-interview-category/' + venture_category_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#venture-category-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/venture-panel-interview-category-index';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
        function confirm_delete_users(obj){
            var r = confirm("Are you sure want to delete this user?");
            if (r == true) {
                $.get('/role/delete/'+obj.id,function(data,status){
                    console.log('Data',data);
                    console.log('Status',status);
                    if(status=='success'){
                        alert(data.message);
                        window.location.reload();
                    }

                });
            } else {
                alert('Delete action cancelled');
            }
        }
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
