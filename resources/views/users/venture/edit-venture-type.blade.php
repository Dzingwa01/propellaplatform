@extends('layouts.admin-layout')

@section('content')
    <input id="user-id-input" disabled hidden value="{{$ventureType->id}}">
    <br><br>
{{--    {{ Breadcrumbs::render('admin-edit-type',$ventureType)}}--}}
    <div class="section">
        <div class="row center">
            <input hidden disabled id="venture-type-id-input" value="{{$ventureType->id}}">
            <h4>Edit venture type</h4>
            <input type="text" style="width:50%;" id="venture-type-name-input" value="{{$ventureType->type_name}}">
            <br>
            <button class="btn blue" id="update-venture-type-button">Update</button>
        </div>
    </div>

    <script>
        //js to update selected venture type
        $(document).ready(function(){
            $('#update-venture-type-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('type_name', $('#venture-type-name-input').val());

                let venture_type_id = $('#venture-type-id-input').val();
                let url = '/update-venture-type/' + venture_type_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    //js to receive system feedback
                    success: function (response, a, b) {
                        $("#venture-type-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/venture-type';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
