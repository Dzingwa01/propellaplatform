@extends('layouts.admin-layout')

@section('content')
    <br>    <br>{{ Breadcrumbs::render('ind-venture')}}
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Industrial Venture</h6>
=        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="alumni-table">
                    <thead>
                    <tr>
                        <th>Venture Name</th>
                        <th>Contact Number</th>
                        <th>Hub</th>
                        <th>Stage</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
    @push('custom-scripts')

        <script>

            $(document).ready(function (data) {
                $(function () {
                    $('#alumni-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '{{route('get-IND')}}',
                        columns: [
                            {data: 'company_name', name: 'company_name'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'hub', name: 'hub'},
                            {data: 'stage', name: 'stage'},
                            {data: 'status', name: 'status'},
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="alumni-table_length"]').css("display","inline");
                });
            });

            function confirm_delete_venture(obj) {
                var r = confirm("Are you sure want to delete this application!");
                console.log("Check", r);
                if (r) {
                    $.get('/Venture-delete-check/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status == 'success') {
                            alert(data.message);
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }
            }

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
