@extends('layouts.admin-layout')

@section('content')


    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Incubatees</h6>
            {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                {{--<input value="{{$venture->id}}" id="venture_id" hidden>--}}
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        {{--<th>Venture Name</th>--}}
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        {{--                        <th>Hub</th>--}}
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Incubatee" href="{{url('create-incubatee')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>
        {{--<button class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example" type="button"><span>Excel</span></button>--}}
    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $(function () {
                    $('#users-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-incubatees')}}',

                        columns: [
                            /*{data:'start_up.startup_name',name:'start_up.startup_name'},*/
                            {data: 'user.name', name: 'user.name'},
                            {data: 'user.surname', name: 'user.surname'},
                            {data: 'user.email', name: 'user.email'},
                            {data: 'user.contact_number', name: 'user.contact_number'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ],

                    });
                    $('select[name="users-table_length"]').css("display","inline");
                });

            });


        </script>
    @endpush
@endsection
