@extends('layouts.admin-layout')

@section('content')

    <div class="row">
        <div class="col s10">
            <div class="card " style="margin-top: 10vh;margin-left: 300px">
                <br>
                <p class="center-align"><b>UPLOAD VIDEO</b></p>
                <div class="row" style="margin-left: 2em;margin-right: 2em">
                    <div class="col m6">
                        <div class="input-field" style="bottom:0px!important;">
                            <input id="video" type="text">
                            <label for="video">Video url</label>
                        </div>
                    </div>
                    <div class="input-field col m6">
                        <input id="video_title" type="text" class="validate">
                        <label for="video_title">Video Title</label>
                    </div>
                </div>
                <div class="row" style="margin-left: 2em;margin-right: 2em">
                    <div class="col m6">
                        <div class="input-field" style="bottom:0px!important;">
                            <input id="date" type="date">
                            <label for="date">Date</label>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left: 400px">
                    <div class="col s4">
                        <a class="waves-effect waves-light btn section" id="video-upload">Save</a>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#video-upload').on('click', function () {

                let formData = new FormData();
                formData.append('video', $('#video').val());
                formData.append('video_title', $('#video_title').val());
                formData.append('date', $('#date').val());


                let url = "{{route('store-video')}}";
                $.ajax({
                    url: url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#video-upload').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/video-index';
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });

    </script>
@endsection
