@extends('layouts.admin-layout')

@section('content')
    <link rel="stylesheet" href="/css/Users/edit.css"/>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

    </head>
<br>
    <br>
    <head>
        <!-- jQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    </head>
    {{ Breadcrumbs::render('edit-user')}}

    <!--Desktop-->
    <div clas="row" style="margin-top:3em;">
        <div class="col s12 card hoverable center ">
            <form class="" id="add-user-desktop" method="post">
                <br />
                <h2 style="font-size: 2em;font-family: Arial;">Edit User</h2>
                <br />
                <input id="user_id" value="{{$user->id}}" hidden>
                @csrf
                <div class="row" style="margin-left: 95px">
                    <div class="input-field col m5">
                        <input id="name" type="text" value="{{$user->name}}" class="validate">
                        <label for="name">Name</label>
                    </div>
                    <div class="input-field col m5">
                        <input id="surname" value="{{$user->surname}}" type="text" class="validate">
                        <label for="surname">Surname</label>
                    </div>
                </div>
                <div class="row"style="margin-left: 95px">
                    <div class="input-field col m5">
                        <input id="email" value="{{$user->email}}" type="email" class="validate">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col m5">
                        <input id="contact_number" value="{{$user->contact_number}}" type="tel" class="validate">
                        <label for="contact_number">Contact Number</label>
                    </div>
                </div>

                <div class="row"style="margin-left: 95px">
                    <div class="input-field col m5">
                        <input id="land_line" value="{{$user->land_line}}" type="tel" class="validate">
                        <label for="land_line">Land line</label>
                    </div>
                    <div class="input-field col m5">
                        <select id="role_id">
                            <option value="{{isset($user->roles[0]->id) ? $user->roles[0]->id : null}}">{{isset($user->roles[0]->display_name) ? $user->roles[0]->display_name : 'Select role'}}</option>
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->display_name}}</option>
                            @endforeach
                        </select>
                        <p style="color:red;">Please make sure you know exactly which role this user should have !!</p>
                    </div>
                    <div class="input-field col m5">
                        <input id="address_one" type="text" value="{{$user->address_one}}" class="validate">
                        <label for="address_one">Address</label>
                    </div>
                </div>

                <button id="success">Submit</button>
                <br />
                <br />

            </form>
        </div>
    </div>

    <style>

        button {
            background-color: cadetblue;
            color: whitesmoke;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            font-size: 18px;
            font-weight: 500;
            border-radius: 7px;
            padding: 15px 35px;
            cursor: pointer;
            white-space: nowrap;
            margin: 10px;
        }
        img {
            width: 200px;
        }
        input[type="text"] {
            padding: 12px 20px;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 10px;
            box-sizing: border-box;
        }
        h1 {
            border-bottom: solid 2px grey;
        }
        #success {
            background: green;
        }
        #error {
            background: red;
        }
        #warning {
            background: coral;
        }
        #info {
            background: cornflowerblue;
        }
        #question {
            background: grey;
        }
        th{
            text-transform: uppercase!important;
        }
          nav {
              margin-bottom: 0;
              background-color: grey;
          }
    </style>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('.tooltipped').tooltip();
                $('select').formSelect();


                $(document).on('click', '#success', function(e) {

                    e.preventDefault();
                    let formData = new FormData();
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('email', $('#email').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('land_line', $('#land_line').val());
                    formData.append('role_id', $('#role_id').val());
                    formData.append('address_one', $('#address_one').val());
                    console.log(formData);

                    let url = '/update-edit-user/' + $('#user_id').val();
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            console.log("error", message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);


                        }
                    });
                    swal(
                        'Success',
                        'User edited <b style="color:green;">Successfully</b>',
                        'success',
                        setTimeout(function(){
                            window.location.href = '{{url('/users-index')}}';
                        }, 3000)
                    )
                });
            });

            // Alert With Custom Icon and Background Image
            $(document).on('click', '#icon', function(event) {
                swal({
                    title: 'Custom icon!',
                    text: 'Alert with a custom image.',
                    imageUrl: 'https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg',
                    imageWidth: 200,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                    animation: false
                })
            });

            $(document).on('click', '#image', function(event) {
                swal({
                    title: 'Custom background image, width and padding.',
                    width: 700,
                    padding: 150,
                    background: '#fff url(https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg)'
                })
            });
            function goBack() {
                window.history.back();
            }
        </script>
    @endpush
@endsection
