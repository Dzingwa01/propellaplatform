@extends('layouts.admin-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <!--Desktop-->  <br>
    <br>
    {{ Breadcrumbs::render('add-user')}}
    <head>
        <!-- jQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    </head>

    <body>
    </body>

    <div class="container">
        <div class="row">
            <div class="row card hoverable">
                <form class="col s12" id="add-user" method="post" name="pleasevalidateme" style="margin-top:1em;">
                    <br/>
                    <br/>
                    <h4 align="center" style="font-size: 2em;font-family: Arial;">Add new user</h4>
                    @csrf
                    <div class="row" style="margin-left: 95px">
                        <div class="input-field col m5">
                            <input id="name" type="text" class="validate">
                            <label for="name">Name</label>
                        </div>
                        <div class="input-field col m5">
                            <input id="surname" type="text" class="validate">
                            <label for="surname">Surname</label>
                        </div>
                    </div>
                    <div class="row" style=" margin-left: 95px">
                        <div class="input-field col m5">
                            <input id="email" type="email" class="validate">
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field col m5">
                            <input id="contact_number" type="number" class="validate" min="10" maxlength="10">
                            <label for="contact_number">Contact Number</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 95px">
                        <div class="input-field col m5">
                            <select id="role_id">
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->display_name}}</option>
                                @endforeach
                            </select>
                            <label>System Role</label>
                        </div>
                        <div class="input-field col m5">
                            <textarea id="address_one" class="materialize-textarea"></textarea>
                            <label for="address_one">Address</label>
                        </div>
                    </div>
                    <button style="margin-left: 440px" id="success">Submit</button>
                </form>
                <br/>
                <br/>
            </div>
        </div>

    </div>

    <style>
        button {
            background-color: cadetblue;
            color: whitesmoke;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            font-size: 18px;
            font-weight: 500;
            border-radius: 7px;
            padding: 15px 35px;
            cursor: pointer;
            white-space: nowrap;
            margin: 10px;
        }
        img {
            width: 200px;
        }
        input[type="text"] {
            padding: 12px 20px;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 10px;
            box-sizing: border-box;
        }
        h1 {
            border-bottom: solid 2px grey;
        }
        #success {
            background: green;
        }
        #error {
            background: red;
        }
        #warning {
            background: coral;
        }
        #info {
            background: cornflowerblue;
        }
        #question {
            background: grey;
        }
        th{
            text-transform: uppercase!important;
        }
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>



    @push('custom-scripts')

        <script>
            $('.tooltipped').tooltip();
            $('select').formSelect();

            // Alert Modal Type
            $(document).on('click', '#success', function(e) {
                e.preventDefault();
                let formData = new FormData();
                formData.append('name', $('#name').val());
                formData.append('surname', $('#surname').val());
                formData.append('email', $('#email').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('role_id', $('#role_id').val());
                formData.append('address_one', $('#address_one').val());
                console.log(formData);

                $.ajax({
                    url: '{{route('add-users')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},


                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
                swal(
                    'Success',
                    'User saved <b style="color:green;">Successfully</b>',
                    'success',
                setTimeout(function(){
                    window.location.href = '{{url('/users-index')}}';
                }, 3000)
                )
            });


            // Alert With Custom Icon and Background Image
            $(document).on('click', '#icon', function(event) {
                swal({
                    title: 'Custom icon!',
                    text: 'Alert with a custom image.',
                    imageUrl: 'https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg',
                    imageWidth: 200,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                    animation: false
                })
            });

            $(document).on('click', '#image', function(event) {
                swal({
                    title: 'Custom background image, width and padding.',
                    width: 700,
                    padding: 150,
                    background: '#fff url(https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg)'
                })
            });



            function ValidateEmail() {
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                var email = document.forms["pleasevalidateme"]["email"].value;
                if (email.match(mailformat)) {
                    return true;
                } else {
                    alert("You have entered an invalid email address!");
                    return false;
                }
            }

            function goBack() {
                window.history.back();
            }
        </script>
    @endpush
@endsection
