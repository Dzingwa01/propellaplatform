@extends('layouts.admin-layout')

@section('content')


    <div class="container-fluid">
        <br>
        <br> <br>
        {{ Breadcrumbs::render('index')}}
        <div class="row center">
            <a class="btn btn-success purple"
               onclick="exportUsers()"
            >Export Users
            </a>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>System Role</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New User" href="{{url('create')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>


    </div>

    @push('custom-scripts')

        <script>
            //
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#users-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        stateSave: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-users')}}',
                        columns: [
                            {data: 'name'},
                            {data: 'surname'},
                            {data: 'email'},
                            {data: 'contact_number'},
                            {data: 'roles[0].name'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]

                    });
                    $('select[name="users-table_length"]').css("display", "inline");
                });
            });

            function confirm_delete_users(obj){
                var r = confirm("Are you sure want to delete this user?");
                if (r == true) {
                    $.get('/user-del/'+obj.id,function(data,status){
                        console.log('Data',data);
                        console.log('Status',status);
                        if(status=='success'){
                            alert(data.message);
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }
            }

            function exportUsers(){
                window.location.href = '/export-users';
            }
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
