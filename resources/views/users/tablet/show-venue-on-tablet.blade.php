@extends('layouts.tablet-layout')

@section('content')
    <br>
{{--    <div class="container-fluid">--}}
{{--        <div class="row">--}}
{{--            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Venue Bookings On Tablet</h6>--}}
{{--        </div>--}}
{{--        <div class="row" style="margin-left: 2em;margin-right: 2em;">--}}
{{--            <div class="col s12">--}}
{{--                <table class="table table-bordered" style="width: 100%!important;" id="booking-table">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th>Date</th>--}}
{{--                        <th>Start Time</th>--}}
{{--                        <th>End Time</th>--}}
{{--                        <th>Booking Person</th>--}}
{{--                        <th>Booking Reason</th>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                </table>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Blog" onclick="bookVenue()">
            <i class="large material-icons">add</i>
        </a>

    </div>
    <div class="section " style="margin-right: 10px; margin-left: 10px;">
        <div class="row timeline" style="margin-left: 5em;margin-right: 5em;">
            @for($i = 0; $i<count($venue); $i++)
                @if($i === 0)
                    <div class="card">
                        <div class="text-block">
                            <h4 class="date">{{$venue[$i]->booking_person}}</h4>
                            <h6 class="date">{{$venue[$i]->booking_reason}}</h6>
                            <h6 class="date">Date : {{$venue[$i]->venue_date}}</h6>
                            <h6 class="date">Start Time: {{$venue[$i]->venue_start_time}}</h6>
                            <h6 class="date">End Time : {{$venue[$i]->venue_end_time}}</h6>
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="text-block">
                            <h4 class="date">{{$venue[$i]->booking_person}}</h4>
                            <h6 class="date">{{$venue[$i]->venue_date}}</h6>
                            <h6 class="date">{{$venue[$i]->venue_start_time}}</h6>
                            <h6 class="date">{{$venue[$i]->venue_end_time}}</h6>
                            <h6 class="date">{{$venue[$i]->booking_reason}}</h6>
                        </div>
                    </div>

                @endif
            @endfor
        </div>
    </div>


    @push('custom-scripts')
        <script>

            $(document).ready(function (data) {
                $(function () {
                    $('#booking-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type: 'get',
                        scrollX: 640,
                        ajax: '/get-venue-all-bookings-on-tablet',
                        columns: [
                            {data: 'venue_date', name: 'venue_date'},
                            {data: 'venue_start_time', name: 'venue_start_time'},
                            {data: 'venue_end_time', name: 'venue_end_time'},
                            {data: 'booking_person', name: 'booking_person'},
                            {data: 'booking_reason', name: 'booking_reason'},
                        ]
                    });
                    $('select[name="booking-table_length"]').css("display", "inline");
                });
            });

        </script>
    @endpush
@endsection

