@extends('layouts.tablet-layout')

@section('content')
    <div class="">
        <div class="row center-align">
            <div class="col s4">
               <h4>COVID-19 FORMS</h4>
            </div>
            <div class="col s4">
                <img src="images/Propella_Logo (1).jpg">
            </div>
        </div>
        <div class="row" style="margin-left: 2em;margin-top: 5vh">
            <div class="card col s8">
                <p><b>Propella users covid forms for  {{$today}}</b></p>
                @foreach($covidStff_array as $staff)
                    @if($staff->staff_covid_symptoms_one != 'None of the above' or $staff->staff_covid_symptoms_two != 'None of the above'
                        or $staff->staff_temperature > 38)
                        <p style="color: red">Name : {{$staff->name}} {{$staff->surname}}</p>
                    @else
                        <p style="color: green">Name : {{$staff->name}} {{$staff->surname}}</p>
                    @endif
                @endforeach
            </div>
        </div>

        <div class="row" style="margin-left: 2em;margin-top: 5vh">
            <div class="card col s8">
                <p><b>Company employees covid forms for {{$today}}</b></p>
                @foreach($users_array as $employees)
                    @if($employees->employee_covid_symptoms_one != 'None of the above' or $employees->employee_covid_symptoms_two != 'None of the above'
                        or $employees->employee_temperature > 38)
                        <p style="color: red">Name : {{$employees->name}} {{$employees->surname}}</p>
                    @else
                        <p style="color: green">Name : {{$employees->name}} {{$employees->surname}}</p>
                    @endif
                @endforeach
            </div>
        </div>

        <div class="row" style="margin-left: 2em;margin-top: 5vh">
            <div class="card col s8">
                <p><b>Visitors covid forms for {{$today}}</b></p>
                @foreach($visitor_array as $visitor)
                    @if($visitor->covid_symptoms_one != 'None of the above' or $staff->covid_symptoms_two != 'None of the above'
                        or $visitor->temperature > 38)
                        <p style="color: red">Name : {{$visitor->first_name}} {{$visitor->last_name}}</p>
                    @else
                        <p style="color: green">Name : {{$visitor->first_name}} {{$visitor->last_name}}</p>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <script>
        function View(obj){
            let user_id = obj.id;
            window.location.href = '/admin-approve-user/' + user_id;
        }
        window.location.reload();
    </script>

@endsection
