@extends('layouts.tablet-layout')

@section('content')
    <br>
    <link rel="stylesheet" type="text/css" href="/css/Tablet/tablet.css"/>
    <br>

    <input id="venue-id-input" value="{{$venue->id}}" hidden disabled>
    <input id="venue-name-input" value="{{$venue->venue_name}}" hidden disabled>
{{--    <img style="margin-left: 750px" src="/images/Propella_Logo.jpg">--}}



        <div class="row" id="booking-row" style="margin-left: 1em;margin-right: 1em" hidden></div>

        <div class="container-fluid" id="datatable-row" hidden>
            <div class="row">
                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Venue Bookings Time Slots</h6>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="col s12">
                    <table class="table table-bordered" style="width: 100%!important;" id="venue-table">
                        <thead>
                        <tr>
                            <th>Venue Name</th>
                            <th>Date</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Booking Person</th>
                            <th>Booking Reason</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


    <style>
        .vl {
            border-left: 6px solid white;
            height: 300px;
            position: absolute;
            left: 50%;
            margin-left: -3px;
            top: 5vh;
        }
    </style>


    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Blog" onclick="bookVenue()">
            <i class="large material-icons">add</i>
        </a>
    </div>
    @push('custom-scripts')
        <script>
            let venue_id = $('#venue-id-input').val();

            $(document).ready(function (data) {
                $(function(){
                    $('#venue-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type: 'get',
                        scrollX: 640,
                        ajax: '/get-venue-bookings/' + venue_id,
                        columns: [
                            {data: 'venue_name', name: 'venue_name'},
                            {data: 'venue_date', name: 'venue_date'},
                            {data: 'venue_start_time', name: 'venue_start_time'},
                            {data: 'venue_end_time', name: 'venue_end_time'},
                            {data: 'booking_person', name: 'booking_person'},
                            {data: 'booking_reason', name: 'booking_reason'}
                        ],

                    });
                    $('select[name="venue-table_length"]').css("display", "inline");
                });
                checkBooking();
            });

            function bookVenue(){
                window.location.href = '/book-venue/' + venue_id;
            }

            function checkBooking(){
                $.ajax({
                    url: '/check-venue-bookings/' + venue_id,
                    type: 'GET',
                    success: function(data) {
                        if(data.active_booking !== null){
                            let venue_name = $('#venue-name-input').val();

                            $('#booking-row').html("");

                            $('#booking-row').append(
                                ' <div class="card col s12" style="border-radius: 20px;height: 450px;background-color: black;margin-top: 13vh"> '
                                +
                                '<div class="text-block col s6" style="margin-top: 4vh">'
                                +
                                '<h6 class="" style="font-size: 3em;color: white;margin-top: 4vh;margin-left: 2em"><b> ' + venue_name + ' </b></h6>'
                                +
                                '<iframe style="margin-left: 9em;margin-top: 4vh" src="https://free.timeanddate.com/clock/i6z2eg3z/n1485/szw110/szh110/hocddd/hbw0/hfc000/cf100/hgr0/fav0/fiv0/mqcfff/mql15/mqw8/mqd100/mhcfff/mhl15/mhw4/mhd100/mmv0/hhcff9/hmcff9" frameborder="0" width="130" height="130"></iframe>'
                                +
                                '</div>'
                                +
                                '<div class="vl"></div>'
                                +
                                '<div class="text-block col s6 " style="margin-top: 11vh" >'
                                +
                                '<h5 class="" style="color: white;margin-left: 2em">'+ data.active_booking.venue_date +'</h5>'
                                +
                                '<h5 class="" style="color: white;margin-left: 2em"><b>'+ data.active_booking.venue_start_time +'  -  '+ data.active_booking.venue_end_time +'</b></h5>'
                                +
                                '<h5 class="" style="color: white;margin-left: 2em">'+ data.active_booking.booking_person +'</h5>'
                                +
                                '<h5 class="" style="color: white;margin-left: 2em"><b>'+ data.active_booking.booking_reason +'</b></h5>'
                                +
                                '</div>'
                                +
                                '</div>'
                            );
                            $('#datatable-row').hide();

                            $('#booking-row').show();

                            setTimeout(function () {
                                window.location.reload();
                            }, 60000);

                        } else if(data.datatable  === true){
                            $('#booking-row').hide();
                            $('#datatable-row').show();

                            setTimeout(function () {
                                checkBooking();
                            }, 60000);
                        }
                    }
                });
            }

        </script>
    @endpush
@endsection
