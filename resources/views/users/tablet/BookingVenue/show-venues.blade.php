@extends('layouts.tablet-layout')
@section('content')
    {{--<input id="user-id-input" disabled hidden value="{{$user->id}}">--}}
    <link rel="stylesheet" type="text/css" href="/css/Tablet/tablet.css"/>

    <div class="container" style="margin-top: 10vh">
        <div class="row">
            <div class="center">
                <h5 style="color: black"><b>BOOK A VENUE HERE</b></h5>
            </div>
        </div>

        <div class="row">
            <div class="center">
                @foreach($venues as $venue)
                    <div class="col s12 m4">
                        <div class="card white hoverable center" id="checkBooked" style="border-radius: 20px;">
                            <div class="circle">
                                <div class="card-content black-text" style="height: 100%;">
                                    <div class="user-view" align="center">
                                        {{--<img class="fullscreen materialboxed" style="width: 100%; height: 200px;" src="{{isset($event_list->event->event_image_url)?'/storage/'.$event_list->event->event_image_url:''}}">--}}
                                    </div>
                                </div>
                                <h6 class=""><b>{{$venue->venue_name}}</b></h6>
                                <h6 class="">{{$venue->number_of_seats}}</h6>
                            </div>
                            <br>
                            <br>
                            <div class="card-action" style="border-radius: 20px">
                                <div class="row booking" style="margin-left: 0.5em;">
                                    <a href="{{url('/venue-booking/'.$venue->id)}}" class="booking-button" style="color: orange;">Book A Venue
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>






    <div class="container">
        <div class="row">
            <div class="center">
                <h5 style="color: black" id="registered-header"><b>VENUES YOU HAVE BOOKED FOR</b></h5>
            </div>
        </div>

        <table style="width:100%;margin-left: 2em;margin-right: 2em">
            <tr>
                <th>Venue</th>
                <th>Venue Date</th>
                <th>Start time</th>
                <th>End time</th>
                <th>Booking Reason</th>
                <th>Update</th>
                <th>Cancel</th>

            </tr>
            @foreach($user_bookings as $user_booking)
                <tr>
                    <td>{{$user_booking->venue_name}}</td>
                    <td>{{$user_booking->venue_date}}</td>
                    <td>{{$user_booking->venue_start_time}}</td>
                    <td>{{$user_booking->venue_end_time}}</td>
                    <td>{{$user_booking->booking_reason}}</td>
                    <th><a class="booking-button modal-trigger"
                           href="{{url('/edit-venue-booking/'.$user_booking->id)}}"
                           style="color: orange;">Update Booking
                        </a>
                    </th>
                    <th><a id="{{$user_booking->id}}"
                           style="color: orange; cursor: pointer;" onclick="deleteBooking(this)">Cancel Booking
                        </a></th>
                </tr>
            @endforeach
        </table>
    </div>


    @push('custom-scripts')
        <script>

            function deleteBooking(obj) {
                var r = confirm("Are you sure want to delete this booking?");
                console.log("Check", r);
                if (r) {
                    $.get('/cancel-delete-check/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status == 'success') {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Delete action cancelled');
                }
            }


        </script>
    @endpush
@endsection
