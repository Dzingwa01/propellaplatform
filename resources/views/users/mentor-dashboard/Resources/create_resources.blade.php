@extends('layouts.mentor-layout')

@section('content')
   <head>
       <!-- include libraries(jQuery, bootstrap) -->
       <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
       <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
       <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

       <!-- include summernote css/js -->
       <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
       <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>
   </head>


    <!--Create Funding Resources-->
    <div class="card" style="width: 1080px;margin: 0 auto;top: 7vh">
        <br>
        <h2 class="center-align" style="font-size: 2em;">Add Resource</h2>
        <div class="row">
            <div class="input-field col s5" style="margin-left: 2em;margin-right: 2em">
                <input id="title" type="text">
                <label for="title">Tittle</label>
            </div>
            <div class="input-field col s5" style="margin-left: 2em;margin-right: 2em">
                <input id="closing_date" type="date">
                <label for="closing_date">Closing date</label>
            </div>
        </div>
        <div class="row" style="margin-right: 2em;margin-left: 2em;">
            <div class="file-field input-field col s5">
                <div class="btn" style="height: 50px;">
                    <span>Funding Resource Image</span>
                    <input type="file" id="image_url">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
            <div class="input-field col s5" style="margin-left: 1em;width: 400px">
                <select id="resource_category_id">
                    <option>Select Category</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                    @endforeach
                </select>
                <label>Category</label>
            </div>
        </div>
        <br>
        <div class="row" style="margin-left: 1000px;">
            <div class="col s4">
                <a class="waves-effect waves-light btn resource-submit-button">Save</a>
            </div>
        </div>
        <br>
    </div>

   <!--create summernote-->
   <div class="container">
       <div class="panel-body">
           <form action="{{route('storeResource')}}" method="POST" style="margin-top: 50px" id="summernote-form">
               <div class="form-group">
                   <textarea id="summernote" name="summernoteInput" class="summernote"></textarea>
               </div>

               <div class="form-group">
                   <button type="submit">Submit</button>
                   {!!csrf_field()!!}

               </div>
           </form>
       </div>
   </div>


   <script>
       let file_array =[];
       let file_array_count = 0;

       $(document).ready(function () {
           $('select').formSelect();

           $('.resource-submit-button').on('click',function () {

                let formData = new FormData();
                formData.append('title', $('#title').val());
                formData.append('closing_date', $('#closing_date').val());
                formData.append('resource_category_id', $('#resource_category_id').val());
                jQuery.each(jQuery('#image_url')[0].files, function (i, file) {
                   formData.append('image_url', file);
               });

                $.ajax({
                    url: '{{route('store-resource-funding')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        // blog_id = response.blog.id;-
                        $('.second-row').show();

                        $('#summernote-form').append(
                            '<input name="fundingResource_id" value="'+response.fundingResource_id+'" hidden >'
                        );
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
            });


       });

        $('#summernote').summernote({
            placeholder: 'Content here ..',
            height: 200,

        });
    </script>

   <head>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
       <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
       <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
       <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

   </head>



   <head>
       <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
       <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

       <!-- include summernote css/js-->
       <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
       <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
   </head>
    @endsection
