@extends('layouts.mentor-layout')

@section('content')
    <head>
        <!-- include libraries(jQuery, bootstrap) -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- include summernote css/js -->
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>

    </head>


    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>

    <!--Documents Datatable-->
    <div class="container-fluid">
        <div class="row">
            <h4 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:5em;">Documents</h4>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="resource_documents-table">
                    <thead>
                    <tr>
                        <th>Documents</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!--Documents edit upload-->
    <div class="card" style="width: 1080px;margin: 0 auto;top: 7vh">
        <form class="col s12">
            <br>
            <input value="{{$fundingResource->id}}" id="fundingResource_id" hidden>
            <h2 class="center-align" style="font-size: 2em;">File Uploads</h2>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="col m6">
                    <div class="file-field input-field" style="bottom:0px!important;">
                        <div class="btn">
                            <span>Upload File</span>
                            <input id="documents" type="file" name="documents" multiple>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="text-align: center;">
                <a class="waves-effect waves-light btn" id="file-upload-submit-button">Submit</a>
            </div>
        </form>
        <br>
    </div>

    <br>
    <!--Edit Funding Resources-->
    <div class="card" style="width: 1080px;margin: 0 auto;top: 7vh">
        <br>
        <form id="update-funding-resource" method="post">
            @csrf
            <input value="{{$fundingResource->id}}" id="fundingResource_id" hidden>
            <h2 class="center-align" style="font-size: 2em;">Edit Resource</h2>
            <div class="row">
                <div class="input-field col s5" style="margin-left: 2em;margin-right: 2em">
                    <input id="title" type="text" value="{{$fundingResource->title}}">
                    <label for="title">Tittle</label>
                </div>
                <div class="input-field col s5" style="margin-left: 2em;margin-right: 2em">
                    <input id="closing_date" type="date" value="{{$fundingResource->closing_date}}">
                    <label for="closing_date">Closing date</label>
                </div>
            </div>
            <div class="input-field col s5" style="margin-left: 1em;width: 400px">
                <select id="resource_category_id">
                    @if(isset($fundingResource->fundingResourceCategory))
                        <option disabled>Select Category</option>
                        <option value="{{$fundingResource->fundingResourceCategory->id}}"
                                selected>{{$fundingResource->fundingResourceCategory->category_name}}</option>

                        @foreach($categories as $category)
                            <option
                                value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach
                    @else
                        <option disabled selected>Select Category</option>
                    @foreach($categories as $category)
                            <option
                                value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach
                    @endif

                </select>
                <label>Category</label>
            </div>
            <br>
            <div class="row" style="margin-left: 1000px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn resource-submit-button">Save</a>
                </div>
            </div>
            <br>
        </form>

    </div>

    <br>
    <!--Edit summernote-->
    <div class="container">
        <div class="panel-body">
            <form action="{{url('/update-funding-summernote/'.$fundingResource->id)}}" method="POST" style="margin-top: 50px" id="summernote-form">
                <div class="form-group">
                    <textarea id="summernote" name="content" class="summernote"></textarea>
                </div>

                <div class="form-group">
                    <button type="submit">Submit</button>
                    {!!csrf_field()!!}
                </div>
            </form>
        </div>
    </div>




    <script>
        let file_array =[];
        let file_array_count = 0;
        $(document).ready(function (data) {
            $(function () {
                let fundingResource_id = $('#fundingResource_id').val();
                let url = '/get-documents/' + fundingResource_id;
                $('#resource_documents-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    type : 'get',
                    scrollX: 640,
                    ajax: url,
                    {{--ajax: '{{route('getDoc')}}',--}}
                    columns: [
                        {data: 'documents', name: 'documents'},
                        {data: 'action', name: 'action', orderable: true, searchable: true}
                    ]
                });
                $('select[name="resource_documents-table_length"]').css("display","inline");
            });
        });

        $(document).ready(function () {
            $('select').formSelect();

            $("#file-upload-submit-button").on('click',function () {
                let formData = new FormData();
                let fundingResourceid = $('#fundingResource_id').val();

                formData.append('fundingResource_id', $('#fundingResource_id').val());

                jQuery.each(jQuery('#documents')[0].files, function (i, file) {
                    file_array.push(file);
                });

                file_array.forEach(function(file, i){
                    formData.append('file_' + i, file);
                    console.log('file: ' + formData.get('file_' + i));
                    file_array_count += 1;
                });

                formData.append('file_array_count', file_array_count);

                let url = '/update-resource/' + fundingResourceid;
                $.ajax({
                    url:url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#file-upload-submit-button').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    }
                });
            });

            $('.resource-submit-button').on('click', function () {

                let formData = new FormData();
                formData.append('title', $('#title').val());
                formData.append('closing_date', $('#closing_date').val());
                formData.append('fundingResource_id', $('#fundingResource_id').val());
                formData.append('resource_category_id', $('#resource_category_id').val());


                console.log(formData);
                let url = '/update-resource-funding/' + '{{$fundingResource->id}}';


                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('.resource-submit-button').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    }
                });
            });


        });
        var content = {!! json_encode($fundingResource->resourceSummernote->content) !!};
        $('.summernote').summernote('code', content);
        $('#summernote').summernote({
            placeholder: 'Content here ..',
            height: 400,

        });
    </script>

{{--    <head>--}}
{{--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">--}}
{{--        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>--}}
{{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>--}}
{{--        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>--}}
{{--        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">--}}
{{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>--}}

{{--    </head>--}}



{{--    <head>--}}
{{--        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">--}}
{{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>--}}
{{--        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>--}}

{{--        <!-- include summernote css/js-->--}}
{{--        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">--}}
{{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>--}}
{{--    </head>--}}



@endsection
