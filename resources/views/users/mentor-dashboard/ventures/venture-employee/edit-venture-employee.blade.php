@extends('layouts.mentor-layout')
@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <div class="container">
        <div class="row">
            <br>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Create
                Employee</h6>
            <div class="row card hoverable">
                <div id="test1" class="col s12">
                    <form id="venture-employee-form" class="col s12" style="margin-top:1em;"
                          enctype="multipart/form-data">
                        @csrf
                        {{--<h5>Personal Details</h5>--}}
                        {{--Additional--}}
                        <div class="row">
                            <input id="employee_id" value="{{$venture_employee->id}}" type="text" class="validate"
                                   hidden>
                            {{--<p>{{$venture_employee->id}}</p>--}}
                            <div class="input-field col m6">
                                <select id="title">
                                    {{--<option value="" disabled selected>Choose Title</option>--}}
                                    <option value="Mr" {{$venture_employee->title=='Mr'?'selected':''}}>Mr</option>
                                    <option value="Mrs"{{$venture_employee->title=='Mrs'?'selected':''}}>Mrs</option>
                                    <option value="Miss"{{$venture_employee->title=='Miss'?'selected':''}}>Miss</option>
                                    <option value="Ms"{{$venture_employee->title=='Ms'?'selected':''}}>Ms</option>
                                    <option value="Dr"{{$venture_employee->title=='Dr'?'selected':''}}>Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="initials" value="{{$venture_employee->initials}}" type="text"
                                       class="validate" required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="employee_name" value="{{$venture_employee->employee_name}}" type="text"
                                       class="validate" required>
                                <label for="employee_name">Employee Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="employee_surname" value="{{$venture_employee->employee_surname}}" type="text"
                                       class="validate" required>
                                <label for="employee_surname">Employee Surname</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="id_number" value="{{$venture_employee->id_number}}" type="text"
                                       class="validate" required>
                                <label for="id_number">ID Number</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="gender">
                                    {{--<option value="" disabled selected>Choose Gender</option>--}}
                                    <option value="Male" {{$venture_employee->gender=='Male'?'selected':''}}>Male
                                    </option>
                                    <option value="Female" {{$venture_employee->gender=='Female'?'selected':''}}>
                                        Female
                                    </option>
                                </select>
                                <label>Gender</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="email" value="{{$venture_employee->email}}" type="email" class="validate"
                                       required>
                                <label for="email">Email Address</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" value="{{$venture_employee->contact_number}}" type="tel"
                                       class="validate" required>
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="position" value="{{$venture_employee->position}}" type="text"
                                       class="validate" required>
                                <label for="position">Employee Position</label>
                            </div>
                        </div>
                        <br>
                        <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">
                            EMPLOYEE UPLOADS</h6>
                        <br>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Client Contract</span>
                                        <input id="contract_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate"
                                               value="{{$venture_employee->contract_url?$venture_employee->contract_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col m6">
                                <input id="start_date" value="{{$venture_employee->start_date}}" type="text"
                                       class="validate">
                                <label for="start_date">Date Started (yyy/mm/dd)</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="status" class="input-select">
                                    {{--<option value="" disabled selected>Employee Status</option>--}}
                                    <option value="Active" {{$venture_employee->status=='Active'?'selected':''}}>
                                        Active
                                    </option>
                                    <option value="Resigned" {{$venture_employee->status=='Resigned'?'selected':''}}>
                                        Resigned
                                    </option>
                                    <option value="Exited" {{$venture_employee->status=='Exited'?'selected':''}}>
                                        Exited
                                    </option>
                                </select>
                                <label>Employee Status</label>
                            </div>
                            <div class="input-field col m6" id="dateResigned" hidden>
                                <input id="resigned_date" value="{{$venture_employee->resigned_date}}" type="date"
                                       class="validate">
                                <label for="resigned_date">Date Resigned</label>
                            </div>
                            <div class="input-field col m6" id="dateEnd" hidden>
                                <input id="end_date" value="{{$venture_employee->end_date}}" type="date"
                                       class="validate">
                                <label for="end_date">Date Ended</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-venture_employee" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();


            $(".input-select").on("change", checkSelect);

            function checkSelect() {
                if ($('#status').val() === 'Active') {
                    $("#dateResigned").hide();
                    $("#dateEnd").hide();
                } else if ($('#status').val() === 'Resigned') {
                    $("#dateResigned").show();
                    $("#dateEnd").hide();
                } else if ($('#status').val() === 'Exited') {
                    $("#dateEnd").show();
                    $("#dateResigned").hide();
                }
            }

            $('#venture-employee-form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();

                formData.append('employee_name', $('#employee_name').val());
                formData.append('position', $('#position').val());
                formData.append('start_date', $('#start_date').val());
                formData.append('end_date', $('#end_date').val());
                formData.append('resigned_date', $('#resigned_date').val());
                formData.append('contract_url', $('#contract_url').val());
                formData.append('employee_surname', $('#employee_surname'));
                formData.append('title', $('#title').val());
                formData.append('initials', $('#initials').val());
                formData.append('id_number', $('#id_number').val());
                formData.append('gender', $('#gender').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('email', $('#email').val());
                formData.append('status', $('#status').val());
                formData.append('employee_id', $('#employee_id').val());

                jQuery.each(jQuery('#contract_url')[0].files, function (i, file) {
                    formData.append('contract_url', file);
                });

                console.log("user ", formData);

                $.ajax({
                    url: "/update-employee/" + '{{$venture_employee->id}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',

                    success: function (response, a, b) {
                        /*console.log("success", response);
                        alert(response.message);*/
                        $("#save-venture_employee").notify(
                            "You have successfully Update this Employee.", "success",
                            {position: "right"}
                        );
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = error.response.message;
                        let errors = error.response.errors;

                        for (var error in errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);
                        $("#modal1").close();
                    }
                });
            });
        });
    </script>
@endsection
