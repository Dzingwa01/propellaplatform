@extends('layouts.mentor-layout')
@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <div class="container">
        <div class="row">
            <br>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Create
                Employee</h6>
            <div class="row card hoverable">
                <div id="test1" class="col s12">
                    <form id="venture-employee-form" class="col s12" style="margin-top:1em;"
                          enctype="multipart/form-data">
                        @csrf
                        {{--<h5>Personal Details</h5>--}}
                        {{--Additional--}}
                        <div class="row">
                            <input id="venture-id-input" value="{{$venture->id}}" hidden disabled>
                            <div class="input-field col m6">
                                <select id="title">
                                    <option value="" disabled selected>Choose Title</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Ms</option>
                                    <option value="Miss">Mr</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr">Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="initials" type="text" class="validate" required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="employee_name" type="text" class="validate" required>
                                <label for="employee_name">Employee Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="employee_surname" type="text" class="validate" required>
                                <label for="employee_surname">Employee Surname</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="id_number" type="text" class="validate" required>
                                <label for="id_number">ID Number</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="gender">
                                    <option value="" disabled selected>Choose Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                                <label>Gender</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="email" type="email" class="validate" required>
                                <label for="email">Email Address</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="tel" class="validate" required>
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="position" type="text" class="validate" required>
                                <label for="position">Employee Position</label>
                            </div>
                        </div>
                        <br>
                        <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">
                            EMPLOYEE UPLOADS</h6>
                        <br>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Client Contract</span>
                                        <input id="contract_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col m6">
                                <input id="start_date" type="date" class="validate">
                                <label for="start_date">Date Started</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="status" class="input-select">
                                    <option value="" disabled selected>Employee Status</option>
                                    <option value="Active">Active</option>
                                    <option value="Resigned">Resigned</option>
                                    <option value="Exited">Exited</option>
                                </select>
                                <label>Employee Status</label>
                            </div>
                            <div class="input-field col m6" id="dateResigned" hidden>
                                <input id="resigned_date" type="date" class="validate">
                                <label for="resigned_date">Date Resigned</label>
                            </div>
                            <div class="input-field col m6" id="dateEnd" hidden>
                                <input id="end_date" type="date" class="validate">
                                <label for="end_date">Date Ended</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-venture_employee" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();



            $(".input-select").on("change", checkSelect);

            function checkSelect() {
                if ($('#status').val() === 'Active') {
                    $("#dateResigned").hide();
                    $("#dateEnd").hide();
                }else if ($('#status').val() === 'Resigned') {
                    $("#dateResigned").show();
                    $("#dateEnd").hide();
                }else if ($('#status').val() === 'Exited') {
                    $("#dateEnd").show();
                    $("#dateResigned").hide();
                }
            }

            $('#venture-employee-form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();

                formData.append('employee_name', $('#employee_name').val());
                formData.append('position', $('#position').val());
                formData.append('start_date', $('#start_date').val());
                formData.append('end_date', $('#end_date').val());
                formData.append('resigned_date', $('#resigned_date').val());
                formData.append('contract_url', $('#contract_url').val());
                formData.append('employee_surname', $('#employee_surname'));
                formData.append('title', $('#title').val());
                formData.append('initials', $('#initials').val());
                formData.append('id_number', $('#id_number').val());
                formData.append('gender', $('#gender').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('email', $('#email').val());
                formData.append('status', $('#status').val());

                jQuery.each(jQuery('#contract_url')[0].files, function (i, file) {
                    formData.append('contract_url', file);
                });

                console.log("user ", formData);
                let venture_id = $('#venture-id-input').val();

                $.ajax({
                    url: "{{ route('store-employee', $venture->id) }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',

                    success: function (response, a, b) {
                        /*console.log("success", response);
                        alert(response.message);*/
                        $("#save-venture_employee").notify(
                            "You have successfully added a Employee.", "success",
                            { position:"right" }
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = error.response.message;
                        let errors = error.response.errors;

                        for (var error in errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);
                        $("#modal1").close();
                    }
                });
            });
        });
    </script>
@endsection
