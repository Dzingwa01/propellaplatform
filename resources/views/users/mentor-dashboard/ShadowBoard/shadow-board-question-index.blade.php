@extends('layouts.mentor-layout')

@section('content')
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Questions</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="shadow_board_question-table">
                    <thead>
                    <tr>
                        <th>Question Number</th>
                        <th>Question Text</th>
                        <th>Question Type</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Upload" href="{{url('uploadShadowQuestion')}}">
            <i class="large material-icons">add</i>
        </a>
    </div>

    <script>

        $(document).ready(function () {
            $('select').formSelect();
            $(function () {
                $('#shadow_board_question-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-shadow-questions')}}',
                    columns: [
                        {data: 'question_number', name: 'question_number'},
                        {data: 'question_text', name: 'question_text'},
                        {data: 'question_type', name: 'question_type'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="shadow_board_question-table_length"]').css("display","inline");
            });

        });

    </script>
@endsection
