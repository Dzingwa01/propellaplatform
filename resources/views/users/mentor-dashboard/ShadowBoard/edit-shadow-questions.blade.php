@extends('layouts.mentor-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <div class="row" style="width: 850px;padding-left:9%;margin-top: 10vh">
        <div class="card white" style="padding-left:5%;">
            <br>
            <input value="{{$shadowBoardQuestion->id}}" id="question_id" hidden>
            <h4 style="margin-left: 3em">Edit shadow board questions</h4>
            <br>
            <div class="input-field" style="width:90%">
                <select id="category-id">
                    @foreach($questionCategories as $category)
                        <option value="{{$category->id}}" {{$category->id==$category->display_name?'selected':''}}>{{$category->category_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="input-field">
                <input style="width:90%" id="question_number" type="number" value="{{$shadowBoardQuestion->question_number}}">
                <label for="question_number">Question Number</label>
            </div>

            <div class="input-field">
                <input style="width:90%" id="question_text" type="text" value="{{$shadowBoardQuestion->question_text}}" class="validate">
                <label for="question_text">Question text</label>
            </div>


            <div class="row" style="margin-left: 500px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn" id="question-submit-button">Save</a>
                </div>
            </div>
            <br/>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('select').formSelect();

            $('#question-submit-button').on('click',function () {
                let formData = new FormData();
                formData.append('question_text', $('#question_text').val());
                formData.append('question_number', $('#question_number').val());
                formData.append('question_category_id', $('#category-id').val());
                formData.append('question_sub_text', $('#question_sub_text').val());


                console.log(formData);

                let url = '/updateShadowQuestion/'+ '{{$shadowBoardQuestion->id}}';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>
@endsection

