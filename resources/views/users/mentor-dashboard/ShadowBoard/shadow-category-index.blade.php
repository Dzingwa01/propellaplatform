@extends('layouts.mentor-layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Shadow Board Category</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="shadow_board_question_categories-table">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add User Role" href="{{url('createShadowQuestionCategory')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>


    </div>

    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#shadow_board_question_categories-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('getShadowCategory')}}',
                        columns: [
                            {data: 'category_name', name: 'category_name'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]

                    });
                    $('select[name="shadow_board_question_categories-table_length"]').css("display","inline");
                } );
            });

        </script>
    @endpush

    @endsection
