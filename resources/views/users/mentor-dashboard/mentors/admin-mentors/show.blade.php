@extends('layouts.mentor-layout')

@section('content')

   <br>
    <br>

   <div class="card" style="width: 1000px;margin: 0 auto">
       <br>
       <div class="row" style="margin-left: 6em; margin-right: 6em;">
           <input hidden disabled id="mentor-id-input" value="{{$user_mentor->id}}">
           <input hidden disabled id="venture-id-input" value="{{$venture->id}}">
           <p style="font-size: 2em;margin-left: 200px"><b>Mentor Evaluation of Venture</b></p>

           <div class="row" style="margin-left: 3em">
               <p><b>Mentor</b> : {{$user->name}} {{$user->surname}}</p>
               <p><b>Venture</b> : {{$venture->company_name}}</p>
               <br>
           </div>

           @if (count($answered_questions) > 1)
               <ol>
                   @foreach ($question_array as $question_answer)
                       <div class="row">
                           <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                           <div class="input-field">
                               <textarea> {{$question_answer->answer_text}}</textarea>
                           </div>
                       </div>

                   @endforeach

               </ol>

               @else
               <ol>
                   @foreach ($g_questions as $question)
                       <li value="{{$question->question_number}}">  {{ $question->question_text}} </li>
                       @if($question->question_type == "1_5")
                           <div class="input-field">
                               <input type="number" min="0" max="5" id="{{$question->id}}"
                                      class="materialize-textarea answer-text-desktop"
                                      style="width:20%">
                           </div>

                       @elseif($question->question_type == "Yes_No")

                           <div class="row">
                               <div class="input-field col l3 s3">
                                   <select id="{{$question->id}}" class="answer-text-desktop">
                                       <option value="" disabled selected>Yes / No</option>
                                       <option value="Yes">Yes</option>
                                       <option value="No">No</option>
                                   </select>
                               </div>
                           </div>


                       @elseif($question->question_type == "text")
                           <div class="input-field ">
                               <input id="{{$question->id}}" type="text" class="validate answer-text-desktop" style="width:100%">
                           </div>
                       @elseif($question->question_type == "feedback_text")
                           <div class="input-field ">
                               <input id="{{$question->id}}" type="text" class="validate answer-text-desktop" style="width:100%">
                           </div>

                       @endif
                   @endforeach

               </ol>

               @endif
           <br>
           <div class="col l6 m5 s12">
               <button class="btn waves-effect waves-light" id="answers-store"> Save <i
                       class="material-icons right">send</i></button>
           </div>
       </div>
       <br>
   </div>
   <br>


   <script>

       $(document).ready(function () {
           $('.tooltipped').tooltip();
           $('select').formSelect();


           $('#answers-store').on('click', function () {

               //To store collection of all the answers
               let answersArray = [];

               //To get current user
               let data = $('#mentor-id-input').val();
               let venture_id = $('#venture-id-input').val();

               //Loop through all inputs created
               $('.answer-text-desktop').each(function () {
                   let model = {
                       shadow_board_question_id: this.id,
                       mentor_id: data,
                       answer_text: this.value,
                       venture_id: venture_id
                   };

                   //Add to list of answers
                   answersArray.push(model);
               });

               let formData = new FormData();
               formData.append('questions_answers',JSON.stringify(answersArray));
               // formData.append('form_completed', 'true');

               $.ajax({
                   url: "/shadowQuestionAnswer",
                   processData: false,
                   contentType: false,
                   dataType: 'json',
                   data: formData,
                   type: 'post',
                   headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                   success: function (response, a, b) {
                       $('#answers-store').notify(response.message, "success");

                       setTimeout(function(){
                           window.location.href = '';
                       }, 3000);
                   },

                   error: function (response) {
                       console.log("error",response);
                       let message = response.responseJSON.message;
                       alert(message);
                   }
               });
           });
       });
   </script>
    @Endsection
