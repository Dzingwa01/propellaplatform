@extends('layouts.mentor-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('mentor-venture-to-mentor')}}
    <br>
    <div class="container-fluid">
        <input hidden disabled value="{{$user_mentor->id}}" id="mentor-id-input">


    </div>
    <br>

    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Ventures assigned to Mentor</h6>
        </div>
        <br>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="venture-table">
                    <thead>
                    <tr>
                        <th>Venture Name</th>
                        <th>Contact Number</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script>


        $(document).ready(function(){
            $('.modal').modal();

            $('select').formSelect();

            $(function () {
                let mentor_id = $('#mentor-id-input').val();
                let url = '/get-venture-assigned-to-mentor/' + mentor_id;
                $('#venture-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    type: 'get',
                    scrollX: 640,
                    ajax: url,
                    columns: [
                        {data: 'company_name', name: 'company_name'},
                        {data: 'contact_number', name: 'contact_number'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},

                    ]
                });
                $('select[name="venture-table_length"]').css("display", "inline");
            });



        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>

    @endsection
