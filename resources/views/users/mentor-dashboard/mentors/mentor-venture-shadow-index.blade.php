@extends('layouts.mentor-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('mentor-shadow-board',$user_mentor)}}
    <br>
    <div class="container-fluid">
        <input hidden disabled value="{{$user_mentor->id}}" id="mentor-id-input">


    </div>
    <br>

    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Shadow board meetings</h6>
        </div>
        <br>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="venture-table">
                    <thead>
                    <tr>
                        <th>Venture Name</th>
                        <th>Shadow board date</th>
                        <th>Shadow board time</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script>


        $(document).ready(function(){
            $('.modal').modal();

            $('select').formSelect();

            $(function () {
                let mentor_id = $('#mentor-id-input').val();
                let url = '/get-venture-mentor-shadow/' + mentor_id;
                $('#venture-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    type: 'get',
                    scrollX: 640,
                    stateSave: true,
                    pageLength: 100,
                    ajax: url,
                    columns: [
                        {data: 'company_name', name: 'company_name'},
                        {data: 'shadow_board_date', name: 'shadow_board_date'},
                        {data: 'shadow_board_time', name: 'shadow_board_time'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},

                    ]
                });
                $('select[name="venture-table_length"]').css("display", "inline");
            });



        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>

@endsection
