@extends('layouts.mentor-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('mentor-venture-evaluation',$user_mentor)}}
    <head>
        <!-- jQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    </head>

    <div class="card">
        <input hidden disabled id="venture-id-input" value="{{$venture->id}}">
        <input hidden disabled id="mentor-id-input" value="{{$user_mentor->id}}">
        <input hidden disabled id="mentor-venture-shadowboard-id" value="{{$mentorVentureShadowboard->id}}">


        @if(count($mentorQuestionsAnswers) > 0)
            <div class="row" style="margin-left: 2em; margin-right: 6em;">
                <p style="font-size: 2em;margin-left: 1em"><b>Mentor Evaluation of Venture</b></p>
                <p style="margin-left: 2em;"><b>Venture</b> : {{$venture->company_name}} </p>
                <p style="margin-left: 2em;"><b>Mentor</b> : {{$user_mentor->user->name}} </p>
                <br>
                <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-5), with one being bad, five being perfect.</b></p>
                <br>
                <div class="input-field col m12">
                    <textarea id="comment_section-top" type="text" class="validate" placeholder="Write comment"></textarea>
                </div>
                <br>
                <button id="success-comment-top">Submit</button>
                <ol>
                    @foreach ($mentorQuestionsAnswers as $mqa)
                        <li value="{{$mqa->question_number}}">  {{$mqa->question_text}} </li>
                        @if($mqa->question_type == "1_5")
                            <div class="input-field">
                                <input type="number" min="0" max="5" id="{{$mqa->id}}"
                                       class="materialize-textarea answer-text-desktop"
                                       style="width:20%" value="{{$mqa->answer_text}}">
                            </div>

                        @elseif($mqa->question_type == "Multiple")
                            @if(count($mqa->question_sub_texts) > 0)
                                @foreach($mqa->question_sub_texts as $sub)
                                    <p class="testing-checkbox">
                                        <label>
                                            <input id="{{$mqa->id}}" value="{{$sub->question_sub_text}}" type="checkbox"/>
                                            <span>{{$sub->question_sub_text}}</span>
                                        </label>
                                    </p>
                                @endforeach
                            @endif

                        @elseif($mqa->question_type == "Yes_No")
                            <div class="row">
                                <div class="input-field col l3 s3">
                                    <select id="{{$mqa->id}}" class="answer-text-desktop">
                                        <option value="{{$mqa->answer_text}}" selected>{{$mqa->answer_text}}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                        @elseif($mqa->question_type == "text")
                            <div class="input-field ">
                                <input id="{{$mqa->id}}" type="text" value="{{$mqa->answer_text}}" class="validate answer-text-desktop" style="width:80%">
                            </div>
                        @elseif($mqa->question_type == "feedback_text")
                            <div class="input-field ">
                                <input id="{{$mqa->id}}" value="{{$mqa->answer_text}}" type="text" class="validate answer-text-desktop" style="width:100%">
                            </div>
                        @endif
                    @endforeach
                </ol>
                <br>
                <div class="col l6 m5 s12">
                    <button style="margin-left: 300px" id="success">Submit</button>
                </div>
            </div>
        @else
            <div class="row" style="margin-left: 2em; margin-right: 6em;">
                <p style="font-size: 2em;margin-left: 1em"><b>Mentor Evaluation of Venture</b></p>
                <p style="margin-left: 2em;"><b>Venture</b> : {{$venture->company_name}} </p>
                <p style="margin-left: 2em;"><b>Mentor</b> : {{$user_mentor->user->name}} </p>
                <br>
                <div class="input-field col m12">
                    <textarea id="comment_section-below" type="text" class="validate" placeholder="Write comment"></textarea>
                </div>
                <br>
                <button id="success-comment-below">Submit</button>
                <ol>
                    @foreach ($questions as $question)
                        <li value="{{$question->question_number}}">  {{ $question->question_text}} </li>
                        @if($question->question_type == "1_5")
                            <div class="input-field">
                                <input type="number" min="0" max="5" id="{{$question->id}}"
                                       class="materialize-textarea answer-text-desktop"
                                       style="width:20%">
                            </div>
                        @elseif($question->question_type == "Multiple")
                            @foreach($question->shadowBoardQuestionSubQuestionText as $sub)
                                <p class="testing-checkbox">
                                    <label>
                                        <input id="{{$question->id}}" value="{{$sub->question_sub_text}}" type="checkbox"/>
                                        <span>{{$sub->question_sub_text}}</span>
                                    </label>
                                </p>
                            @endforeach

                        @elseif($question->question_type == "Yes_No")
                            <div class="row">
                                <div class="input-field col l3 s3">
                                    <select id="{{$question->id}}" class="answer-text-desktop">
                                        <option value="" disabled selected>Yes / No</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                        @elseif($question->question_type == "text")
                            <div class="input-field ">
                                <input id="{{$question->id}}" type="text" class="validate answer-text-desktop" style="width:80%">
                            </div>
                        @elseif($question->question_type == "feedback_text")

                            <div class="input-field ">
                                <input id="{{$question->id}}" type="text" class="validate answer-text-desktop" style="width:100%">
                            </div>

                        @endif
                    @endforeach

                </ol>
                <br>
                <div class="col l6 m5 s12">
                    <button style="margin-left: 300px" id="successfull">Submit</button>
                </div>
            </div>
        @endif
    </div>
    <style>
        button {
            background-color: cadetblue;
            color: whitesmoke;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            font-size: 18px;
            font-weight: 500;
            border-radius: 7px;
            padding: 15px 35px;
            cursor: pointer;
            white-space: nowrap;
            margin: 10px;
        }
        img {
            width: 200px;
        }
        input[type="text"] {
            padding: 12px 20px;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 10px;
            box-sizing: border-box;
        }
        h1 {
            border-bottom: solid 2px grey;
        }
        #success {
            background: green;
        }
        #error {
            background: red;
        }
        #warning {
            background: coral;
        }
        #info {
            background: cornflowerblue;
        }
        #question {
            background: grey;
        }
        th{
            text-transform: uppercase!important;
        }
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
    <script>

        $(document).ready(function () {
            $('.tooltipped').tooltip();
            $('select').formSelect();


            $(document).on('click', '#success', function(e) {
                $(this).text("please wait...");

                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#mentor-id-input').val();
                let venture_id = $('#venture-id-input').val();
                let mentorVentureShadowboard = $('#mentor-venture-shadowboard-id').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        shadow_board_question_id: this.id,
                        mentor_id: data,
                        answer_text: this.value,
                        venture_id: venture_id
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                $(".testing-checkbox input[type=checkbox]:checked").each(function () {
                    let model = {
                        shadow_board_question_id: this.id,
                        mentor_id: data,
                        answer_text: this.value,
                        venture_id: venture_id
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));
                formData.append('mentorVentureShadowboard', mentorVentureShadowboard);

                $.ajax({
                    url: "/storeMentorShadowBoardAnswers",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        swal(
                            'Success',
                            'Mentor evaluation saved <b style="color:green;">Successfully</b>',
                            'success',
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });

            });
            // Alert With Custom Icon and Background Image
            $(document).on('click', '#icon', function(event) {
                swal({
                    title: 'Custom icon!',
                    text: 'Alert with a custom image.',
                    imageUrl: 'https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg',
                    imageWidth: 200,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                    animation: false
                })
            });

            $(document).on('click', '#image', function(event) {
                swal({
                    title: 'Custom background image, width and padding.',
                    width: 700,
                    padding: 150,
                    background: '#fff url(https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg)'
                })
            });

            $(document).on('click', '#successfull', function(e) {
                $(this).text("please wait...");

                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#mentor-id-input').val();
                let venture_id = $('#venture-id-input').val();
                let mentorVentureShadowboard = $('#mentor-venture-shadowboard-id').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        shadow_board_question_id: this.id,
                        mentor_id: data,
                        answer_text: this.value,
                        venture_id: venture_id
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                $(".testing-checkbox input[type=checkbox]:checked").each(function () {
                    let model = {
                        shadow_board_question_id: this.id,
                        mentor_id: data,
                        answer_text: this.value,
                        venture_id: venture_id
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));
                formData.append('mentorVentureShadowboard', mentorVentureShadowboard);

                $.ajax({
                    url: "/storeMentorShadowBoardAnswers",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        swal(
                            'Success',
                            'Mentor evaluation saved <b style="color:green;">Successfully</b>',
                            'success',
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });

            });

            //Submit comment section
            $(document).on('click', '#success-comment-top', function(e) {
                let mentor_venture_id = $('#mentor-venture-shadowboard-id').val();
                let formData = new FormData();
                formData.append('comment_section', $('#comment_section-top').val());
                formData.append('mentor_id', $('#mentor-id-input').val());
                formData.append('venture_id', $('#venture-id-input').val());
                formData.append('mentor_venture_id', $('#mentor-venture-shadowboard-id').val());

                let url = "/storeMentorShadowComment/" + mentor_venture_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        swal(
                            'Success',
                            'Comment saved<b style="color:green;">Successfully</b>',
                            'success',
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            $(document).on('click', '#success-comment-below', function(e) {
                let mentor_venture_id = $('#mentor-venture-shadowboard-id').val();
                let formData = new FormData();
                formData.append('comment_section', $('#comment_section-below').val());
                formData.append('mentor_id', $('#mentor-id-input').val());
                formData.append('venture_id', $('#venture-id-input').val());
                formData.append('mentor_venture_id', $('#mentor-venture-shadowboard-id').val());

                let url = "/storeMentorShadowComment/" + mentor_venture_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        swal(
                            'Success',
                            'Comment saved<b style="color:green;">Successfully</b>',
                            'success',
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
