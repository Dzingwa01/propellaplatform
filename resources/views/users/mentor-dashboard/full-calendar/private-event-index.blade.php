@extends('layouts.mentor-layout')
@section('content')
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Private Events</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="private-events-table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#private-events-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-private-events')}}',
                    columns: [
                        {data: 'title', name: 'title'},
                        {data: 'start', name: 'start'},
                        {data: 'end', name: 'end'},
                        {data: 'description', name: 'description'},
                        {data: 'eventStatus', name: 'eventStatus'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                $('select[name="private-events-table_length"]').css("display","inline");
            });
        });
    </script>
@endsection
