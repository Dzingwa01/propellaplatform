@extends('layouts.mentor-layout')

@section('content')


    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Incubatees</h6>
        </div>
        <br>
        <div class="row" style="margin-left: 2em; margin-right: 2em;">
            <a href="/home" class="modal-close waves-effect waves-green btn">Home<i
                    class="material-icons right">home</i>
            </a>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Category</th>
                        <th>Stage</th>
                        <th>Status</th>
                        <th>Date added</th>
                        <th>POPI</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
               data-tooltip="Add New Incubatee" href="{{url('create-incubatee')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript"
                src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>

        <script type="text/javascript" language="javascript"
                src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
        <script>
            $(document).ready(function () {
                $(function () {
                    $('#users-table').DataTable({
                        dom: 'lBfrtip',
                        buttons: [
                            {
                                extend: 'excel',
                                text: 'Export excel',
                                className: 'exportExcel',
                                filename: 'Export excel',
                                exportOptions: {
                                    modifier: {
                                        page: 'all'
                                    }
                                }
                            }],
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        stateSave: true,
                        pageLength: 100,
                        ajax: '{{route('get-incubatees')}}',
                        columns: [
                            {data: 'name', name: 'name'},
                            {data: 'surname', name: 'surname'},
                            {data: 'email', name: 'email'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'category_name', name: 'category_name'},
                            {data: 'stage', name: 'stage'},
                            {data: 'status', name: 'status'},
                            {data: 'date_joined', name: 'date_joined'},
                            {data: 'incubatee_popi_act_agreement' ,name: 'incubatee_popi_act_agreement'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ],

                    });
                    $('select[name="users-table_length"]').css("display", "inline");
                });

            });

            function confirm_delete_incubatee(obj) {
                var r = confirm("Are you sure want to delete this Incubatee?");
                if (r == true) {
                    $.get('/user-incubatee-delete/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status == 'success') {
                            alert(data.message);
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }

            }
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
