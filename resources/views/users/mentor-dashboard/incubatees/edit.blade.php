@extends('layouts.mentor-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <div class="container">

        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Edit
                Incubatee</h6>
            <div class="row card hoverable">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s6"><a href="#test1" class="active">Personal Details</a></li>
                        <li class="tab col s6"><a href="#test3">Media</a></li>
                    </ul>
                </div>
                <div id="test1" class="col s12">
                    <form id="incubatee-details-update-form" method="post" class="col s12" style="margin-top:1em;"
                          enctype="multipart/form-data">
                        @csrf
                        <input value="{{$current_user->id}}" id="incubatee_id" hidden>
                        <h5>Personal Details</h5>

                        <div class="row">
                            <div class="input-field col m6">
                                <select id="title">
                                    @if($current_user->title != null)
                                        <option value="{{$current_user->title}}">{{$current_user->title}}</option>
                                    @else
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Dr">Dr</option>
                                    @endif
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="initials" type="text" class="validate" value="{{$current_user->initials}}"
                                       required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="name" type="text" class="validate" value="{{$current_user->name}}" required>
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="surname" type="text" class="validate" value="{{$current_user->surname}}"
                                       required>
                                <label for="surname">Surname</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="id_number" type="text" class="validate" value="{{$current_user->id_number}}"
                                       required>
                                <label for="id_number">ID Number</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="dob" type="date" class="validate"
                                       value="{{$current_user->dob}}" required>
                                <label for="dob">Date of Birth</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="age" type="text" class="validate" value="{{$current_user->age}}"
                                       placeholder="Age" required>
                            </div>
                            <div class="input-field col m6">
                                <select id="gender">
                                    @if($current_user->gender != null)
                                        <option value="{{$current_user->gender}}">{{$current_user->gender}}</option>
                                    @else
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    @endif
                                </select>
                                <label>Gender</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="email" type="email" class="validate" value="{{$current_user->email}}"
                                       required>
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="tel" class="validate"
                                       value="{{$current_user->contact_number}}" required>
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="address_one" type="tel" class="validate"
                                       value="{{$current_user->address_one}}" required>
                                <label for="address_one">Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="address_two" type="tel" class="validate"
                                       value="{{$current_user->address_two}}" required>
                                <label for="address_two">Address 2</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="address_three" type="tel" class="validate"
                                       value="{{$current_user->address_three}}" required>
                                <label for="address_three">Address 3</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="city" type="tel" class="validate"
                                       value="{{$current_user->city}}" required>
                                <label for="city">City</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="city" type="tel" class="validate"
                                       value="{{$current_user->code}}" required>
                                <label for="city">Postal Code</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="nmu_graduate">
                                    @if($current_user->nmu_graduate != null)
                                        <option value="{{$current_user->nmu_graduate}}">{{$current_user->nmu_graduate}}</option>
                                    @else
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    @endif
                                </select>
                                <label>NMU Graduate</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="disabled">
                                    @if($current_user->disabled != null)
                                        <option value="{{$current_user->disabled}}">$current_user->disabled</option>
                                    @else
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    @endif
                                </select>
                                <label>Disability</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="home_language">
                                    @if($current_user->home_language != null)
                                        <option value="{{$current_user->home_language}}">{{$current_user->home_language}}</option>
                                    @else
                                        <option value="English">English</option>
                                        <option value="IsiXhosa">IsiXhosa</option>
                                        <option value="IsiZulu">IsiZulu</option>
                                        <option value="Afrikaans">Afrikaans</option>
                                        <option value="IsiNdebele">IsiNdebele</option>
                                        <option value="Sepedi">Sepedi</option>
                                        <option value="SeTswana">SeTswana</option>
                                        <option value="TshiVenḓa">TshiVenḓa</option>
                                        <option value="SiSwati">SiSwati</option>
                                        <option value="XiTsonga">XiTsonga</option>
                                        <option value="SeSotho">SeSotho</option>
                                    @endif
                                </select>
                                <label>Home Language</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="residence">
                                    @if($current_user->residence != null)
                                        <option value="{{$current_user->residence}}">Urban</option>
                                    @else
                                        <option value="Urban">Urban</option>
                                        <option value="Peri-Urban">Peri-Urban</option>
                                        <option value="Rural">Rural</option>
                                    @endif
                                </select>
                                <label>Resides</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_facebook" type="text" value="{{$incubatee->personal_facebook_url}}"
                                       class="validate">
                                <label for="personal_facebook">Facebook</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_linked_in" type="text" value="{{$incubatee->personal_linkedIn_url}}"
                                       class="validate">
                                <label for="personal_linked_in">LinkedIn</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_twitter" type="text" value="{{$incubatee->personal_twitter_url}}"
                                       class="validate">
                                <label for="personal_twitter">Twitter</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_instagram" type="text"
                                       value="{{$incubatee->personal_instagram_url}}" class="validate">
                                <label for="personal_instagram">Instagram</label>
                            </div>
                            <div class="input-field col m6">
                                <textarea id="short_bio" class="materialize-textarea" data-length="120">{{$incubatee->short_bio}}</textarea>
                                <label for="short_bio">About Me</label>
                            </div>
                        </div>
                        <div class="input-field col m6">
                            <select id="status" class="input-select">
                                @if(isset($current_venture->status))
                                    <option selected value="{{$current_venture->status}}">{{$current_venture->status}}</option>
                                @else
                                    <option selected disabled>Choose a status</option>
                                    <option value="Active">Active</option>
                                    <option value="Alumni">Alumni</option>
                                    <option value="Exit">Exit</option>
                                    <option value="Resigned">Resigned</option>
                                    <option value="on-hold">On Hold</option>
                                @endif
                            </select>
                            <label>Status</label>
                        </div>



                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-personal-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>

                    @if(is_null($incubatee->venture_id))
                        <div class="fixed-action-btn" id="float-create-venture">
                            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
                               data-tooltip="Add New Venture"
                               href="{{url('/incubatee-create-venture/'. $incubatee->id)}}">
                                <i class="large material-icons">add</i>
                            </a>
                        </div>
                    @endif

                </div>
                <div id="test3" class="col s12">
                    <form id="incubatee-media-update-form" class="col s12" style="margin-top:1em;" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <input value="{{$current_user->id}}" id="incubatee_id" hidden>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Profile Picture</span>
                                        <input id="profile_picture_url" type="file" name="profile_picture_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->profile_picture_url)?$incubatee_uploads->profile_picture_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <div class="row center" style="width: 100%;">
                                    <div class="col s12 m6 l3" style=" margin-bottom: 2em;">
                                        @if(isset($incubatee_uploads->profile_picture_url))
                                            <div style="border-style: groove; height: 100%; width: 300px; ">
                                                <img style="width:100%;" class="materialboxed"
                                                     src="/storage/{{isset($incubatee_uploads->profile_picture_url)?$incubatee_uploads->profile_picture_url:''}}">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                @if(isset($incubatee_uploads->file_description))
                                    <input id="file_description" type="text" value="{{$incubatee_uploads->file_description}}" class="validate">
                                    <label for="file_description">File name</label>
                                @else
                                    <input id="file_description" type="text"  class="validate">
                                    <label for="file_description">File name</label>
                                @endif
                            </div>
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>ID Document</span>
                                        <input id="id_document" type="file" name="id_document">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->id_document)?$incubatee_uploads->id_document:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <div class="row center" style="width: 100%;">
                                    <div class="col s12 m6 l3" style="margin-bottom: 2em;">
                                        @if(isset($incubatee_uploads->id_document))
                                            <div style="border-style: groove; height: 100%; width: 300px; ">
                                                <img style="width:100%;" class="materialboxed"
                                                     src="/storage/{{isset($incubatee_uploads->id_document)?$incubatee_uploads->id_document:''}}">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                @if(isset($incubatee_uploads->stage_2_pitch_video))
                                    <input id="stage_2_pitch_video" type="text" value="{{$incubatee_uploads->stage_2_pitch_video}}" class="validate">
                                    <label for="stage_2_pitch_video">Stage 2 Pitch Video</label>
                                @else
                                    <input id="stage_2_pitch_video" type="text"  class="validate">
                                    <label for="stage_2_pitch_video">Stage 2 Pitch Video</label>
                                @endif
                            </div>
                            <div class="input-field col m6">
                                @if(isset($incubatee_uploads->stage_2_pitch_video_title))
                                    <input id="stage_2_pitch_video_title" type="text" value="{{$incubatee_uploads->stage_2_pitch_video_title}}" class="validate">
                                    <label for="stage_2_pitch_video_title">Stage 2 Pitch Video Title</label>
                                @else
                                    <input id="stage_2_pitch_video_title" type="text"  class="validate">
                                    <label for="stage_2_pitch_video_title">Stage 2 Pitch Video Title</label>
                                @endif
                            </div>
                        </div>
                        {{-- Proof of address--}}
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Proof of address</span>
                                        <input id="proof_of_address" type="file" name="proof_of_address">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->proof_of_address)?$incubatee_uploads->proof_of_address:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-media" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <style>
        .tabs .tab a:hover, .tabs .tab a.active {
            color: black !important;
            font-weight: bolder;
        }

        .tabs .tab a {
            color: black !important;
        }
    </style>
    @push('custom-scripts')
        <script>

            $(document).ready(function () {
                $("select").formSelect();
                $('.tooltipped').tooltip();


                //Style block where profile picture is loaded
                function profilePictureIsLoaded(e) {
                    $("#profile_picture_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#profile-picture-preview').attr('src', e.target.result);
                    $('#profile-picture-preview').attr('width', '250px');
                    $('#profile-picture-preview').attr('height', '230px');
                }

                //Style block where company logo is loaded
                function companyLogoIsLoaded(e) {
                    $("#company_logo_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#company-logo-preview').attr('src', e.target.result);
                    $('#company-logo-preview').attr('width', '250px');
                    $('#company-logo-preview').attr('height', '230px');
                }

                //Style block where video shot is loaded
                function videoShotIsLoaded(e) {
                    $("#video-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#video-shot-preview').attr('src', e.target.result);
                    $('#video-shot-preview').attr('width', '250px');
                    $('#video-shot-preview').attr('height', '230px');
                }

                //Style block where infographic is loaded
                function infographicIsLoaded(e) {
                    $("#infographic_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#infographic-preview').attr('src', e.target.result);
                    $('#infographic-preview').attr('width', '250px');
                    $('#infographic-preview').attr('height', '230px');
                }

                //Style block where crowdfunding is loaded
                function crowdfundingIsLoaded(e) {
                    $("#crowdfunding_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#crowdfunding-preview').attr('src', e.target.result);
                    $('#crowdfunding-preview').attr('width', '250px');
                    $('#crowdfunding-preview').attr('height', '230px');
                }

                //Style block where product shot is loaded
                function productShotIsLoaded(e) {
                    $("#product-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#product-shot-preview').attr('src', e.target.result);
                    $('#product-shot-preview').attr('width', '250px');
                    $('#product-shot-preview').attr('height', '230px');
                }

                // Function to preview profile picture after validation
                $(function () {
                    $("#profile_picture_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#profile-picture-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = profilePictureIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview company logo after validation
                $(function () {
                    $("#company_logo_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#company-logo-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = companyLogoIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview video shot after validation
                $(function () {
                    $("#video-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#video-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = videoShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview infographic after validation
                $(function () {
                    $("#infographic_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#infographic-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = infographicIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview crowdfunding after validation
                $(function () {
                    $("#crowdfunding_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#crowdfunding-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = crowdfundingIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview product shot after validation
                $(function () {
                    $("#product-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#product-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = productShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });
                let value = $("#dob").val();
                let dob = new Date(value);
                let today = new Date();
                let age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                if (isNaN(age)) {
                    // will set 0 when value will be NaN
                    age = 0;
                } else {
                    age = age;
                }
                let ages = $('#age').val(age);
                $("#dob").change(function () {
                    let value = $("#dob").val();
                    let dob = new Date(value);
                    let today = new Date();
                    let age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                    if (isNaN(age)) {
                        // will set 0 when value will be NaN
                        age = 0;
                    } else {
                        age = age;
                    }
                    let ages = $('#age').val(age);
                });

                $('#incubatee-details-update-form').on('submit', function (e) {
                    e.preventDefault();
                    let formData = new FormData();
                    formData.append('title', $('#title').val());
                    formData.append('initials', $('#initials').val());
                    formData.append('dob', $('#dob').val());
                    formData.append('age', $('#age').val(ages));
                    formData.append('gender', $('#gender').val());
                    formData.append('id_number', $('#id_number').val());
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('email', $('#email').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('personal_facebook_url', $('#personal_facebook').val());
                    formData.append('personal_linkedIn_url', $('#personal_linked_in').val());
                    formData.append('personal_twitter_url', $('#personal_twitter').val());
                    formData.append('personal_instagram_url', $('#personal_instagram').val());
                    formData.append('address_one', $('#address_one').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    formData.append('city', $('#city').val());
                    formData.append('code', $('#code').val());
                    formData.append('nmu_graduate', $('#nmu_graduate').val());
                    formData.append('disabled', $('#disabled').val());
                    formData.append('residence', $('#residence').val());
                    formData.append('home_language', $('#home_language').val());
                    formData.append('short_bio', $('#short_bio').val());
                    formData.append('status', $('#status').val());

                    /*jQuery.each(jQuery('#id_document')[0].files, function (i, file) {
                        formData.append('id_document', file);
                    });*/

                    console.log("user ", formData);


                    let url = '/incubatee-update-personal/' + '{{$current_user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        /**
                         * @param response
                         * @param a
                         * @param b
                         */

                        success: function (response, a, b) {
                            console.log("success", response);
                            alert(response.message);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });


                $('#incubatee-media-update-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();
                    formData.append('file_description', $('#file_description').val());
                    formData.append('stage_2_pitch_video', $('#stage_2_pitch_video').val());
                    formData.append('stage_2_pitch_video_title', $('#stage_2_pitch_video_title').val());

                    jQuery.each(jQuery('#profile_picture_url')[0].files, function (i, file) {
                        formData.append('profile_picture_url', file);
                    });

                    jQuery.each(jQuery('#id_document')[0].files, function (i, file) {
                        formData.append('id_document', file);
                    });

                    jQuery.each(jQuery('#proof_of_address')[0].files, function (i, file) {
                        formData.append('proof_of_address', file);
                    });

                    let url = '/incubatee-update-media/' + '{{$current_user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            console.log("success", response);
                            alert(response.message);
                            sessionStorage.clear();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
