@extends('layouts.administrator-layout')
@section('content')
    <input id="user-id-input" disabled hidden value="{{$bootcamper->id}}">
    <div class="row card" style="margin-top: 10vh;width: 800px;margin-left: 400px">
        <br>
        <dv class="row center">
            <h4><b>Evaluation of : {{$bootcamper->user->name}} {{$bootcamper->user->surname}}</b></h4>
        </dv>
         @if(count($bootcamper_question_array) > 0)
            <div class="row" style="alignment: center; width: 100%;">
                <ol>
                    @foreach ($bootcamper_question_array as $question)
                        <li>  {{ $question->question_text}} </li>
                        <div class="input-field">
                            <input id="{{$question->question_id}}" class="materialize-textarea answer-text-desktop" value="{{$question->answer_text}}"
                                   style="width:90%">
                        </div>
                    @endforeach
                </ol>
            </div>
        @endif
    </div>

    @endsection
