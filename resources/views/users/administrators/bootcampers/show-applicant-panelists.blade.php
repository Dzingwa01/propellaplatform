@extends('layouts.administrator-layout')

@section('content')
    <head>
        <style>
            table {
                width:100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
                text-align: left;
            }
            table#t01 tr:nth-child(even) {
                background-color: #eee;
            }
            table#t01 tr:nth-child(odd) {
                background-color: #fff;
            }
            table#t01 th {
                background-color: black;
                color: white;
            }
        </style>
    </head>
    <br>
    <br>

    <div class="section" style="background-color: grey; margin-bottom: 3em;">
        <input id="bootcamper-id-input" value="{{$bootcamper->id}}" hidden disabled>
        <div class="row center">
            <h5>Panelists assigned to applicant</h5>
{{--            <div class="row center">--}}
{{--                <button class="btn blue" id="{{$applicant->id}}" onclick="downloadExcel(this)">Excel</button>--}}
{{--            </div>--}}

        @foreach($final_panelist_question_score_array as $panelist)
                <div class="col l3 m6 s12 center">
                    <div class="card">
                        <div class="card-content">
                            <h5>{{$panelist->name}} {{$panelist->surname}}</h5>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Question Number</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>Question Score</h6>
                                </div>
                            </div>
                            @if(isset($panelist->user_questions_scores))
                                @foreach($panelist->user_questions_scores as $question_answer)
                                    <div class="row">
                                        <div class="col l6 m6 s12" style="background-color: darkgray;">
                                            <p>{{$question_answer->question_number}}</p>
                                        </div>
                                        <div class="col l6 m6 s12" style="background-color: dimgray;">
                                            <p>{{$question_answer->question_score}}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Total Score</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>{{isset($panelist->panelist_applicant_score) ? $panelist->panelist_applicant_score : 0}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row">
                <div class="col l6 m6 s12">
                    <h5>Total Combined Score: {{$total_panelist_score}}</h5>
                </div>
                <div class="col l6 m6 s12">
                    <h5>Total Average Score: {{$average_panelist_score}}</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="section" style="margin-bottom: 3em; margin-top: 3em;">
        <div class="row center">
            <h5> Combined scores per question </h5>
            <table id="t01">
                <tr>
                    <th>Question Number</th>
                    <th>Total Score</th>
                </tr>
                @foreach($question_scores_array as $question_score)
                    <tr>
                        <td>{{$question_score->question_number}}</td>
                        <td>{{$question_score->question_score}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    <div class="section" style="background-color: grey; margin-top: 3em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Want to see more?</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="bootcamper-panelists-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Selection Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                let bootcamper_id = $('#bootcamper-id-input').val();
                $('#bootcamper-panelists-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/get-ict-applicant-panelists/' + bootcamper_id,
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'selection_date', name: 'selection_date'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="applicant-panelists-table_length"]').css("display","inline");
            });
        });

        // function downloadExcel(obj){
        //     let applicant_id = obj.id;
        //     window.location.href = '/extract-applicant-panelists-data/' + applicant_id;
        // }

    </script>
@endsection
