@extends('layouts.administrator-layout')

@section('content')
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Referred Bootcampers</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="declined-bootcampers-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Category</th>
                        <th>Date Declined</th>
                        <tH>Declined Reason</tH>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#declined-bootcampers-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-referred-bootcampers')}}',
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'contact_number', name: 'contact_number'},
                        {data:'category_name',name:'category_name'},
                        {data:'date_declined', name: 'date_declined'},
                        {data:'declined_reason', name:'declined_reason'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="declined-bootcampers-table_length"]').css("display","inline");
            });
        });
        function confirm_delete_declined_applicant(obj){
            var r = confirm("Are you sure want to delete this declined bootcamper?");
            if (r === true) {
                $.get('/delete-declined-bootcamper/' + obj.id, function (data, status) {
                    if (status === 'success') {
                        alert(data.message);
                        window.location.reload();
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
