@extends('layouts.administrator-layout')

@section('content')
    <br>
    <br>
    <br>

    <div class="section">
        <input hidden disabled id="bootcamper-id-input" value="{{$bootcamper->id}}">
        <div class="row center">
            <div class="col l6 m6 s12">
                <i class="material-icons large" style="color: saddlebrown;" onclick="approveApplicant()">thumb_up</i>
                <h3>Go?</h3>
            </div>
            <div class="col l6 m6 s12">
                <i class="material-icons large" style="color: saddlebrown;" onclick="declineApplicant()">thumb_down</i>
                <h3>No Go?</h3>
            </div>
        </div>

        <div class="row center" hidden id="go-row">
            <div class="col s12 m6 l6" style="width:50%;">
                <select id="venture_category">
                    <option value="" disabled selected>Choose Venture category</option>
                    @foreach($venture_categories as $v_category)
                        <option value="{{$v_category->id}}">{{$v_category->category_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col s12 m6 l6" style="width:50%;">
                <select id="incubatee_stage">
                    <option value="" disabled selected>Choose Venture stage</option>
                    @foreach($incubatee_stages as $i_stage)
                        <option value="{{$i_stage->id}}">{{$i_stage->stage_name}}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn blue" id="submit-chosen-category-button">submit</button>
        </div>

        <div class="row center" hidden id="no-go-row">
            <div class="row center">
                <div class="col l6 m6 s12">
                    <select id="contact-input">
                        <option value="" disabled selected>How did you contact {{$bootcamper->user->name}} {{$bootcamper->user->surname}}?</option>
                        <option value="Phone Call">Phone Call</option>
                        <option value="Face-to-Face">Face-to-Face</option>
                        <option value="Email">Email</option>
                    </select>
                </div>
                <div class="col l6 m6 s12">
                    <select id="contact-result-input">
                        <option value="" disabled selected>What was the result?</option>
                        <option value="Stage 2 - Declined">Stage 2 - Declined</option>
                        <option value="Stage 2 - Referred">Stage 2 - Referred</option>
                        <option value="Bootcamper invited but did not attend">Bootcamper invited but did not attend</option>
                    </select>
                </div>
            </div>
            <div class="col l6 m6 s12">
                <label for="decline-reason-input">Reason it was declined?</label>
                <textarea id="decline-but-referred-reason-input"></textarea>
            </div>
            <div class="col l6 m6 s12">
                <label for="referred-company-input">Who it was referred to?</label>
                <textarea id="referred-company-input"></textarea>
                <button class="btn blue" id="set-bootcamper-to-declined-bootcamper-button">Submit</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

        function approveApplicant(){
            $('#go-row').show();
            $('#no-go-row').hide();
        }

        function declineApplicant(){
            $('#go-row').hide();
            $('#no-go-row').show();
        }

        $('#submit-chosen-category-button').on('click', function(){
            let category_id = $('#venture_category').val();
            let user_id = $('#user-id-input').val();
            let stage_id = $('#incubatee_stage').val();
            let bootcamper_id =$('#bootcamper-id-input').val();
            let r = confirm("Are you sure you want to give this bootcamper incubatee permissions?");
            if(r === true){
                $('#submit-chosen-category-button').text("loading...");
                let formData = new FormData();
                formData.append('category_id', category_id);
                formData.append('stage_id', stage_id);

                let url = '/set-bootcamper-to-incubatee/' + bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#submit-chosen-category-button").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/bootcampers';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        $('#set-bootcamper-to-declined-bootcamper-button').on('click', function(){
            let r = confirm("Are you sure you want to decline this bootcamper?");

            if(r === true){
                $('#set-bootcamper-to-declined-bootcamper-button').text("Please wait");
                let formData = new FormData();
                formData.append('contacted_via', $('#contact-input').val());
                formData.append('result_of_contact', $('#contact-result-input').val());
                formData.append('declined_reason_text', $('#decline-but-referred-reason-input').val());
                formData.append('referred_company', $('#referred-company-input').val());

                let bootcamper_id = $('#bootcamper-id-input').val();
                let url = "/set-bootcamper-to-declined-bootcamper/" + bootcamper_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#set-bootcamper-to-declined-bootcamper-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/bootcampers';
                        }, 3000);
                    },

                    error: function (response) {
                        let message = response.message;
                        alert(message);
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });
    </script>

@endsection
