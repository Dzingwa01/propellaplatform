@extends('layouts.administrator-layout')
@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>

    <div class="card" style="margin-top: 10vh;width: 800px;margin-left: 400px">
        <br>
        <form class="col s12" id="Edit-event-bootcamp" enctype="multipart/form-data">
            <input id="event_bootcamper_id" value="{{$eventBootcamper->id}}" hidden>
            <div class="">
                <h5 class="center-align" >Edit Bootcamper Event</h5>

                <div class="row"style="margin-left:2em;margin-right: 2em">
                    <div class="input-field col s6">
                        <input id="title" type="text" value="{{$eventBootcamper->event->title}}" class="validate" disabled>
                        <label for="title">Event Tittle</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="date_registered" type="text" value="{{$eventBootcamper->date_registered}}" class="validate" disabled>
                        <label for="date_registered">Date Registered</label>
                    </div>
                </div>

                <div class="row" style="margin-left: 2em;margin-right: 2em">
                    <div class="input-field col s6">
                        <select id="attended">
                            <option value="1" {{$eventBootcamper->attended=='1'?'selected':''}}>Yes</option>
                            <option value="0" {{$eventBootcamper->attended=='0'?'selected':''}}>No</option>
                        </select>
                        <label for="type">Attended</label>

                    </div>
                    <div class="input-field col s6">
                        <select id="accepted">
                            <option value="1" {{$eventBootcamper->accepted=='1'?'selected':''}}>Yes</option>
                            <option value="0" {{$eventBootcamper->accepted=='0'?'selected':''}}>No</option>
                        </select>
                        <label for="type">Accepted</label>

                    </div>
                </div>
            </div>

            <button class="btn waves-effect waves-light" style="margin-left:650px;" id="save-event-bootcamper" name="action">Submit
                <i class="material-icons right">send</i>
            </button>
        </form>

    <br>
    </div>

    <script>
        $(document).ready(function(){
            $('select').formSelect();

            $('#Edit-event-bootcamp').on('submit', function (e) {

                e.preventDefault();
                let formData = new FormData();
                formData.append('date_registered', $('#date_registered').val());
                formData.append('attended', $('#attended').val());
                formData.append('accepted', $('#accepted').val());

                console.log(formData);

                let url = '/updateBootcaperEvent/'+ '{{$eventBootcamper->id}}';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

        });
    </script>

    @endsection
