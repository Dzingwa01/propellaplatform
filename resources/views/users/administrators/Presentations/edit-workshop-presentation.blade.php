@extends('layouts.administrator-layout')

@section('content')
    <input id="presentation_id" disabled hidden value="{{$presentation->id}}">
    <div class="card" style="margin-top: 10vh;width: 900px;margin-left: 400px">
        <br>
        <h5 class="center-align"><b>UPDATE EVENT PRESENTATION</b></h5>
        <div class="row" style="margin-left: 7em">
            <div class="input-field col s10">
                <input id="title" type="text" value="{{$presentation->title}}" class="validate">
                <label for="title">Title</label>
            </div>
        </div>

        <div class="row" style="margin-left: 7em">
            <div class="file-field input-field col s8">
                <div class="btn">
                    <span>Upload Multiple Handout </span>
                    <input type="file" id="handout_url" multiple>
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate"
                           type="text" placeholder="Upload in PDF">
                </div>
            </div>
            <div class="col s4 input-field">
                <button class="btn blue " id="handout_upload_btn">Save</button>
            </div>
        </div>
        @if(isset($presentation->handouts_uploads))
            <div class="" style="margin-left: 8em">
                <h6><b>UPLOADED HANDOUTS</b></h6>
                <br>
                @foreach($presentation->handouts_uploads as $handout)
                    <div class="row">
                        <div class="col s8">
                            <input class="center-align" type="text" disabled id="date_of_signature_display" value="{{$handout->handout_url}}">
                        </div>
                        <div class="col l4">
                            <a style="cursor: pointer;" id="{{$handout->id}}" onclick="deleteDocument(this)" target="_blank">Delete</a>
                        </div>
                    </div>
                @endforeach
            </div>

        @endif

        <div class="row" style="margin-left: 100px">
            <div class="row">
                <div class="file-field input-field col s8">
                    <div class="btn">
                        <span>Upload Multiple Presentation </span>
                        <input type="file" id="presentation_url" multiple>
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate"
                               type="text" placeholder="Upload in PDF">
                    </div>
                </div>
                <div class="col s4 input-field">
                    <button class="btn blue " id="presentation_upload_btn">SAVE</button>
                </div>
            </div>
            @if(isset($presentation->presentation_uploads))
                <div class="">
                    <h6><b>UPLOADED PRESENTATIONS</b></h6>
                    <br>
                    @foreach($presentation->presentation_uploads as $presentation_uploads)
                        <div class="row">
                            <div class="col s8">
                                <input class="center-align" type="text" disabled id="date_of_signature_display"
                                       value="{{$presentation_uploads->presentation_url}}">
                            </div>
                            <div class="col l4">
                                <a style="cursor: pointer;" id="{{$presentation_uploads->id}}"
                                   onclick="deleteDocument(this)" target="_blank">Delete</a>
                            </div>
                        </div>
                    @endforeach
                </div>

            @endif
            <div class="col m10">
                <div class="input-field" style="bottom:0px!important;">
                    <input id="video" value="{{isset($presentation->video)?$presentation->video:''}}" type="text">
                    <label for="video">Video link</label>
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 600px;">
            <div class="col s4">
                <a class="waves-effect waves-light btn" id="presentation-update-button">UPDATE</a>
            </div>
        </div>
        <br>
    </div>
    <style>
        .check {
            opacity: 1 !important;
            pointer-events: auto !important;
        }

        table.dataTable tbody tr.selected {
            background-color: #B0BED9;
        }
    </style>


    @push('custom-scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.14.2/highlight.min.js"></script>
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>


        <script>
            $(document).ready(function () {
                $('#presentation-update-button').on('click', function () {
                    let presentation_id = $('#presentation_id').val();
                    let formData = new FormData();
                    formData.append('title', $('#title').val());
                    formData.append('video', $('#video').val());


                    let url = '/update-presentation/' + presentation_id;
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            $('#presentation-update-button').notify(response.message, "success");
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                });
                $('#handout_upload_btn').on('click', function (e) {
                    let formData = new FormData();
                    let presentation_id = $('#presentation_id').val();
                    jQuery.each(jQuery('#handout_url')[0].files, function (i, file) {
                        formData.append('handout_url', file);
                    });

                    let url = '/upload-multiple-handout/' + presentation_id;

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            $('#handout_upload_btn').notify(response.message, "success");
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                });

                $('#presentation_upload_btn').on('click', function (e) {
                    let formData = new FormData();
                    let presentation_id = $('#presentation_id').val();
                    jQuery.each(jQuery('#presentation_url')[0].files, function (i, file) {
                        formData.append('presentation_url', file);
                    });

                    let url = '/upload-multiple-presentation/' + presentation_id;

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            $('#presentation_upload_btn').notify(response.message, "success");
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                });

            });
            function deleteDocument(obj){
                let check = confirm("Are you sure want to delete this document?");
                if (check === true) {
                    $.get('/delete-presentation-document/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status === 'success') {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Delete action cancelled');
                }
            }
        </script>
    @endpush


@endsection








