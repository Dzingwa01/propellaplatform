@extends('layouts.administrator-layout')

@section('content')

    <div class="" style="margin-top: 10vh">
        @if(isset($date->title))
            <h4 class="center-align"><b>{{$date->title}}</b></h4>
        @endif
        <div class="row center-align">
            <div class="col s4">
                @if($date->video!=null)
                    <iframe style="margin-left: 1em" src="{{isset($date->video)?$date->video:''}}" width="300" height="200" allow="autoplay;" allowfullscreen></iframe>
                @endif
            </div>
            @if(isset($date->handouts_uploads))
                @foreach($date->handouts_uploads as $handout)
                    <div class="col s2">
                        <a href="{{'/storage/'.$handout->handout_url}}" target="_blank">
                            <img style="margin-left: 1em;" class="hoverable" src="/images/pdf_image.png" width="80"
                                 height="100">
                        </a>
                        <p>HANDOUT</p>
                    </div>
                @endforeach
            @endif
            @if(isset($date->presentation_uploads))
                @foreach($date->presentation_uploads as $presentation)
                    <div class="col s2">
                        <a href="{{'/storage/'.$presentation->presentation_url}}" target="_blank">
                            <img style="margin-left: 1em;" class="hoverable" src="/images/pdf_image.png" width="80"
                                 height="100">
                        </a>
                        <p>PRESENTATION</p>
                    </div>
                @endforeach
            @endif
        </div>
    </div>


    <script>
        $("#playvideo").click(function(){
            $("#video1")[0].src += "?autoplay=1";
        });
    </script>

@endsection
