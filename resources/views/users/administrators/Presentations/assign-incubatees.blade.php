@extends('layouts.administrator-layout')

@section('content')
    <input id="presentation_id" disabled hidden value="{{$presentation->id}}">
    <div class="container-fluid">
        <div id="top-section">
            <div class="row" style="margin-top: 10vh;margin-right: 4em;margin-left: 4em" >
                <span id="employees-selected-count">No recipients selected</span>
                <button class="btn waves-effect waves-light"   style="margin-right:2em;" id="assign-incubatee-button">ASSIGN
                    <i class="material-icons right">send</i>
                </button>
                <div class="input-field col s4">
                    <input id="start_date" type="date" required class="validate">
                    <label for="start_date">Start date</label>
                </div>
                <div class="input-field col s4">
                    <input id="end_date" type="date" required class="validate">
                    <label for="end_date">End date</label>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">All Propella Incubatees</h6>
                <div class="col s12">
                    <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                        <thead>
                        <tr id="selected-incubatee">
                            <th></th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Email</th>
                            <th>Contact Number</th>
                            <th>Status</th>
                            <th>Cohort</th>
                            <th>Stage</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <style>
        .check {
            opacity: 1 !important;
            pointer-events: auto !important;
        }

        table.dataTable tbody tr.selected {
            background-color: #B0BED9;
        }
    </style>


    @push('custom-scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.14.2/highlight.min.js"></script>
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>


        <script>
            $(document).ready(function () {

                $('select').formSelect();
                $(function () {
                    $('#users-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-ict-and-industrial')}}',
                        columns: [
                            {data: 'action', name: 'action', orderable: false, searchable: false},
                            {data:'name',name:'name'},
                            {data: 'surname', name: 'surname'},
                            {data: 'email', name: 'email'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'status', name: 'status'},
                            {data: 'cohort', name: 'cohort'},
                            {data: 'stage', name: 'stage'},
                        ]
                    });
                    $('select[name="users-table_length"]').css("display","inline");
                });
                $('body').on('click', '#selectAll', function () {
                    if ($(this).hasClass('#selected-recipients')) {
                        $('input[type="checkbox"]', '#users-table').prop('checked', false);
                        $(this).toggleClass('selected');
                        count_selected();
                    } else {
                        $('input[type="checkbox"]', '#users-table').prop('checked', true);
                        $(this).toggleClass('selected');
                        count_selected();
                    }
                    $(this).toggleClass('allChecked');
                });
                $('#users-table tbody').on('click', 'tr', function () {
                    $(this).toggleClass('selected');
                    count_selected();
                });


                $('#cancel-send').on('click',function(){
                    $('#top-section').show();
                    $('#bottom-section').hide();
                });

                function count_selected() {
                    var count = $("table input[type=checkbox]:checked").length;
                    $('#employees-selected-count').empty();
                    if (count <= 0) {
                        $('#send-mail').prop("disabled", true);
                        $('#employees-selected-count').append(' No Recipients Selected');
                    } else {
                        $('#send-mail').prop("disabled", false);
                        $('#employees-selected-count').append(count + ' Recipients Selected');
                    }
                }
            });

            $('#assign-incubatee-button').on('click', function(){
                $(this).text("busy...");
                /* let selected_ventures = [];*/
                let formData = new FormData();

                let selected_ventures = $("table input:checkbox:checked").map(function () {
                    return $(this).val();
                }).get();

                /*jQuery.each(jQuery('allChecked').val(), function (i, value) {
                    selected_ventures.push(value);
                });*/

                formData.append('selected_ventures', JSON.stringify(selected_ventures));
                formData.append('start_date', $('#start_date').val());
                formData.append('end_date', $('#end_date').val());
                formData.append('presentation_id', $('#presentation_id').val());

                let url = "/assign";

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#assign-incubatee-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        $('#incubatee-button').notify(response.message, "error");
                    }
                });
            });

            $('#presentation-update-button').on('click', function () {

                let formData = new FormData();
                formData.append('title', $('#title').val());
                formData.append('status', $('#status').val());
                formData.append('video', $('#video').val());
                jQuery.each(jQuery('#handout')[0].files, function (i, file) {
                    formData.append('handout', file);
                });
                jQuery.each(jQuery('#presentation')[0].files, function (i, file) {
                    formData.append('presentation', file);
                });
                /*jQuery.each(jQuery('#audio')[0].files, function (i, file) {
                    formData.append('audio', file);
                });*/
                let url = '/update-presentation/' + $('#presentation_id').val();
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#presentation-update-button').notify(response.message, "success");
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
            $('#handout_upload_btn').on('click', function(e){
                let formData = new FormData();
                let presentation_id = $('#presentation_id').val();
                jQuery.each(jQuery('#handout_url')[0].files, function (i, file) {
                    formData.append('handout_url', file);
                });

                let url = '/upload-multiple-handout/' + presentation_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#handout_upload_btn').notify(response.message, "success");
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            $('#presentation_upload_btn').on('click', function(e){
                let formData = new FormData();
                let presentation_id = $('#presentation_id').val();
                jQuery.each(jQuery('#presentation_url')[0].files, function (i, file) {
                    formData.append('presentation_url', file);
                });

                let url = '/upload-multiple-presentation/' + presentation_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#presentation_upload_btn').notify(response.message, "success");
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });


            function deleteDocument(obj){
                let check = confirm("Are you sure want to delete this document?");
                if (check === true) {
                    $.get('/delete-presentation-document/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status === 'success') {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Delete action cancelled');
                }
            }
        </script>
    @endpush

@endsection
