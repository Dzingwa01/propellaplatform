@extends('layouts.administrator-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-all-event-venue')}}
    <br>
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">All Event Venues</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="blog-table">
                    <thead>
                    <tr>
                        <th>Venue Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Blog" href="{{url('create-venue')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>

    </div>
    @push('custom-scripts')

        <script>

            $(document).ready(function (data) {
                $(function () {
                    $('#blog-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '/get-all-event-venues',
                        columns: [
                            {data: 'venue_name', name: 'venue_name'},
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="blog-table_length"]').css("display","inline");
                });
            });
            function confirm_delete(obj){
                var r = confirm("Are you sure want to delete this event venue!");
                if (r == true) {
                    $.get('/delete-event-venue/'+obj.id,function(data,status){
                        console.log('Data',data);
                        console.log('Status',status);
                        if(status=='success'){
                            alert(data.message);
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }
            }
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
