@extends('layouts.administrator-layout')
@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-all-events')}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Events</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="events-table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(function () {
                $('#events-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    stateSave: true,
                    pageLength: 100,
                    ajax: '{{route('get-events')}}',
                    columns: [
                        {data: 'title', name: 'title'},
                        {data: 'start', name: 'start'},
                        {data: 'end', name: 'end'},
                        {data: 'description', name: 'description'},
                        {data: 'eventStatus', name: 'eventStatus'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                $('select[name="events-table_length"]').css("display","inline");
            });
        });
        function confirm_delete_events(obj){
            var r = confirm("Are you sure want to delete this event?");
            if (r == true) {
                $.get('/delete-event/'+obj.id,function(data,status){
                    console.log('Data',data);
                    console.log('Status',status);
                    if(status=='success'){
                        alert(data.message);
                        window.location.reload();
                    }

                });
            } else {
                alert('Delete action cancelled');
            }
        }

    </script>
@endsection
