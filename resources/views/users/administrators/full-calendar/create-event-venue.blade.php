@extends('layouts.administrator-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-event-venue')}}
    <br>
    <br>
    <div class="section" id="createDesktop">
        <div class="card hoverable center" style="width: 800px; margin-left: 350px">
            <form class="col s12" id="add-booking" method="post" st>
                <br/>
                <br/>
                <h4 style="font-size: 2em;font-family: Arial;">Add New Event Venue</h4>
                @csrf
                <div class="row" style="margin-left: 2em; margin-right: 2em;">
                    <div class="input-field col m6">
                        <input id="venue_name" type="text" class="validate">
                        <label for="venue_name">Propella Venue Name</label>
                    </div>
                </div>
                <div class="row" style="margin-left: 2em; margin-right: 2em;">
                    <a href="#!" class="modal-close waves-effect waves-green btn">Cancel<i class="material-icons right">close</i>
                    </a>

                    <button class="btn waves-effect waves-light" style="margin-left:300px;" id="save-user"
                            name="action">
                        Submit
                        <i class="material-icons right">send</i>
                    </button>
                </div>

            </form>
            <br/>
            <br/>
        </div>
    </div>
    @push('custom-scripts')
        <script>
            $(document).ready(function () {
                $('.tooltipped').tooltip();
                $('select').formSelect();

                $('#add-booking').on('submit', function (e) {

                    e.preventDefault();
                    let formData = new FormData();
                    formData.append('venue_name', $('#venue_name').val());
                    console.log(formData);

                    $.ajax({
                        url: "/venue-event-store",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            console.log("success", response);
                            $('#save-user').notify(response.message, "success");

                            setTimeout(function(){
                            }, 3000);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection

