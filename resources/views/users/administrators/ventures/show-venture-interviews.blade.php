@extends('layouts.administrator-layout')

@section('content')

    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Venture Interviews</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <input id="user-id-input" hidden disabled value="{{$logged_in_user->id}}">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="venture-interviews-table">
                    <thead>
                    <tr>
                        <th>Venture Name</th>
                        <th>Panel Selection Date</th>
                        <th>Panel Selection Time</th>
                        <th>Interview Category</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            let user_id = $('#user-id-input').val();
            $(function () {
                $('#venture-interviews-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/panelist-get-venture-interviews/' + user_id,
                    columns: [
                        {data: 'venture_name', name: 'venture_name'},
                        {data: 'panel_selection_date', name: 'panel_selection_date'},
                        {data: 'panel_selection_time', name: 'panel_selection_time'},
                        {data: 'interview_category', name: 'interview_category'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="venture-interviews-table_length"]').css("display","inline");
            });
        });


    </script>

@endsection
