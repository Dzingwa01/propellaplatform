@extends('layouts.administrator-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-edit-status')}}
    <br>
    <div class="section">
        <div class="row center">
            <input hidden disabled id="venture-status-id-input" value="{{$ventureStatus->id}}">
            <h4>Edit venture status</h4>
            <input type="text" style="width:50%;" id="venture-status-name-input" value="{{$ventureStatus->status_name}}">
            <br>
            <button class="btn blue" id="update-venture-status-button">Update</button>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#update-venture-status-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('status_name', $('#venture-status-name-input').val());

                let venture_status_id = $('#venture-status-id-input').val();
                let url = '/update-venture-status/' + venture_status_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#venture-status-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/venture-status';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
