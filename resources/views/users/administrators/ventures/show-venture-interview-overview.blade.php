@extends('layouts.administrator-layout')

@section('content')

    <br>
    <br>
    <div class="section">
        <input hidden disabled id="venture-id-input" value="{{$venture->id}}">
        <div class="row center" style="background-color: lightslategray">
            <h6 style="color: white;">Select a category</h6>
        </div>
        <div class="row" style="margin-right: 8em; margin-left: 8em;">
            <select id="venture-panel-interview-category-input">
                <option value="" disabled selected>Choose Panel Interview Category</option>
                @foreach($venture_interview_categories as $item)
                    <option value="{{$item->id}}">{{$item->category_name}}</option>
                @endforeach
            </select>
            <label for="venture-panel-interview-category-input">Panel Interview Category</label>
        </div>
        <div class="row center">
            <a class="btn blue" id="submit-venture-interview-category-button">Get Information</a>
        </div>


        <div id="content-div" class="row center">
        </div>
    </div>


    <script>
        let venture_id = $('#venture-id-input').val();
        $(document).ready(function () {
            $('select').formSelect();
        });

        $('#submit-venture-interview-category-button').on('click', function(){
            if($('#venture-panel-interview-category-input').val() === null){
                alert('Please select a category that you would like to see the information of.');
            } else {
                $(this).text("Please wait...");
                let venture_id = $('#venture-id-input').val();
                let venture_interview_category_id = $('#venture-panel-interview-category-input').val();
                let formData = new FormData();
                formData.append('venture_interview_category_id', venture_interview_category_id);
                formData.append('venture_id', venture_id);

                let url = "/admin-get-venture-panel-interview-overview/" + venture_interview_category_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#content-div').html("");
                        $('#submit-venture-interview-category-button').text('Get Information');
                        $('#content-div').append(
                            response.view_data
                        );
                    },
                    error: function (response) {
                        alert(response.message);
                    }
                });
            }
        })

    </script>
@endsection
