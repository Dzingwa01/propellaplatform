@extends('layouts.administrator-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-edit-type')}}
    <br>
    <div class="section">
        <div class="row center">
            <input hidden disabled id="venture-type-id-input" value="{{$ventureType->id}}">
            <h4>Add a venture type</h4>
            <input type="text" style="width:50%;" id="venture-type-name-input" value="{{$ventureType->type_name}}">
            <br>
            <button class="btn blue" id="update-venture-type-button">Update</button>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#update-venture-type-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('type_name', $('#venture-type-name-input').val());

                let venture_category_id = $('#venture-type-id-input').val();
                let venture_type_id;
                let url = '/update-venture-type/' + venture_type_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#venture-type-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/venture-type';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
