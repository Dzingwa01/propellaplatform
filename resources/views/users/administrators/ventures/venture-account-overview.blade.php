@extends('layouts.administrator-layout')

@section('content')

    <div class="section" style="margin-top: 5vh">
        <input id="user-id-input" value="{{$venture->id}}" hidden disabled>
        <div class="col s10 card z-depth-4" style="border-radius: 10px;width: 1350px;margin: 0 auto">
            {{--APPPLICATION ACOUNT INFO--}}
            <div class="row" id="applicant-account-show" style="margin-right: 3em; margin-left: 3em;">
                <br>
                <h4 class="center"><b>OVERVIEW OF : {{$venture->company_name}}</b></h4>
                <div class="row " style="margin-left: 10em">
                    <br>
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h6><b>Hub : </b> {{$venture->hub}}</h6>
                            <h6><b>Stage </b> {{$venture->stage}}</h6>
                            <h6><b>Cohort : </b> {{$venture->cohort}}</h6>
                            <h6><b>Status : </b> {{$venture->status}}</h6>
                            <h6><b>Venture email : </b> {{$venture->venture_email}}</h6>
                        </div>
                        <div class="col l6 m6 s12">
                            <h6><b>Address One : </b> {{$venture->physical_address_one}}</h6>
                            <h6><b>Address Two </b> {{$venture->physical_address_two}}</h6>
                            <h6><b>Address Three : </b> {{$venture->physical_address_two}}</h6>
                            <h6><b>Smart city tag : </b> {{$venture->smart_city_tags}}</h6>
                            <h6><b>Sustainable development goal : </b> {{$venture->sustainable_development_goals}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s2"></div>
        <div class="row center">
            <h4><b>ACTIONS</b></h4>
        </div>
        <div class="row" style="margin-left: 500px">
            <div class="card col s2" id="ownership" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">OWNERSHIP</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2" id="uploads" style="background-color:#454242;font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">UPLOADS</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="shadowboards" style="background-color:rgba(0, 121, 52, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">SHADOWBOARD</p>
            </div>
        </div>
    </div>

    <div class="section" style="margin-top: 5vh">
        <div class="col s12 card" id="venture-ownership" hidden style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <br>
            <!--ownership-->
            <div class="row"  style="margin-right: 3em; margin-left: 3em;">
                <div class="row center">
                    <h5><b>VENTURE OWNERSHIP</b></h5>
                </div>
                <p>Registration Number : {{$venture->ventureOwnership->registration_number}}</p>
                <p>BBBEE Level : {{$venture->ventureOwnership->BBBEE_level}}</p>
                <p>Ownership : {{$venture->ventureOwnership->ownership}}</p>
            </div>
        </div>
        <div class="col s12 card" id="venture-uploads" hidden
             style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <br>
            <!--uploads-->
            <div class="row" style="margin-right: 3em; margin-left: 3em;">
                <div class="row center">
                    <h5><b>VENTURE UPLOADS</b></h5>
                </div>
                {{-- Evaluation form--}}
                <div class="col s12  row" id="incubatee-evaluation">
                    <br>
                    <table>
                        <tr>
                            <th>Venture Uploads</th>
                            <th>Uploaded</th>
                            <th>Action</th>
                        </tr>
                        {{--Video shot--}}
                        <tr>
                            <td>Venture Video shot</td>
                            @if($venture->ventureUploads->video_shot_url == null)
                                <td>No</td>
                            @else
                                <td href="{{$venture->ventureUploads->video_shot_url}}">Yes</td>
                            @endif
                            @if($venture->ventureUploads->video_shot_url == null)
                                <td>None</td>
                            @else
                                <td><a href="{{$venture->ventureUploads->video_shot_url}}" target="_blank">View</a></td>
                            @endif
                        </tr>
                        {{--Satge 2 to 3 Video shot--}}
                        <tr>
                            <td>Stage 2 to 3 Video shot</td>
                            @if($venture->ventureUploads->stage_two_to_three_video_url == null)
                                <td>No</td>
                            @else
                                <td href="{{$venture->ventureUploads->stage_two_to_three_video_url}}">Yes</td>
                            @endif
                            @if($venture->ventureUploads->stage_two_to_three_video_url == null)
                                <td>None</td>
                            @else
                                <td><a href="{{$venture->ventureUploads->stage_two_to_three_video_url}}" target="_blank">View</a></td>
                            @endif
                        </tr>
                        {{--Satge 3 to 4 Video shot--}}
                        <tr>
                            <td>Stage 3 to 4 Video shot</td>
                            @if($venture->ventureUploads->stage_three_to_four_video == null)
                                <td>No</td>
                            @else
                                <td href="{{$venture->ventureUploads->stage_three_to_four_video}}">Yes</td>
                            @endif
                            @if($venture->ventureUploads->stage_three_to_four_video == null)
                                <td>None</td>
                            @else
                                <td><a href="{{$venture->ventureUploads->stage_three_to_four_video}}" target="_blank">View</a></td>
                            @endif
                        </tr>
                        {{--Satge 4 to 5 Video shot--}}
                        <tr>
                            <td>Stage 4 to 5 Video shot</td>
                            @if($venture->ventureUploads->stage_four_to_five_video == null)
                                <td>No</td>
                            @else
                                <td href="{{$venture->ventureUploads->stage_four_to_five_video}}">Yes</td>
                            @endif
                            @if($venture->ventureUploads->stage_four_to_five_video == null)
                                <td>None</td>
                            @else
                                <td><a href="{{$venture->ventureUploads->stage_four_to_five_video}}" target="_blank">View</a></td>
                            @endif
                        </tr>
                        {{-- Infographic--}}
                        <tr>
                            <td>Infographics</td>
                            @if($venture->ventureUploads->infographic_url == null)
                                <td>No</td>
                            @else
                                <td href="{{'/storage/'.$venture->ventureUploads->infographic_url}}">Yes</td>
                            @endif
                            @if($venture->ventureUploads->infographic_url == null)
                                <p>None</p>
                            @else
                                <td><a href="{{'/storage/'.$venture->ventureUploads->infographic_url}}" target="_blank">View</a>
                                </td>
                            @endif
                        </tr>
                        {{-- Crowd funding--}}
                        <tr>
                            <td>Crowd funding</td>
                            @if($venture_upload->crowdfunding_url == null)
                                <td>No</td>
                            @else
                                <td href="{{'/storage/'.$venture_upload->crowdfunding_url}}">Yes</td>
                            @endif
                            @if($venture_upload->crowdfunding_url == null)
                                <td>None</td>
                            @else
                                <td><a href="{{'/storage/'.$venture_upload->crowdfunding_url}}" target="_blank">View</a>
                                </td>
                            @endif
                        </tr>
                        {{-- Contracts--}}
                        <tr>
                            <td>Contracts</td>
                            <td>Yes</td>
                            <td><a class="waves-effect waves-light  modal-trigger" href="#client-contract">View</a>
                            <td>
                        </tr>
                        {{--Market research--}}
                        <tr>
                            <td>Market research</td>
                            @if($venture_upload->market_research_upload == null)
                                <td>No</td>
                            @else
                                <td href="{{'/storage/'.$venture_upload->market_research_upload}}">Yes</td>
                            @endif
                            @if($venture_upload->market_research_upload == null)
                                <td>None</td>
                            @else
                                <td><a href="{{'/storage/'.$venture_upload->market_research_upload}}" target="_blank">View</a>
                                </td>
                            @endif
                        </tr>
                        {{--Project Plan--}}
                        <tr>
                            <td>Project Plan</td>
                            @if($venture_upload->project_plan_upload == null)
                                <td>No</td>
                            @else
                                <td href="{{'/storage/'.$venture_upload->project_plan_upload}}">Yes</td>
                            @endif
                            @if($venture_upload->project_plan_upload == null)
                                <td>None</td>
                            @else
                                <td><a href="{{'/storage/'.$venture_upload->market_research_upload}}" target="_blank">View</a>
                                </td>
                            @endif
                        </tr>
                        {{--Business plan--}}
                        <tr>
                            <td>Business plan</td>
                            @if($venture_upload->business_plan == null)
                                <td>No</td>
                            @else
                                <td href="{{'/storage/'.$venture_upload->business_plan}}">Yes</td>
                            @endif
                            @if($venture_upload->business_plan == null)
                                <td>None</td>
                            @else
                                <td><a href="{{'/storage/'.$venture_upload->business_plan}}" target="_blank">View</a>
                                </td>
                            @endif
                        </tr>
                        {{--BBBE Certificate--}}
                        <tr>
                            <td>BBBE Certificate</td>
                            @if($venture_upload->bbbee_certificate_url == null)
                                <td>No</td>
                            @else
                                <td href="{{'/storage/'.$venture_upload->bbbee_certificate_url}}">Yes</td>
                            @endif
                            @if($venture_upload->bbbee_certificate_url == null)
                                <td>None</td>
                            @else
                                <td><a href="{{'/storage/'.$venture_upload->bbbee_certificate_url}}" target="_blank">View</a>
                                </td>
                            @endif
                        </tr>
                        {{--Dormant affidavity--}}
                        <tr>
                            <td>Dormant affidavity</td>
                            @if($venture_upload->dormant_affidavit_url == null)
                                <td>No</td>
                            @else
                                <td href="{{'/storage/'.$venture_upload->dormant_affidavit_url}}">Yes</td>
                            @endif
                            @if($venture_upload->dormant_affidavit_url == null)
                                <td>None</td>
                            @else
                                <td><a href="{{'/storage/'.$venture_upload->dormant_affidavit_url}}" target="_blank">View</a>
                                </td>
                            @endif
                        </tr>
                        {{--Management account/ CK documents--}}
                        <tr>
                            <td>Management account/ CK documents</td>
                            <td>Yes</td>
                            <td><a class="waves-effect waves-light  modal-trigger" href="#client-CK">View</a>
                            <td>
                        </tr>
                        {{--Tax clearance--}}
                        <tr>
                            <td>Tax clearance</td>
                            @if($venture_upload->tax_clearance_url == null)
                                <td>No</td>
                            @else
                                <td href="{{'/storage/'.$venture_upload->tax_clearance_url}}">Yes</td>
                            @endif
                            @if($venture_upload->tax_clearance_url == null)
                                <td>None</td>
                            @else
                                <td><a href="{{'/storage/'.$venture_upload->tax_clearance_url}}"
                                       target="_blank">View</a></td>
                            @endif
                        </tr>
                        {{--Coporate ID--}}
                        <tr>
                            <td>Corporate ID</td>
                            @if($venture_upload->corporate_id_url == null)
                                <td>No</td>
                            @else
                                <td href="{{'/storage/'.$venture_upload->corporate_id_url}}">Yes</td>
                            @endif
                            @if($venture_upload->corporate_id_url == null)
                                <td>None</td>
                            @else
                                <td><a href="{{'/storage/'.$venture_upload->corporate_id_url}}"
                                       target="_blank">View</a></td>
                            @endif
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal Structure for client contract-->
        <div id="client-contract" class="modal">
            <br>
            <h4 class="center-align">Venture contracts</h4>
            <div class="modal-content">
                <div class="row center" style="border: 1px solid black;">
                    <br>
                    @foreach($venture_upload->venture_client_contract->sortByDesc('date_of_signature') as $client_contract)
                        <div class="row">
                            <div class="col l2">
                                <input class="center-align" type="text" disabled id="date_of_signature_display" value="{{$client_contract->contract_stage}}">
                                <label for="date_of_signature_display">Stage</label>
                            </div>
                            <div class="col l6">
                                <input class="file-path validate" disabled value="{{$client_contract->client_contract_url}}" type="text" placeholder="Upload in PDF">
                                <label for="date_of_signature_display">Contract</label>
                            </div>
                            <div class="col l2">
                                <input class="center-align" type="text" disabled id="date_of_signature_display" value="{{$client_contract->date_of_signature}}">
                                <label for="date_of_signature_display">Date of Signature</label>
                            </div>
                            <div class="col l2">
                                <a style="cursor: pointer;" href="{{'/storage/'.$client_contract->client_contract_url}}" target="_blank">View</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure for client ck document-->
        <div id="client-CK" class="modal">
            <br>
            <h4 class="center-align">Management account/ CK documents</h4>
            <div class="modal-content">
                <div class="row center" style="border: 1px solid black;">
                    <br>
                    @if(isset($venture_upload->venture_ck_documents))
                        <div class="row center">
                            <br>
                            @foreach($venture_upload->venture_ck_documents as $ck_documents)
                                <div class="row">
                                    <div class="col l6">
                                        <input class="file-path validate" disabled
                                               value="{{$ck_documents->ck_documents_url}}"
                                               type="text" placeholder="Upload in PDF">
                                    </div>
                                    <div class="col l6">
                                        <a style="margin-left: 1em; cursor: pointer;" href="{{'/storage/'.$ck_documents->ck_documents_url}}" target="_blank">View</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <div class="col s12 card" id="venture-shadowboard" hidden style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            <br>
            <div class="row">
                <div class="row center">
                    <h5><b>VENTURE SHADOWBOARDS</b></h5>
                </div>
                <table id="table" style="width:93%;margin-left: 2em;margin-right: 8em">
                    <tr>
                        <th>Venture Name</th>
                        <th>Shadowboard Date</th>
                        <th>Shadowboard Time</th>
                    </tr>
                    @foreach($shadow_array as $shadow_arrays)
                        <tr>
                            <td>{{$shadow_arrays->company_name}}</td>
                            <td>{{$shadow_arrays->shadow_board_date}}</td>
                            <td>{{$shadow_arrays->shadow_board_time}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });
        $('#ownership').on('click', function () {
            $('#venture-ownership').show();
            $('#venture-uploads').hide();
            $('#venture-shadowboard').hide();
        });
        $('#uploads').on('click', function () {
            $('#venture-uploads').show();
            $('#venture-ownership').hide();
            $('#venture-shadowboard').hide();
        });
        $('#shadowboards').on('click', function () {
            $('#venture-shadowboard').show();
            $('#venture-ownership').hide();
            $('#venture-uploads').hide();
        });
    </script>
@endsection
