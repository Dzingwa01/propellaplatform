@extends('layouts.administrator-layout')

@section('content')
    <div class="" style="margin-top: 10vh">
        <div class="container-fluid" style="margin-left: 10em; margin-right: 10em;">
            <h4 class="center-align"><B>UPDATE INTERVIEW TIME AND DATE</B></h4>
            <br>
            <input id="venture-id-input" value="{{$venturePanelInterview->id}}" hidden disabled>
            <div class="row center">
                <div class="col l6 m6 s12">
                    <label for="panel-selection-date-input">Date of panel interview</label>
                    <input type="date" value="{{$venturePanelInterview->panel_selection_date}}" id="panel-selection-date-input">
                </div>
                <div class="col l6 m6 s12">
                    <label for="panel-selection-time-input">Time of panel interview</label>
                    <input type="time" value="{{$venturePanelInterview->panel_selection_time}}" id="panel-selection-time-input">
                </div>
            </div>
            <div class="row center-align">
                <button class="btn blue" id="update-panel-interview-button">Finalize</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            let venture_panel_interview_id = $('#venture-id-input').val();
            $('select').formSelect();

            $('#update-panel-interview-button').on('click', function () {
                let formData = new FormData();
                formData.append('panel_selection_date', $('#panel-selection-date-input').val());
                formData.append('panel_selection_time', $('#panel-selection-time-input').val());
                formData.append('question_category_id', $('#question-category-input').val());
                formData.append('venture_panel_interview_category_id', $('#venture-panel-interview-category-input').val());


                let url = "/update-venture-panel-interview/" + venture_panel_interview_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#update-panel-interview-button').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        $('#update-panel-interview-button').notify(response.message, "error");
                    }
                });

            });
        });
    </script>

@endsection
