@extends('layouts.administrator-layout')

@section('content')
    <input id="user-id-input" disabled hidden value="{{$smartTag->id}}">
    <br><br>
{{--    {{ Breadcrumbs::render('administrator-edit-smart-tag')}}--}}
    <br>
    <div class="section">
        <div class="row center">
            <input hidden disabled id="smart-tag-id-input" value="{{$smartTag->id}}">
            <h4>Edit smart tag</h4>
            <input type="text" style="width:50%;" id="smart-tag-name-input" value="{{$smartTag->smart_tag_name}}">
            <br>
            <button class="btn blue" id="update-smart-tag-button">Update</button>
        </div>
    </div>

    <script>
        //js to update selected smart tag
        $(document).ready(function(){
            $('#update-smart-tag-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('smart_tag_name', $('#smart-tag-name-input').val());

                let smart_tag_id = $('#smart-tag-id-input').val();
                let url = '/update-smart-tag/' + smart_tag_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    //js for system feedback
                    success: function (response, a, b) {
                        $("#smart-tag-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/smart-tag';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection

