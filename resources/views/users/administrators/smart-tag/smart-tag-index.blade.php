@extends('layouts.administrator-layout')

@section('content')
    <br>
    <br>
{{--    {{ Breadcrumbs::render('administrator-smart-tags')}}--}}
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">All Smart Tags</h6>
        </div>
        <br>

        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="smart-tags-table">
                    <thead>
                    <tr>
                        <th>Smart Tag</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
               data-tooltip="Add New Smart Tag" href="{{url('create-smart-tag')}}">
                <i class="large material-icons">add</i>
            </a>
        </div>

    </div>
    @push('custom-scripts')

        <script type="text/javascript"
                src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>

        <script type="text/javascript" language="javascript"
                src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>

        <script>
            //table displaying all records
            $(document).ready(function (data) {
                $(function () {
                    $('#smart-tags-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type: 'get',
                        scrollX: 640,
                        ajax: '{{route('get-smart-tag')}}',
                        columns: [
                            {data: 'smart_tag_name', name: 'smart_tag_name'},
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="smart_tags_length"]').css("display", "inline");
                });
            });
        </script>
    @endpush

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
