@extends('layouts.administrator-layout')

@section('content')
    <br>
<br>
{{ Breadcrumbs::render('administrator-visitor')}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;" id="page-header">Visitors</h6>
            <div class="row center">
                <a class="btn btn-success purple"
                   onclick="exportVisitors()"
                >Download Visitors
                </a>
            </div>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="visitors-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Last Visit</th>
                        <th>Cell number</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <div class="fixed-action-btn">
                        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Visitor" href="{{url('/visitors/create-visitor')}}">
                            <i class="large material-icons">add</i>
                        </a>

                    </div>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#visitors-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-visitors')}}',
                    columns: [
                        {data: 'first_name', name: 'first_name'},
                        {data: 'last_name', name: 'last_name'},
                        {data: 'email', name: 'email'},
                        {data: 'date_in', name: 'date_in'},
                        {data: 'cell_number', name: 'cell_number'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="visitors-table_length"]').css("display","inline");
            });
        });

        function deleteConfirm(obj) {
            let r = confirm("Are you sure want to delete this visitor?");
            if (r == true) {
                $.get('/visitor-delete-check/' + obj.id, function (data, status) {
                    if (status == 'success') {
                        $("#page-header").notify(data.message, "success");

                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    }
                });
            } else {
                $("#page-header").notify('Delete action cancelled', "info");
            }
        }

        function exportVisitors(){
            window.location.href = '/export-all-visitors';
        }

    </script>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
