@extends('layouts.administrator-layout')

@section('content')

    <br>
    <br>

    <div class="container">
        <input hidden disabled value="{{$visitorDeregister->id}}" id="visitor-deregister-id-input">
        <label for="de-register-reason-input">De-register Reason</label>
        <textarea id="de-register-reason-input">{{$visitorDeregister->de_register_reason}}</textarea>
        <div class="row right">
            <button class="btn blue" id="submit-visitor-de-registered-event-update-button">Update</button>
        </div>
    </div>

    <script>
        $('#submit-visitor-de-registered-event-update-button').on('click', function(){
            let visitor_deregister_id = $('#visitor-deregister-id-input').val();
            let url = '/admin-update-visitor-deregistered-event/' + visitor_deregister_id;
            let formData = new FormData();
            formData.append('de_register_reason', $("#de-register-reason-input").val());

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-visitor-de-registered-event-update-button').notify(response.message, "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    $('#submit-visitor-de-registered-event-update-button').notify(response.message, "error");
                }
            });
        });

    </script>

@endsection
