@extends('layouts.admin-layout')

@section('content')
    <input id="user-id-input" disabled hidden value="{{$businessCategory->id}}">
    <br><br>
    {{ Breadcrumbs::render('admin-edit-type',$businessCategory)}}
    <div class="section">
        <div class="row center">
            <input hidden disabled id="business-category-id-input" value="{{$businessCategory->id}}">
            <h4>Add a business category</h4>
            <input type="text" style="width:50%;" id="business-category-name-input" value="{{$ventureType->category_name}}">
            <br>
            <button class="btn blue" id="update-business-category-button">Update</button>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#update-business-category-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('category_name', $('#business-category-name-input').val());

                let business_category_id = $('#business-category-id-input').val();
                let url = '/update-business-category/' + business_category_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#business-category-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/business-category';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
