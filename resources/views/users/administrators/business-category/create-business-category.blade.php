@extends('layouts.admin-layout')

@section('content')
    <br><br>
    <div class="section">
        <div class="row center">
            <h4>Add a business category</h4>
            <input type="text" style="width:50%;" id="business-category-name-input" placeholder="Enter business category here">
            <br>
            <button class="btn blue" id="submit-business-category-button">Save</button>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#submit-business-category-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('category-name', $('#business-category-name-input').val());

                let url = 'store-business-category';

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#business-category-name-input").notify(response.message, "success");

                        setTimeout(function(){
                             window.location.href = 'business-category';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
