@extends('layouts.administrator-layout')

@section('content')
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">VISITORS COVID-19 FORM</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="careers-table">
                    <thead>
                    <tr>
                        <th>Full names</th>
                        <th>ID Number</th>
                        <th>Contact</th>
                        <th>Address</th>
                        <th>Employer</th>
                        <th>Age</th>
                        <th>Temperature</th>
                        <th>Med History</th>
                        <th>Travel</th>
                        <th>Recent medical care</th>
                        <th>Current signs</th>
                        <th>Signature</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
    @push('custom-scripts')

        <script>

            $(document).ready(function (data) {
                $(function () {
                    $('#careers-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '{{route('get-forms')}}',
                        columns: [
                            {data: 'fist_and_last_name', name: 'fist_and_last_name'},
                            {data: 'id_number', name: 'id_number'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'street_address', name: 'street_address'},
                            {data: 'employer', name: 'employer'},
                            {data: 'age', name: 'age'},
                            {data: 'temperature', name: 'temperature'},
                            {data: 'medical_history', name: 'medical_history'},
                            {data: 'travel', name: 'travel'},
                            {data: 'recent_care', name: 'recent_care'},
                            {data: 'signs_symptoms', name: 'signs_symptoms'},
                            {data: 'signature', name: 'signature'},
                            {data: 'date', name: 'date'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="careers-table_length"]').css("display","inline");
                });
            });

            function confirm_delete_form(obj){
                var r = confirm("Are you sure want to delete this form?");
                if (r == true) {
                    $.get('/delete-form/'+obj.id,function(data,status){
                        console.log('Data',data);
                        console.log('Status',status);
                        if(status=='success'){
                            alert(data.message);
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }

            }

        </script>
    @endpush
@endsection
