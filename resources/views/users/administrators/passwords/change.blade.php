@extends('layouts.administrator-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="css/site.css" media="screen,projection"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>


    <div class="container">
        <div class="row">
            <div class="register-form" style=" margin:auto;
        width: 400px;
        margin-top:5%;">

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <form class="col s12 card" method="POST" action="{{ route('updatePassword') }}">
                    @csrf
                    <div class="center">
                        <a href="/"><img style="width: 40%" src="/images/Propella_Logo.jpg"/></a>
                    </div>
                    <div>
                        <h5 class="center-align">Update Password</h5>
                    </div>
                    <div class="row" style="padding-left: 1em;padding-right: 1em;">
                        <div class="input-field col s12{{ $errors->has('current_password') ? ' has-error' : '' }}">
                            <input required name="current_password" id="current_password" type="password" class="validate">
                            @if ($errors->has('current_password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('current_password') }}</strong>
                                    </span>
                            @endif

                            <label for="current_password">Current Password</label>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 1em;padding-right: 1em;">
                        <div class="input-field col s12{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input required name="password" id="password" type="password" class="validate">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif

                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 1em;padding-right: 1em;">
                        <div class="input-field col s12{{ $errors->has('password_confirm') ? ' has-error' : '' }}">
                            <input required name="password-confirm" id="password-confirm" type="password"
                                   class="validate">

                            @if ($errors->has('password-confirm'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                            @endif

                            <label for="password-confirm">Repeat Password</label>
                        </div>
                    </div>

                    <div class="row" style="padding-left: 1em;padding-right: 1em;">
                        <button class="btn right" type="submit">Change Password</button>
                    </div>
                    {{--<div class="row" style="padding-left: 1em;padding-right: 1em;;">
                        <a class="right" href="login">Have an account? Login Now</a>
                    </div>--}}
                </form>
            </div>
        </div>
    </div>

    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });
    </script>
@endsection
