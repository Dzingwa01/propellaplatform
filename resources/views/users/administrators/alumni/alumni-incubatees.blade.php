@extends('layouts.administrator-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-alumni-incubatee')}}
    <div class="container-fluid">
        <br>
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Alumni Incubatees</h6>
        </div>
        <br>
        <div class="row" style="margin-left: 2em; margin-right: 2em;">
            <a href="/home" class="modal-close waves-effect waves-green btn">Home<i
                    class="material-icons right">home</i>
            </a>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
               data-tooltip="Add New Incubatee" href="{{url('create-incubatee')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript"
                src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>

        <script type="text/javascript" language="javascript"
                src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
        <script>
            $(document).ready(function () {
                $(function () {
                    $('#users-table').DataTable({
                        dom: 'lBfrtip',
                        buttons: [
                            {
                                extend: 'excel',
                                text: 'Export excel',
                                className: 'exportExcel',
                                filename: 'Export excel',
                                exportOptions: {
                                    modifier: {
                                        page: 'all'
                                    }
                                }
                            }],
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-alumni-incubatees')}}',
                        columns: [
                            {data: 'user.name', name: 'user.name'},
                            {data: 'user.surname', name: 'user.surname'},
                            {data: 'user.email', name: 'user.email'},
                            {data: 'user.contact_number', name: 'user.contact_number'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ],

                    });
                    $('select[name="users-table_length"]').css("display", "inline");
                });

            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    @endsection
