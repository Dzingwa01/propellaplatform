@extends('layouts.administrator-layout')

@section('content')
    <div class="section" style="margin-top: 5vh">
        <input id="user-id-input" value="{{$incubatee->id}}" hidden disabled>
        <div class="col s10 card z-depth-4" style="border-radius: 10px;width: 1350px;margin: 0 auto">
            {{--APPPLICATION ACOUNT INFO--}}
            <div class="row" id="applicant-account-show" style="margin-right: 3em; margin-left: 3em;">
                <br>
                <h4 class="center"><b>OVERVIEW OF : {{$incubatee->user->name}} {{$incubatee->user->surname}}</b></h4>
                <div class="row " style="margin-left: 10em">
                    <br>
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h6><b>Email : </b> {{$incubatee->user->email}}</h6>
                            <h6><b>Contact Number : </b> {{$incubatee->user->contact_number}}</h6>
                            <h6><b>ID Number : </b> {{$incubatee->user->id_number}}</h6>
                            <h6><b>Age : </b> {{$incubatee->user->age}}</h6>
                            <h6><b>Gender : </b> {{$incubatee->user->gender}}</h6>
                            <h6><b>WhatsApp Number : </b> {{$incubatee->user->whatsapp_number}}</h6>
                            <h6><b>Date added : </b>{{$incubatee->created_at}}</h6>
                            @if(isset($incubatee->venture_category))
                                <h6><b>Category : {{$incubatee->venture_category->category_name}}</b></h6>
                            @else
                                <h6><b>Category :No category</b></h6>
                            @endif
                        </div>
                        <div class="col l6 m6 s12">
                            <h6><b>Address One : </b> {{$incubatee->user->address_one}}</h6>
                            <h6><b>Address Two </b> {{$incubatee->user->address_two}}</h6>
                            <h6><b>Address Three : </b> {{$incubatee->user->address_three}}</h6>
                            <h6><b>Application Created : </b> {{$incubatee->user->created_at}}</h6>
                            <h6><b>Data number : </b> {{$incubatee->user->data_cellnumber}}</h6>
                            <h6><b>Service provider : </b> {{$incubatee->user->service_provider_network}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s2"></div>
        <div class="row center">
            <h4><b>ACTIONS</b></h4>
        </div>
        <div class="row center" style="margin-left: 20em">
            <div class="card col s2" id="application" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">APPLICATION </p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="contact-log" style="background-color:#454242;font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">CONTACT LOG</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="uploads" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">UPLOADS</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="evaluation" style="background-color: rgba(0, 47, 95, 1);font-weight: bold">
                <a href="/incubatee-evaluations/{{$incubatee->id}}" style="color: white"><p class="center txt">EVALUATION</p></a>
            </div>
        </div>
    </div>

    {{--APPLICATION FORM--}}
    <div class="col s12 card" style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
        <div class="row" id="application-details-row" hidden style="margin-right: 3em; margin-left: 3em;">
            <div class="container" id="QuestionsAndAnswers">
                <br>
                <div class="row center" id="ap">
                    <h4><b>APPLICATION DETAILS</b></h4>
                </div>
                <div class="row">
                </div>
                @if(count($incubatee_application_question_answers_array) > 0)
                    @foreach($incubatee_application_question_answers_array as $question_answer)
                        <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                        <div class="input-field">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>

            <br>
            <div class="row center">
                <button id="print-button" class="waves-effect waves-light btn"
                        onclick="printContent('QuestionsAndAnswers')">
                    <i class="material-icons left">local_printshop</i>Print
                </button>
            </div>
        </div>
    </div>
    {{--CONTACT RESULTS--}}
    <div class=" col s12 card row" id="contact-log-row" hidden
         style="margin-left: 10em;margin-right: 10em;border-radius: 10px;">
        <br>
        <div class="row center">
            <h4><b>INCUBATEE CONTACT LOG</b></h4>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="row" style="margin-left: 7em;margin-right: 4em">
                    <div class="input-field col s4">
                        <input id="comment" type="text" class="validate">
                        <label for="comment">Comment</label>
                    </div>
                    <div class="input-field col s4">
                        <select id="incubatee_contact_results">
                            <option value="Phone Call">Phone Call</option>
                            <option value="Face-to-Face">Face-to-Face</option>
                            <option value="Email">Email</option>
                            <option value="Video Call">Video Call</option>
                            <option value="Review">Review</option>
                            <option value="Referral">Referral</option>
                            <option value="Client Visit">Client Visit</option>
                            <option value="Webinar">Webinar</option>
                            <option value="Workshop">Workshop</option>
                        </select>
                        <label>You contacted {{$bootcamper->user->name}} {{$bootcamper->user->surname}} via</label>
                    </div>
                    <div class="input-field col s4">
                        <input id="date" type="date" class="validate">
                        <label for="date">Date</label>
                    </div>
                </div>
                <div class="row" style="margin-left: 360px;">
                    <div class="col s4">
                        <a class="waves-effect waves-light btn" id="incubatee-contact-log-submit-button">Save</a>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col s4" style="margin-left: 2em">
                <h6 style="color: grey;">Incubatee Contact log history</h6>
                <br>
                <table style="width:300%" class="table table-hover" id="bootcamperTable">
                    <tr>
                        <th>Results of contact</th>
                        <th>Comment</th>
                        <th>Date</th>
                    </tr>
                    @if(isset($incubatee->incubateeContactLog))
                        @foreach($incubatee->incubateeContactLog->sortByDesc('date') as $user_log)
                            <tr>
                                <td>{{$user_log->incubatee_contact_results}}</td>
                                <td>{{$user_log->comment}}</td>
                                <td>{{$user_log->date}}</td>
                            </tr>
                        @endforeach
                    @endif
                </table>
            </div>
        </div>

    </div>
    {{-- UPLOADS--}}
    <div class="col s10 card z-depth-4" id="incubatee-uploads-row" style="border-radius: 10px;width: 1250px;margin: 0 auto" hidden>
        <div class="container">
            <div class="row" id="bu">
                <div class="row center">
                    <br>
                    <h4><b>BOOTCAMPER UPLOADS</b></h4>
                </div>
                @if($incubatee_user->bootcamper->pitch_video_link == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">Video Pitch : Not uploaded.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{$incubatee_user->bootcamper->pitch_video_link}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Pitch video</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_user->bootcamper->id_document_url == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">ID : Not uploaded.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_user->bootcamper->id_document_url}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">ID Document Link</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_user->bootcamper->cipc == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">CIPC: Not uploaded</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_user->bootcamper->cipc}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">CIPC</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_user->bootcamper->bootcamper_contract_upload == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">Contract: Not uploaded</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_user->bootcamper->bootcamper_contract_upload}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Contract</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_user->bootcamper->proof_of_address == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">NoProof of address</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_user->bootcamper->proof_of_address}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Proof of address</p></a>
                    </div>
                @endif
            </div>

            <div class="row center">
                <br>
                <h4><b>STAGE 2 UPLOADS</b></h4>
            </div>
            <div class="row">
                @if($incubatee_user->bootcamper->id_document_url == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">ID : Not uploaded.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_user->bootcamper->id_document_url}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">ID
                                Document Link</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_uploads->stage_2_pitch_video_title == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">Video Pitch : Not uploaded.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{$incubatee_uploads->stage_2_pitch_video}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Pitch video link</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_uploads->talent_dynamics_upload == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">No Talent dynamics.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_uploads->talent_dynamics_upload}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Talent
                                dynamics</p></a>
                    </div>
                @endif
            </div>
            <div class="row center">
                <br>
                <h4><b>STAGE 3 UPLOADS</b></h4>
            </div>
            <div class="row">
                @if($incubatee_uploads->disc_assessment_report == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">No Disc assessment report.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_uploads->disc_assessment_report}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Disc assessment report</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_uploads->strength_upload == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">Strength : Not uploaded.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_uploads->strength_upload}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Strength</p></a>
                    </div>
                @endif
            </div>
            <br>
        </div>
    </div>
    <!--EVENTS-->
    <script>
        function editRole(obj){
            let bootcamper_panelist_id = obj.id;
            window.location.href = '/show-score-sheet/' + bootcamper_panelist_id;
        }
        $('#show-guest-panelist-button').on('click', function(){
            $('#show-guest-panelist-button').hide();
            $('#guest-panelist-container').show();
        });

        $('#contact-result-input').on('change', function(){
            if($('#contact-result-input').val() === "Panel Interview"){
                $('#panel-interview-row').show();
                $('#panel-interview-declined-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Declined" || $('#contact-result-input').val() === "Application Declined"){
                $('#panel-interview-declined-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Referred" || $('#contact-result-input').val() === "Application Referred"){
                $('#panel-interview-declined-but-referred-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-row').hide();
            }
        });
        //Append panel
        function appendPanelist(){
            let email = $('#extra-panelist-email-input').val();
            let incubatee_id = $('#user-id-input').val();
            let question_category_id = $('#question-category-id').val();
            let url = '/admin-add-panel-for-panel-access-window-for-incubatees';
            let formData = new FormData();
            formData.append('email', email);
            formData.append('incubatee_id', incubatee_id);
            formData.append('panel_selection_date', $('#panel-selection-date-display').val());
            formData.append('panel_selection_time', $('#panel-selection-time-display').val());
            formData.append('question_category_id', question_category_id);

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#add-panelist-header').notify(response.message + ". Page refresshing in 3 seconds.", "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    $('#add-panelist-header').notify(response.message, "error");
                }
            });
        }

        function approvePanel(){
            $('#go-panel-row').show();
            $('#no-go-panel-row').hide();
        }

        function declinePanel(){
            $('#go-panel-row').hide();
            $('#no-go-panel-row').show();
        }

        $(document).ready(function () {
            $('.modal').modal();
            let user_id = $('#user-id-input').val();
            $('select').formSelect();

            <!--Finalize button-->
            $('#incubatee-panel-interview-button').on('click', function(){
                let selected_panelists = [];
                let formData = new FormData();

                jQuery.each(jQuery('#panelist-input').val(), function (i, value) {
                    selected_panelists.push(value);
                });

                formData.append('contacted_via', $('#contact-input').val());
                formData.append('result_of_contact', $('#contact-result-input').val());
                formData.append('selected_panelists', JSON.stringify(selected_panelists));
                formData.append('guest_panelists', JSON.stringify(guest_panelists));
                formData.append('panel_selection_date', $('#panel-selection-date-input').val());
                formData.append('panel_selection_time', $('#panel-selection-time-input').val());
                formData.append('question_category_id', $('#question-category-input').val());


                let url = "/incubatee-panel-interview-approve/" + user_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#incubatee-panel-interview-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/incubatees';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#incubatee-panel-interview-button-button').notify(response.message, "error");
                    }
                });
            });

            //Update panel interview details
            $('#update-panel-interview-details-button').on('click', function(){
                let formData = new FormData();
                let date = $('#panel-selection-date-display').val();
                let time = $('#panel-selection-time-display').val();
                let contact = $('#contact-input-display').val();
                let contact_result = $('#contact-result-input-display').val();
                let question_category_id = $('#question-category-display').val();

                formData.append('date', date);
                formData.append('time', time);
                formData.append('contact', contact);
                formData.append('contact_result', contact_result);
                formData.append('question_category_id', question_category_id);

                let url = "/update-incubatee-panel-interview/" + user_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#update-panel-interview-details-button').notify(response.message, "success");
                    },
                    error: function (response) {
                        $('#update-panel-interview-details-button').notify(response.message, "error");
                    }
                });

            });

            //DECLINE PANEL INTERVIEW
            $('#set-incubatee-to-to-exited-incubatees-button').on('click', function(){
                let r = confirm("Are you sure you want to decline this incubatee?");

                if(r === true){
                    $('#set-incubatee-to-to-exited-incubatees-button').text("Please wait");
                    let formData = new FormData();
                    formData.append('contacted_via', $('#panel-contact-input').val());
                    formData.append('result_of_contact', $('#panel-contact-result-input').val());
                    formData.append('declined_reason_text', $('#panel-decline-but-referred-reason-input').val());
                    formData.append('referred_company', $('#panel-referred-company-input').val());

                    let user_id = $('#user-id-input').val();
                    let url = "/set-bootcamper-to-declined-bootcamper/" + user_id;

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            $('#set-incubatee-to-to-exited-incubatees-button').notify(response.message, "success");

                            setTimeout(function(){
                                window.location.href = '/bootcampers';
                            }, 3000);
                        },

                        error: function (response) {
                            let message = response.message;
                            alert(message);
                        }
                    });
                } else {
                    alert("Action cancelled.");
                }
            });

            //Incubatee contact log
            $('#incubatee-contact-log-submit-button').on('click',function () {
                let incubatee_id =$('#user-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('date', $('#date').val());
                formData.append('incubatee_contact_results', $('#incubatee_contact_results').val());
                console.log(formData);

                let url = '/store-incubatee-contact-log/' + incubatee_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $("#incubatee-contact-log-submit-button").notify(
                            "You have successfully log a contact", "success",
                            { position:"right" }
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                });
            });

        });

        //Approve Applicant
        let guest_panelists = [];

        $('#add-guest-panelist-button').on('click', function(){
            let title = $('#title-input').val();
            let initials = $('#initial-input').val();
            let name = $('#name-input').val();
            let surname = $('#surname-input').val();
            let position = $('#position-input').val();
            let company = $('#company-input').val();
            let email = $('#email-input').val();
            let contact_number = $('#contact-number-input').val();

            let object = {
                title: title,
                initials: initials,
                name: name,
                surname: surname,
                position: position,
                company: company,
                email: email,
                contact_number: contact_number
            };

            guest_panelists.push(object);

            $('#title-input').val("");
            $('#initial-input').val("");
            $('#name-input').val("");
            $('#surname-input').val("");
            $('#position-input').val("");
            $('#company-input').val("");
            $('#email-input').val("");
            $('#contact-number-input').val("");
        });



        $('#application').on('click', function () {
            $('#application-details-row').show();
            var element = document.getElementById("ap");
            element.scrollIntoView({behavior: "smooth"});
            $('#incubatee-uploads-row').hide();
            $('#contact-log-row').hide();
            $('#incubatee-events').hide();
            $('#panel-results-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#uploads').on('click', function () {
            $('#incubatee-uploads-row').show();
            var element = document.getElementById("bu");
            element.scrollIntoView({behavior: "smooth"});
            $('#application-details-row').hide();
            $('#contact-log-row').hide();
            $('#incubatee-events').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#contact-log').on('click', function () {
            $('#contact-log-row').show();
            var element = document.getElementById("contact-log-row");
            element.scrollIntoView({behavior: "smooth"});
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#incubatee-events').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#events').on('click', function () {
            $('#incubatee-events').show();
            var element = document.getElementById("incubatee-events");
            element.scrollIntoView({behavior: "smooth"});
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });

        $('#workshops').on('click', function () {
            $('#incubatee-evaluation').show();
            $('#workshops-incubatee-evaluation').show();
            $('#pre-post-incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#pre-post').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#incubatee-evaluation').show();
            $('#pre-post-incubatee-evaluation').show();
            $('#workshops-incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#panel-results').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#panel-results-row').show();
            $('#incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#go-no-go-panel').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#go-no-go-panel-row').show();
            $('#bootcamper-panel-results').hide();
            $('#incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#panel-results-row').hide();
        });

        $('#panel-results').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#panel-results-row').show();
            $('#go-no-go-panel-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
        });

        function printContent(el) {
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }

    </script>
    <script>
        $('#applicant-panel').on('click', function () {
            $('#applicant-panel-interview-overview-section').show();
            $('#bootcamp-panel-interview-overview-section').hide();
        });

        $('#bootcamp-panel').on('click', function () {
            $('#bootcamp-panel-interview-overview-section').show();
            $('#applicant-panel-interview-overview-section').hide();
        });

    </script>
@endsection
