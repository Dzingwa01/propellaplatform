@extends('layouts.administrator-layout')

@section('content')
    <div class="section" style="margin-top: 5vh">
        <input id="user-id-input" value="{{$incubatee->id}}" hidden disabled>
        <div class="col s10 card z-depth-4" style="border-radius: 10px;width: 1350px;margin: 0 auto">
            {{--APPPLICATION ACOUNT INFO--}}
            <div class="row" id="applicant-account-show" style="margin-right: 3em; margin-left: 3em;">
                <br>
                <h4 class="center"><b>OVERVIEW OF : {{$incubatee->user->name}} {{$incubatee->user->surname}}</b></h4>
                <div class="row " style="margin-left: 10em">
                    <br>
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h6><b>Email : </b> {{$incubatee->user->email}}</h6>
                            <h6><b>Contact Number : </b> {{$incubatee->user->contact_number}}</h6>
                            <h6><b>ID Number : </b> {{$incubatee->user->id_number}}</h6>
                            <h6><b>Age : </b> {{$incubatee->user->age}}</h6>
                            <h6><b>Gender : </b> {{$incubatee->user->gender}}</h6>
                            <h6><b>WhatsApp Number : </b> {{$incubatee->user->whatsapp_number}}</h6>
                            <h6><b>Date added : </b>{{$incubatee->created_at}}</h6>
                            @if(isset($incubatee->venture_category))
                                <h6><b>Category : {{$incubatee->venture_category->category_name}}</b></h6>
                            @else
                                <h6><b>Category :No category</b></h6>
                            @endif
                        </div>
                        <div class="col l6 m6 s12">
                            <h6><b>Address One : </b> {{$incubatee->user->address_one}}</h6>
                            <h6><b>Address Two </b> {{$incubatee->user->address_two}}</h6>
                            <h6><b>Address Three : </b> {{$incubatee->user->address_three}}</h6>
                            <h6><b>Application Created : </b> {{$incubatee->user->created_at}}</h6>
                            <h6><b>Data number : </b> {{$incubatee->user->data_cellnumber}}</h6>
                            <h6><b>Service provider : </b> {{$incubatee->user->service_provider_network}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s2"></div>
        <div class="row center">
            <h4><b>ACTIONS</b></h4>
        </div>
        <div class="row" style="margin-left: 9em">
            <div class="card col s2" id="application" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">APPLICATION </p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="contact-log" style="background-color:#454242;font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">CONTACT LOG</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="uploads" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">UPLOADS</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="events" style="background-color: #454242;font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">EVENTS</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="evaluation" style="background-color: rgba(0, 47, 95, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">EVALUATION</p>
            </div>
        </div>
    </div>

    {{--APPLICATION FORM--}}
    <div class="col s12 card" style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
        <div class="row" id="application-details-row" hidden style="margin-right: 3em; margin-left: 3em;">
            <div class="container" id="QuestionsAndAnswers">
                <br>
                <div class="row center">
                    <h4><b>APPLICATION DETAILS</b></h4>
                </div>
                <div class="row">
                </div>
                @if(count($incubatee_application_question_answers_array) > 0)
                    @foreach($incubatee_application_question_answers_array as $question_answer)
                        <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                        <div class="input-field">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>

            <br>
            <div class="row center">
                <button id="print-button" class="waves-effect waves-light btn"
                        onclick="printContent('QuestionsAndAnswers')">
                    <i class="material-icons left">local_printshop</i>Print
                </button>
            </div>
        </div>
    </div>
    {{--CONTACT RESULTS--}}
    <div class=" col s12 card row" id="contact-log-row" hidden
         style="margin-left: 10em;margin-right: 10em;border-radius: 10px;">
        <br>
        <div class="row center">
            <h4><b>CONTACT LOG</b></h4>
        </div>
        <div class="row">
            <div class="col s4" style="margin-left: 2em">
                <h6 style="color: grey;">Incubatee Contact log history</h6>
                <br>
                @if(isset($incubatee->incubateeContactLog))
                    @foreach($incubatee->incubateeContactLog->sortByDesc('date') as $user_log)
                        <p><b>Results of contact</b> : {{$user_log->incubatee_contact_results}}</p>
                        <p><b>Comment</b> : {{$user_log->comment}}</p>
                        <p><b>Date</b>:{{$user_log->date}}</p>
                        <hr>
                    @endforeach
                @endif
            </div>

            <div class="col s1"></div>

            <div class="col s7">
                <div class="row" style="margin-left: 7em">
                    <div class="input-field col s10">
                        <input id="comment" type="text" class="validate">
                        <label for="comment">Comment</label>
                    </div>
                </div>
                <div class="row" style="margin-left: 7em">
                    <div class="input-field col s10">
                        <input id="date" type="date" class="validate">
                        <label for="date">Date/Time</label>
                    </div>
                </div>
                <div class="row" style="margin-left: 7em">
                    <div class="input-field col s10">
                        <select id="incubatee_contact_results">
                            <option value="Phone Call">Phone Call</option>
                            <option value="Face-to-Face">Face-to-Face</option>
                            <option value="Email">Email</option>
                            <option value="No response to any contact type">No response to any contact type
                            </option>
                            <option value="Invited, but did not arrive">Invited, but did not arrive</option>
                        </select>
                        <label>You contacted {{$incubatee->user->name}} {{$incubatee->user->surname}} via</label>
                    </div>
                </div>
                <div class="row" style="margin-left: 360px;">
                    <div class="col s4">
                        <a class="waves-effect waves-light btn" id="incubatee-contact-log-submit-button">Save</a>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

    {{-- UPLOADS--}}
    <div class="col s10 card z-depth-4" id="incubatee-uploads-row" style="border-radius: 10px;width: 1250px;margin: 0 auto" hidden>
        <div class="container">
            <div class="row">
                <div class="row center">
                    <h4><b>BOOTCAMPER UPLOADS</b></h4>
                </div>
                <div class="row" style="margin-left: 5em">
                    <div class="row">
                        @if($bootcamper->pitch_video_link == null)
                            <div class="card col s3" style="background-color: red;font-weight: bold">
                                <p class="center txt" style="color: white;cursor:pointer">Video Pitch: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                                <a href="{{$bootcamper->pitch_video_link}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Video Pitch link.</p></a>
                            </div>
                        @endif
                        <div class="col s0.1"></div>
                        @if($bootcamper->pitch_video_link_two == null)
                            <div class="card col s3" style="background-color: red;font-weight: bold">
                                <p  class="center txt" style="color: white;cursor:pointer">Video Pitch 2: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                                <a href="{{$bootcamper->pitch_video_link_two}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Video Pitch 2 link.</p></a>
                            </div>
                        @endif
                        <div class="col s0.1"></div>
                        @if($bootcamper->id_document_url == null)
                            <div class="card col s3" style="background-color: red;font-weight: bold">
                                <p class="center txt" style="color: white;cursor:pointer">ID Document: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                                <a href="{{'/storage/'.$bootcamper->id_document_url}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">ID Document Link</p></a>
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        @if($bootcamper->proof_of_address == null)
                            <div class="card col s3" style="background-color: red;font-weight: bold">
                                <p class="center txt" style="color: white;cursor:pointer">Proof of address: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                                <a href="{{'/storage/'.$bootcamper->proof_of_address}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">Proof of address</p></a>
                            </div>
                        @endif
                        <div class="col s0.1"></div>
                        @if($bootcamper->cipc == null)
                            <div class="card col s3" style="background-color: red;font-weight: bold">
                                <p class="center txt" style="color: white;cursor:pointer">CIPC: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                                <a href="{{'/storage/'.$bootcamper->cipc}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">CIPC Link</p></a>
                            </div>
                        @endif
                        <div class="col s0.1"></div>
                        @if($bootcamper->contract_uploaded == null)
                            <div class="card col s3" style="background-color: red;font-weight: bold">
                                <p class="center txt" style="color: white;cursor:pointer">Contract: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                                <a href="{{'/storage/'.$bootcamper->contract_uploaded}}" target="_blank"><p class="center txt" style="color: white;cursor:pointer">ContractLink</p></a>
                            </div>
                        @endif

                    </div>
                </div>

            </div>

            <div class="row center">
                <br>
                <h4><b>STAGE 2 UPLOADS</b></h4>
            </div>
            <div class="row" style="margin-left: 5em">
                @if($incubatee_uploads->stage_2_pitch_video == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">STAGE 2 VIDEO: None.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{$incubatee_uploads->stage_2_pitch_video}}" target="_blank"><p class="center txt"
                                                                                                 style="color: white;cursor:pointer">
                                STAGE 2 PITCH.</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_uploads->founder_video_shot == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">FOUNDER VIDEO: None.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{$incubatee_uploads->founder_video_shot}}" target="_blank"><p class="center txt"
                                                                                                style="color: white;cursor:pointer">
                                FOUNDER VIDEO.</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_uploads->id_document == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">ID: None.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_uploads->id_document}}" target="_blank"><p class="center txt"
                                                                                                     style="color: white;cursor:pointer">
                                ID Document</p></a>
                    </div>
                @endif
            </div>

            <div class="row center">
                <br>
                <h4><b>STAGE 3 UPLOADS</b></h4>
            </div>
            <div class="row" style="margin-left: 5em">
                @if($incubatee_uploads->disc_assessment_report == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">Disc assessment: None.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_uploads->disc_assessment_report}}" target="_blank"><p
                                class="center txt" style="color: white;cursor:pointer">Disc assessment</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_uploads->strength_upload == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">Strength: None.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_uploads->strength_upload}}" target="_blank"><p
                                class="center txt" style="color: white;cursor:pointer">Strengths</p></a>
                    </div>
                @endif
                <div class="col s0.1"></div>
                @if($incubatee_uploads->talent_dynamics_upload == null)
                    <div class="card col s3" style="background-color: red;font-weight: bold">
                        <p class="center txt" style="color: white;cursor:pointer">Talent Dynamic: None.</p>
                    </div>
                @else
                    <div class="card col s3" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                        <a href="{{'/storage/'.$incubatee_uploads->talent_dynamics_upload}}" target="_blank"><p
                                class="center txt" style="color: white;cursor:pointer">Talent Dynamic</p></a>
                    </div>
                @endif
            </div>
            <br>
        </div>
    </div>

    <!--EVENTS-->
    <div class=" col s12 card row" id="incubatee-events" hidden style="margin-left: 10em;margin-right: 10em;border-radius: 10px;">
        <br>
        <div class="table-responsive">
            <div class="row center">
                <h4><b>BOOTCAMPER EVENT DETAILS</b></h4>
            </div>
            <div class="table-responsive">
                <table class="table table-hover" id="events-table">
                    <thead>
                    <tr>
                        <th scope="col">Event tile</th>
                        <th scope="col">Event date</th>
                        <th scope="col">Date registered</th>
                        <th scope="col">Accepted</th>
                        <th scope="col">Declined</th>
                        <th scope="col">Attended</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($incubatee_bootcamper_events_array as $b_event)
                        <tr>
                            <td>{{$b_event->event_title}}</td>
                            <td>{{$b_event->event_date}}</td>
                            <td>{{$b_event->date_registered}}</td>
                            <td>{{$b_event->accepted}}</td>
                            <td>{{$b_event->declined}}</td>
                            <td>{{$b_event->attended}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <div class="table-responsive">
            <div class="row center">
                <h4><b>INCUBATEE EVENT DETAILS</b></h4>
            </div>
            <div class="table-responsive">
                <table class="table table-hover" id="events-table">
                    <thead>
                    <tr>
                        <th scope="col">Event tile</th>
                        <th scope="col">Event date</th>
                        <th scope="col">Date registered</th>
                        <th scope="col">Attended</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($incubatee_events_array as $b_event)
                        <tr>
                            <td>{{$b_event->event_title}}</td>
                            <td>{{$b_event->event_date}}</td>
                            <td>{{$b_event->date_time_registered}}</td>
                            <td>{{$b_event->attended}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <div class="table-responsive">
            <div class="row center">
                <h4><b>DE-REGISTERED EVENT DETAILS</b></h4>
            </div>
            <div class="table-responsive">
                <table class="table table-hover" id="events-table">
                    <thead>
                    <tr>
                        <th scope="col">Event tile</th>
                        <th scope="col">Event date</th>
                        <th scope="col">Date de-registered</th>
                        <th scope="col">De-register reason</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($incubatee_deregistered_events_array as $b_event)
                        <tr>
                            <td>{{$b_event->event_title}}</td>
                            <td>{{$b_event->event_date}}</td>
                            <td>{{$b_event->date_time_de_register}}</td>
                            <td>{{$b_event->de_register_reason}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>



        @if(count($incubatee_deregistered_events_array) > 0)
            <div class="row" id="incubatee-de-registered-events-details-row"
                 style="margin-right: 3em; margin-left: 3em;">
                <h5 style="color: #00b0ff"><u>DE-REGISTERED EVENTS</u></h5>
                @foreach($incubatee_deregistered_events_array as $i_event)
                    <div class="col l3 m3 s12">
                        <ul class="collapsible">
                            <li>
                                @if(isset($i_event->event_title))
                                    <div class="collapsible-header">{{$i_event->event_title}}</div>
                                @else
                                    <div class="collapsible-header">No event title</div>
                                @endif
                                <div class="collapsible-body">
                                        <span>
                                            @if(isset($i_event->event_date))
                                                Event Date: {{$i_event->event_date}}
                                            @else
                                                Event Date: No event date
                                            @endif
                                            <br>

                                            @if(isset($i_event->date_time_de_register))
                                                Date De-Registered: {{$i_event->date_time_de_register}}
                                            @else
                                                Date De-Registered: No date registered
                                            @endif
                                            <br>

                                            @if($i_event->de_register_reason != null)
                                                De-register Reason: {{$i_event->de_register_reason}}
                                            @else
                                                De-register Reason: No reason given.
                                            @endif
                                            <br>
                                        </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                @endforeach
            </div>
        @endif

    </div>

    {{-- EVALUATION FORMS--}}
    <div class="col s12 row" id="incubatee-evaluation" hidden style="margin-left: 10em;margin-right: 10em;border-radius: 10px;">
        <div class="row center">
            <h5><b>EVALUATIONS </b></h5>
        </div>
        <table style="margin-left: 3em;margin-right: 3em;width: 1000px">
            <tr>
                <th>Evaluations</th>
                <th>Action</th>
            </tr>
            <tr>
                @if(isset($bootcamper))
                    <td>Pre bootcamp workshop evaluation form</td>
                    @if($bootcamper->beginning_evaluation_complete == null)
                        <td>
                            <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreBootcamp">View</a>
                        </td>
                    @else
                        <td>
                            <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreBootcamp">View</a>
                        </td>
                    @endif
            </tr>
            <tr>
                <td>Post bootcamp workshop evaluation form</td>
                @if($bootcamper->post_evaluation_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostBootcamp">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostBootcamp">View</a>
                    </td>
                @endif
                @endif
            </tr>
            <tr>
                <td>Pre Stage 2 workshop evaluation form</td>
                @if($incubatee->pre_stage_two_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreStage2">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreStage2">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Post Stage 2 workshop evaluation form</td>
                @if($incubatee->post_stage_two_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostStage2">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostStage2">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Pre Stage 3 workshop evaluation form</td>
                @if($incubatee->pre_stage_three_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreStage3">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreStage3">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Post Stage 3 workshop evaluation form</td>
                @if($incubatee->post_stage_three_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostStage3">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostStage3">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Pre Stage 4 workshop evaluation form</td>
                @if($incubatee->pre_stage_four_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreStage4">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreStage4">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Post Stage 4 workshop evaluation form</td>
                @if($incubatee->post_stage_four_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostStage4">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostStage4">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Pre Stage 5 workshop evaluation form</td>
                @if($incubatee->pre_stage_five_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreStage5">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreStage5">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Post Stage 5 workshop evaluation form</td>
                @if($incubatee->post_stage_five_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostStage5">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostStage5">View</a>
                    </td>
                @endif
            </tr>
        </table>

        {{-- Pre bootcamp--}}
        @if(isset($bootcamper))
            <div id="collapsePreBootcamp" class="modal">
                <div class="modal-content">
                    <h5 class="center-align"><b>PRE BOOTCAMP WORKSHOP EVALUATION FORM</b></h5>
                    @if($bootcamper->beginning_evaluation_complete == null)
                        <p>Not submitted yet</p>
                    @else
                        @foreach($pre_bootcamp_question_answers_array as $question_answer)
                            <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                            <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                                <textarea disabled>{{$question_answer->answer_text}}</textarea>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                </div>
            </div>
            {{-- Post bootcamp--}}
            <div id="collapsePostBootcamp" class="modal">
                <div class="modal-content">
                    <h5 class="center-align"><b>POST BOOTCAMP  WORKSHOP EVALUATION FORM</b></h5>
                    @if($bootcamper->post_evaluation_complete == null)
                        <p>Not submitted yet</p>
                    @else
                        @foreach($post_bootcamp_question_answers_array as $question_answer)
                            <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                            <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                                <textarea disabled>{{$question_answer->answer_text}}</textarea>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                </div>
            </div>
    @endif
    <!--Stage 2-->
        <div id="collapsePreStage2" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>PRE STAGE 2 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->pre_stage_two_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($pre_stage2_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <div id="collapsePostStage2" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>POST STAGE 2 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->post_stage_two_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($post_stage2_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!--STAGE 3-->
        <div id="collapsePreStage3" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>PRE STAGE 3 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->pre_stage_three_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($pre_stage3_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <div id="collapsePostStage3" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>POST STAGE 3 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->post_stage_three_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($post_stage3_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!--STAGE 4-->
        <div id="collapsePreStage4" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>PRE STAGE 4 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->pre_stage_four_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($pre_stage4_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <div id="collapsePostStage4" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>POST STAGE 4 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->post_stage_four_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($post_stage4_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!--STAGE 5-->
        <div id="collapsePreStage5" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>PRE STAGE 5 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->pre_stage_five_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($pre_stage5_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <div id="collapsePostStage5" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>POST STAGE 5 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->post_stage_five_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($post_stage5_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
    </div>


    <script>
        function editRole(obj){
            let bootcamper_panelist_id = obj.id;
            window.location.href = '/show-score-sheet/' + bootcamper_panelist_id;
        }
        $('#show-guest-panelist-button').on('click', function(){
            $('#show-guest-panelist-button').hide();
            $('#guest-panelist-container').show();
        });

        $('#contact-result-input').on('change', function(){
            if($('#contact-result-input').val() === "Panel Interview"){
                $('#panel-interview-row').show();
                $('#panel-interview-declined-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Declined" || $('#contact-result-input').val() === "Application Declined"){
                $('#panel-interview-declined-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Referred" || $('#contact-result-input').val() === "Application Referred"){
                $('#panel-interview-declined-but-referred-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-row').hide();
            }
        });
        //Append panel
        function appendPanelist(){
            let email = $('#extra-panelist-email-input').val();
            let incubatee_id = $('#user-id-input').val();
            let question_category_id = $('#question-category-id').val();
            let url = '/admin-add-panel-for-panel-access-window-for-incubatees';
            let formData = new FormData();
            formData.append('email', email);
            formData.append('incubatee_id', incubatee_id);
            formData.append('panel_selection_date', $('#panel-selection-date-display').val());
            formData.append('panel_selection_time', $('#panel-selection-time-display').val());
            formData.append('question_category_id', question_category_id);

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#add-panelist-header').notify(response.message + ". Page refresshing in 3 seconds.", "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    $('#add-panelist-header').notify(response.message, "error");
                }
            });
        }

        function approvePanel(){
            $('#go-panel-row').show();
            $('#no-go-panel-row').hide();
        }

        function declinePanel(){
            $('#go-panel-row').hide();
            $('#no-go-panel-row').show();
        }

        $(document).ready(function () {
            $('.modal').modal();
            let user_id = $('#user-id-input').val();
            $('select').formSelect();

            <!--Finalize button-->
            $('#incubatee-panel-interview-button').on('click', function(){
                let selected_panelists = [];
                let formData = new FormData();

                jQuery.each(jQuery('#panelist-input').val(), function (i, value) {
                    selected_panelists.push(value);
                });

                formData.append('contacted_via', $('#contact-input').val());
                formData.append('result_of_contact', $('#contact-result-input').val());
                formData.append('selected_panelists', JSON.stringify(selected_panelists));
                formData.append('guest_panelists', JSON.stringify(guest_panelists));
                formData.append('panel_selection_date', $('#panel-selection-date-input').val());
                formData.append('panel_selection_time', $('#panel-selection-time-input').val());
                formData.append('question_category_id', $('#question-category-input').val());


                let url = "/incubatee-panel-interview-approve/" + user_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#incubatee-panel-interview-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/incubatees';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#incubatee-panel-interview-button-button').notify(response.message, "error");
                    }
                });
            });

            //Update panel interview details
            $('#update-panel-interview-details-button').on('click', function(){
                let formData = new FormData();
                let date = $('#panel-selection-date-display').val();
                let time = $('#panel-selection-time-display').val();
                let contact = $('#contact-input-display').val();
                let contact_result = $('#contact-result-input-display').val();
                let question_category_id = $('#question-category-display').val();

                formData.append('date', date);
                formData.append('time', time);
                formData.append('contact', contact);
                formData.append('contact_result', contact_result);
                formData.append('question_category_id', question_category_id);

                let url = "/update-incubatee-panel-interview/" + user_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#update-panel-interview-details-button').notify(response.message, "success");
                    },
                    error: function (response) {
                        $('#update-panel-interview-details-button').notify(response.message, "error");
                    }
                });

            });

            //DECLINE PANEL INTERVIEW
            $('#set-incubatee-to-to-exited-incubatees-button').on('click', function(){
                let r = confirm("Are you sure you want to decline this incubatee?");

                if(r === true){
                    $('#set-incubatee-to-to-exited-incubatees-button').text("Please wait");
                    let formData = new FormData();
                    formData.append('contacted_via', $('#panel-contact-input').val());
                    formData.append('result_of_contact', $('#panel-contact-result-input').val());
                    formData.append('declined_reason_text', $('#panel-decline-but-referred-reason-input').val());
                    formData.append('referred_company', $('#panel-referred-company-input').val());

                    let user_id = $('#user-id-input').val();
                    let url = "/set-bootcamper-to-declined-bootcamper/" + user_id;

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            $('#set-incubatee-to-to-exited-incubatees-button').notify(response.message, "success");

                            setTimeout(function(){
                                window.location.href = '/bootcampers';
                            }, 3000);
                        },

                        error: function (response) {
                            let message = response.message;
                            alert(message);
                        }
                    });
                } else {
                    alert("Action cancelled.");
                }
            });

            //Incubatee contact log
            $('#incubatee-contact-log-submit-button').on('click',function () {
                let incubatee_id =$('#user-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('date', $('#date').val());
                formData.append('incubatee_contact_results', $('#incubatee_contact_results').val());
                console.log(formData);

                let url = '/store-incubatee-contact-log/' + incubatee_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $("#incubatee-contact-log-submit-button").notify(
                            "You have successfully log a contact", "success",
                            { position:"right" }
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                });
            });

        });

        //Approve Applicant
        let guest_panelists = [];

        $('#add-guest-panelist-button').on('click', function(){
            let title = $('#title-input').val();
            let initials = $('#initial-input').val();
            let name = $('#name-input').val();
            let surname = $('#surname-input').val();
            let position = $('#position-input').val();
            let company = $('#company-input').val();
            let email = $('#email-input').val();
            let contact_number = $('#contact-number-input').val();

            let object = {
                title: title,
                initials: initials,
                name: name,
                surname: surname,
                position: position,
                company: company,
                email: email,
                contact_number: contact_number
            };

            guest_panelists.push(object);

            $('#title-input').val("");
            $('#initial-input').val("");
            $('#name-input').val("");
            $('#surname-input').val("");
            $('#position-input').val("");
            $('#company-input').val("");
            $('#email-input').val("");
            $('#contact-number-input').val("");
        });



        $('#application').on('click', function () {
            $('#application-details-row').show();
            $('#incubatee-uploads-row').hide();
            $('#contact-log-row').hide();
            $('#incubatee-events').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#uploads').on('click', function () {
            $('#incubatee-uploads-row').show();
            $('#application-details-row').hide();
            $('#contact-log-row').hide();
            $('#incubatee-events').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#contact-log').on('click', function () {
            $('#contact-log-row').show();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#incubatee-events').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#events').on('click', function () {
            $('#incubatee-events').show();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#evaluation').on('click', function () {
            $('#incubatee-evaluation').show();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#workshops').on('click', function () {
            $('#incubatee-evaluation').show();
            $('#workshops-incubatee-evaluation').show();
            $('#pre-post-incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#pre-post').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#incubatee-evaluation').show();
            $('#pre-post-incubatee-evaluation').show();
            $('#workshops-incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#panel-results').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#bootcamper-panel-results').show();
            $('#incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#go-no-go-panel').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#go-no-go-panel-row').show();
            $('#bootcamper-panel-results').hide();
            $('#incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#panel-results-row').hide();
        });

        $('#panel-results').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#panel-results-row').show();
            $('#go-no-go-panel-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
        });

        function printContent(el) {
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }

    </script>

@endsection
