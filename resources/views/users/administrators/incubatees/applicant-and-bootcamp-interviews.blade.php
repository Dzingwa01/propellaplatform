@extends('layouts.administrator-layout')

@section('content')
    <div class="section" style="margin-top: 1em;">
        <input id="incubatee-id-input" value="{{$incubatee->id}}" hidden disabled>
        <div class="row center">
            <h4><b>PANEL RESULTS</b></h4>
        </div>

        <div class="row" style="margin-left: 370px">
            <div class="card col s4" id="applicant-panel" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">APPLICANT PANEL RESULTS</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s4" id="bootcamp-panel" style="background-color:green;font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">BOOTCAMP PANEL RESULTS</p>
            </div>
        </div>

        {{--PANEL INTERVIEW OVERVIEW SECTION--}}
        <div class="row" id="applicant-panel-interview-overview-section" hidden style="margin-right: 3em; margin-left: 3em;">
            <div class="section" style="margin-top: 1em;">
                <input id="applicant-id-input" value="{{$applicant->id}}" hidden disabled>
                <div class="row center">
                    <div class="row center" style="background-color: #123c24;">
                        <h5 style="color: white;">Applicant Panelists scores <i class="material-icons">arrow_downward</i>
                        </h5>
                    </div>

                    <div class="row" style="margin-left: 3em; margin-right: 3em;">
                        @foreach($final_panelist_question_score_array as $panelist)
                            <div class="col l3 m6 s12 center">
                                <ul class="collapsible">
                                    <li>
                                        <div class="collapsible-header">
                                            <h5>{{$panelist->name}} {{$panelist->surname}}'s score</h5></div>
                                        <div class="collapsible-body">
                        <span>
                            @if(count($panelist->user_questions_scores) > 0)
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Question Number</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>Question Score</h6>
                                </div>
                            </div>
                                @if(isset($panelist->user_questions_scores))
                                    @foreach($panelist->user_questions_scores as $question_answer)
                                        <div class="row">
                                        <div class="col l6 m6 s12" style="background-color: darkgray;">
                                            <p>{{$question_answer->question_number}}</p>
                                        </div>
                                        <div class="col l6 m6 s12" style="background-color: dimgray;">
                                            <p>{{$question_answer->question_score}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Total Score</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>{{isset($panelist->panelist_applicant_score) ? $panelist->panelist_applicant_score : 0}}</h6>
                                </div>
                            </div>
                            @else
                                <div class="row center">
                                    <h6>The panelist has not completed the score sheet.</h6>
                                </div>
                            @endif
                        </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="row center">
                    <h5>Total combined score: <b>{{$total_panelist_score}}</b></h5>
                    <h5>Total average score: <b>{{$average_panelist_score}}</b></h5>
                </div>
            </div>

            <div class="section" style="margin-bottom: 3em; margin-top: 3em;">
                <div class="row center">
                    <div class="row center" style="background-color: #1a237e;">
                        <h5 style="color: white;"> Combined scores per question <i class="material-icons">arrow_downward</i>
                        </h5>
                    </div>

                    <div class="row" style="margin-right: 3em; margin-left: 3em;">
                        <table id="t01">
                            <tr>
                                <th>Question Number</th>
                                <th>Total Score</th>
                            </tr>
                            @foreach($question_scores_array as $question_score)
                                <tr>
                                    <td>{{$question_score->question_number}}</td>
                                    <td>{{$question_score->question_score}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>

            <div class="section">
                <input hidden disabled id="applicant-id-input" value="{{$applicant->id}}">
                <div class="row center" style="background-color: #323232">
                    <h5 style="color: white;"> Want to see more details? <i
                            class="material-icons">arrow_downward</i></h5>
                </div>
                <table style="width:100%">
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Selection Date</th>
                        <th>Actions</th>
                    </tr>
                    @foreach($panelists as $panelist)
                        <tr>
                            <td>{{$panelist->name}}</td>
                            <td>{{$panelist->surname}}</td>
                            <td>{{$panelist->email}}</td>
                            <td>{{$panelist->selection_date}}</td>
                            <td>
                                <button type="button" class="btn btn-primary" onclick="customerEditDelivery(this)">VIEW</button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

        {{--SHOW BOOTCAMPER PANELIST--}}
        <div class="row" id="bootcamp-panel-interview-overview-section" hidden style="margin-right: 3em; margin-left: 3em;">
            <div class="section" style="margin-top: 1em;">
                <input id="applicant-id-input" value="{{$bootcamper->id}}" hidden disabled>
                <div class="row center">
                    <div class="row center" style="background-color: #123c24;">
                        <h5 style="color: white;">Bootcamp Panelists scores <i class="material-icons">arrow_downward</i>
                        </h5>
                    </div>

                    <div class="row" style="margin-left: 3em; margin-right: 3em;">
                        @foreach($bootcamp_final_panelist_question_score_array as $panelist)
                            <div class="col l3 m6 s12 center">
                                <ul class="collapsible">
                                    <li>
                                        <div class="collapsible-header">
                                            <h5>{{$panelist->name}} {{$panelist->surname}}'s score</h5></div>
                                        <div class="collapsible-body">
                        <span>
                            @if(count($panelist->user_questions_scores) > 0)
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Question Number</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>Question Score</h6>
                                </div>
                            </div>
                                @if(isset($panelist->user_questions_scores))
                                    @foreach($panelist->user_questions_scores as $question_answer)
                                        <div class="row">
                                        <div class="col l6 m6 s12" style="background-color: darkgray;">
                                            <p>{{$question_answer->question_number}}</p>
                                        </div>
                                        <div class="col l6 m6 s12" style="background-color: dimgray;">
                                            <p>{{$question_answer->question_score}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Total Score</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>{{isset($panelist->panelist_applicant_score) ? $panelist->panelist_applicant_score : 0}}</h6>
                                </div>
                            </div>
                            @else
                                <div class="row center">
                                    <h6>The panelist has not completed the score sheet.</h6>
                                </div>
                            @endif
                        </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="row center">
                    <h5>Total combined score: <b>{{$bootcamp_total_panelist_score}}</b></h5>
                    <h5>Total average score: <b>{{$bootcamp_average_panelist_score}}</b></h5>
                </div>
            </div>

            <div class="section" style="margin-bottom: 3em; margin-top: 3em;">
                <div class="row center">
                    <div class="row center" style="background-color: #1a237e;">
                        <h5 style="color: white;"> Combined scores per question <i class="material-icons">arrow_downward</i>
                        </h5>
                    </div>

                    <div class="row" style="margin-right: 3em; margin-left: 3em;">
                        <table id="t01">
                            <tr>
                                <th>Question Number</th>
                                <th>Total Score</th>
                            </tr>
                            @foreach($question_scores_array as $question_score)
                                <tr>
                                    <td>{{$question_score->question_number}}</td>
                                    <td>{{$question_score->question_score}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>

            <div class="section">
                <input hidden disabled id="applicant-id-input" value="{{$applicant->id}}">
                <div class="row center" style="background-color: #323232">
                    <h5 style="color: white;"> Want to see more details? <i
                            class="material-icons">arrow_downward</i></h5>
                </div>
                <table style="width:100%">
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Selection Date</th>
                        <th>Actions</th>
                    </tr>
                    @foreach($panelists as $panelist)
                        <tr>
                            <td>{{$panelist->name}}</td>
                            <td>{{$panelist->surname}}</td>
                            <td>{{$panelist->email}}</td>
                            <td>{{$panelist->selection_date}}</td>
                            <td>
                                <button type="button" class="btn btn-primary" onclick="customerEditDelivery(this)">VIEW</button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
      {{--  <div class="row" id="bootcamp-panel-interview-overview-section" hidden style="width: 170vh;margin-left: 120px;">
            <div class="section" style="background-color: grey; margin-bottom: 3em;">
                <input id="bootcamper-id-input" value="{{$bootcamper->id}}" hidden disabled>
                <div class="row center">
                    <div class="row center" style="background-color: #123c24;">
                        <h5 style="color: white;">Panelists scores <i class="material-icons">arrow_downward</i>
                        </h5>
                    </div>

                    <div class="row" style="margin-left: 3em; margin-right: 3em;">
                        @foreach($final_panelist_question_score_array as $panelist)
                            <div class="col l3 m6 s12 center">
                                <ul class="collapsible">
                                    <li>
                                        <div class="collapsible-header">
                                            <h5>{{$panelist->name}} {{$panelist->surname}}'s score</h5></div>
                                        <div class="collapsible-body">
                        <span>
                            @if(count($panelist->user_questions_scores) > 0)
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Question Number</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>Question Score</h6>
                                </div>
                            </div>
                                @if(isset($panelist->user_questions_scores))
                                    @foreach($panelist->user_questions_scores as $question_answer)
                                        <div class="row">
                                        <div class="col l6 m6 s12" style="background-color: darkgray;">
                                            <p>{{$question_answer->question_number}}</p>
                                        </div>
                                        <div class="col l6 m6 s12" style="background-color: dimgray;">
                                            <p>{{$question_answer->question_score}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Total Score</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>{{isset($panelist->panelist_applicant_score) ? $panelist->panelist_applicant_score : 0}}</h6>
                                </div>
                            </div>
                            @else
                                <div class="row center">
                                    <h6>The panelist has not completed the score sheet.</h6>
                                </div>
                            @endif
                        </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="row center">
                    <h5>Panelists assigned to Bootcamper</h5>

                    @foreach($bootcamp_final_panelist_question_score_array as $panelist)
                        <div class="col l3 m6 s12 center">
                            <div class="card">
                                <div class="card-content">
                                    <h5>{{$panelist->name}} {{$panelist->surname}}</h5>
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <h6>Question Number</h6>
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <h6>Question Score</h6>
                                        </div>
                                    </div>
                                    @if(isset($panelist->user_questions_scores))
                                        @foreach($panelist->user_questions_scores as $question_answer)
                                            <div class="row">
                                                <div class="col l6 m6 s12" style="background-color: darkgray;">
                                                    <p>{{$question_answer->question_number}}</p>
                                                </div>
                                                <div class="col l6 m6 s12" style="background-color: dimgray;">
                                                    <p>{{$question_answer->question_score}}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <h6>Total Score</h6>
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <h6>{{isset($panelist->panelist_applicant_score) ? $panelist->panelist_applicant_score : 0}}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h5>Total Combined Score: {{$bootcamp_total_panelist_score}}</h5>
                        </div>
                        <div class="col l6 m6 s12">
                            <h5>Total Average Score: {{$bootcamp_average_panelist_score}}</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section" style="margin-bottom: 3em; margin-top: 3em;">
                <div class="row center">
                    <h5> Combined scores per question </h5>
                    <table id="t01">
                        <tr>
                            <th>Question Number</th>
                            <th>Total Score</th>
                        </tr>
                        @foreach($bootcamp_question_scores_array as $question_score)
                            <tr>
                                <td>{{$question_score->question_number}}</td>
                                <td>{{$question_score->question_score}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="section" style="background-color: grey; margin-top: 3em;">
                <div class="row">
                    <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Want to see more?</h6>
                </div>
                <div class="row" style="margin-left: 2em;margin-right: 2em;">
                    <div class="col s12">
                        <table class="table table-bordered" style="width: 100%!important;" id="bootcamper-panelists-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Selection Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>--}}
    </div>

    <script>
        $('#applicant-panel').on('click', function () {
            $('#applicant-panel-interview-overview-section').show();
            $('#bootcamp-panel-interview-overview-section').hide();
        });

        $('#bootcamp-panel').on('click', function () {
            $('#bootcamp-panel-interview-overview-section').show();
            $('#applicant-panel-interview-overview-section').hide();
        });

    </script>

@endsection
