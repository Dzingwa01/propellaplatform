@extends('layouts.administrator-layout')
@section('content')
    <input id="incubatee-id-input" disabled hidden value="{{$incubatee->id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-events-incubatee',$incubatee)}}
    <br>
    <div class="container-fluid">
        <input hidden disabled id="incubatee-id-input" value="{{$incubatee->id}}">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Incubatee Registered Events</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="incubatee-registered-events-table">
                    <thead>
                    <tr>
                        <th>Event</th>
                        <th>Date Registered</th>
                        <th>Attended</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>

        <div class="row desktopShowEvent" style=" margin-left: 3em; margin-right: 3em;">
            <div class="col l12 card" style="background: grey">
                <h4  class="center-align" style="color: white " id="notification-header">Incubatee De-registered Events</h4>
            </div>
            @foreach ($deregistered_events as $deregistered)
                <div class="col s12 m3">
                    <div class="card grey hoverable " style="border-radius: 20px;">
                        <div class="circle">
                            <div class="card-content white-text" style="height: 100%;">
                                <h6 class="">Event tittle : {{$deregistered->event}}</h6>
                                <h6 class="">De-registered date : {{$deregistered->date_time_de_register}}</h6>
                                <h6 style="color: red" class="">De-registered reason : {{$deregistered->de_register_reason}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Visitor" href="#event_modal">
                <i class="large material-icons">add</i>
            </a>
        </div>
        <!-- Modal Structure -->
        <div id="event_modal" class="modal" style="background-color: grey;">
            <div class="row center">
                @foreach($events as $event)
                    <div class="col l6 m6 s12">
                        <div class="card">
                            <h5>{{$event->title}}</h5>
                            <p>Start: {{$event->start->toDateString()}}</p>
                            <p>End: {{$event->end->toDateString()}}</p>
                            <p>End: {{$event->type}}</p>
                            <a style="color: orange; cursor: pointer;" class="register-incubatee" data-value="{{$event->id}}">Register</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
        <script>
            $(document).ready(function() {
                $('select').formSelect();
                let incubatee_id = $('#incubatee-id-input').val();

                $(function () {
                    $('#incubatee-registered-events-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '/get-incubatee-registered-events-via-edit/' + incubatee_id,
                        columns: [
                            {data: 'event', name: 'event'},
                            {data: 'date_registered', name: 'date_registered'},
                            {data: 'attended', name: 'attended'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="incubatee-registered-events-table_length"]').css("display","inline");
                });

                $(function () {
                    $('#incubatee-de-registered-events-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '/get-incubatee-de-registered-events-via-edit/' + incubatee_id,
                        columns: [
                            {data: 'event_name', name: 'event_name'},
                            {data: 'de_register_reason', name: 'de_register_reason'},
                            {data: 'date_time_de_register', name: 'date_time_de_register'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="incubatee-de-registered-events-table_length"]').css("display","inline");
                });
            });
            $('.register-incubatee').on('click', function(){
                let event_id = this.getAttribute('data-value');
                let incubatee_id = $('#incubatee-id-input').val();

                let formData = new FormData();
                formData.append('event_id', event_id);
                formData.append('incubatee_id', incubatee_id);

                let url = '/admin-regiter-incubtee-event';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('.register-incubatee').notify(response.message, "success");
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            // function confirm_delete_incubatee(obj){
            //     var r = confirm("Are you sure want to delete this Incubatee?");
            //     if (r == true) {
            //         $.get('/user-incubatee-delete/'+obj.id,function(data,status){
            //             console.log('Data',data);
            //             console.log('Status',status);
            //             if(status=='success'){
            //                 alert(data.message);
            //                 window.location.reload();
            //             }
            //
            //         });
            //     } else {
            //         alert('Delete action cancelled');
            //     }
            // }
        </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
