@extends('layouts.administrator-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-incubatee-account')}}
    <br>
    <div class="section" style="margin-top: 5vh">
        <div class="row" style="margin-left: 5em; margin-right: 5em;">
            <div class="card col l4 center" style="border-radius: 10px">
                <p style="font-size: 1.5em;background-color: #00acc1;height: 5vh;color: white;font-weight: bold">Account Options</p>
                <a id="incubatee-account-details-button" class="btn-flat txt" style="color: #00b0ff;">Account Details</a>
                <br>
                <a id="incubatee-application-details-button" class="btn-flat" style="color:deepskyblue;">Application
                    Details</a>
                <br>

                @if($incubatee_uploads != null)
                    <a id="incubatee-uploads-details-button" class="btn-flat txt" style="color:deepskyblue;">Incubatee Uploads</a>
                    <br>
                @endif

                @if($incubatee_applicant != null)
                    @if(count($inucbatee_applicant_panelists_array) > 0)
                        <a id="incubatee-applicant-panel-details-button" class="btn-flat txt" style="color:deepskyblue;">Applicant
                            Panel</a>
                        <br>
                    @endif
                @endif

                @if($incubatee_bootcamper != null)
                    <a id="incubatee-bootcamper-details-button" class="btn-flat txt" style="color:deepskyblue;">Bootcamper
                        Details</a>
                    <br>

                    @if(count($incubatee_bootcamper_events_array) > 0)
                        <a id="incubatee-bootcamper-event-details-button" class="btn-flat txt" style="color:deepskyblue;">Bootcamper
                            Events</a>
                        <br>
                    @endif

                    @if(count($inucbatee_bootcamper_panelists_array) > 0)
                        <a id="incubatee-bootcamper-panel-details-button" class="btn-flat txt" style="color:deepskyblue;">Bootcamper
                            Panel</a>
                        <br>
                    @endif
                @endif

                @if(count($incubatee_events_array) > 0)
                    <a id="incubatee-events-details-button" class="btn-flat txt" style="color:deepskyblue;">Incubatee Events
                        Details</a>
                    <br>
                @endif

                @if(count($incubatee_deregistered_events_array) > 0)
                    <a id="incubatee-de-registered-events-details-button" class="btn-flat txt" style="color:deepskyblue;">Incubatee
                        De-Registered Events Details</a>
                    <br>
                @endif
                <br>

                {{--                <a id="declined-bootcamper-event-details-button" class="btn-flat" style="color:white;">Event Details</a>--}}
                {{--                <br>--}}
                {{--                <a id="declined-bootcamper-application-details-button" class="btn-flat" style="color:white;">Application Details</a>--}}
                {{--                <br>--}}
                {{--                <a id="declined-bootcamper-panel-details-button" class="btn-flat" style="color:white;">Panel Details</a>--}}

            </div>
            <div class="col s2"></div>

            <div class="col l7 card" style="border-radius: 10px;margin-left: 10px">
                <p class="center-align" style="font-size: 1.5em;background-color: #00acc1;height: 5vh;color: white;font-weight: bold;">
                    Overview of {{$incubatee_user->name}} {{$incubatee_user->surname}}</p>
                @if($incubatee_user != null)
                    <div class="row" id="incubatee-account-details-row" style="margin-right: 3em; margin-left: 3em;">
                        <h5 style="color: #00b0ff"><u>Account Details</u></h5>
                        <div class="col s5">
                            <img style="width:100%; height:30%;border-radius: 50%" src="/storage/{{isset($incubatee->uploads->profile_picture_url)?$incubatee->uploads->profile_picture_url:'images/ProfilePictures/NoProfilePic.jpg'}}"/>
                        </div>
                        <div class="col s7">
                        <span>
                            @if(isset($incubatee_user->title))
                                Title &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->title}}
                            @else
                                Title: No Title
                            @endif
                            <br>

                            @if(isset($incubatee_user->initials))
                                Initials &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->initials}}
                            @else
                                Initials: No Initials
                            @endif
                            <br>

                            @if(isset($incubatee_user->name))
                                Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->name}}
                            @else
                                Name: No Name
                            @endif
                            <br>

                            @if(isset($incubatee_user->surname))
                                Surname &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->surname}}
                            @else
                                Surname: No Surname
                            @endif
                            <br>

                            @if(isset($incubatee_user->email))
                                Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->email}}
                            @else
                                Email: No Email
                            @endif
                            <br>

                            @if(isset($incubatee_user->contact_number))
                                Contact Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->contact_number}}
                            @else
                                Contact Number: No contact number
                            @endif
                            <br>

                            @if(isset($incubatee_user->id_number))
                                ID Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->id_number}}
                            @else
                                ID Number: No ID number
                            @endif
                            <br>

                            @if(isset($incubatee_user->dob))
                                Date of birth &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:{{$incubatee_user->dob}}
                            @else
                                Date of birth: No date of birth
                            @endif
                            <br>

                            @if(isset($incubatee_user->age))
                                Age &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->age}}
                            @else
                                Age: No age
                            @endif
                            <br>

                            @if(isset($incubatee_user->gender))
                                Gender &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->gender}}
                            @else
                                Gender: No gender
                            @endif
                            <br>

                            @if(isset($incubatee_user->race))
                                Race &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->race}}
                            @else
                                Race: No race
                            @endif
                            <br>

                            @if(isset($incubatee_user->address_one))
                                Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->address_one}}, {{$incubatee_user->address_two}}, {{$incubatee_user->address_three}}
                            @else
                                Address: No address
                            @endif
                            <br>

                            @if(isset($incubatee_user->city))
                                City &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->city}}
                            @else
                                City: No city
                            @endif
                            <br>

                            @if(isset($incubatee_user->code))
                                Postal code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$incubatee_user->code}}
                            @else
                                Postal code: No postal code
                            @endif
                        </span>
                        </div>
                    </div>
                @endif

                @if($incubatee_applicant != null)
                    <div class="row" id="incubatee-application-details-row"
                         style="margin-right: 3em; margin-left: 3em;" hidden>
                        <h5 style="color: #00b0ff"><u>Application Details</u></h5>
                        @if(isset($incubatee_applicant->venture_name))
                            <p><b>Venture Name:</b> {{$incubatee_applicant->venture_name}}</p>
                        @else
                            <p><b>Venture Name:</b></p> No venture name
                        @endif

                        @if(isset($incubatee_applicant->chosen_category))
                            <p><b>Chosen Category:</b> {{$incubatee_applicant->chosen_category}}</p>
                        @else
                            <p><b>Chosen Category:</b> No category selected</p>
                        @endif

                        @if(isset($incubatee_applicant->created_at))
                            <p><b>Date Applied:</b> {{$incubatee_applicant->created_at->toDateString()}}</p>
                        @else
                            <p><b>Date Applied:</b> No date</p>
                        @endif

                        @if($incubatee_applicant->contacted_via != null)
                            <p><b>Contacted Via:</b> {{$incubatee_applicant->contacted_via}}</p>
                        @else
                            <p><b>Contacted Via:</b> No information</p>
                        @endif
                        @if($incubatee_applicant->result_of_contact != null)
                            <p><b>Result of Contact:</b> {{$incubatee_applicant->result_of_contact}}</p>
                        @else
                            <p><b>Result of Contact:</b> No information</p>
                        @endif
                        @if($incubatee_applicant->total_panel_score != null)
                            <p><b>Total Panel Score:</b> {{$incubatee_applicant->total_panel_score}}</p>
                        @else
                            <p><b>Total Panel Score:</b> No total panel score</p>
                        @endif
                        @if($incubatee_applicant->average_panel_score != null)
                            <p><b>Average Panel Score:</b> {{$incubatee_applicant->average_panel_score}}</p>
                        @else
                            <p><b>Average Panel Score:</b> No average panel score</p>
                        @endif

                        @if(count($incubatee_application_question_answers_array) > 0)
                            <span>
                                @foreach($incubatee_application_question_answers_array as $item)
                                    <b>{{$item->question_number}} - {{$item->question_text}}</b>
                                    <br>
                                    <b>Answer:</b> {{$item->answer_text}}
                                    <br>
                                    <br>
                                @endforeach
                            </span>
                        @endif
                    </div>
                @endif

                @if($incubatee_uploads != null)
                    <div class="row" id="incubatee-uploads-details-row" style="margin-right: 3em; margin-left: 3em;">
                        <h5 style="color: #00b0ff"><u>Uploads Details</u></h5>
                        <span>
                            @if($incubatee_uploads->id_document != null)
                                <b>ID Document:</b><a href="{{'/download-id-document/'.$incubatee_uploads->id}}" >{{$incubatee_uploads->file_description}}</a>
                            @elseif(isset($incubatee_bootcamper->id_document_url))
                                <b>ID Document:</b> Click <a href="{{$incubatee_bootcamper->id_document_url}}"
                                                             target="_blank">here</a>
                            @else
                                <b>ID Document:</b> Not uploaded yet.
                            @endif
                            <br>
                            <br>
                            @if($incubatee_uploads->stage_2_pitch_video_title != null)
                                    <p style="margin-left: 2em;margin-right: 2em;"><b>{{$incubatee_uploads->stage_2_pitch_video_title}}</b></p>
                                @endif
                                @if($incubatee_uploads->stage_2_pitch_video !=null)
                                    <iframe style="margin-left: 2em;" src="{{isset($incubatee_uploads->stage_2_pitch_video)?$incubatee_uploads->stage_2_pitch_video:''}}" width="630" height="400" allow="autoplay;" allowfullscreen></iframe>
                                @endif
                                {{-- Uploaded document uploaded from Stage 1 Bootcamper--}}
                                 <h5 style="color: #00b0ff"><u>Bootcamper Uploads Details</u></h5>
                                <div class="row">
                                      @if($incubatee_user->bootcamper->pitch_video_link == null)
                                        <p>Video Pitch: Not uploaded.</p>
                                    @else
                                        <a href="{{$incubatee_user->bootcamper->pitch_video_link}}">Pitch video</a>
                                    @endif
                                </div>
                              <div class="row">
                                @if($incubatee_user->bootcamper->id_document_url == null)
                                      <p>ID Document: Not uploaded.</p>
                                  @else
                                      <a href="{{'/storage/'.$incubatee_user->bootcamper->id_document_url}}" target="_blank">ID Document Link</a>
                                  @endif
                              </div>
                             <div class="row">
                                @if($incubatee_user->bootcamper->cipc == null)
                                     <p>CIPC: Not uploaded.</p>
                                 @else
                                     <a href="{{'/storage/'.$incubatee_user->bootcamper->cipc}}" target="_blank">CIPC</a>
                                 @endif
                              </div>
                            <div class="row">
                                @if($incubatee_user->bootcamper->proof_of_address == null)
                                    <p>Proof of address: Not uploaded.</p>
                                @else
                                    <a href="{{'/storage/'.$incubatee_user->bootcamper->proof_of_address}}" target="_blank">Proof of address</a>
                                @endif
                              </div>

                        </span>
                    </div>
                @endif

                @if($incubatee_applicant != null)
                    @if(count($inucbatee_applicant_panelists_array) > 0)
                        <div class="row" id="incubatee-applicant-panel-details-row"
                             style="margin-right: 3em; margin-left: 3em;" hidden>
                            <h5 style="color: #00b0ff"><u>Panel Details</u></h5>
                            <span>
                                @if($incubatee_applicant_panel_selection_date != null)
                                    Panel Date: {{$incubatee_applicant_panel_selection_date}}
                                @else
                                    Panel Date: No date
                                @endif
                                <br>

                                @if($incubatee_applicant_panel_selection_time != null)
                                    Panel Time: {{$incubatee_applicant_panel_selection_time}}
                                @else
                                    Panel Time: No time
                                @endif
                                <br>

                                @if($incubatee_applicant_total_panel_score != null)
                                    Panel Score: {{$incubatee_applicant_total_panel_score}}
                                @else
                                    Panel Score: No total panel score
                                @endif
                                <br>

                                @if($incubatee_applicant_average_panel_score != null)
                                    Average Score: {{$incubatee_applicant_average_panel_score}}
                                @else
                                    Avergae Score: No average panel score
                                @endif
                                <br>
                                <br>
                            </span>

                            @if(count($inucbatee_applicant_panelists_array) > 0)
                                @foreach($inucbatee_applicant_panelists_array as $p_object)
                                    <div class="col l3 m3 s12">
                                        <ul class="collapsible">
                                            <li>
                                                <div class="collapsible-header">{{$p_object->panelist_details}}</div>
                                                <div class="collapsible-body">
                                                    @if(count($p_object->panelist_question_answers) > 0)
                                                        @foreach($p_object->panelist_question_answers as $p_question_answer)
                                                            <span>
                                                                <b>{{$p_question_answer->question_number}} - {{$p_question_answer->question_text}}</b>
                                                                <br>
                                                                <b>Score:</b> {{$p_question_answer->panelist_score}}
                                                                <br>
                                                                <b>Comment:</b> {{$p_question_answer->panelist_comment}}
                                                                <br>
                                                                <br>
                                                            </span>
                                                        @endforeach
                                                    @else
                                                        No question / answers has been submitted.
                                                    @endif

                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endif
                @endif

                @if($incubatee_bootcamper != null)
                    <div class="row" id="incubatee-bootcamper-details-row" style="margin-right: 3em; margin-left: 3em;"
                         hidden>
                        <h5 style="color: #00b0ff"><u>Bootcamper Details</u></h5>
                        @if(isset($incubatee_bootcamper->ventureCategory))
                            <p><b>Category:</b> {{$incubatee_bootcamper->ventureCategory->category_name}}</p>
                        @else
                            <p><b>Category:</b> No category name</p>
                        @endif

                        @if(isset($incubatee_bootcamper->created_at))
                            <p><b>Date Joined:</b> {{$incubatee_bootcamper->created_at->toDateString()}}</p>
                        @else
                            <p><b>Date Joined:</b> No date</p>
                        @endif
                        <br>
                        <p><u><b>Uploads:</b></u></p>
                        @if($incubatee_bootcamper->id_document_url != null)
                            <p><b>ID Document:</b> Click <a href="{{$incubatee_bootcamper->id_document_url}}"
                                                            target="_blank">here</a></p>
                        @else
                            <p><b>ID Document:</b> Not uploaded. </p>
                        @endif
                        <br>

                        @if($incubatee_bootcamper->pitch_video_link != null)
                            <p><b>Pitch Video Date Uploaded:</b> {{$incubatee_bootcamper->pitch_video_date_time}}</p>
                            <p><b>Pitch Video One:</b> Click <a href="{{$incubatee_bootcamper->pitch_video_link}}"
                                                                target="_blank">here</a></p>
                        @else
                            <p><b>Pitch Video One:</b> No link provided. </p>
                        @endif
                        <br>

                        @if($incubatee_bootcamper->pitch_video_link_two != null)
                            <p><b>Pitch Video Two:</b> Click <a href="{{$incubatee_bootcamper->pitch_video_link_two}}"
                                                                target="_blank">here</a></p>
                        @else
                            <p><b>Pitch Video Two:</b> No link provided. </p>
                        @endif
                        <br>
                    </div>

                    @if(count($incubatee_bootcamper_events_array) > 0)
                        <div class="row" id="incubatee-bootcamper-event-details-row"
                             style="margin-right: 3em; margin-left: 3em;" hidden>
                            <h5 style="color: #00b0ff"><u>Event Details</u></h5>
                            @foreach($incubatee_bootcamper_events_array as $b_event)
                                <div class="col l3 m3 s12">
                                    <ul class="collapsible">
                                        <li>
                                            <div class="collapsible-header txt" style="color: #00b0ff">{{$b_event->event_title}}</div>
                                            <div class="collapsible-body">
                                        <span>
                                            Event Date: {{$b_event->event_date}}
                                            <br>
                                            Date Registered: {{$b_event->date_registered}}
                                            <br>
                                            @if($b_event->accepted == true)
                                                Accepted: Yes
                                            @else
                                                Accepted: No
                                            @endif
                                            <br>
                                            @if($b_event->declined == true)
                                                Declined: Yes
                                            @else
                                                Declined: No
                                            @endif
                                            <br>
                                            @if($b_event->attended == true)
                                                Attended: Yes
                                            @else
                                                Attended: No
                                            @endif
                                        </span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    @if(count($inucbatee_bootcamper_panelists_array) > 0)
                        <div class="row" id="incubatee-bootcamper-panel-details-row"
                             style="margin-right: 3em; margin-left: 3em;" hidden>
                            <h5 style="color: #00b0ff"><u>Panel Details</u></h5>
                            <span>
                                Panel Date: {{$incubatee_bootcamper_panel_selection_date}}
                                <br>
                                Panel Time: {{$incubatee_bootcamper_panel_selection_time}}
                                <br>
                                Panel Score: {{$incubatee_bootcamper_total_panel_score}}
                                <br>
                                Average Score: {{$incubatee_bootcamper_average_panel_score}}
                                <br>
                                <br>
                            </span>
                            @foreach($inucbatee_bootcamper_panelists_array as $p_object)
                                <div class="col l3 m3 s12">
                                    <ul class="collapsible">
                                        <li>
                                            <div class="collapsible-header txt" style="color: #00b0ff">{{$p_object->panelist_details}}</div>
                                            <div class="collapsible-body">
                                                @foreach($p_object->panelist_question_answers as $p_question_answer)
                                                    <span>
                                                        <b>{{$p_question_answer->question_number}} - {{$p_question_answer->question_text}}</b>
                                                        <br>
                                                        <b>Score:</b> {{$p_question_answer->panelist_score}}
                                                        <br>
                                                        <b>Comment:</b> {{$p_question_answer->panelist_comment}}
                                                        <br>
                                                        <br>
                                                    </span>
                                                @endforeach
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endif

                @if(count($incubatee_events_array) > 0)
                    <div class="row" id="incubatee-events-details-row" style="margin-right: 3em; margin-left: 3em;"
                         hidden>
                        <h5 style="color: #00b0ff"><u>Event Details</u></h5>
                        @foreach($incubatee_events_array as $i_event)
                            <div class="col l3 m3 s12">
                                <ul class="collapsible">
                                    <li>
                                        @if(isset($i_event->event_title))
                                            <div class="collapsible-header">{{$i_event->event_title}}</div>
                                        @else
                                            <div class="collapsible-header">No event title</div>
                                        @endif
                                        <div class="collapsible-body">
                                        <span>
                                            @if(isset($i_event->event_date))
                                                Event Date: {{$i_event->event_date}}
                                            @else
                                                Event Date: No event date
                                            @endif
                                            <br>

                                            @if(isset($i_event->date_time_registered))
                                                Date Registered: {{$i_event->date_time_registered}}
                                            @else
                                                Date Registered: No date registered
                                            @endif
                                            <br>

                                            @if($i_event->attended == true)
                                                Attended: Yes
                                            @else
                                                Attended: No
                                            @endif
                                            <br>

                                            @if($i_event->found_out_via != null)
                                                Found Out Via: Yes
                                            @else
                                                Found Out Via: No
                                            @endif
                                            <br>
                                        </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        @endforeach
                    </div>
                @endif

                @if(count($incubatee_deregistered_events_array) > 0)
                    <div class="row" id="incubatee-de-registered-events-details-row"
                         style="margin-right: 3em; margin-left: 3em;" hidden>
                        <h5 style="color: #00b0ff"><u>Event Details</u></h5>
                        @foreach($incubatee_deregistered_events_array as $i_event)
                            <div class="col l3 m3 s12">
                                <ul class="collapsible">
                                    <li>
                                        @if(isset($i_event->event_title))
                                            <div class="collapsible-header">{{$i_event->event_title}}</div>
                                        @else
                                            <div class="collapsible-header">No event title</div>
                                        @endif
                                        <div class="collapsible-body">
                                        <span>
                                            @if(isset($i_event->event_date))
                                                Event Date: {{$i_event->event_date}}
                                            @else
                                                Event Date: No event date
                                            @endif
                                            <br>

                                            @if(isset($i_event->date_time_de_register))
                                                Date De-Registered: {{$i_event->date_time_de_register}}
                                            @else
                                                Date De-Registered: No date registered
                                            @endif
                                            <br>

                                            @if($i_event->de_register_reason != null)
                                                De-register Reason: {{$i_event->de_register_reason}}
                                            @else
                                                De-register Reason: No reason given.
                                            @endif
                                            <br>
                                        </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>

    <style>
        .txt:hover {
            text-decoration: underline;
        } nav {
              margin-bottom: 0;
              background-color: grey;
              padding: 5px 16px;
          }
    </style>

    <script>
        $('#incubatee-account-details-button').on('click', function () {
            $('#incubatee-account-details-row').show();
            $('#incubatee-application-details-row').hide();
            $('#incubatee-bootcamper-details-row').hide();
            $('#incubatee-bootcamper-event-details-row').hide();
            $('#incubatee-bootcamper-panel-details-row').hide();
            $('#incubatee-applicant-panel-details-row').hide();
            $('#incubatee-events-details-row').hide();
            $('#incubatee-de-registered-events-details-row').hide();
            $('#incubatee-uploads-details-row').hide();
        });

        $('#incubatee-application-details-button').on('click', function () {
            $('#incubatee-account-details-row').hide();
            $('#incubatee-application-details-row').show();
            $('#incubatee-bootcamper-details-row').hide();
            $('#incubatee-bootcamper-event-details-row').hide();
            $('#incubatee-bootcamper-panel-details-row').hide();
            $('#incubatee-applicant-panel-details-row').hide();
            $('#incubatee-events-details-row').hide();
            $('#incubatee-de-registered-events-details-row').hide();
            $('#incubatee-uploads-details-row').hide();
        });

        $('#incubatee-applicant-panel-details-button').on('click', function () {
            $('#incubatee-account-details-row').hide();
            $('#incubatee-application-details-row').hide();
            $('#incubatee-bootcamper-details-row').hide();
            $('#incubatee-bootcamper-event-details-row').hide();
            $('#incubatee-bootcamper-panel-details-row').hide();
            $('#incubatee-applicant-panel-details-row').show();
            $('#incubatee-events-details-row').hide();
            $('#incubatee-de-registered-events-details-row').hide();
            $('#incubatee-uploads-details-row').hide();
        });

        $('#incubatee-bootcamper-details-button').on('click', function () {
            $('#incubatee-account-details-row').hide();
            $('#incubatee-application-details-row').hide();
            $('#incubatee-bootcamper-details-row').show();
            $('#incubatee-bootcamper-event-details-row').hide();
            $('#incubatee-bootcamper-panel-details-row').hide();
            $('#incubatee-applicant-panel-details-row').hide();
            $('#incubatee-events-details-row').hide();
            $('#incubatee-de-registered-events-details-row').hide();
            $('#incubatee-uploads-details-row').hide();
        });

        $('#incubatee-bootcamper-event-details-button').on('click', function () {
            $('#incubatee-account-details-row').hide();
            $('#incubatee-application-details-row').hide();
            $('#incubatee-bootcamper-details-row').hide();
            $('#incubatee-bootcamper-event-details-row').show();
            $('#incubatee-bootcamper-panel-details-row').hide();
            $('#incubatee-applicant-panel-details-row').hide();
            $('#incubatee-events-details-row').hide();
            $('#incubatee-de-registered-events-details-row').hide();
            $('#incubatee-uploads-details-row').hide();
        });

        $('#incubatee-bootcamper-panel-details-button').on('click', function () {
            $('#incubatee-account-details-row').hide();
            $('#incubatee-application-details-row').hide();
            $('#incubatee-bootcamper-details-row').hide();
            $('#incubatee-bootcamper-event-details-row').hide();
            $('#incubatee-bootcamper-panel-details-row').show();
            $('#incubatee-applicant-panel-details-row').hide();
            $('#incubatee-events-details-row').hide();
            $('#incubatee-de-registered-events-details-row').hide();
            $('#incubatee-uploads-details-row').hide();
        });

        $('#incubatee-events-details-button').on('click', function () {
            $('#incubatee-account-details-row').hide();
            $('#incubatee-application-details-row').hide();
            $('#incubatee-bootcamper-details-row').hide();
            $('#incubatee-bootcamper-event-details-row').hide();
            $('#incubatee-bootcamper-panel-details-row').hide();
            $('#incubatee-applicant-panel-details-row').hide();
            $('#incubatee-events-details-row').show();
            $('#incubatee-de-registered-events-details-row').hide();
            $('#incubatee-uploads-details-row').hide();
        });

        $('#incubatee-de-registered-events-details-button').on('click', function () {
            $('#incubatee-account-details-row').hide();
            $('#incubatee-application-details-row').hide();
            $('#incubatee-bootcamper-details-row').hide();
            $('#incubatee-bootcamper-event-details-row').hide();
            $('#incubatee-bootcamper-panel-details-row').hide();
            $('#incubatee-applicant-panel-details-row').hide();
            $('#incubatee-events-details-row').hide();
            $('#incubatee-de-registered-events-details-row').show();
            $('#incubatee-uploads-details-row').hide();
        });

        $('#incubatee-uploads-details-button').on('click', function () {
            $('#incubatee-account-details-row').hide();
            $('#incubatee-application-details-row').hide();
            $('#incubatee-bootcamper-details-row').hide();
            $('#incubatee-bootcamper-event-details-row').hide();
            $('#incubatee-bootcamper-panel-details-row').hide();
            $('#incubatee-applicant-panel-details-row').hide();
            $('#incubatee-events-details-row').hide();
            $('#incubatee-de-registered-events-details-row').hide();
            $('#incubatee-uploads-details-row').show();
        });
    </script>
@endsection
