@extends('layouts.administrator-layout')
@section('content')
    <input id="user-id-input" disabled hidden value="{{$user->id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('book-venue',$user)}}
    <div class="container">
        <br>
        <br>
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white">Book A Venue Here!</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="center">
                @foreach($venues as $venue)
                    <div class="col l4 s12 m4">
                        <div class="card white hoverable center" style="border-radius: 20px;">
                            <div class="circle">
                                <div class="card-content black-text" style="height: 100%;">
                                    <div class="user-view" align="center">
                                    </div>
                                </div>
                                <p class=""><b>{{$venue->venue_name}}</b></p>
                                <h6 class="">{{$venue->number_of_seats}}</h6>
                            </div>
                            <br>
                            <br>
                            <div class="card-action" style="border-radius: 20px">
                                <div class="row booking" style="margin-left: 0.5em;">
                                    <a href="{{url('/venue-booking/'.$venue->id)}}"
                                       class="booking-button"
                                       style="color: orange;">Book A Venue
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="center">
                <div class="col l12 card" style="background: #6c757d">
                    <h5 style="color: white" id="registered-header">Venues you have booked for.</h5>
                </div>
            </div>
        </div>
        <input style="width: 40%;margin-left: 58%;border: 1px #CCC solid;" type="text" id="search" placeholder="Type to search">

        <table id="table" style="width:100%;margin-left: 2em;margin-right: 2em">
            <tr>
                <th>Venue</th>
                <th>Venue Date</th>
                <th>Start time</th>
                <th>End time</th>
                <th>Booking Reason</th>
                <th>Update</th>
                <th>Cancel</th>

            </tr>
            @foreach($user_bookings as $user_booking)
                <tr>
                    <td>{{$user_booking->venue_name}}</td>
                    <td>{{$user_booking->venue_date}}</td>
                    <td>{{$user_booking->venue_start_time}}</td>
                    <td>{{$user_booking->venue_end_time}}</td>
                    <td>{{$user_booking->booking_reason}}</td>
                    <th><a class="booking-button modal-trigger"
                           href="{{url('/edit-venue-booking/'.$user_booking->id)}}"
                           style="color: orange;">Update Booking
                        </a>
                    </th>
                    <th><a id="{{$user_booking->id}}"
                           style="color: orange; cursor: pointer;" onclick="deleteBooking(this)">Cancel Booking
                        </a></th>
                </tr>
            @endforeach
        </table>



    </div>


    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>

    @push('custom-scripts')
        <script>
            var $rows = $('#table tr');
            $('#search').keyup(function() {

                var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
                    reg = RegExp(val, 'i'),
                    text;

                $rows.show().filter(function() {
                    text = $(this).text().replace(/\s+/g, ' ');
                    return !reg.test(text);
                }).hide();
            });
            function deleteBooking(obj) {
                var r = confirm("Are you sure want to delete this booking?");
                console.log("Check", r);
                if (r) {
                    $.get('/cancel-delete-check/' + obj.id, function (data, status) {
                        console.log('Data', data);
                        console.log('Status', status);
                        if (status == 'success') {
                            alert(data.message);
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Delete action cancelled');
                }
            }
        </script>
    @endpush
@endsection
