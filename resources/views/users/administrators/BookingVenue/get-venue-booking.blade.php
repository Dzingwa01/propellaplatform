@extends('layouts.administrator-layout')

@section('content')
    <input id="user-id-input" disabled hidden value="{{$users->id}}">
    <input id="venue-id-input" disabled hidden value="{{$venue->id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('book-a-venue',$users,$venue)}}

    <br>
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Venue Bookings Time Slots</h6>
        </div>
        <input id="venue-id-input" value="{{$venue->id}}" hidden disabled>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="venue-table">
                    <thead>
                    <tr>
                        <th>Venue Name</th>
                        <th>Date</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Booking Person</th>
                        <th>Booking Reason</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Blog" onclick="bookVenue()">
                <i class="large material-icons">add</i>
            </a>

        </div>

    </div>
    @push('custom-scripts')
        <script>
            let venue_id = $('#venue-id-input').val();

            $(document).ready(function (data) {
                $(function () {
                    $('#venue-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '/get-venue-bookings/' + venue_id,
                        columns: [
                            {data: 'venue_name', name: 'venue_name'},
                            {data: 'venue_date', name: 'venue_date'},
                            {data: 'venue_start_time', name: 'venue_start_time'},
                            {data: 'venue_end_time', name: 'venue_end_time'},
                            {data: 'booking_person', name: 'booking_person'},
                            {data: 'booking_reason', name: 'booking_reason'}
                        ]
                    });
                    $('select[name="venue-table_length"]').css("display","inline");
                });
            });

            function bookVenue(){
                window.location.href = '/book-venue/' + venue_id;
            }
        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
{{--    <br>--}}
{{--    <h4>Upcoming bookings for <b>{{$venue->venue_name}}.</b></h4>--}}
{{--    <br>--}}

{{--    <div class="row">--}}
{{--        <div class="center">--}}
{{--            <input id="venue-id-input" value="{{$venue->id}}" hidden disabled>--}}
{{--            @if($venue->venue_name == 'Propella Training Room')--}}
{{--                @foreach($venue->venueBooking as $user_booking)--}}
{{--                    @if($user_booking->venue_date >= $date)--}}

{{--                        <div class="col s12 m3">--}}
{{--                            <div class="card green hoverable center" style="border-radius: 20px;">--}}
{{--                                <br>--}}
{{--                                <div class="circle ">--}}
{{--                                    <h5 class="">DATE BOOKED: <b>{{$user_booking->venue_date}}</b></h5>--}}
{{--                                    <h5 class="">START TIME: <b>{{$user_booking->venue_start_time}}</b></h5>--}}
{{--                                    <h5 class="">END TIME: <b>{{$user_booking->venue_end_time}}</b></h5>--}}
{{--                                    <h5 class="">BOOKING PERSON: <br><b>{{$user_booking->booking_reason}}</b></h5>--}}
{{--                                </div>--}}
{{--                                <br>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @elseif($venue->venue_name == '4IR Meeting Room (ground floor)')--}}
{{--                @foreach($venue->venueBooking as $user_booking)--}}
{{--                    @if($user_booking->venue_date >= $date)--}}

{{--                        <div class="col s12 m3">--}}
{{--                            <div class="card blue hoverable center" style="border-radius: 20px;">--}}
{{--                                <br>--}}
{{--                                <div class="circle">--}}
{{--                                    <h5 class="">DATE BOOKED: <b>{{$user_booking->venue_date}}</b></h5>--}}
{{--                                    <h5 class="">START TIME: <b>{{$user_booking->venue_start_time}}</b></h5>--}}
{{--                                    <h5 class="">END TIME: <b>{{$user_booking->venue_end_time}}</b></h5>--}}
{{--                                    <h5 class="">REASON: <br><b>{{$user_booking->booking_reason}}</b></h5>--}}
{{--                                </div>--}}
{{--                                <br>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @elseif($venue->venue_name == 'AI Meeting Room (1)')--}}
{{--                @foreach($venue->venueBooking as $user_booking)--}}
{{--                    @if($user_booking->venue_date >= $date)--}}
{{--                        <div class="col s12 m3">--}}
{{--                            <div class="card grey hoverable center" style="border-radius: 20px;">--}}
{{--                                <br>--}}
{{--                                <div class="circle">--}}
{{--                                    <h5 class="">DATE BOOKED: <b>{{$user_booking->venue_date}}</b></h5>--}}
{{--                                    <h5 class="">START TIME: <b>{{$user_booking->venue_start_time}}</b></h5>--}}
{{--                                    <h5 class="">END TIME: <b>{{$user_booking->venue_end_time}}</b></h5>--}}
{{--                                    <h5 class="">REASON: <br><b>{{$user_booking->booking_reason}}</b></h5>--}}
{{--                                </div>--}}
{{--                                <br>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @elseif($venue->venue_name == 'IoT Meeting Room (2)')--}}
{{--                @foreach($venue->venueBooking as $user_booking)--}}
{{--                    @if($user_booking->venue_date >= $date)--}}
{{--                        <div class="col s12 m3">--}}
{{--                            <div class="card grey hoverable center" style="border-radius: 20px;">--}}
{{--                                <br>--}}
{{--                                <div class="circle">--}}
{{--                                    <h5 class="">DATE BOOKED: <b>{{$user_booking->venue_date}}</b></h5>--}}
{{--                                    <h5 class="">START TIME: <b>{{$user_booking->venue_start_time}}</b></h5>--}}
{{--                                    <h5 class="">END TIME: <b>{{$user_booking->venue_end_time}}</b></h5>--}}
{{--                                    <h5 class="">REASON: <br><b>{{$user_booking->booking_reason}}</b></h5>--}}
{{--                                </div>--}}
{{--                                <br>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @elseif($venue->venue_name == 'Propella Board Room')--}}
{{--                @foreach($venue->venueBooking as $user_booking)--}}
{{--                    @if($user_booking->venue_date >= $date)--}}
{{--                        <div class="col s12 m3">--}}
{{--                            <div class="card grey hoverable center" style="border-radius: 20px;">--}}
{{--                                <br>--}}
{{--                                <div class="circle">--}}
{{--                                    <h5 class="">DATE BOOKED: <b>{{$user_booking->venue_date}}</b></h5>--}}
{{--                                    <h5 class="">START TIME: <b>{{$user_booking->venue_start_time}}</b></h5>--}}
{{--                                    <h5 class="">END TIME: <b>{{$user_booking->venue_end_time}}</b></h5>--}}
{{--                                    <h5 class="">REASON: <br><b>{{$user_booking->booking_reason}}</b></h5>--}}
{{--                                </div>--}}
{{--                                <br>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @elseif($venue->venue_name == 'Propella Downstairs Training Venue')--}}
{{--                @foreach($venue->venueBooking as $user_booking)--}}
{{--                    @if($user_booking->venue_date >= $date)--}}
{{--                        <div class="col s12 m3">--}}
{{--                            <div class="card grey hoverable center" style="border-radius: 20px;">--}}
{{--                                <br>--}}
{{--                                <div class="circle">--}}
{{--                                    <h5 class="">DATE BOOKED: <b>{{$user_booking->venue_date}}</b></h5>--}}
{{--                                    <h5 class="">START TIME: <b>{{$user_booking->venue_start_time}}</b></h5>--}}
{{--                                    <h5 class="">END TIME: <b>{{$user_booking->venue_end_time}}</b></h5>--}}
{{--                                    <h5 class="">REASON: <br><b>{{$user_booking->booking_reason}}</b></h5>--}}
{{--                                </div>--}}
{{--                                <br>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @elseif($venue->venue_name == 'Propella Mezz level')--}}
{{--                @foreach($venue->venueBooking as $user_booking)--}}
{{--                    @if($user_booking->venue_date >= $date)--}}
{{--                        <div class="col s12 m3">--}}
{{--                            <div class="card grey hoverable center" style="border-radius: 20px;">--}}
{{--                                <br>--}}
{{--                                <div class="circle">--}}
{{--                                    <h5 class="">DATE BOOKED: <b>{{$user_booking->venue_date}}</b></h5>--}}
{{--                                    <h5 class="">START TIME: <b>{{$user_booking->venue_start_time}}</b></h5>--}}
{{--                                    <h5 class="">END TIME: <b>{{$user_booking->venue_end_time}}</b></h5>--}}
{{--                                    <h5 class="">REASON: <br><b>{{$user_booking->booking_reason}}</b></h5>--}}
{{--                                </div>--}}
{{--                                <br>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @endif--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="fixed-action-btn">--}}
{{--        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"--}}
{{--           data-tooltip="Add New Blog" onclick="bookVenue()">--}}
{{--            <i class="large material-icons">add</i>--}}
{{--        </a>--}}
{{--    </div>--}}

{{--    @push('custom-scripts')--}}
{{--        <script>--}}
{{--            let venue_id = $('#venue-id-input').val();--}}

{{--            function bookVenue() {--}}
{{--                window.location.href = '/book-venue/' + venue_id;--}}
{{--            }--}}
{{--        </script>--}}
{{--    @endpush--}}


@endsection
