@extends('layouts.administrator-layout')

@section('content')
    <head>
        <style>
            table {
                width: 100%;
            }

            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }

            th, td {
                padding: 15px;
                text-align: left;
            }

            table#t01 tr:nth-child(even) {
                background-color: #eee;
            }

            table#t01 tr:nth-child(odd) {
                background-color: #fff;
            }
            table#t01 th {
                background-color: white;
                color: black;
            }
            .txt:hover {
                text-decoration: underline;
            }
        </style>
    </head>

    <div class="section" style="margin-top: 5vh">
        <input id="user-id-input" value="{{$user->id}}" hidden disabled>
        <div class="col s10 card z-depth-4" style="border-radius: 10px;width: 1350px;margin: 0 auto">
            {{--APPPLICATION ACOUNT INFO--}}
            <div class="row" id="applicant-account-show" style="margin-right: 3em; margin-left: 3em;">
                <br>
                <h4 class="center"><b>OVERVIEW OF : {{$user->name}} {{$user->surname}}</b></h4>
                <div class="status-color center">
                    <p id="status-color"><span style="color: black">Applicantion Status :</span> <span
                            style="text-transform: uppercase"><b>{{$user_applicant->status}}</b></span></p>
                </div>
                <div class="row " style="margin-left: 10em">
                    <br>
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h6><b>Category : </b> {{$applicant->chosen_category}}</h6>
                            <h6><b>Email : </b> {{$user->email}}</h6>
                            <h6><b>Contact Number : </b> {{$user->contact_number}}</h6>
                            <h6><b>ID Number : </b> {{$user->id_number}}</h6>
                            <h6><b>Age : </b> {{$user->age}}</h6>
                            <h6><b>Gender : </b> {{$user->gender}}</h6>
                            <h6><b>Race : </b> {{$user->race}}</h6>
                            <h6><b>WhatsApp Number : </b> {{$user->whatsapp_number}}</h6>
                        </div>
                        <div class="col l6 m6 s12">
                            <h6><b>Data Cell Number : </b> {{$user->data_cellnumber}}</h6>
                            <h6><b>Service Provider : </b> {{$user->service_provider_network}}</h6>
                            <h6><b>Address One : </b> {{$user->address_one}}</h6>
                            <h6><b>Address Two  </b> {{$user->address_two}}</h6>
                            <h6><b>Address Three : </b> {{$user->address_three}}</h6>
                            <h6><b>Application Created : </b> {{$applicant->created_at}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s2"></div>
        <div class="row center">
            <h4><b>ACTIONS</b></h4>
        </div>
        <div class="row" style="margin-left: 9em">
            <div class="card col s2" id="application" style="background-color:rgba(0, 47, 95, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">APPLICATION FORM</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2" id="contact-log" style="background-color:#454242;font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">CONTACT LOG</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="go-no-go-panel" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">GO / NO GO PANEL</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="panel-interview-overview" style="background-color:#454242;font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">PANEL RESULTS</p>
            </div>
            <div class="col s0.1"></div>
            <div class="card col s2 " id="go-no-go-incubatee" style="background-color: rgba(0, 121, 52, 1);font-weight: bold">
                <p class="center txt" style="color: white;cursor:pointer">GO / NO GO INCUBATEE</p>
            </div>

        </div>
    </div>

    <div class="section" style="margin-top: 5vh">
        <input id="user-id-input" value="{{$user->id}}" hidden disabled>
        <div class="col s12 card" style="border-radius: 10px;margin-left: 10em;margin-right: 10em">
            {{--APPLICATION FORM--}}
            <div class="row" id="application-details-row" hidden style="margin-right: 3em; margin-left: 3em;">
                <div class="container" id="QuestionsAndAnswers">
                    <br>
                    <div class="row center">
                        <h4><b>APPLICATION DETAILS</b></h4>
                        <div class="row">
                            <div class="col l6 m6 s12">
                                <h6>{{$user->name}} {{$user->surname}}</h6>
                                <h6>Category: {{$applicant->chosen_category}}</h6>
                                <h6>Email: {{$user->email}}</h6>
                                <h6>Contact Number: {{$user->contact_number}}</h6>
                                <h6>ID Number: {{$user->id_number}}</h6>
                                <h6>Age: {{$user->age}}</h6>
                                <h6>Gender: {{$user->gender}}</h6>
                            </div>
                            <div class="col l6 m6 s12">
                                <h6>Address One: {{$user->address_one}}</h6>
                                <h6>Address Two: {{$user->address_two}}</h6>
                                <h6>Address Three: {{$user->address_three}}</h6>
                                <h6>Application Created: {{$applicant->created_at}}</h6>
                            </div>
                        </div>
                        <br>
                        <hr style="background: darkblue; height: 5px;">
                        <br>
                    </div>

                    <div class="row">
                        @foreach($userQuestionAnswers as $question_answer)
                            <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                            <div class="input-field">
                                <textarea disabled>{{$question_answer->answer_text}}</textarea>
                            </div>
                        @endforeach
                    </div>
                </div>

                <br>
                <div class="row center">
                    <button id="print-button" class="waves-effect waves-light btn"
                            onclick="printContent('QuestionsAndAnswers')">
                        <i class="material-icons left">local_printshop</i>Print
                    </button>
                </div>
            </div>

            {{-- APPLICANT CONTACT LOG--}}
            <div class="row" id="applicant-contact-log" hidden style="margin-right: 3em; margin-left: 3em;">
                <br>
                <div class="row center">
                    <h4><b>CONTACT LOG</b></h4>
                </div>
                <div class="row">
                    <div class="col s4">
                        <div class="status-color">
                            <p id="status-color">Applicant Status : <span
                                    style="text-transform: uppercase"><b>{{$user_applicant->status}}</b></span></p>
                        </div>
                        <h5 style="color: grey;">Contact log history</h5>
                        <br>

                        @foreach($user_applicant->contactLog->sortByDesc('date') as $user_log)
                            <p><b>Results of contact</b> : {{$user_log->applicant_contact_results}}</p>
                            <p><b>Comment</b> : {{$user_log->comment}}</p>
                            <p><b>Date</b>:{{$user_log->date}}</p>
                            <hr>
                        @endforeach
                    </div>
                    <div class="col s1"></div>
                    <div class="col s7">
                        <h5 style="color: grey;margin-left: 4.5em">Create applicant contact log</h5>
                        <br>
                        <div class="row" style="margin-left: 7em">
                            <div class="input-field col s10">
                                <select id="applicant_contact_results">
                                    <option value="{{$applicant->contacted_via}}" selected>{{$applicant->contacted_via}}</option>
                                    <option value="Phone Call">Phone Call</option>
                                    <option value="Face-to-Face">Face-to-Face</option>
                                    <option value="Email">Email</option>
                                    <option value="No response to any contact type">No response to any contact type
                                    </option>
                                    <option value="Invited, but did not arrive">Invited, but did not arrive</option>
                                </select>
                                <label>You contacted {{$user->name}} {{$user->surname}} via</label>
                            </div>
                        </div>
                        <div class="row" id="comment-section"  style="margin-left: 7em">
                            <div class="input-field col s10">
                                <input id="comment" type="text" class="validate">
                                <label for="comment">Comment</label>
                            </div>
                        </div>
                        <div class="row" id="comment-email-section" hidden style="margin-left: 7em">
                            <div class="input-field col s10">
                                <select id="email_comment">
                                    <option value="and we tried to call you but were unable to reach you on the number on your application form.
                                        We are unable to progress further with your application unless you provide us with a contact number.">You applied to Propella Business Incubator ICT Program and we tried to call you but were unable to reach you on the number on your application form.
                                        Make contact with Propella by with emailing us on reception@propellaincubator.co.za or calling us on 041 502 3700.
                                        We are unable to progress further with your application unless you provide us with a contact number.
                                    </option>
                                    <option value="and you have not completed your application form
                                    We are unable to progress further with your application unless you make contact with us and complete your application form.
                                        ">You applied to Propella Business Incubator ICT Program and you have not completed your application form.
                                        Make contact with Propella by with emailing us on reception@propellaincubator.co.za or calling us on 041 502 3700.
                                        We are unable to progress further with your application unless you make contact with us and complete your application form.</option>
                                    <option value="and we tried to whatsapp  you but were unable to reach you on the whatsapp number on your application form.
                                        We are unable to progress further with your application unless you respond to our email or contact us with a valid whatsapp number.
                                        ">You applied to Propella Business Incubator ICT Program and we tried to whatsapp  you but were unable to reach you on the whatsapp number on your application form.
                                        Make contact with Propella by responding to the whatsapp, via email reception@propellaincubator.co.za or calling us on 041 502 3700.
                                        We are unable to progress further with your application unless you respond to our email or contact us with a valid whatsapp number.

                                    </option>
                                    <option value="Invited, but did not arrive">Invited, but did not arrive</option>
                                </select>
                                <label>Email comment</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 300px;">
                            <div class="col s6 save-contact-log" hidden>
                                <a class="waves-effect waves-light btn" id="contact-log-submit-button">Save contact log</a>
                            </div>
                            <div class="col s6 send-email-to-applicant" hidden>
                                <a class="waves-effect waves-light btn" id="send-email">Send email</a>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 7em">
                            <h5 style="color: grey;">Update applicant status and chosen category</h5>
                            <br>
                            <div class="input-field col s10">
                                <select id="status">
                                    <option value="{{$applicant->status}}" selected>{{$applicant->status}}</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Closed">Closed</option>
                                    <option value="Reviewed">Reviewed</option>
                                    <option value="Emailed">Emailed</option>
                                </select>
                                <label>Applicant status</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 7em">
                            <div class="input-field col s10">
                                <select id="chosen_category">
                                    <option value="{{$applicant->chosen_category}}" selected>{{$applicant->chosen_category}}</option>
                                    <option value="Application form - ICT">Application form - ICT</option>
                                    <option value="Application form - Industrial">Application form - Industrial</option>
                                    <option value="Application form - Propella Satellite">Application form - Propella Satellite</option>
                                </select>
                                <label>Choosen Category</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--GO / NO GO PANEL INTERVIEW--}}
            <div class="row" id="go-no-go-panel-section" hidden style="margin-right: 3em; margin-left: 3em;">
                <div class="section">
                    <input hidden disabled id="user-id-input" value="{{$user->id}}">
                    <div class="row center">
                        <h4><b>GO / NO GO PANEL</b></h4>
                    </div>
                    <div class="row center">
                        <div class="col l6 m6 s12">
                            <i class="material-icons large" style="color: saddlebrown;" onclick="approveApplicant()">thumb_up</i>
                            <h3>Go?</h3>
                        </div>
                        <div class="col l6 m6 s12">
                            <i class="material-icons large" style="color: saddlebrown;" onclick="declineApplicant()">thumb_down</i>
                            <h3>No Go?</h3>
                        </div>
                    </div>

                    <div class="row center" hidden id="go-row">
                        <input id="user-id-input" value="{{$user->id}}" hidden disabled>
                        <div class="">
                            @if(count($applicant->panelists) > 0)
                                <div class="card" style="width: 150vh;">
                                    <br>
                                    <h4><b>GO PANEL UPDATE</b></h4>
                                    <br>
                                    <div class="container-fluid">
                                        <input id="user-id-input" value="{{$user->id}}" hidden disabled>

                                        <div class="row center" style="margin-left: 2em;margin-right: 2em;">
                                            <div class="input-field col m6 s12">
                                                <select id="contact-input-display">
                                                    <option value="{{$applicant->contacted_via}}" selected>{{$applicant->contacted_via}}</option>
                                                    <option value="Phone Call">Phone Call</option>
                                                    <option value="Face-to-Face">Face-to-Face</option>
                                                    <option value="Email">Email</option>
                                                </select>
                                                <label>You contacted {{$user->name}} {{$user->surname}} via</label>
                                            </div>
                                            <div class="input-field col l6 m5 s12">
                                                <select id="contact-result-input-display">
                                                    <option value="{{$applicant->result_of_contact}}"
                                                            selected>{{$applicant->result_of_contact}}</option>
                                                    <option value="Panel Interview">Panel Interview</option>
                                                    <option value="Application Declined">Application - Declined</option>
                                                    <option value="Application Referred">Application - Referred</option>
                                                    <option value="Interview Declined">Interview - Declined</option>
                                                    <option value="Interview Referred">Interview - Referred</option>
                                                </select>
                                                <label>The result of this contact</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row center" style="margin-left: 2em;margin-right: 2em;">
                                        <input hidden disabled id="question-category-id"
                                               value="{{$applicant->panelists[0]->question_category_id}}">
                                        <div class="input-field col s6">
                                            <input type="date" id="panel-selection-date-display"
                                                   value="{{$applicant->panelists[0]->panel_selection_date}}">
                                        </div>
                                        <div class="input-field col s6">
                                            <input type="time" id="panel-selection-time-display"
                                                   value="{{$applicant->panelists[0]->panel_selection_time}}">
                                            <label for="">Time of panel interview</label>
                                        </div>
                                    </div>
                                    <div class="row"style="margin-left: 2em">
                                        <div class="input-field col s6">
                                            @if(isset($applicant->panelists[0]->question_category_id))
                                                <select id="question-category-display">
                                                    <option value="{{$applicant->panelists[0]->question_category_id}}"
                                                            selected>{{$applicant->panelists[0]->question_category}}</option>
                                                    @foreach($question_categories as $question_category)
                                                        <option
                                                            value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="question-category-input">Questions Category</label>
                                            @else
                                                <select id="question-category-display">
                                                    <option value="" disabled selected>Choose Questions</option>
                                                    @foreach($question_categories as $question_category)
                                                        <option
                                                            value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="question-category-input">Questions Category</label>
                                            @endif
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <button class="btn blue" id="update-panel-interview-details-button">Update</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="row center">
                                        <h4><b>PANELIST YOU HAVE CHOOSEN</b></h4>
                                    </div>
                                    <table id="table" style="width:93%;margin-left: 2em;margin-right: 8em">
                                        <tr>
                                            <th>Name</th>
                                            <th>Surname</th>
                                            <th>Email</th>
                                            <th>Company</th>
                                            <th>Position</th>
                                        </tr>
                                        @foreach($applicantion_panelists as $applicant_panelist)
                                            <tr>
                                                <td>{{$applicant_panelist->name}}</td>
                                                <td>{{$applicant_panelist->surname}}</td>
                                                <td>{{$applicant_panelist->email}}</td>
                                                <td>{{$applicant_panelist->company_name}}</td>
                                                <td>{{$applicant_panelist->position}}</td>
                                            </tr>
                                        @endforeach
                                    </table>


                                </div>
                                <h5 class="center" id="add-panelist-header">Want to add more panelists?</h5>
                                <div class="row" style="margin-left: 380px">
                                    <div class="input-field col s6">
                                        <input type="email" id="extra-panelist-email-input" class="validate">
                                        <label class="center" for="email">Enter email address of the person</label>
                                        <btn class="btn blue" onclick="appendPanelist()">Add</btn>
                                    </div>
                                </div>



                                <!-- If the applicant panelists are not set -->
                            @else
                                <div class="container-fluid">
                                    <input id="user-id-input" value="{{$user->id}}" hidden disabled>
                                    <div class="row center">
                                        <br>
                                        <h4><b>PANEL INTERVIEW</b></h4>
                                        <div class="col l6 m6 s12">
                                            <select id="contact-input-panel">
                                                <option value="" disabled selected>How did you
                                                    contact {{$user->name}} {{$user->surname}}?
                                                </option>
                                                <option value="Phone Call">Phone Call</option>
                                                <option value="Face-to-Face">Face-to-Face</option>
                                                <option value="Email">Email</option>
                                                <option value="No response to any contact type">No response to any contact
                                                    type
                                                </option>
                                                <option value="Invited, but did not arrive">Invited, but did not arrive
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <select id="contact-result-input-panel">
                                                <option value="" disabled selected>What was the result?</option>
                                                <option value="Panel Interview">Panel Interview</option>
                                                <option value="Application Declined">App - Declined</option>
                                                <option value="Application Referred">App - Referred</option>
                                                <option value="Interview Declined">Interview - Declined</option>
                                                <option value="Interview Referred">Interview - Referred</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="section" id="panel-interview-row">
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <select id="panelist-input" multiple="multiple">
                                                <option value="" disabled selected>Panelists?</option>
                                                <option value="grant@propellaincubator.co.za">Grant Minnie</option>
                                                <option value="anita@propellaincubator.co.za">Anita Palmer</option>
                                                <option value="woosthuizen@engeli.co.za">Wayne Oosthuizen</option>
                                                <option value="bwiseman@engeli.co.za">Barry Wiseman</option>
                                                <option value="rdames@engeli.co.za">Ricardo Dames</option>
                                                <option value="errol@propellaincubator.co.za">Error Wills</option>
                                                <option value="linda1@propellaincubator.co.za">Linda Lawrie</option>
                                                <option value="daryl.mcwilliams@gmail.com">Daryl McWilliams</option>
                                                <option value="Nqobile.Gumede@mandela.ac.za">Nqobile Gumede</option>
                                                <option value="Mante.Kgaria@mandela.ac.za">Mante Kgaria</option>
                                            </select>

                                            <input type="date" id="panel-selection-date-input">

                                            <label for="panel-selection-time-input">Time of panel interview</label>
                                            <input type="time" id="panel-selection-time-input">
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <div class="row">
                                                <select id="question-category-input">
                                                    <option value="" disabled selected>Choose Questions</option>
                                                    @foreach($question_categories as $question_category)
                                                        <option
                                                            value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <button class="btn blue" id="show-guest-panelist-button">Add Guest Panelist
                                            </button>
                                            <div class="container" id="guest-panelist-container" hidden>
                                                <div class="row">
                                                    <div class="col l6 m6 s12">
                                                        <select id="title-input">
                                                            <option value="" disabled selected>Choose Title</option>
                                                            <option value="Mr">Mr</option>
                                                            <option value="Mrs">Mrs</option>
                                                            <option value="Miss">Miss</option>
                                                            <option value="Ms">Ms</option>
                                                            <option value="Dr">Dr</option>
                                                        </select>
                                                        <label>Title</label>
                                                    </div>
                                                    <div class="col l6 m6 s12">
                                                        <input id="initial-input" name="initial-input" type="text"
                                                               class="validate">
                                                        <label for="initial-input">Initials</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col l6 m6 s12">
                                                        <input id="name-input" name="name-input" type="text"
                                                               class="validate">
                                                        <label for="name-input">First Name</label>
                                                    </div>
                                                    <div class="col l6 m6 s12">
                                                        <input id="surname-input" name="surname-input" type="text"
                                                               class="validate">
                                                        <label for="surname-input">Last Name</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col l6 m6 s12">
                                                        <input id="company-input" name="company-input" type="text"
                                                               class="validate">
                                                        <label for="company-input">Company Name</label>
                                                    </div>
                                                    <div class="col l6 m6 s12">
                                                        <input id="position-input" name="position-input" type="text"
                                                               class="validate">
                                                        <label for="position-input">Position</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col l6 m6 s12">
                                                        <input id="email-input" name="email-input" type="email"
                                                               class="validate">
                                                        <label for="email-input">Email</label>
                                                    </div>
                                                    <div class="col l6 m6 s12">
                                                        <input id="contact-number-input" name="contact-number-input"
                                                               type="text"
                                                               class="validate">
                                                        <label for="contact-number-input">Contact Number</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <button class="btn blue" id="add-guest-panelist-button">Add</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <button class="btn blue" id="panel-interview-button">Finalize</button>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>

                    <div class="row center" hidden id="no-go-row">
                        <div class="row center">
                            <br>
                            <h4><b>NO GO PANEL</b></h4>
                            <br>
                            <div class="col l6 m6 s12">
                                <select id="contact-input">
                                    <option value="" disabled selected>How did you contact {{$user->name}} {{$user->surname}}?</option>
                                    <option value="Phone Call">Phone Call</option>
                                    <option value="Face-to-Face">Face-to-Face</option>
                                    <option value="Email">Email</option>
                                    <option value="No response to any contact type">No response to any contact type</option>
                                    <option value="Invited, but did not arrive">Invited, but did not arrive</option>

                                </select>
                            </div>
                            <div class="col l6 m6 s12">
                                <select id="contact-result-input">
                                    <option value="" disabled selected>What was the result?</option>
                                    <option value="Application Declined">App - Declined</option>
                                    <option value="Application Referred">App - Referred</option>\
                                </select>
                            </div>
                        </div>
                        <!--Declined-->
                        <div class="col l6 m6 s12" id="declined-applicants" hidden>
                            <label for="decline-reason-input">Reason it was declined?</label>
                            <textarea id="decline-but-referred-reason-input"></textarea>
                            <br>
                            <label for="declined-applicant-categories-input">Select category</label>
                            <select id="declined-applicant-categories-input">
                                <option value="" disabled selected>Select applicant category</option>
                                @foreach($declined_applicant_categories as $d_category)
                                    <option value="{{$d_category->id}}">{{$d_category->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col l6 m6 s12" id="declined-button" hidden>
                            <label for="referred-company-input">Who it was referred to?</label>
                            <textarea id="referred-company-input"></textarea>
                            <div class="input-field col m8">
                                <select id="declined_reason_two">
                                    <option value="" disabled selected>Choose decline reason</option>
                                    <option value="Unfortunately your application was unsuccessful, we are recruiting from the Nelson Mandela Bay area only">Unfortunately your application was unsuccessful, we are recruiting from the Nelson Mandela Bay area only</option>
                                    <option value="Unfortunately your application was unsuccessful,	your idea does not fit the business sector that we incubate">Unfortunately your application was unsuccessful, your idea does not fit the business sector that we incubate</option>
                                    <option value="Unfortunately your application was unsuccessful ,your application does not meet our funder’s criteria">Unfortunately your application was unsuccessful, your application does not meet our funder’s criteria</option>
                                    <option value="Unfortunately your application was unsuccessful, your application was incomplete">Unfortunately your application was unsuccessful, your application was incomplete</option>
                                    <option value="Unfortunately your application was unsuccessful, you did not attend the bootcamp">Unfortunately your application was unsuccessful, you did not attend the bootcamp</option>
                                </select>
                                <label>Choose Declined reason</label>
                            </div>
                            <button class="btn blue" id="panel-interview-declined-but-referred-button">Submit</button>
                        </div>

                        <!--Referred-->
                        <div class="col l6 m6 s12" id="referred-applicants" hidden>
                            <label for="referred-reason-input">Reason it was referred?</label>
                            <textarea id="referred-reason-input"></textarea>
                            <br>
                            <label for="referred-applicant-categories-input">Select category</label>
                            <select id="referred-applicant-categories-input">
                                <option value="" disabled selected>Select referred category</option>
                                @foreach($referred_applicant_categories as $r_category)
                                    <option value="{{$r_category->id}}">{{$r_category->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col l6 m6 s12" id="referred-button" hidden>
                            <label for="referred-company-input">Who it was referred to?</label>
                            <textarea id="referred-company-input"></textarea>
                            <button class="btn blue" id="panel-interview-referred-button">Submit Referred</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--APPLICANT PANEL INTERVIEW--}}
        <div class="row" id="applicant-panel-interview" hidden style="margin-right: 3em; margin-left: 3em;">
            <input id="user-id-input" value="{{$user->id}}" hidden disabled>
            <div class="">
                @if(count($applicant->panelists) > 0)
                    <div class="card" style="width: 170vh;margin-left: 120px;">
                        <br>
                        <div class="container-fluid">
                            <input id="user-id-input" value="{{$user->id}}" hidden disabled>

                            <div class="row center" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col m6 s12">
                                    <select id="contact-input-display">
                                        <option value="{{$applicant->contacted_via}}" selected>{{$applicant->contacted_via}}</option>
                                        <option value="Phone Call">Phone Call</option>
                                        <option value="Face-to-Face">Face-to-Face</option>
                                        <option value="Email">Email</option>
                                    </select>
                                    <label>You contacted {{$user->name}} {{$user->surname}} via</label>
                                </div>
                                <div class="input-field col l6 m5 s12">
                                    <select id="contact-result-input-display">
                                        <option value="{{$applicant->result_of_contact}}"
                                                selected>{{$applicant->result_of_contact}}</option>
                                        <option value="Panel Interview">Panel Interview</option>
                                        <option value="Application Declined">Application - Declined</option>
                                        <option value="Application Referred">Application - Referred</option>
                                        <option value="Interview Declined">Interview - Declined</option>
                                        <option value="Interview Referred">Interview - Referred</option>
                                    </select>
                                    <label>The result of this contact</label>
                                </div>
                            </div>
                        </div>
                        <div class="row center" style="margin-left: 2em;margin-right: 2em;">
                            <input hidden disabled id="question-category-id"
                                   value="{{$applicant->panelists[0]->question_category_id}}">
                            <div class="input-field col s6">
                                <input type="date" id="panel-selection-date-display"
                                       value="{{$applicant->panelists[0]->panel_selection_date}}">
                            </div>
                            <div class="input-field col s6">
                                <input type="time" id="panel-selection-time-display"
                                       value="{{$applicant->panelists[0]->panel_selection_time}}">
                                <label for="">Time of panel interview</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                @if(isset($applicant->panelists[0]->question_category_id))
                                    <select id="question-category-display">
                                        <option value="{{$applicant->panelists[0]->question_category_id}}"
                                                selected>{{$applicant->panelists[0]->question_category}}</option>
                                        @foreach($question_categories as $question_category)
                                            <option
                                                value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <select id="question-category-display">
                                        <option value="" disabled selected>Choose Questions</option>
                                        @foreach($question_categories as $question_category)
                                            <option
                                                value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <label for="question-category-input">Questions Category</label>
                        </div>
                        <br>
                        <div class="row">
                            <button class="btn blue" id="update-panel-interview-details-button">Update</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="row center"
                             style="background-color: grey;margin-right: 8em;margin-left: 8em;">
                            <h5 style="color: white;" id="panelist-header"><b>Panelists you have chosen</b></h5>
                        </div>
                        <div class="row" style="margin-left: 7em;margin-right: 7em;">
                            @foreach($applicantion_panelists as $applicant_panelist)
                                <div class="col s6">
                                    <div class="card" style="background: grey">
                                        <div class="card-content">
                                            <div class="row">
                                                <div class="col l6 m6 s12">
                                                    <label for="title-display"
                                                           style="color: white;font-size: 1em"><b>Title</b></label>
                                                    <input id="title-display" disabled name="title-display"
                                                           type="text" value="{{$applicant_panelist->title}}">
                                                </div>
                                                <div class="col l6 m6 s12">
                                                    <label for="initial-display"
                                                           style="color: white;font-size: 1em"><b>Initials</b></label>
                                                    <input id="initial-display" disabled name="initial-display"
                                                           type="text"
                                                           value="{{$applicant_panelist->initials}}">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col l6 m6 s12">
                                                    <label for="name-display"
                                                           style="color: white;font-size: 1em"><b>First
                                                            Name</b></label>
                                                    <input id="name-display" disabled name="name-display"
                                                           type="text" value="{{$applicant_panelist->name}}">
                                                </div>
                                                <div class="col l6 m6 s12">
                                                    <label for="surname-display"
                                                           style="color: white;font-size: 1em"><b>Surname</b></label>
                                                    <input id="surname-display" disabled name="surname-display"
                                                           type="text" value="{{$applicant_panelist->surname}}">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col l6 m6 s12">
                                                    <label for="company-display"
                                                           style="color: white;font-size: 1em"><b>Company
                                                            Name</b></label>
                                                    <input id="company-display" disabled name="company-display"
                                                           type="text"
                                                           value="{{$applicant_panelist->company_name}}">
                                                </div>
                                                <div class="col l6 m6 s12">
                                                    <label for="position-display"
                                                           style="color: white;font-size: 1em"><b>Position</b></label>
                                                    <input id="position-display" disabled
                                                           name="position-display"
                                                           type="text"
                                                           value="{{$applicant_panelist->position}}">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col l6 m6 s12">
                                                    <label for="email-display"
                                                           style="color: white;font-size: 1em"><b>Email</b></label>
                                                    <input id="email-display" disabled name="email-input"
                                                           type="email" value="{{$applicant_panelist->email}}">
                                                </div>
                                                <div class="col l6 m6 s12">
                                                    <label for="contact-number-display"
                                                           style="color: white;font-size: 1em"><b>Contact
                                                            Number</b></label>
                                                    <input id="contact-number-display" disabled
                                                           name="contact-number-display" type="text"
                                                           value="{{$applicant_panelist->contact_number}}">
                                                </div>
                                            </div>
                                            <div class="row center">
                                                <i style="color: red; cursor: pointer;"
                                                   class="material-icons small"
                                                   id="{{$applicant_panelist->applicant_panelist_id}}"
                                                   onclick="deletePanelist(this)">delete_forever</i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="row" style="margin-left: 7em">
                        <h5 id="add-panelist-header">Want to add more panelists?</h5>
                        <div class="input-field col s4">
                            <input type="email" id="extra-panelist-email-input" class="validate">
                            <label for="email">Enter email address of the person</label>
                            <btn class="btn blue" onclick="appendPanelist()">Add</btn>
                        </div>
                    </div>



                    <!-- If the applicant panelists are not set -->
                @else
                    <div class="container-fluid">
                        <input id="user-id-input" value="{{$user->id}}" hidden disabled>
                        <div class="row center">
                            <br>
                            <h4><b>PANEL INTERVIEW</b></h4>
                            <div class="col l6 m6 s12">
                                <select id="contact-input-panel">
                                    <option value="" disabled selected>How did you
                                        contact {{$user->name}} {{$user->surname}}?
                                    </option>
                                    <option value="Phone Call">Phone Call</option>
                                    <option value="Face-to-Face">Face-to-Face</option>
                                    <option value="Email">Email</option>
                                    <option value="No response to any contact type">No response to any contact
                                        type
                                    </option>
                                    <option value="Invited, but did not arrive">Invited, but did not arrive
                                    </option>
                                </select>
                            </div>
                            <div class="col l6 m6 s12">
                                <select id="contact-result-input-panel">
                                    <option value="" disabled selected>What was the result?</option>
                                    <option value="Panel Interview">Panel Interview</option>
                                    <option value="Application Declined">App - Declined</option>
                                    <option value="Application Referred">App - Referred</option>
                                    <option value="Interview Declined">Interview - Declined</option>
                                    <option value="Interview Referred">Interview - Referred</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="section" id="panel-interview-row">
                        <div class="row">
                            <div class="col l6 m6 s12">
                                <select id="panelist-input" multiple="multiple">
                                    <option value="" disabled selected>Panelists?</option>
                                    <option value="grant@propellaincubator.co.za">Grant Minnie</option>
                                    <option value="anita@propellaincubator.co.za">Anita Palmer</option>
                                    <option value="woosthuizen@engeli.co.za">Wayne Oosthuizen</option>
                                    <option value="bwiseman@engeli.co.za">Barry Wiseman</option>
                                    <option value="rdames@engeli.co.za">Ricardo Dames</option>
                                    <option value="errol@propellaincubator.co.za">Error Wills</option>
                                    <option value="linda1@propellaincubator.co.za">Linda Lawrie</option>
                                    <option value="daryl.mcwilliams@gmail.com">Daryl McWilliams</option>
                                    <option value="Nqobile.Gumede@mandela.ac.za">Nqobile Gumede</option>
                                    <option value="Mante.Kgaria@mandela.ac.za">Mante Kgaria</option>
                                </select>

                                <input type="date" id="panel-selection-date-input">

                                <label for="panel-selection-time-input">Time of panel interview</label>
                                <input type="time" id="panel-selection-time-input">
                            </div>
                            <div class="col l6 m6 s12">
                                <div class="row">
                                    <select id="question-category-input">
                                        <option value="" disabled selected>Choose Questions</option>
                                        @foreach($question_categories as $question_category)
                                            <option
                                                value="{{$question_category->id}}">{{$question_category->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button class="btn blue" id="show-guest-panelist-button">Add Guest Panelist
                                </button>
                                <div class="container" id="guest-panelist-container" hidden>
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <select id="title-input">
                                                <option value="" disabled selected>Choose Title</option>
                                                <option value="Mr">Mr</option>
                                                <option value="Mrs">Mrs</option>
                                                <option value="Miss">Miss</option>
                                                <option value="Ms">Ms</option>
                                                <option value="Dr">Dr</option>
                                            </select>
                                            <label>Title</label>
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <input id="initial-input" name="initial-input" type="text"
                                                   class="validate">
                                            <label for="initial-input">Initials</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <input id="name-input" name="name-input" type="text"
                                                   class="validate">
                                            <label for="name-input">First Name</label>
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <input id="surname-input" name="surname-input" type="text"
                                                   class="validate">
                                            <label for="surname-input">Last Name</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <input id="company-input" name="company-input" type="text"
                                                   class="validate">
                                            <label for="company-input">Company Name</label>
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <input id="position-input" name="position-input" type="text"
                                                   class="validate">
                                            <label for="position-input">Position</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col l6 m6 s12">
                                            <input id="email-input" name="email-input" type="email"
                                                   class="validate">
                                            <label for="email-input">Email</label>
                                        </div>
                                        <div class="col l6 m6 s12">
                                            <input id="contact-number-input" name="contact-number-input"
                                                   type="text"
                                                   class="validate">
                                            <label for="contact-number-input">Contact Number</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button class="btn blue" id="add-guest-panelist-button">Add</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <button class="btn blue" id="panel-interview-button">Finalize</button>
                        </div>
                    </div>
                @endif

            </div>
        </div>

        {{--PANEL INTERVIEW OVERVIEW SECTION--}}
        <div class="row" id="panel-interview-overview-section" hidden
             style="margin-right: 3em; margin-left: 3em;">
            <div class="row center">
                <h4><b>PANEL RESULTS</b></h4>
            </div>
            <div class="section" style="margin-top: 1em;">
                <input id="applicant-id-input" value="{{$applicant->id}}" hidden disabled>
                <div class="row center">
                    <div class="row center" style="background-color: #123c24;">
                        <h5 style="color: white;">Panelists scores <i class="material-icons">arrow_downward</i>
                        </h5>
                    </div>

                    <div class="row" style="margin-left: 3em; margin-right: 3em;">
                        @foreach($final_panelist_question_score_array as $panelist)
                            <div class="col l3 m6 s12 center">
                                <ul class="collapsible">
                                    <li>
                                        <div class="collapsible-header">
                                            <h5>{{$panelist->name}} {{$panelist->surname}}'s score</h5></div>
                                        <div class="collapsible-body">
                        <span>
                            @if(count($panelist->user_questions_scores) > 0)
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Question Number</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>Question Score</h6>
                                </div>
                            </div>
                                @if(isset($panelist->user_questions_scores))
                                    @foreach($panelist->user_questions_scores as $question_answer)
                                        <div class="row">
                                        <div class="col l6 m6 s12" style="background-color: darkgray;">
                                            <p>{{$question_answer->question_number}}</p>
                                        </div>
                                        <div class="col l6 m6 s12" style="background-color: dimgray;">
                                            <p>{{$question_answer->question_score}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Total Score</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>{{isset($panelist->panelist_applicant_score) ? $panelist->panelist_applicant_score : 0}}</h6>
                                </div>
                            </div>
                            @else
                                <div class="row center">
                                    <h6>The panelist has not completed the score sheet.</h6>
                                </div>
                            @endif
                        </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="row center">
                    <h5>Total combined score: <b>{{$total_panelist_score}}</b></h5>
                    <h5>Total average score: <b>{{$average_panelist_score}}</b></h5>
                    <button class="btn green" id="{{$applicant->id}}" onclick="downloadExcel(this)">Excel
                    </button>
                </div>
            </div>

            <div class="section" style="margin-bottom: 3em; margin-top: 3em;">
                <div class="row center">
                    <div class="row center" style="background-color: #1a237e;">
                        <h5 style="color: white;"> Combined scores per question <i class="material-icons">arrow_downward</i>
                        </h5>
                    </div>

                    <div class="row" style="margin-right: 3em; margin-left: 3em;">
                        <table id="t01">
                            <tr>
                                <th>Question Number</th>
                                <th>Total Score</th>
                            </tr>
                            @foreach($question_scores_array as $question_score)
                                <tr>
                                    <td>{{$question_score->question_number}}</td>
                                    <td>{{$question_score->question_score}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>

            <div class="section">
                <input hidden disabled id="applicant-id-input" value="{{$applicant->id}}">
                <div class="row center" style="background-color: #323232">
                    <h5 style="color: white;"> Want to see more details? <i
                            class="material-icons">arrow_downward</i></h5>
                </div>
                <div class="row" style="margin-left: 2em;margin-right: 2em;">
                    <div class="col s12">
                        <table class="table table-bordered" style="width: 100%!important;"
                               id="applicant-panelists-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Selection Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{--GO / NO GO INCUBATEE--}}
        <div class="row" id="go-no-go-incubatee-section" hidden style="margin-right: 3em; margin-left: 3em;">
            <div class="section">
                <div class="row center">
                    <h4><b>GO / NO GO INCUBATEE</b></h4>
                </div>
                <input hidden disabled id="user-id-input" value="{{$user->id}}">
                <div class="row center">
                    <div class="col l6 m6 s12">
                        <i class="material-icons large" style="color: saddlebrown;"
                           onclick="approveInterviewPanel()">thumb_up</i>
                        <h3>Go?</h3>
                    </div>
                    <div class="col l6 m6 s12">
                        <i class="material-icons large" style="color: saddlebrown;"
                           onclick="declineInterviewPanel()">thumb_down</i>
                        <h3>No Go?</h3>
                    </div>
                </div>

                <div class="row center" hidden id="go-row-panel-decision-decline">
                    <div class="row center">
                        <h4><b>GO INCUBATEE</b></h4>
                    </div>
                    <div class="col s12 m6 l6" style="width:50%;">
                        <select id="venture_category">
                            <option value="" disabled selected>Choose Venture category</option>
                            @foreach($venture_categories as $v_category)
                                <option value="{{$v_category->id}}">{{$v_category->category_name}}</option>
                            @endforeach
                        </select>

                        <select id="events" multiple="multiple">
                            <option value="" disabled selected>Assign to event</option>
                            @foreach($events as $event)
                                <option value="{{$event->id}}">{{$event->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col s12 m6 l6" style="width:50%;">
                        <select id="incubatee_stage">
                            <option value="" disabled selected>Choose Venture stage</option>
                            @foreach($incubatee_stages as $i_stage)
                                <option value="{{$i_stage->id}}">{{$i_stage->stage_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn blue" id="submit-chosen-category-button">submit</button>
                </div>

                <div class="row center" hidden id="no-go-panel-decision-row">
                    <div class="row center">
                        <h4><b>NO GO INCUBATEE</b></h4>
                    </div>
                    <div class="row center">
                        <div class="col l6 m6 s12">
                            <select id="panel-contact-input">
                                <option value="" disabled selected>How did you
                                    contact {{$user->name}} {{$user->surname}}?
                                </option>
                                <option value="Phone Call">Phone Call</option>
                                <option value="Face-to-Face">Face-to-Face</option>
                                <option value="Email">Email</option>
                                <option value="No response to any contact type">No response to any contact type
                                </option>
                                <option value="Invited, but did not arrive">Invited, but did not arrive</option>

                            </select>
                        </div>
                        <div class="col l6 m6 s12">
                            <select id="panel-contact-result-input">
                                <option value="" disabled selected>What was the result?</option>
                                <option value="Panel decision declined">Panel decision declined</option>
                                <option value="Panel decision referred">Panel decision referred</option>
                                <option value="App - declined">App - declined</option>
                                <option value="App - reffered">App - reffered</option>
                            </select>
                        </div>
                    </div>
                    <!--Declined-->
                    <div class="col l6 m6 s12" id="panel-decision-declined" hidden>
                        <label for="panel-decline-reason-input">Reason panel interview is declined?</label>
                        <textarea id="panel-decline-but-referred-reason-input"></textarea>
                        <br>
                        <label for="declined-applicant-categories-input">Select category</label>
                        <select id="panel-declined-applicant-categories-input">
                            <option value="" disabled selected>Select applicant category</option>
                            @foreach($declined_applicant_categories as $d_category)
                                <option value="{{$d_category->id}}">{{$d_category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col l6 m6 s12" id="panel-declined-button" hidden>
                        <label for="panel-referred-company-input">Who it was referred to?</label>
                        <textarea id="panel-referred-company-input"></textarea>
                        <div class="input-field col m8">
                            <select id="panel-declined_reason_two">
                                <option value="" disabled selected>Choose decline reason</option>
                                <option
                                    value="Unfortunately your application was unsuccessful, we are recruiting from the Nelson Mandela Bay area only">
                                    Unfortunately your application was unsuccessful, we are recruiting from the
                                    Nelson
                                    Mandela Bay area only
                                </option>
                                <option
                                    value="Unfortunately your application was unsuccessful,	your idea does not fit the business sector that we incubate">
                                    Unfortunately your application was unsuccessful, your idea does not fit the
                                    business
                                    sector that we incubate
                                </option>
                                <option
                                    value="Unfortunately your application was unsuccessful ,your application does not meet our funder’s criteria">
                                    Unfortunately your application was unsuccessful, your application does not meet
                                    our
                                    funder’s criteria
                                </option>
                                <option
                                    value="Unfortunately your application was unsuccessful, your application was incomplete">
                                    Unfortunately your application was unsuccessful, your application was incomplete
                                </option>
                                <option value="Unfortunately your application was unsuccessful, you did not attend the bootcamp">Unfortunately your application was unsuccessful, you did not attend the bootcamp</option>
                            </select>
                            <label>Choose Declined reason</label>
                        </div>
                        <button class="btn blue" id="panel-interview-declined">Submit panel declined</button>
                    </div>

                    <!--Referred-->
                    <div class="col l6 m6 s12" id="panel-referred-applicants" hidden>
                        <label for="panel-referred-reason-input">Reason panel interview is referred?</label>
                        <textarea id="panel-referred-reason-input"></textarea>
                        <br>
                        <label for="panel-referred-applicant-categories-input">Select category</label>
                        <select id="referred_applicant_category_id">
                            <option value="" disabled selected>Select referred category</option>
                            @foreach($referred_applicant_categories as $r_category)
                                <option value="{{$r_category->id}}">{{$r_category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col l6 m6 s12" id="panel-referred-button-section" hidden>
                        <label for="panel-referred-company-input">Who it was referred to?</label>
                        <textarea id="panel-referred-company-input"></textarea>
                        <button class="btn blue" id="panel-interview-referred-button-section">Submit Panel Referred</button>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>
        function downloadExcel(obj){
            let applicant_id = obj.id;
            window.location.href = '/extract-applicant-panelists-data/' + applicant_id;
        }
        $(document).ready(function () {
            //CONTACT LOG BUTTON TOGLE
            $('#applicant_contact_results').on('change', function(){
                if(this.value === "Email"){
                    $('.save-contact-log').hide();
                    $('.send-email-to-applicant').show();
                    $('#comment-section').show();
                    $('#comment-email-section').show();
                }else{
                    $('.save-contact-log').show();
                    $('.send-email-to-applicant').hide();
                    $('#comment-email-section').hide();
                }
            });

            $(function () {
                let applicant_id = $('#applicant-id-input').val();
                $('#applicant-panelists-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '/get-applicant-panelists/' + applicant_id,
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'selection_date', name: 'selection_date'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="applicant-panelists-table_length"]').css("display","inline");
            });

            $('.status-color:contains("Closed")').css('color', 'red');
            $('.status-color:contains("Pending")').css('color', 'green');
            $('.status-color:contains("Reviewed")').css('color', 'orange');

            $('.modal').modal();
            $('.logCard').on('click', function () {
                $('#logCardShow').show();
            });
            $('.viewLogs').on('click', function () {
                $('#viewAllLogs').show();
            });

            //Applicant contact log
            $('#contact-log-submit-button').on('click',function () {
                let user_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('applicant_contact_results', $('#applicant_contact_results').val());
                formData.append('status', $('#status').val());
                formData.append('chosen_category', $('#chosen_category').val());

                let url = '/store-contact-log/' + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#contact-log-submit-button').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/applicant-show/' + user_id;
                        }, 3000);
                    },
                });
            });

            //Send email to applicants
            $('#send-email').on('click',function () {
                let user_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('email_comment', $('#email_comment').val());
                formData.append('comment', $('#comment').val());
                formData.append('applicant_contact_results', $('#applicant_contact_results').val());
                formData.append('status', $('#status').val());
                formData.append('chosen_category', $('#chosen_category').val());

                let url = '/store-contact-log/' + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#applicant_contact_results').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/applicant-show/' + user_id;
                        }, 3000);
                    },
                });
            });

            //Status save
            $('#status').on('change',function () {
                let user_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('status', $('#status').val());

                let url = '/update-applicant-status/' + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#status').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/applicant-show/' + user_id;
                        }, 3000);
                    },
                });
            });

            //Chosen category update
            $('#chosen_category').on('change',function () {
                let user_id = $('#user-id-input').val();

                let formData = new FormData();
                formData.append('chosen_category', $('#chosen_category').val());

                let url = '/update-applicant-chosen-category/' + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $('#chosen_category').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/applicant-show/' + user_id;
                        }, 3000);
                    },
                });
            });

            $('select').formSelect();
            $('#contact-result-input').on('change', function(){
                if($('#contact-result-input').val() === "Application Declined"){
                    $('#declined-applicants').show();
                    $('#declined-button').show();
                    $('#referred-applicants').hide();
                    $('#referred-button').hide();
                }else if($('#contact-result-input').val() === "Application Referred"){
                    $('#referred-applicants').show();
                    $('#referred-button').show();
                    $('#declined-applicants').hide();
                    $('#declined-button').hide();
                }
            });

            //PANEL CONTACT RESULT INPUT
            $('#panel-contact-result-input').on('change', function(){
                if($('#panel-contact-result-input').val() === "Panel decision declined"){
                    $('#panel-decision-declined').show();
                    $('#panel-declined-button').show();
                    $('#panel-referred-applicants').hide();
                    $('#panel-referred-button-section').hide();
                }else if($('#panel-contact-result-input').val() === "Panel decision referred"){
                    $('#panel-referred-applicants').show();
                    $('#panel-referred-button-section').show();
                    $('#panel-decision-declined').hide();
                    $('#panel-declined-button').hide();
                }

            });
        });

        function approveApplicant() {
            $('#go-row').show();
            $('#no-go-row').hide();
        }

        function declineApplicant() {
            $('#go-row').hide();
            $('#no-go-row').show();
        }

        //GO / NO GO PANEL INTERVIEW
        function approveInterviewPanel(){
            $('#go-row-panel-decision-decline').show();
            $('#no-go-panel-decision-row').hide();
        }
        function declineInterviewPanel(){
            $('#no-go-panel-decision-row').show();
            $('#go-row-panel-decision-decline').hide();
        }



        $('#submit-chosen-category-button').on('click', function () {
            let category_id = $('#venture_category').val();
            let user_id = $('#user-id-input').val();
            let stage_id = $('#incubatee_stage').val();
            let selected_events = [];
            let r = confirm("Are you sure you want to give this applicant incubatee permissions?");
            if (r === true) {
                jQuery.each(jQuery('#events').val(), function (i, value) {
                    selected_events.push(value);
                });
                $('#submit-chosen-category-button').text("loading...");
                let formData = new FormData();
                formData.append('category_id', category_id);
                formData.append('stage_id', stage_id);
                formData.append('events', JSON.stringify(selected_events));

                let url = '/set-applicant-to-incubatee/' + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#submit-chosen-category-button").notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/applicants';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;alert(message);
                        window.location.reload();
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        $('#panel-interview-declined-but-referred-button').on('click', function () {
            let r = confirm("Are you sure you want to decline this applicant?");

            if (r === true) {
                $('#panel-interview-declined-but-referred-button').text("Loading...");
                let formData = new FormData();
                formData.append('contacted_via', $('#contact-input').val());
                formData.append('result_of_contact', $('#contact-result-input').val());
                formData.append('declined_reason_text', $('#decline-but-referred-reason-input').val());
                formData.append('referred_company', $('#referred-company-input').val());
                formData.append('declined_applicant_category_id', $('#declined-applicant-categories-input').val());
                formData.append('declined_reason_two', $('#declined_reason_two').val());


                let user_id = $('#user-id-input').val();
                let url = "/applicant-panel-interview-decline/" + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#panel-interview-declined-but-referred-button').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/applicants';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#decline-but-referred-reason-input').notify(response.message, "error");
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        //PANEL INTERVIEW DECLINED FOR GO / NO GO INCUBATEE
        $('#panel-interview-declined').on('click', function () {
            let r = confirm("Panel decision declined, Are you sure you want to decline this applicant?");

            if (r === true) {
                $('#panel-interview-declined').text("Loading...");
                let formData = new FormData();
                formData.append('contacted_via', $('#panel-contact-input').val());
                formData.append('result_of_contact', $('#panel-contact-result-input').val());
                formData.append('declined_reason_text', $('#panel-decline-but-referred-reason-input').val());
                formData.append('referred_company', $('#panel-referred-company-input').val());
                formData.append('declined_applicant_category_id', $('#panel-declined-applicant-categories-input').val());
                formData.append('declined_reason_two', $('#panel-declined_reason_two').val());


                let user_id = $('#user-id-input').val();
                let url = "/applicant-panel-interview-decline/" + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#panel-interview-declined').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/applicants';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#decline-but-referred-reason-input').notify(response.message, "error");
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        //REFERRED APPLICANTS
        $('#panel-interview-referred-button').on('click', function () {
            let r = confirm("Are you sure you want to Refer this applicant?");

            if (r === true) {
                $('#panel-interview-referred-button').text("Loading...");
                let formData = new FormData();
                formData.append('contacted_via', $('#contact-input').val());
                formData.append('result_of_contact', $('#contact-result-input').val());
                formData.append('referred_reason', $('#referred-reason-input').val());
                formData.append('referred_company', $('#referred-company-input').val());
                formData.append('referred_applicant_category_id', $('#referred-applicant-categories-input').val());

                let user_id = $('#user-id-input').val();
                let url = "/applicant-panel-interview-referred/" + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#panel-interview-referred-button').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/applicants';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#referred-reason-input').notify(response.message, "error");
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        //PANEL INTERVIEW REFERRED APPLICANTS
        $('#panel-interview-referred-button-section').on('click', function(){
            let r = confirm("Panel decision referred,Are you sure you want to Refer this applicant?");

            if(r === true){
                $('#panel-interview-referred-button-section').text("Loading...");
                let formData = new FormData();
                formData.append('contacted_via', $('#panel-contact-input').val());
                formData.append('result_of_contact', $('#panel-contact-result-input').val());
                formData.append('referred_reason', $('#panel-referred-reason-input').val());
                formData.append('referred_company', $('#panel-referred-company-input').val());
                formData.append('referred_applicant_category_id', $('#referred_applicant_category_id').val());

                let user_id = $('#user-id-input').val();
                let url = "/applicant-panel-interview-referred/" + user_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#panel-interview-referred-button-section').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/applicants';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#referred-reason-input').notify(response.message, "error");
                    }
                });
            } else {
                alert("Action cancelled.");
            }
        });

        ////////
        let user_id = $('#user-id-input').val();
        $('#contact-result-input').on('change', function () {
            if ($('#contact-result-input').val() === "Panel Interview") {
                $('#panel-interview-row').show();
                $('#panel-interview-declined-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if ($('#contact-result-input').val() === "Interview Declined" || $('#contact-result-input').val() === "Application Declined") {
                $('#panel-interview-declined-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if ($('#contact-result-input').val() === "Interview Referred" || $('#contact-result-input').val() === "Application Referred") {
                $('#panel-interview-declined-but-referred-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-row').hide();
            }
        });

        $('#show-guest-panelist-button').on('click', function () {
            $('#show-guest-panelist-button').hide();
            $('#guest-panelist-container').show();
        });

        //Submit Panel Interview Buttons

        //Approve Applicant
        let guest_panelists = [];

        $('#add-guest-panelist-button').on('click', function () {
            let title = $('#title-input').val();
            let initials = $('#initial-input').val();
            let name = $('#name-input').val();
            let surname = $('#surname-input').val();
            let position = $('#position-input').val();
            let company = $('#company-input').val();
            let email = $('#email-input').val();
            let contact_number = $('#contact-number-input').val();

            let object = {
                title: title,
                initials: initials,
                name: name,
                surname: surname,
                position: position,
                company: company,
                email: email,
                contact_number: contact_number
            };

            guest_panelists.push(object);

            $('#title-input').val("");
            $('#initial-input').val("");
            $('#name-input').val("");
            $('#surname-input').val("");
            $('#position-input').val("");
            $('#company-input').val("");
            $('#email-input').val("");
            $('#contact-number-input').val("");
        });

        $('#panel-interview-button').on('click', function () {
            let user_id = $('#user-id-input').val();
            let selected_panelists = [];
            let formData = new FormData();

            jQuery.each(jQuery('#panelist-input').val(), function (i, value) {
                selected_panelists.push(value);
            });

            formData.append('contacted_via', $('#contact-input-panel').val());
            formData.append('result_of_contact', $('#contact-result-input-panel').val());
            formData.append('selected_panelists', JSON.stringify(selected_panelists));
            formData.append('guest_panelists', JSON.stringify(guest_panelists));
            formData.append('panel_selection_date', $('#panel-selection-date-input').val());
            formData.append('panel_selection_time', $('#panel-selection-time-input').val());
            formData.append('question_category_id', $('#question-category-input').val());

            let url = "/applicant-panel-interview-approve/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#panel-interview-button').notify(response.message, "success");

                    setTimeout(function () {
                        window.location.href = '/applicant-show/' + user_id;
                    }, 3000);
                },

                error: function (response) {
                    $('#panel-interview-button').notify(response.message, "error");
                }
            });
        });

        //Decline Applicant
        $('#panel-interview-declined-button').on('click', function () {
            let formData = new FormData();
            formData.append('contacted_via', $('#contact-input').val());
            formData.append('result_of_contact', $('#contact-result-input').val());
            formData.append('declined_reason_text', $('#decline-reason-input').val());
            formData.append('declined_applicant_category_id', $('#declined-applicant-categories-input').val());

            let url = "/applicant-panel-interview-decline/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#panel-interview-declined-button').notify(response.message, "success");

                    setTimeout(function () {
                        window.location.href = '/applicants';
                    }, 3000);
                },

                error: function (response) {
                    $('#panel-interview-declined-button').notify(response.message, "error");
                }
            });
        });

        //Decline but refer
        $('#panel-interview-declined-but-referred-button').on('click', function () {
            let formData = new FormData();
            formData.append('contacted_via', $('#contact-input').val());
            formData.append('result_of_contact', $('#contact-result-input').val());
            formData.append('declined_reason_text', $('#decline-but-referred-reason-input').val());
            formData.append('referred_company', $('#referred-company-input').val());
            formData.append('declined_applicant_category_id', $('#declined-applicant-categories-input').val());

            let url = "/applicant-panel-interview-decline-but-refer/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#panel-interview-declined-but-referred-button').notify(response.message, "success");

                    setTimeout(function () {
                        window.location.href = '/applicants';
                    }, 3000);
                },

                error: function (response) {
                    $('#panel-interview-declined-but-referred-button').notify(response.message, "error");
                }
            });
        });

        //Update panel interview details
        $('#update-panel-interview-details-button').on('click', function () {
            let formData = new FormData();
            let date = $('#panel-selection-date-display').val();
            let time = $('#panel-selection-time-display').val();
            let contact = $('#contact-input-display').val();
            let contact_result = $('#contact-result-input-display').val();
            let question_category_id = $('#question-category-display').val();

            formData.append('date', date);
            formData.append('time', time);
            formData.append('contact', contact);
            formData.append('contact_result', contact_result);
            formData.append('question_category_id', question_category_id);

            let url = "/admin-update-panel-interview-details/" + user_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},


                success: function (response, a, b) {
                    $('#update-panel-interview-details-button').notify(response.message, "success");

                    setTimeout(function () {
                        window.location.href = '/applicant-show/' + user_id;
                    }, 3000);
                },
                error: function (response) {
                    $('#update-panel-interview-details-button').notify(response.message, "error");
                }
            });

        });

        function deletePanelist(obj) {
            var r = confirm("Are you sure want to remove this panelist?");
            if (r === true) {
                $.get('/admin-remove-panelist-from-applicant/' + obj.id, function (data, status) {
                    console.log('Data', data);
                    if (status === 'success') {
                        $("#panelist-header").notify(data.message, "success");
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }
                });
            } else {
                alert('Delete action cancelled');
            }
        }

        function appendPanelist() {
            let email = $('#extra-panelist-email-input').val();
            let user_id = $('#user-id-input').val();
            let url = '/admin-add-industrial-panelist-from-panel-access-window';
            let question_category_id = $('#question-category-id').val();

            let formData = new FormData();
            formData.append('email', email);
            formData.append('user_id', user_id);
            formData.append('panel_selection_date', $('#panel-selection-date-display').val());
            formData.append('panel_selection_time', $('#panel-selection-time-display').val());
            formData.append('question_category_id', question_category_id);

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#add-panelist-header').notify(response.message + ". Page refresshing in 3 seconds.", "success");

                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    $('#add-panelist-header').notify(response.message, "error");
                }
            });
        }

        $(document).ready(function () {

            $('.status:contains("Closed")').css('color', 'red');
            $('.status:contains("Pending")').css('color', 'green');

            $('.modal').modal();
            $('.logCard').on('click', function () {
                $('#logCardShow').show();
            });
            $('.viewLogs').on('click', function () {
                $('#viewAllLogs').show();
            });


            $('select').formSelect();


        });

        $('#application').on('click', function () {
            $('#application-details-row').show();
            $('#applicant-contact-log').hide();
            $('#applicant-panel-interview').hide();
            $('#panel-interview-overview-section').hide();
            $('#go-no-go-incubatee-section').hide();
            $('#go-no-go-panel-section').hide();
        });
        $('#contact-log').on('click', function () {
            $('#applicant-contact-log').show();
            $('#application-details-row').hide();
            $('#pplicant-panel-interview').hide();
            $('#panel-interview-overview-section').hide();
            $('#go-no-go-incubatee-section').hide();
            $('#go-no-go-panel-section').hide();
        });

        $('#panel-interview').on('click', function () {
            $('#applicant-panel-interview').show();
            $('#application-details-row').hide();
            $('#applicant-contact-log').hide();
            $('#panel-interview-overview-section').hide();
            $('#go-no-go-incubatee-section').hide();
            $('#go-no-go-panel-section').hide();
        });

        $('#panel-interview-overview').on('click', function () {
            $('#panel-interview-overview-section').show();
            $('#application-details-row').hide();
            $('#applicant-contact-log').hide();
            $('#applicant-panel-interview').hide();
            $('#go-no-go-incubatee-section').hide();
            $('#go-no-go-panel-section').hide();
        });

        $('#go-no-go-incubatee').on('click', function () {
            $('#go-no-go-incubatee-section').show();
            $('#panel-interview-overview-section').hide();
            $('#application-details-row').hide();
            $('#applicant-contact-log').hide();
            $('#applicant-panel-interview').hide();
            $('#go-no-go-panel-section').hide();
        });

        $('#go-no-go-panel').on('click', function () {
            $('#go-no-go-panel-section').show();
            $('#go-no-go-incubatee-section').hide();
            $('#panel-interview-overview-section').hide();
            $('#application-details-row').hide();
            $('#applicant-contact-log').hide();
            $('#applicant-panel-interview').hide();
        });


        function printContent(el) {
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }


    </script>


@endsection
