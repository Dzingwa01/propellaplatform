@extends('layouts.administrator-layout')


@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('applicant-index')}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Applicants</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="users-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Category</th>
                        <th>Date/Time Applied</th>
                        <th>Status</th>
                        <th>Form</th>
                        <th>POPI</th>
                        <tH>Data Protected</tH>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#users-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    stateSave: true,
                    pageLength: 100,
                    ajax: '{{route('get-applicants')}}',
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'phone', name: 'phone'},
                        {data:'category',name:'category'},
                        {data:'date_time_applied', name: 'date_time_applied'},
                        {data: 'status', name: 'status'},
                        {data: 'completed_form', name: 'completed_form'},
                        {data: 'popi_act_agreement', name: 'popi_act_agreement'},
                        {data: 'data_protected', name: 'data_protected'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="users-table_length"]').css("display","inline");
            });

        });

        function confirm_delete(obj){
            var r = confirm("Are you sure want to delete this application!");
            if (r == true) {
                $.get('/applicant-delete/'+obj.id,function(data,status){
                    console.log('Data',data);
                    console.log('Status',status);
                    if(status=='success'){
                        alert(data.message);
                        window.location.reload();
                    }

                });
            } else {
                alert('Delete action cancelled');
            }
        }

    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection

