@extends('layouts.administrator-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <form class="col s12 " id="edit-category" method="post">
            <br>
            @csrf
            <input value="{{$referredApplicantCategory->id}}" id="question_category_id" hidden>
            <br>
            <h4 class="center-align">Edit Declined Applicant Category Name</h4>
            <div class="input-field col m4">
                <input id="category_name" value="{{$referredApplicantCategory->category_name}}" type="text" class="validate">
                <label for="category_name">Category name</label>
            </div>
            <div class="modal-footer">
                <a class="btn-flat" id="referred-applicant-category-name-button">Submit</a>
            </div>
        </form>
    </div>
    <script>
        $(document).ready(function () {
            $('#referred-applicant-category-name-button').on('click', function(){
                $(this).text("Loading...");
                let url = '/update-referred-applicant-category/'+ '{{$referredApplicantCategory->id}}';
                let formData = new FormData();
                formData.append('category_name', $('#category_name').val());
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#category_name").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                    }
                });
            });

        });

    </script>
@endsection

