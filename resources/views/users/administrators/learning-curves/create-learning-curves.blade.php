@extends('layouts.administrator-layout')

@section('content')
    <head>
        <meta charset="UTF-8">
        <title>bootstrap4</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
    </head>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head> <br><br>
    <br>    <ul class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li><a href="/get-learning-curves">Learning Curves</a></li>
        <li><a href="/create-learning-curves">Add Learning Curve</a></li>
    </ul>
    <body>

    <!--Create Learning curves-->
    <div class="" style="margin-top: 10vh">
        <div class="card" style="width: 1070px;margin: 0 auto">
            <br>
            <h4 style="margin-left: 350px">Create learning curves</h4>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <div class="input-field col m6">
                    <input id="title" type="text" class="validate">
                    <label for="title">Learning title</label>
                </div>
                <div class="input-field col m6">
                    <input id="description" type="text" class="validate">
                    <label for="description">Learning description</label>
                </div>
            </div>
            <div class="row" style="margin-right: 2em;margin-left: 2em;">
                <div class="file-field input-field col m6">
                    <div class="btn" style="height: 50px;">
                        <span>Learning  Image</span>
                        <input type="file" id="learning_image_url">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 800px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn section" id="learning-upload-submit-button">Save</a>
                </div>
            </div>

            <br>
        </div>
    </div>

    <!--Create Learning curve summernote-->
    <div class="container">
        <div class="panel-body">
            <form action="{{route('store-learning-curve-summernote')}}" method="POST" style="margin-top: 50px" id="summernote-form">
                <div class="form-group">
                    <textarea id="summernote" name="summernoteInput" class="summernote"></textarea>
                </div>

                <div class="form-group">
                    <button type="submit">Submit</button>
                    {!!csrf_field()!!}

                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#learning-upload-submit-button').on('click', function () {
                let title = $('#title').val();
                let description = $('#description').val();

                if (title === "" || description === "") {
                    alert("Please insert a title and description!");
                } else {
                    let formData = new FormData();
                    formData.append('title', title);
                    formData.append('description', description);

                    jQuery.each(jQuery('#learning_image_url')[0].files, function (i, file) {
                        formData.append('learning_image_url', file);
                    });

                    let url = "{{route('store-learning-curve')}}";
                    $.ajax({
                        url: url,
                        data: formData,
                        type: 'post',
                        processData: false,
                        contentType: false,
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            alert(response.message);
                            // blog_id = response.blog.id;-
                            $('.second-row').show();

                            $('#summernote-form').append(
                                '<input name="learning_curve_id" value="'+response.learning_curve_id+'" hidden >'
                            );
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                }
            });
        });

        $('#summernote').summernote({
            placeholder: 'Content here ..',
            height: 700,
        });
    </script>
    </body>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

    </head>



    <head>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

        <!-- include summernote css/js-->
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    </head>
    <style>
        ul.breadcrumb {
            padding: 10px 16px;
            list-style: none;
            background-color:grey;
        }
        ul.breadcrumb li {
            display: inline;
            font-size: 18px;
        }
        ul.breadcrumb li+li:before {
            padding: 8px;
            color: white;
            content: ">\00a0";
        }
        ul.breadcrumb li a {
            color: black;
            text-decoration: none;
        }
    </style>
    @endsection
