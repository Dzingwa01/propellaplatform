@extends('layouts.administrator-layout')

@section('content')
    <br>
    <div class="container-fluid">
        <input hidden disabled value="{{$user_mentor->id}}" id="mentor-id-input">
        <div class="row center" style="margin-left: 2em;margin-right: 2em; margin-top: 2em;">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;">Mentors Info</h6>
            <table>
                <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Surname</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <tr><td>{{$user->name}}</td>
                    <td> {{$user->surname}}</td>
                    <td><a href="{{"mailto:".$user->email}}">{{$user->email}}</a></td>
                </tbody>
            </table>
        </div>
    </div>
    <br>

    <div class="row center">
        <div class="col l6 m6 s12">
            <div class="card">
                <h6 style="text-transform:uppercase;"><b>Assign Venture(s) to Mentor</b></h6>
                <br>
                <div class="row center">
                    <div class="col l12 m12 s12">
                        <label for="assign-venture-to-mentor-input">Choose venture(s)</label>
                        <select id="ventures-to-assign-input" multiple="multiple">
                            <option>Select Venture</option>
                            @foreach($venture as $ventures)
                                @if($ventures->mentor_id == $user_mentor->id)
                                    <option value="{{$ventures->id}}">{{$ventures->company_name}} - Already Assigned</option>
                                @else
                                    <option value="{{$ventures->id}}">{{$ventures->company_name}}</option>
                                @endif
                            @endforeach
                        </select>
                        <div class="row">
                            <button class="btn teal" id="assign-venture-to-mentor-button">Assign</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col l6 m6 s12">
            <div class="card">
                <h6 style="text-transform:uppercase;"><b>Set up Shadow Board</b></h6>
                <br>
                <div class="row" id="incubatee-row">
                    <div class="input-field col l4 m4 s12">
                        <select id="venture-input" multiple="multiple">
                            <option>Select Venture</option>
                            @foreach($venture as $ventures)
                                @if($ventures->mentor_id == $user_mentor->id)
                                    <option value="{{$ventures->id}}">{{$ventures->company_name}}</option>
                                @endif
                            @endforeach
                        </select>
                        <label>Choose Venture(s)</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                        <input type="date" id="shadow_board_date">
                        <label for="shadow_board_date">Date of shadow board</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                        <input type="time" id="shadow_board_time">
                        <label for="shadow_board_time">Time of shadow board</label>
                    </div>
                </div>
                <div class="row">
                    <button class="btn teal" id="assign-shadow-board-button">Submit</button>
                </div>
                <br>
            </div>
        </div>
    </div>



    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;">Mentor Ventures</h6>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="venture-table">
                    <thead>
                    <tr>
                        <th>Venture Name</th>
                        <th>Contact Number</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.modal').modal();

            $('select').formSelect();

            $(function () {
                let mentor_id = $('#mentor-id-input').val();
                let url = '/get-venture-assigned-to-mentor/' + mentor_id;
                $('#venture-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    type: 'get',
                    scrollX: 640,
                    ajax: url,
                    columns: [
                        {data: 'company_name', name: 'company_name'},
                        {data: 'contact_number', name: 'contact_number'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},

                    ]
                });
                $('select[name="venture-table_length"]').css("display", "inline");
            });

            $('#mentor-feedback').on('click', function(){
                let mentor_id = $('#mentor-id-input').val();
                location.href='/mentor-feedback/'+mentor_id ;
            });


            $('#assign-venture-to-mentor-button').on('click', function(){
                $(this).text("busy...");
                let selected_ventures = [];
                let formData = new FormData();

                jQuery.each(jQuery('#ventures-to-assign-input').val(), function (i, value) {
                    selected_ventures.push(value);
                });

                formData.append('selected_ventures', JSON.stringify(selected_ventures));
                formData.append('mentor_id', $('#mentor-id-input').val());

                let url = "/assign-venture-to-mentor";

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#assign-venture-to-mentor-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        $('#incubatee-button').notify(response.message, "error");
                    }
                });
            });


            $('#assign-shadow-board-button').on('click', function(){
                $(this).text("busy...");
                let selected_ventures = [];
                let formData = new FormData();

                jQuery.each(jQuery('#venture-input').val(), function (i, value) {
                    selected_ventures.push(value);
                });

                formData.append('shadow_board_date', $('#shadow_board_date').val());
                formData.append('shadow_board_time', $('#shadow_board_time').val());
                formData.append('selected_ventures', JSON.stringify(selected_ventures));
                formData.append('mentor_id', $('#mentor-id-input').val());
                 let url = "/assign-shadow-boards";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#assign-shadow-board-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        $('#incubatee-button').notify(response.message, "error");
                    }
                });
            });
        });
    </script>

    @endsection
