@extends('layouts.administrator-layout')

@section('content')
    <input id="company-employee-id-input" disabled hidden value="{{$companyEmployee->id}}">

    <input id="company-id-input" disabled hidden value="{{$company->company_id}}">
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-edit-employee',$company->company_id,$companyEmployee)}}
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Administrator Edit Contact</h6>
            {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <div class="row">
                    <form class="col s12">
                        @csrf
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="name" type="text" value="{{$companyEmployee->name}}" class="validate">
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="surname" type="text" value="{{$companyEmployee->surname}}" class="validate">
                                <label for="surname">Surname</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="email" type="email" value="{{$companyEmployee->email}}" class="validate">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" value="{{$companyEmployee->contact_number}}" type="tel" class="validate">
                                <label for="contact_number">Primary Contact Number</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m6">
                                <input id="contact_number_two" value="{{$companyEmployee->contact_number_two}}" type="tel" class="validate">
                                <label for="contact_number_two">Secondary Contact Number</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="company_id">
                                    <option>Select Company</option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}" {{$companyEmployee->company_id==$company->id?'selected':''}}>{{$company->company_name}}</option>
                                    @endforeach
                                </select>
                                <label>Company Company</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="category_id" multiple>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" >{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                                <label>Person Interest</label>
                            </div>
                            <div class="input-field col m6">
                                <textarea id="address" class="materialize-textarea">{{$companyEmployee->address}}</textarea>
                                <label for="address">Address 1</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <textarea id="address_two" class="materialize-textarea">{{$companyEmployee->address_two}}</textarea>
                                <label for="address">Address 2</label>
                            </div>
                            <div class="input-field col m6">
                                <textarea id="address_three" class="materialize-textarea">{{$companyEmployee->address_three}}</textarea>
                                <label for="address">Address 3</label>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col offset-m4 col m8">
                            <button class="btn waves-effect waves-light" style="margin-left:2em;" id="back" name="action">Back
                                <i class="material-icons right">arrow_back</i>
                            </button>
                            <button class="btn waves-effect waves-light" style="margin-left:2em;" id="update-company-employee" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $('#back').on('click',function(){
                    window.history.back();
                });

                $('#update-company-employee').on('click',function(){
                    let formData = new FormData();
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('email', $('#email').val());
                    formData.append('company_id', $('#company_id').val());
                    formData.append('category_id', $('#category_id').val());
                    formData.append('address', $('#address').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    formData.append('contact_number_two', $('#contact_number_two').val());
                    console.log("company ", formData);
                    let companyEmployee = {!! $companyEmployee !!}
                    $.ajax({
                        url: "/company-employee/"+companyEmployee.id+'/update',
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success",response);
                            alert(response.message);
                            window.history.back();
                        },
                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            console.log("error",message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error",error)
                                if( errors.hasOwnProperty(error) ) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });

            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
