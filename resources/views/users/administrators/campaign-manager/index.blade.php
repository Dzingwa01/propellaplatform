@extends('layouts.administrator-layout')

@section('content')<br>
<br>
{{ Breadcrumbs::render('administrator-companies')}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Administrator Companies</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="companies-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Contact Number</th>
                        <th>Website</th>
                        <th>Address 1</th>
                        <th>Interests</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn " data-position="left" data-tooltip="Add New Company" href="/company-create">
                <i class="large material-icons">add</i>
            </a>

        </div>
        <style>
            th{
                text-transform: uppercase!important;
            }
        </style>
    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $(function () {
                    $('#companies-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '{{route('get-companies')}}',
                        columns: [
                            {data: 'company_name', name: 'company_name'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: "website_url", name:"website_url"},
                            {data: 'address_one', name: 'address_one'},

                            {data:'category.category_name',name:'category.category_name'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="companies-table_length"]').css("display","inline");
                });

                $('#save-company').on('click',function(){
                    let formData = new FormData();
                    formData.append('company_name', $('#company_name').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('address_one', $('#address_one').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    formData.append('city', $('#city').val());

                    formData.append('postal_code', $('#postal_code').val());
                    formData.append('website_url', $('#website_url').val());
                    formData.append('category_id', $('#category_id').val());
                    formData.append('secondary_category_id', $('#secondary_category_id').val());
                    console.log("company ", formData);

                    $.ajax({
                        url: "{{ route('companies.store') }}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success",response);
                            alert(response.message);
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            console.log("error",message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error",error)
                                if( errors.hasOwnProperty(error) ) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });
            });

            function confirm_delete_company(obj){
                var r = confirm("Are you sure want to delete this company?");
                if (r == true) {
                    $.get('/company/delete/'+obj.id,function(data,status){
                        console.log('Data',data);
                        console.log('Status',status);
                        if(status=='success'){
                            alert(data.message);
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }
            }
        </script>
    @endpush
<style>
    nav {
        margin-bottom: 0;
        background-color: grey;
        padding: 5px 16px;
    }
</style>
@endsection
