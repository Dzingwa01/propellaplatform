@extends('layouts.administrator-layout')
@section('content')<br><br>
{{ Breadcrumbs::render('create-question-category')}}
    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <br>
        <h6 style="margin-left: 15em">Create Category</h6>
        <div class="row" style="margin-left: 7em">
            <div class="input-field col s10">
                <input id="category_name" type="text" class="validate">
                <label for="category_name">Category name</label>
            </div>
        </div>
        <div class="row" style="margin-left: 7em">
            <div class="input-field col s10">
                <input id="category_description" type="text" class="validate">
                <label for="category_description">Category description</label>
            </div>
        </div>
        <div class="row" style="margin-left: 100px">
            <div class="file-field input-field col s10">
                <div class="btn">
                    <span>Category Image</span>
                    <input type="file" id="category_image_url">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 600px;">
            <div class="col s4">
                <a class="waves-effect waves-light btn" id="question-category-submit-button">Save</a>
            </div>
        </div>
        <br>
    </div>

    <script>
        $(document).ready(function(){
            $('#question-category-submit-button').on('click',function () {

                let formData = new FormData();
                formData.append('category_name', $('#category_name').val());
                formData.append('category_description', $('#category_description').val());
                jQuery.each(jQuery('#category_image_url')[0].files, function (i, file) {
                formData.append('category_image_url', file);
                });

                $.ajax({
                    url: '{{route('store-category')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    @endsection
