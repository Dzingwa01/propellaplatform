@extends('layouts.administrator-layout')

@section('content')<br>
<br>
{{ Breadcrumbs::render('create-funding-category')}}
    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <br>
        <h5 style="margin-left: 40%">Create Category Resource</h5>
        <br>
        <div class="input-field col s2" style="margin-left: 2em;margin-right: 2em;">
            <input id="category_name" type="text" class="validate">
            <label for="category_name">Category Name</label>
        </div>
        <br>
        <button class="btn waves-effect waves-light" style="margin-left:500px;" id="save-category" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
        <br>
        <br>
    </div>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('#save-category').on('click',function () {

                let formData = new FormData();
                formData.append('category_name', $('#category_name').val());


                $.ajax({
                    url: '{{route('store-resource-category')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        console.log("success", response);
                        $('#save-category').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '{{url('/indexCategory')}}';
                        }, 3000);

                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
            });
        });
    </script>

@endsection
