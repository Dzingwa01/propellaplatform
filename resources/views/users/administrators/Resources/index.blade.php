@extends('layouts.administrator-layout')

@section('content')

    <div class="container-fluid">

        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Funding Resouces</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="funding_resources-table">
                    <thead>
                    <tr>
                        <th>Tittle</th>
                        <th>Closing date</th>
                        <th>Category</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add User Role" href="{{url('/addResources')}}">
                <i class="large material-icons">add</i>
            </a>

        </div>
    </div>

    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $(function () {
                    let fundingResource_id = $('#funding-id-input').val();

                    $('#funding_resources-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: '/get-funding',

                        {{--ajax: '{{route('get-funding')}}',--}}
                        columns: [
                            {data: 'title', name: 'title'},
                            {data: 'closing_date', name: 'closing_date'},
                            {data: 'category_name', name: 'category_name'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]

                    });
                    $('select[name="funding_resources-table_length"]').css("display", "inline");
                });
            });

        </script>
    @endpush
@endsection
