@extends('layouts.administrator-layout')

@section('content')


    <div class="section">
        <br>
        {{ Breadcrumbs::render('venture-email-index')}}
        <div class="row" style="margin-left: 500px">
            <br>
            <br>
            <h5 style="top: 10vh;margin-left: 4em"><b>Send email to Ventures</b></h5>

            <div class="input-field col s6" style="margin-top: 10vh">
                <select id="ventures-selected" multiple>
                    <option value="" disabled selected>Please select Venture</option>
                    @foreach($incubatees as $user_incubatee)
                        <option value="{{$user_incubatee->id}}"> {{$user_incubatee->user->email}}</option>
                    @endforeach
                </select>
                <label>Ventures</label>
            </div>

        </div>
        <div class="row center-align">
            <a class="waves-effect waves-light btn " id="send-mail-button">Send mail</a>
        </div>
    </div>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>

    <script>
        $(document).ready(function () {
            $('select').formSelect();

            $('#send-mail-button').on('click', function(){
                let selected_ventures = [];
                let formData = new FormData();

                jQuery.each(jQuery('#ventures-selected').val(), function (i, value) {
                    selected_ventures.push(value);
                });

                formData.append('selected_ventures', JSON.stringify(selected_ventures));

                let url = "/send-email-to-ventures";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#send-mail-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#send-mail-button').notify(response.message, "error");
                    }
                });
            });

        });
    </script>
@endsection
