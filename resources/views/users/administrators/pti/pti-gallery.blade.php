@extends('layouts.administrator-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('pti-gallery')}}
    <div class="container">
        <div class="card hoverable" style="margin-top: 5em!important;">
            <br>
            <div class="row">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s3"><a class="active" href="#test1">Gallery</a></li>
                    </ul>
                </div>
                <!--Gallery-->
                <div id="test1" class="col s12">
                    <form class="col s12">
                        <div class="row"style="margin-right: 2em;margin-left: 2em;">
                            <div class="input-field col s6">
                                <textarea id="description" type="text" class="materialize-textarea"></textarea>
                                <label for="description">Description</label>
                            </div>
                            <div class="row" style="margin-left: 100px">
                                <div class="file-field input-field col m5">
                                    <div class="btn">
                                        <span>Single Image</span>
                                        <input type="file" id="single_image">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Upload Image</span>
                                        <input id="image_url" type="file" name="image_url" onchange="appendImagePreviews()">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path" type="text">
                                    </div>
                                </div>
                                <br/>
                                <span id="select-message"></span><br/>
                                <span id="message"></span>
                            </div>
                        </div>
                        <div class="row" id="image-preview">
                        </div>

                        <div class="row" style="text-align: center;">
                            <a class="waves-effect waves-light btn" id="image-upload-submit-button">Submit</a>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>

    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    <script>

        let images_array =[];
        let image_array_count = 0;
        var image_counter=0;
        var preview_attr ="";

        $(document).ready(function () {

            $('select').formSelect();
            $('#image-upload-submit-button').on('click', function () {
                let formData = new FormData();
                formData.append('description' ,$('#description').val());
                images_array.forEach(function(image, i){
                    formData.append('image_' + i, image);
                    image_array_count += 1;
                });
                formData.append('image_array_count', image_array_count);
                jQuery.each(jQuery('#single_image')[0].files, function (i, file) {
                    formData.append('single_image', file);
                });
                let url = "{{route('gallery.store')}}";
                $.ajax({
                    url:url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

        });

        function appendImagePreviews(){
            image_counter++;
            $("#message").empty();
            var file = jQuery('#image_url')[0].files[0];
            var imagefile = file.type;

            let id ="image_"+image_counter;
            let card_id = "card_"+image_counter;
            let card_action = "action_"+image_counter;

            //Appending a card to the image preview area
            $("#image-preview").append(' <div class="col s12 m6">\n' +
                '      <div class="card" id='+card_id+'>\n' +
                '        <div class="card-image">\n' +
                '          <img id='+id+'>\n' +
                '        </div>\n' +
                '        <div class="card-action">\n' +
                '          <a href="#" id='+card_action+' onclick="removePreview('+card_id+')">Delete</a>\n' +
                '        </div>\n' +
                '      </div>');

            //Only allow certain file types
            var match = ["image/jpeg", "image/png", "image/jpg"];

            if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                // $('#'+id).attr('src', 'noimage.png');
                $("#"+card_id).remove();
                $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                image_counter--;
                return false;
            } else {
                $("#select-message").empty();
                $("#select-message").append("Click again to add more images");

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(file);
                preview_attr = id;
                jQuery.each(jQuery('#image_url')[0].files, function (i, file) {
                    images_array.push(file);
                });
            }
        }

        //Callback to append the file to the preview section
        function imageIsLoaded(e) {
            $("#"+preview_attr).css("color", "green");
            $("#"+preview_attr).css("display", "block");
            $("#"+preview_attr).attr('src', e.target.result);
            $("#"+preview_attr).attr('width', '200px');
            $("#"+preview_attr).attr('height', '200px');
        }

        function removePreview(card_id){

            $("#"+card_id.id).remove();
        }
    </script>
@endsection
