@extends('layouts.administrator-layout')
@section('content')

    <br>
    <br>
    {{ Breadcrumbs::render('administrator-edit-category-enquiry')}}
    <div class="section" style="margin-top: 2em;">
        <input hidden disabled id="enquiry-category-id" value="{{$enquiryCategory->id}}">
        <div class="row">
            <div class="row center">
                <h4>Update Enquiry Category</h4>
            </div>
            <div class="row center" style="width: 50%">
                <label for="enquiry-category-category-name-input">Enquiry Category Name</label>
                <input type="text" value="{{$enquiryCategory->category_name}}" id="enquiry-category-category-name-input">
                <a class="btn-flat blue" style="color:white;" id="submit-enquiry-category-update-button">Update</a>
            </div>
        </div>
    </div>



    <script>
        $('#submit-enquiry-category-update-button').on('click', function(){
            $(this).text("Updating...");
           let category_name = $('#enquiry-category-category-name-input').val();
           let formData = new FormData();
           let category_id = $('#enquiry-category-id').val();
           formData.append('category_name', category_name);

            let url = '/update-enquiry-category/' + category_id;

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success: function (response, a, b) {
                    $("#submit-enquiry-category-update-button").notify(response.message, "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },
                error: function (response) {
                    let message = response.message;
                    alert(message);
                }
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
