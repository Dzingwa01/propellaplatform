@extends('layouts.administrator-layout')
@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-preassigned-enquiry')}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Pre-Assigned Enquiries</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="pre-assigned-enquiries-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Enquiry Category</th>
                        <th>Date Enquired</th>
                        <th>Enquiry</th>
                        <th>Pre-Assigned To</th>
                        <th>Date Pre Assigned</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#pre-assigned-enquiries-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-pre-assigned-enquiries')}}',
                    columns: [
                        {data: 'name', name: 'name'},
                        {data: 'surname', name: 'surname'},
                        {data: 'email', name: 'email'},
                        {data: 'contact_number', name: 'contact_number'},
                        {data: 'category', name: 'category'},
                        {data: 'date_enquired', name: 'date_enquired'},
                        {data: 'enquiry_message', name: 'enquiry_message'},
                        {data: 'pre_assigned_user', name: 'pre_assigned_user'},
                        {data: 'date_pre_assigned', name: 'date_pre_assigned'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="pre-assigned-enquiries-table_length"]').css("display","inline");
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
