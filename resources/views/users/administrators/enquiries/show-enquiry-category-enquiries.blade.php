@extends('layouts.administrator-layout')
@section('content')

    <br>
    <br>
    {{ Breadcrumbs::render('administrator-show-category-enquiry')}}
    <div class="section" style="margin-top: 2em;">
        <div class="row">
            <div class="row center">
                <h4>{{$enquiryCategory->category_name}} enquiries</h4>
            </div>
            @if(count($enquiryCategory->general_enquiries))
                @foreach($enquiryCategory->general_enquiries as $enquiry)
                    <div class="col l4 m4 s12">
                        <ul class="collapsible">
                            <li>
                                <div class="collapsible-header"><i class="material-icons">date_range</i>Date received: {{$enquiry->date_enquired}}</div>

                                <div class="collapsible-body">
                                    <span>
                                        <b>Title:</b> {{$enquiry->title}}
                                        <br>
                                        <b>Name:</b> {{$enquiry->name}}
                                        <br>
                                        <b>Surname:</b> {{$enquiry->surname}}
                                        <br>
                                        <b>Email:</b> {{$enquiry->email}}
                                        <br>
                                        <b>Contact Number:</b> {{$enquiry->contact_number}}
                                        <br>
                                        <b>Company:</b> {{$enquiry->company}}
                                        <br>
                                        <b>City:</b> {{$enquiry->city}}
                                        <br>
                                        <b>Heard About Us:</b> {{$enquiry->heard_about_us}}
                                        <br>
                                        <b>Message:</b> {{$enquiry->enquiry_message}}
                                        <br>
                                        <hr>
                                        <b>Initial Contact Person:</b> {{$enquiry->initial_contact_person}}
                                        <br>
                                        <b>Initial Contact Method:</b> {{$enquiry->initial_contact_method}}
                                        <br>
                                        <b>Initial Report:</b> {{$enquiry->initial_report}}
                                        <br>
                                        <b>Next Step:</b> {{$enquiry->enquiry_next_step}}
                                        <br>
                                        <b>Date Assigned:</b> {{$enquiry->date_assigned}}
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                @endforeach
            @else
                <h4>There are no enquiries for this category.</h4>
            @endif
        </div>
    </div>






    <script>
        $(document).ready(function(){
            $('.collapsible').collapsible();
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
