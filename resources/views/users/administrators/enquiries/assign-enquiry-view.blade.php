@extends('layouts.administrator-layout')
@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-assign-preassigned-enquiry')}}

    <div class="section" style="margin-top: 2em;">
        <input id="enquiry-id-input" hidden disabled value="{{$enquiry->id}}">
        <div class="row">
            <div class="row center">
                <h4>Enquiry Follow Up</h4>
            </div>

            @if($enquiry->is_assigned == true)
                <div class="row" style="width: 50%;">
                    <p>Who made the initial contact?</p>
                    <input disabled type="text" value="{{$enquiry->initial_contact_person}}">
                </div>
                <div class="row" style="width: 50%;">
                    <p>Contact Method:</p>
                    <input disabled type="text" value="{{$enquiry->initial_contact_method}}">
                </div>
                <div class="row" style="width: 50%;">
                    <p >Report / Discussion:</p>
                    <input disabled type="text" value="{{$enquiry->initial_report}}">
                </div>
                <div class="row" style="width: 50%;">
                    <p>Next step:</p>
                    <input disabled type="text" value="{{$enquiry->enquiry_next_step}}">
                </div>
                <div class="row" style="width: 50%;">
                    <p>Assigned To:</p>
                    <input disabled type="text" value="{{$enquiry->enquiry_assigned_person}}">
                </div>
            @else
                <div class="row" style="width: 50%;" id="initial-contact-row-desktop">
                    <select id="enquiry-initial-contact-desktop">
                        <option value="" disabled selected>Who made the initial contact?</option>
                        @foreach($enquiry_users_array as $user)
                            <option value="{{$user->user_name}}">{{$user->user_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row" style="width: 50%;" id="initial-contact-method-row-desktop" hidden>
                    <select id="enquiry-initial-contact-method-desktop">
                        <option value="" disabled selected>How did you contact {{$enquiry->name}} {{$enquiry->surname}}?</option>
                        <option value="Phone">Phone</option>
                        <option value="Email">Email</option>
                        <option value="Face-to-face">Face-to-face</option>
                        <option value="Zoom">Zoom</option>
                        <option value="No response to any contact type">No response to any contact type</option>
                    </select>
                </div>
                <div class="row" style="width: 50%;" id="initial-report-row-desktop" hidden>
                    <label for="enquiry-initial-report-input">Report</label>
                    <input type="text" id="enquiry-initial-report-input" placeholder="Discussion Points / Requirements">
                    <a class="btn-flat blue" style="color:white;" id="enquiry-initial-report-button">Next</a>
                </div>
                <div class="row" style="width: 50%;" id="next-step-row-desktop" hidden>
                    <label for="enquiry-next-step-input">Next step</label>
                    <input type="text" id="enquiry-next-step-input" placeholder="What are the next steps?">
                    <a class="btn-flat blue" style="color:white;" id="enquiry-next-step-button">Next</a>
                </div>
                <div class="row" style="width: 50%;" id="assigned-to-row-desktop" hidden>
                    <select id="enquiry-assigned-to-desktop">
                        <option value="" disabled selected>Assigned to?</option>
                        @foreach($enquiry_users_array as $user)
                            <option value="{{$user->user_name}}~{{$user->user_email}}">{{$user->user_name}}</option>
                        @endforeach
                    </select>
                    <a class="btn-flat blue" style="color:white;" id="enquiry-assigned-to-button">Submit</a>
                </div>
            @endif
        </div>
    </div>


    <script>
        $(document).ready(function(){
            $('select').formSelect();
        });

        $('#enquiry-initial-contact-desktop').on('change', function(){
            $('#initial-contact-method-row-desktop').show();
        });

        $('#enquiry-initial-contact-method-desktop').on('change', function(){
            $('#initial-report-row-desktop').show();
        });

        $('#enquiry-initial-report-button').on('click', function(){
            $('#next-step-row-desktop').show();
        });

        $('#enquiry-next-step-button').on('click', function(){
            $('#assigned-to-row-desktop').show();
        });

        $('#enquiry-assigned-to-button').on('click', function(){
            let initial_contact_person = $('#enquiry-initial-contact-desktop').val();
            let initial_contact_method = $('#enquiry-initial-contact-method-desktop').val();
            let initial_report = $('#enquiry-initial-report-input').val();
            let enquiry_next_step = $('#enquiry-next-step-input').val();
            let assigned_to_value = $('#enquiry-assigned-to-desktop').val();
            let enquiry_id = $('#enquiry-id-input').val();
            let split = assigned_to_value.split('~');
            let assigned_to = split[0];
            let assigned_to_email = split[1];
            let formData = new FormData();

            formData.append('initial_contact_person', initial_contact_person);
            formData.append('initial_contact_method', initial_contact_method);
            formData.append('initial_report', initial_report);
            formData.append('enquiry_next_step', enquiry_next_step);
            formData.append('enquiry_assigned_person', assigned_to);
            formData.append('enquiry_assigned_person_email', assigned_to_email);


            if(assigned_to === null){
                $('#enquiry-assigned-to-button').notify("Please select the assigned to field.", "error");
            }

            let url = '/admin-assign-enquiry/' + enquiry_id;

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success: function (response, a, b) {
                    $("#enquiry-assigned-to-button").notify(response.message, "success");

                    setTimeout(function(){
                        window.location.href = '/unassigned-enquiry-index';
                    }, 3000);
                },
                error: function (response) {
                    let message = response.message;
                    alert(message);
                }
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
