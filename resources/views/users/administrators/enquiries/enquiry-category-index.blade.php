@extends('layouts.administrator-layout')
@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('administrator-enquiry-category')}}
    <br>
    <br>
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Enquiry Categories</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="enquiry-categories-table">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <th># of Enquiries</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left"
           data-tooltip="Add New Category" href="#enquiry-category-modal">
            <i class="large material-icons">add</i>
        </a>
    </div>

    <div id="enquiry-category-modal" class="modal">
        <div class="modal-content center">
            <h4>Add Category</h4>
            <input type="text" id="enquiry-category-name-input" placeholder="Category name">
            <a class="waves-effect waves-green btn-flat" id="submit-enquiry-category-button">Submit</a>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#enquiry-categories-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-enquiry-categories')}}',
                    columns: [
                        {data: 'category_name', name: 'category_name'},
                        {data: 'category_count', name: 'category_count'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
                $('select[name="enquiry-categories-table_length"]').css("display","inline");
            });
        });

        $('#submit-enquiry-category-button').on('click', function(){
            let category_name = $('#enquiry-category-name-input').val();
            let formData = new FormData();
            formData.append('category_name', category_name);

            let url = 'store-enquiry-category';

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success: function (response, a, b) {
                    $("#submit-enquiry-category-button").notify(response.message, "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },
                error: function (response) {
                    let message = response.message;
                    alert(message);
                }
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
