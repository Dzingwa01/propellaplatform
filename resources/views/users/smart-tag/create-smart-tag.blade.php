@extends('layouts.administrator-layout')

@section('content')
    <br><br>
{{--    {{ Breadcrumbs::render('administrator-create-smart-tags')}}--}}
    <div class="section">
        <div class="row center">
            <h4>Add a smart tag</h4>
            <input type="text" style="width:50%;" id="smart-tag-name-input" placeholder="Enter smart city tag here">
            <br>
            <button class="btn blue" id="submit-smart-tag-button">Save</button>
        </div>
    </div>

    <script>
        //js function to store entered smart city name
        $(document).ready(function(){
            $('#submit-smart-tag-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('smart_tag_name', $('#smart-tag-name-input').val());

                let url = 'store-smart-tag';

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    //js to receive system feedback
                    success: function (response, a, b) {
                        $("#smart-tag-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = 'smart-tag';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
@endsection
