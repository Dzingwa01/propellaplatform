@extends('layouts.admin-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <form id="incubateeEmployee-form" class="col s12" style="margin-top:1em;" enctype="multipart/form-data">
        @csrf
        <div class="card-content black-text">
            <h2 style="font-size: 2em;padding-left: 220px">Create Employee</h2>
            <input id="recipe_id" value="{{$incubateeEmployees->id}}" hidden>
            <div class="row">
                <div class="input-field col m6">
                    <input id="employee_name" type="text" class="validate">
                    <label for="employee_name">Name of Employee</label>
                </div>
                <div class="input-field col m6">
                    <input id="position" type="text" class="validate">
                    <label for="position">Position</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6">
                    <input id="start_date" type="text" class="datepicker">
                    <label for="start_date">Start Date</label>
                </div>
                <div class="input-field col m6">
                    <input id="end_date" type="text" class="datepicker">
                    <label for="end_date">End Date</label>
                </div>
            </div>
            <div class="row">
                <div class="file-field input-field col m6">
                    <div class="btn">
                        <span>Contract</span>
                        <input type="file" id="contract_url" multiple>
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="Upload in PDF">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col offset-m4">
                    <a
                        class="btn waves-effect waves-light" href="{{url('users.incubatees.admin-job.index')}}">Cancel
                        <i class="material-icons left">arrow_back</i>
                    </a>
                    <button id="save-incubatee-employee-details" style="margin-left: 2em"
                            class="btn waves-effect waves-light" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        </div>
    </form>

    @push('custom-scripts')
        <script>
            $('#incubateeEmployee-form').on('submit', function (e) {
                e.preventDefault();

                formData.append('employee_name', $('#employee_name').val());
                formData.append('position', $('#position').val());
                formData.append('start_date', $('#start_date').val());
                formData.append('end_date', $('#end_date').val());
                jQuery.each(jQuery('#contract_url')[0].files, function (i, file) {
                    formData.append('contract_url', file);
                });
                console.log(formData);


                let url = "{{ route('add-incEmployee') }}" + $('#incubatee_id').val();
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        console.log("success", response);
                        alert(response.message);
                        window.location.href = 'users.incubatees.admin-job.index';
                        /*incubatee = response.incubatee;
                        sessionStorage.setItem('incubatee_id', response.incubatee['id']);*/
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);
                    }
                });
            });
        </script>
    @endpush
@endsection
