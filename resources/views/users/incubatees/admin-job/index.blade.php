@extends('layouts.admin-layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Incubatee Employees</h6>
            {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="incEmployee-table">
                    <thead>
                    <tr>
                        <th>Employee Name</th>
                        <th>Position</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Incubatee" href="/users/incubatees/admin-job/create">
                <i class="large material-icons">add</i>
            </a>

        </div>

    </div>
    @push('custom-scripts')

        <script>

            $(document).ready(function (data) {
                $(function () {
                    $('#incEmployee-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        type : 'get',
                        scrollX: 640,
                        ajax: '{{route('get-incEmployee')}}',
                        columns: [
                            {data: 'employee_name', name: 'employee_name'},
                            {data: 'position', name: 'position'},
                            {data: 'start_date', name: 'start_date'},
                            {data: 'end_date', name: 'end_date'},
                            /*{data: 'start_time', name: 'start_time'},*/
                            {data: 'action', name: 'action', orderable: true, searchable: true}
                        ]
                    });
                    $('select[name="incEmployee-table_length"]').css("display","inline");
                });
            });

        </script>
    @endpush
@endsection
