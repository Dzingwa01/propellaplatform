@extends('layouts.admin-layout')

@section('content')
    <div class="section" style="margin-top: 5vh">
        <input id="user-id-input" value="{{$incubatee->id}}" hidden disabled>
    </div>


    {{--PANEL INTERVIEW OVERVIEW SECTION--}}

    <script>
        function editRole(obj){
            let bootcamper_panelist_id = obj.id;
            window.location.href = '/show-score-sheet/' + bootcamper_panelist_id;
        }
        $('#show-guest-panelist-button').on('click', function(){
            $('#show-guest-panelist-button').hide();
            $('#guest-panelist-container').show();
        });

        $('#contact-result-input').on('change', function(){
            if($('#contact-result-input').val() === "Panel Interview"){
                $('#panel-interview-row').show();
                $('#panel-interview-declined-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Declined" || $('#contact-result-input').val() === "Application Declined"){
                $('#panel-interview-declined-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-but-referred-row').hide();
            } else if($('#contact-result-input').val() === "Interview Referred" || $('#contact-result-input').val() === "Application Referred"){
                $('#panel-interview-declined-but-referred-row').show();
                $('#panel-interview-row').hide();
                $('#panel-interview-declined-row').hide();
            }
        });
        //Append panel
        function appendPanelist(){
            let email = $('#extra-panelist-email-input').val();
            let incubatee_id = $('#user-id-input').val();
            let question_category_id = $('#question-category-id').val();
            let url = '/admin-add-panel-for-panel-access-window-for-incubatees';
            let formData = new FormData();
            formData.append('email', email);
            formData.append('incubatee_id', incubatee_id);
            formData.append('panel_selection_date', $('#panel-selection-date-display').val());
            formData.append('panel_selection_time', $('#panel-selection-time-display').val());
            formData.append('question_category_id', question_category_id);

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#add-panelist-header').notify(response.message + ". Page refresshing in 3 seconds.", "success");

                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    $('#add-panelist-header').notify(response.message, "error");
                }
            });
        }

        function approvePanel(){
            $('#go-panel-row').show();
            $('#no-go-panel-row').hide();
        }

        function declinePanel(){
            $('#go-panel-row').hide();
            $('#no-go-panel-row').show();
        }

        $(document).ready(function () {
            $('.modal').modal();
            let user_id = $('#user-id-input').val();
            $('select').formSelect();

            <!--Finalize button-->
            $('#incubatee-panel-interview-button').on('click', function(){
                let selected_panelists = [];
                let formData = new FormData();

                jQuery.each(jQuery('#panelist-input').val(), function (i, value) {
                    selected_panelists.push(value);
                });

                formData.append('contacted_via', $('#contact-input').val());
                formData.append('result_of_contact', $('#contact-result-input').val());
                formData.append('selected_panelists', JSON.stringify(selected_panelists));
                formData.append('guest_panelists', JSON.stringify(guest_panelists));
                formData.append('panel_selection_date', $('#panel-selection-date-input').val());
                formData.append('panel_selection_time', $('#panel-selection-time-input').val());
                formData.append('question_category_id', $('#question-category-input').val());


                let url = "/incubatee-panel-interview-approve/" + user_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#incubatee-panel-interview-button').notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/incubatees';
                        }, 3000);
                    },

                    error: function (response) {
                        $('#incubatee-panel-interview-button-button').notify(response.message, "error");
                    }
                });
            });

            //Update panel interview details
            $('#update-panel-interview-details-button').on('click', function(){
                let formData = new FormData();
                let date = $('#panel-selection-date-display').val();
                let time = $('#panel-selection-time-display').val();
                let contact = $('#contact-input-display').val();
                let contact_result = $('#contact-result-input-display').val();
                let question_category_id = $('#question-category-display').val();

                formData.append('date', date);
                formData.append('time', time);
                formData.append('contact', contact);
                formData.append('contact_result', contact_result);
                formData.append('question_category_id', question_category_id);

                let url = "/update-incubatee-panel-interview/" + user_id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#update-panel-interview-details-button').notify(response.message, "success");
                    },
                    error: function (response) {
                        $('#update-panel-interview-details-button').notify(response.message, "error");
                    }
                });

            });

            //DECLINE PANEL INTERVIEW
            $('#set-incubatee-to-to-exited-incubatees-button').on('click', function(){
                let r = confirm("Are you sure you want to decline this incubatee?");

                if(r === true){
                    $('#set-incubatee-to-to-exited-incubatees-button').text("Please wait");
                    let formData = new FormData();
                    formData.append('contacted_via', $('#panel-contact-input').val());
                    formData.append('result_of_contact', $('#panel-contact-result-input').val());
                    formData.append('declined_reason_text', $('#panel-decline-but-referred-reason-input').val());
                    formData.append('referred_company', $('#panel-referred-company-input').val());

                    let user_id = $('#user-id-input').val();
                    let url = "/set-bootcamper-to-declined-bootcamper/" + user_id;

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            $('#set-incubatee-to-to-exited-incubatees-button').notify(response.message, "success");

                            setTimeout(function(){
                                window.location.href = '/bootcampers';
                            }, 3000);
                        },

                        error: function (response) {
                            let message = response.message;
                            alert(message);
                        }
                    });
                } else {
                    alert("Action cancelled.");
                }
            });

            //Incubatee contact log
            $('#incubatee-contact-log-submit-button').on('click',function () {
                let incubatee_id =$('#user-id-input').val();

                let formData = new FormData();
                formData.append('comment', $('#comment').val());
                formData.append('date', $('#date').val());
                formData.append('incubatee_contact_results', $('#incubatee_contact_results').val());
                console.log(formData);

                let url = '/store-incubatee-contact-log/' + incubatee_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        $("#incubatee-contact-log-submit-button").notify(
                            "You have successfully log a contact", "success",
                            { position:"right" }
                        );
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },
                });
            });

        });

        //Approve Applicant
        let guest_panelists = [];

        $('#add-guest-panelist-button').on('click', function(){
            let title = $('#title-input').val();
            let initials = $('#initial-input').val();
            let name = $('#name-input').val();
            let surname = $('#surname-input').val();
            let position = $('#position-input').val();
            let company = $('#company-input').val();
            let email = $('#email-input').val();
            let contact_number = $('#contact-number-input').val();

            let object = {
                title: title,
                initials: initials,
                name: name,
                surname: surname,
                position: position,
                company: company,
                email: email,
                contact_number: contact_number
            };

            guest_panelists.push(object);

            $('#title-input').val("");
            $('#initial-input').val("");
            $('#name-input').val("");
            $('#surname-input').val("");
            $('#position-input').val("");
            $('#company-input').val("");
            $('#email-input').val("");
            $('#contact-number-input').val("");
        });



        $('#application').on('click', function () {
            $('#application-details-row').show();
            var element = document.getElementById("ap");
            element.scrollIntoView({behavior: "smooth"});
            $('#incubatee-uploads-row').hide();
            $('#contact-log-row').hide();
            $('#incubatee-events').hide();
            $('#panel-results-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#uploads').on('click', function () {
            $('#incubatee-uploads-row').show();
            var element = document.getElementById("bu");
            element.scrollIntoView({behavior: "smooth"});
            $('#application-details-row').hide();
            $('#contact-log-row').hide();
            $('#incubatee-events').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#contact-log').on('click', function () {
            $('#contact-log-row').show();
            var element = document.getElementById("contact-log-row");
            element.scrollIntoView({behavior: "smooth"});
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#incubatee-events').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });
        $('#events').on('click', function () {
            $('#incubatee-events').show();
            var element = document.getElementById("incubatee-events");
            element.scrollIntoView({behavior: "smooth"});
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
            $('#incubatee-evaluation').hide();
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
        });

        $('#workshops').on('click', function () {
            $('#incubatee-evaluation').show();
            $('#workshops-incubatee-evaluation').show();
            $('#pre-post-incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#pre-post').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#incubatee-evaluation').show();
            $('#pre-post-incubatee-evaluation').show();
            $('#workshops-incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#panel-results').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#panel-results-row').show();
            $('#incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#go-no-go-panel-row').hide();
            $('#panel-results-row').hide();
        });
        $('#go-no-go-panel').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#go-no-go-panel-row').show();
            $('#bootcamper-panel-results').hide();
            $('#incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
            $('#panel-results-row').hide();
        });

        $('#panel-results').on('click', function () {
            $('#workshops-incubatee-evaluation').hide();
            $('#pre-post-incubatee-evaluation').hide();
            $('#panel-results-row').show();
            $('#go-no-go-panel-row').hide();
            $('#bootcamper-panel-results').hide();
            $('#incubatee-evaluation').hide();
            $('#incubatee-events').hide();
            $('#contact-log-row').hide();
            $('#incubatee-uploads-row').hide();
            $('#application-details-row').hide();
        });

        function printContent(el) {
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }

    </script>
    <script>
        $('#applicant-panel').on('click', function () {
            $('#applicant-panel-interview-overview-section').show();
            $('#bootcamp-panel-interview-overview-section').hide();
        });

        $('#bootcamp-panel').on('click', function () {
            $('#bootcamp-panel-interview-overview-section').show();
            $('#applicant-panel-interview-overview-section').hide();
        });

    </script>
@endsection
