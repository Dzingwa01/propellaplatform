@extends('layouts.admin-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>

    <br>
    <br>
    {{--{{ Breadcrumbs::render('admin-incubatee-events-edit',$eventIncubatee)}}--}}
    <div class="card" style="margin-top: 10vh;width: 800px;margin-left: 400px">
        <br>
        <form class="col s12" id="Edit-event-incubatee" enctype="multipart/form-data">
            <input id="event_incubatee_id" value="{{$eventIncubatee->id}}" hidden>
            <div class="">
                <h5 class="center-align">Edit Incubatee Event</h5>

                <div class="row" style="margin-left:2em;margin-right: 2em">
                    <div class="input-field col s6">
                        <input id="title" type="text" value="{{$eventIncubatee->events->title}}" class="validate"
                               disabled>
                        <label for="title">Event Tittle</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="start" type="text" value="{{$eventIncubatee->events->start->toDateString()}}"
                               class="validate" disabled>
                        <label for="start">Date Registered</label>
                    </div>
                </div>

                <div class="row" style="margin-left: 2em;margin-right: 2em">
                    <div class="input-field col s6">
                        <select id="attended">
                            <option value="1" {{$eventIncubatee->attended=='1'?'selected':''}}>Yes</option>
                            <option value="0" {{$eventIncubatee->attended=='0'?'selected':''}}>No</option>
                        </select>
                        <label for="type">Attended</label>
                    </div>
                    <div class="input-field col s6">
                        <select id="registered">
                            <option value="1" {{$eventIncubatee->registered=='1'?'selected':''}}>Yes</option>
                            <option value="0" {{$eventIncubatee->registered=='0'?'selected':''}}>No</option>
                        </select>
                        <label for="type">Registered</label>
                    </div>
                </div>

                <div class="container" hidden id="deregister-container">
                    <div class="row">
                        <div class="col l6 m6">
                            <label for="date_time_de_register">Date / Time De-registered</label>
                            <input id="date_time_de_register" type="date">
                        </div>
                        <div class="col l6 m6">
                            <label for="de_register_reason">De-registered Reason</label>
                            <textarea id="de_register_reason"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <button class="btn waves-effect waves-light" style="margin-left:650px;" id="save-event-bootcamper"
                    name="action">Submit
                <i class="material-icons right">send</i>
            </button>
        </form>

        <br>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
            $('#attended').on('change', function(){
                if($('#attended').val() === "0"){
                    $('#deregister-container').show();
                }else {
                    $('#deregister-container').hide();
                }

            });
            $('#Edit-event-incubatee').on('submit', function (e) {

                e.preventDefault();
                let formData = new FormData();
                if ($('#registered').val() == '1') {
                    formData.append('attended', $('#attended').val());
                    formData.append('registered', $('#registered').val());
                    formData.append('date_time_de_register', $('#date_time_de_register').val());
                    formData.append('de_register_reason', $('#de_register_reason').val());


                    console.log(formData);

                    let url = '/updateIncubateeEvent/' + '{{$eventIncubatee->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            alert(response.message);
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                }
            });

        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }

    </style>
@endsection






{{--
@extends('layouts.admin-layout')

@section('content')
    <form class="col s12" id="Edit-event-incubatee" enctype="multipart/form-data">
        <div class="card" style="width: 800px;margin: 0 auto;top: 10vh">
            <div class="row">
                <div class="row" style="margin-left: 2em">
                    <div class="col l12 m12">
                        <div class="row center">
                            <h5><b>Event Details:</b></h5>
                            <div class="input-field col m6">
                                <input id="title" value="{{$eventIncubatee->events->title}}" type="text"
                                       class="validate" disabled>
                                <label for="title">Event Title</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="start" value="{{$eventIncubatee->events->start->toDateString()}}" type="text"
                                       class="validate" disabled>
                                <label for="start">Event start</label>
                            </div>
                        </div>
                    </div>
                    <input hidden disabled id="event-id-input" value="{{$eventIncubatee->events->id}}">
                    <input hidden disabled id="incubatee-id-input" value="{{$eventIncubatee->incubatees->id}}">
                    <input hidden disabled id="incubatee-event-id-input" value="{{$eventIncubatee->id}}">
                </div>

                <div class="row" style="margin-left: 2em;margin-right: 2em">
                    <div class="col l6 m6">
                        <label for="registered">Registered</label>
                        <select id="registered">
                            @if($eventIncubatee->registered == true)
                                <option value="Yes" selected>Yes</option>
                                <option value="No">No</option>
                            @else
                                <option value="Yes">Yes</option>
                                <option value="No" selected>No</option>
                            @endif
                        </select>
                    </div>
                    <div class="col l6 m6">
                        <label for="attended">Attended</label>
                        <select id="attended">
                            @if($eventIncubatee->attended == true)
                                <option value="Yes" selected>Yes</option>
                                <option value="No">No</option>
                            @else
                                <option value="Yes">Yes</option>
                                <option value="No" selected>No</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="container" hidden id="deregister-container">
                    <div class="row">
                        <div class="col l6 m6">
                            <label for="date_time_registered">Date / Time De-registered</label>
                            <input id="date_time_registered" type="date">
                        </div>
                        <div class="col l6 m6">
                            <label for="de_registered_reason">De-registered Reason</label>
                            <textarea id="de_registered_reason"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left: 2em">
                    <div class="col l12 m12">
                        <label for="date_time_registered">Date / Time Registered</label>
                        <input id="date_time_registered" value="{{$eventIncubatee->date_time_registered}}"
                               type="date">
                    </div>
                </div>

                <div class="row" style="margin-left: 2em">
                    <button class="btn blue" id="submit-incubatee-event-update-button">Update</button>
                </div>
            </div>
        </div>
    </form>

    <script>
        $(document).ready(function () {
            $('select').formSelect();

            $('#registered').on('change', function () {
                if ($('#registered').val() == 'No') {
                    $('#deregister-container').show();
                } else {
                    $('#deregister-container').hide();
                    $('#de_registered').val("");
                    $('#de_registered_reason').val("");
                    $('#date_time_de_registered').val("");
                }
            });
        });

        $('#Edit-event-incubatee').on('submit', function (e) {

            e.preventDefault();
            let formData = new FormData();
            let incubatee_event_id = $('#incubatee-event-id-input').val();
            let incubatee_id = $('#incubatee-id-input').val();
            let url = '/admin-update-incubatee-registered-event/' + incubatee_event_id;

            if ($('#registered-input').val() == 'Yes') {
                formData.append('date_time_registered', $('#date_time_registered').val());
                formData.append('registered', $('#registered').val());
                formData.append('attended', $('#attended').val());
                formData.append('event_id', $('#event-id-input').val());
                formData.append('incubatee_id', $('#incubatee-id-input').val());

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            } else if ($('#registered-input').val() == 'No') {
                let r = confirm("Are you sure want to de-register this visitor?");
                if (r == true) {
                    formData.append('registered', $('#registered-input').val());
                    formData.append('de_registered-reason', $('#de-registered-reason-input').val());
                    formData.append('de-registered_date_time', $('#date-time-de-registered-input').val());
                    formData.append('event_id', $('#event-id-input').val());
                    formData.append('incubatee_id', $('#incubatee-id-input').val());

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                        success: function (response, a, b) {
                            alert(response.message);
                            window.location.href = '/get-incubatee-registered-events-via-edit/' + incubatee_id;
                        },

                        error: function (response) {
                            console.log("error", response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
                } else {
                    alert('De-registration cancelled');
                }
            }
        });
    </script>

@endsection
--}}
