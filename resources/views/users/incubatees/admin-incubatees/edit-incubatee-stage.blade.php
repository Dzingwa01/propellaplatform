@extends('layouts.admin-layout')

@section('content')

    <br><br>
    {{ Breadcrumbs::render('admin-edit-stages')}}
    <div class="section">
        <div class="row center">
            <input hidden disabled id="incubatee-stage-id-input" value="{{$incubateeStage->id}}">
            <h4>Edit incubatee stage</h4>
            <input type="text" style="width:50%;" id="incubatee-stage-name-input" value="{{$incubateeStage->stage_name}}">
            <br>
            <button class="btn blue" id="update-stage-name-button">Update</button>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#update-stage-name-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('stage_name', $('#incubatee-stage-name-input').val());

                let incubatee_stage_id = $('#incubatee-stage-id-input').val();
                let url = '/update-incubatee-stage/' + incubatee_stage_id;

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#incubatee-stage-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = '/incubatee-stages';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
