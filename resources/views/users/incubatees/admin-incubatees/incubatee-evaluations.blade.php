@extends('layouts.admin-layout')
@section('content')
    <br>

   <div class="row">
       <nav>
           <div class="nav-wrapper" style="background-color: gray">
               <div class="col s12">
                   <a href="/home" class="breadcrumb">Home</a>
                   <a href="/incubatees" class="breadcrumb">Incubatees</a>
                   <a href="/show-incubatee-details/{{$incubatee->id}}" class="breadcrumb" style="color: white">Incubatee Details</a>
                   <a href="#!" class="breadcrumb" style="color: white">Evaluations</a>
               </div>
           </div>
       </nav>
   </div>
    <div class="row center">
        <h4><b>{{$incubatee->user->name}} {{$incubatee->user->surname}} Evaluations</b></h4>
    </div>

    {{-- EVALUATION FORMS--}}
    <div class="col s12 row" id="incubatee-evaluation" style="margin-left: 25%;margin-right: 30%;border-radius: 10px;">
        <table style="width: 1000px">
            <tr>
                <th>Evaluations</th>
                <th>Action</th>
            </tr>
            <tr>
                @if(isset($bootcamper))
                    <td>Pre bootcamp workshop evaluation form</td>
                    @if($bootcamper->beginning_evaluation_complete == null)
                        <td>
                            <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreBootcamp">View</a>
                        </td>
                    @else
                        <td>
                            <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreBootcamp">View</a>
                        </td>
                    @endif
            </tr>
            <tr>
                <td>Post bootcamp workshop evaluation form</td>
                @if($bootcamper->post_evaluation_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostBootcamp">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostBootcamp">View</a>
                    </td>
                @endif
                @endif
            </tr>
            <tr>
                <td>Pre Stage 2 workshop evaluation form</td>
                @if($incubatee->pre_stage_two_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreStage2">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreStage2">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Post Stage 2 workshop evaluation form</td>
                @if($incubatee->post_stage_two_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostStage2">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostStage2">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Pre Stage 3 workshop evaluation form</td>
                @if($incubatee->pre_stage_three_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreStage3">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreStage3">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Post Stage 3 workshop evaluation form</td>
                @if($incubatee->post_stage_three_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostStage3">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostStage3">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Pre Stage 4 workshop evaluation form</td>
                @if($incubatee->pre_stage_four_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreStage4">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreStage4">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Post Stage 4 workshop evaluation form</td>
                @if($incubatee->post_stage_four_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostStage4">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostStage4">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Pre Stage 5 workshop evaluation form</td>
                @if($incubatee->pre_stage_five_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePreStage5">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePreStage5">View</a>
                    </td>
                @endif
            </tr>
            <tr>
                <td>Post Stage 5 workshop evaluation form</td>
                @if($incubatee->post_stage_five_complete == null)
                    <td>
                        <a disabled="" class="waves-effect waves-light btn modal-trigger" href="#collapsePostStage5">View</a>
                    </td>
                @else
                    <td>
                        <a class="waves-effect waves-light btn modal-trigger blue" href="#collapsePostStage5">View</a>
                    </td>
                @endif
            </tr>
        </table>

        {{-- Pre bootcamp--}}
        @if(isset($bootcamper))
            <div id="collapsePreBootcamp" class="modal">
                <div class="modal-content">
                    <h5 class="center-align"><b>PRE BOOTCAMP WORKSHOP EVALUATION FORM</b></h5>
                    @if($bootcamper->beginning_evaluation_complete == null)
                        <p>Not submitted yet</p>
                    @else
                        @foreach($pre_bootcamp_question_answers_array as $question_answer)
                            <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                            <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                                <textarea disabled>{{$question_answer->answer_text}}</textarea>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                </div>
            </div>
            {{-- Post bootcamp--}}
            <div id="collapsePostBootcamp" class="modal">
                <div class="modal-content">
                    <h5 class="center-align"><b>POST BOOTCAMP  WORKSHOP EVALUATION FORM</b></h5>
                    @if($bootcamper->post_evaluation_complete == null)
                        <p>Not submitted yet</p>
                    @else
                        @foreach($post_bootcamp_question_answers_array as $question_answer)
                            <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                            <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                                <textarea disabled>{{$question_answer->answer_text}}</textarea>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                </div>
            </div>
        @endif
        <!--Stage 2-->
        <div id="collapsePreStage2" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>PRE STAGE 2 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->pre_stage_two_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($pre_stage2_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <div id="collapsePostStage2" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>POST STAGE 2 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->post_stage_two_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($post_stage2_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!--STAGE 3-->
        <div id="collapsePreStage3" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>PRE STAGE 3 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->pre_stage_three_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($pre_stage3_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <div id="collapsePostStage3" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>POST STAGE 3 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->post_stage_three_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($post_stage3_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!--STAGE 4-->
        <div id="collapsePreStage4" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>PRE STAGE 4 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->pre_stage_four_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($pre_stage4_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <div id="collapsePostStage4" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>POST STAGE 4 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->post_stage_four_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($post_stage4_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!--STAGE 5-->
        <div id="collapsePreStage5" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>PRE STAGE 5 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->pre_stage_five_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($pre_stage5_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <div id="collapsePostStage5" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><b>POST STAGE 5 WORKSHOP EVALUATION FORM</b></h5>
                @if($incubatee->post_stage_five_complete == null)
                    <p>Not submitted yet</p>
                @else
                    @foreach($post_stage5_question_answers_array as $question_answer)
                        <h6 style="margin-left: 4em;margin-right: 4em">{{ $question_answer->question_text}} </h6>
                        <div class="input-field" style="margin-left: 4em;margin-right: 10em">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
    </div>
@endsection
