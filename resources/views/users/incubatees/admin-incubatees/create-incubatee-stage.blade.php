@extends('layouts.admin-layout')

@section('content')

    <br><br>
    {{ Breadcrumbs::render('admin-create-stages')}}
    <div class="section">
        <div class="row center">
            <h4>Add an incubatee stage</h4>
            <input type="text" style="width:50%;" id="incubatee-stage-name-input" placeholder="Enter stage name here">
            <br>
            <button class="btn blue" id="submit-incubatee-stage-button">Save</button>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#submit-incubatee-stage-button').on('click', function (e) {
                $(this).text("Loading...");
                e.preventDefault();
                let formData = new FormData();
                formData.append('stage_name', $('#incubatee-stage-name-input').val());

                let url = 'store-incubatee-stage';

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        $("#incubatee-stage-name-input").notify(response.message, "success");

                        setTimeout(function(){
                            window.location.href = 'incubatee-stages';
                        }, 3000);
                    },
                    error: function (response) {
                        let message = response.message;
                        alert(message);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
