@extends('layouts.admin-layout')

@section('content')
    <div class="container">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Create
                Venture</h6>
            <div class="row card hoverable">
                <div class="col s12">
                    <ul class="tabs">
                        {{--<li class="tab col s4"><a href="#test1" class="active">Start Up Details</a></li>--}}
                        <li class="tab col s6"><a href="#test1">Personal Details</a></li>
                        <li class="tab col s6"><a href="#test2">Media</a></li>
                    </ul>
                </div>
                <div id="test1" class="card col s12">
                    <br>
                    <br>
                    <form id="start_details__update_form" class="col s12" style="margin-top:1em; margin-right: 2em;margin-left: 2em;" >
                        <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Update Venture</h6>

                        @csrf
                        {{--<input value="{{$current_user->id}}" id="incubatee_id" hidden>--}}
                        <input value="{{$venture->id}}" id="venture_id" hidden>
                        <h6 style="padding-left: 50px;"><b>START UP DETAILS</b></h6>
                        <div class="row" >
                            <div class="input-field col m6">
                                <input id="company_name" value="{{$venture->company_name}}" type="text" class="validate">

                                <label for="company_name">Company Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" value="{{$venture->contact_number}}" type="text" class="validate">
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="ownership" value="{{isset($current_venture_ownership->registration_number)?$current_venture_ownership->registration_number:''}}" type="text" class="validate">
                                <label for="ownership">Ownership (fill in percentage)</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="registration_number" value="{{isset($current_venture_ownership->registration_number)?$current_venture_ownership->registration_number:''}}" type="text" class="validate">
                                <label for="registration_number">Registration Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="BBBEE_Level">
                                    {{--<option value="" disabled selected>Choose BBBEE Level</option>--}}
                                    <option
                                        value="1" {{isset($current_venture_ownership->BBBEE_level)=='1'?$current_venture_ownership->BBBEE_level:''}}>
                                        Level 01
                                    </option>
                                    <option
                                        value="2" {{isset($current_venture_ownership->BBBEE_level)=='2'?$current_venture_ownership->BBBEE_level:''}}>
                                        Level 02
                                    </option>
                                    <option
                                        value="3" {{isset($current_venture_ownership->BBBEE_level)=='3'?$current_venture_ownership->BBBEE_level:''}}>
                                        Level 03
                                    </option>
                                    <option
                                        value="4" {{isset($current_venture_ownership->BBBEE_level)=='4'?$current_venture_ownership->BBBEE_level:''}}>
                                        Level 04
                                    </option>
                                    <option
                                        value="5" {{isset($current_venture_ownership->BBBEE_level)=='5'?$current_venture_ownership->BBBEE_level:''}}>
                                        Level 05
                                    </option>
                                    <option
                                        value="6" {{isset($current_venture_ownership->BBBEE_level)=='6'?$current_venture_ownership->BBBEE_level:''}}>
                                        Level 06
                                    </option>
                                    <option
                                        value="7" {{isset($current_venture_ownership->BBBEE_level)=='7'?$current_venture_ownership->BBBEE_level:''}}>
                                        Level 07
                                    </option>
                                </select>
                            </div>
                            <div class="input-field col m6">
                                <input id="venture_type" value="{{$venture->venture_type}}" type="text" class="validate">
                                <label for="venture_type">Venture Type</label>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>ADDRESS DETAILS</b></h6>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="physical_address_one" value="{{$venture->physical_address_one}}" type="text" class="validate">
                                <label for="physical_address_one">Physical Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="physical_address_two" value="{{$venture->physical_address_two}}" type="text" class="validate">
                                <label for="physical_address_two">Physical Address 2</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="physical_address_three" value="{{$venture->physical_address_three}}" type="text" class="validate">
                                <label for="physical_address_three">Physical Address 3</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="physical_city" value="{{$venture->physical_city}}" type="text" class="validate">
                                <label for="physical_city">City</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="physical_postal_code" value="{{$venture->physical_postal_code}}" type="text" class="validate">
                                <label for="physical_code">Code</label>
                            </div>
                            {{--<p>
                                <label>
                                    <input type="checkbox"  id="address_check" onclick="isChecked();"/>
                                    <span>Select if physical address is the same as postal address</span>
                                </label>
                            </p>--}}
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="postal_address_one" value="{{$venture->postal_address_one}}" type="text" class="validate"
                                       placeholder="Postal Address 1">
                                <label for="postal_address_1">Postal Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="postal_address_two" value="{{$venture->postal_address_two}}"  type="text" class="validate"
                                       placeholder="Postal Address 2">
                                <label for="postal_address_2">Postal Address 2</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="postal_address_three" value="{{$venture->postal_address_three}}" type="text" class="validate"
                                       placeholder="Postal Address 3">
                                <label for="postal_address_3">Postal Address 3</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="postal_city" value="{{$venture->postal_city}}" type="text" class="validate" placeholder="City">
                                <label for="postal_city">City</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="postal_code" value="{{$venture->postal_code}}" type="text" class="validate" placeholder="postal code">
                                <label for="postal_code">Code</label>
                            </div>

                        </div>
                        <br>
                        <br>
                        <h6 style="padding-left: 50px;"><b>ONBOARDING INFO</b></h6>

                        <div class="row">
                            <div class="input-field col m6">
                                <select id="hub">
                                    {{--<option value="" disabled selected>Choose hub</option>--}}
                                    <option value="ICT" {{$venture->hub=='ICT'?'selected':''}}>ICT</option>
                                    <option value="Industrial"{{$venture->hub=='Industrial'?'selected':''}}>Industrial</option>
                                </select>
                                <label>Hub</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="stage">
                                    {{--<option value="" disabled selected>Choose Stage</option>--}}
                                    <option value="Stage 1"{{$venture->stage=='Stage 1'?'selected':''}}>Stage 1</option>
                                    <option value="Stage 2"{{$venture->stage=='Stage 2'?'selected':''}}>Stage 2</option>
                                    <option value="Stage 3"{{$venture->stage=='Stage 3'?'selected':''}}>Stage 3</option>
                                </select>
                                <label>Stage</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="business_type">
                                    {{--<option value="" disabled selected>Choose option</option>--}}
                                    <option value="Agriculture" {{$venture->business_type=='Agriculture'?'selected':''}}>Agriculture; plantations; rural sectors</option>
                                    <option value="Basic Metal Product" {{$venture->business_type=='Basic Metal Product'?'selected':''}}>Basic Metal Product</option>
                                    <option value="Chemical industries" {{$venture->business_type=='Chemical industries'?'selected':''}}>Chemical industries</option>
                                    <option value="Commerce" {{$venture->business_type=='Commerce'?'selected':''}}>Commerce</option>
                                    <option value="Construction" {{$venture->business_type=='Construction'?'selected':''}}>Construction</option>
                                    <option value="Education"{{$venture->business_type=='Education'?'selected':''}}>Education</option>
                                    <option value="Financial service"{{$venture->business_type=='Financial service'?'selected':''}}>Financial and professional services</option>
                                    <option value="Food" {{$venture->business_type=='Food'?'selected':''}}>Food; drink; tobacco</option>
                                    <option value="Forestry" {{$venture->business_type=='Forestry'?'selected':''}}>Forestry; wood; pulp and paper</option>
                                    <option value="Health service" {{$venture->business_type=='Health service'?'selected':''}}>Health service</option>
                                    <option value="Touring" {{$venture->business_type=='Touring'?'selected':''}}>Tourism; (hotels; catering)</option>
                                    <option value="Mining" {{$venture->business_type=='Mining'?'selected':''}}>Mining (coal; other mining)</option>
                                    <option value="Mechanical and electrical engineering"{{$venture->business_type=='Mechanical and electrical engineering'?'selected':''}}>Mechanical and electrical
                                        engineering
                                    </option>
                                    <option value="Oil and gas production" {{$venture->business_type=='Oil and gas production'?'selected':''}}>Oil and gas production</option>
                                    <option value="Postal and telecommunications services" {{$venture->business_type=='Postal and telecommunications services'?'selected':''}}>Postal and telecommunications
                                        services
                                    </option>
                                    <option value="Public service" {{$venture->business_type=='Public service'?'selected':''}}>Public service</option>
                                    <option value="Fishing" {{$venture->business_type=='Fishing'?'selected':''}}>Fishing (ports; fisheries; inland waterways)</option>
                                    <option value="Transport" {{$venture->business_type=='Transport'?'selected':''}}>Transport (including civil aviation; railways; road
                                        transport)
                                    </option>
                                    <option value="Clothing" {{$venture->business_type=='Clothing'?'selected':''}}>Clothing (Textiles; clothing; footwear)</option>
                                    <option value="Utilities" {{$venture->business_type=='Utilities'?'selected':''}}>Utilities (waters; gas; electricity)</option>
                                </select>
                                <label>Business-Type</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="smart_city_tags" multiple>
                                    {{--<option value="" disabled selected>Choose Tags</option>--}}
                                    <option
                                        value="Smart Business" {{$venture->smart_city_tags=='Samrt Business'?'selected':''}}>
                                        Smart Business
                                    </option>
                                    <option
                                        value="Smart Community" {{$venture->smart_city_tags=='Samrt Community'?'selected':''}}>
                                        Smart Community
                                    </option>
                                    <option
                                        value="Smart Healthcare" {{$venture->smart_city_tags=='Samrt Healthcare'?'selected':''}}>
                                        Smart Healthcare
                                    </option>
                                    <option
                                        value="Smart Living" {{$venture->smart_city_tags=='Samrt Living'?'selected':''}}>
                                        Smart Living
                                    </option>
                                    <option
                                        value="Smart Mobility" {{$venture->smart_city_tags=='Samrt Mobility'?'selected':''}}>
                                        Smart Mobility
                                    </option>
                                    <option
                                        value="Smart Building" {{$venture->smart_city_tags=='Samrt Building'?'selected':''}}>
                                        Smart Building
                                    </option>
                                    <option
                                        value="Smart Infrastructure" {{$venture->smart_city_tags=='Samrt Infrastructure'?'selected':''}}>
                                        Smart Infrastructure
                                    </option>
                                    <option
                                        value="Smart Governance" {{$venture->smart_city_tags=='Samrt Governance'?'selected':''}}>
                                        Smart Governance
                                    </option>
                                    <option
                                        value="Smart Environment" {{$venture->smart_city_tags=='Samrt Environment'?'selected':''}}>
                                        Smart Environment
                                    </option>
                                </select>
                                <label>Smart City Tags
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="Virtual_or_Physical">
                                    {{--<option value="" disabled selected>Choose option</option>--}}
                                    <option
                                        value="Physical" {{$venture->Virtual_or_Physical=='Physical'?'selected':''}}>
                                        Physical
                                    </option>
                                    <option
                                        value="Virtual" {{$venture->Virtual_or_Physical=='Virtual'?'selected':''}}>
                                        Virtual
                                    </option>
                                </select>
                                <label>Business-Type</label>
                            </div>
                            <div class="input-field col m6">
                                <label for="advisor">Advisor Name</label>
                                <input id="advisor" value="{{$venture->advisor}}" type="text" class="validate">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <label for="mentor">Mentor Name</label>
                                <input id="mentor" value="{{$venture->mentor}}" type="text" class="validate">
                            </div>
                            <div class="input-field col m6">
                                <select id="status" class="input-select">
                                    {{--<option value="" disabled selected>Choose Status</option>--}}
                                    <option value="Active"{{$venture->status=='Active'?'selected':''}}>Active</option>
                                    <option value="Alumni{{$venture->status=='Alumni'?'selected':''}}">Alumni</option>
                                    <option value="Exit"{{$venture->status=='Exit'?'selected':''}}>Exit</option>
                                    <option value="Resigned"{{$venture->status=='Resigned'?'selected':''}}>Resigned</option>
                                </select>
                                <label>Status</label>
                            </div>
                        </div>
                        <div class="row"  id="alumniDate">
                            <div class="input-field col m6">
                                <input id="alumni_date" value="{{$venture->alumni_date}}" type="text" {{--class="datepicker"--}}>
                                <label for="alumni_date">Alumni Date</label>
                            </div>
                        </div>
                        <div class="row"  id="exitDate">
                            <div class="input-field col m6">
                                <input id="exit_date" value="{{$venture->exit_date}}" type="text" {{--class="datepicker"--}}>
                                <label for="exit_date">Exit Date</label>
                            </div>
                        </div>
                        <div class="row"  id="resignDate">
                            <div class="input-field col m6">
                                <input id="resigned_date" value="{{$venture->resigned_date}}" type="text" {{--class="datepicker"--}}>
                                <label for="resigned_date">Resigned Date</label>
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>GOVERNANCE</b></h6>

                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>Client Contract </span>
                                    <input type="file" id="client_contract" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" value="{{isset($venture_upload->client_contract)?$venture_upload->client_contract:''}}" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="input-field col m6">
                                {{--<label for="date_of_signature">Date of Signature</label>--}}
                                <input type="date" id="date_of_signature" value="{{isset($venture_upload->date_of_signature)?$venture_upload->date_of_signature:''}}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>Tax Clearance</span>
                                    <input type="file" id="tax_clearance_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" value="{{isset($venture->ventureUploads->tax_clearance_url)?$venture->ventureUploads->tax_clearance_url:''}}" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="input-field col m6">
                                {{--<label for="date_of_clearance">Date of Tax Clearance</label>--}}
                                <input type="date" id="date_of_clearance_tax" value="{{isset($venture_upload->date_of_clearance_tax)?$venture_upload->date_of_clearance_tax:''}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>BBBEE Certificate</span>
                                    <input type="file" id="bbbee_certificate_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" value="{{isset($venture_upload->bbbee_certificate_url)?$venture_upload->bbbee_certificate_url:''}}" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="input-field col m6">
                                {{--<label for="date_of_bbbee">Date of BBBEE Certificate</label>--}}
                                <input type="date" id="date_of_bbbee" value="{{isset($venture_upload->date_of_bbbee)?$venture_upload->date_of_bbbee:''}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>CK Documents</span>
                                    <input type="file" id="ck_documents_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" value="{{isset($venture_upload->ck_documents_url)?$venture_upload->ck_documents_url:''}}" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>VAT Registration Documents</span>
                                    <input type="file" id="vat_registration_number_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" value="{{isset($venture_upload->vat_registration_number_url)?$venture_upload->vat_registration_number_url:''}}" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>CIPC Document</span>
                                    <input type="file" id="cipc_document_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" value="{{isset($venture_upload->cipc_document_url)?$venture_upload->cipc_document_url:''}}" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>Management Account</span>
                                    <input type="file" id="management_account_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" value="{{isset($venture_upload->management_account_url)?$venture_upload->management_account_url:''}}" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>Dormant Affidavit</span>
                                    <input type="file" id="dormant_affidavit_url" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" value="{{isset($venture_upload->dormant_affidavit_url)?$venture_upload->dormant_affidavit_url:''}}" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row" id="precinct_info">
                            <h6 style="padding-left: 50px;"><b>PRECINCT INFO</b></h6>

                            <div class="input-field col m6">
                                <input id="pricinct_telephone_code" value="{{$venture->pricinct_telephone_code}}" type="text" class="validate">
                                <label for="pricinct_telephone_code">Telephone code</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="pricinct_keys">
                                    <option value="" disabled selected>Do You Have Keys</option>
                                    <option value="Yes"{{$venture->pricinct_keys=='Yes'?'selected':''}}>Yes</option>
                                    <option value="No"{{$venture->pricinct_keys=='No'?'selected':''}}>No</option>
                                </select>
                                <label>Keys</label>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="pricinct_office_position" value="{{$venture->pricinct_office_position}}" type="text" class="validate">
                                    <label for="pricinct_office_position">Office Position</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="pricinct_printing_code" value="{{$venture->pricinct_printing_code}}"  type="text" class="validate">
                                    <label for="pricinct_printing_code">Printing Code</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="pricinct_warehouse_position" value="{{$venture->pricinct_warehouse_position}}" type="text" class="validate">
                                    <label for="pricinct_warehouse_position">Warehouse Position</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="cohort" value="{{$venture->cohort}}" type="text" class="validate">
                                <label for="cohort">Cohort</label>
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>VENTURE FUNDING</b></h6>
                        <br>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="venture_funding">
                                    <option value="" disabled selected>Who is Funding Your Venture?</option>
                                    <option value="TIA Seed Fund"{{$venture->venture_funding=='TIA Seed Fund'?'selected':''}}>TIA Seed Fund</option>
                                    <option value="Propella BDS"{{$venture->venture_funding=='Propella BDS'?'selected':''}}>Propella BDS</option>
                                </select>
                                <label>Venture Funding</label>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="date_awarded" value="{{$venture->date_awarded}}" type="date" {{--class="datepicker"--}}>
                                <label for="date_awarded">Date Awarded</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="fund_value" value="{{$venture->fund_value}}" type="text" class="validate">
                                <label for="fund_value">Amount</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="date_closedout" value="{{$venture->date_closedout}}" type="text" {{--class="datepicker"--}}>
                                <label for="date_closedout">Due Date</label>
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>RENT TURNOVER</b></h6>
                        <br>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="rent_turnover" form="start_details_form" class="inputs-select">
                                    <option value="" disabled selected>Deposited Rent?</option>
                                    <option value="Yes"{{$venture->rent_turnover=='Yes'?'selected':''}}>Yes</option>
                                    <option value="No"{{$venture->rent_turnover=='No'?'selected':''}}>No</option>
                                </select>
                                <label>Rent Turnover</label>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="input-field col m6" id="rentDate">
                                <input id="rent_date" value="{{$venture->rent_date}}" type="date" {{--class="datepicker"--}}>
                                <label for="rent_date">Due Date</label>
                            </div>
                            <div class="input-field col m6" id="rentAmount">
                                <input id="rent_amount" value="{{$venture->rent_amount}}" type="date" {{--class="validate"--}} placeholder="Amount">
                                <label for="rent_amount">Amount</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('venture')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-startup-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="test2" class="col s12">
                    <form id="venture-media-update-form" class="col s12" style="margin-top:1em;" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <input value="{{$venture->id}}" id="incubatee_id" hidden>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Profile Picture</span>
                                        <input id="profile_picture_url" type="file" name="profile_picture_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($venture_upload->profile_picture_url)?$venture_upload->profile_picture_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <img src="" id="profile-picture-preview">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <label for="elevator_pitch">Elevator Pitch</label>
                                <textarea id="elevator_pitch" name="elevator_pitch"
                                          class="materialize-textarea">{{$venture->elevator_pitch}}</textarea>
                            </div>
                            <div class="input-field col m6">
                                <textarea id="venture_profile_information"
                                          class="materialize-textarea">{{$venture->venture_profile_information}}</textarea>
                                <label for="venture_profile_information">Venture Profile Information</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="business_facebook" type="text" value="{{$venture->business_facebook_url}}"
                                       class="validate">
                                <label for="business_facebook">Facebook</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="business_linkedIn" type="text" value="{{$venture->business_linkedIn_url}}"
                                       class="validate">
                                <label for="business_linkedIn">LinkedIn</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="business_twitter" type="text" value="{{$venture->business_twitter_url}}"
                                       class="validate">
                                <label for="business_twitter">Twitter</label>
                            </div>

                            <div class="input-field col m6">
                                <input id="business_instagram" type="text"
                                       value="{{$venture->business_instagram_url}}" class="validate">
                                <label for="business_instagram">Instagram</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <label for="company_website"> Company Website</label>
                                <input id="company_website" type="text" value="{{$venture->company_website}}"
                                       class="validate">
                            </div>
                            <div class="input-field col m6">
                                <label for="venture_email">Venture Email</label>
                                <input id="venture_email" value="{{$venture->venture_email}}" type="text" class="validate">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Company Logo</span>
                                        <input id="company_logo_url" type="file" name="company_logo_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($venture_upload->company_logo_url)?$venture_upload->company_logo_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col m6">
                                <img src="" id="company-logo-preview">
                            </div>


                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="input-field" style="bottom:0px!important;">
                                    <input id="video_shot_url"
                                           value="{{isset($venture_upload->video_shot_url)?$venture_upload->video_shot_url:''}}"
                                           type="text">
                                    <label for="video-shot">Video Shot</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Infographic</span>
                                        <input id="infographic_url" type="file" name="infographic_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($venture_upload->infographic_url)?$venture_upload->infographic_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col m6">
                                <img src="" id="infographic-preview">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Crowdfunding</span>
                                        <input id="crowdfunding_url" type="file" name="crowdfunding_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($venture_upload->crowdfunding_url)?$venture_upload->crowdfunding_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col m6">
                                <img src="" id="crowdfunding-preview">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Product Shots</span>
                                        <input id="product_shot_url" type="file" name="product-shot">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($venture_upload->product_shot_url)?$venture_upload->product_shot_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <img src="" id="product-shot-preview">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('venture')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-media" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        .tabs .tab a:hover, .tabs .tab a.active {
            color: black !important;
            font-weight: bolder;
        }

        .tabs .tab a {
            color: black !important;
        }
    </style>
    @push('custom-scripts')
        <script>

            $(document).ready(function () {
                $("select").formSelect();
                $('.tooltipped').tooltip();


                $('#short_bio').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 75) {
                        alert('Word Limit Reached!');
                    }
                });$('#elevator_pitch').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 50) {
                        alert('Word Limit Reached!');
                    }
                });$('#venture_profile_information').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 100) {
                        alert('Word Limit Reached!');
                    }
                });


                //Style block where profile picture is loaded
                function profilePictureIsLoaded(e) {
                    $("#profile_picture_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#profile-picture-preview').attr('src', e.target.result);
                    $('#profile-picture-preview').attr('width', '250px');
                    $('#profile-picture-preview').attr('height', '230px');
                }

                //Style block where company logo is loaded
                function companyLogoIsLoaded(e) {
                    $("#company_logo_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#company-logo-preview').attr('src', e.target.result);
                    $('#company-logo-preview').attr('width', '250px');
                    $('#company-logo-preview').attr('height', '230px');
                }

                //Style block where video shot is loaded
                function videoShotIsLoaded(e) {
                    $("#video-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#video-shot-preview').attr('src', e.target.result);
                    $('#video-shot-preview').attr('width', '250px');
                    $('#video-shot-preview').attr('height', '230px');
                }

                //Style block where infographic is loaded
                function infographicIsLoaded(e) {
                    $("#infographic_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#infographic-preview').attr('src', e.target.result);
                    $('#infographic-preview').attr('width', '250px');
                    $('#infographic-preview').attr('height', '230px');
                }

                //Style block where crowdfunding is loaded
                function crowdfundingIsLoaded(e) {
                    $("#crowdfunding_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#crowdfunding-preview').attr('src', e.target.result);
                    $('#crowdfunding-preview').attr('width', '250px');
                    $('#crowdfunding-preview').attr('height', '230px');
                }

                //Style block where product shot is loaded
                function productShotIsLoaded(e) {
                    $("#product-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#product-shot-preview').attr('src', e.target.result);
                    $('#product-shot-preview').attr('width', '250px');
                    $('#product-shot-preview').attr('height', '230px');
                }

                // Function to preview profile picture after validation
                $(function () {
                    $("#profile_picture_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#profile-picture-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = profilePictureIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview company logo after validation
                $(function () {
                    $("#company_logo_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#company-logo-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = companyLogoIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview video shot after validation
                $(function () {
                    $("#video-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#video-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = videoShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview infographic after validation
                $(function () {
                    $("#infographic_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#infographic-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = infographicIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview crowdfunding after validation
                $(function () {
                    $("#crowdfunding_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#crowdfunding-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = crowdfundingIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview product shot after validation
                $(function () {
                    $("#product-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#product-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = productShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });

                $(".input-select").on("change", checkSelect);
                $("#alumniDate").hide();
                $("#exitDate").hide();
                $("#resignDate").hide();
                function checkSelect() {
                    if ($('#status').val() == 'Alumni') {
                        $("#alumniDate").show();
                        $("#exitDate").hide();
                        $("#resignDate").hide();
                    }
                    if ($('#status').val() == 'Exit') {
                        $("#exitDate").show();
                        $("#alumniDate").hide();
                        $("#resignDate").hide();
                    }
                    if ($('#status').val() == 'Resigned') {
                        $("#resignDate").show();
                        $("#alumniDate").hide();
                        $("#exitDate").hide();
                    }
                    if ($('#status').val() == 'Active') {
                        $("#alumniDate").hide();
                        $("#exitDate").hide();
                        $("#resignDate").hide();
                    }
                }
                $("#rent_amount").val() == "";
                $("#rent_date").val() == "";
                $("#rentDate").show();
                $("#rentAmount").show();
                let now = new Date();
                let day = ("0" + now.getDate()).slice(-2);
                let month = ("0" + (now.getMonth() + 1)).slice(-2);
                let today = now.getFullYear()+"-"+(month)+"-"+(day) ;

                $(".inputs-select").on("change", rentSelect)
                function rentSelect() {
                    if ($('#rent_turnover').val() == 'No') {
                        $("#rentDate").show();
                        $("#rent_date").val(today).show();
                        $("#rentAmount").show();
                        $("#rent_amount").val() ==  0;
                    }
                    if ($('#rent_turnover').val() == 'Yes') {
                        $("#rentDate").show();
                        $("#rentAmount").show();
                        $("#rent_amount").val() == "";
                        $("#rent_date").val() == "";

                    }

                }


                $('#start_details__update_form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();
                    formData.append('company_name', $('#company_name').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('physical_address_one', $('#physical_address_one').val());
                    formData.append('physical_address_two', $('#physical_address_two').val());
                    formData.append('physical_address_three', $('#physical_address_three').val());
                    formData.append('physical_city', $('#physical_city').val());
                    formData.append('physical_postal_code', $('#physical_postal_code').val());
                    formData.append('postal_address_one', $('#postal_address_one').val());
                    formData.append('postal_address_two', $('#postal_address_two').val());
                    formData.append('postal_address_three', $('#postal_address_three').val());
                    formData.append('postal_city', $('#postal_city').val());
                    formData.append('postal_code', $('#postal_code').val());
                    formData.append('hub', $('#hub').val());
                    formData.append('smart_city_tags', $('#smart_city_tags').val());
                    formData.append('business_type', $('#business_type').val());
                    formData.append('cohort', $('#cohort').val());
                    formData.append('stage', $('#stage').val());
                    formData.append('virtual_or_physical', $('#virtual_or_physical').val());
                    formData.append('ownership', $('#ownership').val());
                    formData.append('registration_number', $('#registration_number').val());
                    formData.append('BBBEE_level', $('#BBBEE_Level').val());
                    formData.append('date_of_signature', $('#date_of_signature').val());
                    formData.append('date_of_clearance_tax', $('#date_of_clearance_tax').val());
                    formData.append('date_of_bbbee', $('#date_of_bbbee').val());
                    formData.append('pricinct_telephone_code', $('#pricinct_telephone_code').val());
                    formData.append('pricinct_keys', $('#pricinct_keys').val());
                    formData.append('pricinct_office_position', $('#pricinct_office_position').val());
                    formData.append('pricinct_printing_code', $('#pricinct_printing_code').val());
                    formData.append('pricinct_warehouse_position', $('#pricinct_warehouse_position').val());
                    formData.append('rent_turnover', $('#rent_turnover').val());
                    formData.append('rent_date', $('#rent_date').val());
                    formData.append('rent_amount', $('#rent_amount').val());
                    formData.append('status', $('#status').val());
                    formData.append('alumni_date', $('#alumni_date').val());
                    formData.append('exit_date', $('#exit_date').val());
                    formData.append('resigned_date', $('#resigned_date').val());
                    formData.append('mentor', $('#mentor').val());
                    formData.append('advisor', $('#advisor').val());
                    formData.append('venture_funding', $('#venture_funding').val());
                    formData.append('date_awarded', $('#date_awarded').val());
                    formData.append('fund_value', $('#fund_value').val());
                    formData.append('date_closedout', $('#date_closedout').val());
                    formData.append('venture_type', $('#venture_type').val());

                    jQuery.each(jQuery('#client_contract')[0].files, function (i, file) {
                        formData.append('client_contract', file);
                    });

                    jQuery.each(jQuery('#tax_clearance_url')[0].files, function (i, file) {
                        formData.append('tax_clearance_url', file);
                    });
                    jQuery.each(jQuery('#bbbee_certificate_url')[0].files, function (i, file) {
                        formData.append('bbbee_certificate_url', file);
                    });
                    jQuery.each(jQuery('#ck_documents_url')[0].files, function (i, file) {
                        formData.append('ck_documents_url', file);
                    });
                    jQuery.each(jQuery('#vat_registration_number_url')[0].files, function (i, file) {
                        formData.append('vat_registration_number_url', file);
                    });

                    jQuery.each(jQuery('#cipc_document_url')[0].files, function (i, file) {
                        formData.append('cipc_document_url', file);
                    });
                    jQuery.each(jQuery('#management_account_url')[0].files, function (i, file) {
                        formData.append('management_account_url', file);
                    });
                    jQuery.each(jQuery('#dormant_affidavit_url')[0].files, function (i, file) {
                        formData.append('dormant_affidavit_url', file);
                    });


                    let url = '/venture-update/' + '{{$venture->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            console.log("success", response);
                            alert(response.message);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });

                $('#venture-media-update-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();
                    formData.append('video_shot_url', $('#video_shot_url').val());
                    formData.append('company_website', $('#company_website').val());
                    formData.append('elevator_pitch', $('#elevator_pitch').val());
                    formData.append('venture_profile_information', $('#venture_profile_information').val());
                    formData.append('venture_email', $('#venture_email').val());
                    formData.append('business_facebook_url', $('#business_facebook').val());
                    formData.append('business_twitter_url', $('#business_twitter').val());
                    formData.append('business_linkedIn_url', $('#business_linkedIn').val());
                    formData.append('business_instagram_url', $('#business_instagram').val());

                    jQuery.each(jQuery('#profile_picture_url')[0].files, function (i, file) {
                        formData.append('profile_picture_url', file);
                    });

                    jQuery.each(jQuery('#company_logo_url')[0].files, function (i, file) {
                        formData.append('company_logo_url', file);
                    });

                    jQuery.each(jQuery('#infographic_url')[0].files, function (i, file) {
                        formData.append('infographic_url', file);
                    });

                    jQuery.each(jQuery('#crowdfunding_url')[0].files, function (i, file) {
                        formData.append('crowdfunding_url', file);
                    });

                    jQuery.each(jQuery('#product_shot_url')[0].files, function (i, file) {
                        formData.append('product_shot_url', file);
                    });


                    let url = '/venture-update-media/' + '{{$venture->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            console.log("success", response);
                            alert(response.message);
                            sessionStorage.clear();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
