@extends('layouts.clerk-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <div class="container">

        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Edit
                Incubatee</h6>
            <div class="row card hoverable">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s4"><a href="#test1" class="active">Personal Details</a></li>
                        <li class="tab col s4"><a href="#test2">Start Up Details</a></li>
                        <li class="tab col s4"><a href="#test3">Media</a></li>
                    </ul>
                </div>
                <div id="test1" class="col s12">
                    <form id="incubatee-details-update-form" method="post" class="col s12" style="margin-top:1em;"
                          enctype="multipart/form-data">
                        @csrf
                        <input value="{{$current_user->id}}" id="incubatee_id" hidden>
                        <h5>Personal Details</h5>

                        <div class="row">
                            <div class="input-field col m6">
                                <select id="title">
                                    <option value="" disabled selected>Choose Title</option>

                                    <option value="Mr" {{$current_user->title=='Mr'?'selected':''}}>Mr</option>
                                    <option value="Mrs"{{$current_user->title=='Mrs'?'selected':''}}>Ms</option>
                                    <option value="Miss"{{$current_user->title=='Miss'?'selected':''}}>Mr</option>
                                    <option value="Ms"{{$current_user->title=='Ms'?'selected':''}}>Ms</option>
                                    <option value="Dr"{{$current_user->title=='Dr'?'selected':''}}>Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="initials" type="text" class="validate" value="{{$current_user->initials}}"
                                       required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="name" type="text" class="validate" value="{{$current_user->name}}" required>
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="surname" type="text" class="validate" value="{{$current_user->surname}}"
                                       required>
                                <label for="surname">Surname</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="id_number" type="text" class="validate" value="{{$current_user->id_number}}"
                                       required>
                                <label for="id_number">ID Number</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="dob" type="date" class="validate"
                                       value="{{$current_user->dob/*->toDateString()*/}}" required>
                                <label for="dob">Date of Birth</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="age" type="text" class="validate"value="{{$current_user->age}}" placeholder="Age" required>
                                {{--<label for="age">Age</label>--}}
                            </div>
                            <div class="input-field col m6">
                                <select id="gender">
                                    <option value="" disabled selected>Choose Gender</option>

                                    <option value="Male" {{$current_user->gender=='Male'?'selected':''}}>Male</option>
                                    <option value="Female" {{$current_user->gender=='Female'?'selected':''}}>Female</option>
                                </select>
                                <label>Gender</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="email" type="email" class="validate" value="{{$current_user->email}}"
                                       required>
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="tel" class="validate"
                                       value="{{$current_user->contact_number}}" required>
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_facebook" type="text" value="{{$incubatee->personal_facebook_url}}"
                                       class="validate">
                                <label for="personal_facebook">Facebook</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_linked_in" type="text" value="{{$incubatee->personal_linkedIn_url}}"
                                       class="validate">
                                <label for="personal_linked_in">LinkedIn</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_twitter" type="text" value="{{$incubatee->personal_twitter_url}}"
                                       class="validate">
                                <label for="personal_twitter">Twitter</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_instagram" type="text"
                                       value="{{$incubatee->personal_instagram_url}}" class="validate">
                                <label for="personal_instagram">Instagram</label>
                            </div>
                        </div>
                        <div class="row">

                        </div>

                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-personal-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="test2" class="col s12">
                    <form id="incubatee-business-update-form" method="post" class="col s12" style="margin-top:1em;">
                        @csrf
                        <input value="{{$current_user->id}}" id="incubatee_id" hidden>
                        <h6 style="padding-left: 50px;"><b>START UP DETAILS</b></h6>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="startup_name" type="text" value="{{$incubatee->startup_name}}"
                                       class="validate">
                                <label for="startup_name">Startup Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="text" value="{{$incubatee->contact_number}}"
                                       class="validate">
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="ownership" type="text"
                                       value="{{isset($incubatee_ownership->ownership)?$incubatee_ownership->ownership:''}}"
                                       class="validate">
                                <label for="ownership">Ownership</label>
                            </div>

                            <div class="input-field col m6">
                                <input id="registration_number" type="text"
                                       value="{{isset($incubatee_ownership->registration_number)?$incubatee_ownership->registration_number:''}}"
                                       class="validate">
                                <label for="registration_number">Registration Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="BBBEE_Level">
                                    <option value="" disabled selected>Choose BBBEE Level</option>
                                    <option
                                        value="1" {{isset($incubatee_ownership->BBBEE_level)=='1'?$incubatee_ownership->BBBEE_level:''}}>
                                        Level 01
                                    </option>
                                    <option
                                        value="2" {{isset($incubatee_ownership->BBBEE_level)=='2'?$incubatee_ownership->BBBEE_level:''}}>
                                        Level 02
                                    </option>
                                    <option
                                        value="3" {{isset($incubatee_ownership->BBBEE_level)=='3'?$incubatee_ownership->BBBEE_level:''}}>
                                        Level 03
                                    </option>
                                    <option
                                        value="4" {{isset($incubatee_ownership->BBBEE_level)=='4'?$incubatee_ownership->BBBEE_level:''}}>
                                        Level 04
                                    </option>
                                    <option
                                        value="5" {{isset($incubatee_ownership->BBBEE_level)=='5'?$incubatee_ownership->BBBEE_level:''}}>
                                        Level 05
                                    </option>
                                    <option
                                        value="6" {{isset($incubatee_ownership->BBBEE_level)=='6'?$incubatee_ownership->BBBEE_level:''}}>
                                        Level 06
                                    </option>
                                    <option
                                        value="7" {{isset($incubatee_ownership->BBBEE_level)=='7'?$incubatee_ownership->BBBEE_level:''}}>
                                        Level 07
                                    </option>
                                </select>
                            </div>
                            <div class="input-field col m6">
                                <label for="short_bio">Short Biography</label>
                                <textarea id="short_bio" name="short_bio"
                                          class="materialize-textarea">{{$incubatee->short_bio}}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <label for="elevator_pitch">Elevator Pitch</label>
                                <textarea id="elevator_pitch" name="elevator_pitch"
                                          class="materialize-textarea">{{$incubatee->elevator_pitch}}</textarea>
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>ADDRESS DETAILS</b></h6>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="physical_address_1" type="text" value="{{$incubatee->physical_address_1}}"
                                       class="validate">
                                <label for="physical_address_1">Physical Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="physical_address_2" type="text" value="{{$incubatee->physical_address_2}}"
                                       class="validate">
                                <label for="physical_address_2">Physical Address 2</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="physical_address_3" type="text" value="{{$incubatee->physical_address_3}}"
                                       class="validate">
                                <label for="physical_address_3">Physical Address 3</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="physical_city" type="text" value="{{$incubatee->physical_city}}"
                                       class="validate">
                                <label for="physical_city">City</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="physical_code" type="text" value="{{$incubatee->physical_code}}"
                                       class="validate">
                                <label for="physical_code">Code</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="postal_address_1" type="text" value="{{$incubatee->postal_address_1}}"
                                       class="validate">
                                <label for="postal_address_1">Postal Address 1</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="postal_address_2" type="text" value="{{$incubatee->postal_address_2}}"
                                       class="validate">
                                <label for="postal_address_2">Postal Address 2</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="postal_address_3" type="text" value="{{$incubatee->postal_address_3}}"
                                       class="validate">
                                <label for="postal_address_3">Postal Address 3</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="postal_city" type="text" value="{{$incubatee->postal_city}}"
                                       class="validate">
                                <label for="postal_city">City</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="postal_code" type="text" value="{{$incubatee->postal_code}}"
                                       class="validate">
                                <label for="postal_code">Code</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <label for="company_website"> Company Website</label>
                                <input id="company_website" type="text" value="{{$incubatee->company_website}}"
                                       class="validate">
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>ONBOARDING INFO</b></h6>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="hub">
                                    <option value="" disabled selected>Choose hub</option>
                                    <option value="ICT" {{$incubatee->hub=='ICT'?'selected':''}}>ICT</option>
                                    <option value="Industrial" {{$incubatee->hub=='Industrial'?'selected':''}}>
                                        Industrial
                                    </option>
                                </select>
                                <label>Hub</label>
                            </div>

                            <div class="input-field col m6">
                                <select id="stage">
                                    <option value="" disabled selected>Choose Stage</option>
                                    <option value="Stage 1" {{$incubatee->stage=='Stage 1'?'selected':''}}>Stage 1
                                    </option>
                                    <option value="Stage 2" {{$incubatee->stage=='Stage 2'?'selected':''}}>Stage 2
                                    </option>
                                    <option value="Stage 3" {{$incubatee->stage=='Stage 3'?'selected':''}}>Stage 3
                                    </option>
                                </select>
                                <label>Stage</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="b_type">
                                    <option value="" disabled selected>Choose option</option>
                                    <option value="B2B" {{$incubatee->b_type=='B2B'?'selected':''}}>B2B</option>
                                    <option value="B2C" {{$incubatee->b_type=='B2C'?'selected':''}}>B2C</option>
                                </select>
                                <label>Business-Type</label>
                            </div>

                            <div class="input-field col m6">
                                <select id="smart_city_tags" multiple>
                                    <option value="" disabled selected>Choose Tags</option>
                                    <option
                                        value="Smart Business" {{$incubatee->smart_city_tags=='Samrt Business'?'selected':''}}>
                                        Smart Business
                                    </option>
                                    <option
                                        value="Smart Community" {{$incubatee->smart_city_tags=='Samrt Community'?'selected':''}}>
                                        Smart Community
                                    </option>
                                    <option
                                        value="Smart Healthcare" {{$incubatee->smart_city_tags=='Samrt Healthcare'?'selected':''}}>
                                        Smart Healthcare
                                    </option>
                                    <option
                                        value="Smart Living" {{$incubatee->smart_city_tags=='Samrt Living'?'selected':''}}>
                                        Smart Living
                                    </option>
                                    <option
                                        value="Smart Mobility" {{$incubatee->smart_city_tags=='Samrt Mobility'?'selected':''}}>
                                        Smart Mobility
                                    </option>
                                    <option
                                        value="Smart Building" {{$incubatee->smart_city_tags=='Samrt Building'?'selected':''}}>
                                        Smart Building
                                    </option>
                                    <option
                                        value="Smart Infrastructure" {{$incubatee->smart_city_tags=='Samrt Infrastructure'?'selected':''}}>
                                        Smart Infrastructure
                                    </option>
                                    <option
                                        value="Smart Governance" {{$incubatee->smart_city_tags=='Samrt Governance'?'selected':''}}>
                                        Smart Governance
                                    </option>
                                    <option
                                        value="Smart Environment" {{$incubatee->smart_city_tags=='Samrt Environment'?'selected':''}}>
                                        Smart Environment
                                    </option>
                                </select>
                                <label>Smart City Tags
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="Virtual_or_Physical">
                                    <option value="" disabled selected>Choose option</option>
                                    <option
                                        value="Physical" {{$incubatee->Virtual_or_Physical=='Physical'?'selected':''}}>
                                        Physical
                                    </option>
                                    <option
                                        value="Virtual" {{$incubatee->Virtual_or_Physical=='Virtual'?'selected':''}}>
                                        Virtual
                                    </option>
                                </select>
                                <label>Business-Type</label>
                            </div>
                            <div class="input-field col m6">
                                <label for="advisor_name">Advisor Name</label>
                                <input id="advisor_name" type="text" value="{{$incubatee->company_website}}"
                                       class="validate">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <label for="mentor_name">Mentor Name</label>
                                <input id="mentor_name" type="text" value="{{$incubatee->mentor_name}}"
                                       class="validate">
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>GOVERNANCE</b></h6>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>CLIENT CONTRACT</span>
                                        <input id="client_contract" type="file" name="client_contract">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee->client_contract)?$incubatee->client_contract:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col m6">
                                <input id="date_of_signature" type="date"
                                       value="{{$incubatee->date_of_signature/*->toDateString()*/}}"
                                       class="validate">
                                <label for="date_of_signature">Signature of Signature</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>COPY OF ID DOCUMENT</span>
                                        <input id="id_document" type="file" name="id_document">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee->id_document)?$incubatee->id_document:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>TAX CLEARANCE</span>
                                        <input id="tax_clearance" type="file" name="tax_clearance">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee->tax_clearance)?$incubatee->tax_clearance:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col m6">
                                <input id="date_of_clearance" type="date"
                                       value="{{$incubatee->date_of_clearance/*->toDateString()*/}}"
                                       class="validate">
                                <label for="date_of_clearance">Date of Tax Clearance</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>BBBEE CERTIFICATE</span>
                                        <input id="bbbee_certificate" type="file" name="bbbee_certificate">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee->bbbee_certificate)?$incubatee->bbbee_certificate:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col m6">
                                <input id="date_of_bbbee" type="date"
                                       value="{{$incubatee->date_of_bbbee/*->toDateString()*/}}" class="validate">
                                <label for="date_of_bbbee">Date of Tax BBBEE</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>CK DOCUMENT</span>
                                        <input id="ck_document" type="file" name="ck_document">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee->ck_document)?$incubatee->ck_document:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>VAT REGISTRATION DOCUMENTS</span>
                                        <input id="vat_reg" type="file" name="vat_reg">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee->vat_reg)?$incubatee->vat_reg:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row" id="precinct_info">
                            <h6 style="padding-left: 50px;"><b>PRECINCT INFO</b></h6>

                            <div class="input-field col m6">
                                <input id="telephone_code" value="{{$incubatee->telephone_code}}" type="text"
                                       class="validate">
                                <label for="telephone_code">Telephone code</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="precinct_info">
                                    <option value="" disabled selected>Do You Have Keys</option>
                                    <option value="Yes"{{$incubatee->precinct_info=='Yes'?'selected':''}}>Yes</option>
                                    <option value="No"{{$incubatee->precinct_info=='No'?'selected':''}}>No</option>
                                </select>
                                <label>Keys</label>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="office_position" type="text" value="{{$incubatee->office_position}}"
                                           class="validate">
                                    <label for="office_position">Office Position</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="printing_code" type="text" value="{{$incubatee->printing_code}}"
                                           class="validate">
                                    <label for="printing_code">Printing Code</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="warehouse_position" type="text"
                                           value="{{$incubatee->warehouse_position}}" class="validate">
                                    <label for="warehouse_position">Warehouse Position</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">


                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="business_facebook" type="text" value="{{$incubatee->business_facebook_url}}"
                                       class="validate">
                                <label for="business_facebook">Facebook</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="business_linkedIn" type="text" value="{{$incubatee->business_linkedIn_url}}"
                                       class="validate">
                                <label for="business_linkedIn">LinkedIn</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="business_twitter" type="text" value="{{$incubatee->business_twitter_url}}"
                                       class="validate">
                                <label for="business_twitter">Twitter</label>
                            </div>

                            <div class="input-field col m6">
                                <input id="business_instagram" type="text"
                                       value="{{$incubatee->business_instagram_url}}" class="validate">
                                <label for="business_instagram">Instagram</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m6">
                                <input id="cohort" type="text" value="{{$incubatee->cohort}}" class="validate">
                                <label for="cohort">Cohort</label>
                            </div>

                            <div class="input-field col m6">
                                <textarea id="venture_profile_information"
                                          class="materialize-textarea">{{$incubatee->venture_profile_information}}</textarea>
                                <label for="venture_profile_information">Venture Profile Information</label>
                            </div>
                        </div>


                        {{--<div class="row">

                            <div class="input-field col m6">
                                <label for="elevator_pitch">Elevator Pitch</label>
                                <textarea id="elevator_pitch" name="elevator_pitch"
                                          class="materialize-textarea">{{$incubatee->elevator_pitch}}</textarea>
                            </div>
                        </div>--}}


                        <div class="row">
                            <div class="col offset-m4">
                                <a
                                    class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-startup-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>


                <div id="test3" class="col s12">
                    <form id="incubatee-media-update-form" class="col s12" style="margin-top:1em;" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <input value="{{$current_user->id}}" id="incubatee_id" hidden>
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Profile Picture</span>
                                        <input id="profile_picture_url" type="file" name="profile_picture_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->profile_picture_url)?$incubatee_uploads->profile_picture_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <img src="" id="profile-picture-preview">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Company Logo</span>
                                        <input id="company_logo_url" type="file" name="company_logo_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->company_logo_url)?$incubatee_uploads->company_logo_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col m6">
                                <img src="" id="company-logo-preview">
                            </div>


                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="input-field" style="bottom:0px!important;">
                                    <input id="video-shot"
                                           value="{{isset($incubatee_uploads->video_shot_url)?$incubatee_uploads->video_shot_url:''}}"
                                           type="text">
                                    <label for="video-shot">Video Shot</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Infographic</span>
                                        <input id="infographic_url" type="file" name="infographic_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->infographic_url)?$incubatee_uploads->infographic_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col m6">
                                <img src="" id="infographic-preview">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Crowdfunding</span>
                                        <input id="crowdfunding_url" type="file" name="crowdfunding_url">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->crowdfunding_url)?$incubatee_uploads->crowdfunding_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col m6">
                                <img src="" id="crowdfunding-preview">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Product Shots</span>
                                        <input id="product-shot" type="file" name="product-shot">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path"
                                               value="{{isset($incubatee_uploads->product_shot_url)?$incubatee_uploads->product_shot_url:''}}"
                                               type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col m6">
                                <img src="" id="product-shot-preview">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-media" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    </div>
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add Incubatee Employee" href="{{url('/users/incubatees/admin-job/create')}}">
            <i class="large material-icons">business_center</i>
        </a>

    </div>

    <style>
        .tabs .tab a:hover, .tabs .tab a.active {
            color: black !important;
            font-weight: bolder;
        }

        .tabs .tab a {
            color: black !important;
        }
    </style>
    @push('custom-scripts')
        <script>

            $(document).ready(function () {
                $("select").formSelect();
                $('.tooltipped').tooltip();

                $('#short_bio').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 75) {
                        alert('Word Limit Reached!');
                    }
                });$('#elevator_pitch').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 50) {
                        alert('Word Limit Reached!');
                    }
                });$('#venture_profile_information').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 100) {
                        alert('Word Limit Reached!');
                    }
                });


                //Style block where profile picture is loaded
                function profilePictureIsLoaded(e) {
                    $("#profile_picture_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#profile-picture-preview').attr('src', e.target.result);
                    $('#profile-picture-preview').attr('width', '250px');
                    $('#profile-picture-preview').attr('height', '230px');
                }

                //Style block where company logo is loaded
                function companyLogoIsLoaded(e) {
                    $("#company_logo_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#company-logo-preview').attr('src', e.target.result);
                    $('#company-logo-preview').attr('width', '250px');
                    $('#company-logo-preview').attr('height', '230px');
                }

                //Style block where video shot is loaded
                function videoShotIsLoaded(e) {
                    $("#video-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#video-shot-preview').attr('src', e.target.result);
                    $('#video-shot-preview').attr('width', '250px');
                    $('#video-shot-preview').attr('height', '230px');
                }

                //Style block where infographic is loaded
                function infographicIsLoaded(e) {
                    $("#infographic_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#infographic-preview').attr('src', e.target.result);
                    $('#infographic-preview').attr('width', '250px');
                    $('#infographic-preview').attr('height', '230px');
                }

                //Style block where crowdfunding is loaded
                function crowdfundingIsLoaded(e) {
                    $("#crowdfunding_url").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#crowdfunding-preview').attr('src', e.target.result);
                    $('#crowdfunding-preview').attr('width', '250px');
                    $('#crowdfunding-preview').attr('height', '230px');
                }

                //Style block where product shot is loaded
                function productShotIsLoaded(e) {
                    $("#product-shot").css("color", "green");
                    $('#image_preview').css("display", "block");
                    $('#product-shot-preview').attr('src', e.target.result);
                    $('#product-shot-preview').attr('width', '250px');
                    $('#product-shot-preview').attr('height', '230px');
                }

                // Function to preview profile picture after validation
                $(function () {
                    $("#profile_picture_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#profile-picture-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = profilePictureIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview company logo after validation
                $(function () {
                    $("#company_logo_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#company-logo-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = companyLogoIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview video shot after validation
                $(function () {
                    $("#video-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#video-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = videoShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview infographic after validation
                $(function () {
                    $("#infographic_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#infographic-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = infographicIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview crowdfunding after validation
                $(function () {
                    $("#crowdfunding_url").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#crowdfunding-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = crowdfundingIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });


                //Function to preview product shot after validation
                $(function () {
                    $("#product-shot").change(function () {
                        $("#message").empty(); // To remove the previous error message
                        var file = this.files[0];
                        var imagefile = file.type;
                        var match = ["image/jpeg", "image/png", "image/jpg"];
                        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                            $('#product-shot-preview').attr('src', 'noimage.png');
                            $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                            return false;
                        } else {
                            var reader = new FileReader();
                            reader.onload = productShotIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                });

                $("#dob").change(function () {
                    let value = $("#dob").val();
                    let dob = new Date(value);
                    let today = new Date();
                    let age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                    if (isNaN(age)) {
                        // will set 0 when value will be NaN
                        age = 0;
                    } else {
                        age = age;
                    }
                    let ages = $('#age').val(age);
                });

                $('#incubatee-details-update-form').on('submit', function (e) {
                    e.preventDefault();
                    let formData = new FormData();
                    formData.append('title', $('#title').val());
                    formData.append('initials', $('#initials').val());
                    formData.append('dob', $('#dob').val());
                    formData.append('age', $('#age').val());
                    formData.append('gender', $('#gender').val());
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('email', $('#email').val());
                    formData.append('id_number', $('#id_number').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('personal_facebook_url', $('#personal_facebook').val());
                    formData.append('personal_linkedIn_url', $('#personal_linked_in').val());
                    formData.append('personal_twitter_url', $('#personal_twitter').val());
                    formData.append('personal_instagram_url', $('#personal_instagram').val());

                    console.log("user ", formData);


                    let url = '/incubatee-update-personal/' + '{{$current_user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        /**
                         * @param response
                         * @param a
                         * @param b
                         */

                        success: function (response, a, b) {
                            console.log("success", response);
                            alert(response.message);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });

                $('#incubatee-business-update-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();
                    formData.append('startup_name', $('#startup_name').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('physical_address_1', $('#physical_address_1').val());
                    formData.append('physical_address_2', $('#physical_address_2').val());
                    formData.append('physical_address_3', $('#physical_address_3').val());
                    formData.append('physical_city', $('#physical_city').val());
                    formData.append('physical_code', $('#physical_code').val());
                    formData.append('postal_address_1', $('#postal_address_1').val());
                    formData.append('postal_address_2', $('#postal_address_2').val());
                    formData.append('postal_address_3', $('#postal_address_3').val());
                    formData.append('postal_city', $('#postal_city').val());
                    formData.append('postal_code', $('#postal_code').val());
                    formData.append('hub', $('#hub').val());
                    formData.append('smart_city_tags', $('#smart_city_tags').val());
                    formData.append('b_type', $('#b_type').val());
                    formData.append('company_website', $('#company_website').val());
                    formData.append('cohort', $('#cohort').val());
                    formData.append('stage', $('#stage').val());
                    formData.append('Virtual_or_Physical', $('#Virtual_or_Physical').val());
                    formData.append('ownership', $('#ownership').val());
                    formData.append('venture_profile_information', $('#venture_profile_information').val());
                    formData.append('business_facebook_url', $('#business_facebook').val());
                    formData.append('business_twitter_url', $('#business_twitter').val());
                    formData.append('business_linkedIn_url', $('#business_linkedIn').val());
                    formData.append('business_instagram_url', $('#business_instagram').val());
                    formData.append('registration_number', $('#registration_number').val());
                    formData.append('BBBEE_level', $('#BBBEE_Level').val());
                    formData.append('date_of_signature', $('#date_of_signature').val());
                    formData.append('date_of_clearance', $('#date_of_clearance').val());
                    formData.append('date_of_bbbee', $('#date_of_bbbee').val());
                    formData.append('telephone_code', $('#telephone_code').val());
                    formData.append('precinct_info', $('#precinct_info').val());
                    formData.append('office_position', $('#office_position').val());
                    formData.append('printing_code', $('#printing_code').val());
                    formData.append('elevator_pitch', $('#elevator_pitch').val());
                    formData.append('short_bio', $('#short_bio').val());
                    formData.append('warehouse_position', $('#warehouse_position').val());

                    jQuery.each(jQuery('#client_contract')[0].files, function (i, file) {
                        formData.append('client_contract', file);
                    });
                    jQuery.each(jQuery('#id_document')[0].files, function (i, file) {
                        formData.append('id_document', file);
                    });
                    jQuery.each(jQuery('#tax_clearance')[0].files, function (i, file) {
                        formData.append('tax_clearance', file);
                    });
                    jQuery.each(jQuery('#bbbee_certificate')[0].files, function (i, file) {
                        formData.append('bbbee_certificate', file);
                    });
                    jQuery.each(jQuery('#ck_document')[0].files, function (i, file) {
                        formData.append('ck_document', file);
                    });
                    jQuery.each(jQuery('#vat_reg')[0].files, function (i, file) {
                        formData.append('vat_reg', file);
                    });


                    let url = '/incubatee-update-business/' + '{{$current_user->id}}';
                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            console.log("success", response);
                            alert(response.message);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });


                $('#incubatee-media-update-form').on('submit', function (e) {
                    e.preventDefault();

                    let formData = new FormData();
                    formData.append('video_shot_url', $('#video-shot').val());

                    jQuery.each(jQuery('#profile_picture_url')[0].files, function (i, file) {
                        formData.append('profile_picture_url', file);
                    });

                    jQuery.each(jQuery('#company_logo_url')[0].files, function (i, file) {
                        formData.append('company_logo_url', file);
                    });

                    jQuery.each(jQuery('#infographic_url')[0].files, function (i, file) {
                        formData.append('infographic_url', file);
                    });

                    jQuery.each(jQuery('#crowdfunding_url')[0].files, function (i, file) {
                        formData.append('crowdfunding_url', file);
                    });

                    jQuery.each(jQuery('#product-shot')[0].files, function (i, file) {
                        formData.append('product_shot_url', file);
                    });


                    let url = '/incubatee-update-media/' + '{{$current_user->id}}';

                    console.log(url);

                    $.ajax({
                        url: url,
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response, a, b) {
                            console.log("success", response);
                            alert(response.message);
                            sessionStorage.clear();
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
