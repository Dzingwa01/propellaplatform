@extends('layouts.clerk-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <div class="container">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Create
                Incubatee</h6>
            <div class="row card hoverable">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s4"><a href="#test1" class="active">Personal Details</a></li>
                        <li class="tab col s4"><a href="#test2">Start Up Details</a></li>
                        <li class="tab col s4"><a href="#test3">Media</a></li>
                    </ul>
                </div>
                <div id="test1" class="col s12">
                    <form id="incubatee-form" class="col s12" style="margin-top:1em;" enctype="multipart/form-data">
                        @csrf
                        {{--<h5>Personal Details</h5>--}}
                        {{--Additional--}}
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="title">
                                    <option value="" disabled selected>Choose Title</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Ms</option>
                                    <option value="Miss">Mr</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr">Dr</option>
                                </select>
                                <label>Title</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="initials" type="text" class="validate" required>
                                <label for="initials">Initials</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="name" type="text" class="validate" required>
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="surname" type="text" class="validate" required>
                                <label for="surname">Surname</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="id_number" type="text" class="validate" required>
                                <label for="id_number">ID Number</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="dob" type="date" class="validate" required>
                                <label for="dob">Date of Birth</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="age" type="text" class="validate" placeholder="Age" required>
                                {{--<label for="age">Age</label>--}}
                            </div>
                            <div class="input-field col m6">
                                <select id="title">
                                    <option value="" disabled selected>Choose Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                                <label>Gender</label>
                            </div>
                        </div>
                        {{--/Additional--}}

                        <div class="row">
                            <div class="input-field col m6">
                                <input id="email" type="email" class="validate" required>
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="tel" class="validate" required>
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_facebook" type="text" class="validate">
                                <label for="personal_facebook">Facebook</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="personal_linked_in" type="text" class="validate">
                                <label for="personal_linked_in">LinkedIn</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="personal_twitter" type="text" class="validate">
                                <label for="personal_twitter">Twitter</label>
                            </div>

                            <div class="input-field col m6">
                                <input id="personal_instagram" type="text" class="validate">
                                <label for="personal_instagram">Instagram</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-personal-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>


                <div id="test2" class="col s12">
                    <form id="start_details_form" class="col s12" style="margin-top:1em;">
                        @csrf
                        <h6 style="padding-left: 50px;"><b>START UP DETAILS</b></h6>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="startup_name" type="text" class="validate">
                                <label for="startup_name">Startup Name</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="contact_number" type="text" class="validate">
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="ownership" type="text" class="validate">
                                <label for="ownership">Ownership (fill in percentage)</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="registration_number" type="text" class="validate">
                                <label for="registration_number">Registration Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="BBBEE_Level">
                                    <option value="" disabled selected>Choose BBBEE Level</option>
                                    <option value="1"> Level 01</option>
                                    <option value="2"> Level 02</option>
                                    <option value="3"> Level 03</option>
                                    <option value="4"> Level 04</option>
                                    <option value="5"> Level 05</option>
                                    <option value="6"> Level 06</option>
                                    <option value="7"> Level 07</option>
                                </select>
                            </div>
                            <div class="input-field col m6">
                                <label for="short_bio">Short Biography</label>
                                <textarea id="short_bio" name="short_bio" class="materialize-textarea"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <label for="elevator_pitch">Elevator Pitch</label>
                                <textarea id="elevator_pitch" name="elevator_pitch"
                                          class="materialize-textarea"></textarea>
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>ADDRESS DETAILS</b></h6>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="physical_address_1" type="text" class="validate">
                                <label for="physical_address_1">Physical Address 1</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="physical_address_2" type="text" class="validate">
                                <label for="physical_address_2">Physical Address 2</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="physical_address_3" type="text" class="validate">
                                <label for="physical_address_3">Physical Address 3</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="physical_city" type="text" class="validate">
                                <label for="physical_city">City</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="physical_code" type="text" class="validate">
                                <label for="physical_code">Code</label>
                            </div>
                            <p>
                                <label>
                                    <input type="checkbox" id="address_check" {{--onclick="isChecked();"--}}/>
                                    <span>Select if physical address is the same as postal address</span>
                                </label>
                            </p>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="postal_address_1" type="text" class="validate"
                                       placeholder="Postal Address 1">
                                {{--<label for="postal_address_1">Postal Address 1</label>--}}
                            </div>
                            <div class="input-field col m6">
                                <input id="postal_address_2" type="text" class="validate"
                                       placeholder="Postal Address 2">
                                {{--<label for="postal_address_2">Postal Address 2</label>--}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="postal_address_3" type="text" class="validate"
                                       placeholder="Postal Address 3">
                                {{--<label for="postal_address_3">Postal Address 3</label>--}}
                            </div>
                            <div class="input-field col m6">
                                <input id="postal_city" type="text" class="validate" placeholder="City">
                                {{--<label for="postal_city">City</label>--}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="postal_code" type="text" class="validate" placeholder="postal code">
                                {{--<label for="postal_code">Code</label>--}}
                            </div>
                            <div class="input-field col m6">
                                <label for="company_website"> Company Website</label>
                                <input id="company_website" type="text" class="validate">
                            </div>
                        </div>
                        <br>
                        <br>
                        <h6 style="padding-left: 50px;"><b>ONBOARDING INFO</b></h6>

                        <div class="row">
                            <div class="input-field col m6">
                                <select id="hub">
                                    <option value="" disabled selected>Choose hub</option>
                                    <option value="ICT">ICT</option>
                                    <option value="Industrial">Industrial</option>
                                </select>
                                <label>Hub</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="stage">
                                    <option value="" disabled selected>Choose Stage</option>
                                    <option value="Stage 1">Stage 1</option>
                                    <option value="Stage 2">Stage 2</option>
                                    <option value="Stage 3">Stage 3</option>
                                </select>
                                <label>Stage</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="b_type">
                                    <option value="" disabled selected>Choose option</option>
                                    <option value="B2B">B2B</option>
                                    <option value="B2C">B2C</option>
                                </select>
                                <label>Business-Type</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="smart_city_tags" multiple>
                                    <option value="" disabled selected>Choose Tags</option>
                                    <option value="Smart Business">Smart Business</option>
                                    <option value="Smart Community">Smart Community</option>
                                    <option value="Smart Healthcare">Smart Healthcare</option>
                                    <option value="Smart Living">Smart Living</option>
                                    <option value="Smart Mobility">Smart Mobility</option>
                                    <option value="Smart Building">Smart Building</option>
                                    <option value="Smart Infrastructure">Smart Infrastructure</option>
                                    <option value="Smart Governance">Smart Governance</option>
                                    <option value="Smart Environment">Smart Environment</option>
                                </select>
                                <label>Smart City Tags
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <select id="Virtual_or_Physical">
                                    <option value="" disabled selected>Choose your Availability</option>
                                    <option id="Physical" value="Physical">Physical</option>
                                    <option id="Virtual" value="Virtual">Virtual</option>
                                </select>
                                <label>Physical or Virtual</label>
                            </div>
                            <div class="input-field col m6">
                                <label for="advisor_name">Advisor Name</label>
                                <input id="advisor_name" type="text" class="validate">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <label for="mentor_name">Mentor Name</label>
                                <input id="mentor_name" type="text" class="validate">
                            </div>
                        </div>
                        <br>
                        <h6 style="padding-left: 50px;"><b>GOVERNANCE</b></h6>

                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>Client Contract </span>
                                    <input type="file" id="client_contract" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="input-field col m6">
                                <label for="date_of_signature">Date of Signature</label>
                                <input type="date" id="date_of_signature">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>Copy of ID Document</span>
                                        <input type="file" id="id_document" multiple>
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text"
                                               placeholder="Upload one or more ID Documents">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>Tax Clearance</span>
                                    <input type="file" id="tax_clearance" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="input-field col m6">
                                <label for="date_of_clearance">Date of Tax Clearance</label>
                                <input type="date" id="date_of_clearance">
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>BBBEE Certificate</span>
                                    <input type="file" id="bbbee_certificate" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="input-field col m6">
                                <label for="date_of_bbbee">Date of BBBEE Certificate</label>
                                <input type="date" id="date_of_bbbee">
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>CK Documents</span>
                                    <input type="file" id="ck_document" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                            <div class="file-field input-field col m6">
                                <div class="btn">
                                    <span>VAT Registration Documents</span>
                                    <input type="file" id="vat_reg" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload in PDF">
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row" id="precinct_info">
                            <h6 style="padding-left: 50px;"><b>PRECINCT INFO</b></h6>

                            <div class="input-field col m6">
                                <input id="telephone_code" type="text" class="validate">
                                <label for="telephone_code">Telephone code</label>
                            </div>
                            <div class="input-field col m6">
                                <select id="precinct_info">
                                    <option value="" disabled selected>Do You Have Keys</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <label>Keys</label>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="office_position" type="text" class="validate">
                                    <label for="office_position">Office Position</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="printing_code" type="text" class="validate">
                                    <label for="printing_code">Printing Code</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="warehouse_position" type="text" class="validate">
                                    <label for="warehouse_position">Warehouse Position</label>
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="input-field col m6">
                                <input id="business_facebook" type="text" class="validate">
                                <label for="business_facebook">Facebook</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="business_linkedIn" type="text" class="validate">
                                <label for="business_linkedIn">LinkedIn</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m6">
                                <input id="business_twitter" type="text" class="validate">
                                <label for="business_twitter">Twitter</label>
                            </div>
                            <div class="input-field col m6">
                                <input id="business_instagram" type="text" class="validate">
                                <label for="business_instagram">Instagram</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6">
                                <input id="cohort" type="text" class="validate">
                                <label for="cohort">Cohort</label>
                            </div>
                            <div class="input-field col m6">
                                <textarea id="venture_profile_information" class="materialize-textarea"></textarea>
                                <label for="venture_profile_information">Venture Profile Information</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col offset-m4">
                                <a
                                    class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-startup-details" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>


                <div id="test3" class="col s12">
                    <form id="media_form" class="col s12" style="margin-top:1em;">
                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Profile Picture</span>
                                        <input id="profile_picture_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <img src="" id="previewing">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Company Logo</span>
                                        <input id="company_logo_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col m6">
                                <div class="input-field" style="bottom:0px!important;">
                                    <input id="video-shot" type="text">
                                    <label for="video-shot">Video Shot</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Infographic</span>
                                        <input id="infographic_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Crowdfunding</span>
                                        <input id="crowdfunding_url" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col m6">
                                <div class="file-field input-field" style="bottom:0px!important;">
                                    <div class="btn">
                                        <span>Product Shot</span>
                                        <input id="product-shot" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col offset-m4">
                                <a class="btn waves-effect waves-light" href="{{url('incubatees')}}">Cancel
                                    <i class="material-icons left">arrow_back</i>
                                </a>
                                <button id="save-incubatee-media" style="margin-left: 2em"
                                        class="btn waves-effect waves-light" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        .tabs .tab a:hover, .tabs .tab a.active {
            color: black !important;
            font-weight: bolder;
        }

        .tabs .tab a {
            color: black !important;
        }
    </style>
    @push('custom-scripts')
        <script>
            let formData = new FormData();
            let incubatee = {};
            $(document).ready(function () {
                $('.datepicker').datepicker();
                $('select').formSelect();


                $('#short_bio').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 75) {
                        alert('Word Limit Reached!');
                    }
                });
                $('#elevator_pitch').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 50) {
                        alert('Word Limit Reached!');
                    }
                });$('#venture_profile_information').keyup(function () {
                    var words = $.trim($(this).val()).split(' ');
                    console.log(words.length);
                    if (words.length > 100) {
                        alert('Word Limit Reached!');
                    }
                });

            });
            // Function to preview image after validation
            $(function () {
                $("#profile_picture_url").change(function () {
                    $("#message").empty(); // To remove the previous error message
                    var file = this.files[0];
                    var imagefile = file.type;
                    var match = ["image/jpeg", "image/png", "image/jpg"];
                    if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                        $('#previewing').attr('src', 'noimage.png');
                        $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                        return false;
                    } else {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        reader.readAsDataURL(this.files[0]);
                    }
                });
            });

            function imageIsLoaded(e) {
                $("#profile_picture_url").css("color", "green");
                $('#image_preview').css("display", "block");
                $('#previewing').attr('src', e.target.result);
                $('#previewing').attr('width', '250px');
                $('#previewing').attr('height', '230px');
            }


            $("#dob").change(function () {
                let value = $("#dob").val();
                let dob = new Date(value);
                let today = new Date();
                let age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                if (isNaN(age)) {
                    // will set 0 when value will be NaN
                    age = 0;
                } else {
                    age = age;
                }
                let ages = $('#age').val(age);

                $('#incubatee-form').on('submit', function (e) {
                    e.preventDefault();
                    formData.append('title', $('#title').val());
                    formData.append('initials', $('#initials').val());
                    formData.append('dob', $('#dob').val());
                    formData.append('age', $('#age').val(ages));
                    formData.append('gender', $('#gender').val());
                    formData.append('id_number', $('#id_number').val());
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('email', $('#email').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('personal_facebook_url', $('#personal_facebook').val());
                    formData.append('personal_linkedIn_url', $('#personal_linked_in').val());
                    formData.append('personal_twitter_url', $('#personal_twitter').val());
                    formData.append('personal_instagram_url', $('#personal_instagram').val());

                    console.log("user ", formData);

                    $.ajax({
                        url: "{{ route('incubatee.store') }}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success", response);
                            alert(response.message);
                            incubatee = response.incubatee;
                            sessionStorage.setItem('incubatee_id', response.incubatee['id']);
                        },
                        error: function (response) {
                            console.log("error", response);
                            let message = error.response.message;
                            let errors = error.response.errors;

                            for (var error in   errors) {
                                console.log("error", error)
                                if (errors.hasOwnProperty(error)) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });

            });


            $('#address_check').click(function () {
                if ($(this).is(':checked')) {
                    let physical_address_1 = $('#physical_address_1').val();
                    let physical_address_2 = $('#physical_address_2').val();
                    let physical_address_3 = $('#physical_address_3').val();
                    let physical_city = $('#physical_city').val();
                    let physical_code = $('#physical_code').val();

                    $('#postal_address_1').val(physical_address_1);
                    $('#postal_address_2').val(physical_address_2);
                    $('#postal_address_3').val(physical_address_3);
                    $('#postal_city').val(physical_city);
                    $('#postal_code').val(physical_code);
                } else {
                    formData.append('postal_address_1', $('#postal_address_1').val());
                    formData.append('postal_address_2', $('#postal_address_2').val());
                    formData.append('postal_address_3', $('#postal_address_3').val());
                    formData.append('postal_city', $('#postal_city').val());
                    formData.append('postal_code', $('#postal_code').val());
                }
                /*if ($(this).not(':checked')){
                    $('#postal_address_1').val("");
                    $('#postal_address_2').val("");
                    $('#postal_address_3').val("");
                    $('#postal_city').val("");
                    $('#postal_code').val("");
                }*/
            });


            $('#start_details_form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();
                formData.append('startup_name', $('#startup_name').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('physical_address_1', $('#physical_address_1').val());
                formData.append('physical_address_2', $('#physical_address_2').val());
                formData.append('physical_address_3', $('#physical_address_3').val());
                formData.append('physical_city', $('#physical_city').val());
                formData.append('physical_code', $('#physical_code').val());
                formData.append('postal_address_1', $('#postal_address_1').val());
                formData.append('postal_address_2', $('#postal_address_2').val());
                formData.append('postal_address_3', $('#postal_address_3').val());
                formData.append('postal_city', $('#postal_city').val());
                formData.append('postal_code', $('#postal_code').val());
                formData.append('hub', $('#hub').val());
                formData.append('smart_city_tags', $('#smart_city_tags').val());
                formData.append('b_type', $('#b_type').val());
                formData.append('company_website', $('#company_website').val());
                formData.append('cohort', $('#cohort').val());
                formData.append('stage', $('#stage').val());
                formData.append('Virtual_or_Physical', $('#Virtual_or_Physical').val());
                formData.append('ownership', $('#ownership').val());
                formData.append('venture_profile_information', $('#venture_profile_information').val());
                formData.append('business_facebook_url', $('#business_facebook').val());
                formData.append('business_twitter_url', $('#business_twitter').val());
                formData.append('business_linkedIn_url', $('#business_linkedIn').val());
                formData.append('business_instagram_url', $('#business_instagram').val());
                formData.append('registration_number', $('#registration_number').val());
                formData.append('BBBEE_level', $('#BBBEE_Level').val());
                formData.append('date_of_signature', $('#date_of_signature').val());
                formData.append('date_of_clearance', $('#date_of_clearance').val());
                formData.append('date_of_bbbee', $('#date_of_bbbee').val());
                formData.append('telephone_code', $('#telephone_code').val());
                formData.append('precinct_info', $('#precinct_info').val());
                formData.append('office_position', $('#office_position').val());
                formData.append('printing_code', $('#printing_code').val());
                formData.append('warehouse_position', $('#warehouse_position').val());
                formData.append('short_bio', $('#short_bio').val());
                formData.append('elevator_pitch', $('#elevator_pitch').val());

                jQuery.each(jQuery('#client_contract')[0].files, function (i, file) {
                    formData.append('client_contract', file);
                });
                jQuery.each(jQuery('#id_document')[0].files, function (i, file) {
                    formData.append('id_document', file);
                });
                jQuery.each(jQuery('#tax_clearance')[0].files, function (i, file) {
                    formData.append('tax_clearance', file);
                });
                jQuery.each(jQuery('#bbbee_certificate')[0].files, function (i, file) {
                    formData.append('bbbee_certificate', file);
                });
                jQuery.each(jQuery('#ck_document')[0].files, function (i, file) {
                    formData.append('ck_document', file);
                });
                jQuery.each(jQuery('#vat_reg')[0].files, function (i, file) {
                    formData.append('vat_reg', file);
                });


                console.log("user ", formData);
                let url = 'incubatee-store-business/' + incubatee.id;
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',

                    success: function (response, a, b) {
                        console.log("success", response);
                        alert(response.message);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = error.response.message;
                        let errors = error.response.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);
//                        $("#modal1").close();
                    }
                });
            });

            $(function () {
                $("#Virtual_or_Physical").change(function () {
                    if ($("#Physical").is(":selected")) {
                        $("#precinct_info").show();

                    } else {
                        $("#precinct_info").hide();

                    }
                }).trigger('change');
            });

            $('#media_form').on('submit', function (e) {
                e.preventDefault();

                let incubatee_id = sessionStorage.getItem('incubatee_id');


                let formData = new FormData();
                formData.append('incubatee_id', incubatee_id);
                formData.append('video_shot_url', $('#video-shot').val());


                jQuery.each(jQuery('#profile_picture_url')[0].files, function (i, file) {
                    formData.append('profile_picture_url', file);
                });

                jQuery.each(jQuery('#company_logo_url')[0].files, function (i, file) {
                    formData.append('company_logo_url', file);
                });

                jQuery.each(jQuery('#infographic_url')[0].files, function (i, file) {
                    formData.append('infographic_url', file);
                });

                jQuery.each(jQuery('#crowdfunding_url')[0].files, function (i, file) {
                    formData.append('crowdfunding_url', file);
                });

                jQuery.each(jQuery('#product-shot')[0].files, function (i, file) {
                    formData.append('product_shot_url', file);
                });


                let url = 'incubatee-store-media';

                console.log(url);

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response, a, b) {
                        console.log("success", response);
                        alert(response.message);
                        window.location.href = 'incubatees';
                        sessionStorage.clear();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = error.response.message;
                        let errors = error.response.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);
                    }
                });
            });
        </script>
    @endpush
@endsection
