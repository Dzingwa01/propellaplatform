@extends('layouts.applicant-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>

    <br>
    {{ Breadcrumbs::render('applicant-edit-app')}}

    <div class="section" style="margin-top: 2em;">

        <input  id="user-id-input" value="{{$user->id}}" hidden>
        <div class="card white">
            @if(count($applicant_array) > 0)
                <div class="row" style="margin-left: 4em; margin-right: 4em;">
                    <ol>
                        @foreach ($applicant_array as $question)
                            <li value="{{$question->question_number}}">  {{ $question->question_text}} </li>
                            <div class="input-field">
                                <input id="{{$question->question_id}}" class="materialize-textarea answer-text-desktop" value="{{$question->answer_text}}"
                                       style="width:90%">
                            </div>
                        @endforeach
                    </ol>
                </div>
                @else
                <div class="row center">
                    <h4>{{$g_question_category->category_name}}</h4>
                    <p style="color: red; !important;">*Once you are sure of all your answers, click the "Finalize" button, this will submit your
                        application form.</p>
                </div>

                <div class="row" style="margin-left: 4em; margin-right: 4em;">
                    <ol>
                        @foreach ($g_questions as $question)
                            <li value="{{$question->question_number}}">  {{ $question->question_text}} </li>
                            <div class="input-field">
                                <input id="{{$question->id}}" class="materialize-textarea answer-text-desktop"
                                       style="width:90%">
                            </div>
                        @endforeach
                    </ol>
                </div>
            @endif


            <div class="row center">
                <div class="col l6 m5 s12">
                    <button class="btn waves-effect waves-light" id="upload-answers-form-desktop"> Finalize <i
                            class="material-icons right">send</i></button>
                </div>
            </div>
        </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large blue btn modal-trigger" data-position="left" id="save-progress-button">Save</a>
    </div>


    <script>

        $(document).ready(function () {

            $('#save-progress-button').on('click', function(){
                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#user-id-input').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        question_id: this.id,
                        user_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));

                //To push the answersArray as data to the controller and save all the answers to the db
                $.ajax({
                    url: "/Application-Process/questions",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        console.log("success",response);
                        alert(response.message);
                        window.location.href = "/login";
                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            //Desktop Form
            $('#upload-answers-form-desktop').on('click', function () {

                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#user-id-input').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        question_id: this.id,
                        user_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));
                formData.append('application_completed', 'true');

                //To push the answersArray as data to the controller and save all the answers to the db
                $.ajax({
                    url: "/Application-Process/questions",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        console.log("success",response);
                        alert(response.message);
                        window.location.href = "/login";
                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>
    @endsection
