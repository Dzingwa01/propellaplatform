a@extends('layouts.admin-layout')
@section('content')
    <br>
    <br>
    <div class="card">
        <h3>Application Additional Information</h3>

    </div>
    <br>
    <form id="add_application_rep-form" style="margin-top:1em;">
        <div class="row">
            <div class="input-field col m6">
                <p>
                    <label>
                        <input type="checkbox" id="invite_pitch" {{--onclick="isChecked();"--}}/>
                        <span>Select if invited to pitch</span>
                    </label>
                </p>
            </div>
            <div class="input-field col m6">
                <p>
                    <label>
                        <input type="checkbox" id="referred"/>
                        <span>Select if referred</span>
                    </label>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="input-field col m6">v
                <p>
                    <label>
                        <input type="checkbox" id="declined"/>
                        <span>Select if declined</span>
                    </label>
                </p>
            </div>
            <div class="input-field col m6">
                <p>
                    <label>
                        <input type="checkbox" id="incomplete"/>
                        <span>Select if application is incomplete</span>
                    </label>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col offset-m4">
                <a class="btn waves-effect waves-light" href="#">Cancel
                    <i class="material-icons left">arrow_back</i>
                </a>
                <button id="save-applicants-additional-details" style="margin-left: 2em"
                        class="btn waves-effect waves-light" type="submit">Submit
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </div>
    </form>

    @push('custom-scripts')
        <script>
            $('#add_application_rep-form').on('submit', function (e) {
                e.preventDefault();
                formData.append('invite_pitch', $('#invite_pitch').val());
                formData.append('referred', $('#referred').val());
                formData.append('declined', $('#declined').val());
                formData.append('incomplete', $('#incomplete').val(ages));

                console.log("user ", formData);

                $.ajax({
                    url: "{{ route('incubatee.store') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',

                    success: function (response, a, b) {
                        console.log("success", response);
                        alert(response.message);
                        incubatee = response.incubatee;
                        sessionStorage.setItem('incubatee_id', response.incubatee['id']);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = error.response.message;
                        let errors = error.response.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);
                        $("#modal1").close();
                    }
                });
            });


        </script>
    @endpush
@endsection
