@extends('layouts.app')
@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <br>
    <br>
    {{ Breadcrumbs::render('admin-edit-questt')}}
    <form id="update-questions-form" enctype="multipart/form-data">
        <div class="row"style="width: 850px;padding-left:9%;">
            <div class="card white"style="padding-left:5%;">
                <br />
                <h4>Update Question</h4>
{{--                {{dd($question)}}--}}
                <input value="{{$question->id}}" id="question_id" hidden>
                <div class="input-field"style="width:90%">
                    <select id="category-id" >
                        <option value="" disabled selected>Chose the question category</option>
                        @foreach($questionCategories as $category)
                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="input-field"style="width:90%">
                    <select id="question-type-id">
                        <option  value="" disabled selected>Chose the data type</option>
                        @foreach($questionTypes as $datatype)
                            <option  value="{{$datatype->id}}">{{$datatype->datatype}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="input-field">
                    <input style="width:90%" id="question-number" type="text" value="{{$question->question_number}}">
                    <label for="question-number">Question Number</label>
                </div>

                <div class="input-field">
                    <input style="width:90%" id="question-text" type="text" class="validate" value="{{$question->question_text}}">
                    <label for="question-text">Question text</label>
                </div>

                <div class="input-field">
                    Percentage:
                    <input style="width:90%" type="number" name="points" min="0" max="100" step="10" id="percentage" value="{{$question->percentage}}">
                </div>
                <button class="btn waves-effect waves-light" id="save-question-btn" type="submit"> Submit <i class="material-icons right">send</i> </button>


                <br />
                <br />
                <br />
            </div>
        </div>
    </form>



    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>

    <script>
        $(document).ready(function() {
            $('#update-questions-form').on('submit', function(e) {

                e.preventDefault();

                let formData = new FormData();

                formData.append('question_text', $('#question-text').val());
                formData.append('percentage', $('#percentage').val());
                formData.append('question_category_id', $('#category-id').val());
                formData.append('question_type_id', $('#question-type-id').val());
                formData.append('question_number', $('#question-number').val());


                console.log(formData);
                let url = '/update-question-edit/'+$('#question_id').val();
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.href = '/view-questions';
                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>
@endsection

