 @extends('layouts.mentor-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('mentor')}}
    <input hidden disabled id="user-id-input" value="{{$user->id}}">

    <div class="row" style="margin-top:4em;">
            <div class="col s12 m4">
                <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: rgba(0, 121, 52, 1); height: 185px">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">people</i>-->Venturess</span>
                        <p style="font-weight: bolder">This is where you view ventures assigned to you.</p>

                    </div>
                    <div class="card-action center" style="border-radius: 15px;">
                        <a class="btn" id="mentor-ventures-button" style="cursor: pointer; color: white;background-color: rgba(0, 121, 52, 1)"><b>View Ventures</b></a>
                    </div>
                </div>
            </div>
        <div class="col s12 m4">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color:#5A5A5A; height: 185px">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">people</i>-->View Shadow boards</span>
                    <p style="font-weight: bolder">This is where you view shadow boards meetings.</p>

                </div>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="btn" id="shadow-button" style="cursor: pointer; color: white; background-color: #5A5A5A"><b>View Shadow Boards</b></a>
                </div>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: rgba(0, 47, 95, 1); height: 185px">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">perm_contact_calendar</i>--> &nbsp;Events</span>
                    <p style="font-weight: bolder">This is where you view your events.</p>

                </div>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="btn" id="user-events-button" style="cursor: pointer; color: white;background-color: rgba(0, 47, 95, 1);"><b>View Events</b></a>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: #5A5A5A; height: 185px">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">border_color</i>-->   Book Venues</span>
                    <p style="font-weight: bolder">This is where you view your booked venues.</p>

                </div>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="btn" id="user-venue-button" href="{{url('/show-booking/'.$user->id)}}" style="cursor: pointer; color: white;background-color: #5A5A5A;"><b>View Venues</b></a>
                </div>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: rgba(0, 121, 52, 1); height: 185px">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">border_color</i>-->Make appointment</span>
                    <p style="font-weight: bolder">This is where you make appointment with your incubatees.</p>

                </div>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="waves-effect waves-light btn modal-trigger" style="background-color: rgba(0, 121, 52, 1);" href="#appointment"><b>Create Appointment</b></a>
                    <a class="waves-effect waves-light btn modal-trigger" style="background-color: rgba(0, 121, 52, 1);" href="#viewAppointments"><b>View Appointment</b></a>
                </div>
            </div>
        </div>
       {{-- View appointments--}}
        <div id="viewAppointments" class="modal">
            <div class="modal-content">
                <table>
                    <tr>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Message</th>
                        <th>Mentor name</th>
                    </tr>
                    @foreach($appointment as $appointment)
                    <tr>
                        <td>{{$appointment->title}}</td>
                        <td>{{$appointment->date}}</td>
                        <td>{{$appointment->time}}</td>
                        <td>{{$appointment->message}}</td>
                        <td>{{$appointment->mentor_name}}</td>
                    </tr>
                        @endforeach
                </table>

            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Appointment Modal -->
        <div id="appointment" class="modal modal-fixed-footer">
            <div class="modal-content">

                <h4 class="center-align"><b>Create an appointment</b></h4>
                <div class="row" style="margin-left: 2em;margin-right: 2em">
                    <form class="col s12" id="AddEvent" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="title" name="title" type="text" class="validate">
                                <label for="title">Title</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="date" type="date" class="validate">
                                    <label for="date">Date</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="time" type="time" class="validate">
                                <label for="time">Time</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea id="message" name="message" class="materialize-textarea"></textarea>
                                <label for="message">Message</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select id="user_id" multiple="multiple">
                                    <option>Choose incubatee</option>
                                    @foreach($incubatees as $user)
                                        <option value="{{$user->id}}">{{$user->name}} {{$user->surname}}</option>
                                    @endforeach
                                </select>
                                <label>Select incubatee</label>
                            </div>
                        </div>
                        <div class="col offset-m4">
                            <button class="btn waves-effect waves-light" type="submit" style="margin-left:2em;"
                                    id="save-event" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @foreach($user->roles as $user_role)
            @if($user_role->name == 'panelist')
                <div class="col s12 m4" style="height: 400px;">
                    <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: #1a237e; ">
                        <div class="card-content white-text">
                            <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i> Panel Interview </span>
                            <p style="align-content: center; ">This is where you will see the applicants assigned to your panel interview. </p>
                            <br/>
                            <br/>
                        </div>
                        <div class="card-action center" style="border-radius: 15px;">
                            <a class="btn" href="/panelist-show-industrial-applicants"
                               style="cursor: pointer; color: white;background-color: #1a237e; "><b>View Applicants</b></a>
                            <a class="btn" href="/panelist-show-ict-applicants"
                               style="cursor: pointer; color: white;background-color: #1a237e; "><b>View Bootcampers</b></a>
                            <a class="btn" href="/panelist-show-ventures"
                               style="cursor: pointer; color: white;background-color: #1a237e;"><b>View Ventures</b></a>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach

        @if($user_enquiries != null or $user_pre_assigned_enquiries != null)
            @if(count($user_enquiries) > 0 or count($user_pre_assigned_enquiries) > 0)
                <div class="col s12 m4" style="height: 400px;">
                    <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: #1a237e; ">
                        <div class="card-content white-text">
                            <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i> Manage General Enquiries </span>
                            <p style="align-content: center; ">This is where you will see the enquiries that you have been assigned to. </p>
                            <br/>
                            <br/>
                        </div>
                        <div class="card-action center" style="border-radius: 15px;">
                            <a href="/pre-assigned-enquiry-index"
                               style="cursor: pointer; color: white;"><b>View Pre-Assigned Enquirires</b></a>
                            <a href="/assigned-enquiry-index"
                               style="cursor: pointer; color: white;"><b>View Assigned Enquirires</b></a>
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>


    {{--<button id="user-events-button">Calendar</button>--}}
    <script>
        $('#shadow-button').on('click', function(){
            let user_id = $('#user-id-input').val();
            window.location.href = '/mentor-venture-shadow/' + user_id;
        });
        $('#mentor-ventures-button').on('click', function(){
            let user_id = $('#user-id-input').val();
            window.location.href = '/mentor-show/' + user_id;
        });

        $(document).ready(function(){
            let stored_event_id = sessionStorage.getItem('stored_event_id');
            let stored_found_out = sessionStorage.getItem('stored_found_out_via');
            let user_id = $('#user-id-input').val();

            if(stored_event_id !== "" && stored_found_out !== ""){
                let formData = new FormData();
                formData.append('event_id', stored_event_id);
                formData.append('found_out_via', stored_found_out);
                formData.append('user_id', user_id);

                let url = "{{route('user.storeUserEventViaPublicEventPage')}}";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        sessionStorage.setItem('stored_event_id', "");
                        sessionStorage.setItem('stored_found_out_via', "");
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            }


            $('select').formSelect();

            //Make mentor appointment

            $('#AddEvent').on('submit', function (e) {
                let selected_incubatees = [];

                jQuery.each(jQuery('#user_id').val(), function (i, value) {
                    selected_incubatees.push(value);
                });
                e.preventDefault();
                let formData = new FormData();
                formData.append('title', $('#title').val());
                formData.append('date', $('#date').val());
                formData.append('time', $('#time').val());
                formData.append('message', $('#message').val());
                formData.append('selected_incubatees', JSON.stringify(selected_incubatees));


                console.log(formData);

                $.ajax({
                    url: "store-appointment",
                    processData: false,
                    contentType: false,
                    type: "POST",
                    data: formData,
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        console.log("success", response);
                        $('#save-event').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.href = '/';
                            $("#modal1").close();
                        }, 3000);

                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                        $("#modal1").close();
                    }


                });
            });
        });

        $('#user-events-button').on('click', function(){
            let user_id = $('#user-id-input').val();
            window.location.href = '/users/show-events/' + user_id;
        });


    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>

@endsection
