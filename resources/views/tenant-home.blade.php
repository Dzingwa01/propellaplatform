@extends('layouts.tenant-layout')

@section('content')

    <input hidden disabled id="user-id-input" value="{{$user->id}}">

    <div class="row" style="margin-left: 5em;">
        <div class="col s12 m4" style="height: 400px;margin-top: 10vh">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: grey ">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i>   Book Venues</span>
                    <br/>
                    <p style="align-content: center; ">This is where you view your booked venues</p>
                    <br/>
                </div>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="btn" id="user-venue-button" href="{{url('/show-booking/'.$user->id)}}" style="cursor: pointer; color: white;background-color: grey"><b>View Venues</b></a>
                </div>
            </div>
        </div>
        <div class="col s12 m4" style="height: 400px;margin-top: 10vh">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: rgba(0, 121, 52, 1) ">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><i class="small material-icons">perm_contact_calendar</i>   Events</span>
                    <p>Total number of Events - {{$all_events}}</p>
                    <br/>
                    <p style="align-content: center; ">This is where you view your events</p>
                    <br/>

                </div>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="btn" id="user-events-button" style="cursor: pointer; color: white;background-color: rgba(0, 121, 52, 1) "><b>View Events</b></a>
                </div>
            </div>
        </div>

        @if($user_enquiries != null or $user_pre_assigned_enquiries != null)
            @if(count($user_enquiries) > 0 or count($user_pre_assigned_enquiries) > 0)
                <div class="col s12 m4" style="height: 400px;">
                    <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: #1a237e; ">
                        <div class="card-content white-text">
                            <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i> Manage General Enquiries </span>
                            <p style="align-content: center; ">This is where you will see the enquiries that you have been assigned to. </p>
                            <br/>
                            <br/>
                        </div>
                        <div class="card-action center" style="border-radius: 15px;">
                            <a href="/pre-assigned-enquiry-index"
                               style="cursor: pointer; color: white;"><b>View Pre-Assigned Enquirires</b></a>
                            <a href="/assigned-enquiry-index"
                               style="cursor: pointer; color: white;"><b>View Assigned Enquirires</b></a>
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>

    <script>
        $(document).ready(function () {
            let stored_event_id = sessionStorage.getItem('stored_event_id');
            let stored_found_out = sessionStorage.getItem('stored_found_out_via');
            let user_id = $('#user-id-input').val();

            if (stored_event_id !== "" && stored_found_out !== "") {
                let formData = new FormData();
                formData.append('event_id', stored_event_id);
                formData.append('found_out_via', stored_found_out);
                formData.append('user_id', user_id);

                let url = "{{route('user.storeUserEventViaPublicEventPage')}}";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $.notify(response.message, "success");
                        sessionStorage.setItem('stored_event_id', "");
                        sessionStorage.setItem('stored_found_out_via', "");
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        $.notify(message, "error");                    }
                });
            }
        });

        $('#user-events-button').on('click', function () {
            let user_id = $('#user-id-input').val();
            window.location.href = '/users/show-events/' + user_id;
        });
        /*$('#user-venue-button').on('click', function () {
            let user_id = $('#user-id-input').val();*/
        /*window.location.href = '/BookingVenue/show-venues/' + user_id;*/
        /*window.location.href = "";
    });*/
    </script>

@endsection
