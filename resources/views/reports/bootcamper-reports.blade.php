@extends('layouts.admin-layout')

@section('content')
    <div class="container-fluid">
        <div class="row" style="margin-top: 5em;">
            <div class="col l4 m6 s12">
                <div class="card  darken-1 hoverable">
                    <div class="card-title">
                        <span class="card-title">Detailed Report</span>
                        <hr>
                    </div>
                    <div class="card-content">
                        <p>Fields included:</p>
                        <ol>
                            <li>Name</li>
                            <li>Surname</li>
                            <li>Email</li>
                            <li>Venture name</li>
                            <li>Contact number</li>
                            <li>WhatsApp number</li>
                            <li>Data number</li>
                            <li>Service Provider</li>
                            <li>ID number</li>
                            <li>Age</li>
                            <li>Gender</li>
                            <li>Race</li>
                            <li>Address  One</li>
                            <li>Address  Two</li>
                            <li>Address  Three</li>
                            <li>City</li>
                            <li>Event</li>
                            <li>Accepted event</li>
                            <li>Attended event</li>
                            <li>NDA agreement (yes/no)</li>
                            <li>CIPC (yes/no)</li>
                            <li>Proof of address (yes/no)</li>
                            <li>ID copy (yes/no)</li>
                            <li>Evaluation form (yes/no)</li>
                            <li>Pitch video (yes/no)</li>
                        </ol>
                    </div>
                    <div class="card-action center">
                        <a class="waves-effect waves-light btn modal-trigger" href="#detailed-bootcamper-report-modal"><b>Choose Report</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="detailed-bootcamper-report-modal" class="modal" style="height: 400px;">
        <div class="modal-content">
            <select id="event-id-input">
                <option selected>Select Report</option>
                @foreach($bootcampEvents as $bootcampEvent)
                    <option value="{{isset($bootcampEvent) ? $bootcampEvent->id : null}}">{{isset($bootcampEvent) ? $bootcampEvent->title : 'No title'}}</option>
                @endforeach
            </select>
            <a class="btn" id="export-detailed-bootcamper-report-button" onclick="downloadDetailedBootcamperReport()"><b>Download</b></a>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('select').formSelect();
        });

        function downloadDetailedBootcamperReport(){
            //Get event id
            let eventId = $('#event-id-input').val();
            //Go to function to download
            window.location.href = '/download-detailed-bootcamper-report/' + eventId;
        }
    </script>
@endsection
