@extends('layouts.admin-layout')
@section('content')
    <div class="container-fluid">
        <div class="row" style="margin-top: 5em;">
            <div class="col l4 m6 s12">
                <div class="card  darken-1 hoverable">
                    <div class="card-content">
                        <span class="card-title" style="font-weight: bolder">Reports</span>
                    </div>
                    <div class="card-action center">
                        <a class="btn" href="/view-bootcamper-reports"><b>View reports</b></a>
                    </div>
                </div>
            </div>
            <div class="col l4 m6 s12">
                <div class="card  darken-1 hoverable">
                    <div class="card-content">
                        <span class="card-title" style="font-weight: bolder">Personalized script</span>
                        <p>Use this area to write a script that will return values. !! SELECT ONLY !!</p>
                        <input type="text" id="sql-script-text">
                    </div>
                    <div class="card-action center">
                        <a class="btn" onclick="sendPersonalizedScript()"><b>Send script</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function sendPersonalizedScript(){
            //Get sql script
            let sqlScript = $('#sql-script-text').val();
            //Go to function to download
            window.location.href = '/send-personalized-sql-script/' + sqlScript;
            /*//create url
            let url = '/send-personalized-sql-script';
            //Ajax request
            $.ajax({
                url: url,
                type: 'post',
                data: {
                    sqlScript: $('#sql-script-text').val()
                },
                success: function (response, a, b) {
                    console.log("success");
                },
                error: function (response) {
                    console.log("error", response);
                }
            });*/
        }
    </script>
@endsection
