@extends('layouts.app')

@section('content')

    <link rel="stylesheet" type="text/css" href="/css/Blog/blogShow.css"/>
    <style>
        .paragraph { margin-top: -20px; }
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>

    <!--Desktop-->
    <div class="showBlogDesktop">
        <div class="section ">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/How-To-Start-a-Travel-Blog.jpg" style="width: 100%;height: 70vh;-webkit-filter: grayscale(100%);">
                    </div>
                    <h4 class="center-align" style="margin-top: 58vh;color: white"><b><span><a href="/">Propella</a></span><i class="material-icons dp48">arrow_forward</i>{{$object->blog_title}}</b></h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col m8 blogIndex">
                <div class="row show" style="margin-left: 10em;">
                    <p class="paragraph" style="font-size: 2em;"><b>{{$object->blog_title}}</b></p>

                    <p style="">{{$object->blog_date->toDateString()}}</p>
                </div>
                <div class="row">
                    <div class="row" style="margin-left: 10em;margin-right: 5em;">
                        @if($object->blog_image_url != null)
                            <img style="height: 375px;" src="/storage/{{isset($object->blog_image_url)?$object->blog_image_url:'Nothing Detected'}}">
                        @endif
                        {{--<img style="height: 375px;" src="/images/PartnerPageImages/3.5 Talk.jpg">--}}
                    </div>
                    <div class="row" style="margin-left: 10em;">
                        <p style="margin-right: 3em;font-size: 1.7em">{{$object->blog_description}}</p>
                        @foreach($object->blog_sections as $section)
                            <div class="row">
                                @if($section->section_image_url != null)
                                    <div class="col s2">
                                        <img style="height: 300px;" src="{{isset($section->section_image_url)?$section->section_image_url:'Nothing Detected'}}">
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                @if($section->section_video_url != null)
                                    <iframe src="http://www.youtube.com/embed/'. $section_video_url.'" width="320" height="240" frameborder="0">
                                    {{--<iframe  width="425" height="344" src="{{$section->section_video_url}}" frameborder="0" allowfullscreen></iframe>--}}
                                <div class="row" style="margin-left: 1px">
                                    <a href="{{$section->section_video_url}}" target="_blank">Watch this video</a>
                                </div>
                                @endif

                            </div>
                            <div class="row" style="margin: 0;padding: 0">
                                <p  style="font-size: 2em" ><b>{{$section->section_header}}</b></p>
                            </div>
                            @foreach($section->blogParagraph as $paragraph)
                                @if($paragraph->blog_section_id == $section->id)
                                    <p class="paragraph" style="margin:0;">{{$paragraph->paragraph_title}}</p>
                                    <p class="paragraph" style="margin-right: 8em;">{{$paragraph->paragraph_text}}</p>
                                    <br>
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col m2"></div>
            <div class="col m3 " style="float: bottom;background-color: lightgray;margin-right: 1em">
                <h4 style="font-size: 1em"><b>RECENT POSTS</b></h4>
                <hr align="left">
                @for($i = 0; $i<count($blogs); $i++)
                    @if($i === 0)
                            <div class="blogShow"  data-value="{{$blogs[$i]->id}}" onclick="navigateBlog(this)" style="cursor: pointer">
                                @if($blogs[$i]->blog_image_url != null)
                                    {{--<img style="width:100%;height:250%;" src="/images/PartnerPageImages/3.5 Talk.jpg">--}}
                                    <img width="650" style="height: 150px;width: 350px"  src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                @endif
                                 <div class="row">
                                    <a style="font-size: 1em;margin-left: 1em"><b>{{$blogs[$i]->title}}</b></a>
                                </div>
                            <p style="margin-right: 1em">{{$blogs[$i]->description}}</p>
                        </div>
                        @else
                        <div class="" data-value="{{$blogs[$i]->id}}" onclick="navigateBlog(this)" style="cursor: pointer">
                            @if($blogs[$i]->blog_image_url != null)
                                {{--<img style="width:100%;height:250%;" src="/images/PartnerPageImages/3.5 Talk.jpg">--}}
                                <img width="650" style="height: 150px;width: 350px"  src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                            @endif
                           <div class="row">
                                <a style="font-size: 1em;margin-left: 1em"><b>{{$blogs[$i]->title}}</b></a>
                            </div>
                            <p style="margin-right: 1em">{{$blogs[$i]->description}}</p>
                        </div>

                    @endif
                @endfor

            </div>
        </div>
    </div>


    <!--Mobile-->
    <div class="showBlogMobile">
        <div class="section ">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/How-To-Start-a-Travel-Blog.jpg" style="width: 100%;height: 70vh;-webkit-filter: grayscale(100%);">
                    </div>
                    <h5 class="center-align" style="margin-top: 68vh;color: white"><b><span><a href="/">Propella</a></span><i class="material-icons dp48">arrow_forward</i>{{$object->blog_title}}</b></h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col m7 blogIndex">
                <div class="row show" style="margin-left: 2em;">
                    <p style="font-size: 2em;"><b>{{$object->blog_title}}</b></p>
                </div>
                <div class="row">
                    <div class="row" style="margin-left:2em;margin-right: 2em;">
                        @if($object->blog_image_url != null)
                            <img width="300" style="height: 250px;" src="/storage/{{isset($object->blog_image_url)?$object->blog_image_url:'Nothing Detected'}}">
                        @endif
                        {{--<img style="height: 350px;" src="/images/PartnerPageImages/3.5 Talk.jpg">--}}
                    </div>
                    <div class="row" style="margin-left: 2em;margin-right: 2em">
                        <p>{{$object->blog_description}}</p>
                        @foreach($object->blog_sections as $section)
                            <div class="row">
                                @if($section->section_image_url != null)
                                    <div class="col s2">
                                        <img class="materialboxed" width="650" style="height: 300px;" src="/storage/{{isset($section->section_image_url)?$section->section_image_url:'Nothing Detected'}}">
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                @if($section->section_video_url != null)
                                    <video src="/storage/{{$section->section_video_url}}" width="50%" controls></video>
                                @endif
                            </div>
                            <p style="font-size: 2em"><b>{{$section->section_header}}</b></p>
                            @foreach($section->blogParagraph as $paragraph)
                                @if($paragraph->blog_section_id == $section->id)
                                    <p>{{$paragraph->paragraph_title}}</p>
                                    <p>{{$paragraph->paragraph_text}}</p>
                                    <br>
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col m1"></div>
            <div class="col m4" style="margin-top: 14vh">
                <h4 style="font-size: 1em"><b>RECENT POSTS</b></h4>
                <hr align="left">
                @for($i = 0; $i<count($blogs); $i++)
                    @if($i === 0)
                        <div class="blogShow"  data-value="{{$blogs[$i]->id}}" onclick="navigateBlog(this)" style="cursor: pointer">
                            <a style="font-size: 1em;"><b>{{$blogs[$i]->title}}</b></a>
                            <p>{{$blogs[$i]->description}}</p>
                        </div>
                    @else
                        <div class="" data-value="{{$blogs[$i]->id}}" onclick="navigateBlog(this)" style="cursor: pointer">
                            <a style="font-size: 1em;"><b>{{$blogs[$i]->title}}</b></a>
                            <p>{{$blogs[$i]->description}}</p>
                        </div>

                    @endif
                @endfor

            </div>
        </div>
    </div>




    <script>
        $(document).ready(function () {

            $('.fixed-action-btn').floatingActionButton();

            $('.modal').modal();
            $('.materialboxed').materialbox();


        });
        function navigateBlog(obj){
            let blog_id= obj.getAttribute('data-value');
            window.location.href="/Blogs/show-blog/" + blog_id
        }
    </script>

@endsection
