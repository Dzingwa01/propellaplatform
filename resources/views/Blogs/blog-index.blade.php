@extends('layouts.app')
@section('content')

        <link rel="stylesheet" type="text/css" href="/css/Blog/blog.css"/>


<br>
    <br>


    <!--Desktop-->
    <div class="section blogDesktop" style="margin-right: 40px; margin-left: 40px;">
        <div class="row">
            @for($i = 0; $i<count($blog_array); $i++)
                @if($i === 0)
                    <br>
                    <br>

                    <div class="col s12 m12">
                        <div class="card blog horizontal">
                            <div class="card-image">
                                {{--@if($blogs[$i]->blog_image_url != null)--}}
                                    {{--<img style="width:98%;height:300px;" src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">--}}
                                {{--@endif--}}
                                    {{--<img style="width:98%;height:300px;" src="/images/PartnerPageImages/3.5 Talk.jpg">--}}

                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                    <div class="row">
                                        <div class="col s12">

                                           <p>{{$blog_array[$i]->description}}</p>
                                            <img src="{{$blog_array[$i]->description}}">

                                        </div>
                                    </div>
                                    {{--<p style="">{{$blogs[$i]->blog_date}}</p>--}}
                                    <p style="font-size: 1.3em;margin: 0;"><b>{{$blog_array[$i]->title}}</b></p>
                                    {{--<p style="font-size: 1.3em;margin: 0;"><b>{{$blogs[$i]->body}}</b></p>--}}


                                    {{--<input hidden disabled class="blog_id" data-value="{{$blogs[$i]->id}}">--}}
                                </div>
                            </div>
                        </div>
                    </div>

                @else

                    <div class="row col s12 m3">
                        <div class=" blog hoverable card2" style="border-radius: 10px">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 400px;">
                                    <div class="user-view" align="left">
                                        @if($blogs[$i]->blog_image_url != null)
                                            {{--<img style="width:100%;height:200%;" src="/images/PartnerPageImages/3.5 Talk.jpg">--}}
                                            <img style="width:100%;height:200%;" src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                        @endif
                                            {{--<img style="width:100%;height:250%;" src="/images/PartnerPageImages/3.5 Talk.jpg">--}}

                                    </div>
                                    <br/>
                                    <div class="center-align ">
                                        <h6 class=""> {{$blogs[$i]->title}}</h6>
                                        <p>{{date("Y/m/d")}}</p>
                                        {{--<h6 class=""> {{$blogs[$i]->blog_date}}</h6>--}}
                                        <h6 class="bottom-blog-description"> {{$blogs[$i]->description}} </h6>
                                        <input hidden disabled class="blog_id" data-value="{{$blogs[$i]->id}}">
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                @endif
            @endfor
        </div>
    </div>

    <!--Mobile-->
    <div class="section blogMobile" style="margin-right: 10px; margin-left: 10px;">
        <div class="row">
            @for($i = 0; $i<count($blogs); $i++)
                @if($i === 0)
                    <br>
                    <br>

                    <div class="col s12 m3">
                        <div class="card blog hoverable" style="border-radius: 10px">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 450px;">
                                    <div class="user-view" align="left">
                                        @if($blogs[$i]->blog_image_url != null)
                                            {{--<img style="width:100%;height:100%;" src="/images/PartnerPageImages/3.5 Talk.jpg">--}}
                                            <img style="width:100%;height:250%;"
                                                 src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                        @endif
                                    </div>
                                    <br/>
                                    <div class="center-align ">
                                        <h6 class=""> {{$blogs[$i]->title}}</h6>
                                        <h6 class=""> {{$blogs[$i]->blog_date}}</h6>
                                        <h6 class="bottom-blog-description"> {{$blogs[$i]->description}} </h6>
                                        <input hidden disabled class="blog_id" data-value="{{$blogs[$i]->id}}">
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                @else

                    <div class="col s12 m3">
                        <div class="card blog hoverable card1" style="border-radius: 10px">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 450px;">
                                    <div class="user-view" align="left">
                                        @if($blogs[$i]->blog_image_url != null)
                                            {{--<img style="width:100%;height:100%;" src="/images/PartnerPageImages/3.5 Talk.jpg">--}}
                                            <img style="width:100%;height:250%;"
                                                 src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                        @endif
                                    </div>
                                    <br/>
                                    <div class="center-align ">
                                        <h6 class=""> {{$blogs[$i]->title}}</h6>
                                        <h6 class=""> {{$blogs[$i]->blog_date}}</h6>
                                        <h6 class="bottom-blog-description"> {{$blogs[$i]->description}} </h6>
                                        <input hidden disabled class="blog_id" data-value="{{$blogs[$i]->id}}">
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                @endif
            @endfor
        </div>
    </div>

    <script>

        $(document).ready(function () {

            //Card onclick
            $('.blog').each(function () {
                let blog_id = $(this).find('.blog_id').attr('data-value');

                $(this).on('click', function () {
                    location.href = '/Blogs/show-blog/' + blog_id;
                });
            });


            $('.top-blog-description').each(function () {
                var max_length = 400;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<br><br><br><i href="#" class="more_horiz material-icons small">more_horiz</i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });

            $('.bottom-blog-description').each(function () {
                var max_length = 50;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<br><br><br><i href="#" class="more_horiz material-icons small">more_horiz</i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });

        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
