@extends('layouts.app')

@section('content')
    <head>
        <link rel="stylesheet" type="text/css" href="/css/Events/gallery.css"/>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>
    <body>
    <br>
    <div class="container" style="width: 100%;">
        <br>
        <br>
        <div id="crouton">
            <ul>
                <li><a href="/home/gallery">Gallery</a></li>
                <li><a href="#">{{$event->title}}</a></li>
            </ul>
        </div>
        <div class="row">
            <br>
            <h4>{{$event->title}}</h4>
            @if(isset($event->EventsMedia))
                <p>{{$event->EventsMedia->media_description}}</p>
            @endif
        </div>

        <div class="container1">
        @foreach($event_media_images as $event_media_image)
            @if(isset($event_media_image->event_media_url))
                <div class="col s12 m6 l3 image materialboxed">
                    <img src="/storage/{{$event_media_image->event_media_url}}">
                </div>
            @endif
        @endforeach
        </div>
    </div>
    </body>

    <style>
        #crouton ul {
            margin: 0;
            padding: 0;
            overflow: hidden;
            width: 100%;
            list-style: none;
        }

        #crouton li {
            float: left;
            margin: 0 10px;
        }

        #crouton a {
            background: #ddd;
            padding: .7em 1em;
            float: left;
            text-decoration: none;
            color: #444;
            text-shadow: 0 1px 0 rgba(255,255,255,.5);
            position: relative;
        }

        #crouton li:first-child a {
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        #crouton li:last-child a {
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        #crouton a:hover {
            background: #99db76;
        }

        #crouton li:not(:first-child) > a::before {
            content: "";
            position: absolute;
            top: 50%;
            margin-top: -1.5em;
            border-width: 1.5em 0 1.5em 1em;
            border-style: solid;
            border-color: #ddd #ddd #ddd transparent;
            left: -1em;
        }

        #crouton li:not(:first-child) > a:hover::before {
            border-color: #99db76 #99db76 #99db76 transparent;
        }

        #crouton li:not(:last-child) > a::after {
            content: "";
            position: absolute;
            top: 50%;
            margin-top: -1.5em;
            border-top: 1.5em solid transparent;
            border-bottom: 1.5em solid transparent;
            border-left: 1em solid #ddd;
            right: -1em;
        }

        #crouton li:not(:last-child) > a:hover::after {
            border-left-color: #99db76;
        }
        hr {
            position: relative;
            top: 40px;
            border: none;
            height: 6px;
            background: black;
            margin-bottom: 30px;
        }
    </style>

    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();
        });
    </script>
@endsection
