@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>
    <br>
    <br>
    <br>
    <br>

    <div class="container" style="height: 850px; width: 1000px;">
        <div class="row">
            <a class="waves-effect waves-light btn" href="/show-events"><i class="material-icons left">remove_red_eye</i>Upcoming Events</a>
            <a class="waves-effect waves-light btn" href="/show-all-events"><i class="material-icons left">remove_red_eye</i>View All Events</a>
        </div>
        <div class="row">

            {!! $calendar->calendar() !!}
            {!! $calendar->script() !!}
        </div>
    </div>
    <div id="register-modal" class="modal">
        <div class="modal-content">
            <h5>One more step!</h5>

            <select id="found-us-via-input">
                <option value="" disabled selected>How did you hear about us?</option>
                <option value="Radio">Radio</option>
                <option value="Email Campaign">Email Campaign</option>
                <option value="Facebook">Facebook</option>
                <option value="Website">Website</option>
                <option value="LinkedIn">LinkedIn</option>
                <option value="Twitter">Twitter</option>
                <option value="Print Media">Print Media</option>
                <option value="Other">Other</option>
            </select>
            <input id="found-us-via-other-input" hidden placeholder="Please specify?" type="text">

        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat" onclick="saveFoundOutDetails()">Submit</a>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    @push('custom-scripts')
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

        <script>
            $(document).ready(function () {
                $(function () {
                    $('#calendar').fullCalendar({


                        editable: true,
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay,listMonth'
                        },
                        // eventClick: function(info){
                        //     alert('Please click on the view events button to view ' + info.event.title);
                        // },
                        select: function (start, end, jsEvent, view){
                            var root_url="https://google.com/";
                            window.location = root_url
                        }
                    });
                });
            });

            // function alertMessage(info) {
            //     alert('Please click on "View Events" to view ' + info.title + "'s" + ' details');
            // }
        </script>
    @endpush
<style>
    nav {
        margin-bottom: 0;
        background-color: grey;
    }
</style>
@endsection
