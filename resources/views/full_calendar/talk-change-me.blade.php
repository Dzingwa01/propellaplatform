@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Events/showEvents.css"/>
    <!--Desktop-->
    <div class="row" style="text-align: center;margin-top:4em;">
        @if($events_array != null)
            @foreach ($events_array as $event_list)
                <div class="col s12 m3">
                    <div class="card white hoverable center" style="border-radius: 20px;">
                        <div class="circle">
                            <div class="card-content black-text" style="height: 100%;">
                                <div class="user-view" align="center">
                                    <img class="fullscreen materialboxed" style="width: 100%; height: 200px;"
                                         src="{{isset($event_list->event->event_image_url)?'/storage/'.$event_list->event->event_image_url:''}}">
                                </div>
                                <h6 class="">{{$event_list->event->title}}</h6>
                                <h6 class="" id="start"
                                    value="{{$event_list->start_time}}">{{$event_list->start_time}} </h6>
                                <h6>{{$event_list->event->start->toDateString()}}</h6>
                                @if(isset($event_list->event->EventVenue->venue_name))
                                    <h6 class=""> {{$event_list->event->EventVenue->venue_name}} </h6>
                                @else
                                    <h6 class=""> Old Building </h6>
                                @endif
                                @if($event_list->event->type == 'Private')
                                    <h6 class="private-event-type-header" style="color:red;">
                                        <b>{{$event_list->event->type}}</b></h6>
                                @else
                                    <h6 class="event-type-header">{{$event_list->event->type}} </h6>
                                @endif

                            </div>
                            <div class="card-action">
                                @if($event_list->event->start > new DateTime())
                                    @if($event_list->participant_count > $event_list->registered_participants)
                                        <div class="row" style="margin-left:1em;">
                                            <a href="#register-modal" class="modal-trigger"
                                               data-value="{{$event_list->event->id}}"
                                               data-type="{{$event_list->event->type}}"
                                               style="color: orange;" onclick="saveEventDetails(this)">Register
                                                | Deregister</a>
                                        </div>
                                    @else
                                        <div class="row" style="margin-left:1em;">
                                            <a href="#participant-notification-modal" class="modal-trigger"
                                               style="color: orange;">Register
                                                | Deregister</a>
                                        </div>
                                    @endif
                                @endif

                                <div class="row" style="margin-left: 1em;">
                                    <a href="#event-detail-modal" class="modal-trigger"
                                       data-value="{{$event_list->event->invite_image_url}}" onclick="showModal(this)"
                                       style="color: orange;">Event Details</a>
                                </div>

                                @if(new DateTime() > $event_list->event->end)
                                    <div class="row" style="margin-left: 1em;">
                                        <div class="view_media" id="photo">
                                            <a style="color: orange;"
                                               href="{{url('/full_calendar/show-gallery/'.$event_list->event->id)}}">View
                                                Gallery</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif($events_array == null)
            <div class="row" style="margin-left: 1em;">
                <h3>There Are No Events Today</h3>
            </div>
        @endif
    </div>

    <div id="register-modal" class="modal">
        <div class="modal-content">
            <h5>One more step!</h5>

            <select id="found-us-via-input">
                <option value="" disabled selected>How did you hear about us?</option>
                <option value="Radio">Radio</option>
                <option value="Email Campaign">Email Campaign</option>
                <option value="Facebook">Facebook</option>
                <option value="Website">Website</option>
                <option value="LinkedIn">LinkedIn</option>
                <option value="Twitter">Twitter</option>
                <option value="Print Media">Print Media</option>
                <option value="Other">Other</option>
            </select>
            <input id="found-us-via-other-input" hidden placeholder="Please specify?" type="text">

        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat" onclick="saveFoundOutDetails()">Submit</a>
        </div>
    </div>

    <div id="participant-notification-modal" class="modal">
        <div class="modal-content">
            <h5>So sorry that you missed out.</h5>

            <p>Unfortunately this event has exceeded the amount of participants that can register for this event.</p>
            <p>Please contact Aphelele Jonas at <a href="mailto:ahjonas@propellaincubator.co.za">agjonas@propellaincubator.co.za</a>
                for any enquiries on how you could possibly register.</p>
        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    <div id="event-detail-modal" class="modal">
        <div class="modal-content" id="modal-image-div">

        </div>
        <div class="modal-footer">
            <a href="#" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>

        $(document).ready(function () {
            $('.modal').modal();
            $('#found-us-via-input').change(function () {
                if ($('#found-us-via-input').val() === 'Other') {
                    $('#found-us-via-other-input').show();
                } else {
                    $('#found-us-via-other-input').hide();
                }
            });
        });


        function viewMedia(obj) {
            let event_id = obj.getAttribute('data-value');

            location.href = '/home/event-show-gallery/' + event_id;
        }

        function saveEventDetails(obj) {
            let event_id = obj.getAttribute('data-value');
            let event_type = obj.getAttribute('data-type');
            sessionStorage.setItem('stored_event_id', event_id);
            sessionStorage.setItem('stored_event_type', event_type);
        }

        function saveFoundOutDetails() {
            if ($('#found-us-via-input').val() === "") {
                alert('Please choose an option.');
            } else {
                let found_out_via;
                let event_type = sessionStorage.getItem('stored_event_type');

                if ($('#found-us-via-input').val() === 'Other') {
                    found_out_via = $('#found-us-via-other-input').val();
                } else {
                    found_out_via = $('#found-us-via-input').val();
                }

                sessionStorage.setItem('stored_found_out_via', found_out_via);

                if (event_type === 'Public') {
                    window.location.href = '/visitors/visitor-login';
                } else if (event_type === 'Private') {
                    window.location.href = '/login';
                } else if (event_type === 'View Only') {
                    alert('Sorry, this event is for viewing purposes only.');
                }

            }
        }

        function showModal(obj) {
            $('.modal').modal({
                onOpenStart: function () {
                    let image_source = obj.getAttribute('data-value');
                    $('#modal-image-div').html(
                        '<div class="row" style="height: 100%; width: 100%">'
                        +
                        '<img id="event-detail-modal-image" src=' + image_source + '"/storage" alt="No Image Uploaded" style="height: 100%; width: 100%;">'
                        +
                        '</div>'
                    );
                }
            });
        }

        $('.private-event-type-header').hover(function () {
            $(this).html(
                '<p>' + 'So sorry, this is an internal event.' + '</p>'
            )
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
