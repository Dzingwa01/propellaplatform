@extends('layouts.applicant-layout')

@section('content')

    <br>
    <link rel="stylesheet" type="text/css" href="/css/ApplicantDashboard/ApplicantDashboard.css">

    <div id="applicantDashboardDesktop">
        {{ Breadcrumbs::render('applicant')}}
        <br />
        <br>
        <h6 style="text-align: center">Welcome <b>{{$user->applicant->venture_name}}</b></h6>
        <h6 style="text-align: center">Category : {{$user->applicant->chosen_category}}</h6>

        <div class="row">
            <div class="col s12 m4" style="height: 400px">
                <div class="card darken-1 hoverable"style="border-radius: 15px;background-color:#454242;">
                    <div class="card-content white-text">
                        <h5 style="margin-left: 7em;">Status</h5>
                        <p style="color:white;">Received.</p><br>
                        <p style="color:white;">Application Pending...</p>
                    </div>
                    <div class="card-action"style="border-radius: 15px">
                    </div>
                </div>
            </div>
            <div class="col s12 m4" style="height: 400px">
                <div class="card darken-1 hoverable"style="border-radius: 15px;background-color:rgba(0, 47, 95, 1);">
                    <div class="card-content white-text">
                        <h5 style="margin-left: 6em">Application</h5>
                    </div>
                    <div class="card-action"style="border-radius: 15px">
                        <div class="center-align">
                            <a class="btn" href="{{url('/applicant-edit-application/'.$user->id)}}" style="color: white;background-color:rgba(0, 47, 95, 1);">Edit</a>
                            @if ($user->applicant->application_completed == false)
                                <a class="btn" href="{{url('/destroyOwnApplication/'.$user->id)}}" style="color: white;background-color:rgba(0, 47, 95, 1);">Delete</a>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
            <div class="col s12 m4" style="height: 400px">
                <div class="card darken-1 hoverable" style="border-radius: 15px;background-color:rgba(0, 121, 52, 1);">
                    <div class="card-content white-text">
                        <h5 style="margin-left: 7em;">Details</h5>
                    </div>
                    <div class="card-action"style="border-radius: 15px">
                        <div class="center-align">
                            <a class="btn" href="{{(url('/applicant-edit-basic-info/'.$user->id))}}" style="color: white;background-color:rgba(0, 121, 52, 1);">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            nav {
                margin-bottom: 0;
                background-color: grey;
                padding: 5px 16px;
            }
        </style>
    </div>


    <div id="applicantDashboardMobile">
        {{ Breadcrumbs::render('applicant')}}
        <br />
        <br>
        <h5 style="text-align: center">Welcome <b>{{$user->applicant->venture_name}}</b></h5>
        <h5 style="text-align: center">Category : {{$user->applicant->chosen_category}}</h5>

        <div class="row">
            <div class="col s12 m4">
                <div class="card darken-1 hoverable"style="border-radius: 15px;background-color:#454242;">
                    <div class="card-content white-text">
                        <h5 style="margin-left: 7em;">Status</h5>
                        <p style="color:white;">Received.</p><br>
                        <p style="color:white;">Application Pending...</p>
                    </div>
                    <div class="card-action"style="border-radius: 15px">
                    </div>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="card darken-1 hoverable"style="border-radius: 15px;background-color:rgba(0, 47, 95, 1);">
                    <div class="card-content white-text">
                        <h5 style="margin-left: 6em">Application</h5>
                    </div>
                    <div class="card-action"style="border-radius: 15px">
                        <div class="center-align">
                            <a class="btn" href="{{url('/applicant-edit-application/'.$user->id)}}" style="color: white;background-color:rgba(0, 47, 95, 1);">Edit</a>
                            @if ($user->applicant->application_completed == false)
                                <a class="btn" href="{{url('/destroyOwnApplication/'.$user->id)}}" style="color: white;background-color:rgba(0, 47, 95, 1);">Delete</a>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="card darken-1 hoverable" style="border-radius: 15px;background-color:rgba(0, 121, 52, 1);">
                    <div class="card-content white-text">
                        <h5 style="margin-left: 7em;">Details</h5>
                    </div>
                    <div class="card-action"style="border-radius: 15px">
                        <div class="center-align">
                            <a class="btn" href="{{(url('/applicant-edit-basic-info/'.$user->id))}}" style="color: white;background-color:rgba(0, 121, 52, 1);">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            nav {
                margin-bottom: 0;
                background-color: grey;
                padding: 5px 16px;
            }
        </style>
    </div>

@endsection
