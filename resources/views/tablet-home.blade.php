@extends('layouts.tablet-layout')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Tablet/tablet.css"/>

    <input hidden disabled id="user-id-input" value="{{$user->id}}">

    <div class="" id="tab" style="margin-left:5em">
        <div class="col s12 m4" style="height: 400px;margin-top: 10vh">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: rgba(0, 121, 52, 1);width: 600px ">
                <div class="card-content white-text center-align">
                    <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i>   Book Venues</span>
                    <br/>
                    <p style="align-content: center; ">This is where you view your booked venues</p>
                    <br/>
                </div>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="btn" id="user-venue-button" href="{{url('/show-booking/'.$user->id)}}" style="cursor: pointer; color: white;background-color: rgba(0, 121, 52, 1)"><b>View Venues</b></a>
                </div>
            </div>
        </div>
        <div class="col s12 m4" style="height: 400px;">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: rgba(0, 121, 52, 1);width: 600px ">
                <div class="card-content white-text center-align">
                    <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i>COVID-19 Forms</span>
                    <br/>
                    <p style="align-content: center; ">Today's Forms</p>
                    <br/>
                </div>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="btn" id="user-venue-button" href="{{url('/view-covid-forms')}}" style="cursor: pointer; color: white;background-color: rgba(0, 121, 52, 1)"><b>View</b></a>
                </div>
            </div>
        </div>
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <div class="" style="color:black;font-weight: bolder;margin-left: 80%" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
        <a style="color:black;" href="{{ url('/logout') }}" class=""
        ><i
                class="tiny material-icons">vpn_key</i>
            Sign Out
        </a>
    </div>

    @endsection
