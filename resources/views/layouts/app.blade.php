<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!=='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PTWSFMC');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Propella Platform</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">


    <link rel="stylesheet" href="/css/platform_styles.css"/>
    <link rel="stylesheet" href="/css/site.css"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    {{--<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>--}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

{{--    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>--}}
    <script src="sweetalert2.all.min.js"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

</head>
<body>
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTWSFMC"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->
        <ul id="dropdown1" class="dropdown-content">
            {{--<li><a class="nav-tab" href="/Application-Process/question-category">Apply</a></li>
          <li><a class="nav-tab" href="/visitors/visitor-login">Visitor Profile</a></li>--}}
          <li><a class="nav-tab" href="{{url('login')}}">Propella Profile</a></li>
          {{--<li><a class="nav-tab" href="{{url('register')}}">Register</a></li>--}}

        </ul>
        <ul id="dropdown2" class="dropdown-content">
            {{--<li><a class="nav-tab" href="/Application-Process/question-category">Apply</a></li>
            <li><a class="nav-tab" href="#">Visitor</a></li>--}}
            <li><a class="nav-tab" href="{{url('login')}}">Propella</a></li>
        </ul>
        <ul id="dropdown3" class="dropdown-content">
          {{--  <li><a class="nav-tab" href="/blogs">Blog</a></li>--}}
            <li><a class="nav-tab"  href="{{url('/media')}}">Media</a></li>
          {{--  <li><a class="nav-tab"  href="{{url('/home/gallery/')}}">Gallery</a></li>--}}
            <li><a class="nav-tab"  href="{{url('/full_calendar/public-event')}}">Events</a></li>
            {{--<li><a class="nav-tab" href="/learning-curves">Learning Curves</a></li>--}}
            <li><a class="nav-tab" href="/hedgeSa">Salutaris</a></li>
           {{-- <li><a class="nav-tab" href="/video-gallery">Video gallery</a></li>
            <li><a class="nav-tab" href="/news">News</a></li>--}}
        </ul>

        <ul id="dropdown7" class="dropdown-content">
            <li><a class="nav-tab" href="/Home/Categories">Current Ventures</a></li>
            <li><a class="nav-tab" href="/alumni-categories">Alumni</a></li>
            <li><a class="nav-tab" href="/exited-categories">Exited</a></li>
            <li><a class="nav-tab" href="/Home/Cohorts">Cohorts</a></li>
            <li><a class="nav-tab" href="/home/SmartCity">Smart City</a></li>
            <li><a class="nav-tab" href="/home/SDG">SDG's</a></li>
        </ul>
        <ul id="dropdown8" class="dropdown-content">
            <li><a class="nav-tab" href="/Home/Categories">Current Ventures</a></li>
            <li><a class="nav-tab" href="/alumni-categories">Alumni</a></li>
            <li><a class="nav-tab" href="/exited-categories">Exited</a></li>
            <li><a class="nav-tab" href="/Home/Cohorts">Cohorts</a></li>
            <li><a class="nav-tab" href="/home/SmartCity">Smart City</a></li>
            <li><a class="nav-tab" href="/home/SDG">SDG's</a></li>

        </ul>
        <ul id="dropdown20" class="dropdown-content">

        </ul>

        <ul id="dropdown21" class="dropdown-content">
            <li><a class="nav-tab" href="/home/AboutUs">About us</a></li>
            <li><a class="nav-tab" href="/home/Services">Services</a></li>
            <li><a class="nav-tab" href="/home/Programme">Programme</a></li>
            <li><a class="nav-tab" href="/home/Apply">Apply</a></li>
            <li><a class="nav-tab" href="/home/Contact">Contact</a></li>
            <li><a class="nav-tab" href="/home/show-satellite-gallery">Media</a></li>


        </ul>

        <ul id="dropdown4" class="dropdown-content">

            <li><a class="nav-tab" href="/media">Media</a></li>
            <li><a class="nav-tab" href="{{url('/full_calendar/public-event')}}">Events</a></li>
            <li><a class="nav-tab" href="/hedgeSa">Salutaris</a></li>
        </ul>

        <div id="app">
            <div class="navbar-fixed">
                <nav class="white">
                    <div class="nav-wrapper">
                        <a   href="/" class="brand-logo">
                            <img style="margin-left:1em;width: 200px" src="/images/Propella_Logo.jpg" class="propella-logo"/></a>
                        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                        <ul class="right hide-on-med-and-down">
                            <li><a class="nav-tab" href="/"><i class="material-icons left"></i>Home</a></li>
                            <li><a class="nav-tab" href="/Application-Process/question-category"><i class="material-icons left"></i>Apply</a></li>
                            {{--<li><a class="nav-tab" href="/test-notification/welcome"><i class="material-icons left">info_outline</i>Test</a></li>--}}
                            <li><a class="nav-tab" href="/Home/About"><i class="material-icons left"></i>About</a></li>
                            <li><a class="nav-tab" href="/Home/Partners"><i class="material-icons left"></i>Partner</a></li>
                            <li><a class="nav-tab" href="/Home/Innovators"><i class="material-icons left"></i>Innovators</a></li>
                            <li><a class="dropdown-trigger-cus nav-tab" href="#!" data-target="dropdown8">Venture<i
                                        class="material-icons right">arrow_drop_down</i></a></li>
                            <li><a class="nav-tab" href="{{url('/media')}}"><i class="material-icons left"></i>Media</a></li>
                            <li><a class="nav-tab" href="/Home/contactUs"><i class="material-icons left"></i>Contact</a></li>
                            <li><a class="nav-tab" href="/donation-page"><i class="material-icons left"></i>Invest</a></li>

                            <li><a class="dropdown-trigger-cus nav-tab" href="#!" data-target="dropdown1">Account<i
                                        class="material-icons right">arrow_drop_down</i></a></li>
                        </ul>
                    </div>
                </nav>
            </div>
            <ul class="sidenav" id="mobile-demo">
                <li><a class="nav-tab" href="/"><i class="material-icons left">home</i>Home</a></li>
                <li><a class="nav-tab" href="/Application-Process/question-category"><i class="material-icons left">border_color</i>Apply</a></li>
                <li><a class="nav-tab" href="/Home/About"><i class="material-icons left">info_outline</i>About</a></li>
                <li><a class="nav-tab" href="/Home/Partners"><i class="material-icons left">wc</i>Partner</a></li>
                {{--<li><a href="/Home/Calendar"><i class="material-icons left">assignment</i>Calendar</a></li>--}}
                <li><a class="nav-tab" href="/Home/Innovators"><i class="material-icons left">android</i>Innovators</a>
                </li>

                <li><a class="dropdown-trigger-cus nav-tab" href="#!" data-target="dropdown7"><i class="material-icons left">group</i>Venture<i
                            class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class="dropdown-trigger-cus nav-tab" href="#!" data-target="dropdown4"><i class="material-icons left">video_library</i>Media<i
                            class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class="nav-tab" href="/Home/contactUs"><i class="material-icons left">phone</i>Contact</a></li>
                <li><a class="nav-tab" href="/donation-page"><i class="material-icons left">money</i>Invest</a></li>
                <li><a class="dropdown-trigger-cus nav-tab" href="{{url('login')}}"><i class="material-icons left">login</i>Account Login</a></li>
            </ul>
            <div class="container-fluid">
                @yield('content')
            </div>
            <!-- Site footer -->
            <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>

            <footer class="footer">
                <br>
                <div class="rounded-social-buttons">
                    <a class="social-button facebook" href="https://www.facebook.com/Propella-Business-Incubator-1531773490473015" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a class="social-button twitter" href="https://twitter.com/PropellaHub/" target="_blank"><i class="fab fa-twitter"></i></a>
                    <a class="social-button linkedin" href="https://www.linkedin.com/company/propella-business-incubator/" target="_blank"><i class="fab fa-linkedin"></i></a>
                    <a class="social-button youtube" href="https://www.youtube.com/results?search_query=propella+business+incubator" target="_blank"><i class="fab fa-youtube"></i></a>
                    <a class="social-button instagram" href="https://www.instagram.com/propellahub/" target="_blank"><i class="fab fa-instagram"></i></a>
                    <a class="social-button instagram" href="https://goo.gl/maps/XuWMyMQ6g9FVk88D7" target="_blank"><i class="fas fa-map-marker-alt"></i></a>
                    <div class="copyright">© Propella Business Incubator. All rights reserved.</div>
                </div>

            </footer>

            {{--<footer class="site-footer">
                <div class="container">--}}

           {{-- <footer class="page-footer darken-2 grey" style="background-color:#454242">
                <div class="container-fluid">
                    <div class="row">
                      --}}{{--  <div class="col l6 s12" style="font-weight: bolder">
                            <h5 class="white-text" style="font-weight: bolder">Contacts</h5>
                            <p class="grey-text text-lighten-4">For general information about the Propella Business Incubator,
                                please contact us at:</p>
                            <ul>
                                <li>
                                    <a style="color:white!important;" href="mailto:info@propellaincubator.co.za"><i
                                            class="material-icons">contact_mail</i> info@propellaincubator.co.za</a></li>
                                <li><a style="color:white!important;" href="#"><i
                                        class="material-icons">contact_mail</i>Contact number :   072 096 5106</a></li>

                                <li><a style="color:white!important;" href="#"><i class="material-icons">contact_phone</i> +27
                                        41 502 3700</a></li>
                                <li><a style="color:white!important;" href="#"><i class="material-icons">print</i> 0861 55 55 33</a>
                                </li>
                                <li><a style="color:white!important;" href="#"><i class="material-icons">business</i>   7 Oakworth Road, South End, Port Elizabeth 6001</a></li>
                            </ul>
                        </div>--}}{{--
                        <div class="row" style="font-weight: bolder">
                            <div class="col s2">
                                <a class="white-text" href="https://www.facebook.com/Propella-Business-Incubator-1531773490473015/?fref=ts"><i class="fab fa-facebook fa-2x"></i></a></li>
                            </div>
                            <div class="col s2">
                                <a class="white-text" href="https://twitter.com/PropellaHub" target="_blank" style="margin-left: 1em;"><i class="fab fa-twitter fa-2x"></i></a>
                            </div>
                            <div class="col s2">
                                <a class="white-text" href="https://www.instagram.com/propellahub/" style="margin-left: 1em;"><i class="fab fa-instagram fa-2x"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container-fluid">
                        Made by <a class="brown-text text-lighten-3" href="!#"><span style="color: white"><b>Propella Dev Team</b></span></a>
                    </div>
                </div>
            </footer>--}}
        </div>
        <style>
            .rounded-social-buttons {
                text-align: center;
            }

            .rounded-social-buttons .social-button {
                display: inline-block;
                position: relative;
                cursor: pointer;
                width: 3.125rem;
                height: 3.125rem;
                border: 0.125rem solid transparent;
                padding: 0;
                text-decoration: none;
                text-align: center;
                color: #fefefe;
                font-size: 1.5625rem;
                font-weight: normal;
                line-height: 2em;
                border-radius: 1.6875rem;
                transition: all 0.5s ease;
                margin-right: 0.25rem;
                margin-bottom: 0.25rem;
            }

            .rounded-social-buttons .social-button:hover, .rounded-social-buttons .social-button:focus {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                transform: rotate(360deg);
            }

            .rounded-social-buttons .fa-twitter, .fa-facebook-f, .fa-linkedin, .fa-youtube, .fa-instagram {
                font-size: 25px;
            }

            .rounded-social-buttons .social-button.facebook {
                background: #3b5998;
            }

            .rounded-social-buttons .social-button.facebook:hover, .rounded-social-buttons .social-button.facebook:focus {
                color: #3b5998;
                background: #fefefe;
                border-color: #3b5998;
            }

            .rounded-social-buttons .social-button.twitter {
                background: #55acee;
            }

            .rounded-social-buttons .social-button.twitter:hover, .rounded-social-buttons .social-button.twitter:focus {
                color: #55acee;
                background: #fefefe;
                border-color: #55acee;
            }

            .rounded-social-buttons .social-button.linkedin {
                background: #007bb5;
            }

            .rounded-social-buttons .social-button.linkedin:hover, .rounded-social-buttons .social-button.linkedin:focus {
                color: #007bb5;
                background: #fefefe;
                border-color: #007bb5;
            }

            .rounded-social-buttons .social-button.youtube {
                background: #bb0000;
            }

            .rounded-social-buttons .social-button.youtube:hover, .rounded-social-buttons .social-button.youtube:focus {
                color: #bb0000;
                background: #fefefe;
                border-color: #bb0000;
            }

            .rounded-social-buttons .social-button.instagram {
                background: #125688;
            }

            .rounded-social-buttons .social-button.instagram:hover, .rounded-social-buttons .social-button.instagram:focus {
                color: #125688;
                background: #fefefe;
                border-color: #125688;
            }


    .nav-tab {
        font-weight: bolder !important;

    }
    .nav-tab {
        font-weight: bolder !important;

    }
    @media screen and (max-width: 767px) {
        nav a{
            color:black!important;
        }
        .propella-logo{
            height: 57px!important;

        }
    }
            .footer  {
                line-height: 50px;
                width: 100%;
                color: #fff;
                background-color: #454242;
                min-height: 100%;
                bottom: 0;
                margin-top:-59px;
                height: auto !important;
            }

            .site-footer hr
    {
        border-top-color:#bbb;
        opacity:0.5
    }
    .site-footer hr.small
    {
        margin:20px 0
    }
    .site-footer h6
    {
        color:#fff;
        font-size:16px;
        text-transform:uppercase;
        margin-top:5px;
        letter-spacing:2px
    }
    .site-footer a
    {
        color:#737373;
    }
    .site-footer a:hover
    {
        color:#3366cc;
        text-decoration:none;
    }
    .footer-links
    {
        padding-left:0;
        list-style:none
    }
    .footer-links li
    {
        display:block
    }
    .footer-links a
    {
        color:#737373
    }
    .footer-links a:active,.footer-links a:focus,.footer-links a:hover
    {
        color:#3366cc;
        text-decoration:none;
    }
    .footer-links.inline li
    {
        display:inline-block
    }
    .site-footer .social-icons
    {
        text-align:right
    }
    .site-footer .social-icons a
    {
        width:40px;
        height:40px;
        line-height:40px;
        margin-left:6px;
        margin-right:0;
        border-radius:100%;
        background-color:#33353d
    }
    .copyright-text
    {
        margin:0
    }
    @media (max-width:991px)
    {
        .site-footer [class^=col-]
        {
            margin-bottom:30px
        }
    }
    @media (max-width:767px)
    {
        .site-footer
        {
            padding-bottom:0
        }
        .site-footer .copyright-text,.site-footer .social-icons
        {
            text-align:center
        }
    }
    .social-icons
    {
        padding-left:0;
        margin-bottom:0;
        list-style:none
    }
    .social-icons li
    {
        display:inline-block;
        margin-bottom:4px
    }
    .social-icons li.title
    {
        margin-right:15px;
        text-transform:uppercase;
        color:#96a2b2;
        font-weight:700;
        font-size:13px
    }
    .social-icons a{
        background-color:#eceeef;
        color:#818a91;
        font-size:16px;
        display:inline-block;
        line-height:44px;
        width:44px;
        height:44px;
        text-align:center;
        margin-right:8px;
        border-radius:100%;
        -webkit-transition:all .2s linear;
        -o-transition:all .2s linear;
        transition:all .2s linear
    }
    .social-icons a:active,.social-icons a:focus,.social-icons a:hover
    {
        color:#fff;
        background-color:#29aafe
    }
    .social-icons.size-sm a
    {
        line-height:34px;
        height:34px;
        width:34px;
        font-size:14px
    }
    .social-icons a.facebook:hover
    {
        background-color:#3b5998
    }
    .social-icons a.twitter:hover
    {
        background-color:#00aced
    }
    .social-icons a.linkedin:hover
    {
        background-color:#007bb6
    }
    .social-icons a.dribbble:hover
    {
        background-color:#ea4c89
    }
    @media (max-width:767px)
    {
        .social-icons li.title
        {
            display:block;
            margin-right:0;
            font-weight:600
        }
    }

</style>
{{--<script--}}
{{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
{{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
{{--crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" src="/js/notify/notify.js"></script>
<script type="text/javascript" src="/js/notify/notify.min.js"></script>

{{--<script src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/widget.js'></script>--}}

<script>
    $(document).ready(function () {
//        M.AutoInit();
        $('.parallax').parallax();
        $('.sidenav').sidenav();
        $('.dropdown-trigger-cus').dropdown();
        $('.dropdown-trigger-cus-3').dropdown();
        $('.dropdown-trigger-c').dropdown();
        $('select').formSelect();
        $('.carousel').carousel({
            fullWidth: true,
            indicators: true
        });
    });


    let image;
    var botmanWidget = {
        title: '💡  Bulb ',
        aboutText: 'Propella Business Incubator',
        introMessage: ' Hello, I am Bulb from Propella Business Incubator. I am here to assist you and answer all your questions about Propella.' +'<br>'+ '<br>'+ '<img src="https://cdn.vectorstock.com/i/thumb-large/49/25/cartoon-light-bulb-vector-18784925.jpg">'+ '<br>'+'<br>'+'Please type Hi',
        displayMessageTime : 'true',
        content: "Sample hover text.",
        bubbleAvatarUrl: 'https://cdn.vectorstock.com/i/thumb-large/49/25/cartoon-light-bulb-vector-18784925.jpg',
        aboutLink : 'https://thepropella.co.za/',
    };




</script>

@stack('custom-scripts')
</body>
</html>
