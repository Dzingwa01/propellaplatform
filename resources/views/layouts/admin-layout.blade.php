 <!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Propella Platform</title>
    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="/css/Users/create.css"/>
    {{--<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>--}}
    <link href="/css/site-styles.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    {{--<link href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css" rel="stylesheet"/>--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    {{--<link href="/css/jquery-step-maker.css" type="text/css" rel="stylesheet" media="screen,projection"/>--}}
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <!-- Compiled and minified JavaScript -->
</head>
<style>
    .darknav {
        background: darkblue;
        color:white;
        font-weight: bolder

    }
    .darknav a{
        color:white
    }
    .darknav:hover {
        background: whitesmoke;
        border: 2px solid darkblue;
        font-weight: bolder;
        color:black
    }

    .darknav:hover a {
        color:black;
        font-weight: bolder
    }

    <!-- -->
    .greynav {
        background: gray;
        color:white;
        font-weight: bolder

    }
    .greynav a{
        color:white
    }
    .greynav:hover {
        background: whitesmoke;
        border: 2px solid gray;
        font-weight: bolder;
        color:black
    }

    .greynav:hover a {
        color:black;
        font-weight: bolder
    }
    <!-- -->
    .greennav {
        background: green;
        color:white;
        font-weight: bolder

    }
    .greennav a{
        color:white
    }
    .greennav:hover {
        background: whitesmoke;
        border: 2px solid green;
        font-weight: bolder;
        color:black
    }

    .greennav:hover a {
        color:black;
        font-weight: bolder
    }
    <!-- -->
    .lightBluenav {
        background: blue;
        color:white;
        font-weight: bolder

    }
    .lightBluenav a{
        color:white
    }
    .lightBluenav:hover {
        background: whitesmoke;
        border: 2px solid blue;
        font-weight: bolder;
        color:black
    }

    .lightBluenav:hover a {
        color:black;
        font-weight: bolder
    }

</style>
<body>

<div class="navbar-fixed">
    <nav class="white" role="navigation" style="height: 100px!important;">
        <div class="nav-wrapper">
            <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <a id="logo-container" href="{{url('/home')}}" class="brand-logo center">
                <img src="/images/Propella_Logo.jpg"/>
            </a>
            <ul class="right hide-on-med-and-down">
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Account<i
                            class="material-icons right">arrow_drop_down</i></a></li>
            </ul>

        </div>


    </nav>
    <ul id="dropdown1" class="dropdown-content">
        <li>
            <a href="{{!is_null(\Illuminate\Support\Facades\Auth::user()->profile_picture_url)?\Illuminate\Support\Facades\Auth::user()->profile_picture_url:'/images/profile_placeholder.jpg'}}{{!is_null(\Illuminate\Support\Facades\Auth::user()->profile_picture_url)?\Illuminate\Support\Facades\Auth::user()->profile_picture_url:'/images/profile_placeholder.jpg'}}">Profile</a>
        </li>
        <li><a style="color:black;" href="{{ url('/logout') }}" class=""
               onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Sign Out
            </a></li>
    </ul>
    <ul id="slide-out" class="sidenav ">
        <li>
            <div class="user-view">
                <a href="#user"><img class="circle"
                                     src="{{!is_null(\Illuminate\Support\Facades\Auth::user()->profile_picture_url)?\Illuminate\Support\Facades\Auth::user()->profile_picture_url:'/img/profile_placeholder.jpg'}}"/></a>
                <a href="#name"><span class="name"
                                      style="color:black;font-weight: bolder">{{\Illuminate\Support\Facades\Auth::user()->name . " ".\Illuminate\Support\Facades\Auth::user()->surname}}</span></a>
                <a href="#email"><span class="email"
                                       style="color:black;font-weight: bolder">{{\Illuminate\Support\Facades\Auth::user()->roles[0]->display_name}}</span></a>

            </div>
        </li>
        <div class="divider"></div>

        <!-- HOME -->
        <ul class="collapsible popout" style="margin-top:1em;" onclick="dashboard_show()">
            <li class="darknav">
                <div class="collapsible-header"><i class="tiny material-icons">home</i><a
                        href="/home"> Home</a>
                </div>
                <div class="collapsible-body">
                </div>
            </li>
        </ul>
        <!-- USERS -->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="darknav">
                <div class="collapsible-header"><i class="tiny material-icons">person</i><a  >Users</a>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('users-index')}}"><i
                                    class="tiny material-icons">business_center</i>View Users</a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('indexRole')}}"><i
                                    class="tiny material-icons">business_center</i>View Roles</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!-- EMAILER -->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="darknav">
                <div class="collapsible-header"><i
                        class="tiny material-icons">contact_mail</i>
                    E-Mailer
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a style="color:black;font-weight: bolder; cursor: pointer;" class=""
                               onclick="bulk_mailer()">
                                <i class="tiny material-icons">control_point</i>Companies</a></li>
                        <li><a style="color:black;font-weight: bolder; cursor: pointer;" class=""
                               onclick="visitor_bulk_mailer()">
                                <i class="tiny material-icons">control_point</i>Visitors</a></li>
                        <li><a style="color:black;font-weight: bolder; cursor: pointer;" class="" onclick="incubatee_bulk_mailer_index()">
                                <i class="tiny material-icons">control_point</i>All users</a></li>
                        <li><a style="color:black;font-weight: bolder; cursor: pointer;" class="" onclick="venture_bulk_mailer_index()">
                                <i class="tiny material-icons">control_point</i>All Ventures</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!-- Application Admin -->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greynav">
                <div class="collapsible-header"><i class="tiny material-icons">business_center</i>
                    Application Admin
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{url('questionIndex')}}">
                                <i class="tiny material-icons">control_point</i>Manage Questions </a></li>
                        <li><a href="{{url('categoryIndex')}}">
                                <i class="tiny material-icons">control_point</i>Manage Category </a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!-- VENTURE ADMIN -->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greynav">
                <div class="collapsible-header"><i class="tiny material-icons">account_balance</i><a
                        class="">Venture Admin</a>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('venture-type')}}"><i
                                    class="tiny material-icons">business_center</i>Venture Types</a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('venture-status')}}"><i
                                    class="tiny material-icons">business_center</i>Venture Status</a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('venture-category')}}"><i
                                    class="tiny material-icons">account_circle</i>Hub Categories</a></li>

                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('smart-tag')}}"><i
                                    class="tiny material-icons">business_center</i>Smart City Tags</a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('business-category')}}"><i
                                    class="tiny material-icons">business_center</i>Business Categories</a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('incubatee-stages')}}"><i
                                    class="tiny material-icons">account_circle</i>Manage Stages</a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="/venture-panel-interview-category-index"><i
                                    class="tiny material-icons">account_circle</i>Interview Categories</a></li>

                    </ul>
                </div>
            </li>
        </ul>
        <!--Media Admin-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greynav">
                <div class="collapsible-header"><i class="tiny material-icons">chat_bubble_outline</i><a
                        class="">Media Admin </a>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a class="" href="{{url('blog-index')}}"><i
                                    class="tiny material-icons">business_center</i>Blogs</a></li>
                    </ul>
                    <ul>
                        <li><a  class="" href="{{url('video-index')}}"><i
                                    class="tiny material-icons">business_center</i>Videos</a></li>
                    </ul>
                    <ul>
                        <li><a  class="" href="{{url('propella-news-index')}}"><i
                                    class="tiny material-icons">business_center</i>News</a></li>
                    </ul>
                 {{--   <ul>
                        <li><a  class="" href="{{url('gallery-index')}}"><i
                                    class="tiny material-icons">business_center</i>PTI Gallery</a></li>
                    </ul>--}}
                    <ul>
                        <li><a  class="" href="{{url('get-learning-curves')}}"><i
                                    class="tiny material-icons">business_center</i>Learning curves</a></li>
                    </ul>
                    <ul>
                        <li><a  class="" href="{{url('career-index')}}"><i
                                    class="tiny material-icons">business_center</i>Careers</a></li>
                    </ul>
                    <ul>
                        <li><a  class="" href="{{url('latest-info-index')}}"><i
                                    class="tiny material-icons">business_center</i>Latest posts</a></li>
                    </ul>
                    <ul>
                        <li><a  class="" href="{{url('newsletter-index')}}"><i
                                    class="tiny material-icons">business_center</i>Newsletter</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Resources Admin-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greynav">
                <div class="collapsible-header"><i class="tiny material-icons">announcement</i><a
                        class="">Resources Admin</a>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a class="" href="{{url('indexCategory')}}"><i
                                    class="tiny material-icons">business_center</i>Funding Categories</a></li>
                        <li><a class="" href="{{url('resourceIndex')}}"><i
                                    class="tiny material-icons">business_center</i>Funding Resources</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Workshop Admin-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greynav">
                <div class="collapsible-header"><i class="tiny material-icons">announcement</i><a
                        class="">Workshop Admin</a>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a class="" href="{{url('index-presentation')}}"><i
                                    class="tiny material-icons">business_center</i>Workshops</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Application Process-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greennav">
                <div class="collapsible-header"><i class="tiny material-icons">business_center</i>
                    Application Process
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  class="" href="{{url('applicants')}}">
                                <i class="tiny material-icons">account_circle</i>Manage Pending App</a></li>
                        <li><a  class="" href="{{url('declined-applicants')}}"><i
                                    class="tiny material-icons">account_circle</i>Manage Declined App</a></li>
                        <li><a  class="" href="{{url('referred-applicants')}}"><i
                                    class="tiny material-icons">account_circle</i>Manage Referred App</a></li>
                        <li><a class="" href="{{url('panel-declined-applicant-index')}}">
                                <i class="tiny material-icons">account_circle</i>Panel Declined App</a></li>
                        <li><a  class="" href="{{url('panel-referred-applicant-index')}}">
                                <i class="tiny material-icons">account_circle</i>Panel Referred App</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Bootcampers-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greennav">
                <div class="collapsible-header"><i class="tiny material-icons">supervisor_account</i>
                    Bootcampers
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a class="" href="{{url('bootcampers')}}"><i
                                    class="tiny material-icons">account_circle</i>Manage Bootcampers</a></li>
                        <li><a class="" href="{{url('bootcamper-panel-interviews')}}"><i
                                    class="tiny material-icons">account_circle</i>Bootcamper Interviews</a></li>
                        <li><a class="" href="{{url('declined-bootcampers')}}"><i
                                    class="tiny material-icons">account_circle</i>Declined Bootcampers</a></li>
                        <li><a class="" href="{{url('referred-bootcamper-index')}}"><i
                                    class="tiny material-icons">account_circle</i>Referred Bootcampers</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Propella Platform-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greennav">
                <div class="collapsible-header"><i class="tiny material-icons">supervisor_account</i>
                    Propella Platform
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{url('venture')}}"><i
                                    class="tiny material-icons">account_circle</i>Active Ventures</a></li>
                        <li><a  href="{{url('incubatees')}}"><i
                                    class="tiny material-icons">account_circle</i>Manage Incubatees</a></li>

                        <li><a  href="{{url('propella-ventures')}}"><i
                                    class="tiny material-icons">account_circle</i>Propella ventures</a></li>

                    </ul>
                </div>
            </li>
        </ul>
        <!--Resources-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greennav">
                <div class="collapsible-header"><i class="tiny material-icons">
                        announcement</i><a>Resources</a>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="{{url('tradeShow')}}"><i
                                    class="tiny material-icons">business_center</i>Trade Show</a></li>
                        <li><a  href="{{url('grant')}}"><i
                                    class="tiny material-icons">business_center</i>Grant</a></li>
                        <li><a  href="{{url('fundings')}}"><i
                                    class="tiny material-icons">business_center</i>Funding</a></li>
                        <li><a  href="{{url('resource-events')}}"><i
                                    class="tiny material-icons">business_center</i>Events</a></li>
                        <li><a  href="{{url('competition')}}"><i
                                    class="tiny material-icons">business_center</i>Competition</a></li>
                        <li><a  href="{{url('crowd')}}"><i
                                    class="tiny material-icons">business_center</i>Crowd Funding</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--M + E-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greennav">
                <div class="collapsible-header"><i class="tiny material-icons">supervisor_account</i>
                    M + E
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="{{url('ventures_oct_2021')}}"><i
                                    class="tiny material-icons">account_circle</i>Oct 2021</a></li>
                        <li><a  href="{{url('ventures_nov_2021')}}"><i
                                    class="tiny material-icons">account_circle</i>Nov 2021</a></li>
                        <li><a  href="{{url('ventures_dec_2021')}}"><i
                                    class="tiny material-icons">account_circle</i>Dec 2021</a></li>
                        <li><a  href="{{url('ventures_jan_2022')}}"><i
                                    class="tiny material-icons">account_circle</i>Jan 2022</a></li>
                        <li><a  href="{{url('venture-feb-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>Feb 2022</a></li>
                        <li><a  href="{{url('venture-march-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>March 2022</a></li>
                        <li><a  href="{{url('venture-april-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>April 2022</a></li>
                        <li><a  href="{{url('venture-may-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>May 2022</a></li>
                        <li><a  href="{{url('venture-june-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>June 2022</a></li>
                        <li><a  href="{{url('venture-july-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>July 2022</a></li>
                        <li><a  href="{{url('venture-aug-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>Aug 2022</a></li>
                        <li><a  href="{{url('venture-sept-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>Sept 2022</a></li>
                        <li><a  href="{{url('venture-oct-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>Oct 2022</a></li>
                        <li><a  href="{{url('venture-nov-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>Nov 2022</a></li>
                        <li><a  href="{{url('venture-dec-2022')}}"><i
                                    class="tiny material-icons">account_circle</i>Dec 2022</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Shadowboard-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greennav">
                <div class="collapsible-header"><i class="tiny material-icons">person</i><a
                    >Shadowboard</a>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="{{url('mentors')}}"><i
                                    class="tiny material-icons">account_circle</i>Manage Mentors</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Online Content-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greennav">
                <div class="collapsible-header">
                    <i
                        class="tiny material-icons">business_center</i>
                    <a  class="" href="{{url('index-presentation')}}">Online Content</a>
                </div>
                <div class="collapsible-body">
                </div>
            </li>
        </ul>
        <!--Exited Ventures-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greennav">
                <div class="collapsible-header"><i class="tiny material-icons">supervisor_account</i>
                    Exited Ventures
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="{{url('exited-venture')}}"><i
                                    class="tiny material-icons">account_circle</i>Manage Exited</a></li>
                        <li><a  href="{{url('exited-incubatees')}}"><i
                                    class="tiny material-icons">account_circle</i>Exited Incubatees</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Propella Alumni-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="greennav">
                <div class="collapsible-header"><i class="tiny material-icons">supervisor_account</i>
                    Propella Alumni
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="{{url('alumni-venture')}}"><i
                                    class="tiny material-icons">account_circle</i>Manage Alumni</a></li>
                        <li><a  href="{{url('alumni-incubatees')}}"><i
                                    class="tiny material-icons">account_circle</i>Alumni Incubatees</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Venues-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="lightBluenav">
                <div class="collapsible-header"><i class="tiny material-icons">border_color</i>
                    Venues
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="/get-venues"><i
                                    class="tiny material-icons">business_center</i>View Venues</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Calender-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="lightBluenav">
                <div class="collapsible-header"><i class="tiny material-icons">event</i><a> Calendar</a>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="{{url('/full_calendar/events')}}"><i
                                    class="tiny material-icons">business_center</i>View Calendar</a></li>
                        <li><a  href="/events">
                                <i class="tiny material-icons">control_point</i> All Events </a></li>
                        <li><a href="/private-events">
                                <i class="tiny material-icons">control_point</i> Private Events </a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Companies Database-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="darknav">
                <div class="collapsible-header"><i class="tiny material-icons">event_note</i>
                    Companies Database
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="{{url('companies')}}"><i
                                    class="tiny material-icons">business_center</i>Companies</a></li>
                        <li><a href="{{url('company-employees')}}"><i
                                    class="tiny material-icons">people</i>Company employees</a></li>
                        <li><a  href="{{url('contact-logs')}}"><i
                                    class="tiny material-icons">perm_contact_calendar</i>Contact Logs</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Visitors-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="darknav">
                <div class="collapsible-header"><i class="tiny material-icons">face</i>
                    Visitor
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="/visitors/visitors-index"><i
                                    class="tiny material-icons">account_circle</i>Manage Visitors</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Covid-19 Form-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="darknav">
                <div class="collapsible-header"><i class="tiny material-icons">business_center</i>
                    COVID-19 Forms
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="{{url('visitor-covid-index')}}">
                                <i class="tiny material-icons">control_point</i>Visitors Covid Forms</a></li>
                        <li><a  href="{{url('staff-covid-index')}}">
                                <i class="tiny material-icons">control_point</i>Propella Staff Forms</a></li>
                        <li><a  href="{{url('company-emplyees-covid-index')}}">
                                <i class="tiny material-icons">control_point</i>Engeli Staff Forms</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--QR CODER-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="darknav">
                <div class="collapsible-header"><i class="tiny material-icons">business_center</i>
                    QR CODE GENERATOR
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="{{url('qr-code-generator')}}">
                                <i class="tiny material-icons">control_point</i>QR CODE generator</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <div class="divider"></div>
        <!--Profile-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="darknav">
                <div class="collapsible-header"><i class="tiny material-icons">account_circle</i><a
                        href="#"> Profile</a>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a  href="/user-profile"><i
                                    class="tiny material-icons">account_circle</i>Edit Profile</a></li>
                    </ul>
                    <ul>
                        <li><a href="{{url('change')}}"><i class="tiny material-icons">account_circle</i>Change Password</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <!--Sign Out-->
        <ul class="collapsible popout" style="margin-top:1em;">
            <li class="darknav">
                <div class="collapsible-header"  onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    <a href="{{ url('/logout') }}"
                    ><i
                            class="tiny material-icons">vpn_key</i>
                        Sign Out
                    </a>
                </div>
                <div class="collapsible-body">
                </div>
            </li>
        </ul>
        <ul class="collapsible popout" style="margin-top:1em;">
            <li>
                <div class="collapsible-header" style="color:black;font-weight: bolder"><i class="tiny material-icons">business_center</i>
                    Hedge Enquiries
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('/enquiry-category-index')}}">
                                <i class="tiny material-icons">control_point</i> Manage Categories </a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('/unassigned-enquiry-index')}}">
                                <i class="tiny material-icons">control_point</i> Pending Enquiries </a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('/pre-assigned-enquiry-index')}}">
                                <i class="tiny material-icons">control_point</i> Pre Assigned </a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('/assigned-enquiry-index')}}">
                                <i class="tiny material-icons">control_point</i> Assigned Enquiries </a></li>
                    </ul>
                </div>
            </li>
        </ul>

        <ul class="collapsible popout" style="margin-top:1em;">
            <li>
                <div class="collapsible-header" style="color:black;font-weight: bolder"><i class="tiny material-icons">business_center</i>
                    RINP
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('/rinp-applicants-index')}}">
                                <i class="tiny material-icons">control_point</i> RINP Pending App </a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('')}}">
                                <i class="tiny material-icons">control_point</i> RINP Declined App </a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="{{url('')}}">
                                <i class="tiny material-icons">control_point</i> RINP Referred App </a></li>
                    </ul>
                </div>
            </li>
        </ul>

    </ul>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>
<div class="container-fluid">
    @yield('content')
</div>


<style>
    .sidenav-overlay {
        z-index: 996;
    }

    @media only screen and (min-width: 993px) {
        nav a.sidenav-trigger {
            display: inline;
        }
    }

    nav a {
        color: black !important;
        font-weight: bolder !important;
    }

</style>
<!--  Scripts-->
{{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/b-1.5.6/datatables.min.js"></script>--}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/notify/notify.js"></script>
<script type="text/javascript" src="/js/notify/notify.min.js"></script>
{{--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">--}}
{{--<script type="text/javascript" charset="utf8"--}}
{{--src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>--}}

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>



{{--<script src="/js/jquery-step-maker.js"></script>--}}
<script>
    let options = {
        format: 'yyyy-mm-dd'
    }
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.datepicker');
        var instances = M.Datepicker.init(elems, options);
    });

    function venture_bulk_mailer_index(){
        window.location.href = '/send-email-to-venture';
    }

    function bulk_mailer() {
        window.location.href = '/bulk-mailer';
    }

    function visitor_bulk_mailer() {
        window.location.href = '/visitor-bulk-mailer';
    }
    function incubatee_bulk_mailer_index() {
        window.location.href = '/incubatee-bulk-mailer-index';
    }

    $(document).ready(function () {
        console.log("initializing");
        $('input.autocomplete').autocomplete({
            data: {
                "Apple": null,
                "Microsoft": null,
                "Google": 'https://placehold.it/250x250'
            },
        });
        $('.collapsible').collapsible();
        $('.sidenav').sidenav();
        $(".dropdown-trigger").dropdown();
        $('.carousel').carousel();
        $('.tabs').tabs();
        $('.modal').modal();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    function dashboard_show() {
        window.location.href = '/home';
    }

    function reports_show() {

    }

</script>
@stack('custom-scripts')
</body>
</html>
