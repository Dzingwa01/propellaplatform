@extends('layouts.incubatee-layout')
@section('content')
    <br>
    <br>
    <br/>
    <br/>
    {{ Breadcrumbs::render('inc-home')}}
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/css/Incubatee/incubateeHome.css"/>
    <input hidden disabled id="user-id-input" value="{{$user->id}}">
    <h4 class="center-align">Hello <b>{{$user->name}}</b></h4>
    <form method="post" id="edit-popi" action="{{ route('update-incubatee-popi-act', $incubatee->id) }}"
          class="form-horizontal">
        @method('post')
        @csrf
        @if($incubatee->popi_is_agreed == true)
            <p></p>
        @else
            <p class="center-align">
                <label>
                    <input required class="checkbox" onChange='submit();' type="checkbox"/>
                    <span>I agree to all <a class="modal-trigger" href="#popi">click to view</a></span>
                </label>
            </p>
        @endif
    </form>
    <form method="post" id="edit-nda" action="{{ route('update-incubatee-nda-agreement', $incubatee->id) }}"
          class="form-horizontal">
        @method('post')
        @csrf
        @if($incubatee->nda_agreed == true)
            <p></p>
        @else
        <p class="center-align">
            <label>
                <input required class="checkbox" onChange='submit();' type="checkbox"/>
                <span>NDA Agreement <a class="modal-trigger" href="#nda"> click to view</a></span>
            </label>
        </p>
            @endif
    </form>
    {{--Agreement modal--}}
    <div id="nda" class="modal">
        <div class="modal-content">
            <img style="width: 30vh" src="/images/Prop agrre.png">
            <div class="col s12">
                <h5 class="center-align"><b>Open Communications FutureMakers Multi-Party Nondisclosure Agreement</b></h5>
            </div>
            <p>This multi-party nondisclosure agreement ("Agreement") is entered into and made effective as of the date set forth above among the parties listed at the end of this Agreement and any third party that executes this Agreement after the Effective Date. This Agreement is intended to cover the parties’ confidentiality obligations with respect to disclosures regarding or pertinent to the exploration ways the parties can work together for mutual benefit.</p>
            <p><b>The Parties agree as follows:</b></p>
            <p>1.	Confidential Information. The confidential, proprietary and trade secret information of each party ("Confidential Information") to be disclosed hereunder is that tangible or intangible information which is marked with a "Confidential," "Proprietary", or similar legend and which is clearly identified in writing as provided under this Agreement and includes any information disclosed during discussion sessions. To be considered Confidential Information, non-tangible disclosures must be identified as confidential under the terms and conditions of this Agreement prior to disclosure and reduced to writing, marked as provided above, and delivered to the receiving party within thirty (30) calendar days of the original date of disclosure. Any information received from any other participant in Bootcamp 1 will not be used by any other party.
                Ideas or discussions not forming part of the Disclosing Parties normal course of business and ideas generated during discussions will not be regarded as confidential information.
            </p>
            <p>2.	Obligations of receiving party. Each receiving party will maintain the confidentiality of the Confidential Information for each disclosing party with at least the same degree of care that it uses to protect its own confidential and proprietary information, but no less than a reasonable degree of care under the circumstances. No receiving party will disclose any of a disclosing party's confidential information to any employees or to any third parties except to employees, contractors, and Affiliates (as below defined) of such receiving party who have a need to know and who agree to abide by nondisclosure terms at least as
                comprehensive as those set forth herein; provided such receiving party will be liable for breach of any such entity. The receiving party will not make any copies of the Confidential Information received from a disclosing party except as necessary for their employees and contractors and those employees and contractors of the receiving party's Affiliates with a need to know. Any copies which are made will include the legend and identification statement as supplied by the disclosing party.  The term “Affiliate” of a party, as used herein, shall mean any company, corporation or other business entity which directly or indirectly (i) controls said party or (ii) is controlled by said party or (iii) is controlled by any entity under (i) above, where “to control” an entity shall mean to own more than fifty per cent (50%) of the shares of, or of the ownership interest in, such entity.
            </p>
            <p>3.    Period of confidentiality. The receiving party’s obligation of confidentiality shall be for a period of two (2) years from the date of receipt of the disclosing party’s Confidential Information</p>
            <p>4.    Termination of obligation of confidentiality. No receiving party will be liable for the disclosure of a disclosing party's confidential information which is:</p>
            <p>a)	Published with the consent of the disclosing party or is otherwise rightfully in the public domain other than by a breach of a duty of confidentiality to the disclosing party;</p>
            <p>b)   Rightfully received from a third party without any obligation of confidentiality;</p>
            <p>c)	Rightfully known to the receiving party without any limitation on use or disclosure prior to its receipt from the disclosing party</p>
            <p>d)   Independently developed by employees of the receiving party; or</p>
            <p>e)    Generally made available to third parties by the disclosing party without restriction on disclosure.</p>
            <p>5.    Title. Title or the right to possess Confidential Information as between receiving parties and the disclosing party will remain in the disclosing party.</p>
            <p>6.    No obligation of disclosure; Termination. No party has an obligation to disclose Confidential Information to any other
                party.  Any party may withdraw from this Agreement at any time and without cause upon written notice to the remaining
                parties, provided that such withdrawing party's obligations with respect to Confidential Information of other parties
                disclosed during the term of this Agreement will survive any such termination until the end of the period set forth in
                Section 3. A disclosing party may at any time: (a) cease giving Confidential Information to the other party without any
                liability, and/or (b) request in writing the return or destruction of all or part of its Confidential Information previously
                disclosed, and all copies thereof, and each other party will comply with such request, and certify in writing its compliance
                within sixty (60) calendar days.
            </p>
            <p>7.	No warranty.  All parties acknowledge that all information provided hereunder is provided "AS IS" WITH NO WARRANTIES WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, AND THE PARTIES EXPRESSLY DISCLAIM ANY WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, FITNESS FOR ANY PARTICULAR PURPOSE, OR ANY WARRANTY OTHERWISE ARISING OUT OF ANY PROPOSAL, SPECIFICATION, OR SAMPLE.</p>
            <p>8.    General</p>
            <p>a)	This Agreement is neither intended to, nor will it be construed as creating a joint venture, a partnership or other form of a business association between the parties, nor an obligation to buy or sell products using or incorporating the Confidential Information.</p>
            <p>b)   Each party understands and acknowledges that no license under any patents, copyrights, trademarks, or maskworks is granted to or conferred upon any party or by the disclosure of any Confidential Information by one party to the other party as contemplated hereunder, either expressly, by implication, inducement, estoppel or otherwise, and that any license under such intellectual property rights must be express and in writing.</p>
            <p>c)	The failure of any party to enforce any rights resulting from breach of any provision of this Agreement will not be deemed a waiver of any right relating to a subsequent breach of such provision or of any other right hereunder.</p>
            <p>d)   This Agreement constitutes the entire agreement between the parties with respect to the disclosures of Confidential
                Information, and may not be amended except in a writing signed by a duly authorized representative of the respective parties. e)    Each party shall execute a copy of this Agreement, each of which shall be deemed an original and all of which together shall
                constitute one instrument. A list shall be maintained of all parties that have executed this Agreement and shall be made available to all parties.
            </p>
            <p>e)    Each party shall execute a copy of this Agreement, each of which shall be deemed an original and all of which together shall
                constitute one instrument. A list shall be maintained of all parties that have executed this Agreement and shall be made available to all parties.
            </p>
            <p id="thank-you">f)    This Agreement shall be subject to and governed by the law of South Africa and the place of jurisdiction shall be Port Elizabeth, South Africa.

            </p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    <div class="row" id="desktopLog">
        @if($incubatee->venture_id)
            <div class="col s12 m4">
                <div class="card" style="border-radius: 10px;background-color:green; height: 185px">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i>Shadowboard</span>
                        <p style="font-weight: bolder">View your Shadowboards.</p>
                    </div>
                    <div class="card-action center white-text" style="border-radius: 10px;">
                        <a style="color: white" href="{{url('/venture-shadow/'.$incubatee->venture->id)}}"
                           onclick="viewShadowboard(this)"
                        ><b>Manage Your Shadowboards</b></a>
                    </div>
                </div>
            </div>
        @endif
            @if(is_null($incubatee->venture_id))
                <div class="col s12 m4">
                    <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1); height: 185px">
                        <div class="card-content white-text">
                            <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">border_color</i>-->  Add Venture</span>
                            <p style="font-weight: bolder">Create Your Venture.</p>
                        </div>
                        <div class="card-action center white-text" style="border-radius: 10px;">
                            <a class="btn"  style="color: white; background-color:rgba(0, 47, 95, 1)"  href="{{url('/incubatee-create-venture', $incubatee->id)}}"
                            ><b>Create Your Venture</b></a>
                        </div>
                    </div>
                </div>
            @elseif($incubatee->venture_id)
                <div class="col s12 m4">
                    <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1); height: 185px">
                        <div class="card-content white-text">
                            <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">border_color</i>-->Edit Venture Details</span>
                            <p style="font-weight: bolder">Edit Your Venture.</p>
                        </div>
                        <div class="card-action center white-text" style="border-radius: 10px;">
                            <a class="btn" style="color: white; background-color:rgba(0, 47, 95, 1)" href="{{url('/venture-edit/'.$incubatee->venture->slug)}}"
                               onclick="viewVenture(this)"
                            ><b>Manage Your Venture</b></a>
                        </div>
                    </div>
                </div>
            @endif


            <div class="col s12 m4">
                <div class="card" style="border-radius: 10px;background-color: green; height: 185px">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">border_color</i>-->  Edit Personal Details</span>
                        <p style="font-weight: bolder">Edit Your Personal Details Here!</p>
                    </div>
                    <div class="card-action center" style="border-radius: 10px;">
                        <a class="btn" style="color: white; background-color:green" href="{{url('/incubatee-edit', $incubatee->id)}}"
                           onclick="createVenture(this)"
                        ><b>Edit Your Details</b></a>
                    </div>
                </div>
            </div>


            <div class="col s12 m4">
                <div class="card" style="border-radius: 10px;background-color:green; height: 185px">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">border_color</i>-->  Events</span>
                        <p style="font-weight: bolder">Register For Events!</p>
                    </div>
                    <div class="card-action center" style="border-radius: 10px;">
                        <a class="btn" style="color: white; background-color:green" id="incubatee-events-button"
                           style="cursor: pointer;"><b>View Calendar</b></a>
                    </div>
                </div>
            </div>

            <input hidden disabled value="{{$incubatee->id}}" id="incubatee-id-input">
            <div class="col s12 m4">
                <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1); height: 185px; height: 185px">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">border_color</i>-->   Book Venues</span>
                        <h6><b>This is where you view your booked venues.</b></h6>
                    </div>
                    <div class="card-action center" style="border-radius: 15px;">
                        <a class="btn" id="user-venue-button" href="{{url('/show-booking/'.$user->id)}}"
                            style="cursor: pointer;color: white; background-color:rgba(0, 47, 95, 1)"><b>View
                                Venues</b></a>
                    </div>
                </div>
            </div>

        @foreach($user->roles as $user_role)
            @if($user_role->name == 'panelist')
                    <div class="col s12 m4">
                        <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1); height: 185px">
                            <div class="card-content white-text">
            <span class="card-title" style="font-weight: bolder"><i
                    class="small material-icons">border_color</i> Panel Interview </span>
                                <p style="align-content: center; font-weight: bolder">This is where you will see the applicants
                                    assigned to your panel interview. </p>
                            </div>
                        </div>
                        <div class="card-action center" style="border-radius: 15px;">
                            <a class="btn" href="/panelist-show-industrial-applicants"
                               style="cursor: pointer; color: white;background-color: #1a237e; "><b>View
                                    Applicants</b></a>
                            <a class="btn" href="/panelist-show-ict-applicants"
                               style="cursor: pointer; color: white;background-color: #1a237e; "><b>View
                                    Bootcampers</b></a>
                            <a class="btn" href="/panelist-show-ventures"
                               style="cursor: pointer; color: white;background-color: #1a237e;"><b>View
                                    Ventures</b></a>
                        </div>
                    </div>
            @endif
        @endforeach
        @if($incubatee->venture_id)
                <div class="col s12 m4">
                    <div class="card" style="border-radius: 10px;background-color:green; height: 185px">
                        <div class="card-content white-text">
   <span class="card-title" style="font-weight: bolder"><i
           class="small material-icons">person_add</i> Add Employee</span>
                            <br/>
                            <p style="align-content: center; ">This is where you view and add your employee.</p>
                        </div>
                        <div class="card-action center" style="border-radius: 15px;">
                            <a style="color: white" id="user-venue-button"
                               href="{{url('/employee-index/'.$incubatee->venture->id)}}"
                               style="cursor: pointer; "><b>Mange
                                    Your Employees</b></a>
                        </div>
                    </div>
                </div>
        @endif

            @if($incubatee->venture->stage == 'Stage 2')
            <!--STAGE 2 WORKSHOP CARD-->
            <div class="col s12 m4">
            <div class="card" style="border-radius: 10px;background-color:green; height: 185px">
                <div class="card-content white-text">
            <span class="card-title" style="font-weight: bolder"><!--<i
           class="small material-icons">person_add</i>-->Stage 2 Workshop</span>
                    <p style="font-weight: bolder">Stage 2 Workshop evaluation form.</p>
                </div>
                <div class="card-action center" style="border-radius: 15px; padding: 0px">
                    <div class="row">
                        <div class="col s6">
                            @foreach($pre_stage2_workshop as $pre_stage2_workshops)
                                @if($incubatee->pre_stage_two_complete == true)
                                    @else
                                <a class="tooltipped modal-trigger" style="color: white" href="#pre_stage2_workshop"
                                   style="cursor: pointer;"><b>{{$pre_stage2_workshops->category_name}}</b></a>
                                @endif
                            @endforeach
                        </div>
                        <div class="col s6">
                            @foreach($post_stage2_workshop as $post_stage2_workshops)
                                @if($incubatee->pre_stage_two_complete == false)
                                    @else
                                <a class="tooltipped modal-trigger float-left" style="color: white" href="#post_stage2_workshop"
                                   style="cursor: pointer;"><b>{{$post_stage2_workshops->category_name}}</b></a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @elseif($incubatee->venture->stage == 'Stage 3')
            <!--STAGE 3 WORKSHOP CARD-->
            <div class="col s12 m4">
                <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1); height: 185px">
                    <div class="card-content white-text">
            <span class="card-title" style="font-weight: bolder"><!--<i
           class="small material-icons">person_add</i>-->Stage 3 Workshop</span>
                        <p style="font-weight: bolder">Stage 3 Workshop evaluation form.</p>
                    </div>
                    <div class="card-action center" style="border-radius: 15px; padding: 0px">
                        <div class="row">
                            <div class="col s6">
                                @foreach($pre_stage3_workshop as $pre_stage3_workshops)
                                    @if($incubatee->pre_stage_three_complete == true)
                                        @else
                                    <a class="tooltipped modal-trigger" style="color: white;background-color: rgba(0, 47, 95, 1)" href="#pre_stage3_workshop"
                                       style="cursor: pointer;"><b>{{$pre_stage3_workshops->category_name}}</b></a>
                                    @endif
                                @endforeach
                            </div>
                            <div class="col s6">
                                @foreach($post_stage3_workshop as $post_stage3_workshops)
                                    @if($incubatee->pre_stage_three_complete == false)
                                    @else
                                    <a class="tooltipped modal-trigger float-left" style="color: white;background-color: rgba(0, 47, 95, 1)" href="#post_stage3_workshop"
                                       style="cursor: pointer;"><b>{{$post_stage3_workshops->category_name}}</b></a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @elseif($incubatee->venture->stage == 'Stage 4')
            <!--STAGE 4 WORKSHOP CARD-->
            <div class="col s12 m4">
                <div class="card" style="border-radius: 10px;background-color:green; height: 185px">
                    <div class="card-content white-text">
            <span class="card-title" style="font-weight: bolder"><!--<i
           class="small material-icons">person_add</i>-->Stage 4 Workshop</span>
                        <p style="font-weight: bolder">Stage 4 Workshop evaluation form.</p>
                    </div>
                    <div class="card-action center" style="border-radius: 15px; padding: 0px">
                        <div class="row">
                            <div class="col s6">
                                @foreach($pre_stage4_workshop as $pre_stage4_workshops)
                                    @if($incubatee->pre_stage_four_complete == true)
                                        @else
                                    <a class="tooltipped modal-trigger" style="color: white" href="#pre_stage4_workshop"
                                       style="cursor: pointer;"><b>{{$pre_stage4_workshops->category_name}}</b></a>
                                    @endif
                                @endforeach
                            </div>
                            <div class="col s6">
                                @foreach($post_stage4_workshop as $post_stage4_workshops)
                                    @if($incubatee->pre_stage_four_complete == false)
                                        @else
                                    <a class="tooltipped modal-trigger float-left" style="color: white" href="#post_stage4_workshop"
                                       style="cursor: pointer;"><b>{{$post_stage4_workshops->category_name}}</b></a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @elseif($incubatee->venture->stage == 'Stage 5')
            <!--STAGE 5 WORKSHOP CARD-->
            <div class="col s12 m4">
                <div class="card" style="border-radius: 10px;background-color:green; height: 185px">
                    <div class="card-content white-text">
            <span class="card-title" style="font-weight: bolder"><!--<i
           class="small material-icons">person_add</i>-->Stage  Workshop</span>
                        <p style="font-weight: bolder">Stage 5 Workshop evaluation form.</p>
                    </div>
                    <div class="card-action center" style="border-radius: 15px; padding: 0px">
                        <div class="row">
                            <div class="col s6">
                                @foreach($pre_stage5_workshop as $pre_stage5_workshops)
                                    @if($incubatee->pre_stage_five_complete == true)
                                        @else
                                    <a class="tooltipped modal-trigger" style="color: white" href="#pre_stage5_workshop"
                                       style="cursor: pointer;"><b>{{$pre_stage5_workshops->category_name}}</b></a>
                                    @endif
                                @endforeach
                            </div>
                            <div class="col s6">
                                @foreach($post_stage5_workshop as $post_stage5_workshops)
                                    @if($incubatee->pre_stage_five_complete == false)
                                        @else
                                    <a class="tooltipped modal-trigger float-left" style="color: white" href="#post_stage5_workshop"
                                       style="cursor: pointer;"><b>{{$post_stage5_workshops->category_name}}</b></a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <!--PRE STAGE 2 EVALUATION FORM-->
            <div id="pre_stage2_workshop" class="modal">
                @foreach($pre_stage2_workshop as $pre_stage2_workshops)
                    <h4 class="center-align"><b>{{$pre_stage2_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->pre_stage_two_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($pre_stage2_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="pre-stage2-upload-answers-form-desktop" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>
            <!--POST STAGE 2 EVALUATION FORM-->
            <div id="post_stage2_workshop" class="modal">
                @foreach($post_stage2_workshop as $post_stage2_workshops)
                    <h4 class="center-align"><b>{{$post_stage2_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->post_stage_two_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10),
                                    with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($post_stage2_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text"
                                                           class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1"
                                                           max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="post-stage2-upload-answers-form-desktop"
                                name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>


            <!--PRE STAGE 3 EVALUATION FORM-->
            <div id="pre_stage3_workshop" class="modal">
                @foreach($pre_stage3_workshop as $pre_stage3_workshops)
                    <h4 class="center-align"><b>{{$pre_stage3_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->pre_stage_three_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($pre_stage3_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="pre-stage3-upload-answers-form-desktop" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>
            <!--POST STAGE 3 EVALUATION FORM-->
            <div id="post_stage3_workshop" class="modal">
                @foreach($post_stage3_workshop as $post_stage3_workshops)
                    <h4 class="center-align"><b>{{$post_stage3_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->post_stage_three_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10),
                                    with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($post_stage3_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text"
                                                           class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1"
                                                           max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="post-stage3-upload-answers-form-desktop"
                                name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>

            <!--PRE STAGE 4 EVALUATION FORM-->
            <div id="pre_stage4_workshop" class="modal">
                @foreach($pre_stage4_workshop as $pre_stage4_workshops)
                    <h4 class="center-align"><b>{{$pre_stage4_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->pre_stage_four_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($pre_stage4_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="pre-stage4-upload-answers-form-desktop" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>
            <!--POST STAGE 4 EVALUATION FORM-->
            <div id="post_stage4_workshop" class="modal">
                @foreach($post_stage4_workshop as $post_stage4_workshops)
                    <h4 class="center-align"><b>{{$post_stage4_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->post_stage_four_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10),
                                    with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($post_stage4_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text"
                                                           class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1"
                                                           max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="post-stage4-upload-answers-form-desktop"
                                name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>

            <!--PRE STAGE 5 EVALUATION FORM-->
            <div id="pre_stage5_workshop" class="modal">
                @foreach($pre_stage5_workshop as $pre_stage5_workshops)
                    <h4 class="center-align"><b>{{$pre_stage5_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->pre_stage_five_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($pre_stage5_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="pre-stage5-upload-answers-form-desktop" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>
            <!--POST STAGE 5 EVALUATION FORM-->
            <div id="post_stage5_workshop" class="modal">
                @foreach($post_stage5_workshop as $post_stage5_workshops)
                    <h4 class="center-align"><b>{{$post_stage5_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->post_stage_five_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10),
                                    with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($post_stage5_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text"
                                                           class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1"
                                                           max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="post-stage5-upload-answers-form-desktop"
                                name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>


        @if($user_enquiries != null or $user_pre_assigned_enquiries != null)
            @if(count($user_enquiries) > 0 or count($user_pre_assigned_enquiries) > 0)
                    <div class="col s12 m3">
                        <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1)">
                            <div class="card-content white-text">
      <span class="card-title" style="font-weight: bolder"><i
              class="small material-icons">border_color</i> Manage General Enquiries </span>
                                <p style="align-content: center; ">This is where you will see the enquiries that
                                    you have been assigned to. </p>
                            </div>
                            <div class="card-action center" style="border-radius: 15px;">
                                <a href="/pre-assigned-enquiry-index"
                                   style="cursor: pointer; color: white;"><b>View Pre-Assigned
                                        Enquirires</b></a>
                                <a href="/assigned-enquiry-index"
                                   style="cursor: pointer;"><b>View Assigned Enquirires</b></a>
                            </div>
                        </div>
                    </div>
            @endif
        @endif
    </div>
    <!-- Modal Structure for workshop evaluation form -->
    <div id="workshop_evaluation_form_modal" class="modal">
        @foreach($category as $categories)
            <h4 class="center-align"><b>{{$categories->category_name}}</b></h4>
        @endforeach
        @if($incubatee->workshop_evaluation_complete == true)
            <div class="row" style="margin-left: 4em;margin-right: 4em">
                <p class="center-align">Form already submmitted</p>
            </div>
        @else
            <ol>
            <div class="modal-content">
                <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                @foreach($category as $cur_category)
                    @foreach($cur_category->questions as $cur_question)
                        <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                        @if($cur_question->question_number == 5 or $cur_question->question_number == 6 or $cur_question->question_number == 7)
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text" class=" answer-text-desktop" style="width:50%">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @else
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text"
                                                   class="answer-answer-text" style="width:50%">
                                        </div>
                                    @else
                                    <div class="input-field">
                                        <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-desktop" style="width:30%">
                                    </div>
                                    @endif
                                    @if(count($cur_question->questionSubTexts) > 0)
                                        @foreach($cur_question->questionSubTexts as $question_sub_text)
                                            <p>{{ $question_sub_text->question_sub_text}}</p>
                                                @if($cur_question->question_type == "text")
                                                    <div class="input-field">
                                                        <input id="{{$cur_question->id}}" required type="text"
                                                               class="answer-answer-text" style="width:50%">
                                                    </div>
                                                @else
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="text" class="answer-text-desktop" style="width:50%">
                                            </div>
                                                @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endforeach
            </div>
            </ol>
            <div class="col s1 answers-submit" hidden></div>
            <div class="row center">
                <button class="btn waves-effect waves-light" id="upload-answers-form-desktop" name="action" style="margin-right: auto;">Save
                    <i class="material-icons right">send</i>
                </button>
            </div>
        @endif
    </div>

    <!--Stage 3 Modal Structure for workshop evaluation form -->
    <div id="stage3_workshop_evaluation_form_modal" class="modal">
        @foreach($stage3_workshop as $categories)
            <h4 class="center-align"><b>{{$categories->category_name}}</b></h4>
        @endforeach
        @if($incubatee->stage3_workshop_complete == true)
            <div class="row" style="margin-left: 4em;margin-right: 4em">
                <p class="center-align">Form already submmitted</p>
            </div>
        @else
            <ol>
            <div class="modal-content">
                <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                @foreach($stage3_workshop as $cur_category)
                    @foreach($cur_category->questions as $cur_question)
                        <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                        @if($cur_question->question_number == 5 or $cur_question->question_number == 6 or $cur_question->question_number == 7)
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text" class=" stage3-answer-text" style="width:50%">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @else
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text" class=" stage3-answer-text" style="width:50%">
                                        </div>
                                    @else
                                    <div class="input-field">
                                        <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" stage3-answer-text" style="width:30%">
                                    </div>
                                        @endif
                                    @if(count($cur_question->questionSubTexts) > 0)
                                        @foreach($cur_question->questionSubTexts as $question_sub_text)
                                            <p>{{ $question_sub_text->question_sub_text}}</p>
                                                @if($cur_question->question_type == "text")
                                                    <div class="input-field">
                                                        <input id="{{$cur_question->id}}" required type="text" class=" stage3-answer-text" style="width:50%">
                                                    </div>
                                                    @else
                                                    <div class="input-field">
                                                        <input id="{{$cur_question->id}}" required type="text" class="stage3-answer-text" style="width:50%">
                                                    </div>
                                                @endif

                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endforeach
            </div>
            </ol>
            <div class="col s1 answers-submit" hidden></div>
            <div class="row center">
                <button class="btn waves-effect waves-light" id="stage3-upload-answers-form" name="action" style="margin-right: auto;">Save
                    <i class="material-icons right">send</i>
                </button>
            </div>
        @endif
    </div>

    <!--End of Stage 3 Modal Structure for workshop evaluation form -->
    <div id="end_of_stage3_workshop_evaluation_form_modal" class="modal">
        @foreach($end_of_stage3_workshop as $categories)
            <h4 class="center-align"><b>{{$categories->category_name}}</b></h4>
        @endforeach
        @if($incubatee-> end_of_stage3_complete== true)
            <div class="row" style="margin-left: 4em;margin-right: 4em">
                <p class="center-align">Form already submmitted</p>
            </div>
        @else
            <ol>
            <div class="modal-content">
                <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                @foreach($end_of_stage3_workshop as $cur_category)
                    @foreach($cur_category->questions as $cur_question)
                        <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                        @if($cur_question->question_number == 5 or $cur_question->question_number == 6 or $cur_question->question_number == 7)
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text" class="end-of-stage3-answer-text" style="width:50%">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @else
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text" class="end-of-stage3-answer-text" style="width:50%">
                                        </div>
                                        @else
                                        <div class="input-field">
                                        <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" end-of-stage3-answer-text" style="width:30%">
                                    </div>
                                    @endif

                                    @if(count($cur_question->questionSubTexts) > 0)
                                        @foreach($cur_question->questionSubTexts as $question_sub_text)
                                            <p>{{ $question_sub_text->question_sub_text}}</p>
                                                @if($cur_question->question_type == "text")
                                                    <div class="input-field">
                                                        <input id="{{$cur_question->id}}" required type="text" class="end-of-stage3-answer-text" style="width:50%">
                                                    </div>
                                                @else
                                                <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="text" class="end-of-stage3-answer-text" style="width:50%">
                                                    @endif
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endforeach
            </div>
            </ol>
            <div class="col s1 answers-submit" hidden></div>
            <div class="row center">
                <button class="btn waves-effect waves-light" id="end-of-stage3-upload-answers-form" name="action" style="margin-right: auto;">Save
                    <i class="material-icons right">send</i>
                </button>
            </div>
        @endif
    </div>


    <!--Stage 4 Modal Structure for workshop evaluation form -->
    <div id="stage4_workshop_evaluation_form_modal" class="modal">
        @foreach($stage4_workshop as $categories)
            <h4 class="center-align"><b>{{$categories->category_name}}</b></h4>
        @endforeach
        @if($incubatee->stage4_workshop_complete == true)
            <div class="row" style="margin-left: 4em;margin-right: 4em">
                <p class="center-align">Form already submmitted</p>
            </div>
        @else
            <ol>
            <div class="modal-content">
                <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                @foreach($stage4_workshop as $cur_category)
                    @foreach($cur_category->questions as $cur_question)
                        <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                        @if($cur_question->question_number == 5 or $cur_question->question_number == 6 or $cur_question->question_number == 7)
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text" class="stage4-answer-text" style="width:50%">                                        </div>
                                    @endif
                                </div>
                            </div>
                        @else
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text" class="stage4-answer-text" style="width:50%">
                                        </div>
                                        @else
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class="stage4-answer-text" style="width:30%">
                                        </div>
                                    @endif

                                    @if(count($cur_question->questionSubTexts) > 0)
                                        @foreach($cur_question->questionSubTexts as $question_sub_text)
                                            <p>{{ $question_sub_text->question_sub_text}}</p>
                                                @if($cur_question->question_type == "text")
                                                    <div class="input-field">
                                                        <input id="{{$cur_question->id}}" required type="text" class="stage4-answer-text" style="width:50%">
                                                    </div>
                                                @else
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="text" class="stage4-answer-text" style="width:50%">
                                            </div>
                                                @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endforeach
            </div>
            </ol>
            <div class="col s1 answers-submit" hidden></div>
            <div class="row center">
                <button class="btn waves-effect waves-light" id="stage4-upload-answers-form" name="action" style="margin-right: auto;">Save
                    <i class="material-icons right">send</i>
                </button>
            </div>
        @endif
    </div>

    <!--Stage 5 Modal Structure for workshop evaluation form -->
    <div id="stage5_workshop_evaluation_form_modal" class="modal">
        @foreach($stage5_workshop as $categories)
            <h4 class="center-align"><b>{{$categories->category_name}}</b></h4>
        @endforeach
        @if($incubatee->stage5_workshop_complete == true)
            <div class="row" style="margin-left: 4em;margin-right: 4em">
                <p class="center-align">Form already submmitted</p>
            </div>
        @else
            <ol>
                <div class="modal-content">
                    <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                    @foreach($stage5_workshop as $cur_category)
                        @foreach($cur_category->questions as $cur_question)
                            <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                            @if($cur_question->question_number == 5 or $cur_question->question_number == 6 or $cur_question->question_number == 7)
                                <div class="row" style="border-style: ridge">
                                    <div class="" style="margin-left: 4em">
                                        @if($cur_question->question_type == "text")
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="text"
                                                       class="stage5-answer-text" style="width:50%">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @else
                                <div class="row" style="border-style: ridge">
                                    <div class="" style="margin-left: 4em">
                                        @if($cur_question->question_type == "text")
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="text"
                                                       class="stage5-answer-text" style="width:50%">
                                            </div>
                                        @else
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="number" min="1"
                                                       max="10" class="stage5-answer-text" style="width:30%">
                                            </div>
                                        @endif
                                        {{-- <p>{{ $cur_question->question_text}}</p>--}}

                                        @if(count($cur_question->questionSubTexts) > 0)
                                            @foreach($cur_question->questionSubTexts as $question_sub_text)
                                                <p>{{ $question_sub_text->question_sub_text}}</p>
                                                @if($cur_question->question_type == "text")
                                                    <div class="input-field">
                                                        <input id="{{$cur_question->id}}" required type="text"
                                                               class="stage5-answer-text" style="width:50%">
                                                    </div>
                                                @else
                                                    <div class="input-field">
                                                        <input id="{{$cur_question->id}}" required type="text" class="stage5-answer-text" style="width:50%">
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endforeach
                </div>
            </ol>
            <div class="col s1 answers-submit" hidden></div>
            <div class="row center">
                <button class="btn waves-effect waves-light" id="stage5-upload-answers-form" name="action" style="margin-right: auto;">Save
                    <i class="material-icons right">send</i>
                </button>
            </div>
        @endif
    </div>

    <!--Stage 6 Modal Structure for workshop evaluation form -->
    <div id="stage6_workshop_evaluation_form_modal" class="modal">
        @foreach($stage6_workshop as $categories)
            <h4 class="center-align"><b>{{$categories->category_name}}</b></h4>
        @endforeach
        @if($incubatee->stage6_workshop_complete == true)
            <div class="row" style="margin-left: 4em;margin-right: 4em">
                <p class="center-align">Form already submmitted</p>
            </div>
        @else
            <ol>
            <div class="modal-content">
                <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                @foreach($stage6_workshop as $cur_category)
                    @foreach($cur_category->questions as $cur_question)
                        <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                        @if($cur_question->question_number == 5 or $cur_question->question_number == 6 or $cur_question->question_number == 7)
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text" class="stage6-answer-text" style="width:50%">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @else
                            <div class="row" style="border-style: ridge">
                                <div class="" style="margin-left: 4em">
                                    @if($cur_question->question_type == "text")
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="text" class="stage6-answer-text" style="width:50%">
                                        </div>
                                        @else
                                        <div class="input-field">
                                            <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class="stage6-answer-text" style="width:30%">
                                        </div>
                                    @endif

                                    @if(count($cur_question->questionSubTexts) > 0)
                                        @foreach($cur_question->questionSubTexts as $question_sub_text)
                                            <p>{{ $question_sub_text->question_sub_text}}</p>
                                                @if($cur_question->question_type == "text")
                                                    <div class="input-field">
                                                        <input id="{{$cur_question->id}}" required type="text" class="stage6-answer-text" style="width:50%">
                                                    </div>
                                                @else
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="text" class="stage6-answer-text" style="width:50%">
                                            </div>
                                                @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endforeach
            </div>
            </ol>
            <div class="col s1 answers-submit" hidden></div>
            <div class="row center">
                <button class="btn waves-effect waves-light" id="stage6-upload-answers-form" name="action" style="margin-right: auto;">Save
                    <i class="material-icons right">send</i>
                </button>
            </div>
        @endif
    </div>



    <!-- Modal Structure -->
    <div id="popi" class="modal">
        <div class="modal-content">
            <p><b>Website Privacy Notice</b></p>
            <p>The purpose of this information privacy strategy is to enhance the information privacy capabilities within the
                Propella application services, including (without limitation and to align with the requirements of the
                in country’s privacy legislation, namely the Protection of Personal Information Act (POPIA 2013) our website
                and other interactive properties through which the services are delivered (collectively, the “Service”) are
                owned, operated and distributed by Propella (referred to in this Privacy Notice as “Propella” or “we” and
                through similar words such as “us,” “our,” etc.).. This Privacy Notice outlines the personal information that
                Propella may collect, how Propella uses and safeguards that information, and with whom we
                may share it.
            </p>
            <p>Propella encourages our customers, visitors, business associates, and other interested parties to read
                this Privacy Notice, which applies to all users. By using our Service (our website and other interactive
                properties through which the services are delivered)  or submitting personal information to
                Propella by any other means, you acknowledge that you understand and agree to be bound by
                this Privacy Notice, and agree that Propella may collect, process, transfer, use,
                and disclose your personal information as described in this Notice. Further, by accessing any part of
                the Service, you are agreeing to .<span><b>THE TERMS AND CONDITIONS OF OUR TERMS OF SERVICE (the “Terms of Service”).
                                         IF YOU DO NOT AGREE WITH ANY PART OF THIS PRIVACY NOTICE OR OUR TERMS OF SERVICE, PLEASE DO NOT USE ANY OF
                                                 THE SERVICES.</b></span>
            </p>
            <p><b>What personal information do we collect about you?</b></p>
            <p>Personal information (also commonly known as personally identifiable information (PII) or personal data)
                is information that can be used to identify you, or any other individual to whom the information may relate.
            </p>
            <p>The personal information that we collect directly from those registering for the Service, includes the
                following categories:
            </p>
            <ul>
                Name and contact information (e.g. address; phone number; email, Proof of Address
                Billing Information (e.g. business bank account, billing contact information)
                Company/employer information
                Geographic or location information
                Information contained in posts you may on the public forums and interactive features of the Service.
                Other information that may be exchanged in the course of engaging with the Service.  You will be aware
                of any subsequently collected information because it will come directly from you.
            </ul>
            <p><b>Collection of User Generated Content</b></p>
            <p>We may invite you to post content on the Service, including your comments and any other information that you
                would like to be available on the Service, which may become public (“User Generated Content”).
                If you post User Generated Content, all of the information that you post will be available to
                authorized personnel of Propella. You expressly acknowledge and agree that we may access
                in real-time, record and store archives of any User Generated Content on our servers to make use of
                them in connection with the Service. If you submit a review, recommendation, endorsement,
                or other User Generated Content through the Service, or through other websites including Facebook,
                Instagram, Google, Yelp, and other similar channels, we may share that review, recommendation,
                endorsement or content publicly on the Service.
            </p>
            <p><b>What are the sources of personal information collected by Propella</b></p>
            <p>When providing personal information to Propella as described in this Notice, that personal information
                is collected directly from you, and you will know the precise personal information being collected by us.
                Propella does not collect personal information from any other sources, except where it may automatically
                be collected as described in the section titled “Cookies, Device Data, and How it is Used, if the information
                in that section is considered personal information.</p>
            <p><b>Why does Propella collect your personal information?</b></p>
            <p>Subject to the terms of this Privacy Notice, Propella uses the above described categories of personal
                information in several ways. Unless otherwise stated specifically, the above information may be used
                for any of the following purposes:
            </p>
            <ul>
                to administer the Service to you;
                to respond to your requests;
                to distribute communications relevant to your use of the Service, such as system updates or information about your use of the Service;
                as may be necessary to support the operation of the Service, such as for billing, account maintenance, and record-keeping purposes;
                to send to you Propella solicitations, product announcements, and the like that we feel may be of interest to  you. <span style="color: red">Please note that you may “opt out” of receiving these marketing materials</span>
                in other manners after subsequent notice is provided to you and/or your consent is obtained, if necessary.
            </ul>
            <p><b>Propella does not sell, re-sell, or distribute for re-sale your personal information.</b></p>
            <p><b>How do we share your Personal Information with third parties?</b></p>
            <p>If you agree to this in writing, we may provide any of the described categories of personal information
                to Propella employees, funders consultants, affiliates or other businesses or persons for the purpose
                of processing such information on our behalf in order to provide the Service to you. In such circumstances,
                we require that these parties agree to protect the confidentiality of such information consistent with the
                terms of this Privacy Notice.</p>
            <p>We will not share your personal information with other, third-party companies for their commercial or
                marketing use without your consent or except as part of a specific program or feature which you will
                specifically be able to <span style="color: red">opt-out of</span>.</p>
            <p>In addition, we may release personal information: (i) to the extent we have a good-faith belief that such
                action is necessary to comply with any applicable law; (ii) to enforce any provision of the Terms of Service ,
                protect ourselves against any liability, defend ourselves against any claims, protect the rights, property
                and personal safety of any user, or protect the public welfare; (iii) when disclosure is required to
                maintain the security and integrity of the Service, or to protect any user’s security or the security
                of other persons, consistent with applicable laws (iv) to respond to a court order, subpoena, search
                warrant, or other legal process, to the extent permitted and as restricted by law; or (v) in the event
                that we go through a business transition, such as a merger, divestiture, acquisition, liquidation or
                sale of all or a portion of our assets.
            </p>
            <p><b>Direct Marketing Communications</b></p>
            <p>We may communicate with you using email, SMS,whatsapp and other channels (sometimes through automated means)
                as part of our effort to market our products or services, administer or improve our products or services,
                or for other reasons stated in this Privacy Notice. You have an opportunity to withdraw consent to receive
                such direct marketing communications, as permitted by law.
            </p>
            <p>If you no longer wish to receive correspondence, emails, or other communications from us, you may opt-out by
                submitting a request through to the following email address reception@propellaincubator.co.za</p>
            <p>Please note that you may continue to receive non-marketing communications as may be required to maintain
                your relationship with Propella</p>
            <p>In addition to the communication described here, you may receive third-party marketing communications from
                providers we have engaged to market or promote our products and services. These third-party providers may
                be using communications lists they have acquired on their own, and you may have opted-in to those lists
                through other channels.  If you no longer wish to receive emails, SMSs, or other communications from
                such third parties, you may need to contact that third party directly.</p>
            <p><b>Retention of Data</b></p>
            <p>Propella will retain your personal information only for as long as is necessary for the purposes set
                out in this Notice. We will retain and use personal information to the extent necessary to comply
                with our legal obligations (for example, if we are required to retain your data to comply with applicable laws),
                resolve disputes and enforce our legal agreements and policies.
            </p>
            <p>Propella will also retain usage data for internal analysis purposes. Usage data is generally retained for
                a shorter period of time, except when this data is used to strengthen the security or to improve the
                functionality of our Sites and/or Portals, or we are legally obligated to retain this data for longer periods.
            </p>
            <p><b>Cookies, Device Data, and How it is Used</b></p>
            <p>When you use our Service, we may record unique identifiers associated with your device
                (such as the device ID and IP address), your activity within the Service, and your network location.
                Propella uses aggregated information (such as anonymous user usage information, cookies,
                IP addresses, browser type, clickstream information, etc.) to improve the quality and design
                of the Service and to create new features, promotions, functionality, and services by storing,
                tracking, and analyzing user preferences and trends. Specifically, we may automatically collect
                the following information about your use of Service through cookies, web beacons,
                and other technologies: </p>
            <ul>
                domain name
                browser type and operating system
                web pages you view
                links you click
                IP address
                the length of time you visit the Sites, Portals, and/or Services
                the referring URL or the webpage that led you to the Sites

            </ul>
            <p>We may also collect information regarding application-level events, such as crashes, and associate
                that temporarily with your account to provide customer service. In some circumstances,
                we may combine this information with personal information collected from you
                (and third-party service providers may do so on behalf of us).
            </p>
            <p>In addition, we may use "cookies," clear gifs, and log file information that help us determine the type of
                content and pages to which you link, the length of time you spend at any particular area of the Service,
                and the portion of the Service you choose to use. A cookie is a small text file that is sent by a website
                to your computer or mobile device where it is stored by your web browser. A cookie contains limited
                information, usually a unique identifier and the name of the site. Your browser has options to accept,
                reject or provide you with notice when a cookie is sent. Our cookies can only be read by Propella
                they do not execute any code or virus; and they do not contain any personal information.
                Cookies allow Propella to serve you better and more efficiently, and to personalize your experience
                with the Service. We may use cookies for many purposes, including (without limitation) to save
                your password so you don’t have to re-enter it each time you visit the Service, and to deliver content
                (which may include third party advertisements) specific to your interests.
            </p>
            <p>We may use third party service providers to help us analyze certain online activities. For example, these
                service providers may help us measure the performance of our online campaigns or analyze visitor activity
                on the Service. We may permit these service providers to use cookies and other technologies to perform these
                services for Propella. We do not share any personal information about our customers with these third-party service
                providers, and these service providers do not collect such personal information on our behalf. Our third-party
                service providers are required to comply fully with this Privacy Notice.</p>
            <p><b>South African Privacy Rights</b></p>
            <p>If you are a South African resident, South Africa law may provide you with certain rights with regard to your
                personal information under the Protection of Personal Information Act (“POPIA”) and Promotion of Access to
                Information Act (“PAIA”).as well the Consumer Protection Act Throughout this Privacy Notice you will find
                information required by POPIA regarding the categories of personal information collected from you; the
                purposes for which we use personal information, and the categories of third parties your data may be
                shared with.  This information is current as of the date of the Notice and is applicable in the 12 months
                preceding the effective date of the Notice.
            </p>
            <p>As a South African resident, the POPIA and PAIA provide you the ability to make inquiries regarding to your
                personal information. Specifically, the degree to which the information is not already provided in this
                Privacy Notice, you have the right to request disclosure or action your personal information, including:
            </p>
            <ul>
                If your personal information is collected by us.
                The specific pieces of personal information collected about you.
                The ability to correct or delete certain personal information collected about you.
                The ability to delete all the personal information collected about you, subject to certain exceptions.
                To opt-in or opt-out of direct marketing to you.
                To object to processing of your personal information, or
                Appeal any rejection of access to your personal information
            </ul>
            <p>You may submit a request regarding your rights under POPIA or PAIA by submitting a request through the
                following formreception@propellaincubator.co.za  or by contacting us at one of the following:
                +27 41 502 3700 .
            </p>
            <p>If we receive a POPIA request from you, we will first make a determination regarding the applicability of the law,
                and we will then take steps to verify your identity prior to responding. The steps to verify your identity may
                vary based on our relationship with you, but, at a minimum, it will take the form of confirming and matching
                the information submitted in the request with information already held by Propella and/or contacting you through
                previously used channels to confirm that you submitted the request (i.e. confirming identity through contact
                information that we have on file, and/or the contact information submitted to make the request).
            </p>
            <p>Propella does not knowingly collect or process the special personal information such as your religious or
                philosophical beliefs, race or ethnic origins, trade union memberships, political persuasion,
                health or sex life, or your criminal behaviour or biometric information.
            </p>
            <p>If you have a comment, question, or complaint about how we are processing your personal information,
                we hope that you contact us at reception@propellaincubator.co.za[INSERT FORM]  in order to allow us
                to resolve the matter. In addition, if you are located in the Republic of South Africa, you may submit
                a complaint regarding the processing of your personal information to the Information Regulator at the following link:
                <a href="https://www.justice.gov.za/inforeg/contact.html" target="_blank">  <span style="color: blue">https://www.justice.gov.za/inforeg/contact.html</span></a>
            </p>
            <p><b>Third Party Advertisers</b></p>
            <p>We may allow other companies, called third-party ad servers or ad networks, to serve advertisements within the
                Service. These third-party ad servers or ad networks use technology to send, directly to your device,
                the advertisements and links that appear on the Service. They automatically receive your device ID and
                IP address when this happens. They may also use other technologies (such as cookies, JavaScript, or Web Beacons)
                to measure the effectiveness of their advertisements and to personalize the advertising content you see.
                You should consult the respective privacy policies of these third-party ad servers or ad networks for
                more information on their practices and for instructions on how to opt-out of certain practices.
                This Privacy Notice does not apply to them, and we cannot control their activities.
            </p>
            <p><b>Information Storage and Security</b></p>
            <p>We employ industry-standard and/or generally accepted security measures designed to secure the integrity and
                confidentiality of all information submitted through the Service. However, the security of information transmitted
                through the internet or via a mobile device can never be guaranteed. We are not responsible for any interception
                or interruption of any communications through the internet or for changes to or losses of data.</p>
            <p>Users of the Service are responsible for maintaining the security of any password, user ID or other form of
                authentication involved in obtaining access to password protected or secure areas of the Service.
                In order to protect you and your information, we may suspend your use of any of the Service,
                without notice, pending an investigation, if any breach of security is suspected.</p>
            <p><b>External Links</b></p>
            <p>The Service may contain links to other websites maintained by third parties. Please be aware that we exercise
                no control over linked sites and Propella is not responsible for the privacy practices or the content
                of such sites. Each linked site maintains its own independent privacy and data collection policies and procedures,
                and you are encouraged to view the privacy policies of these other sites before providing any personal
                information.
            </p>
            <p>You hereby acknowledge and agree that Propella is not responsible for the privacy practices, data collection
                policies and procedures, or the content of such third-party sites, and you hereby release Propella
                from any and all claims arising out of or related to the privacy practices, data collection policies
                and procedures, and/or the content of such third-party sites.
            </p>
            <p><b>Changes to this Privacy Notice</b></p>
            <p>Proeplla reserves the right to modify this Privacy Notice from time to time in order that it accurately reflects
                the regulatory environment and our data collection principles. When material changes are made to this Privacy
                Notice, Propella will post the revised Notice on our website. This Privacy Notice was last modified as of 11 June 2021</p>
            <p><b>Contact Us</b></p>
            <p>If you have any questions or comments about this Privacy Notice or the Service provided by Propella,
                please contact us at: reception@propellaincubator.co.za</p>


        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>


    <div class="row" id="Mobile">
        <div class="row">
            @if(is_null($incubatee->venture_id))
            <div class="col s12 m6">
                <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1)">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i>  Add Venture</span>
                        <h6><b>This is Where You Create Your Venture</b></h6>
                    </div>
                    <div class="card-action center white-text" style="border-radius: 10px;">
                        <a style="color: white" href="{{url('/incubatee-create-venture', $incubatee->id)}}"
                           ><b>Create Your
                                Venture</b></a>
                    </div>
                </div>
            </div>
            @elseif($incubatee->venture_id)
                <div class="col s12 m6">
                    <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1)">
                        <div class="card-content white-text">
                            <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i>Edit Venture Details</span>
                            <h6><b>This is Where You Edit Your Venture</b></h6>
                        </div>
                        <div class="card-action center white-text" style="border-radius: 10px;">
                            <a style="color: white" href="{{url('/venture-edit/'.$incubatee->venture->slug)}}"
                               onclick="viewVenture(this)"
                              ><b>Manage Your Venture</b></a>
                        </div>
                    </div>
                </div>
                @endif
        </div>
        <div class="row">
            <div class="col s12 m6">
                <div class="card" style="border-radius: 10px;background-color: grey">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i>  Edit Personal Details</span>
                        <h6><b>Edit Your Personal Details Here!</b></h6>
                    </div>
                    <div class="card-action center" style="border-radius: 10px;">
                        <a style="color: white" href="{{url('/incubatee-edit', $incubatee->id)}}"
                           onclick="createVenture(this)"
                           ><b>Edit Your Details</b></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m6">
                <div class="card" style="border-radius: 10px;background-color:green">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i>  Events</span>
                        <h6><b>This is Where You Register For Events!</b></h6>
                    </div>
                    <div class="card-action center" style="border-radius: 10px;">
                        <a style="color: white" id="incubatee-events-button-mobile"
                           style="cursor: pointer;"><b>View Calendar</b></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <input hidden disabled value="{{$incubatee->id}}" id="incubatee-id-input">
            <div class="col s12 m6">
                <div class="card" style="border-radius: 10px;background-color: grey">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i>   Book Venues</span>
                        <p style="align-content: center; ">This is where you view your booked venues.</p>
                        <br/>
                    </div>
                    <div class="card-action center" style="border-radius: 15px;">
                        <a  id="user-venue-button" href="{{url('/show-booking/'.$user->id)}}"
                           style="cursor: pointer;color: white"><b>View
                                Venues</b></a>
                    </div>
                </div>
            </div>
        </div>
        @foreach($user->roles as $user_role)
            @if($user_role->name == 'panelist')
                <div class="row">
                    <div class="col s12 m6">
                        <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1)">
                            <div class="card-content white-text">
            <span class="card-title" style="font-weight: bolder"><i
                    class="small material-icons">border_color</i> Panel Interview </span>
                                <p style="align-content: center; ">This is where you will see the applicants
                                    assigned to your panel interview. </p>
                            </div>
                        </div>
                        <div class="card-action center" style="border-radius: 15px;">
                            <a class="btn" href="/panelist-show-ict-applicants"
                               style="cursor: pointer; color: white;background-color: #1a237e; "><b>View
                                    ICT</b></a>
                            <a class="btn" href="/panelist-show-industrial-applicants"
                               style="cursor: pointer; color: white;background-color: #1a237e; "><b>View
                                    Industrial</b></a>
                            <a class="btn" href="/panelist-show-ventures"
                               style="cursor: pointer; color: white;background-color: #1a237e;"><b>View
                                    Ventures</b></a>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        @if($incubatee->venture_id)
            <div class="row">
                <div class="col s12 m6">
                    <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1)">
                        <div class="card-content white-text">
   <span class="card-title" style="font-weight: bolder"><i
           class="small material-icons">person_add</i> Add Employee</span>
                            <br/>
                            <p style="align-content: center; ">This is where you view and add your employee.</p>
                        </div>
                        <div class="card-action center" style="border-radius: 15px;">
                            <a style="color: white" id="user-venue-button"
                               href="{{url('/employee-index/'.$incubatee->venture->id)}}"
                               style="cursor: pointer; "><b>Mange
                                    Your Employees</b></a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="col s12 m3">
            <div class="card" style="border-radius: 10px;background-color:green">
                <div class="card-content white-text">
            <span class="card-title" style="font-weight: bolder"><i
                    class="small material-icons">person_add</i>Workshop</span>
                    <br/>
                    <p style="align-content: center; ">Workshop evaluation form</p>
                </div>
                <div class="card-action center" style="border-radius: 15px;">
                    <div class="card-action center" style="border-radius: 15px; padding: 0px">
                        @if($incubatee->venture->stage == 'Stage 2')
                        {{--STAGE 2 PRE WORKSHOP CARD--}}
                        <div class="row">
                            <div class="col s12">
                                @foreach($pre_stage2_workshop as $pre_stage2_workshops)
                                    @if($incubatee->pre_stage_two_complete == true)
                                        @else
                                    <a class="btn blue tooltipped modal-trigger" data-position="left" href="#pre_stage2_workshop_mobile">
                                        {{$pre_stage2_workshops->category_name}}</a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        {{--STAGE 2 POST WORKSHOP CARD--}}
                        <div class="row">
                            <div class="col s12">
                                @foreach($post_stage2_workshop as $post_stage2_workshops)
                                    @if($incubatee->pre_stage_two_complete == false)
                                        @else
                                    <a class="btn blue tooltipped modal-trigger" data-position="left" href="#post_stage2_workshop_mobile">
                                        {{$post_stage2_workshops->category_name}}</a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <!--stage3-->
                        @elseif($incubatee->venture->stage == 'Stage 3')
                        {{--STAGE 3 PRE WORKSHOP CARD--}}
                        <div class="row">
                            <div class="col s12">
                                @foreach($pre_stage3_workshop as $pre_stage3_workshops)
                                    @if($incubatee->pre_stage_three_complete == true)
                                        @else
                                    <a class="tooltipped modal-trigger" data-position="left" href="#pre_stage3_workshop_mobile">
                                        {{$pre_stage3_workshops->category_name}}</a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        {{--STAGE 2 POST WORKSHOP CARD--}}
                        <div class="row">
                            <div class="col s12">
                                @foreach($post_stage3_workshop as $post_stage3_workshops)
                                    @if($incubatee->pre_stage_three_complete == false)
                                        @else
                                    <a class=" tooltipped modal-trigger" data-position="left" href="#post_stage3_workshop_mobile">
                                        {{$post_stage3_workshops->category_name}}</a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @elseif($incubatee->venture->stage == 'Stage 4')
                        <!--stage4-->
                        {{--STAGE 4 PRE WORKSHOP CARD--}}
                        <div class="row">
                            <div class="col s12">
                                @foreach($pre_stage4_workshop as $pre_stage4_workshops)
                                    @if($incubatee->pre_stage_four_complete == true)
                                        @else
                                    <a class="btn blue tooltipped modal-trigger" data-position="left" href="#pre_stage4_workshop_mobile">
                                        {{$pre_stage4_workshops->category_name}}</a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        {{--STAGE 4 POST WORKSHOP CARD--}}
                        <div class="row">
                            <div class="col s12">
                                @foreach($post_stage4_workshop as $post_stage4_workshops)
                                    @if($incubatee->pre_stage_four_complete == false)
                                        @else
                                    <a class="btn blue tooltipped modal-trigger" data-position="left" href="#post_stage4_workshop_mobile">
                                        {{$post_stage4_workshops->category_name}}</a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @elseif($incubatee->venture->stage == 'Stage 5')
                        <!--stage5-->
                        {{--STAGE 5 PRE WORKSHOP CARD--}}
                        <div class="row">
                            <div class="col s12">
                                @foreach($pre_stage5_workshop as $pre_stage5_workshops)
                                    @if($incubatee->pre_stage_five_complete == true)
                                        @else
                                    <a class="btn blue tooltipped modal-trigger" data-position="left" href="#pre_stage5_workshop_mobile">
                                        {{$pre_stage5_workshops->category_name}}</a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        {{--STAGE 5 POST WORKSHOP CARD--}}
                        <div class="row">
                            <div class="col s12">
                                @foreach($post_stage5_workshop as $post_stage5_workshops)
                                    @if($incubatee->pre_stage_five_complete == false)
                                        @else
                                    <a class="btn blue tooltipped modal-trigger" data-position="left" href="#post_stage5_workshop_mobile">
                                        {{$post_stage5_workshops->category_name}}</a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                            @endif
                    </div>
                </div>
            </div>

            <!--PRE STAGE 2 EVALUATION FORM-->
            <div id="pre_stage2_workshop_mobile" class="modal">
                @foreach($pre_stage2_workshop as $pre_stage2_workshops)
                    <h4 class="center-align"><b>{{$pre_stage2_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->pre_stage_two_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($pre_stage2_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-mobile" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-mobile" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="pre-stage2-upload-answers-form-mobile" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>
            <!--PRE STAGE 2 EVALUATION FORM-->
            <div id="post_stage2_workshop_mobile" class="modal">
                @foreach($post_stage2_workshop as $post_stage2_workshops)
                    <h4 class="center-align"><b>{{$post_stage2_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->post_stage_two_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10),
                                    with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($post_stage2_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text"
                                                           class=" answer-text-mobile" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1"
                                                           max="10" class=" answer-text-mobile" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="post-stage2-upload-answers-form-mobile"
                                name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>

            <!--PRE STAGE 3 EVALUATION FORM-->
            <div id="pre_stage3_workshop_mobile" class="modal">
                @foreach($pre_stage3_workshop as $pre_stage3_workshops)
                    <h4 class="center-align"><b>{{$pre_stage3_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->pre_stage_three_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($pre_stage3_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-mobile" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-mobile" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="pre-stage3-upload-answers-form-mobile" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>
            <!--PRE STAGE 3 EVALUATION FORM-->
            <div id="post_stage3_workshop_mobile" class="modal">
                @foreach($post_stage3_workshop as $post_stage3_workshops)
                    <h4 class="center-align"><b>{{$post_stage3_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->post_stage_three_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10),
                                    with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($post_stage3_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text"
                                                           class=" answer-text-mobile" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1"
                                                           max="10" class=" answer-text-mobile" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="post-stage3-upload-answers-form-mobile"
                                name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>

            <!--PRE STAGE 4 EVALUATION FORM-->
            <div id="pre_stage4_workshop_mobile" class="modal">
                @foreach($pre_stage4_workshop as $pre_stage4_workshops)
                    <h4 class="center-align"><b>{{$pre_stage4_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->pre_stage_four_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($pre_stage4_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-mobile" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-mobile" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="pre-stage4-upload-answers-form-mobile" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>
            <!--PRE STAGE 4 EVALUATION FORM-->
            <div id="post_stage4_workshop_mobile" class="modal">
                @foreach($post_stage4_workshop as $post_stage4_workshops)
                    <h4 class="center-align"><b>{{$post_stage4_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->post_stage_four_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10),
                                    with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($post_stage4_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text"
                                                           class=" answer-text-mobile" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1"
                                                           max="10" class=" answer-text-mobile" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="post-stage4-upload-answers-form-mobile"
                                name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>

            <!--PRE STAGE 5 EVALUATION FORM-->
            <div id="pre_stage5_workshop_mobile" class="modal">
                @foreach($pre_stage5_workshop as $pre_stage5_workshops)
                    <h4 class="center-align"><b>{{$pre_stage5_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->pre_stage_five_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($pre_stage5_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-mobile" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-mobile" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="pre-stage5-upload-answers-form-mobile" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>
            <!--PRE STAGE 5 EVALUATION FORM-->
            <div id="post_stage5_workshop_mobile" class="modal">
                @foreach($post_stage5_workshop as $post_stage5_workshops)
                    <h4 class="center-align"><b>{{$post_stage5_workshops->category_name}}</b></h4>
                @endforeach
                @if($incubatee->post_stage_five_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10),
                                    with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($post_stage5_workshop as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text"
                                                           class=" answer-text-mobile" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1"
                                                           max="10" class=" answer-text-mobile" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="post-stage5-upload-answers-form-mobile"
                                name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>

        </div>
        @if($user_enquiries != null or $user_pre_assigned_enquiries != null)
            @if(count($user_enquiries) > 0 or count($user_pre_assigned_enquiries) > 0)
        <div class="row">
            <div class="col s12 m6">
                <div class="card" style="border-radius: 10px;background-color:rgba(0, 47, 95, 1)">
                    <div class="card-content white-text">
      <span class="card-title" style="font-weight: bolder"><i
              class="small material-icons">border_color</i> Manage General Enquiries </span>
                        <p style="align-content: center; ">This is where you will see the enquiries that
                            you have been assigned to. </p>
                    </div>
                    <div class="card-action center" style="border-radius: 15px;">
                        <a href="/pre-assigned-enquiry-index"
                           style="cursor: pointer; color: white;"><b>View Pre-Assigned
                                Enquirires</b></a>
                        <a href="/assigned-enquiry-index"
                           style="cursor: pointer;"><b>View Assigned Enquirires</b></a>
                    </div>
                </div>
            </div>
        </div>
            @endif
        @endif
    </div>


    <script>
        /* pre2 evaluation  desktop*/
        $('#pre-stage2-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-pre-stage2-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* post2 evaluation  desktop*/
        $('#post-stage2-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-stage2-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* pre2 evaluation  mobile*/
        $('#pre-stage2-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-pre-stage2-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $("#pre-stage2-upload-answers-form-mobile").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* post2 evaluation  mobile*/
        $('#post-stage2-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-stage2-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $("#post-stage2-upload-answers-form-mobile").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });




        /* pre3 evaluation  desktop*/
        $('#pre-stage3-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-pre-stage3-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                      window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* post3 evaluation  desktop*/
        $('#post-stage3-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-stage3-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* pre3 evaluation  mobile*/
        $('#pre-stage3-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-pre-stage3-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $("#pre-stage3-upload-answers-form-mobile").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* post3 evaluation  mobile*/
        $('#post-stage3-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-stage3-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $("#post-stage3-upload-answers-form-mobile").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });



        /* pre4 evaluation  desktop*/
        $('#pre-stage4-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-pre-stage4-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* post4 evaluation  desktop*/
        $('#post-stage4-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-stage4-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* pre4 evaluation  mobile*/
        $('#pre-stage4-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-pre-stage4-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $("#pre-stage4-upload-answers-form-mobile").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* post4 evaluation  mobile*/
        $('#post-stage4-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-stage4-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $("#post-stage4-upload-answers-form-mobile").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });


        /* pre5 evaluation  desktop*/
        $('#pre-stage5-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-pre-stage5-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* post5 evaluation  desktop*/
        $('#post-stage5-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-stage5-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* pre5 evaluation  mobile*/
        $('#pre-stage5-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-pre-stage5-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $("#pre-stage5-upload-answers-form-mobile").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* post5 evaluation  mobile*/
        $('#post-stage5-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-stage5-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $("#post-stage5-upload-answers-form-mobile").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });


        function viewShadowboard(obj) {
            let incubatee_id = obj.getAttribute('data-value');

            location.href = 'venture-shadow/' + incubatee_id;
        }

        $('#incubatee-events-button').on('click', function () {
            let incubatee_id = $('#incubatee-id-input').val();
            window.location.href = '/users/incubatee-dashboard/full-calendar/show-events/' + incubatee_id;
        });
        $('#incubatee-events-button-mobile').on('click', function () {
            let incubatee_id = $('#incubatee-id-input').val();
            window.location.href = '/users/incubatee-dashboard/full-calendar/show-events/' + incubatee_id;
        });
        function createVenture(obj) {
            let incubatee_id = obj.getAttribute('data-value');

            location.href = '/venture-edit/' + incubatee_id;
        }

        function viewVenture(obj) {
            let incubatee_id = obj.getAttribute('data-value');

            location.href = '/venture-edit/' + incubatee_id;
        }

        $(document).ready(function () {

            let stored_event_id = sessionStorage.getItem('stored_event_id');
            let stored_found_out = sessionStorage.getItem('stored_found_out_via');
            let incubatee_id = $('#incubatee-id-input').val();

            if (stored_event_id !== "" && stored_found_out !== "") {
                let formData = new FormData();
                formData.append('event_id', stored_event_id);
                formData.append('found_out_via', stored_found_out);
                formData.append('incubatee_id', incubatee_id);

                let url = "{{route('incubatee.storeIncubateeEventViaPublicEventPage')}}";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        sessionStorage.setItem('stored_event_id', "");
                        sessionStorage.setItem('stored_found_out_via', "");
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            }
        });

        $('#incubatee-events-button').on('click', function () {
            let incubatee_id = $('#incubatee-id-input').val();
            window.location.href = '/users/incubatee-dashboard/full-calendar/show-events/' + incubatee_id;
        });
        //UPLOAD WORSHOP FORM
        $('#upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-workshop-incubatee-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //UPLOAD WORSHOP FORM
        $('#upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-workshop-incubatee-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //DESKTOP
        //STAGE 3 UPLOAD WORSHOP FORM
        $('#stage3-upload-answers-form').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.stage3-answer-text').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-stage3-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //End of STAGE 3 UPLOAD WORSHOP FORM
        $('#end-of-stage3-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.end-of-stage3-answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-end-of-stage-3-evaluation",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //End of STAGE 3 UPLOAD WORSHOP FORM
        $('#end-of-stage3-upload-answers-form').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.end-of-stage3-answer-text').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-end-of-stage-3-evaluation",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //STAGE 4 UPLOAD WORSHOP FORM
        $('#stage4-upload-answers-form').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.stage4-answer-text').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-stage4-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //STAGE 5 UPLOAD WORSHOP FORM
        $('#stage5-upload-answers-form').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.stage5-answer-text').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-stage5-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //STAGE 6 UPLOAD WORSHOP FORM
        $('#stage6-upload-answers-form').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.stage6-answer-text').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-stage6-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //MOBILE
        //STAGE 3 UPLOAD WORSHOP FORM
        $('#stage3-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.stage3-answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-stage3-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //STAGE 4 UPLOAD WORKSHOP FORM
        $('#stage4-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.stage4-answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-stage4-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //STAGE 5 UPLOAD WORSHOP FORM
        $('#stage5-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.stage5-answer-text-mobie').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-stage5-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //STAGE 6 UPLOAD WORSHOP FORM
        $('#stage6-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.stage6-answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-stage6-evaluation-form",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Form saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>

@endsection
