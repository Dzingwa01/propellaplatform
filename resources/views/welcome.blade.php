@extends('layouts.app')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <link rel="stylesheet" type="text/css" href="/css/Home/HomePage.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

    <div class="row" id="homeDesktop">
        <div class="row" id="Tags" style="margin-bottom: 0px!important;">
            <a href="/Home/Innovators">
            <div class="col s2 Innovators"  id="" style="cursor:pointer;">
                <div class="overlay">
                <text class="txt " id="InnovateTxt">Innovators</text>
                <text class="txt"  id="InnovateTxt2">Bring ideas to life</text>
                </div>
            </div>
            </a>
            <a href="/Home/Partners">
            <div class="col s2 Partner" id="" style="cursor:pointer">
                <div class="overlay">
                <text class="txt box"  id="PartnerTxt">Partner With Us</text>
                <text class="txt" id="PartnerTxt2">Join the winning team</text>
                </div>
            </div>
            </a>
            <a href="/Home/Incubatees">
            <div class="col s2 FutureMakers" id="" style="cursor:pointer">
                <div class="overlay">
                <text class="txt box" id="FutureMakersTxt">ICT Ventures</text>
                <text class="txt" id="FutureMakersTxt2">Technology taking us to the future</text>
                </div>
            </div>
            </a>
            <a href= "/Home/INDVentures">
            <div class="col s2 Industrial" id="" style="cursor:pointer">
                <div class="overlay">
                <text class="txt box" id="IndustrialTxt">Industrial Ventures</text>
                <text class="txt" id="IndustrialTxt2">Leaders in the game</text>
                </div>
            </div>
            </a>
            <a href="/pti-ventures">
            <div class="col s2 SmartCity" id="" style="cursor:pointer">
                <div class="overlay">
                <text class="txt box" id="SmartCityTxt" style="margin-left: 12px">Media</text>
                <text class="txt" id="SmartCityTxt2">Our mini-gallery</text>
                </div>
            </div>
            </a>
            <a href="/Application-Process/question-category">
            <div class="col s2 Apply" id="" style="cursor:pointer">
                <div class="overlay">
                <text class="txt box" id="ApplyTxt">Apply</text>
                <text class="txt" id="ApplyTxt2">Show what you can do</text>
                </div>
            </div>
            </a>
        </div>
    </div>

    {{-- POPUP WINDOW--}}
     <div class="" id="desktopKutu">
        <div class="kutu" >
            <div id="newsletter">
                <div class="centered">
                    <form action="">
                        <input  id="fullname" type="text" placeholder="Name & Surname" />
                        <input  id="email" type="text" placeholder="Enter your email here" />
                        <a class="waves-effect waves-light btn section" id="submit-newsletter">Subscribe now</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- POPUP WINDOW--}}
   <div class="" id="MobileKutu">
        <div class="kutu">
            <div id="newsletterMobile" style="width: 500px;">
                <div class="centered" style="margin-left: 2em">
                    <form action="">
                        <input  id="fullname-mobile" type="text" placeholder="Name & Surname" />
                        <input  id="email-mobile" type="text" placeholder="Enter your email here" />
                        <a class="waves-effect waves-light btn section" id="submit-newsletter-mobile">Subscribe now</a>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div class="container" id="mobileHome">
        <div class="carousel" id="myCarousel" style="margin-top: 5vh">
            <a class="carousel-item Innovators "style="width: 100%;height:100%" href="/Home/Innovators">
                <div class="overlay"></div>
                <div class="txt" id="InnovateTxt" style="bottom:50px;">Innovators</div>
                <div class="txt" id="InnovateTxt2" style="bottom:50px;">Bring ideas to life</div>
            </a>
            <a class="carousel-item Partner"style="width:100%;height:100%" href="/Home/Partners">
                <div class="overlay"></div>
                <div class="txt" id="PartnerTxt" style="bottom:125px;">Partner with us</div>
                <div class="txt" id="PartnerTxt2" style="bottom:125px;">Join the winning team</div>
            </a>
            <a class="carousel-item FutureMakers"style="width:100%;height:100%" href="/Home/Incubatees">
                <div class="overlay"></div>
                <div class="txt" id="FutureMakersTxt" style="bottom:125px">ICT Ventures</div>
                <div class="txt" id="FutureMakersTxt2" style="bottom:125px;">Technology taking us to the future</div>
            </a>
            <a class="carousel-item Industrial"style="width:100%;height:100%" href= "/Home/INDVentures">
                <div class="overlay"></div>
                <div class="txt" id="IndustrialTxt" style="bottom:125px">Industrial Ventures</div>
                <div class="txt" id="IndustrialTxt2" style="bottom:125px;">Leaders in the game</div>
            </a>
            <a class="carousel-item SmartCity"style="width:100%;height:100%" href="/pti-ventures">
                <div class="overlay"></div>
                <div class="txt" id="SmartCityTxt" style="bottom:125px">Media</div>
                <div class="txt" id="SmartCityTxt2" style="bottom:125px;">Our mini-gallery</div>
            </a>
            <a class="carousel-item Apply"style="width:100%;height:100%" href="/Application-Process/question-category">
                <div class="overlay"></div>
                <div class="txt" id="ApplyTxt">Apply</div>
                <div class="txt" id="ApplyTxt2">Show what you can do</div>
            </a>
        </div>
    </div>
    <div class="" hidden style="margin-left: 2em;margin-right: 2em">
        <h4 class="center-align"><b>OUR PARTNERS</b></h4>
        <section class="customer-logos slider" style="height: 100%">
            <div class="slide"><img src="images/About/LOGOS/engeli1.jpg"></div>
            <div class="slide"><img src="images/About/LOGOS/NELSON MANDELA 2.jpg"></div>
            <div class="slide"><img src="images/About/LOGOS/INNOVOLVE.jpg"></div>
            <div class="slide"><img src="images/About/LOGOS/BASF.jpg"></div>
            <div class="slide"><img src="images/About/LOGOS/isuzu1.png"></div>
            <div class="slide"><img src="images/About/LOGOS/sefa_new-removebg-preview.png"></div>
            <div class="slide"><img src="images/About/LOGOS/seda_new-removebg-preview.png"></div>
        </section>
        <br>
    </div>
    <div class="row mobileHome" hidden>
        <div class="wrapper" hidden>
            <div class="col s4">
                <div class="counter" style="background-color: darkblue">
                    <i class="fa fa-lightbulb fa-2x hoverable"></i>
                    <h2 class="count white-text">80</h2>
                    <p class="center-align white-text"># incubated ventures</p>
                </div>
            </div>
            <div class="col s4">
                <div class="counter" style="background-color: #454242">
                    <i class="fa fa-building fa-2x hoverable"></i>
                    <h2 class="count white-text">191</h2>
                    <p class="center-align white-text"># ventures onboarded</p>
                </div>
            </div>
            <div class="col s4">
                <div class="counter"  style="background-color: green">
                    <i class="fa  fa-coffee fa-2x hoverable"></i>
                    <h2 class="count white-text">8600</h2>
                    <p class="center-align white-text">Litres of coffee consumed</p>
                </div>
            </div>
        </div>
    </div>
    {{--Upcoming events--}}
    <div class="row mobileHome" hidden style="margin-left: 3em;margin-right: 3em;margin-top: 10vh">
        <h4 class="center-align"><b>UPCOMING EVENTS</b></h4>
        <br>
        <div class="row" style="text-align: center;margin-top:4em;">

            <div class="row"  style="margin-top: 10vh">
                <div class="col s12">
                    @for($i = 0; $i<count($events_array); $i++)
                        @if($i <= 2)
                            <div class="container col s12 blog card1  ">
                                <div class="card" >
                                    <div class="card-image">
                                        <img style="width:100%;height: 40vh;"
                                             src="/storage/{{isset($events_array[$i]->event->event_image_url)?$events_array[$i]->event->event_image_url:'Nothing Detect]=ed'}}">
                                        <div class="overlay">
                                            <br>
                                            <br>
                                            <br>
                                            <h6 style="font-size: 16px" class="center-align"><b>{{$events_array[$i]->event->title}}</b></h6>
                                            <h6 style="font-size: 16px" id="start"
                                                value="{{$events_array[$i]->start_time}}">{{$events_array[$i]->start_time}} </h6>
                                            <h6 style="font-size: 16px">{{$events_array[$i]->event->start->toDateString()}}</h6>
                                            @if(isset($events_array[$i]->event->EventVenue->venue_name))
                                                <h6 style="font-size: 16px"> {{$events_array[$i]->event->EventVenue->venue_name}} </h6>
                                            @else
                                                <h6 style="font-size: 16px"> Old Building </h6>
                                            @endif
                                            @if($events_array[$i]->event->type == 'Private')
                                                <h6 class="private-event-type-header" style="color:red;">
                                                    <b>{{$events_array[$i]->event->type}}</b></h6>
                                            @else
                                                <h6 class="event-type-header">{{$events_array[$i]->event->type}} </h6>
                                            @endif
                                            <br>
                                            @if($events_array[$i]->event->start > new DateTime())
                                                @if($events_array[$i]->participant_count > $events_array[$i]->registered_participants)
                                                    <div class="row" style="margin-left:1em;">
                                                        <a href="#register-modal" class="modal-trigger"
                                                           data-value="{{$events_array[$i]->event->id}}"
                                                           data-type="{{$events_array[$i]->event->type}}"
                                                           style="color: orange;" onclick="saveEventDetails(this)">Register
                                                            | Deregister</a>
                                                    </div>
                                                @else
                                                    <div class="row" style="margin-left:1em;">
                                                        <a href="#participant-notification-modal" class="modal-trigger"
                                                           style="color: orange;">Register
                                                            | Deregister</a>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="row" style="margin-left: 1em;">
                                                <a href="#event-detail-modal" class="modal-trigger"
                                                   data-value="{{$events_array[$i]->event->invite_image_url}}"
                                                   onclick="showModal(this)"
                                                   style="color: orange;">Event Details</a>
                                            </div>

                                            @if(new DateTime() > $events_array[$i]->event->end)
                                                <div class="row" style="margin-left: 1em;">
                                                </div>
                                            @endif
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endfor
                </div>
            </div>

        </div>
        <div class="row center-align">
            <a href="/show-events" style="border-radius: 30px;background-color: darkblue" class="waves-effect waves-light btn-large"><i class="material-icons right">chevron_right</i><b>View more events</b></a>
        </div>
    </div>

    <div class="row desktop hidden">

       {{-- <a href="/donation-page">
            <button class="popup">DONATE</button>
        </a>--}}
        <div class="wrapper" hidden>
            <div class="col s4">
                <div class="counter" style="background-color: darkblue">
                    <i class="fa fa-lightbulb fa-2x hoverable"></i>
                    <h2 class="count white-text">80</h2>
                    <p class="center-align white-text">Number of currently incubated ventures</p>
                </div>
            </div>
            <div class="col s4">
                <div class="counter" style="background-color: #454242">
                    <i class="fa fa-building fa-2x hoverable"></i>
                    <h2 class="count white-text">191</h2>
                    <p class="center-align white-text">Total number of ventures onboarded</p>
                </div>
            </div>
            <div class="col s4">
                <div class="counter"  style="background-color: green">
                    <i class="fa  fa-coffee fa-2x hoverable"></i>
                    <h2 class="count white-text">8600</h2>
                    <p class="center-align white-text">Litres of coffee consumed</p>
                </div>
            </div>
        </div>
    </div>

    <div class="desktop" hidden>
         {{--Upcoming events--}}
         <div class="row" style="margin-left: 3em;margin-right: 3em;margin-top: 10vh">
             <h4 class="center-align"><b>UPCOMING EVENTS</b></h4>
             <br>
             <div class="row" style="text-align: center;margin-top:4em;">

                 <div class="row"  style="margin-top: 10vh">
                     <div class="col m12">
                         @for($i = 0; $i<count($events_array); $i++)
                             @if($i <= 2)
                                 <div class="container col l4 blog card1  ">
                                     <div class="card" >
                                             <div class="card-image">
                                                 <img style="width:100%;height: 40vh;"
                                                      src="/storage/{{isset($events_array[$i]->event->event_image_url)?$events_array[$i]->event->event_image_url:'Nothing Detect]=ed'}}">
                                                 <div class="overlay">
                                                     <br>
                                                     <br>
                                                     <p class="center-align"><b>{{$events_array[$i]->event->title}}</b></p>
                                                     <h6 style="font-size: 16px" id="start"
                                                         value="{{$events_array[$i]->start_time}}">{{$events_array[$i]->start_time}} </h6>
                                                     <h6 style="font-size: 16px">{{$events_array[$i]->event->start->toDateString()}}</h6>
                                                     @if(isset($events_array[$i]->event->EventVenue->venue_name))
                                                         <h6 style="font-size: 16px"> {{$events_array[$i]->event->EventVenue->venue_name}} </h6>
                                                     @else
                                                         <h6 style="font-size: 16px"> Old Building </h6>
                                                     @endif
                                                     @if($events_array[$i]->event->type == 'Private')
                                                         <h6 class="private-event-type-header" style="color:red;">
                                                             <b>{{$events_array[$i]->event->type}}</b></h6>
                                                     @else
                                                         <h6 class="event-type-header">{{$events_array[$i]->event->type}} </h6>
                                                     @endif
                                                     <br>
                                                     @if($events_array[$i]->event->start > new DateTime())
                                                         @if($events_array[$i]->participant_count > $events_array[$i]->registered_participants)
                                                             <div class="row" style="margin-left:1em;">
                                                                 <a href="#register-modal" class="modal-trigger"
                                                                    data-value="{{$events_array[$i]->event->id}}"
                                                                    data-type="{{$events_array[$i]->event->type}}"
                                                                    style="color: orange;" onclick="saveEventDetails(this)">Register
                                                                     | Deregister</a>
                                                             </div>
                                                         @else
                                                             <div class="row" style="margin-left:1em;">
                                                                 <a href="#participant-notification-modal" class="modal-trigger"
                                                                    style="color: orange;">Register
                                                                     | Deregister</a>
                                                             </div>
                                                         @endif
                                                     @endif
                                                     <div class="row" style="margin-left: 1em;">
                                                         <a href="#event-detail-modal" class="modal-trigger"
                                                            data-value="{{$events_array[$i]->event->invite_image_url}}"
                                                            onclick="showModal(this)"
                                                            style="color: orange;">Event Details</a>
                                                     </div>

                                                     @if(new DateTime() > $events_array[$i]->event->end)
                                                         <div class="row" style="margin-left: 1em;">
                                                         </div>
                                                     @endif
                                                 <br>
                                                 </div>
                                             </div>
                                         </div>
                                 </div>
                             @endif
                         @endfor
                     </div>
                 </div>

             </div>
             <div class="row center-align">
                 <a href="/show-events" style="border-radius: 30px;background-color: darkblue" class="waves-effect waves-light btn-large"><i class="material-icons right">chevron_right</i><b>View more events</b></a>
             </div>
         </div>
        <!--Testimonials-->
        <br>

     </div>
    <div class="testimonials" hidden style="margin-right: 2em;margin-left: 2em">
        <h4 class="center-align"><b>TESTIMONIALS</b></h4>
        <div class="test-body">
            <div class="item">
                <img src="images/Denise.png">
                <div class="name">Denise van Huyssteen</div>
                <small class="desig">Isuzu Motors South Africa</small>
                <div class="share"><i class="fa fa-facebook"></i><i class="fa fa-twitter"></i><i class="fa fa-instagram"></i></div>
                <p style="text-align:center;font-size: 1em"> As Isuzu Motors South Africa we value the support which we get from Propella for
                    some of
                    our key enterprise development programmes, as well with initiatives to further build
                    supplier capacity.
                    Propella has demonstrated excellent innovation in finding solutions to addressing
                    some
                    of the challenges which we face in our industry. Also very importantly,
                    their team members are very engaged, energetic and responsive to meeting the needs
                    of
                    our business.</p>
            </div>
            <div class="item">
                <img src="images/Linda[594].jpg">
                <div class="name">Linda Brown</div>
                <small class="desig">BASF</small>
                <div class="share"><i class="fa fa-facebook"></i><i class="fa fa-twitter"></i><i class="fa fa-instagram"></i></div>
                <p style="text-align:center;font-size: 1em">BASF South Africa is proud to be associated with the Propella Incubator in Port
                    Elizabeth. At BASF, we create chemistry for a sustainable future through encouraging
                    innovative and disruptive
                    thinking which then translates into practical solutions. Innovation is a key lens of
                    BASF’s global strategy, and it is only through innovation
                    that we can solve the challenges faced by business and society – today and into the
                    future. The Propella Incubator does exactly this – with a
                    sense of energy that is contagious. I would encourage any corporate that supports
                    creative entrepreneurship to get involved and be inspired by
                    the young minds and talent that are involved in the Propella programs.</p>
            </div>
        </div>
    </div>

    {{--register for upcoming events--}}
    <div id="register-modal" class="modal">
        <div class="modal-content">
            <h5>One more step!</h5>

            <select id="found-us-via-input">
                <option value="" disabled selected>How did you hear about us?</option>
                <option value="Radio">Radio</option>
                <option value="Email Campaign">Email Campaign</option>
                <option value="Facebook">Facebook</option>
                <option value="Website">Website</option>
                <option value="LinkedIn">LinkedIn</option>
                <option value="Twitter">Twitter</option>
                <option value="Print Media">Print Media</option>
                <option value="Other">Other</option>
            </select>
            <input id="found-us-via-other-input" hidden placeholder="Please specify?" type="text">

        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat" onclick="saveFoundOutDetails()">Submit</a>
        </div>
    </div>

    {{--participants modal--}}
    <div id="participant-notification-modal" class="modal">
        <div class="modal-content">
            <h5>So sorry that you missed out.</h5>

            <p>Unfortunately this event has exceeded the amount of participants that can register for this event.</p>
            <p>Please contact Aphelele Jonas at <a href="mailto:ahjonas@propellaincubator.co.za">agjonas@propellaincubator.co.za</a>
                for any enquiries on how you could possibly register.</p>
        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    {{--event details--}}
    <div id="event-detail-modal" class="modal">
        <div class="modal-content" id="modal-image-div">

        </div>
        <div class="modal-footer">
            <a href="#" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>
    <style>
        .centered {
            position: absolute;
            top: 53%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        .section{
            color:#fff;
            font-weight:bold;
            font-family:'Open Sans', sans-serif;
            border-color:#1c385c;
            box-shadow:inset 0 1px 0 #7fade7;
            text-shadow:0 -1px #34629f;
            width: 290px;
            background: linear-gradient(to top, #3e74bb 0, #4887dd 100%);
        }
        input {
            color:#fff;
            font-weight:bold;
            font-family:'Open Sans', sans-serif;
            border-color:grey;
            box-shadow:inset 0 1px 0 grey;
            text-shadow:0 -1px grey;
            background: linear-gradient(to top, grey 0, grey 100%);
        }
        input[type="text"] {
            padding:2px 12px;
            margin-bottom:8px;
            border-color:#c9c9c9;
            box-shadow:inset 0 1px 3px rgba(0,0,0,0.25);
            background-color: white;
        }
        input[type="submit"] {
            color:#fff;
            font-weight:bold;
            font-family:'Open Sans', sans-serif;
            border-color:#1c385c;
            box-shadow:inset 0 1px 0 #7fade7;
            text-shadow:0 -1px #34629f;
            background: linear-gradient(to top, #3e74bb 0, #4887dd 100%);
        }
        #newsletter {
            width:560px;
            height:60vh;
            margin:0 auto;
            background-image: url("/images/NewsletterPopUp2.png");
            position:relative;
            border-radius:4px;
            z-index:20;
        }
        #newsletterMobile {
            width:560px;
            height:55vh;
            margin:-270px;
            background-image: url("/images/NewsletterPopUp2.png");
            position:relative;
            border-radius:4px;
            z-index:20;
        }
        .kutu{
            position:relative;
            background-size: cover;
            z-index:100;
            -moz-border-radius:20px;
            -webkit-border-radius:20px;
            border-radius:20px;
            display:none;
        }
        #kutuMobile{
            position:relative;
            background-size: cover;
            z-index:100;
            -moz-border-radius:20px;
            -webkit-border-radius:20px;
            border-radius:20px;
            display:none;
            box-shadow: 2px 2px 50px #000;
        }

        .kutu p{
            font-size:16px;
            margin:5px 5px 5px;
            color:#000;
            text-align:center;
            position:absolute;
        }
        .kapla {
            width: 100%;
            height: 100%;
            background: #999;
            position: absolute;
            z-index: 98;
            display: block;
            opacity:0.8;
        }
        .kapat {
            float:right;
            position:absolute;
            right:0px;
            margin-top:-35px;
            cursor:pointer;
        }


        .Innovators .overlay {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background: rgba(0,204,51,0);
            background: -moz-linear-gradient(top, rgba(0,204,51,0) 0%, rgba(0,204,51,0.7) 65%, rgba(0,204,51,0.9) 100%);
            background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(0,204,51,0)), color-stop(65%, rgba(0,204,51,0.7)), color-stop(100%, rgba(0,204,51,0.9)));
            background: -webkit-linear-gradient(top, rgba(0,204,51,0) 0%, rgba(0,204,51,0.7) 65%, rgba(0,204,51,0.9) 100%);
            background: -o-linear-gradient(top, rgba(0,204,51,0) 0%, rgba(0,204,51,0.7) 65%, rgba(0,204,51,0.9) 100%);
            background: -ms-linear-gradient(top, rgba(0,204,51,0) 0%, rgba(0,204,51,0.7) 65%, rgba(0,204,51,0.9) 100%);
            background: linear-gradient(to bottom, rgba(0,204,51,0) 0%, rgba(0,204,51,0.7) 65%, rgba(0,204,51,0.9) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00cc33', endColorstr='#00cc33', GradientType=0 );
        }

        .Partner .overlay {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background: rgba(250,201,3,0);
            background: -moz-linear-gradient(top, rgba(250,201,3,0) 0%, rgba(250,201,3,0.1) 21%, rgba(250,201,3,0.7) 50%, rgba(250,201,3,0.72) 88%, rgba(250,201,3,0.9) 100%);
            background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(250,201,3,0)), color-stop(21%, rgba(250,201,3,0.1)), color-stop(50%, rgba(250,201,3,0.7)), color-stop(88%, rgba(250,201,3,0.72)), color-stop(100%, rgba(250,201,3,0.9)));
            background: -webkit-linear-gradient(top, rgba(250,201,3,0) 0%, rgba(250,201,3,0.1) 21%, rgba(250,201,3,0.7) 50%, rgba(250,201,3,0.72) 88%, rgba(250,201,3,0.9) 100%);
            background: -o-linear-gradient(top, rgba(250,201,3,0) 0%, rgba(250,201,3,0.1) 21%, rgba(250,201,3,0.7) 50%, rgba(250,201,3,0.72) 88%, rgba(250,201,3,0.9) 100%);
            background: -ms-linear-gradient(top, rgba(250,201,3,0) 0%, rgba(250,201,3,0.1) 21%, rgba(250,201,3,0.7) 50%, rgba(250,201,3,0.72) 88%, rgba(250,201,3,0.9) 100%);
            background: linear-gradient(to bottom, rgba(250,201,3,0) 0%, rgba(250,201,3,0.1) 21%, rgba(250,201,3,0.7) 50%, rgba(250,201,3,0.72) 88%, rgba(250,201,3,0.9) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffff20', endColorstr='#ffff20', GradientType=0 );
        }

        .FutureMakers .overlay {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background: rgba(254,104,18,0);
            background: -moz-linear-gradient(top, rgba(254,104,18,0) 0%, rgba(254,104,18,0.7) 65%, rgba(254,104,18,0.9) 100%);
            background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(254,104,18,0)), color-stop(65%, rgba(254,104,18,0.7)), color-stop(100%, rgba(254,104,18,0.9)));
            background: -webkit-linear-gradient(top, rgba(254,104,18,0) 0%, rgba(254,104,18,0.7) 65%, rgba(254,104,18,0.9) 100%);
            background: -o-linear-gradient(top, rgba(254,104,18,0) 0%, rgba(254,104,18,0.7) 65%, rgba(254,104,18,0.9) 100%);
            background: -ms-linear-gradient(top, rgba(254,104,18,0) 0%, rgba(254,104,18,0.7) 65%, rgba(254,104,18,0.9) 100%);
            background: linear-gradient(to bottom, rgba(254,104,18,0) 0%, rgba(254,104,18,0.7) 65%, rgba(254,104,18,0.9) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fe6812', endColorstr='#fe6812', GradientType=0 );
        }
        .Industrial .overlay {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background: rgba(141,41,141,0);
            background: -moz-linear-gradient(top, rgba(141,41,141,0) 0%, rgba(141,41,141,0.7) 65%, rgba(141,41,141,0.9) 100%);
            background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(141,41,141,0)), color-stop(65%, rgba(141,41,141,0.7)), color-stop(100%, rgba(141,41,141,0.9)));
            background: -webkit-linear-gradient(top, rgba(141,41,141,0) 0%, rgba(141,41,141,0.7) 65%, rgba(141,41,141,0.9) 100%);
            background: -o-linear-gradient(top, rgba(141,41,141,0) 0%, rgba(141,41,141,0.7) 65%, rgba(141,41,141,0.9) 100%);
            background: -ms-linear-gradient(top, rgba(141,41,141,0) 0%, rgba(141,41,141,0.7) 65%, rgba(141,41,141,0.9) 100%);
            background: linear-gradient(to bottom, rgba(141,41,141,0) 0%, rgba(141,41,141,0.7) 65%, rgba(141,41,141,0.9) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#8d298d', endColorstr='#8d298d', GradientType=0 );
        }
        .SmartCity .overlay {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background: rgba(141,41,141,0);
            background: -moz-linear-gradient(top, rgba(141,41,141,0) 0%, rgba(255, 20, 147, 0.7) 65%, rgba(255, 20, 147, 0.9) 100%);
            background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(141,41,141,0)), color-stop(65%, rgba(255, 20, 147, 0.7)), color-stop(100%, rgba(255, 20, 147, 0.9)));
            background: -webkit-linear-gradient(top, rgba(141,41,141,0) 0%, rgb(170 104 167) 65%, rgba(255, 20, 147, 0.9) 100%);
            background: -o-linear-gradient(top, rgba(141,41,141,0) 0%, rgba(255, 20, 147, 0.7) 65%, rgba(255, 20, 147, 0.9) 100%);
            background: -ms-linear-gradient(top, rgba(141,41,141,0) 0%, rgba(255, 20, 147, 0.7) 65%, rgba(255, 20, 147, 0.9) 100%);
            background: linear-gradient(to bottom, rgba(141,41,141,0) 0%, rgba(255, 20, 147, 0.7) 65%, rgba(255, 20, 147, 0.9) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#8d298d', endColorstr='#8d298d', GradientType=0 );
        }
        .Apply .overlay {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background: rgba(254,104,18,0);
            background: -moz-linear-gradient(top, rgba(254,104,18,0) 0%, rgba(115, 147, 179, 0.7) 65%, rgba(115, 147, 179, 0.9) 100%);
            background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(254,104,18,0)), color-stop(65%, rgba(115, 147, 179, 0.7)), color-stop(100%, rgba(115, 147, 179, 0.9)));
            background: -webkit-linear-gradient(top, rgba(254,104,18,0) 0%, rgba(115, 147, 179, 0.7) 65%, rgba(115, 147, 179, 0.9) 100%);
            background: -o-linear-gradient(top, rgba(254,104,18,0) 0%, rgba(115, 147, 179, 0.7) 65%, rgba(115, 147, 179, 0.9) 100%);
            background: -ms-linear-gradient(top, rgba(254,104,18,0) 0%, rgba(115, 147, 179, 0.7) 65%, rgba(115, 147, 179, 0.9) 100%);
            background: linear-gradient(to bottom, rgba(254,104,18,0) 0%, rgba(115, 147, 179, 0.7) 65%, rgba(115, 147, 179, 0.9) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fe6812', endColorstr='#fe6812', GradientType=0 );
        }
        .popupMobile {
            position: absolute;
            z-index: 1;
            top: 65%;
            left: 49%;

            width: 200px;
            height: 70px;

            font-size: 25px;
            text-transform: uppercase;
            font-weight: 700;
            color: #fff;

            background: blue;
            border: none;
            border-radius: 5px;

            transform: translate(-50%, -50%);
            transition: 0.3s linear all;
        }


        .popup {
            position: absolute;
            z-index: 1;
            top: 30%;
            left: 25%;

            width: 300px;
            height: 50vh;

            font-size: 25px;
            text-transform: uppercase;
            font-weight: 700;
            color: #fff;

            background: blue;
            border: none;
            border-radius: 5px;

            transform: translate(-50%, -50%);
            transition: 0.3s linear all;
        }

        .popup:hover,
        .popup:focus {
            background: blue;
            box-shadow:   0 0 15px 5px blue;
            color: grey;
        }
        #homeDesktop{
            /*filter: brightness(0.4);*/
            /*filter: opacity(50%);*/
        }
        .testimonials{
            padding-bottom: 2.2em;
            text-align: center;
            color: #666;
        }
        .testimonials h1{
            color: #222;
            font-size: 1.3em;
        }
        .testimonials .test-body{padding: 1em;}
        .testimonials .item{
            text-align: center;
            padding: 1em 0;
        }
        .testimonials img{
            width: 4.8em;
            height: 4.8em;
            border-radius: 50%;
        }
        .testimonials .name{color: blue;}
        .testimonials .desig{
            font-size: 0.7em;
            padding: 0.5em 0;
            color: #777;
        }
        .testimonials .share{
            margin: 0 auto;
            width: 5em;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        .testimonials i{
            color: #bfbfbf;
            padding: 0.5em 0;
            -webkit-transition: all 0.5s;
            -o-transition: all 0.5s;
            transition: all 0.5s;
            cursor: pointer;
        }
        .testimonials i:hover{color: blue;}
        .testimonials p{
            max-width: 47em;
            margin: 0 auto;
            font-size: 0.8em;
        }
        .testimonials button{
            font-size: 1em;
            background-color: blue;
            color: white;
            padding: 0.8em 2em;
            border-radius: 2em;
            border: 0;
            cursor: pointer;
            -webkit-transition: all 0.2s;
            -o-transition: all 0.2s;
            transition: all 0.2s;
        }
        .testimonials button:hover{
            background-color: #444;
        }

        @media (min-width: 38.4rem){
            .testimonials{
                font-size: 1.2em;
            }
            .testimonials .test-body{
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
            }
            .testimonials p{
                text-align: left;
                padding: 1em;
            }
        }

        .col-fourth {
            width: 25.5%;
        }

        .col-fourth {
            position: relative;
            display: inline-block;
            float: left;

            margin-bottom: 20px;
        }

        .wrapper {
            position: relative;
        }

        .wrapper {
            position: relative;
        }

        .counter {
            background-color: #fff;
            padding: 20px 0;
            border-radius: 25px;
        }

        .count {
            font-size: 20px;
            font-weight: bold;
            margin-top: 10px;
            margin-bottom: 0;
            text-align: center;


        }

        .fa-2x {
            margin: 0 auto;
            float: none;
            display: table;
            color: white;
        }
        h2{
            text-align:center;
            padding: 20px;
        }
        /* Slider */

        .slick-slide {
            margin: 0px 20px;
        }

        .slick-slide img {
            width: 100%;
        }

        .slick-slider
        {
            position: relative;
            display: block;
            box-sizing: border-box;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-touch-callout: none;
            -khtml-user-select: none;
            -ms-touch-action: pan-y;
            touch-action: pan-y;
            -webkit-tap-highlight-color: transparent;
        }

        .slick-list
        {
            position: relative;
            display: block;
            overflow: hidden;
            margin: 0;
            padding: 0;
        }
        .slick-list:focus
        {
            outline: none;
        }
        .slick-list.dragging
        {
            cursor: pointer;
            cursor: hand;
        }

        .slick-slider .slick-track,
        .slick-slider .slick-list
        {
            -webkit-transform: translate3d(0, 0, 0);
            -moz-transform: translate3d(0, 0, 0);
            -ms-transform: translate3d(0, 0, 0);
            -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

        .slick-track
        {
            position: relative;
            top: 0;
            left: 0;
            display: block;
        }
        .slick-track:before,
        .slick-track:after
        {
            display: table;
            content: '';
        }
        .slick-track:after
        {
            clear: both;
        }
        .slick-loading .slick-track
        {
            visibility: hidden;
        }

        .slick-slide
        {
            display: none;
            float: left;
            height: 100%;
            min-height: 1px;
        }
        [dir='rtl'] .slick-slide
        {
            float: right;
        }
        .slick-slide img
        {
            display: block;
        }
        .slick-slide.slick-loading img
        {
            display: none;
        }
        .slick-slide.dragging img
        {
            pointer-events: none;
        }
        .slick-initialized .slick-slide
        {
            display: block;
        }
        .slick-loading .slick-slide
        {
            visibility: hidden;
        }
        .slick-vertical .slick-slide
        {
            display: block;
            height: auto;
            border: 1px solid transparent;
        }
        .slick-arrow.slick-hidden {
            display: none;
        }

    </style>
    <script type="text/javascript">window.$crisp = [];
        window.CRISP_WEBSITE_ID = "c48071d0-4e08-4bb1-b8d5-de99a9162e2a";
        (function () {
            d = document;
            s = d.createElement("script");
            s.src = "https://client.crisp.chat/l.js";
            s.async = 1;
            d.getElementsByTagName("head")[0].appendChild(s);
        })();
    </script>

    <script>
        $(document).ready(function(){
            $('#submit-newsletter').on('click', function(){

                let formData = new FormData();

                formData.append('email', $('#email').val());
                formData.append('fullname', $('#fullname').val());

                $.ajax({
                    url: '{{route('store-newsletter')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#submit-newsletter').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            $('#submit-newsletter-mobile').on('click', function(){

                let formData = new FormData();

                formData.append('email', $('#email-mobile').val());
                formData.append('fullname', $('#fullname-mobile').val());

                $.ajax({
                    url: '{{route('store-newsletter')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#submit-newsletter-mobile').notify(response.message, "success");
                        setTimeout(function(){
                            window.location.reload();
                        }, 3000);
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            $('.kutu').fadeIn(1500);

            $('.kapla').click(function() {
                $('.kutu').fadeOut(1000);
                $('.kapla').delay(1000).slideUp(1500);
            })

            $('.kapat').click(function() {
                $('.kutu').fadeOut(1000);
                $('.kapla').delay(1000).slideUp(1500);
            })

            $(window).resize(function(){

                $('.kutu').css({
                    position:'absolute',
                    left: ($(window).width() - $('.kutu').outerWidth())/2,
                    top: ($(window).height() - $('.kutu').outerHeight())/2
                });

            });

            $(window).resize();


        });

            $(document).ready(function(){
                $('.customer-logos').slick({
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 1500,
                    arrows: false,
                    dots: false,
                    pauseOnHover: false,
                    responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 4
                        }
                    }, {
                        breakpoint: 520,
                        settings: {
                            slidesToShow: 3
                        }
                    }]
                });
            });
            function viewMedia(obj) {
                let event_id = obj.getAttribute('data-value');

                location.href = '/home/event-show-gallery/' + event_id;
            }

            function saveEventDetails(obj) {
                let event_id = obj.getAttribute('data-value');
                let event_type = obj.getAttribute('data-type');
                sessionStorage.setItem('stored_event_id', event_id);
                sessionStorage.setItem('stored_event_type', event_type);
            }

            function saveEventDetailsMobile(obj) {
                let event_id = obj.getAttribute('data-value');
                let event_type = obj.getAttribute('data-type');
                sessionStorage.setItem('stored_event_id', event_id);
                sessionStorage.setItem('stored_event_type', event_type);
            }

            function saveFoundOutDetails() {
                if ($('#found-us-via-input').val() === "") {
                    alert('Please choose an option.');
                } else {
                    let found_out_via;
                    let event_type = sessionStorage.getItem('stored_event_type');

                    if ($('#found-us-via-input').val() === 'Other') {
                        found_out_via = $('#found-us-via-other-input').val();
                    } else {
                        found_out_via = $('#found-us-via-input').val();
                    }

                    sessionStorage.setItem('stored_found_out_via', found_out_via);

                    if (event_type === 'Public') {
                        window.location.href = '/visitors/visitor-login';
                    } else if (event_type === 'Private') {
                        window.location.href = '/login';
                    } else if (event_type === 'View Only') {
                        alert('Sorry, this event is for viewing purposes only.');
                    }

                }
            }

            function showModal(obj) {
                $('.modal').modal({
                    onOpenStart: function () {
                        let image_source = obj.getAttribute('data-value');
                        $('#modal-image-div').html(
                            '<div class="row" style="height: 100%; width: 100%">'
                            +
                            '<img id="event-detail-modal-image" src="/storage/' + image_source + '" alt="No Image Uploaded" style="height: 100%; width: 100%;">'
                            +
                            '</div>'
                        );
                    }
                });
            }

            function showModalMobile(obj) {
                $('.modal').modal({
                    onOpenStart: function () {
                        let image_source = obj.getAttribute('data-value');
                        $('#modal-image-div').html(
                            '<div class="row" style="height: 100%; width: 100%">'
                            +
                            '<img id="event-detail-modal-image" src="/storage/' + image_source + '" alt="No Image Uploaded" style="height: 100%; width: 100%;">'
                            +
                            '</div>'
                        );
                    }
                });
            }

            $(document).ready(function () {
                $('.modal').modal();
                $('#found-us-via-input').change(function () {
                    if ($('#found-us-via-input').val() === 'Other') {
                        $('#found-us-via-other-input').show();
                    } else {
                        $('#found-us-via-other-input').hide();
                    }
                });

                $('.private-event-type-header').hover(function () {
                    $(this).html(
                        '<p>' + 'So sorry, this is an internal event.' + '</p>'
                    )
                });

                $("#testimonial-slider").owlCarousel({
                    items: 2,
                    itemsDesktop: [5200, 2],
                    itemsDesktopSmall: [990, 2],
                    itemsTablet: [768, 2],
                    itemsMobile: [650, 1],
                    pagination: false,
                    navigation: false,
                    autoPlay: false
                });
                $("#testimonial-slide-mobile").owlCarousel({
                    items: 2,
                    itemsDesktop: [1000, 2],
                    itemsDesktopSmall: [990, 2],
                    itemsTablet: [768, 2],
                    itemsMobile: [650, 1],
                    pagination: false,
                    navigation: false,
                    autoPlay: false
                });
            });
            //Card onclick
            $('.latest_info').each(function () {
                let latest_info_id = $(this).find('.latest_info_id').attr('data-value');

                $(this).on('click', function () {
                    location.href = '/latest-info/' + latest_info_id;
                });
            });


            $('.date').each(function () {
                var max_length = 17;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<i href="#" class="more_horiz material-icons small"></i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });


            $('.top-description').each(function () {
                var max_length = 30;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content);

                }

            });


            $('#events').on('click', function () {
                location.href = '/show-events';
            });
            $('.count').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 40000,
                    ease: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now))
                    }
                })
            })


            $('.popup').click(function () {
                window.location = '/Application-Process/question-category';
            });
            $(document).ready(function () {

                $('.carousel').carousel({
                    interval: 200
                });

                $('.popup-closer').click(function () {
                    $('.kutu').hide();
                    $('.kapla').hide();
                });
                $('.kutu').fadeIn(1500);

                $('.kapla').click(function () {
                    $('.kutu').fadeOut(1000);
                    $('.kapla').delay(1000).slideUp(1500);
                })

                $('.kapat').click(function () {
                    $('.kutu').fadeOut(1000);
                    $('.kapla').delay(1000).slideUp(1500);
                })

                $(window).resize(function () {

                    $('.kutu').css({
                        position: 'absolute',
                        left: ($(window).width() - $('.kutu').outerWidth()) / 2,
                        top: ($(window).height() - $('.kutu').outerHeight()) / 2
                    });

                });

                $(window).resize();


                $('.ventana button').click(function () {
                    $('.ventana').fadeOut('slow', function () {
                        this.remove();
                    });
                });

                $("#testimonial-slider").owlCarousel({
                    items: 2,
                    itemsDesktop: [1000, 2],
                    itemsDesktopSmall: [979, 2],
                    itemsTablet: [768, 1],
                    pagination: true,
                    navigation: false,
                    autoplay: true
                });

                $('.Innovators').on('click', function () {
                    location.href = "/Home/Innovators"
                });
                $('.Partner').on('click', function () {
                    location.href = "/Home/Partners"
                });
                $('.FutureMakers').on('click', function () {
                    location.href = "/Home/Incubatees"
                });
                $('.SmartCity').on('click', function () {
                    location.href = "/pti-ventures"
                });
                $('.Industrial').on('click', function () {
                    location.href = "/Home/INDVentures"
                });
                $('.Apply').on('click', function () {
                    location.href = "/Application-Process/question-category"
                });
            });
        </script>

@endsection



