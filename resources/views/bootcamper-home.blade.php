@extends('layouts.bootcamper-layout')

@section('content')
    <br>
    <br>
    <link rel="stylesheet" type="text/css" href="/css/BootcamperDashboard/BootcamperDashboard.css">

    <div id="bootcamperDashboardDesktop"><div class="row" style="margin-left: 7em; margin-right: 4em">
            <br/>
            <h4 style="margin-left: 35%">WELCOME <b>{{$user->name}} {{$user->surname}}</b></h4>
            <input hidden disabled id="bootcamper-id-input" value="{{$bootcamper->id}}">
            <input hidden disabled id="user-id-input" value="{{$user->id}}">
            <br/>
            <br/>
            <div class="row center">
                <div class="col l12 card" style="background: #6c757d">
                    <h4 style="color: white" id="notification-header"><b>EVENTS YOU NEED TO ACCEPT</b></h4>
                </div>
                @if(count($bootcamper_event_array) > 0)
                    @foreach($bootcamper_event_array as $b_event)
                        @if(isset($b_event))
                            @if($b_event->accepted == false)
                                @if(isset($b_event->declined))
                                    <div class="col l3 m6 s12" hidden style="width: 400px;">
                                        <div class="card  hoverable" style="border-radius: 10px;background-color:#454242;">
                                            <div class="card-content white-text center">
                                                <h5><b>{{$b_event->title}}</b></h5>
                                                <p>Date: {{$b_event->start}}</p>
                                            </div>
                                            <div class="card-action center" style="border-radius: 10px;">
                                                <a class="bootcamper-accept-event-button btn" data-value="{{$b_event->b_event_id}}"
                                                   style="color: white; cursor: pointer;background-color:#454242;"><b>Accept</b></a>
                                                <a class="bootcamper-decline-event-button btn" data-value="{{$b_event->b_event_id}}"
                                                   style="color: white; cursor: pointer;background-color:#454242;"><b>Decline</b></a>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="col l3 m6 s12" style="width: 400px;">
                                        <div class="card  hoverable" style="border-radius: 10px;background-color:#454242;">
                                            <div class="card-content white-text center">
                                                <h5><b>{{$b_event->title}}</b></h5>
                                                <p>Date: {{$b_event->start}}</p>
                                            </div>
                                            <div class="card-action center" style="border-radius: 10px;">
                                                <a class="bootcamper-accept-event-button btn" data-value="{{$b_event->b_event_id}}"
                                                   style="color: white; cursor: pointer;background-color:#454242;"><b>Accept</b></a>
                                                <a class="bootcamper-decline-event-button btn" data-value="{{$b_event->b_event_id}}"
                                                   style="color: white; cursor: pointer;background-color:#454242;"><b>Decline</b></a>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                            @endif
                        @endif

                    @endforeach
                @endif
            </div>

            <div class="row center">
                <div class="col l12 card" style="background: #6c757d">
                    <h4 style="color: white"><b>EVENTS YOU HAVE ACCEPTED.</b></h4>
                </div>

                @if(count($bootcamper_event_array) > 0)
                    @foreach($bootcamper_event_array as $b_event)
                        @if(isset($b_event))
                            @if($b_event->accepted == true)
                                <div class="col l3 m6 s12" style="width: 400px;">
                                    <div class="card  hoverable" style="border-radius: 10px;background-color:#454242;">
                                        <div class="card-content white-text center">
                                            <h5><b>{{$b_event->title}}</b></h5>
                                            <p>Date: {{$b_event->start}}</p>
                                        </div>
                                            @if($user->data_cellnumber)
                                            <div class="card-action center" style="border-radius: 10px;">
                                                <a class="waves-effect waves-light btn modal-trigger data-details-button" disabled id="{{$b_event->id}}" href="#modal1">SUBMIT DATA DETAILS</a>
                                            </div>
                                        @else
                                            <div class="card-action center" style="border-radius: 10px;">
                                                <a class="waves-effect waves-light btn modal-trigger data-details-button" id="{{$b_event->id}}" href="#modal1">SUBMIT DATA DETAILS</a>
                                            </div>
                                        @endif
                                        <br>
                                    </div>
                                </div>
                            @endif
                        @endif
                    @endforeach
                @endif
            </div>



            <!-- Modal Structure -->
            <div id="modal4" class="modal">
                @foreach($category as $categories)
                    <h4 class="center-align"><b>{{$categories->category_name}}</b></h4>
                @endforeach
                @if($bootcamper->evaluation_form_completed == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                        <ol>
                            <div class="modal-content">
                                <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                                @foreach($category as $cur_category)
                                    @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                        <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                            <div class="row" style="border-style: ridge">
                                                <div class="" style="margin-left: 4em">
                                                    @if($cur_question->question_type == "text")
                                                        <div class="input-field">
                                                            <input id="{{$cur_question->id}}" required type="text" class=" answer-text-desktop" style="width:50%">
                                                        </div>
                                                    @else
                                                        <div class="input-field">
                                                            <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-desktop" style="width:30%">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="upload-answers-form-desktop" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>

            <!--beginningOfBootcamp-->
            <div id="modal5" class="modal">
                @foreach($beginning_of_bootcamper as $beginningOfBootcamp)
                    <h4 class="center-align"><b>{{$beginningOfBootcamp->category_name}}</b></h4>
                @endforeach
                @if($bootcamper->beginning_evaluation_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($beginning_of_bootcamper as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="beginning-of-bootcamp-upload-answers-form-desktop" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>

            <!--postOfBootcamp-->
            <div id="modal6" class="modal">
                @foreach($post_evaluation as $post_evaluations)
                    <h4 class="center-align"><b>{{$post_evaluations->category_name}}</b></h4>
                @endforeach
                @if($bootcamper->post_evaluation_complete == true)
                    <div class="row" style="margin-left: 4em;margin-right: 4em">
                        <p class="center-align">Form already submmitted</p>
                    </div>
                @else
                    <ol>
                        <div class="modal-content">
                            <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                            @foreach($post_evaluation as $cur_category)
                                @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                    <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                    <div class="row" style="border-style: ridge">
                                        <div class="" style="margin-left: 4em">
                                            @if($cur_question->question_type == "text")
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="text" class=" answer-text-desktop" style="width:50%">
                                                </div>
                                            @else
                                                <div class="input-field">
                                                    <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-desktop" style="width:30%">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </ol>
                    <div class="col s1 answers-submit" hidden></div>
                    <div class="row center">
                        <button class="btn waves-effect waves-light" id="post-of-bootcamp-upload-answers-form-desktop" name="action"
                                style="margin-right: auto;">Save
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                @endif
            </div>


            <!-- Modal Structure -->
            <div id="modal1" class="modal">
                <div class="modal-content">
                    <div class="row" style="margin-left: 100px">
                        <h4 class="center-align" ><b>DATA DETAILS</b></h4>
                        <div class="input-field col m5">
                            <input id="data_cellnumber" type="number" required maxlength="10" class="validate">
                            <label for="data_cellnumber">Cell number</label>
                        </div>
                        <div class="input-field col m5">
                            <select id="service_provider_network" class="required">
                                <option value="" disabled selected>Choose your option</option>
                                <option value="MTN">MTN</option>
                                <option value="CELL C">CELL C</option>
                                <option value="TELKOM">TELKOM</option>
                                <option value="VODACOM">VODACOM</option>
                                <option value="No data required">No data required</option>
                            </select>
                            <label>Choose your Network</label>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 400px;">
                        <div class="col s4">
                            <a class="waves-effect waves-light btn section" id="data-upload-submit-button">Save</a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row center">
                <div class="col l12">
                    <h4 style="color: black"><b>BOOTCAMPER UPLOADS (click to upload)</b></h4>
                </div>

                <!--Tabs-->
                <div class="row">
                    @if($bootcamper->pitch_video_link == null)
                        <div class="card tab1 col s4" style="background-color: red;font-weight: bold;width: 430px">
                            <p class="center txt" style="color: white;cursor:pointer">Video Pitch: Not uploaded.</p>
                        </div>
                    @else
                        <div class="card tab1 col s4" style="background-color: rgba(0, 121, 52, 1);font-weight: bold;width: 430px">
                            <p class="center txt" style="color: white;cursor:pointer">Video Pitch: Uploaded.</p>
                        </div>
                    @endif
                        <div class="col s0.1"></div>
                        @if($bootcamper->pitch_video_link_two == null)
                            <div class="card tab2 col s4" style="background-color: red;font-weight: bold;width: 430px">
                                <p  class="center txt" style="color: white;cursor:pointer">Video Pitch 2: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card tab2 col s4" style="background-color: rgba(0, 121, 52, 1);font-weight: bold;width: 430px">
                                <p  class="center txt" style="color: white;cursor:pointer">Video Pitch 2: Uploaded.</p>
                            </div>
                        @endif
                        <div class="col s0.1"></div>
                        @if($bootcamper->id_document_url == null)
                            <div class="card tab3 col s4" style="background-color: red;font-weight: bold;width: 430px">
                                <p class="center txt" style="color: white;cursor:pointer">ID Document: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card tab3 col s4" style="background-color: rgba(0, 121, 52, 1);font-weight: bold;width: 430px">
                                <p class="center txt" style="color: white;cursor:pointer">ID Document: Uploaded.</p>
                            </div>
                        @endif
                </div>
                <div class="row">
                    @if($bootcamper->proof_of_address == null)
                        <div class="card tab4 col s4" style="background-color: red;font-weight: bold;width: 430px">
                            <p class="center txt" style="color: white;cursor:pointer">Proof of address: Not uploaded.</p>
                        </div>
                    @else
                        <div class="card col tab4 s4" style="background-color: rgba(0, 121, 52, 1);font-weight: bold;width: 430px">
                            <p class="center txt" style="color: white;cursor:pointer">Proof of address: Uploaded.</p>
                        </div>
                    @endif
                        <div class="col s0.1"></div>
                        @if($bootcamper->cipc == null)
                            <div class="card tab5 col s4" style="background-color: red;font-weight: bold;width: 430px">
                                <p class="center txt" style="color: white;cursor:pointer">CIPC: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card col tab5 s4" style="background-color: rgba(0, 121, 52, 1);font-weight: bold;width: 430px">
                                <p class="center txt" style="color: white;cursor:pointer">CIPC: Uploaded.</p>
                            </div>
                        @endif
                        <div class="col s0.1"></div>
                        <div class="card tab6 col s4" style="background-color: rgba(0, 121, 52, 1);font-weight: bold;width: 430px">
                            <p class="center txt" style="color: white;cursor:pointer">VIEW APPLICATION FORM</p>
                        </div>
                </div>
                <div class="row">
                    @if($bootcamper->bootcamp_nda_agreement == false)
                        <div class="card tab8 col s4" style="background-color: red;font-weight: bold;width: 430px">
                            <p class="center txt" style="color: white;cursor:pointer">NDA: Not accepted.</p>
                        </div>
                    @else
                        <div class="card col tab8 s4" style="background-color: rgba(0, 121, 52, 1);font-weight: bold;width: 430px">
                            <p class="center txt" style="color: white;cursor:pointer">NDA: Accepted.</p>
                        </div>
                    @endif
                        <div class="col s0.1"></div>
                        @if($bootcamper->beginning_evaluation_complete == false and $bootcamper->post_evaluation_complete == false)
                            <div class="card tab7 col s4" style="background-color: red;font-weight: bold;width: 430px">
                                <p class="center txt" style="color: white;cursor:pointer">EVALUATION NOT COMPLETED.</p>
                            </div>
                        @else
                            <div class="card col tab7 s4" style="background-color: rgba(0, 121, 52, 1);font-weight: bold;width: 430px">
                                <p class="center txt" style="color: white;cursor:pointer">EVALUATION COMPLETED.</p>
                            </div>
                        @endif
                        <div class="col s0.1"></div>
                        @if($bootcamper->contract_uploaded == false)
                            <div class="card tab9 col s4" style="background-color: red;font-weight: bold;width: 430px">
                                <p class="center txt" style="color: white;cursor:pointer">Contract: Not uploaded.</p>
                            </div>
                        @else
                            <div class="card col tab9 s4" style="background-color: rgba(0, 121, 52, 1);font-weight: bold;width: 430px">
                                <p class="center txt" style="color: white;cursor:pointer">Contract: Uploaded.</p>
                            </div>
                        @endif
                </div>

                {{--UPLOAD AND EDIT CONTRACT--}}
                <div class="col l6 m6 s12 " style="margin-left: 400px" hidden id="contract-upload">
                    <h5><b>CONTRACT:</b></h5>
                    @if(isset($bootcamper->bootcamper_contract_upload))
                        <input type="file" id="bootcamper-contract-input-display" value="{{$bootcamper->bootcamper_contract_upload}}">
                        <p style="color: red;">Contract uploaded.</p>
                        <button class="btn blue" id="update-contract-upload">Update</button>
                        <a href="{{'/storage/'.$bootcamper->bootcamper_contract_upload}}" target="_blank">View Contact</a>
                        <br>
                    @else
                        <input class="file-path validate" id="bootcamper-contract-input"
                               type="file" placeholder="Contract">
                        <button class="btn blue" id="submit-contract-upload">Submit</button>
                        <br>
                    @endif
                </div>

                {{--Agreement modal--}}
                <div class="col l6 m6 s12 card" style="margin-left: 400px" hidden id="desktop-nda">
                        <img style="width: 30vh" src="/images/Prop agrre.png">
                        <div class="col s12">
                            <h5 class="center-align"><b>Open Communications FutureMakers Multi-Party Nondisclosure Agreement</b></h5>
                        </div>
                        <p>This multi-party nondisclosure agreement ("Agreement") is entered into and made effective as of the date set forth above among the parties listed at the end of this Agreement and any third party that executes this Agreement after the Effective Date. This Agreement is intended to cover the parties’ confidentiality obligations with respect to disclosures regarding or pertinent to the exploration ways the parties can work together for mutual benefit.</p>
                        <p><b>The Parties agree as follows:</b></p>
                        <p>1.	Confidential Information. The confidential, proprietary and trade secret information of each party ("Confidential Information") to be disclosed hereunder is that tangible or intangible information which is marked with a "Confidential," "Proprietary", or similar legend and which is clearly identified in writing as provided under this Agreement and includes any information disclosed during discussion sessions. To be considered Confidential Information, non-tangible disclosures must be identified as confidential under the terms and conditions of this Agreement prior to disclosure and reduced to writing, marked as provided above, and delivered to the receiving party within thirty (30) calendar days of the original date of disclosure. Any information received from any other participant in Bootcamp 1 will not be used by any other party.
                            Ideas or discussions not forming part of the Disclosing Parties normal course of business and ideas generated during discussions will not be regarded as confidential information.
                        </p>
                        <p>2.	Obligations of receiving party. Each receiving party will maintain the confidentiality of the Confidential Information for each disclosing party with at least the same degree of care that it uses to protect its own confidential and proprietary information, but no less than a reasonable degree of care under the circumstances. No receiving party will disclose any of a disclosing party's confidential information to any employees or to any third parties except to employees, contractors, and Affiliates (as below defined) of such receiving party who have a need to know and who agree to abide by nondisclosure terms at least as
                            comprehensive as those set forth herein; provided such receiving party will be liable for breach of any such entity. The receiving party will not make any copies of the Confidential Information received from a disclosing party except as necessary for their employees and contractors and those employees and contractors of the receiving party's Affiliates with a need to know. Any copies which are made will include the legend and identification statement as supplied by the disclosing party.  The term “Affiliate” of a party, as used herein, shall mean any company, corporation or other business entity which directly or indirectly (i) controls said party or (ii) is controlled by said party or (iii) is controlled by any entity under (i) above, where “to control” an entity shall mean to own more than fifty per cent (50%) of the shares of, or of the ownership interest in, such entity.
                        </p>
                        <p>3.    Period of confidentiality. The receiving party’s obligation of confidentiality shall be for a period of two (2) years from the date of receipt of the disclosing party’s Confidential Information</p>
                        <p>4.    Termination of obligation of confidentiality. No receiving party will be liable for the disclosure of a disclosing party's confidential information which is:</p>
                        <p>a)	Published with the consent of the disclosing party or is otherwise rightfully in the public domain other than by a breach of a duty of confidentiality to the disclosing party;</p>
                        <p>b)   Rightfully received from a third party without any obligation of confidentiality;</p>
                        <p>c)	Rightfully known to the receiving party without any limitation on use or disclosure prior to its receipt from the disclosing party</p>
                        <p>d)   Independently developed by employees of the receiving party; or</p>
                        <p>e)    Generally made available to third parties by the disclosing party without restriction on disclosure.</p>
                        <p>5.    Title. Title or the right to possess Confidential Information as between receiving parties and the disclosing party will remain in the disclosing party.</p>
                        <p>6.    No obligation of disclosure; Termination. No party has an obligation to disclose Confidential Information to any other
                            party.  Any party may withdraw from this Agreement at any time and without cause upon written notice to the remaining
                            parties, provided that such withdrawing party's obligations with respect to Confidential Information of other parties
                            disclosed during the term of this Agreement will survive any such termination until the end of the period set forth in
                            Section 3. A disclosing party may at any time: (a) cease giving Confidential Information to the other party without any
                            liability, and/or (b) request in writing the return or destruction of all or part of its Confidential Information previously
                            disclosed, and all copies thereof, and each other party will comply with such request, and certify in writing its compliance
                            within sixty (60) calendar days.
                        </p>
                        <p>7.	No warranty.  All parties acknowledge that all information provided hereunder is provided "AS IS" WITH NO WARRANTIES WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, AND THE PARTIES EXPRESSLY DISCLAIM ANY WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, FITNESS FOR ANY PARTICULAR PURPOSE, OR ANY WARRANTY OTHERWISE ARISING OUT OF ANY PROPOSAL, SPECIFICATION, OR SAMPLE.</p>
                        <p>8.    General</p>
                        <p>a)	This Agreement is neither intended to, nor will it be construed as creating a joint venture, a partnership or other form of a business association between the parties, nor an obligation to buy or sell products using or incorporating the Confidential Information.</p>
                        <p>b)   Each party understands and acknowledges that no license under any patents, copyrights, trademarks, or maskworks is granted to or conferred upon any party or by the disclosure of any Confidential Information by one party to the other party as contemplated hereunder, either expressly, by implication, inducement, estoppel or otherwise, and that any license under such intellectual property rights must be express and in writing.</p>
                        <p>c)	The failure of any party to enforce any rights resulting from breach of any provision of this Agreement will not be deemed a waiver of any right relating to a subsequent breach of such provision or of any other right hereunder.</p>
                        <p>d)   This Agreement constitutes the entire agreement between the parties with respect to the disclosures of Confidential
                            Information, and may not be amended except in a writing signed by a duly authorized representative of the respective parties. e)    Each party shall execute a copy of this Agreement, each of which shall be deemed an original and all of which together shall
                            constitute one instrument. A list shall be maintained of all parties that have executed this Agreement and shall be made available to all parties.
                        </p>
                        <p>e)    Each party shall execute a copy of this Agreement, each of which shall be deemed an original and all of which together shall
                            constitute one instrument. A list shall be maintained of all parties that have executed this Agreement and shall be made available to all parties.
                        </p>
                    <p id="thank-you">f)    This Agreement shall be subject to and governed by the law of South Africa and the place of jurisdiction shall be Port Elizabeth, South Africa.

                    </p>
                    <div class="row" style="margin-left: 2em; margin-right: 2em;">
                        <button class="btn right" id="agreement-button-desktop">Agree</button>
                    </div>
                </div>
                </div>

                <div class="col l6 m6 s12 " style="margin-left: 400px" hidden id="pitch-video-one">
                    <h5><b>UPLOAD PITCH VIDEO:</b></h5>
                    @if(isset($bootcamper->pitch_video_link))
                        <input type="text" id="bootcamper-video-link-input-display" value="{{$bootcamper->pitch_video_link}}">
                        <p>Date Uploaded: {{$bootcamper->pitch_video_date_time}}</p>
                        <button class="btn blue" id="update-pitch-video">Update</button>
                        <br>
                        <br>
                    @else
                        <input type="text" id="bootcamper-video-link-input" placeholder="Upload Pitch Video">
                        <button class="btn blue" id="submit-pitch-video">Submit</button>
                        <br>
                        <br>
                    @endif
                </div>

                <!--Second Video-->
                <div class="col l6 m6 s12" style="margin-left: 400px" hidden id="pitch-video-two">
                    <h5><b>UPLOAD SECOND PITCH VIDEO:</b></h5>
                    @if(isset($bootcamper->pitch_video_link_two))
                        <input type="text" id="bootcamper-second-video-link-input-display" value="{{$bootcamper->pitch_video_link_two}}">
                        <p>Date Uploaded: {{$bootcamper->pitch_video_date_time}}</p>
                        <button class="btn blue" id="update-second-pitch-video">Update</button>
                        <br>
                        <br>
                    @else
                        <input type="text" id="bootcamper-second-video-link-input" placeholder="Upload Second Pitch Video">
                        <button class="btn blue" id="submit-second-pitch-video">Submit</button>
                        <br>
                        <br>
                    @endif
                </div>

                <div class="col l6 m6 s12" style="margin-left: 400px" hidden id="id-document-upload">
                    <h5><b>UPLOAD ID DOCUMENT:</b></h5>
                    @if(isset($bootcamper->id_document_url))
                        <input type="file" id="bootcamper-id-document-input-display" value="{{$bootcamper->id_document_url}}">
                        <p style="color: red;">Only upload another ID document if you wish to update the one you uploaded previously.</p>
                        <button class="btn blue" id="update-id-document">Update</button>
                    <br>
                        <a href="{{'/storage/'.$bootcamper->id_document_url}}" target="_blank">View ID</a>
                        <br>
                    @else
                        <input class="file-path validate" id="bootcamper-id-document-input"
                               type="file" placeholder="Upload ID Document">
                        <button class="btn blue" id="submit-id-document">Submit</button>
                        <br>
                    @endif
                </div>

                <!--Application form-->
            <div class="col s12 card" style="border-radius: 10px" hidden id="application-form">
                    <div class="row">
                        <div class="container" id="QuestionsAndAnswers">
                            <br>
                            <div class="row center">
                                <h4><b>APPLICATION DETAILS</b></h4>
                                <div class="row">
                                    <div class="col l6 m6 s12">
                                        <h6>{{$bootcamper->user->name}} {{$bootcamper->user->surname}}</h6>
                                        <h6>Initials: {{$bootcamper->user->initials}}</h6>
                                        <h6>Email: {{$bootcamper->user->email}}</h6>
                                        <h6>Contact Number: {{$bootcamper->user->contact_number}}</h6>
                                        <h6>ID Number: {{$bootcamper->user->id_number}}</h6>
                                        <h6>Age: {{$bootcamper->user->age}}</h6>
                                        <h6>Gender: {{$bootcamper->user->gender}}</h6>
                                    </div>
                                    <div class="col l6 m6 s12">
                                        <h6>Address One: {{$bootcamper->user->address_one}}</h6>
                                        <h6>Address Two: {{$bootcamper->user->address_two}}</h6>
                                        <h6>Address Three: {{$bootcamper->user->address_three}}</h6>
                                    </div>
                                </div>
                                <br>
                                <hr style="background: darkblue; height: 5px;">
                                <br>
                            </div>

                            <div class="row">
                                @foreach($bootcamper_question_array as $question_answer)
                                    <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                                    <div class="input-field">
                                        <textarea disabled>{{$question_answer->answer_text}}</textarea>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    <div class="row " style="margin-left: 7em">
        <div class="col l6 m6 s12" style="margin-left: 400px" hidden id="p-o-a-upload">
            <h5><b>UPLOAD PROOF OF ADDRESS:</b></h5>
            @if(isset($bootcamper->proof_of_address))
                <input type="file" id="bootcamper-proof-of-address-input-display" value="{{$bootcamper->proof_of_address}}">
                <p style="color: red;">Proof of address uploaded.</p>
                <button class="btn blue" id="update-proof-of-address">Update</button>
                <a href="{{'/storage/'.$bootcamper->proof_of_address}}" target="_blank">View Proof</a>
                <br>
            @else
                <input class="file-path validate" id="bootcamper-proof-of-address-input"
                       type="file" placeholder="Proof of residance">
                <button class="btn blue" id="submit-proof-of-address">Submit</button>
                <br>
            @endif
        </div>
        <div class="col l6 m6 s12" style="margin-left: 400px" hidden id="cipc-doc">
            <h5><b>UPLOAD CIPC:</b></h5>
            @if(isset($bootcamper->cipc))
                <input type="file" id="bootcamper-cipc-input-display" value="{{$bootcamper->cipc}}">
                <p style="color: red;">CIPC uploaded.</p>
                <button class="btn blue" id="update-cipc">Update</button>
                <a href="{{'/storage/'.$bootcamper->cipc}}" target="_blank">View CICP</a>

            @else
                <input class="file-path validate" id="bootcamper-cipc-input"
                       type="file" placeholder="CICP">
                <button class="btn blue" id="submit-cipc">Submit</button>
            @endif

        </div>
        <div class="col l6 m6 s12 card" style="margin-left: 400px" hidden id="application-form">
            <h5><b>APPLICATION FORM:</b></h5>
            <a class="btn blue tooltipped modal-trigger" data-position="left" data-tooltip="View Application" href="#bootcamper_questions_modal">
                <i class="large material-icons">remove_red_eye</i>
            </a>
            <br>
        </div>
        <div class="col l6 m6 s12 " style="margin-left: 400px" hidden id="evaluation">
            <br>
            @foreach($beginning_of_bootcamper as $beginning_of_bootcampers)
                <div class="col l6 m6 s6">
                <a class="waves-effect waves-light btn modal-trigger" style="background-color:  rgba(0, 121, 52, 1)" href="#modal5">{{$beginning_of_bootcampers->category_name}}</a>
                </div>
            @endforeach
            @if($bootcamper->beginning_evaluation_complete ==false)
                @else
            @foreach($post_evaluation as $post_evaluations)
                    <div class="col l6 m6 s6 ">
                <a class="waves-effect waves-light btn  modal-trigger" style="background-color:  rgba(0, 121, 52, 1)" href="#modal6">{{$post_evaluations->category_name}}</a>
                    </div>
            @endforeach
            @endif
            <br>
            <br>
        </div>
    </div>

    <div id="bootcamper_questions_modal" class="modal">
        <div class="row" style="padding-right: 20px; padding-left: 20px;">
            @if(count($bootcamper_question_array) > 0)
                <div class="row" style="margin-left: 4em; margin-right: 4em;">
                    <ol>
                        @foreach ($bootcamper_question_array as $question)
                            <li value="{{$question->question_number}}">  {{ $question->question_text}} </li>
                            <div class="input-field">
                                <input id="{{$question->question_id}}" class="materialize-textarea application-answer-text" value="{{$question->answer_text}}"
                                       style="width:90%">
                            </div>
                        @endforeach
                    </ol>
                    <div class="modal-footer">
                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                    </div>
                </div>
            @endif
        </div>
    </div>



    <div  id="bootcamperDashboardMobile">
        <br>
        <br>
        <div class="row center" >
            <br/>
            <h4 style="alignment: center">Hello <b>{{$user->name}} {{$user->surname}}</b></h4>
            <input hidden disabled id="bootcamper-id-input" value="{{$bootcamper->id}}">
            <input hidden disabled id="user-id-input" value="{{$user->id}}">
            <br/>
            <br/>
            <div class="row center">
                <div class="col l12 card" style="background: #454242; alignment: center; width: 100%">
                    <h4 style="color: white" id="notification-header">Events you need to accept.</h4>
                    @if(count($bootcamper_event_array) > 0)
                        @foreach($bootcamper_event_array as $b_event)
                            @if(isset($b_event))
                                @if($b_event->accepted == false)
                                    @if(isset($b_event->declined))
                                        <div class="col l3 m6 s12" hidden style="width: 400px;">
                                            <div class="card  hoverable" style="border-radius: 10px;background-color:#1164a8;">
                                                <div class="card-content white-text center">
                                                    <h5><b>{{$b_event->title}}</b></h5>
                                                    <p>Date: {{$b_event->start}}</p>
                                                </div>
                                                <div class="card-action center" style="border-radius: 10px;">
                                                    <a class="bootcamper-accept-event-button-mobile btn" data-value="{{$b_event->b_event_id}}"
                                                       style="color: white; cursor: pointer;background-color:#1b6b25;"><b>Accept</b></a>
                                                    <a class="bootcamper-decline-event-button-mobile btn" data-value="{{$b_event->b_event_id}}"
                                                       style="color: white; cursor: pointer;background-color:#941e1e;"><b>Decline</b></a>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col l3 m6 s12" style="width: 100%;">
                                            <div class="card  hoverable" style="border-radius: 10px;background-color:#1164a8;">
                                                <div class="card-content white-text center">
                                                    <h5><b>{{$b_event->title}}</b></h5>
                                                    <p>Date: {{$b_event->start}}</p>
                                                </div>
                                                <div class="card-action center" style="border-radius: 10px;">
                                                    <a class="bootcamper-accept-event-button-mobile btn" data-value="{{$b_event->b_event_id}}"
                                                       style="color: white; cursor: pointer;background-color:#1b6b25;"><b>Accept</b></a>
                                                    <a class="bootcamper-decline-event-button-mobile btn" data-value="{{$b_event->b_event_id}}"
                                                       style="color: white; cursor: pointer;background-color:#941e1e;"><b>Decline</b></a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @endif
                            @endif

                        @endforeach
                    @endif
                </div>

            </div>

            <div class="row center">
                <div class="col l12 card" style="background: #454242; width: 100%">
                    <h4 style="color: white"><b>EVENTS YOU HAVE ACCEPTED.</b></h4>
                    @if(count($bootcamper_event_array) > 0)
                        @foreach($bootcamper_event_array as $b_event)
                            @if(isset($b_event))
                                @if($b_event->accepted == true)
                                    <div class="col l3 m6 s12" style="width: 100%;">
                                        <div class="card  hoverable" style="border-radius: 10px;background-color:#1b6b25;">
                                            <div class="card-content white-text center">
                                                <h5><b>{{$b_event->title}}</b></h5>
                                                <p>Date: {{$b_event->start}}</p>
                                            </div>
                                            @if($user->data_cellnumber)
                                                <div class="card-action center" style="border-radius: 10px;">
                                                    <a class="waves-effect waves-light btn modal-trigger data-details-button-mobile" disabled id="{{$b_event->id}}" href="#modal2">SUBMIT DATA DETAILS</a>
                                                </div>
                                            @else
                                                <div class="card-action center" style="border-radius: 10px;">
                                                    <a class="waves-effect waves-light btn modal-trigger data-details-button-mobile" id="{{$b_event->id}}" href="#modal2" style="background-color: #1164a8;">SUBMIT DATA DETAILS</a>
                                                </div>
                                            @endif
                                            <br>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @endforeach
                    @endif
                </div>

            </div>
            <!-- Modal Structure -->
            <div role="dialog" id="modal2" class="modal">
                <div class="modal-content">
                    <div class="row" style="alignment: center; align-content: center;">
                        <h4 class="center-align" ><b>Data Details: </b></h4>
                        <div class="input-field col m5" style="alignment: center;">
                            <label for="data_cellnumber_mobile">Cell number</label>
                            <input id="data_cellnumber_mobile" type="number" class="validate">

                        </div>
                        <div class="input-field col m5">
                            <select id="service_provider_network_mobile">
                                <option value="" disabled selected>Choose your option</option>
                                <option value="MTN">MTN</option>
                                <option value="CELL C">CELL C</option>
                                <option value="TELKOM">TELKOM</option>
                                <option value="VODACOM">VODACOM</option>
                                <option value="No data required">No data required</option>
                            </select>
                            <label>Choose your Network</label>
                        </div>
                    </div>
                    <div class="row center">
                        <div class="col s4">
                            <a class="waves-effect waves-light btn section" id="data-upload-submit-button-mobile">Save</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row center">
                <div class="col l12 card" style="background: #454242; width: 100%;">
                    <h4 style="color: white">Documents</h4>
                </div>
                <div class="col l4 m4 s12">
                    <br/><br/>
                    <h5>Pitch Video:</h5>
                    @if(isset($bootcamper->pitch_video_link))
                        <input type="text" id="bootcamper-video-link-input-display-mobile" value="{{$bootcamper->pitch_video_link}}">
                        <p>Date Uploaded: {{$bootcamper->pitch_video_date_time}}</p>
                        <button class="btn blue" id="update-pitch-video-mobile">Update</button><br/><br/><br/>
                    @else
                        <input type="text" id="bootcamper-video-link-input-mobile" placeholder="Upload Pitch Video">
                        <button class="btn blue" id="submit-pitch-video-mobile">Submit</button><br/><br/><br/>
                    @endif
                </div>

                <!--Second Video-->
                <div class="col l4 m4 s12">
                    <h5>Second Pitch Video:</h5>
                    @if(isset($bootcamper->pitch_video_link_two))
                        <input type="text" id="bootcamper-second-video-link-input-display-mobile" value="{{$bootcamper->pitch_video_link_two}}">
                        <p>Date Uploaded: {{$bootcamper->pitch_video_date_time}}</p>
                        <button class="btn blue" id="update-second-pitch-video-mobile">Update</button><br/><br/><br/>
                    @else
                        <input type="text" id="bootcamper-second-video-link-input-mobile" placeholder="Upload Second Pitch Video">
                        <button class="btn blue" id="submit-second-pitch-video-mobile">Submit</button><br/><br/><br/>
                    @endif
                </div>

                <div class="col l4 m4 s12 center">
                    <h5>ID Document:</h5>
                    @if(isset($bootcamper->id_document_url))
                        <input type="file" id="bootcamper-id-document-input-display-mobile" value="{{$bootcamper->id_document_url}}">
                        <p style="color: red;">Only upload another ID document if you wish to update the one you uploaded previously.</p>
                        <button class="btn blue" id="update-id-document-mobile">Update</button><br/><br/><br/>
                        <a href="{{'/storage/'.$bootcamper->id_document_url}}" target="_blank">View ID</a>
                    @else
                        <input class="file-path validate" id="bootcamper-id-document-input-mobile"
                               type="file" placeholder="Upload ID Document">
                        <button class="btn blue" id="submit-id-document-mobile">Submit</button><br/><br/><br/>
                    @endif
                </div>
                <div class="col l4 m4 s12">
                    <h5>Proof of address:</h5>
                    @if(isset($bootcamper->proof_of_address))
                        <input type="file" id="bootcamper-proof-of-address-input-display-mobile" value="{{$bootcamper->proof_of_address}}">
                        <p style="color: red;">Proof of address uploaded.</p>
                        <button class="btn blue" id="update-proof-of-address-mobile">Update</button><br/><br/><br/>
                        <a href="{{'/storage/'.$bootcamper->proof_of_address}}" target="_blank">View Proof</a>
                    @else
                        <input class="file-path validate" id="bootcamper-proof-of-address-input-mobile"
                               type="file" placeholder="Proof of residance">
                        <button class="btn blue" id="submit-proof-of-address-mobile">Submit</button><br/><br/><br/>
                    @endif
                </div>
                <div class="col l4 m4 s12">
                    <h5>CIPC:</h5>
                    @if(isset($bootcamper->cipc))
                        <input type="file" id="bootcamper-cipc-input-display-mobile" value="{{$bootcamper->cipc}}">
                        <p style="color: red;">CICP uploaded.</p>
                        <button class="btn blue" id="update-cipc-mobile">Update</button><br/><br/><br/>
                        <a href="{{'/storage/'.$bootcamper->cipc}}" target="_blank">View CICP</a>

                    @else
                        <input class="file-path validate" id="bootcamper-cipc-input-mobile"
                               type="file" placeholder="CICP">
                        <button class="btn blue" id="submit-cipc-mobile">Submit</button><br/><br/><br/>
                    @endif
                </div>

                <div class="l4 m4 s12" >
                    <h5>Application Form:</h5>
                    <a class="btn blue tooltipped modal-trigger" data-position="left" data-tooltip="View Application" href="#bootcamper_questions_modal1">
                        <i class="large material-icons">remove_red_eye</i>
                    </a>
                </div>

                <br>

                <div class="col l3 m3 s12" style="margin-top: 5vh">
                    @foreach($beginning_of_bootcamper as $beginning_of_bootcampers)
                        <a class="btn  tooltipped modal-trigger" data-position="left" style="background-color: #454242" href="#beginning-of-bootcamper_category_mobile">
                            {{$beginning_of_bootcampers->category_name}}</a>
                    @endforeach
                </div>
                @if($bootcamper->beginning_evaluation_complete ==false)
                @else
                <div class="col l3 m3 s12" style="margin-top: 5vh">
                    @foreach($post_evaluation as $post_evaluations)
                        <a class="btn  tooltipped modal-trigger" data-position="left" style="background-color: #454242" href="#post-of-bootcamper_category_mobile">
                            {{$post_evaluations->category_name}}</a>
                    @endforeach
                </div>
                @endif



                <div class="col l3 m3 s12" style="margin-top: 5vh">
                    @if($bootcamper->bootcamp_nda_agreement == true)
                        <a class="waves-effect waves-light btn blue modal-trigger" href="#agreement-mobile-modal">Bootcamp NDA Agreement</a>
                    @else
                        <div class="row"></div>
                    @endif
                </div>
                {{--UPLOAD AND EDIT CONTRACT--}}
                <div class="col l4 m4 s12">
                    <h5>Stage 2 Contract:</h5>
                    @if(isset($bootcamper->bootcamper_contract_upload))
                        <input type="file" id="bootcamper-contract-input-display-mobile" value="{{$bootcamper->bootcamper_contract_upload}}">
                        <p style="color: red;">Contract uploaded.</p>
                        <button class="btn blue" id="update-contract-upload-mobile">Update</button>
                        <a href="{{'/storage/'.$bootcamper->bootcamper_contract_upload}}" target="_blank">View Contact</a>
                        <br>
                    @else
                        <input class="file-path validate" id="bootcamper-contract-input-mobile"
                               type="file" placeholder="Contract">
                        <button class="btn blue" id="submit-contract-upload-mobile">Submit</button>
                        <br>
                    @endif
                </div>
                {{--Agreement modal--}}
                <div id="agreement-mobile-modal" class="modal">
                    <div class="modal-content">
                        <img style="width: 30vh" src="/images/Prop agrre.png">
                        <div class="col s12">
                            <h5 class="center-align"><b>Open Communications FutureMakers Multi-Party Nondisclosure Agreement</b></h5>
                        </div>
                        <p>This multi-party nondisclosure agreement ("Agreement") is entered into and made effective as of the date set forth above among the parties listed at the end of this Agreement and any third party that executes this Agreement after the Effective Date. This Agreement is intended to cover the parties’ confidentiality obligations with respect to disclosures regarding or pertinent to the exploration ways the parties can work together for mutual benefit.</p>
                        <p><b>The Parties agree as follows:</b></p>
                        <p>1.	Confidential Information. The confidential, proprietary and trade secret information of each party ("Confidential Information") to be disclosed hereunder is that tangible or intangible information which is marked with a "Confidential," "Proprietary", or similar legend and which is clearly identified in writing as provided under this Agreement and includes any information disclosed during discussion sessions. To be considered Confidential Information, non-tangible disclosures must be identified as confidential under the terms and conditions of this Agreement prior to disclosure and reduced to writing, marked as provided above, and delivered to the receiving party within thirty (30) calendar days of the original date of disclosure. Any information received from any other participant in Bootcamp 1 will not be used by any other party.
                            Ideas or discussions not forming part of the Disclosing Parties normal course of business and ideas generated during discussions will not be regarded as confidential information.
                        </p>
                        <p>2.	Obligations of receiving party. Each receiving party will maintain the confidentiality of the Confidential Information for each disclosing party with at least the same degree of care that it uses to protect its own confidential and proprietary information, but no less than a reasonable degree of care under the circumstances. No receiving party will disclose any of a disclosing party's confidential information to any employees or to any third parties except to employees, contractors, and Affiliates (as below defined) of such receiving party who have a need to know and who agree to abide by nondisclosure terms at least as
                            comprehensive as those set forth herein; provided such receiving party will be liable for breach of any such entity. The receiving party will not make any copies of the Confidential Information received from a disclosing party except as necessary for their employees and contractors and those employees and contractors of the receiving party's Affiliates with a need to know. Any copies which are made will include the legend and identification statement as supplied by the disclosing party.  The term “Affiliate” of a party, as used herein, shall mean any company, corporation or other business entity which directly or indirectly (i) controls said party or (ii) is controlled by said party or (iii) is controlled by any entity under (i) above, where “to control” an entity shall mean to own more than fifty per cent (50%) of the shares of, or of the ownership interest in, such entity.
                        </p>
                        <p>3.    Period of confidentiality. The receiving party’s obligation of confidentiality shall be for a period of two (2) years from the date of receipt of the disclosing party’s Confidential Information</p>
                        <p>4.    Termination of obligation of confidentiality. No receiving party will be liable for the disclosure of a disclosing party's confidential information which is:</p>
                        <p>a)	Published with the consent of the disclosing party or is otherwise rightfully in the public domain other than by a breach of a duty of confidentiality to the disclosing party;</p>
                        <p>b)   Rightfully received from a third party without any obligation of confidentiality;</p>
                        <p>c)	Rightfully known to the receiving party without any limitation on use or disclosure prior to its receipt from the disclosing party</p>
                        <p>d)   Independently developed by employees of the receiving party; or</p>
                        <p>e)    Generally made available to third parties by the disclosing party without restriction on disclosure.</p>
                        <p>5.    Title. Title or the right to possess Confidential Information as between receiving parties and the disclosing party will remain in the disclosing party.</p>
                        <p>6.    No obligation of disclosure; Termination. No party has an obligation to disclose Confidential Information to any other
                            party.  Any party may withdraw from this Agreement at any time and without cause upon written notice to the remaining
                            parties, provided that such withdrawing party's obligations with respect to Confidential Information of other parties
                            disclosed during the term of this Agreement will survive any such termination until the end of the period set forth in
                            Section 3. A disclosing party may at any time: (a) cease giving Confidential Information to the other party without any
                            liability, and/or (b) request in writing the return or destruction of all or part of its Confidential Information previously
                            disclosed, and all copies thereof, and each other party will comply with such request, and certify in writing its compliance
                            within sixty (60) calendar days.
                        </p>
                        <p>7.	No warranty.  All parties acknowledge that all information provided hereunder is provided "AS IS" WITH NO WARRANTIES WHATSOEVER, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, AND THE PARTIES EXPRESSLY DISCLAIM ANY WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, FITNESS FOR ANY PARTICULAR PURPOSE, OR ANY WARRANTY OTHERWISE ARISING OUT OF ANY PROPOSAL, SPECIFICATION, OR SAMPLE.</p>
                        <p>8.    General</p>
                        <p>a)	This Agreement is neither intended to, nor will it be construed as creating a joint venture, a partnership or other form of a business association between the parties, nor an obligation to buy or sell products using or incorporating the Confidential Information.</p>
                        <p>b)   Each party understands and acknowledges that no license under any patents, copyrights, trademarks, or maskworks is granted to or conferred upon any party or by the disclosure of any Confidential Information by one party to the other party as contemplated hereunder, either expressly, by implication, inducement, estoppel or otherwise, and that any license under such intellectual property rights must be express and in writing.</p>
                        <p>c)	The failure of any party to enforce any rights resulting from breach of any provision of this Agreement will not be deemed a waiver of any right relating to a subsequent breach of such provision or of any other right hereunder.</p>
                        <p>d)   This Agreement constitutes the entire agreement between the parties with respect to the disclosures of Confidential
                            Information, and may not be amended except in a writing signed by a duly authorized representative of the respective parties. e)    Each party shall execute a copy of this Agreement, each of which shall be deemed an original and all of which together shall
                            constitute one instrument. A list shall be maintained of all parties that have executed this Agreement and shall be made available to all parties.
                        </p>
                        <p>e)    Each party shall execute a copy of this Agreement, each of which shall be deemed an original and all of which together shall
                            constitute one instrument. A list shall be maintained of all parties that have executed this Agreement and shall be made available to all parties.
                        </p>
                        <p id="thank-you">f)    This Agreement shall be subject to and governed by the law of South Africa and the place of jurisdiction shall be Port Elizabeth, South Africa.

                        </p>
                    </div>

                    <div class="row" style="margin-left: 2em; margin-right: 2em;">
                        <button class="btn right" id="agreement-button-mobile">Agree</button>
                    </div>
                </div>
            </div>
        </div>


        <br>
        <br>

        <!-- Beginning of bootcamp evaluation form-->
        <div id="beginning-of-bootcamper_category_mobile" class="modal">
            @foreach($beginning_of_bootcamper as $beginningOfBootcamp)
                <h4 class="center-align"><b>{{$beginningOfBootcamp->category_name}}</b></h4>
            @endforeach
            @if($bootcamper->beginning_evaluation_complete == true)
                <div class="row" style="margin-left: 4em;margin-right: 4em">
                    <p class="center-align">Form already submmitted</p>
                </div>
            @else
                <ol>
                    <div class="modal-content">
                        <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                        @foreach($beginning_of_bootcamper as $cur_category)
                            @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                <div class="row" style="border-style: ridge">
                                    <div class="" style="margin-left: 4em">
                                        @if($cur_question->question_type == "text")
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="text" class=" answer-text-mobile" style="width:50%">
                                            </div>
                                        @else
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-mobile" style="width:30%">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </ol>
                <div class="col s1 answers-submit" hidden></div>
                <div class="row center">
                    <button class="btn waves-effect waves-light" id="beginning-of-bootcamp-upload-answers-form-mobile" name="action"
                            style="margin-right: auto;">Save
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            @endif
        </div>

        <!-- Post of bootcamp evaluation form-->
        <div id="post-of-bootcamper_category_mobile" class="modal">
            @foreach($post_evaluation as $post_evaluations)
                <h4 class="center-align"><b>{{$post_evaluations->category_name}}</b></h4>
            @endforeach
            @if($bootcamper->post_evaluation_complete == true)
                <div class="row" style="margin-left: 4em;margin-right: 4em">
                    <p class="center-align">Form already submmitted</p>
                </div>
            @else
                <ol>
                    <div class="modal-content">
                        <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                        @foreach($post_evaluation as $cur_category)
                            @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                <div class="row" style="border-style: ridge">
                                    <div class="" style="margin-left: 4em">
                                        @if($cur_question->question_type == "text")
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="text" class=" answer-text-mobile" style="width:50%">
                                            </div>
                                        @else
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-mobile" style="width:30%">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </ol>
                    <div class="col s1 answers-submit" hidden></div>
                <div class="row center">
                    <button class="btn waves-effect waves-light" id="post-of-bootcamp-upload-answers-form-mobile" name="action"
                            style="margin-right: auto;">Save
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            @endif
        </div>

        <!-- Modal Structure -->
        <div id="bootcamper_category_modal1" class="modal">
            @foreach($category as $categories)
                <h4 class="center-align"><b>{{$categories->category_name}}</b></h4>
            @endforeach
            @if($bootcamper->evaluation_form_completed == true)
                <div class="row" style="margin-left: 4em;margin-right: 4em">
                    <p class="center-align">Form already submmitted</p>
                </div>
            @else
                <ol>
                    <div class="modal-content">
                        <p style="color: red"><b>Please rate the following items on a scale of one to ten (1-10), with one being bad, five being acceptable, and ten being perfect.</b></p>
                        @foreach($category as $cur_category)
                            @foreach($cur_category->questions->sortBy('question_number') as $cur_question)
                                <li value="{{$cur_question->question_number}}">  {{ $cur_question->question_text}} </li>
                                <div class="row" style="border-style: ridge">
                                    <div class="" style="margin-left: 4em">
                                        @if($cur_question->question_type == "text")
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="text" class=" answer-text-mobile" style="width:50%">
                                            </div>
                                        @else
                                            <div class="input-field">
                                                <input id="{{$cur_question->id}}" required type="number" min="1" max="10" class=" answer-text-mobile" style="width:30%">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </ol>
                <div class="col s1 answers-submit" hidden></div>
                <div class="row center">
                    <button class="btn waves-effect waves-light" id="upload-answers-form-mobile" name="action"
                            style="margin-right: auto;">Save
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            @endif
        </div>

        <div  role="dialog" id="bootcamper_questions_modal1" class="modal">
            <div class="container" id="QuestionsAndAnswers">
                <br>
                <div class="row center">
                    <h4><b>APPLICATION DETAILS</b></h4>
                    <div class="row">
                        <div class="col l6 m6 s12">
                            <h6>{{$bootcamper->user->name}} {{$bootcamper->user->surname}}</h6>
                            <h6>Initials: {{$bootcamper->user->initials}}</h6>
                            <h6>Email: {{$bootcamper->user->email}}</h6>
                            <h6>Contact Number: {{$bootcamper->user->contact_number}}</h6>
                            <h6>ID Number: {{$bootcamper->user->id_number}}</h6>
                            <h6>Age: {{$bootcamper->user->age}}</h6>
                            <h6>Gender: {{$bootcamper->user->gender}}</h6>
                        </div>
                        <div class="col l6 m6 s12">
                            <h6>Address One: {{$bootcamper->user->address_one}}</h6>
                            <h6>Address Two: {{$bootcamper->user->address_two}}</h6>
                            <h6>Address Three: {{$bootcamper->user->address_three}}</h6>
                        </div>
                    </div>
                    <br>
                    <hr style="background: darkblue; height: 5px;">
                    <br>
                </div>

                <div class="row">
                    @foreach($bootcamper_question_array as $question_answer)
                        <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                        <div class="input-field">
                            <textarea disabled>{{$question_answer->answer_text}}</textarea>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>





    <script>
        $('.tab1').on('click', function () {
            $('#pitch-video-one').show();
            $('#pitch-video-two').hide();
            $('#id-document-upload').hide();
            $('#p-o-a-upload').hide();
            $('#cipc-doc').hide();
            $('#application-form').hide();
            $('#evaluation').hide();
            $('#desktop-nda').hide();
            $('#contract-upload').hide();
        });
        $('.tab2').on('click', function () {
            $('#pitch-video-two').show();
            $('#pitch-video-one').hide();
            $('#id-document-upload').hide();
            $('#p-o-a-upload').hide();
            $('#cipc-doc').hide();
            $('#application-form').hide();
            $('#evaluation').hide();
            $('#desktop-nda').hide();
            $('#contract-upload').hide();
        });
        $('.tab3').on('click', function () {
            $('#id-document-upload').show();
            $('#pitch-video-two').hide();
            $('#pitch-video-one').hide();
            $('#p-o-a-upload').hide();
            $('#cipc-doc').hide();
            $('#application-form').hide();
            $('#evaluation').hide();
            $('#desktop-nda').hide();
            $('#contract-upload').hide();
        });
        $('.tab4').on('click', function () {
            $('#p-o-a-upload').show();
            $('#pitch-video-two').hide();
            $('#pitch-video-one').hide();
            $('#id-document-upload').hide();
            $('#cipc-doc').hide();
            $('#application-form').hide();
            $('#evaluation').hide();
            $('#desktop-nda').hide();
            $('#contract-upload').hide();
        });
        $('.tab5').on('click', function () {
            $('#cipc-doc').show();
            $('#p-o-a-upload').hide();
            $('#pitch-video-two').hide();
            $('#pitch-video-one').hide();
            $('#id-document-upload').hide();
            $('#application-form').hide();
            $('#evaluation').hide();
            $('#desktop-nda').hide();
            $('#contract-upload').hide();
        });
        $('.tab6').on('click', function () {
            $('#application-form').show();
            $('#cipc-doc').hide();
            $('#p-o-a-upload').hide();
            $('#pitch-video-two').hide();
            $('#pitch-video-one').hide();
            $('#id-document-upload').hide();
            $('#evaluation').hide();
            $('#desktop-nda').hide();
            $('#contract-upload').hide();
        });
        $('.tab7').on('click', function () {
            $('#evaluation').show();
            $('#application-form').hide();
            $('#cipc-doc').hide();
            $('#p-o-a-upload').hide();
            $('#pitch-video-two').hide();
            $('#pitch-video-one').hide();
            $('#id-document-upload').hide();
            $('#desktop-nda').hide();
            $('#contract-upload').hide();
        });
        $('.tab8').on('click', function () {
            $('#desktop-nda').show();
            $('#evaluation').hide();
            $('#application-form').hide();
            $('#cipc-doc').hide();
            $('#p-o-a-upload').hide();
            $('#pitch-video-two').hide();
            $('#pitch-video-one').hide();
            $('#id-document-upload').hide();
            $('#contract-upload').hide();
        });

        $('.tab9').on('click', function () {
            $('#contract-upload').show();
            $('#desktop-nda').hide();
            $('#evaluation').hide();
            $('#application-form').hide();
            $('#cipc-doc').hide();
            $('#p-o-a-upload').hide();
            $('#pitch-video-two').hide();
            $('#pitch-video-one').hide();
            $('#id-document-upload').hide();
        });

        $(document).ready(function(){
            $('select').formSelect();
            $('.modal').modal();
            $('.tabs').tabs();
        });
        let b_event_id = null;

        $('.data-details-button').on('click', function(){
            b_event_id = this.id;
        });

        $('.data-details-button-mobile').on('click', function (){
            b_event_id = this.id;
        })
        //Bootcamp agreement
        $('#agreement-button-mobile').on('click', function () {
            let bootcamper_id = $('#bootcamper-id-input').val();
            let formData = new FormData();

            let url = '/update-bootcamp-agreement/' + bootcamper_id;

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                success: function (response, a, b) {
                    $('#thank-you').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                    sessionStorage.clear();
                },
            });
        });

        $('#agreement-button-desktop').on('click', function () {
            let bootcamper_id = $('#bootcamper-id-input').val();
            let formData = new FormData();

            let url = '/update-bootcamp-agreement/' + bootcamper_id;

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                success: function (response, a, b) {
                    $('#thank-you').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                    sessionStorage.clear();
                },
            });
        });

        /*Submit Proof of address*/
        $('#submit-contract-upload').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-contract-input')[0].files, function (i, file) {
                formData.append('bootcamper_contract_upload', file);
            });

            let url = '/store-bootcamper-contract/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-contract-upload').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $("#update-contract-upload").on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-contract-input-display')[0].files, function (i, file) {
                formData.append('bootcamper_contract_upload', file);
            });


            let url = '/store-bootcamper-contract/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-contract-upload').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /*Submit Proof of address mobile*/
        $('#submit-contract-upload-mobile').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-contract-input-mobile')[0].files, function (i, file) {
                formData.append('bootcamper_contract_upload', file);
            });

            let url = '/store-bootcamper-contract/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-contract-upload-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $("#update-contract-upload-mobile").on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('bootcamper_contract_upload', $('#bootcamper-contract-input-display-mobile').val());

            let url = '/store-bootcamper-contract/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-contract-upload-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });
        /* POST of bootcamp desktop*/
        $('#post-of-bootcamp-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));
            formData.append('application_completed', 'true');

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-of-bootcamp-evaluation",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* POST of bootcamp mobile*/
        $('#post-of-bootcamp-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));
            formData.append('application_completed', 'true');

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-post-of-bootcamp-evaluation",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });


        /* beginning of bootcamp desktop*/
        $('#beginning-of-bootcamp-upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));
            formData.append('application_completed', 'true');

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-beginning-of-bootcamp-evaluation",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /* mobile beginning of bootcamp desktop*/
        $('#beginning-of-bootcamp-upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));
            formData.append('application_completed', 'true');

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-beginning-of-bootcamp-evaluation",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });


        //Save bootcamper evaluation form
        $('#upload-answers-form-desktop').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));
            formData.append('application_completed', 'true');

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-bootcamp-evaluation-answers",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#upload-answers-form-mobile').on('click', function () {

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-mobile').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));
            formData.append('application_completed', 'true');

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "store-bootcamp-evaluation-answers",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $(".answers-submit").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        //Bootcamper data upload
        $('#data-upload-submit-button').on('click',function () {
            let user_id = $('#user-id-input').val();

            let formData = new FormData();
            formData.append('data_cellnumber', $('#data_cellnumber').val());
            formData.append('service_provider_network', $('#service_provider_network').val());
            console.log(formData);

            let url = '/bootcamper-store-data-network/' + user_id;

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                success: function (response, a, b) {
                    $("#data-upload-submit-button").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },
            });
        });

        $('#data-upload-submit-button-mobile').on('click',function () {
            let user_id = $('#user-id-input').val();

            let formData = new FormData();
            formData.append('data_cellnumber', $('#data_cellnumber_mobile').val());
            formData.append('service_provider_network', $('#service_provider_network_mobile').val());
            console.log(formData);

            let url = '/bootcamper-store-data-network/' + user_id;

            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                success: function (response, a, b) {
                    $("#data-upload-submit-button-mobile").notify(
                        "Details saved successfully", "success",
                        { position:"right" }
                    );
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },
            });
        });

        $('.bootcamper-accept-event-button').on('click', function(){
            let b_event_id = this.getAttribute('data-value');

            let formData = new FormData();
            formData.append('b_event_id', b_event_id);

            let url = '/bootcamper-accept-event';
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#notification-header').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('.bootcamper-accept-event-button-mobile').on('click', function(){
            let b_event_id = this.getAttribute('data-value');

            let formData = new FormData();
            formData.append('b_event_id', b_event_id);

            let url = '/bootcamper-accept-event';
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#notification-header').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('.bootcamper-decline-event-button').on('click', function(){
            let b_event_id = this.getAttribute('data-value');

            let formData = new FormData();
            formData.append('b_event_id', b_event_id);

            let url = '/bootcamper-decline-event';
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#notification-header').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('.bootcamper-decline-event-button-mobile').on('click', function(){
            let b_event_id = this.getAttribute('data-value');

            let formData = new FormData();
            formData.append('b_event_id', b_event_id);

            let url = '/bootcamper-decline-event';
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#notification-header').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#submit-pitch-video').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('pitch_video', $('#bootcamper-video-link-input').val());

            let url = '/bootcamper-upload-pitch-video/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-pitch-video').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#submit-pitch-video-mobile').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('pitch_video', $('#bootcamper-video-link-input-mobile').val());

            let url = '/bootcamper-upload-pitch-video/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-pitch-video-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        <!--Submit second video-->
        $('#submit-second-pitch-video').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('second_pitch_video', $('#bootcamper-second-video-link-input').val());

            let url = '/bootcamper-upload-second-pitch-video/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-second-pitch-video').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#submit-second-pitch-video-mobile').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('second_pitch_video', $('#bootcamper-second-video-link-input-mobile').val());

            let url = '/bootcamper-upload-second-pitch-video/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-second-pitch-video-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $("#update-pitch-video").on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('pitch_video', $('#bootcamper-video-link-input-display').val());

            let url = '/bootcamper-upload-pitch-video/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-pitch-video').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $("#update-pitch-video-mobile").on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('pitch_video', $('#bootcamper-video-link-input-display-mobile').val());

            let url = '/bootcamper-upload-pitch-video/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-pitch-video-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        <!--Update second video-->
        $("#update-second-pitch-video").on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('second_pitch_video', $('#bootcamper-second-video-link-input-display').val());

            let url = '/bootcamper-upload-second-pitch-video/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-second-pitch-video').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $("#update-second-pitch-video-mobile").on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();
            formData.append('second_pitch_video', $('#bootcamper-second-video-link-input-display-mobile').val());

            let url = '/bootcamper-upload-second-pitch-video/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-second-pitch-video-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#submit-id-document').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-id-document-input')[0].files, function (i, file) {
                formData.append('id_document', file);
            });

            let url = '/bootcamper-upload-id-document/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-id-document').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#submit-id-document-mobile').on('click', function(){
            console.log("test");
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-id-document-input-mobile')[0].files, function (i, file) {
                formData.append('id_document', file);
            });

            let url = '/bootcamper-upload-id-document/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-id-document-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        })

        $('#update-id-document').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-id-document-input-display')[0].files, function (i, file) {
                formData.append('id_document', file);
            });

            let url = '/bootcamper-upload-id-document/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-id-document').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#update-id-document-mobile').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-video-link-input-display-mobile')[0].files, function (i, file) {
                formData.append('id_document', file);
            });

            let url = '/bootcamper-upload-id-document/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-id-document-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /*Submit Proof of address*/
        $('#submit-proof-of-address').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-proof-of-address-input')[0].files, function (i, file) {
                formData.append('proof_of_address', file);
            });

            let url = '/store-proof-of-address/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-proof-of-address').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#submit-proof-of-address-mobile').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-proof-of-address-input-mobile')[0].files, function (i, file) {
                formData.append('proof_of_address', file);
            });

            let url = '/store-proof-of-address/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-proof-of-address-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /*Update proof of address*/
        $('#update-proof-of-address').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-proof-of-address-input-display')[0].files, function (i, file) {
                formData.append('proof_of_address', file);
            });

            let url = '/store-proof-of-address/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-proof-of-address').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#update-proof-of-address-mobile').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-proof-of-address-input-display-mobile')[0].files, function (i, file) {
                formData.append('proof_of_address', file);
            });

            let url = '/store-proof-of-address/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-proof-of-address-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /*Submit CICP*/
        $('#submit-cipc').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-cipc-input')[0].files, function (i, file) {
                formData.append('cipc', file);
            });

            let url = '/store-cipc/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#bootcamper-cipc-input').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#submit-cipc-mobile').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-cipc-input-mobile')[0].files, function (i, file) {
                formData.append('cipc', file);
            });

            let url = '/store-cipc/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#submit-cipc-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        /*Update CICP*/
        $('#update-cipc').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-cipc-input-display')[0].files, function (i, file) {
                formData.append('cipc', file);
            });

            let url = '/store-cipc/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#bootcamper-cipc-input-display').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#update-cipc-mobile').on('click', function(){
            let bootcamper_id = $('#bootcamper-id-input').val();

            let formData = new FormData();

            jQuery.each(jQuery('#bootcamper-cipc-input-display-mobile')[0].files, function (i, file) {
                formData.append('cipc', file);
            });

            let url = '/store-cipc/' + bootcamper_id;
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                success: function (response, a, b) {
                    $('#update-cipc-mobile').notify(response.message, "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error", response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#bootcamper-update-application-form').on('click', function(){
            $('#bootcamper-update-application-form').text("Loading...");
            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));
            formData.append('application_completed', 'true');

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "/Application-Process/questions",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $('#bootcamper-update-application-form').notify("Successfully updated.", "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#bootcamper-update-application-form-mobile').on('click', function(){
            $('#bootcamper-update-application-form').text("Loading...");
            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));
            formData.append('application_completed', 'true');

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "/Application-Process/questions",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $('#bootcamper-update-application-form').notify("Successfully updated.", "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#bootcamper-submit-application-form').on('click', function(){
            $('#bootcamper-submit-application-form').text("Loading...");

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "/Application-Process/questions",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $('#bootcamper-update-application-form').notify("Thank you for completing your applcation form.", "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });

        $('#bootcamper-submit-application-form-mobile').on('click', function(){
            $('#bootcamper-submit-application-form').text("Loading...");

            //To store collection of all the answers
            let answersArray = [];

            //To get current user
            let data = $('#user-id-input').val();

            //Loop through all inputs created
            $('.answer-text-desktop').each(function () {
                let model = {
                    question_id: this.id,
                    user_id: data,
                    answer_text: this.value,
                };

                //Add to list of answers
                answersArray.push(model);
            });

            let formData = new FormData();
            formData.append('questions_answers',JSON.stringify(answersArray));

            //To push the answersArray as data to the controller and save all the answers to the db
            $.ajax({
                url: "/Application-Process/questions",
                processData: false,
                contentType: false,
                dataType: 'json',
                data: formData,
                type: 'post',
                headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                success: function (response, a, b) {
                    $('#bootcamper-update-application-form').notify("Thank you for completing your applcation form.", "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },

                error: function (response) {
                    console.log("error",response);
                    let message = response.responseJSON.message;
                    alert(message);
                }
            });
        });
    </script>

@endsection
