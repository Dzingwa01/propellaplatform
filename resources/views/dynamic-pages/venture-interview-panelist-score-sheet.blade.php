@extends('layouts.app')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>

    <div class="section" id="QuestionsAndAnswers" style="margin-top: 2em;">
        <div class="row center">
            <h5>{{$user_object->panelist_full_name}}'s score sheet</h5>
        </div>
        <div class="row" style="margin-left: 4em; margin-right: 4em;">
            <ul>
                @foreach ($panelist_question_answers as $pqa)
                    @if(count($pqa->question_sub_texts) > 0)
                        <li>{{$pqa->question_number}} - {{ $pqa->question_text}} </li>
                        <div class="row">
                            @foreach($pqa->question_sub_texts as $question_sub)
                                <div class="col l3 m3 s6 center"
                                     style="border: solid; border-width: 1px; border-color: black;">
                                    <p>{{$question_sub}}</p>
                                </div>
                            @endforeach
                        </div>
                        <div class="input-field panel-question-answers-comment">
                            <input value="{{$pqa->score}}" disabled class="materialize-textarea answer-text-desktop"
                                   style="width:10%" type="number">
                            <label for="answer-text-desktop">Score</label>

                            <textarea class="comments" disabled>{{$pqa->comment}}</textarea>
                        </div>
                    @else
                        <li>{{$pqa->question_number}} - {{ $pqa->question_text}} </li>
                        <div class="input-field panel-question-answers-comment">
                            <input value="{{$pqa->score}}" disabled class="materialize-textarea answer-text-desktop"
                                   style="width:10%" type="number">
                            <label for="answer-text-desktop">Score</label>

                            <textarea class="comments" disabled>{{$pqa->comment}}</textarea>
                        </div>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>

    <div class="row center">
        <button id="print-button"  class="waves-effect waves-light btn" onclick="printContent('QuestionsAndAnswers')">
            <i class="material-icons left">local_printshop</i>Print
        </button>
    </div>

    <script>
        function printContent(el){
            var restorepage = $('body').html();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
        }
    </script>
