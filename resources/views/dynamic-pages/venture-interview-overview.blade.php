@extends('layouts.app')
    <head>
        <style>
            table {
                width:100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
                text-align: left;
            }
            table#t01 tr:nth-child(even) {
                background-color: #eee;
            }
            table#t01 tr:nth-child(odd) {
                background-color: #fff;
            }
            table#t01 th {
                background-color: black;
                color: white;
            }
        </style>
    </head>
    <br>
    <br>

    <div class="section" style="background-color: grey; margin-left: 5em; margin-right: 5em;">
        <input id="venture-id-input" value="{{$data->venture->id}}" hidden disabled>
        <div class="row center">
            <h5>Panelists assigned to venture interview</h5>

            @foreach($data->venture_interview_results_array as $panelist)
                <div class="col l3 m6 s12 center">
                    <div class="card">
                        <div class="card-content">
                            <h5>{{$panelist->panelist_full_name}}</h5>
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Question Number</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>Question Score</h6>
                                </div>
                            </div>
                            @if(isset($panelist->user_questions_scores))
                                @foreach($panelist->user_questions_scores as $question_answer)
                                    <div class="row">
                                        <div class="col l6 m6 s12" style="background-color: darkgray;">
                                            <p>{{$question_answer->question_number}}</p>
                                        </div>
                                        <div class="col l6 m6 s12" style="background-color: dimgray;">
                                            <p>{{$question_answer->question_score}}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="row">
                                <div class="col l6 m6 s12">
                                    <h6>Total Score</h6>
                                </div>
                                <div class="col l6 m6 s12">
                                    <h6>{{isset($panelist->panelist_applicant_score) ? $panelist->panelist_applicant_score : 0}}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <div class="row center">
                                <a class="btn-flat" onclick="getPanelistScoreSheet(this)" id="{{$panelist->venture_interview_id}}">Get score sheet</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row">
                <div class="col l6 m6 s12">
                    <h5>Total Combined Score: {{$data->total_interview_score}}</h5>
                </div>
                <div class="col l6 m6 s12">
                    <h5>Total Average Score: {{$data->average_interview_score}}</h5>
                </div>
            </div>
        </div>
    </div>

    <div id="panelist-score-sheet-row" class="left-align">

    </div>

    <div class="section" style="margin: 3em 3em 3em 3em;">
        <div class="row center">
            <h5> Combined scores per question </h5>
            <table id="t01">
                <tr>
                    <th>Question Number</th>
                    <th>Total Score</th>
                </tr>
                @foreach($data->question_scores_array as $question_score)
                    <tr>
                        <td>{{$question_score->question_number}}</td>
                        <td>{{$question_score->question_score}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    <script>
        function getPanelistScoreSheet(obj){
            let venture_interview_id = obj.id;
            let url = '/admin-get-panelist-venture-interview-score-sheet/' + venture_interview_id;

            $.get(url, function(data){
                $('#panelist-score-sheet-row').append(
                    data.view_data
                )
            });
        }
    </script>
