@extends('layouts.app')
@section('content')

    <link rel="stylesheet" type="text/css" href="/css/Founder/FounderMain.css"/>
    <!---Link For Used Icons-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;500&family=Roboto:wght@100&display=swap" rel="stylesheet">
    <!---Links for Social Media-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" type="text/css" rel="stylesheet">
    </head>
    <br>

<style>
    .videoWrapper {
        position: relative;
        padding-bottom: 56.25%; /* 16:9 */
        padding-top: 25px;
        height: 0;
    }
    .videoWrapper iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style>

    <!--Comment-->
    <!--Lost page-->
    <!--NEW PROFILE DESKTOP-->
    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTWSFMC"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="row " id="VentureDesktop">
        <div class="col s6">
            <br>
            <h6 class="oswald-light" style="margin-left: 120px">PROPELLA BUSINESS INCUBATOR - VENTURES</h6>
            <h4 class="oswald" style="margin-left: 120px;text-transform: capitalize">{{$venture->company_name}}</h4>
            <div class="row" style="margin-left: 120px;">
                <div>
                    @if($venture->hub!= null)
                        <a class="tags" style="text-transform: lowercase;" class="btn disabled">{{$venture->hub}}</a>
                    @endif
                        @if($venture->stage!= null)
                            <a class="tags" style="text-transform: lowercase;" class="btn disabled">{{$venture->stage}}</a>
                        @elseif(is_null($venture->stage))
                            <a class="tags" style="text-transform: lowercase;" class="btn disabled" hidden>{{$venture->stage}}</a>
                        @endif
                        @if($venture->cohort!= null)
                            <a class="tags" style="text-transform: lowercase;" class="btn disabled">{{$venture->cohort}}</a>
                        @endif
                    @if($venture->smart_city_tags!= null)
                            <a class="tags" style="text-transform: lowercase;" class="btn disabled">{{$venture->smart_city_tags}}</a>
                    @endif
                </div>
            </div>

            <div class="z-depth-3 founderlogo" style="height: 250px">
                    <img class="imglogo" src="/storage/{{isset($venture_upload->company_logo_url)?$venture_upload->company_logo_url:''}}">
            </div>

            <div class="card founderlogo">
                <br>
                <div style="margin-left: 2em">
                   <h6 class="oswaldbody">COMPANY PROFILE</h6>
                </div>

                @if($venture->elevator_pitch != null)
                    <p class="roboto" style="margin-left: 2em;margin-right: 2em;">
                        {{$venture->elevator_pitch}}
                    </p>
                @endif
                <br>
                @if($venture->venture_profile_information != null)
                    <p class="roboto" style="margin-left: 2em;margin-right: 2em;">
                        {{$venture->venture_profile_information}}
                    </p>
                @endif
                <!--Video shot-->
                @if($venture_upload->video_title != null)
                    <p style="margin-left: 2em;margin-right: 2em;"><b>{{$venture_upload->video_title}}</b></p>
                @endif
                @if($venture_upload->video_shot_url != null)
                    <iframe style="margin-left: 2em;" src="{{isset($venture_upload->video_shot_url)?$venture_upload->video_shot_url:''}}" width="550" height="400" allow="autoplay;" allowfullscreen></iframe>
                @endif

                <div style="margin-left: 1em;margin-right: 1em;">
                    <br/>
                    <br/>
                    <br>
                    @if(isset($venture_upload->infographic_url))
                        <div class="col s4">
                            <a class="waves-effect waves-light btn modal-trigger infographicBtn" target="_blank" href="{{'/storage/'.$venture_upload->infographic_url}}"
                               style="width:100%;background-color: rgba(0, 121, 52, 1)">Infographic</a>
                        </div>
                    @endif

                    <div>
                    @if(isset($venture->ventureOwnership->ownership) or isset($venture->ventureOwnership->registration_number) or isset($venture->ventureOwnership->BBBEE_level))
                        <div class="col s4">
                            <a class="waves-effect waves-light btn modal-trigger" href="#modal4" href="#"
                               style="width:100%;background-color: rgba(0, 47, 95, 1)">Ownership</a>
                            <div id="modal4" class="modal">
                                <div class="modal-content">
                                    <h4>{{$venture->company_name}}'s Ownership</h4>
                                    <p>Company
                                        Ownership: {{isset($current_venture_ownership->ownership)?$current_venture_ownership->ownership:''}}</p>
                                    <p>Company Registration
                                        Number: {{isset($current_venture_ownership->registration_number)?$current_venture_ownership->registration_number:''}}</p>
                                    <p>Company BBBEE
                                        Level: {{isset($current_venture_ownership->BBBEE_level)?$current_venture_ownership->BBBEE_level:''}}</p>
                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                </div>
                            </div>
                        </div>
                    @endif
                    </div>
                    <div>
                        @if(isset($venture_upload->crowdfunding_url))
                            <div class="col s4">
                                <a class="waves-effect blue waves-light btn modal-trigger" href="#modal5" href="#"
                                   style="width:100%;">Crowd Funding</a>
                                <div id="modal5" class="modal">
                                    <div class="modal-content">
                                        <img
                                            src="/storage/{{isset($venture_upload->crowdfunding_url)?$venture_upload->crowdfunding_url:''}}">
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div style="margin-left: 1em;margin-right: 1em;">
                        <div class="col s4">
                            <a class="waves-effect waves-light btn modal-trigger" href="#modal7" href="#"
                               style="width:100%;background-color: rgba(0, 121, 52, 1)">Invest in us</a>
                            <div id="modal7" class="modal">
                                <div class="modal-content">
                                    <div class="row" style="width: 70%;margin-left: 100px;">
                                        <form id="ContactUsForm">
                                            <div class="input-field">
                                                <input id="name" type="tel"/>
                                                <label for="name">First name</label>
                                            </div>

                                            <div class="input-field">
                                                <select>
                                                    <option value="Subject" disabled selected>Subject</option>
                                                    <option value="1">General Discussion</option>
                                                    <option value="2">Invest</option>
                                                    <option value="3">Entrepreneurs</option>
                                                    <option value="4">Apply</option>
                                                </select>
                                            </div>
                                            <div class="input-field">
                                                <input id="email" type="email"/>
                                                <label for="email">Email</label>
                                            </div>

                                            <div class="input-field">
                                                <input id="Message" type="tel"/>
                                                <label for="Message">Message</label>
                                            </div>

                                            <div class="input-field">
                                                <input id="phone" type="tel"/>
                                                <label for="phone">Phone Number</label>
                                            </div>

                                            <div>
                                                <button class="btn waves-effect waves-light float-right" type="submit"
                                                        name="action">Send
                                                    <i class="material-icons right">send</i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <!--Video shot-->
{{--                    @if($venture_upload->video_shot_url !=null)--}}

                    <div style="margin-left: 1.75em; margin-right: 1em">
                        <div class="col s4">
                            <a class="waves-effect waves-light btn modal-trigger" href="{{isset($venture_upload->video_shot_url)?$venture_upload->video_shot_url:''}}" target="_blank"
                               style="width:100%;background-color: #454242">VENTURE VIDEO</a>
                        </div>
                    </div>
                    <div id="modal22" class="modal" style="width: 80%">
                        <p style="margin-left: 2em;margin-right: 2em;"><b>{{$venture_upload->video_title}}</b>
                        </p>
                        <div class="modal-content">
                            <iframe style="margin-left: 6em;" src="{{isset($venture_upload->video_shot_url)?$venture_upload->video_shot_url:''}}" width="450" height="200" allow="autoplay;" allowfullscreen></iframe>

                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                        </div>
                    </div>

{{--                    @endif--}}
                </div>



                <br>
                <!--Media-->
                <div class="row">
                    <div class="col s7">
                        <div class="row" style="padding-left: 25px;">
                            @if($venture->business_facebook_url != null)
                                <div class="col s2">
                                    <a href="{{$venture->business_facebook_url}}"><i class="small fab fa-facebook"></i></a>
                                </div>
                            @endif

                            @if($venture->business_twitter_url != null)
                                <div class="col s2">
                                    <a href="{{$venture->business_twitter_url}}"><i
                                            class="small fab fa-twitter"></i></a>
                                </div>
                            @endif

                            @if($venture->business_linkedin_url != null)
                                <div class="col s2">
                                    <a href="{{$venture->business_linkedin_url}}"><i

                                            class="small fab fa-linkedin"></i></a>
                                </div>
                            @endif
                            @if($venture->business_instagram_url != null)
                                <div class="col s2">
                                    <a href="{{$venture->business_instagram_url}}"><i
                                            class="small fab fa-instagram"></i></a>
                                </div>
                            @endif
                            @if($venture->company_website != null)
                                <div class="col s2">
                                    <a href="{{$venture->company_website}}"><i class="small material-icons dp48">language</i></a>
                                </div>
                            @endif
                            @if($venture->contact_number != null)
                                <div class="col s2">
                                    <a href="{{$venture->contact_number}}" href="#modal20"><a
                                            class="waves-effect waves-light  modal-trigger" href="#modal20"><i
                                                class="small material-icons dp48">local_phone</i></a></a>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
                <br>
                </div>

                <!-- Modal Trigger -->

                <!-- Modal Structure -->
                <div id="modal20" class="modal" style="width:20%;margin-top: 50vh;">
                    <div class="modal-content">
                        <p>Phone number:{{$venture->contact_number}}</p>
                    </div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Ok</a>
                    </div>
                </div>
        </div>


        <!--Team profile-->

        <div class="col s6" style="margin-left: -5em; margin-top: 11.5em">
            @if($venture_upload->product_shot_url != null)
            <div class="z-depth-3 teamprofile" style="height: 250px; margin-top: 0.5em">
                <img class="imglogo" src="/storage/{{isset($venture_upload->product_shot_url)?$venture_upload->product_shot_url:''}}">
            </div>
            @endif


            <div class="card teamprofile">
                <div>
                    <br>
                    <h6 class="oswaldbody" style="margin-left: 2em">TEAM PROFILE</h6>
                    @foreach($venture->incubatee as $incubatee)
                        <div class="row" style="margin-left: 1.5em">
                            <div class="col s5">
                                <img class="imgprofile"
                                     src="/storage/{{isset($incubatee->uploads->profile_picture_url)?$incubatee->uploads->profile_picture_url:'images/ProfilePictures/NoProfilePic.jpg'}}"
                                />
                            </div>
                            <div class="col s7">
                                <div class="row">
                                    <h6 class="oswaldbody" style="margin-left: 3em;">{{$incubatee->user->name}} {{$incubatee->user->surname}}</h6>
                                    <br>
                                    <h6 class="robotobody" style="margin-left: 4.5em;">{{$incubatee->user->email}}</h6>
                                    <br>
                                    @if($incubatee->uploads->founder_video_shot != null)
                                    <div class="row">
                                        <div style="margin-left: 3em;">
                                            <div class="col s7">
                                                <a class="waves-effect waves-light btn modal-trigger" href="{{$incubatee->uploads->founder_video_shot}}" target="_blank"
                                                   style="width:100%;background-color: rgba(0, 121, 52, 1)">FOUNDERS VIDEO</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @if($incubatee->personal_facebook_url != null)
                                        <div class="col s2" style="margin-left: 3em;">
                                            <a href="{{$incubatee->personal_facebook_url}}"><i
                                                    class="small fab fa-facebook"></i></a>
                                        </div>
                                    @endif

                                    @if($incubatee->personal_twitter_url != null)
                                        <div class="col s2">
                                            <a href="{{$incubatee->personal_twitter_url}}"><i
                                                    class="small fab fa-twitter"></i></a>
                                        </div>
                                    @endif

                                    @if($incubatee->personal_linkedin_url != null)
                                        <div class="col s2">
                                            <a href="{{$incubatee->personal_linkedin_url}}"><i
                                                    class="small fab fa-linkedin"></i></a>
                                        </div>
                                    @endif

                                    @if($incubatee->personal_instagram_url != null)
                                        <div class="col s2">
                                            <a href="{{$incubatee->personal_instagram_url}}"><i
                                                    class="small fab fa-instagram"></i></a>
                                        </div>
                                    @endif
                                </div>

                            </div>
                        </div>


                        <div style="margin-left: 3em">
                            <h4 class="oswaldbody">About Me</h4>
                            <div class="item" style="padding: 20px 60px 20px 10px">
                                <p class="roboto" style="text-align: justify;
                                   text-justify: inter-word;">
                                    {{$incubatee->short_bio}}
                                </p>
                            </div>
                            <br>
                        </div>
                    @endforeach
                </div>
                <br/>
            </div>
        </div>
    </div>


{{----------------------------------------------------------------------------------------------------------------------------}}
    <!--NEW PROFILE Mobile-->
    <div class="row left-align" id="VentureMobile">
        <br>
        <br>
        <div class="row col s12">
            <h6 style="margin-left: 1em;color: black;text-transform: capitalize"><b>{{$venture->company_name}}</b></h6>
            <div class="row" style="margin-left: 1em;">
                <div class="">
                    @if($venture->smart_city_tags!= null)
                        <a class="btn disabled">{{$venture->smart_city_tags}}</a>
                    @endif
                    @if($venture->hub!= null)
                        <a class="btn disabled">{{$venture->hub}}</a>
                    @endif
                    @if($venture->stage!= null)
                        <a class="btn disabled">{{$venture->stage}}</a>
                    @endif
                </div>
            </div>
            <div class="z-depth-3">
                <img style="width: 100%;"
                     src="/storage/{{isset($venture_upload->company_logo_url)?$venture_upload->company_logo_url:''}}"
                     style="height: 50%; width: 100%;">
            </div>
            <br>
            <div class="card" style="width: 340px;margin:0 auto;border-radius: 2%;">
                <div class="" style="padding-left: 2em;padding-right: 2em;">
                    <br>
                    <h4 class="standard-header">TEAM PROFILE</h4>
                    @foreach($venture->incubatee as $incubatee)
                        <div class="row">
                            <div class="col s5">
                                <img style="width:100%; height:30%;border-radius: 50%"
                                     src="/storage/{{isset($incubatee->uploads->profile_picture_url)?$incubatee->uploads->profile_picture_url:'images/ProfilePictures/NoProfilePic.jpg'}}"/>
                                {{--<img style="width:100%; height:30%;border-radius: 50%" src="/images/ProfilePictures/Propella Entrepreneurs/_H2A1052.JPG"/>--}}
                            </div>
                            <div class="col s7">
                                <div class="row">
                                    <h6 style="margin-left: 1em;">{{$incubatee->user->name}} {{$incubatee->user->surname}}</h6>
                                    <br>
                                    @if($incubatee->uploads->founder_video_shot != null)
                                    <div class="row" style="margin-left: 1em">
                                        <div>
                                            <div class="col s7">
                                                <a class="waves-effect waves-light btn modal-trigger" href="{{$incubatee->uploads->founder_video_shot}}" target="_blank"
                                                   style="width:100%;background-color: rgba(0, 121, 52, 1)">VIDEO</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    {{--<h6 style="margin-left: 1em;"> {{$incubatee->user->email}}</h6>--}}
                                    <br>
                                    @if($incubatee->personal_facebook_url != null)
                                        <div class="col s3">
                                            <a href="{{$incubatee->personal_facebook_url}}"><i
                                                    class="small fab fa-facebook"></i></a>
                                        </div>
                                    @endif

                                    @if($incubatee->personal_twitter_url != null)
                                        <div class="col s3">
                                            <a href="{{$incubatee->personal_twitter_url}}"><i
                                                    class="small fab fa-twitter"></i></a>
                                        </div>
                                    @endif

                                    @if($incubatee->personal_linkedin_url != null)
                                        <div class="col s3">
                                            <a href="{{$incubatee->personal_linkedin_url}}"><i
                                                    class="small fab fa-linkedin"></i></a>
                                        </div>
                                    @endif
                                    @if($incubatee->personal_linkedIn_url != null)
                                        <div class="col s3">
                                            <a href="mailto:{{$incubatee->user->email}}"><i
                                                    class="small material-icons dp48">email</i></a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class=" ">
                            <h4 class="" style="font-size: 2rem;font-family: Arial;">About Me</h4>
                            <p>
                                {{$incubatee->short_bio}}
                            </p>
                        </div>
                    @endforeach
                </div>
                <br/>
            </div>
        </div>
        <div class="col s1"></div>

        <div class="col m6">
            {{--<img style="width: 100%;height: 500%" src="/images/Propella_Logo.jpg"/>--}}
            <br>
            <div class="card" style="width: 340px;margin: 0 auto;border-radius: 2%;">
                <br>
                <h4 class="standard-header" style="margin-left: 1em;margin-right: 1em;">Company Profile</h4>
                @if($venture->elevator_pitch != null)
                    <p style="margin-left: 1em;margin-right: 1em;">{{$venture->elevator_pitch}}</p>
                @endif
                <br>
                @if($venture->venture_profile_information != null)
                    <p style="margin-left: 1em;margin-right: 1em;">{{$venture->venture_profile_information}}</p>
                @endif
                 <!--Video shot-->
                @if($venture_upload->video_title != null)
                    <p style="margin-left: 2em;margin-right: 2em;"><b>{{$venture_upload->video_title}}</b></p>
                @endif
                @if($venture_upload->video_shot_url !=null)
                    <iframe style="margin-left: 1em" src="{{isset($venture_upload->video_shot_url)?$venture_upload->video_shot_url:''}}" width="300" height="200" allow="autoplay;" allowfullscreen></iframe>
                @endif
                <br>
            </div>

            <div class="row">
                <br/>
                <br/>
                @if(isset($venture_upload->infographic_url))
                    <div class="col s12">
                        <a class="waves-effect blue waves-light btn modal-trigger infographicBtn" href="#modal10"
                           style="width:100%; ">Infographic</a>
                        <!-- Modal Structure -->
                        <div id="modal10" class="modal">
                            <div class="modal-content">
                                <div class="row">
                                    <div class="col m4">
                                        <img style="width:120%; height:100%;"
                                             src="/storage/{{isset($venture_upload->infographic_url)?$venture_upload->infographic_url:''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                            </div>
                        </div>
                    </div>
                @endif
                <br>
                <br/>
                <div class="">
                    @if(isset($current_venture_ownership->ownership) or isset($current_venture_ownership->registration_number) or isset($current_venture_ownership->BBBEE_level))
                        <div class="col s4">
                            <a class="waves-effect blue waves-light btn modal-trigger" href="#modal11" href="#"
                               style="width:342%;">Ownership</a>
                            <div id="modal11" class="modal">
                                <div class="modal-content">
                                    <h6><b>{{$venture->company_name}}'s Ownership</b></h6>
                                    <p>Company
                                        Ownership: {{isset($current_venture_ownership->ownership)?$current_venture_ownership->ownership:''}}</p>
                                    <p>Company Registration
                                        Number: {{isset($current_venture_ownership->registration_number)?$current_venture_ownership->registration_number:''}}</p>
                                    <p>Company BBBEE
                                        Level: {{isset($current_venture_ownership->BBBEE_level)?$current_venture_ownership->BBBEE_level:''}}</p>
                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <br>
                <br/>
                <div class="">
                    @if(isset($venture->ventureUploads->crowdfunding_url))
                        <div class="col s12">
                            <a class="waves-effect blue waves-light btn modal-trigger" href="#modal12" href="#"
                               style="width:100%;">Crowd Funding</a>
                            <div id="modal12" class="modal">
                                <div class="modal-content">
                                    <img
                                        src="/storage/{{isset($venture_upload->crowdfunding_url)?$venture_upload->crowdfunding_url:''}}">
                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col s12">
                    <a class="waves-effect green waves-light btn modal-trigger" href="#modal13" href="#"
                       style="width:100%;">Invest in us</a>
                    <div id="modal13" class="modal">
                        <div class="modal-content">
                            <div class="row" style="width: 70%;margin-left: 2em;">
                                <form id="ContactUsForm">
                                    <div class="input-field">
                                        <input id="name" type="tel"/>
                                        <label for="name">First name</label>
                                    </div>


                                    <div class="input-field">
                                        <select>
                                            <option value="Subject" disabled selected>Subject</option>
                                            <option value="1">General Discussion</option>
                                            <option value="2">Invest</option>
                                            <option value="3">Entrepreneurs</option>
                                            <option value="4">Apply</option>
                                        </select>
                                    </div>
                                    <div class="input-field">
                                        <input id="email" type="email"/>
                                        <label for="email">Email</label>
                                    </div>

                                    <div class="input-field">
                                        <input id="Message" type="tel"/>
                                        <label for="Message">Message</label>
                                    </div>

                                    <div class="input-field">
                                        <input id="phone" type="tel"/>
                                        <label for="phone">Phone Number</label>
                                    </div>

                                    <div>
                                        <button class="btn waves-effect waves-light float-right" type="submit"
                                                name="action">Send
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                        </div>
                    </div>
                </div>


                @if(isset($venture_upload->video_shot_url))
                    <div class="col s12" style="margin-top: 1vh">
                        <a class="waves-effect blue waves-light btn modal-trigger infographicBtn" href="#modal30"
                           style="width:100%; ">Video</a>
                        <!-- Modal Structure -->
                        <div id="modal30" class="modal" style="width:100%">
                            <div class="modal-content">
                                <div class="row">
                                    <div class="col m4">
                                        <iframe style="margin-left: 1em" src="{{isset($venture_upload->video_shot_url)?$venture_upload->video_shot_url:''}}" width="300" height="200" allow="autoplay;" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>


        </div>
        <br>
        <br>
        <br>


        <div class="row">
            <div class="col s7">
                <div class="row" style="padding-left: 25px;">
                    @if($venture->business_facebook_url != null)
                        <div class="col s2">
                            <a href="{{$venture->business_facebook_url}}"><i class="small fab fa-facebook"></i></a>
                        </div>
                    @endif

                    @if($venture->business_twitter_url != null)
                        <div class="col s2">
                            <a href="{{$venture->business_twitter_url}}"><i class="small fab fa-twitter"></i></a>
                        </div>
                    @endif

                    @if($venture->business_linkedin_url != null)
                        <div class="col s2">
                            <a href="{{$venture->business_linkedin_url}}"><i class="small fab fa-linkedin"></i></a>
                        </div>
                    @endif
                    @if($venture->business_instagram_url != null)
                        <div class="col s2">
                            <a href="{{$venture->business_instagram_url}}"><i class="small fab fa-instagram"></i></a>
                        </div>
                    @endif
                    @if($venture->company_website != null)
                        <div class="col s2">
                            <a href="{{$venture->company_website}}"><i
                                    class="small material-icons dp48">language</i></a></a>
                        </div>
                    @endif
                    @if($venture->contact_number != null)
                        <div class="col s2">
                            <a href="{{$venture->contact_number}}" href="#modal20"><a
                                    class="waves-effect waves-light  modal-trigger" href="#modal20"><i
                                        class="small material-icons dp48">local_phone</i></a></a>
                        </div>
                    @endif
                </div>

            </div>
        </div>


    </div>

    </body>


    <script>
        $(document).ready(function () {
            $('.modal').modal();

        });
    </script>

@endsection
