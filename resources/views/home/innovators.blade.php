@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Innovators/Innovators.css" />
    <link rel="stylesheet" type="text/css" href="/css/Programs/main.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>

    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTWSFMC"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="section  container-fluid">
        <div class="row">
            <div class="parallax-container" id="InnovatorsTopSection">
                <div class="parallax">
                    <img src="/images/Innovators/2.jpg" id="InnovatorsTopSectionImage">
                </div>
                <h1 class="innovators-section-headers">The Innovators</h1>
                <h4 class=""id="back"></h4>
            </div>
        </div>
    </div>

    {{--@* Innovators Second Section Desktop *@--}}
    <div class="section standard-section-with-margins" id="InnovatorsSecondSectionDesktop">
        <div class="row">
            <div class="col l4 text-justify">
                <h4 class="standard-header">The Innovators</h4>
                <p class="standard-paragraphs">
                    <b>For innovators:</b> We are the catalyst for your creativity - Propella helps IT and
                    manufacturing innovators and entreprenuers to go from zero to hero.
                </p>
                <br />
                {{--<a  class="btn" href="/Home/Categories">Ventures</a>--}}
                <div class="ventureBlock col l4">
                    <h5 class="standard-paragraphs"href="/Home/Categories" style="font-size: 1rem;  margin-top: 13px;margin-left: 14px">Ventures</h5>
                </div>
            </div>


            <div class="col l2"></div>

            <div class="col l6 center-align">
                <div class="row " >
                    <div class="standard-blocks IdeaBlock col l4">
                        <h5 class="standard-block-headers"> I have an idea, what's next? </h5>
                    </div>

                    <div class="standard-blocks ProgrammesBlock col l4">
                        <h5 class="standard-block-headers"> What programmes are offered? </h5>
                    </div>

                    <div class="standard-blocks IdeaReadyBlock col l4">
                        <h5 class="standard-block-headers"> Is my idea ready to be incubated? </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="standard-blocks FundingOptionsBlock col l4">
                        <h5 class="standard-block-headers"> Are there funding options? </h5>
                    </div>

                    <div class="standard-blocks FAQBlock col l4">
                        <h5 class="standard-block-headers"> Frequently Asked Questions. </h5>
                    </div>

                    <div class="standard-blocks ApplyBlock col l4">
                        <h5 class="standard-block-headers"> How do I apply?  </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Second section mobile-->
    <div class="section standard-section-with-margins" id="InnovatorsSecondSectionMobile">
        <div class="row center-align">
            <h4 class="standard-header">The Innovators</h4>
            <p class="standard-paragraphs">
                <b>For innovators:</b> We are the catalyst for your creativity - Propella helps IT and
                manufacturing innovators and entreprenuers to go from zero to hero.
            </p>
            <br />
            <div class="ventureBlock col l4 ">
                <h5 class="standard-paragraphs"href="/Home/Categories" style="font-size: 1rem;  margin-top: 13px;margin-left: 6px">Ventures</h5>
            </div>
        </div>

        <div class="row center-align">
            <div class="row">
                <div class="standard-blocks col s6 IdeaBlock">
                    <h5 class="standard-block-headers"> I have an idea, what's next? </h5>
                </div>

                <div class="standard-blocks col s6 ProgrammesBlock">
                    <h5 class="standard-block-headers"> What programmes are offered? </h5>
                </div>
            </div>

            <div class="row">
                <div class="standard-blocks col s6 IdeaReadyBlock">
                    <h5 class="standard-block-headers"> Is my idea ready to be incubated? </h5>
                </div>

                <div class="standard-blocks col s6 FundingOptionsBlock">
                    <h5 class="standard-block-headers"> Are there funding options? </h5>
                </div>
            </div>

            <div class="row">
                <div class="standard-blocks col s6 FAQBlock">
                    <h5 class="standard-block-headers"> Frequently Asked Questions. </h5>
                </div>

                <div class="standard-blocks col s6 ApplyBlock">
                    <h5 class="standard-block-headers"> How do I apply?  </h5>
                </div>
            </div>
        </div>
    </div>

<!--Image-->
    <div class=""id="idea"></div>
    <div id="desktop">
    <br />
    <br />
    <br />
    <br />

    <div class="section  container-fluid">
        <div class="row">
            <div class="parallax-container" id="InnovatorsThirdSection">
                <div class="parallax">
                    <img src="/images/Propella_resized_images/Lean Coffee ladies1.jpg" id="InnovatorsThirdSectionImage">
                </div>
                <h1 class="innovators-section-header2"> So you have an idea? </h1>
            </div>
        </div>
    </div>
    </div>

    <div id="mobile">
        <div class="section  container-fluid">
            <div class="row">
                <div id="InnovatorsThirdSection">
                    <div>
                        <img src="/images/Propella_resized_images/Lean Coffee ladies1.jpg" id="InnovatorsThirdSectionImage">
                    </div>
                    <h1 class="innovators-section-header2"> So you have an idea? </h1>
                </div>
            </div>
        </div>
    </div>

    <!--Third Section Desktop-->
    <div class="section standard-section-with-margins" id="InnovatorsThirdSectionDesktop">
        <div class="row center-align">
            <h4 class="standard-header">Congrats! You're in the right place!</h4>
            <p class="standard-paragraphs">Propella can help you to take your product or IT solution to the market.</p>

            <p class="standard-paragraphs">
                If your innovation can improve the lives of people living in cities around the world,
                we'd like to engage further but first we need to know a bit more...
            </p>
        </div>


        <div id="IdeaFirstOptionRowDesktop">
            <div class="row ">

                <div class="col m4 center-align IndustrialHubBlock1">
                    <h2 class="standard-header"style="margin-top:20vh;"> What type of idea is it? </h2>
                </div>
                <div class="col m4 ICTBlock">
                    <h5 class="standard-block-headers"style="padding-left:55%;margin-top:25vh;text-shadow: 5px 3px 1px #000000; "> Information Communication Technology? </h5>
                </div>

                <div class="col m4 INDBlock">
                    <h5 class="standard-block-headers"style="padding-left:10%;margin-top:30vh;text-shadow: 5px 3px 1px #000000; "> Industrial? </h5>
                </div>
            </div>
        </div>

        <div id="ICTIdeaFirstOptionRowDesktop" hidden>
            <div class="row">
                <div class="col l3 center-align HowAdvancedBlock">
                    <h2 class="standard-header" style="margin-top:16vh;"> How advanced is the idea? </h2>
                </div>

                <div class="col l3 ICTIdeaBlock">
                    <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have a Tech idea </h5>
                </div>

                <div class="col l3 ICTDesignBlock">
                    <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have a Wire Frame </h5>
                </div>

                <div class="col l3 ICTPrototypeBlock">
                    <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have a functioning app, website, platform or device. </h5>
                </div>
            </div>
            <div class="row center-align ICTFirstCancelBlock">
                <h4> Not sure if this is you? </h4>
            </div>
        </div>

        <div id="ICTFinalOptionRowDesktop" hidden>
            <div class="row">
                <div class="col l5 center-align">
                    <img src="/images/Innovators/IdeaImage.jpg" style="width: 100%; height: 40vh;" />
                </div>

                <div class="col l7 ICTOptionsBlock">
                    <h4>We host Technology Innovation. Futured by BCX and you could qualify.</h4>
                    <p class="standard-paragraphs"> Click below to apply. </p>
                    <a href="/Application-Process/question-category">Apply</a>
                </div>
            </div>

            <div class="row center-align ICTSecondCancelBlock">
                <h4> Not sure if this is you? </h4>
            </div>
        </div>

        <div id="INDIdeaFirstOptionRowDesktop" hidden>
            <div class="row">
                <div class="col l3 center-align HowAdvancedIND">
                    <h2 class="standard-header" style="margin-top:16vh;"> How advanced is the idea? </h2>
                </div>

                <div class="col l3 INDIdeaBlock">
                    <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have an idea. </h5>
                </div>

                <div class="col l3 INDDesignBlock">
                    <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have a design. </h5>
                </div>

                <div class="col l3 INDPrototypeBlock">
                    <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have a prototype. </h5>
                </div>
            </div>

            <div class="row center-align INDFirstCancelBlock">
                <h4> Not sure if this is you? </h4>
            </div>
        </div>

        <div id="INDTechIdeaRowDesktop" hidden>
            <div class="row">
                <div class="col l5 center-align">
                    <img src="/images/Innovators/INDIdea.jpg" style="width: 100%; height: 40vh" />
                </div>

                <div class="col l7 INDTechBlock">
                    <h4>Please keep up the great work!</h4>
                    <p class="standard-paragraphs"> Contact us as soon as you have a prototype.</p>
                </div>
            </div>

            <div class="row center-align INDSecondCancelBlock">
                <h4> Not sure if this is you? </h4>
            </div>
        </div>

        <div id="INDDesignRowDesktop" hidden>
            <div class="row">
                <div class="col l5 center-align">
                    <img src="/images/Innovators/1.2 Ind Design.jpg" style="width: 100%; height: 40vh" />
                </div>

                <div class="col l7 DesignBlock">
                    <h4>This is great news!</h4>
                    <p class="standard-paragraphs"> Please give us a call.</p>
                </div>
            </div>

            <div class="row center-align INDSecondCancelBlock1">
                <h4> Not sure if this is you? </h4>
            </div>
        </div>

        <div id="INDPrototypeRowDesktop" hidden>
            <div class="row">
                <div class="col l5 center-align">
                    <img src="/images/Innovators/1.3 Ind Prototyype.jpg" style="width: 100%; height: 40vh" />
                </div>

                <div class="col l7 PrototypeBlock">
                    <h4>Awesome!</h4>
                    <p class="standard-paragraphs"> Please apply here.</p>
                    <a href="/Application-Process/question-category">Apply</a>
                </div>
            </div>

            <div class="row center-align INDSecondCancelBlock2">
                <h4> Not sure if this is you? </h4>
            </div>
        </div>

    </div>


    <!--Third Section Mobile-->
    <div class="section standard-section-with-margins" id="InnovatorsThirdSectionMobile">
        <div class="row center-align">
            <h4 class="standard-header">Congrats! You're in the right place!</h4>
            <p class="standard-paragraphs">Propella can help you to take your product or IT solution to the market.</p>
            <br />
            <p class="standard-paragraphs">
                If your innovation can improve the lives of people living in cities around the world,
                we'd like to engage further but first we need to know a bit more...
            </p>
        </div>

        <div id="IdeaFirstOptionRowMobile">
            <div class="row" id="ICTHubBlock">
                <h1 class="standard-block-headers center-align" > What type of idea is it? </h1>
            </div>

            <div class="row ICTBlockMobile">
                <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> Information Communication Technology? </h5>
            </div>


            <div class="row INDBlockMobile">
                <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> Industrial? </h5>
            </div>
        </div>
        <!--ICT-->
        <div id="ICTIdeaFirstOptionRowMobile" hidden>
            <div class="row center-align" id="HowAdvancedBlock">
                <h2 class="standard-block-headers center-align"> How advanced is the idea? </h2>
            </div>

            <div class="row ICTIdeaBlockMobile">
                <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have a Tech idea </h5>
            </div>

            <div class="row ICTDesignBlockMobile">
                <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have a Wire Frame </h5>
            </div>

            <div class="row ICTPrototypeBlockMobile">
                <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have a functioning app, website, platform or device. </h5>
            </div>

            <div class="row center-align CancelBlock1">
                <h4 class="standard-block-headers center-align"> Not sure if this is you? </h4>
            </div>
        </div>

        <div id="ICTFinalOptionRowMobile" hidden>
            <div class="idea1 row">
                <h4 class="standard-block-headers">We host Technology Innovation. Futured by BCX and you could qualify.</h4>
                <p class="standard-paragraphs"> Click below to apply. </p>
                <a href="/Application-Process/question-category">Apply</a>
            </div>

            <div class="row center-align CancelBlock">
                <h4 class="standard-block-headers"> Not sure if this is you? </h4>
            </div>
        </div>

        <!--INDUSTRIAL-->
        <div id="INDIdeaFirstOptionRowMobile" hidden>
            <div class="row center-align" id="INDAdvancedBlock">
                <h2 class="standard-block-headers center-align"> How advanced is the idea? </h2>
            </div>

            <div class="row INDIdeaBlockMobile">
                <h5 class="standard-block-headers"style="text-shadow: 5px 3px 1px #000000;"> I have an idea. </h5>
            </div>

            <div class="row INDDesignBlockMobile">
                <h5 class="standard-block-headers"style="color: black;"> <b>I have a design. </b></h5>
            </div>

            <div class="row INDPrototypeBlockMobile">
                <h5 class="standard-block-headers" style="text-shadow: 5px 3px 1px #000000;"> I have a prototype. </h5>
            </div>

            <div class="row center-align CancelBlockInd">
                <h4 class="standard-block-headers center-align"> Not sure if this is you? </h4>
            </div>
        </div>

        <div id="INDTechIdeaRowMobile" hidden>
            <div class="row center-align">
                <h2 class="standard-header"> You have an Idea? </h2>
            </div>
            <div class="row" id="INDTechBlock">
                <h4 class="standard-block-headers center-align">Please keep up the great work!</h4>
                <p class="standard-paragraphs"> Contact us as soon as you have a prototype.</p>
            </div>

            <div class="row center-align CancelBlockIND0">
                <h4 class="standard-block-headers center-align"> Not sure if this is you? </h4>
            </div>
        </div>

        <div id="INDDesignRowMobile" hidden>
            <div class="row center-align">
                <h2 class="standard-header"> You have a design? </h2>
            </div>

            <div class="row" id="DesignBlock">
                <h4  class="standard-block-headers center-align">This is great news!</h4>
                <p  class="standard-block-headers center-align"> Please give us a call.</p>
            </div>

            <div class="row center-align CancelBlockIND2">
                <h4 class="standard-block-headers center-align"> Not sure if this is you? </h4>
            </div>
        </div>

        <div id="INDPrototypeRowMobile" hidden>
            <div class="row center-align">
                <h2 class="standard-header"> You have a prototype? </h2>
            </div>

            <div class="row" id="PrototypeBlock">
                <h4  class="standard-block-headers ">Awesome!</h4>
                <p class="standard-paragraphs">Please apply here.</p>
                <a href="/Application-Process/question-category">Apply</a>
            </div>

            <div class="row center-align  CancelBlockINDM3">
                <h4 class="standard-block-headers center-align"> Not sure if this is you? </h4>
            </div>
        </div>

    </div>

<!--Image-->
    <div class=""id="ideaready"></div>
    <div id="desktop">
    <br />
    <br />
    <br />
    <br />

    <div class="section  container-fluid">
        <div class="row">
            <div class="parallax-container" id="ProgrammesTopSection1">
                <div class="parallax">
                    <img  src="/images/Propella_resized_images/Dev Team1.jpg" id="topimage1">
                </div>
                <h1 class="innovators-section-header3"style="text-shadow: 5px 3px 1px #000000;"> Ideas Propella looks for... </h1>
            </div>
        </div>
    </div>
    </div>

    <div id="mobile">
        <div class="section  container-fluid">
            <div class="row">
                <div id="ProgrammesTopSection1">
                    <div>
                        <img  src="/images/Propella_resized_images/Dev Team1.jpg" id="topimage1">
                    </div>
                    <h1 class="innovators-section-header3"style="text-shadow: 5px 3px 1px #000000;"> Ideas Propella looks for... </h1>
                </div>
            </div>
        </div>
    </div>

    <!--Fourth Section Desktop-->
    <div id="InnovatorsFourthSectionDesktop" class="section standard-section-with-margins ">
        <div class="row">
            <div class="col l6">
                <ul>
                    <li>We ideally work with businesses or business ideas that have been tested in the market and are:</li>
                    <li>• Disruptive (SMART, out of the ordinary thinking, unique, innovative)</li>
                    <li>• Appropriate</li>
                    <li>• Scalable</li>
                    <li>• Sustainable</li>
                    <li>• Create jobs or have the ability to create jobs</li>
                    <li>• Based in Nelson Mandela Bay</li>
                </ul>
            </div>

            <div class="col l6">
                <p>We look into the following sectors : </p>
                <ul>
                    <li>•  Renewable Energy</li>
                    <li>• Energy Efficiency</li>
                    <li>• Circular Economy</li>
                    <li>• Advanced Manufacturing</li>
                    <li>• Maritime</li>
                    <li>• Internet of Things (IoT) / Smart City Solutions</li>
                </ul>
            </div>
        </div>


        <!--Quiz Section-->
        <div id="QuizImage">

        </div>

        <div class="center-align">
            {{--<a class="waves-effect waves-light btn modal-trigger"id="ReadyCheckBtn" href="#modal1">Ready to take the Quiz?</a>--}}
            <button class="btn waves-light modal-trigger blue" id="ReadyCheckBtn" href="#modal1">Ready to take the Quiz?</button>
        </div>
        <!--Quiz-->

        <div id="modal1" class="modal">
            <div class="modal-content">
                <div id="QuestionContent" hidden>
                    <div class="row center-align white-text">
                        <div class="col m12 QuestionOneDiv"style="background-color:rgba(0, 121, 52, 1)">
                            <p>Have you been in business before (even if it was selling cookies)?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionOneBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionOneBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionTwoDiv "style="background-color: rgba(0, 47, 95, 1)" hidden>
                            <p>Is your idea or product ICT smart?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionTwoBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionTwoBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionThreeDiv"style="background-color: #454242" hidden>
                            <p>Do you have a working prototype?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionThreeBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionThreeBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionFourDiv"style="background-color: rgba(0, 121, 52, 1)" hidden>
                            <p>Have you tested the idea or product in the market?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionFourBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionFourBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionFiveDiv"style="background-color: rgba(0, 47, 95, 1)" hidden>
                            <p>Will it stand out from the crowd?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionFiveBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionFiveBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionSixDiv"style="background-color: #454242" hidden>
                            <p>Will it improve the lives of those living in towns and cities?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionSixBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionSixBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionSevenDiv"style="background-color: rgba(0, 121, 52, 1)" hidden>
                            <p>Can it be protected by copyright or patent?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionSevenBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionSevenBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionEightDiv"style="background-color: rgba(0, 47, 95, 1)" hidden>
                            <p>Do you work well with others?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionEightBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionEightBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionNineDiv"style="background-color: #454242" hidden>
                            <p>Do you listen to advice?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionNineBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionNineBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionTenDiv"style="background-color: rgba(0, 121, 52, 1)" hidden>
                            <p>Are you ready to work long hours?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn QuestionTenBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn QuestionTenBtn  white black-text">No</button>
                                </div>
                            </div>
                        </div>
                        <div class="col m12 QuestionElevenDiv"style="background-color: rgba(0, 47, 95, 1)" hidden>
                            <p>Are you ready to fly through the storms that face all innovators?</p>
                            <div class="row">
                                <div class="col m6">
                                    <button class="btn scoreBtn finalBtn QuestionElevenBtn white black-text">Yes</button>
                                </div>
                                <div class="col m6">
                                    <button class="btn finalBtn QuestionElevenBtn white black-text">No</button>
                                </div>
                            </div>
                        </div>

                        <div class="col m12 QuestionTwelveDiv"style="background-color: #454242" hidden>
                            <a class="" href="/Application-Process/question-category"><p id="HighParagraph" style=" color: white" hidden>
                            That’s amazing, it’s time to have a chat.  Please tell us more
                                <i class=""style="color: red">Click here to apply</i>
                                </p></a>
                            {{--@*<a style="margin-left:50%" class="black-text" href="#ReadyCheckBtn"><i class="material-icons large scroll-icon">arrow_drop_up_circle</i></a>*@--}}
                            {{--@*<button style="margin-left:50%" class="black-text" href="#ReadyCheckBtn"><i class="material-icons large scroll-icon">arrow_drop_up_circle</i>></button>*@--}}
                            {{--</p>--}}
                            <p id="AverageParagraph" hidden>Promising, but you may need to refine your ideas and approach a little more before sharing your idea with us. </p>
                            <p id="LowParagraph" hidden>Congratulations – You have made a great start on the journey to be a smart entrepreneur. However, there’s a way to go before you’re likely to be ready for incubation. </p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Ok</a>
            </div>
        </div>
    </div>


<!--Fourth Section Mobile-->
    <div id="InnovatorsFourthSectionMobile" class="section standard-section-with-margins">
        <div class="row center-align">
            <ul>
                <li>We ideally work with businesses or business ideas that have been tested in the market and are:</li>
                <li>• Disruptive (SMART, out of the ordinary thinking, unique, innovative)</li>
                <li>• Appropriate</li>
                <li>• Scalable</li>
                <li>• Sustainable</li>
                <li>• Create jobs or have the ability to create jobs</li>
                <li>• Based in Nelson Mandela Bay</li>
            </ul>
        </div>
        <div class="row center-align">
            <p>We look into the following sectors : </p>
            <ul>
                <li>• Renewable Energy</li>
                <li>• Energy Efficiency</li>
                <li>• Circular Economy</li>
                <li>• Advanced Manufacturing</li>
                <li>• Maritime</li>
                <li>• Internet of Things (IoT) / Smart City Solutions</li>
            </ul>
        </div>
    </div>

    {{--@* Innovators Fifth Section: Image Only *@--}}
    <div class=""id="programs"></div>
    <div id="desktop">
    <br />
    <br />
    <br />
    <br />
    <div class="section  container-fluid">
        <div class="row">
            <div class="parallax-container" id="InnovatorsFifthSection">
                <div class="parallax">
                    <img src="/images/Innovators-Resized-Images/Programs.jpg" id="InnovatorsFifthSectionImage">
                </div>
                <h1 class="innovators-section-header4"> Programmes </h1>
            </div>
        </div>
    </div>
    </div>

    <div id="mobile">
        <div class="section  container-fluid">
            <div class="row">
                <div id="InnovatorsFifthSection">
                    <div>
                        <img src="/images/Innovators-Resized-Images/Programs.jpg" id="InnovatorsFifthSectionImage">
                    </div>
                    <h1 class="innovators-section-header4"> Programmes </h1>
                </div>
            </div>
        </div>
    </div>

    <!--Fifth Section Desktop-->
    <div id="InnovatorsFifthSectionDesktop" class="section standard-section-with-margins">
        <div id="ProgrammesFirstRow">
            <div class="row">
                <div class="col l4 standard-blocks center-align FuturemakersBlock1">
                    <h4 class="standard-block-headers"style="margin-top:15vh;"> Which programme would you like to know more about? </h4>
                </div>

                <div class="col l4 ICTBlock1">
                    <h5 class="standard-block-headers" style="padding-left:28%;margin-top:18vh;text-shadow: 5px 3px 1px #000000; "><b>Technology Innovation Programme</b></h5>

                </div>
                <div class="col l4 INDBlock1">
                    <h5 class="standard-block-headers" style="padding-right:38%;margin-top:18vh;text-shadow: 5px 3px 1px #000000; "><b>Propella Industrial Programme</b></h5>
                </div>

            </div>
        </div>

        <div id="ProgrammesICTFirstRow" hidden>
            <div class="row">
                <div class="col l3 standard-blocks center-align FuturemakersBlock">
                    <h4 class="standard-block-headers"> Technology Innovation Futured by BCX </h4>
                </div>

                <div class="col l3 standard-blocks center-align FuturemakersProgrammeInfoBlock">
                    <h4 class="standard-block-headers"> Tell me more about the programme </h4>
                </div>

                <div class="col l3 standard-blocks center-align FuturemakersContentBlock">
                    <h4 class="standard-block-headers"> What content is covered? </h4>
                </div>

                <div class="col l3 center-align FuturemakersVenturesBlock">
                    <h4 class="standard-block-headers"> Who is currently on the programme? </h4>
                </div>
            </div>
        </div>

        <div id="ProgrammesINDFirstRow" hidden>
            <div class="row">
                <div class="col l3 standard-blocks center-align FuturemakersBlock">
                    <h4 class="standard-block-headers"> Isuzu Industrial Hub </h4>
                </div>

                <div class="col l3 standard-blocks center-align ISUZUProgrammeInfoBlock">
                    <h4 class="standard-block-headers"> Tell me more about the programme </h4>
                </div>

                <div class="col l3 standard-blocks center-align ISUZUContentBlock">
                    <h4 class="standard-block-headers"> What content is covered? </h4>
                </div>

                <div class="col l3 standard-blocks center-align ISUZUVenturesBlock">
                    <h4 class="standard-block-headers"> Who is currently on the programme? </h4>
                </div>
            </div>
        </div>
    </div>

    <!--Fifth Section Mobile-->
    <div id="InnovatorsFifthSectionMobile" class="section standard-section-with-margins">
        <div id="ProgrammesFirstRow">
            <div class="row center-align">
                <h4 class="standard-header"> Which programme would you like to know more about? </h4>
            </div>

            <div class="row center-align FuturemakersBlock">
                <h5 class="standard-block-headers">Technology Innovation Programme Futured by BCX </h5>
            </div>


            <div class="row center-align IndustrialHubBlock">
                <h5 class="standard-block-headers">Propella Industrial Programme</h5>
            </div>
        </div>

        <div id="ProgrammesICTFirstRow" hidden>
            <div class="row center-align FuturemakersBlock">
                <h4 class="standard-block-headers"> Technology Innovation Programme Futured by BCX </h4>
            </div>

            <div class="row center-align FuturemakersProgrammeInfoBlock">
                <h6 class="standard-block-headers"> Tell me more about the programme </h6>
            </div>

            <div class="row center-align FuturemakersContentBlock">
                <h6 class="standard-block-headers"> What content is covered? </h6>
            </div>

            <div class="row center-align FuturemakersVenturesBlock">
                <h6 class="standard-block-headers"> Who is currently on the programme? </h6>
            </div>
        </div>

        <div id="ProgrammesINDFirstRow" hidden>
            <div class="row center-align FuturemakersBlock">
                <h6 class="standard-block-headers"> Propella Industrial Programme </h6>
            </div>

            <div class="row center-align ISUZUProgrammeInfoBlock">
                <h6 class="standard-block-headers"> Tell me more about the programme </h6>
            </div>

            <div class="row center-align ISUZUContentBlock">
                <h6 class="standard-block-headers"> What content is covered? </h6>
            </div>

            <div class="row center-align ISUZUVenturesBlock">
                <h6 class="standard-block-headers"> Who is currently on the programme? </h6>
            </div>
        </div>

    </div>

    {{--@* Innovators Sixth Section: Image Only *@--}}
    <div class=""id="hub"></div>
    <div id="desktop">
    <br />
    <br />
    <br />
    <br />
    <div class="section  container-fluid">
        <div class="row">
            <div class="parallax-container" id="InnovatorsSixthSection">
                <div class="parallax">
                    <img src="/images/Innovators-Resized-Images/Futuremakers.jpg" id="InnovatorsSixthSectionImage">
                </div>
                <h1 class="innovators-section-header5"> ICT Hub </h1>
            </div>
        </div>
    </div>
    </div>

    <div id="mobile">
        <div class="section  container-fluid">
            <div class="row">
                <div id="InnovatorsSixthSection">
                    <div>
                        <img src="/images/Innovators-Resized-Images/Futuremakers.jpg" id="InnovatorsSixthSectionImage">
                    </div>
                    <h1 class="innovators-section-header5"> ICT Hub </h1>
                </div>
            </div>
        </div>
    </div>

    {{--@* Innovators Sixth Section Desktop *@--}}
    <div class="section standard-section-with-margins" id="InnovatorsSixthSectionDesktop">
        <div class="row">
            <div class="col l4">
                <div class="row">
                    <div class="col l6">
                        <img src="/images/Innovators-resized/300.JPG" style="width: 100%; height: 100%;" />
                    </div>
                    <div class="col l6">
                        <img src="/images/Innovators-resized/350.JPG" style="width: 100%; height: 100%;" />
                    </div>
                </div>
                <div class="row">
                    <div class="col l6">
                        <img src="/images/Innovators-resized/FM200.JPG" style="width: 100%; height: 100%" />

                    </div>
                    <div class="col l6">
                        <img src="/images/Innovators-resized/475.JPG" style="width: 100%; height: 100%" />

                    </div>
                </div>
                <div class="row">
                    <div class="col l6">
                        <img src="/images/Innovators-resized/525.JPG" style="width: 100%; height: 100%" />
                    </div>
                    <div class="col l6">
                        <img src="/images/Innovators-resized/450.JPG" style="width: 100%; height: 100%" />


                    </div>
                </div>
            </div>
            <div class="col l1">

            </div>
            <div class="col l7">
                <p class="standard-paragraphs">
                <h4>Technology Innovation Programme</h4>
                The Technology Innovation Programme is an intensive "from concept to market" programme run by Propella Business Incubator for ICT entrepreneurs.
                The programme is completely virtual and open to entrepreneurs throughout South Africa.
                </p>
                <ul>
                    <li>•	You need a great idea and some determination</li>
                    <li>•	The idea can be an app, software, service, or an IT application</li>
                    <li>•	Idea must fit into the scope of ICT</li>
                    <li>•	Top 100 applications are selected to take part in a 2-day Virtual ICT Bootcamp</li>
                    <li>•	No cost or fees to participate in the Virtual ICT Bootcamp</li>
                    <li>•	The Propella Virtual ICT Bootcamp is an intensive full day programme over a 2-day period</li>
                    <li>•	After the Virtual ICT Bootcamp submit a pitch video and convince the panel that your tech idea has potential to turn into a viable startup to progress to the next stage</li>
                    <li>•	You will be expected to invest at least eight hours a week in Propella business development workshops, and even longer doing your research and groundwork.</li>
                </ul>

                <p class="standard-paragraphs">
                    Be sure to bookmark this page to return regularly to apply for the next Technology Innovation Programme.
                    Applications open at the start of each year, so be sure to keep visiting the site for dates.
                </p>
                <br />
                <p style="font-style: italic;"><a href="#back">Go back</a></p>

            </div>
        </div>
    </div>


    {{--@* Innovators Sixth Section Mobile *@--}}
    <div class="section standard-section-with-margins" id="InnovatorsSixthSectionMobile">
        <div class="row">
            <div class="col 12">
                <div class="row center-align">
                        <img src="/images/Innovators-resized/300.JPG" style="width: 40%; height: 50%;" />
                        <img src="/images/Innovators-resized/350.JPG" style="width: 40%; height: 50%;" />
                </div>
                <div class="row center-align">
                        <img src="/images/Innovators-resized/FM200.JPG" style="width: 40%; height: 50%" />
                        <img src="/images/Innovators-resized/475.JPG" style="width: 40%; height: 50%" />
                </div>
                <div class="row center-align">
                        <img src="/images/Innovators-resized/525.JPG" style="width: 40%; height: 50%" />
                        <img src="/images/Innovators-resized/450.JPG" style="width: 40%; height: 550%" />
                </div>
            </div>

            </div>
            <div class="col l1">

            </div>
            <div class="col l7 center-align">
                <p class="standard-paragraphs">
                <h4 style="font-size: 2rem;">Technology Innovation Programme</h4>
                The Technology Innovation Programme is an intensive "from concept to market" programme run by Propella Business Incubator for ICT entrepreneurs.
                The programme is completely virtual and open to entrepreneurs throughout South Africa.
                </p>
                <p>To successfully take part in the ICT Programme take note of the tips below:</p>
                <ul>
                    <li>• You need a great idea and some determination</li>
                    <li>• An app, a service or an IT application that no-one else has thought of yet or requires help to commercialise and scale</li>
                    <li>• Idea must fit into the operations of a Smart City or Community, such as Industry 4.0, IoT, etc.</li>
                    <li>• Approximately 100 ventures are selected to attend a three day Stage 1 “Bootcamp”</li>
                    <li>• No cost to participate</li>
                    <li>• But there are also no free lunches – you will be expected to invest at least eight hours a week in Propella workshops, and even longer doing your research and groundwork. </li>
                    <li>• Convince the panel that your idea has potential to turn into a business to become one of the 10 ventures who progress to Stage 2</li>
                    <li>• Stage 2 is eight weeks on a business incubation journey to validate and prove that it’s a viable opportunity.</li>
                    <li>• Crack the nod for Stage 3 for an additional three months of business acceleration to obtain your first revenue paying customer.  </li>
                </ul>


                <p class="standard-paragraphs">
                    This is the chance you've been looking for to make your big idea a reality.
                </p>

                <p class="standard-paragraphs">
                    Be sure to bookmark this page and to return regularly to apply for the next ICT adventure.
                    Applications open every few months, so you need to keep visiting the site for dates.
                </p>
            </div>
        </div>
    </div>

    <!---------Code Below Technology Inovation Content----->
    <!---------DESKTOP----->
        <div>
            <div id="desktop">
                <div class="section container-fluid">
                    <div class="row">
                        <div class="parallax-container" id="ProgrammesTopSection1">
                            <div class="parallax">
                                <img src="/images/Programmes Images/2.3 Programmes.jpg" id="topimage1">
                            </div>
                            <h2 class="programs-section-headers"style="text-shadow: 5px 3px 1px #000000;">Technology Innovation Content</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ProgramsStagesDesktop">
                <div class="row left-align" id="ButtonStage0">
                    <div class="">
                        <h5 class="standard-block-headers hoverable" id="Zero " style="margin-top:05px; color:#ffffff;font-size:large;text-align:left">Stage 1</h5>
                    </div>
                </div>
                <!---Infor on Stage0 Desktop-->

                <div class="standard-section-with-margins">
                    <div class="row" id="Stage0RowDesk">
                        <div class="col s8">
                                <div class="card white darken-1 hoverable" style="margin-top: 08px">
                                    <div class="card-content black-text">
                                        <h4>STAGE 1</h4>
                                        <p>
                                            Stage 1 is Bootcamp. This is the ideation phase where the Entrepreneur joins
                                            with an idea. Where the Entrepreneur decides if Incubation is for them by
                                            orientation and a series of workshops covering the ideation phase.
                                            Progression to Stage 2 is via a panel selection.
                                        </p>
                                    </div>
                                </div>
                            </div>

                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 20 (1).JPG"
                                 style="width: 140%; height: 100%; margin-top: 02em"/>
                            <br>
                        </div>
                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 20 (2).JPG"
                                 style="width: 140%; height: 100%; margin-top: 02em; margin-left: 04em"/>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="standard-section-with-margins">
                    <div class="row" id="Stage0RowDesk">
                        <div class="col s8">
                            <div class="card white darken-1 hoverable" style="margin-top: 08px">
                                    <div class="card-content black-text">
                                        <h4>STAGE 2</h4>
                                        <p>
                                            Is referred to as Pre-Incubation. At this stage, the entrepreneur will have completed the ideation process or entered the programme with market research that proves a well-defined business problem and solution and (or) product proof of concept in development and (or) pre-revenue stage. The purpose of Stage 2 is to validate the business, proving it to be a viable opportunity.
                                        </p>
                                    </div>
                                </div>
                        </div>
                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 40.JPG" style="width: 140%; height: 100%; margin-bottom: 01em" />
                        </div>
                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 50.JPG" style="width: 140%; height: 100%; margin-left: 04em"/><br>
                            <img src="/images/FutureMarkers-resized-images/FM 51.JPG" style="width: 140%; height: 110%; margin-left: 04em"/>
                        </div>
                        <div class="col s2" id="stage1Zoom">
                        </div>
                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 52.JPG" style="width: 140%; height: 100%; margin-top: 01em" />
                        </div>
                    </div>

            </div>

                <div class="standard-section-with-margins">
                    <div class="row" id="Stage0RowDesk">
                        <div class="RowMargins">
                            <div class="col s8">
                                <div class="card white darken-1 hoverable" style="margin-top: 08px">
                                    <div class="card-content black-text">
                                        <h4>STAGE 3</h4>
                                        <p>
                                            Minimum Viable Product (MVP) stage.
                                            The entrepreneur has completed their business registration and corporate identity and can now proceed on developing/building the most basic form of their product or service
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 53.JPG" style="width: 140%; height: 100%; margin-bottom: 01em" />
                        </div>
                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 54.JPG" style="width: 140%; height: 100%; margin-left: 04em" />
                            <img src="/images/FutureMarkers-resized-images/FM 55.JPG" style="width: 140%; height: 110%; margin-left: 04em" />
                        </div>
                            <div class="col s2" id="stage1Zoom">
                            </div>
                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 60.JPG" style="width: 140%; height: 100%; margin-top: 01em" />
                        </div>
                    </div>
                </div>


                    <div class="standard-section-with-margins">
                        <div class="row" id="Stage0RowDesk">
                            <div class="RowMargins">
                                <div class="col s8">
                                    <div class="card white darken-1 hoverable" style="margin-top: 08px">
                                        <div class="card-content black-text">
                                            <h4>STAGE 4</h4>
                                            <p>
                                                Revenue generation stage. At this stage the Entrepreneur has completed their market research, their MVP and are in the process of securing their first customer.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="col s2" id="stage1Zoom">
                                    <img src="/images/FutureMarkers-resized-images/FM 65.JPG" style="width: 140%; height: 100%; margin-top: 02em" />
                                </div>
                                <div class="col s2" id="stage1Zoom">
                                    <img src="/images/FutureMarkers-resized-images/FM 70.JPG" style="width: 140%; height: 100%; margin-top: 02em; margin-left: 04em" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="standard-section-with-margins">
                    <div class="row" id="Stage0RowDesk">
                        <div class="RowMargins">
                            <div class="col s8">
                                <div class="card white darken-1 hoverable" style="margin-top: 08px">
                                    <div class="card-content black-text">
                                        <h4>STAGE 5</h4>
                                        <p>
                                            Scaling. At this stage the Entrepreneur will have completed their business plan and MVP. The purpose of this Stage is to grow the business further and increase revenue generation through marketing support.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 75.JPG" style="width: 140%; height: 120%; margin-top: 02em" />
                        </div>
                        <div class="col s2" id="stage1Zoom">
                            <img src="/images/FutureMarkers-resized-images/FM 80.JPG" style="width: 140%; height: 120%; margin-top: 02em; margin-left: 04em" />
                        </div>
                    </div>
                </div>
            </div>

            <div id="desktop">
                <div class="section container-fluid">
                    <div class="row">
                        <div class="parallax-container" id="ProgrammesTopSection1">
                            <div class="parallax">
                                <img src="/images/Programmes Images/2.3 Programmes.jpg" id="topimage1">
                            </div>
                            <h2 class="programs-section-headers"style="text-shadow: 5px 3px 1px #000000;">Technology Innovation Content</h2>
                        </div>
                    </div>
                </div>
            </div>


<!---PROGRAMS Stage0 Desktop-->
<!---PROGRAMS Stage0 Mobile-->

    <div>
        <div class="section standard-section-with-margins" id="InnovatorsSixthSectionMobile">
            <div class="col l4">
                <!--STAGE 0-->
                <div class="RowMargins">
                    <div class="col s12 m8">
                        <div class="card white darken-1 hoverable">
                            <div class="card-content black-text">
                                <h4>STAGE 1</h4>
                                <p>
                                    Stage 1 is Bootcamp. This is the ideation phase where the Entrepreneur joins with an idea. Where the Entrepreneur decides if Incubation is for them by orientation and a series of workshops covering the ideation phase. Progression to Stage 2 is via a panel selection.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row center-align">
                    <img src="/images/FutureMarkers-resized-images/FM 20 (1).JPG" style="width: 40%; height: 50%;" />
                    <img src="/images/FutureMarkers-resized-images/FM 20 (2).JPG" style="width: 40%; height: 50%;" />
                </div>
                <div class="col s12 m8" id="Stage1InfoDesk">
                    <div class="card white darken-1 hoverable">
                        <div class="card-content black-text">
                            <h4>STAGE 2</h4>
                            <p>
                                Is referred to as Pre-Incubation. At this stage, the entrepreneur will have completed the ideation process or entered the programme with market research that proves a well-defined business problem and solution and (or) product proof of concept in development and (or) pre-revenue stage. The purpose of Stage 2 is to validate the business, proving it to be a viable opportunity.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col l4">
                    <div class="row center-align">
                        <img src="/images/FutureMarkers-resized-images/FM 40.JPG" style="width: 40%; height: 50%;" />
                        <img src="/images/FutureMarkers-resized-images/FM 50.JPG" style="width: 40%; height: 50%;" />
                    </div>
                    <div class="row center-align">
                        <img src="/images/FutureMarkers-resized-images/FM 51.JPG" style="width: 40%; height: 50%;" />
                        <img src="/images/FutureMarkers-resized-images/FM 52.JPG" style="width: 40%; height: 50%;" />
                    </div>
                </div>

                <div class="col s12 m8">
                    <div class="card white darken-1 hoverable">
                        <div class="card-content black-text">
                            <h4>STAGE 3</h4>
                            <p>
                                Minimum Viable Product (MVP) stage. The entrepreneur has completed their business registration and corporate identity and can now proceed on developing/building the most basic form of their product or service.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col l4">
                    <div class="row center-align">
                        <img src="/images/FutureMarkers-resized-images/FM 53.JPG" style="width: 40%; height: 50%;" />
                        <img src="/images/FutureMarkers-resized-images/FM 54.JPG" style="width: 40%; height: 50%;" />
                    </div>
                    <div class="row center-align">
                        <img src="/images/FutureMarkers-resized-images/FM 55.JPG" style="width: 40%; height: 50%;" />
                        <img src="/images/FutureMarkers-resized-images/FM 60.JPG" style="width: 40%; height: 50%;" />
                    </div>
                </div>
            </div>

            <div class="col s12 m8">
                <div class="card white darken-1 hoverable">
                    <div class="card-content black-text">
                        <h4>STAGE 4</h4>
                        <p>
                            Revenue generation stage. At this stage the Entrepreneur has completed their market research, their MVP and are in the process of securing their first customer.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col l4">
                <div class="row center-align">
                    <img src="/images/FutureMarkers-resized-images/FM 65.JPG" style="width: 40%; height: 50%;" />
                    <img src="/images/FutureMarkers-resized-images/FM 70.JPG" style="width: 40%; height: 50%;" />
                </div>
            </div>
            <div class="col s12 m8">
                <div class="card white darken-1 hoverable">
                    <div class="card-content black-text">
                        <h4>STAGE 5</h4>
                        <p>
                        <div class="col s12 m8">
                            <div class="card white darken-1 hoverable">
                                <div class="card-content black-text">
                                    <h4>STAGE 5</h4>
                                    <p>
                                        Scaling. At this stage the Entrepreneur will have completed their business plan and MVP. The purpose of this Stage is to grow the business further and increase revenue generation through marketing support.
                                    </p>
                                </div>
                            </div>
                        </div>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col l4">
                <div class="row center-align">
                    <img src="/images/FutureMarkers-resized-images/FM 75.JPG" style="width: 40%; height: 50%;" />
                    <img src="/images/FutureMarkers-resized-images/FM 80.JPG" style="width: 40%; height: 50%;" />
                </div>
            </div>
        </div>
    </div>
    <div id="mobile">
        <div class="section container-fluid">
            <div class="row">
                <div id="ProgrammesTopSection1">
                    <div>
                        <img src="/images/Programmes Images/2.3 Programmes.jpg" id="topimage1">
                    </div>
                    <h2 class="programs-section-headers"style="text-shadow: 5px 3px 1px #000000;">Technology Innovation Content</h2>
                </div>
            </div>
        </div>
    </div>



</div>


    {{--@* Innovators Ninth Section Desktop *@--}}
    <div class="section standard-section-with-margins" id="InnovatorsNinthSectionDesktop">
        <div class="row">
            <div class="col l4">
                <div class="row">
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND01.JPG" style="width: 100%; height: 100%;" />
                    </div>
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND02.JPG" style="width: 100%; height: 100%;" />
                    </div>
                </div>
                <div class="row">
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND03.JPG" style="width: 100%; height: 100%" />
                    </div>
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND04.JPG" style="width: 100%; height: 100%" />
                    </div>
                </div>
                <div class="row">
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND05.JPG" style="width: 100%; height: 100%" />
                    </div>
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND06.JPG" style="width: 100%; height: 100%" />
                    </div>
                </div>
            </div>
            <div class="col l1"></div>
            <div class="col l7">
                <p class="standard-paragraphs">
                    Propella have positioned themselves to assist both B2E (Business to Entrepreneur) and B2B (Business to Business) applicants that are seeking assistance in developing their innovative products,
                    processes and/or services within the following sectors:
                </p>
                <ul style="font-family: Arial">
                    <li>• Renewable Energy</li>
                    <li>• Energy Efficiency</li>
                    <li>• Advanced Manufacturing, Automation and Industry 4.0</li>
                    <li>• Internet of Things </li>
                    <li>• Smart City solutions</li>
                    <li>• Maritime</li>
                    <li>• Agriculture</li>
                    <li>• Healthcare</li>
                </ul>


                <div class="row">
                    <p class="standard-paragraphs">
                        The industrial incubator will consider applicants who already have a proof-of-concept or an early-stage prototype. Applicants are required to make an online submission on the website –
                    <a href="https://propellaincubator.co.za/Application-Process/ind-application" style="font-style: italic;"> Apply for the Propella Industrila Programme</a>.
                    Once the applications are screened internally and align to Propella’s outlined focus sectors,
                    applicants will then be required to present their innovation to a panel.
                    </p>
                </div>

                <div class="row">
                    <p class="standard-paragraphs">
                        The facility is well equipped and ideally suited for low volume production. Prototyping support facilities such as 3D printing, 3D scanning and small part machining and molding are available,
                        together with a well-resourced workshop that has all the basic tools required for manufacturing. There is also a network of skilled technical and business support staff.
                    </p>
                    <br />
                    <p style="font-style: italic;"><a href="#back">Go back</a></p>

                </div>
            </div>
        </div>
    </div>

    {{--@* Innovators Ninth Section Mobile *@--}}
    <div class="section standard-section-with-margins" id="InnovatorsNinthSectionMobile">
        <div class="col l4">
            <div class="row center-align">
                    <img src="/images/Innovators/Industrial/IND01.JPG" style="width: 40%; height: 50%;" />
                    <img src="/images/Innovators/Industrial/IND02.JPG" style="width: 40%; height: 50%;" />
            </div>
            <div class="row center-align">
                    <img src="/images/Innovators/Industrial/IND03.JPG" style="width: 40%; height: 50%" />
                    <img src="/images/Innovators/Industrial/IND04.JPG" style="width: 40%; height: 50%" />
            </div>
            <div class="row center-align">
                    <img src="/images/Innovators/Industrial/IND05.JPG" style="width: 40%; height: 50%" />
                    <img src="/images/Innovators/Industrial/IND06.JPG" style="width: 40%; height: 50%" />
            </div>
        </div>
        <div class="row center-align">
            <p class="standard-paragraphs">
                Propella have positioned themselves to assist both B2E (Business to Entrepreneur) and B2B (Business to Business) applicants that are
                seeking assistance in developing their innovative products, processes and/or services within the following areas:
            </p>
            <ul style="font-family: Arial">
                <li>• Renewable Energy</li>
                <li>• Energy Efficiency</li>
                <li>• Advanced Manufacturing, Automation and Industry 4.0</li>
                <li>• Internet of Things </li>
                <li>• Smart City solutions</li>
            </ul>
        </div>

        <div class="row center-align">
            <p class="standard-paragraphs">
                The industrial incubator will consider applicants who already have a proof-of-concept or an early-stage prototype. Applicants are required to make an online submission on the website –
                <p style="font-style: italic;"><a href="https://propellaincubator.co.za/Application-Process/ind-application"> Apply for the Propella Industrila Programme</a></p>.
                Once the applications are screened internally and align to Propella’s outlined focus sectors,
                applicants will then be required to present their innovation to a panel.
            </p>
        </div>

        <div class="row center-align">
            <p class="standard-paragraphs">
                Propella offers both virtual and physical incubation services. The warehouse facility is well equipped and ideally suited to low volume production.
                Prototyping support facilities such as 3D printing, 3D scanning and small part machining and molding are available, together with a well-resourced workshop
                that has all the basic tools required for manufacturing. There is also a network of skilled technical and business support.
                If there are products that demonstrate great growth / market potential, Propella would also be willing to engage in ‘cash for equity’ discussions.
            </p>
        </div>
    </div>

    {{--@* Innovators Tenth Section: Image Only *@--}}
    <div id="desktop">
    <div class="section  container-fluid">
        <div class="row">
            <div class="parallax-container" id="InnovatorsTenthSection">
                <div class="parallax">
                    <img src="/images/propella/ind-content.jpeg" id="InnovatorsTenthSectionImage">
                </div>
                <h1 class="innovators-section-header7">Industrial Content</h1>
            </div>
        </div>
    </div>
    </div>

    <div id="mobile">
    <div class="section  container-fluid">
        <div class="row">
            <div id="InnovatorsTenthSection">
                <div>
                    <img src="/images/propella/ind-content.jpeg" id="InnovatorsTenthSectionImage">
                </div>
                <h1 class="innovators-section-header7">Industrial Content</h1>
            </div>
        </div>
    </div>
    </div>

    {{--@* Innovators Tenth Section Desktop *@--}}
    <div class="section standard-section-with-margins  " id="InnovatorsTenthSectionDesktop">
        <div class="row">

            <div class="col l4">
                <div class="row">
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND01.JPG" style="width: 100%; height: 100%;" />
                    </div>
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND02.JPG" style="width: 100%; height: 100%;" />
                    </div>
                </div>
                <div class="row">
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND03.JPG" style="width: 100%; height: 100%" />
                    </div>
                    <div class="col l6">
                        <img src="/images/Innovators/Industrial/IND04.JPG" style="width: 100%; height: 100%" />
                    </div>
                </div>

            </div>
            <div class="col l1"></div>
            <div class="col l7">
                <p class="standard-paragraphs ">
                    Content covered for the Propella Industrial Hub programme is determined by an in-depth diagnostic conducted on the business and technology as the entrepreneur enters the programme.
                    A journey is charted and agreed on that best utilises the resources made available by Propella and the objective and times frames of the entrepreneur.
                </p>

                <p class="standard-paragraphs">
                    An advisor and mentor are assigned to the entrepreneur who also receives access to the Propella Content Platform and bi-weekly workshops offered by Propella.
                </p>

                <p class="standard-paragraphs ">
                    The following key development stages are covered:
                </p>
                <ul style="font-family: Arial">
                    <li>• Prototype finalisation testing and trialling</li>
                    <li>• Go to market preparation, and</li>
                    <li>• Growth scenarios, preparation and scaling of the business</li>
                </ul>
                <br />
                <p style="font-style: italic;"><a href="#back">Go back</a></p>

            </div>

        </div>
    </div>

    {{--@* Innovators Tenth Section Mobile *@--}}
    <div class="section standard-section-with-margins" id="InnovatorsTenthSectionMobile">
        <div class="col l4">
            <div class="row center-align">
                    <img src="/images/Innovators/Industrial/IND01.JPG" style="width: 40%; height: 50%;" />
                    <img src="/images/Innovators/Industrial/IND02.JPG" style="width: 40%; height: 50%;" />
            </div>
            <div class="row center-align">
                    <img src="/images/Innovators/Industrial/IND03.JPG" style="width: 40%; height: 50%" />
                    <img src="/images/Innovators/Industrial/IND04.JPG" style="width: 40%; height: 50%" />
            </div>

        </div>
        <div class="row center-align">
            <p class="standard-paragraphs">
                Content covered for Industrial innovators is determined by an in-depth diagnostic conducted on the business and
                technology as the venture enters the programme.  A journey is charted and agreed on that best utilises the resources made
                available by Propella and the objective and times frames of the innovator.
            </p>
        </div>

        <div class="row center-align">
            <p class="standard-paragraphs">
                Typically, an advisor and mentor are assigned to the innovator who also receives access to the Propella Content Platform and bi-weekly workshops offered by Propella.
            </p>
        </div>

        <div class="row center-align ">
            <p class="standard-paragraphs ">
                The following key development stages are covered over a maximum period of 3 years;
            </p>
            <ul style="font-family: Arial">
                <li>• Prototype finalisation testing and trialling</li>
                <li>• Go to market preparation, and</li>
                <li>• Growth scenarios, preparation and scaling of the business</li>
            </ul>
        </div>
    </div>

    {{--@* Innovators Seventh Section: Image Only *@--}}
    <div class="" id="funding"></div>
    <div id="desktop">
    <br />
    <br />
    <br />
    <br />
    <div class="section  container-fluid">
        <div class="row">
            <div class="parallax-container" id="InnovatorsSeventhSection">
                <div class="parallax">
                    <img src="/images/Innovators-Resized-Images/Funding.JPG" id="InnovatorsSeventhSectionImage">
                </div>
                <h1 class="innovators-section-header8"> Funding Options </h1>
            </div>
        </div>
    </div>
    </div>

    <div id="mobile">
        <div class="section  container-fluid">
            <div class="row">
                <div id="InnovatorsSeventhSection">
                    <div>
                        <img src="/images/Innovators-Resized-Images/Funding.JPG" id="InnovatorsSeventhSectionImage">
                    </div>
                    <h1 class="innovators-section-header8"> Funding Options </h1>
                </div>
            </div>
        </div>
    </div>

    {{--@* Innovators Seventh Section Desktop *@--}}
    <div class="section standard-section-with-margins" id="InnovatorsSeventhSectionDesktop">
        <div class="row">
            <div class="col l4">
                <div class="standard-left-buttons MoreInfoBlock">
                    <h6 class="standard-left-button-headers"> Tell me more about the fund </h6>
                </div>

                <div class="standard-left-buttons TIAQuestionsBlock">
                    <h6 class="standard-left-button-headers"> Question to answer before applying for TIA funding</h6>
                </div>
                <div class="standard-left-buttons CriteriaBlock">
                    <h6 class="standard-left-button-headers"> Qualifying criteria for TIA funding</h6>
                </div>

                <div class="standard-left-buttons FundedActivitiesBlock">
                    <h6 class="standard-left-button-headers"> TIA Funded activities include</h6>
                </div>
                <div class="standard-left-buttons ExcludedActivitiesBlock">
                    <h6 class="standard-left-button-headers"> Activities exclude from TIA funding </h6>
                </div>

                <div class="standard-left-buttons TIAApplyBlock">
                    <h6 class="standard-left-button-headers"> How do I apply for TIA funding </h6>
                </div>
            </div>

            <div class="col l1"></div>

            <div class="col l7">
                <div class="row" id="TIASeedFundInfoDesktop">

                    <h4 class="standard-header"> Funding Options </h4>
                    <p>
                        We completely understand that start-up ventures need capital and that the “go to” for most is often the standard “Friends, Family & Fools” option.
                        Propella assists with applications for funding through its network of government and private sector institutions.
                    </p>



                    <h4 class="standard-header">TIA SMME Seed Fund</h4>
                    <p>
                        Propella has been contracted by the Technology Innovation Agency (TIA) to <br />
                        implement to an SMME Seed Fund, aimed at assisting knowledge economy-based<br />
                        SMMEs in the Eastern Cape to access bridging finance to advance their innovative<br />
                        ideas to market. The monetary value of the grant funding will range from R50 000<br />
                        (minimum) to R650 000 (maximum) per project.
                    </p>
                </div>

                <div class="row" id="TIAProposalInfoDesktop" hidden>
                    <h6 class="standard-header">
                        TIA seed fund proposal: please take note of the following considerations
                        before completing the application form
                    </h6>
                    <p class="standard-paragraphs">
                    <ul>
                        <li>• Is your product / service beyond just an idea?</li><li>
                            •  Have you developed a prototype/minimum viable product (MVP) for your<br />
                            product / service / idea?
                        </li>
                        <li>
                            •  Can you substantiate that there is a real need in the market for your product / <br />
                            service? Have you done a market assessment and or viability study?
                        </li>
                        <li>•  Is your product / service unique? Is it patentable?</li>
                        <li>•  Will the product / service be developed and remain in Nelson Mandela Bay?</li>
                        <li>
                            •  Does your product / service fall  within the renewable energy, energy<br />
                            effeciency, circular economy, advanced manufacturing, IoT sectors?
                        </li>
                        <li>
                            • Are you prepared to participate in the Propella Incubation Programme if<br />
                            requested to do so?
                        </li>
                    </ul>
                    </p>
                </div>

                <div class="row" id="CriteriaInfoDesktop" hidden>
                    <h6 class="standard-header">Applicants must please take note of the following qualifying criteria:</h6>

                    <p class="standard-paragraphs">
                    <ul>
                        <li>• The product / service must be at Technology Level (TRL) 3</li><li>
                            • A basic working prototype must have been deployed - i.e. you must be able to<br />
                            demostrat that the prototype works
                        </li>
                        <li>• The product / service must be unique or a differentiator in the market</li><li>
                            • Applicants must have done  a market assessment to establish a clear need for their<br />
                            product / service
                        </li>
                        <li>•  Applicants must be prepared to enter the Propella Incubation Programme</li>
                    </ul>
                    <br />
                    This Proposal Call is limited to the following sectors:
                    <ul>
                        <li>• Renewable Energy</li>
                        <li>• Energy Efficiency</li>
                        <li>•  Circular Economy</li>
                        <li>• Advanced Manufacturing</li>
                        <li>• Internet of Things (IoT) / Smart City Solutions.</li>
                    </ul>
                    </p>
                </div>

                <div class="row" id="FundedActivitiesInfoDesktop" hidden>
                    <h6 class="standard-header">Funding will be directed at SMMEs / Entrepreneurs requiring the following activities:</h6>
                    <p class="standard-paragraphs">
                    <ul>
                        <li>
                            • Developing the prototype or process further to market readiness (as part of<br />
                            comprehensive technology package)</li>
                        <li>•  Intellectual property ("IP") support and sourcing of IP opinions;</li>
                        <li>•  Production of market samples and/or associated testing;</li>
                        <li>•  Techno-economic evaluation studies;</li>
                        <li>•  Conducting field studies to further develop the technology, market and/or customer needs;</li>
                        <li>•  Refining and implementing designs;</li>
                        <li>•  Support of certification activities;</li>
                        <li>•  Piloting outputs and technology scale-up and techno-economic evaluation;</li>
                        <li>•  Business plan development</li>
                    </ul>
                    </p>
                </div>

                <div class="row" id="ExcludedActivitiesInfoDesktop" hidden>
                    <h6 class="standard-header">The following activities will not quality for funding support:</h6>
                    <p class="standard-paragraphs">
                    <ul>
                        <li>•  Working Capital</li>
                        <li>• Operation expenses</li>
                        <li>
                            • Purchasing of equipment that is not within the applicant's scope of work and<br />
                            relevant to the allowable activities and objective of their application.
                        </li>
                    </ul>
                    Projects should have the potential for further investment and development by funders.<br />
                    <br />
                    <b>Please read the information and criteria below and contact</b><br />
                    mara@propellaincubator.co.za <b>
                        for an application form or download the <br />
                        appliaction form below if you believe you meet the requirements.
                    </b>
                    </p>
                </div>

                <div class="row" id="ApplyForFundingInfoDesktop" hidden>
                    <p style="font-style: italic;"><a href="/Application-Process/question-category">Click here to Apply</a></p>
                </div>

                <p style="font-style: italic;"><a href="#back">Go back</a></p>

            </div>
        </div>
    </div>


    {{--@* Innovators Seventh Section Mobile *@--}}
    <div class="section standard-section-with-margins" id="InnovatorsSeventhSectionMobile">
        <div class="row center-align">
            <div class="row">
                <div class="standard-blocks col s6 MoreInfoBlock">
                    <h5 class="standard-block-headers"> Tell me more about the fund. </h5>
                </div>

                <div class="standard-blocks col s6 TIAQuestionsBlock">
                    <h5 class="standard-block-headers"> Question to answer before applying for TIA funding. </h5>
                </div>
            </div>

            <div class="row">
                <div class="standard-blocks col s6 CriteriaBlock">
                    <h5 class="standard-block-headers"> Qualifying criteria for TIA funding. </h5>
                </div>

                <div class="standard-blocks col s6 FundedActivitiesBlock">
                    <h5 class="standard-block-headers"> TIA Funded activities include. </h5>
                </div>
            </div>

            <div class="row">
                <div class="standard-blocks col s6 ExcludedActivitiesBlock">
                    <h5 class="standard-block-headers"> Activities exclude from TIA funding. </h5>
                </div>

                <div class="standard-blocks col s6 TIAApplyBlock">
                    <h5 class="standard-block-headers"> How do I apply for TIA funding </h5>
                </div>
            </div>
        </div>



        <div class="row text-justify" id="TIASeedFundInfoMobile">
            <div class="row center-align">
                <h4 class="standard-header"> Funding Options </h4>
                <p>
                    We completely understand that start-up ventures need capital and that the "go to" for most is ofter the standard
                    "Friends, Family &amp; Fools" options. Propella assists with the application for funding through its network of government
                    and private sector institutions.
                </p>
            </div>
            <hr />
            <div class="row center-align">
                <h4 class="standard-header">TIA SMME Seed Fund</h4>
                <p>
                    Propella has been contracted by the Technology Innovation Agency (TIA) to <br />
                    implement to an SMME Seed Fund, aimed at assisting knowledge economy-based<br />
                    SMMEs in the Eastern Cape to access bridging finance to advance their innovative<br />
                    ideas to market. The monetary value of the grant funding will range from R50 000<br />
                    (minimum) to R650 000 (maximum) per project.
                </p>
            </div>
        </div>

        <div class="row text-justify" id="TIAProposalInfoMobile" hidden>
            <h6 class="standard-header">
                TIA seed fund proposal: please take note of the following considerations
                before completing the application form
            </h6>
            <p class="standard-paragraphs">
            <ul>
                <li>• Is your product / service beyond just an idea?</li><li>
                    •  Have you developed a prototype/minimum viable product (MVP) for your<br />
                    product / service / idea?
                </li>
                <li>
                    •  Can you substantiate that there is a real need in the market for your product / <br />
                    service? Have you done a market assessment and or viability study?
                </li>
                <li>• •  Is your product / service unique? Is it patentable?</li>
                <li>•  Will the product / service be developed and remain in Nelson Mandela Bay?</li>
                <li>
                    • Does your product / service fall  within the renewable energy, energy<br />
                    effeciency, circular economy, advanced manufacturing, IoT sectors?
                </li>
                <li>
                    •  Are you prepared to participate in the Propella Incubation Programme if<br />
                    requested to do so?
                </li>
            </ul>
            </p>
        </div>

        <div class="row text-justify" id="CriteriaInfoMobile" hidden>
            <h6 class="standard-header">Applicants must please take note of the following qualifying criteria:</h6>

            <p class="standard-paragraphs">
            <ul>
                <li>• • The product / service must be at Technology Level (TRL) 3</li><li>
                    •  A basic working prototype must have been deployed - i.e. you must be able to<br />
                    demostrat that the prototype works
                </li>
                <li• The product / service must be unique or a differentiator in the market</li><li>
                   •  Applicants must have done  a market assessment to establish a clear need for their<br />
                    product / service
                </li>
                <li>• Applicants must be prepared to enter the Propella Incubation Programme</li>
            </ul>
            <br />
            This Proposal Call is limited to the following sectors:
            <ul>
                <li>•  Renewable Energy</li>
                <li>• Energy Efficiency</li>
                <li>•  Circular Economy</li>
                <li>• Advanced Manufacturing</li>
                <li>• Internet of Things (IoT) / Smart City Solutions.</li>
            </ul>
            </p>
        </div>

        <div class="row text-justify" id="FundedActivitiesInfoMobile" hidden>
            <h6 class="standard-header">Funding will be directed at SMMEs / Entrepreneurs requiring the following activities:</h6>
            <p class="standard-paragraphs">
            <ul>
                <li>
                    •  Developing the prototype or process further to market readiness (as part of<br />
                    comprehensive technology package);/li>
                <li>•  Intellectual property ("IP") support and sourcing of IP opinions;</li>
                <li>•  Production of market samples and/or associated testing;</li>
                <li>• Techno-economic evaluation studies;</li>
                <li>•  Conducting field studies to further develop the technology, market and/or customer needs;</li>
                <li>•  Refining and implementing designs;</li>
                <li>•  Support of certification activities;</li>
                <li>•  Piloting outputs and technology scale-up and techno-economic evaluation;</li>
                <li>•  Business plan development</li>
            </ul>
            </p>
        </div>

        <div class="row text-justify" id="ExcludedActivitiesInfoMobile" hidden>
            <h6 class="standard-header">The following activities will not quality for funding support:</h6>
            <p class="standard-paragraphs">
            <ul>
                <li>• •  Working Capital</li>
                <li>• Operation expenses</li>
                <li>
                    •  Purchasing of equipment that is not within the applicant's scope of work and<br />
                    relevant to the allowable activities and objective of their application.
                </li>
            </ul>
            Projects should have the potential for further investment and development by funders.<br />
            <br />
            <b>Please read the information and criteria below and contact</b><br />
            mara@propellaincubator.co.za <b>
                for an application form or download the <br />
                appliaction form below if you believe you meet the requirements.
            </b>
            </p>
        </div>

        <div class="row text-justify" id="ApplyForFundingInfoMobile" hidden>
            <p>Information to be confirmed.</p>
        </div>
    </div>

    {{--@* Innovators Eigth Section: Image Only *@--}}
    <div class=""id="apply"></div>
    <div id="desktop">
    <br />
    <br />
    <br />
    <br />
    <div class="section  container-fluid">
        <div class="row">
            <div class="parallax-container" id="InnovatorsEigthSection">
                <div class="parallax">
                    <img src="/images/Innovators-Resized-Images/Application.JPG" id="InnovatorsEigthSectionImage">
                </div>
                <h1 class="innovators-section-header9"> Application Process </h1>
            </div>
        </div>
    </div>
    </div>
    <div class="section standard-section-with-margins" id="InnovatorsEigthSectionDesktop">
        <div class="row">
            <div class="col l4">
                <div class="standard-left-buttons ApplicationBlock1">
                    <a href="/Application-Process/question-category"><h6 class="standard-left-button-headers"style="color:white;"> Apply for Industrial </h6></a>
                </div>
                <div class="standard-left-buttons ApplicationBlock">
                    <a href="/Application-Process/question-category"><h6 class="standard-left-button-headers" style="color: white;"> Apply for ICT </h6></a>
                </div>
                <div class="standard-left-buttons InterestedBlock">
                    <h6 class="standard-left-button-headers"> If I’m interested in applying what is the process? </h6>
                </div>

                <div class="standard-left-buttons SubmittedApplicationBlock">
                    <h6 class="standard-left-button-headers"> My application has been submitted, what’s next? </h6>
                </div>



            </div>

            <div class="col l1"></div>

            <div class="col l7">
                <div class="row center-align" id="ApplicationProcessImageDesktop">
                    <h5>There is a process, but it’s a quick and easy one. Take a look at the journey below. </h5>
                    <br>
                    <img class="materialboxed" src="/images/Innovators/Appliaction/Process.jpg" style="width: 100%; height: 100%;" />
                </div>

                <div class="row" id="SubmittedApplicationInfoDesktop" hidden>
                    <h5> My application has been submitted, what’s next? </h5>
                    <p class="standard-paragraphs">
                        We will be in contact with you to let you know that we’ve received your application and what the next step is.
                    </p>
                    <p class="standard-paragraphs">
                        It may be a referral to another Enterprise Development Agency, a call to find out more or an invitation to come in for a chat.
                    </p>
                    <p class="standard-paragraphs">
                        If there is mutual interest, you will be invited to present to a Selection Panel who make the final decision regarding incubation.  And then the journey begins.
                    </p>
                </div>

                <div class="row" id="AcceptedInfoDesktop" hidden>
                    <h5> If I’m accepted into a Programme, what support can I expect to get? </h5>
                    <p>Typical support includes:</p>
                    <ul>
                        <li><i class="tiny material-icons">chevron_right</i>  Mentoring and coaching</li>
                        <li><i class="tiny material-icons">chevron_right</i> Generic business training</li>
                        <li><i class="tiny material-icons">chevron_right</i>Specialised business training </li>
                        <li><i class="tiny material-icons">chevron_right</i> Access to technical training</li>
                        <li><i class="tiny material-icons">chevron_right</i> Product development</li>
                        <li><i class="tiny material-icons">chevron_right</i> Technology transfer</li>
                        <li><i class="tiny material-icons">chevron_right</i>Product testing and verification</li>
                        <li><i class="tiny material-icons">chevron_right</i> Facilitation of patent and copyright applications</li>
                        <li><i class="tiny material-icons">chevron_right</i> Advanced market research</li>
                        <li><i class="tiny material-icons">chevron_right</i>Inter-company comparison/benchmarking</li>
                        <li><i class="tiny material-icons">chevron_right</i> Marketing assistance</li>
                        <li><i class="tiny material-icons">chevron_right</i> Management system implementation</li>
                        <li><i class="tiny material-icons">chevron_right</i> QMS advising and implementation</li>
                        <li><i class="tiny material-icons">chevron_right</i> Business-to-business facilitation</li>
                        <li><i class="tiny material-icons">chevron_right</i>Facilitate access to finance</li>
                        <li><i class="tiny material-icons">chevron_right</i> Trade show facilitation</li>
                        <li><i class="tiny material-icons">chevron_right</i> Export readiness assistance</li>
                        <li><i class="tiny material-icons">chevron_right</i> Business Plan development facilitation</li>
                    </ul>
                    <br />
                </div>
                <p style="font-style: italic;"><a href="#back">Go back</a></p>
            </div>
        </div>
    </div>

    <div id="mobile">
        <div class="section container-fluid">
            <div class="row">
                <div id="InnovatorsEigthSection">
                    <div>
                        <img src="/images/Innovators-Resized-Images/Application.JPG" id="InnovatorsEigthSectionImage">
                    </div>
                    <h1 class="innovators-section-header9"> Application Process </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="section standard-section-with-margins" id="InnovatorsEigthSectionMobile">
        <div class="row  ApplicationBlock1 center-align">
            <a href="/Application-Process/question-category"><h6> Apply for IND </h6></a>
        </div>
        <div class="row  ApplicationBlock center-align">
            <a href="/Application-Process/question-category"><h6> Apply for ICT </h6></a>
        </div>

        <div class="row InterestedBlock center-align">
            <br>
            <h7> If I’m interested in applying what is the process? </h7>
        </div>


        <div class="row SubmittedApplicationBlock center-align">
            <br>
            <h7> My application has been submitted, what’s next? </h7>
        </div>





        <div class="row text-justify " id="ApplicationProcessImageMobile">
            <h5>There is a process, but it’s a quick and easy one. Take a look at the journey below. </h5>
            <img class="materialboxed" src="/images/Innovators/Appliaction/Process.jpg" style="width: 100%; height: 100%;" />
        </div>

        <div class="row text-justify" id="SubmittedApplicationInfoMobile" hidden>
            <h5> My application has been submitted, what’s next? </h5>
            <p class="standard-paragraphs">
                We will be in contact with you to let you know that we’ve received your application and what the next step is.
            </p>
            <p class="standard-paragraphs">
                It may be a referral to another Enterprise Development Agency, a call to find out more or an invitation to come in for a chat.
            </p>
            <p class="standard-paragraphs">
                If there is mutual interest, you will be invited to present to a Selection Panel who make the final decision regarding incubation.  And then the journey begins.
            </p>
        </div>
    </div>

    <!--FAQ DESKTOP-->
    <div class="" id="faq"></div>
    <div id="desktop">
        <br />
        <br />
        <br />
        <br />
        <div class="section  container-fluid">
            <div class="row">
                <div class="parallax-container" id="InnovatorsTwelfthSection">
                    <div class="parallax">
                        <img src="/images/Innovators/3.jpg" id="InnovatorsTwelfthSectionImage" alt="">
                    </div>
                    <h1 class="innovators-section-header10"> OTHER FAQ'S </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="section standard-section-with-margins" id="InnovatorsTwelfthSectionDesktop">
        <div class="row">
            <div class="col l4">
                <div class="standard-left-buttons JourneyBlock">
                    <h6 class="standard-left-button-headers">What does the incubation journey look like if I’m accepted?</h6>
                </div>
                <div class="standard-left-buttons IncubationBenefitsBlock">
                    <h6 class="standard-left-button-headers">What are the benefits of incubation?</h6>
                </div>
                <div class="standard-left-buttons AccelerationBenefitsBlock">
                    <h6 class="standard-left-button-headers">What are the benefits of acceleration?</h6>
                </div>

                <div class="standard-left-buttons OnsiteBlock">
                    <h6 class="standard-left-button-headers">Do I physically need to be onsite?</h6>
                </div>
                <div class="standard-left-buttons MentorsBlock1">
                    <h6 class="standard-left-button-headers">Everyone talks about mentors, what do they do?</h6>
                </div>

                <div class="standard-left-buttons MentorsBlock2">
                    <h6 class="standard-left-button-headers"><a href="/Home/About#pic2" style="color:white">Who are the mentors who work with Propella?</a></h6>
                </div>

                <div class="standard-left-buttons GraduateBlock">
                    <h6 class="standard-left-button-headers">What happens once I graduate?</h6>
                </div>
            </div>

            <div class="col l1"></div>

            <div class="col l7">
                <div class="row" id="JourneyInfoDesktop">

                    <h4 class="standard-header">Incubation</h4>
                    <p>
                        Once accepted into a Propella Business Incubator entrepreneurship programme you start on a structured journey that will take you from zero to hero – or help you exit gracefully from an idea or product whose time has not yet come.
                    </p>

                    <h4 class="standard-header">Assessment</h4>
                    <p>
                        Engeli Enterprise Development, which manages Propella, has a box of powerful tools that interrogate your business and idea to identify the strengths and weaknesses.
                        These tools, which have been customised for Propella, are used to diagnose the state and potential of your enterprise from entrepreneurial, business case and technology perspectives.

                    </p>

                    <h4 class="standard-header">Contract</h4>
                    <p>
                        Now that we know what needs to be done, we sign a contract whereby you, the potential business success of tomorrow, agree to certain measurable commitments.
                        We also agree on a target date for you to be viable enough to exit the programme.
                    </p>
                </div>
                <div class="row" id="IncubationBenefitsInfoDesktop" hidden>
                    <h6 class="standard-header">
                        Incubate
                    </h6>
                    <p class="standard-paragraphs">
                    <ul>
                        <li><b>• Lowering the cost of earning your entrepreneurial wings</b></li>
                        <li>
                            You will find everything you need to support your business at the Propella Business Incubation hub
                            – all at subsidised rates. Propella provides affordable office space, access to shared testing and prototyping equipment,
                            meeting facilities, and on-site technical assistance.
                        </li>
                        <br>
                        <li>
                            <b>•	Access to finance</b>
                        </li>
                        <li>
                            Let’s face it, you need to eat – Propella helps to identify potential sources of funding,
                            and then assists you to make the applications and presentations to the funding institutions.
                            Qualifying entrepreneurs also have access to Engeli’s loan and venture capital funds.
                        </li>
                        <br>
                        <li><b>•  Encouraging you to attain new heights</b></li>
                        <li>
                            Success in business is usually directly related to the amount of blood, sweat and tears invested into the company, particularly during the start-up phase. Your mentors and business coaches understand how tough it is – they have “been there, done that”.  Now, they are eager to share their knowledge and experience with you.
                        </li>
                    </ul>
                    </p>
                </div>
                <div class="row" id="AccelerationBenefitsInfoDesktop" hidden>
                    <h6 class="standard-header">Accelerate</h6>

                    <p class="standard-paragraphs">
                    <ul>
                        <li>
                            <b>•	Avoiding failure through success</b>
                        </li>
                        <li>
                            Ironically, many small businesses fail because they do not know how to grow. Propella prepares you for growth by ensuring you have the systems in place to support acceleration in demand for your products.

                        </li><br>
                        <li>
                            <b>•	Establishing a network</b>
                        </li>
                        <li>
                            Ultimately, sustainability in business depends largely on who you know. Propella will help you build up a network of suppliers, partners, funders, and customers as you grow from being a new entrepreneur with a good idea to an entrepreneur with a viable business.

                        </li><br>
                        <li><b>•	Credibility</b></li>
                        <li>Being selected by Propella helps to open doors to investors, customers, and partners. They know that Propella companies have substance and the support needed to succeed.</li>
                    </ul>
                    </p>

                </div>
                <div class="row" id="OnsiteInfoDesktop" hidden>
                    <h6 class="standard-header">Virtual incubation </h6>
                    <p class="standard-paragraphs">
                        If you already have facilities, then you may qualify as a “virtual incubatee”
                        where you have access to all the support provided by Propella as indicated in the “Incubation Programme”
                    </p>
                    <br>
                    <h6 class="standard-header">Propella Virtual ICT Programme</h6>
                    <p class="standard-paragraphs">
                        Our ICT Programme the Technology Innovation Programme is run completely virtually.
                        This means that entrepreneurs from anywhere in South Africa can take part in the programme.
                    </p>
                </div>
                <div class="row" id="Mentors1InfoDesktop" hidden>
                    Each venture has a hand in appointing their own Mentor.
                    The Mentor’s role is to guide and assist you through the start-up phase of your business.
                    They are the “Entrepreneur in the Room” having generally successfully started and run businesses of their own.
                    This invaluable experience guides them in questioning, encouraging, motivating, and inspiring you to make decisions that will benefit you and your business.
                    It’s important to remember that they aren’t there to build your business for you, that’s your role and that you have the final decision with regards to the direction that you want your business to take.
                    Mentor appointments are reviewed every 3 months.
                </div>
                <div class="row" id="GraduateInfoDesktop" hidden>
                    <h6 class="standard-header">Graduate</h6><br>
                    <h4 class="standard-header">Flying high after Propella</h4>
                    <p class="standard-paragraphs">
                        Much as we may like your personality and your energy, we want you to outgrow Propella, and to soar.
                        The solid grounding you receive while working at Propella and the network you build up will help you have a sustainable business
                        – and you are always welcome back for coffee and a chat.
                    </p>
                    <br />
                    <h4 class="standard-header">After you’ve left the building</h4>
                    <p class="standard-paragraphs">
                        If you need or want it, Propella provides support to incubatees in the next phase of commercialisation after they have exited the formal incubation programme. The main offerings include:
                    <ul>
                        <li>
                            <b>•	Funding</b>
                        </li>
                        <li>One of the biggest challenges facing start-up businesses is funding. Finance, when it is made available,
                            is often at the maximum interest rate as you are seen as an unknown risk. Propella Business Incubator support includes:
                                 <li>o	Assistance with identifying potential funders and the formulation and presentation of the funding request</li>
                                 <li>o	Raising money through partnerships with venture capital funds (Engeli Section 12J VCC and others)</li>
                        </li>
                        <br>
                        <li>
                            <b>•	Post Incubation Assistance</b>
                        </li>
                        <li>
                            Propella Business Incubator may offer an opportunity for entrepreneurs that have graduated to continue benefiting from the services and partnerships that are available through the Incubator for an extended period of six months while they settle into their new location.
                        </li>
                    </ul>
                    </p>

                </div>
                <p style="font-style: italic;"><a href="#back">Go back</a></p>

            </div>
        </div>
    </div>


    <!--FAQ MOBILE-->
    <div class="" id="faq"></div>
    <div id="mobile">
        <br />
        <br />
        <br />
        <br />
        <div class="section  container-fluid">
            <div class="row">
                <div class="parallax-container" id="InnovatorsTwelfthSection">
                    <div class="parallax">
                        <img src="/images/Innovators/3.jpg" id="InnovatorsTwelfthSectionImage" alt="">
                    </div>
                    <h1 class="innovators-section-header12"> OTHER FAQ'S </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="section standard-section-with-margins" id="InnovatorsTwelfthSectionMobile">
        <div class="row JourneyBlock center-align">
            <h6>What does the incubation journey look like if I’m accepted?</h6>
        </div>
        <div class="row IncubationBenefitsBlock center-align">
            <h6>What are the benefits of incubation?</h6>
        </div>
        <div class="row AccelerationBenefitsBlock center-align">
            <h6>What are the benefits of acceleration?</h6>
        </div>

        <div class="row OnsiteBlock center-align">
            <h6>Do I physically need to be onsite?</h6>
        </div>
        <div class="row MentorsBlock1 center-align">
            <h6>Everyone talks about mentors, what do they do?</h6>
        </div>

        <div class="row MentorsBlock2 center-align">
            <h6><a href="/Home/About#pic2" style="color:white">Who are the mentors who work with Propella?</a></h6>
        </div>

        <div class="row GraduateBlock center-align">
            <h6>What happens once I graduate?</h6>
        </div>
        <div class="row" id="JourneyInfoMobile">

            <h4 class="standard-header">Incubation</h4>
            <p>
                Once accepted into a Propella Business Incubator entrepreneurship programme you start on a structured journey that will take you from zero to hero – or help you exit gracefully from an idea or product whose time has not yet come.
            </p>

            <h4 class="standard-header">Assessment</h4>
            <p>
                Engeli Enterprise Development, which manages Propella, has a box of powerful tools that interrogate your business and idea to identify the strengths and weaknesses.
                These tools, which have been customised for Propella, are used to diagnose the state and potential of your enterprise from entrepreneurial, business case and technology perspectives.

            </p>

            <h4 class="standard-header">Contract</h4>
            <p>
                Now that we know what needs to be done, we sign a contract whereby you, the potential business success of tomorrow, agree to certain measurable commitments.
                We also agree on a target date for you to be viable enough to exit the programme.
            </p>
        </div>
        <div class="row" id="IncubationBenefitsInfoMobile" hidden>
            <h6 class="standard-header">
                Incubate
            </h6>
            <p class="standard-paragraphs">
            <ul>
                <li><b>• Lowering the cost of earning your entrepreneurial wings</b></li>
                <li>
                    You will find everything you need to support your business at the Propella Business Incubation hub
                    – all at subsidised rates. Propella provides affordable office space, access to shared testing and prototyping equipment,
                    meeting facilities, and on-site technical assistance.
                </li>
                <br>
                <li>
                    <b>•	Access to finance</b>
                </li>
                <li>
                    Let’s face it, you need to eat – Propella helps to identify potential sources of funding,
                    and then assists you to make the applications and presentations to the funding institutions.
                    Qualifying entrepreneurs also have access to Engeli’s loan and venture capital funds.
                </li>
                <br>
                <li><b>•  Encouraging you to attain new heights</b></li>
                <li>
                    Success in business is usually directly related to the amount of blood, sweat and tears invested into the company, particularly during the start-up phase. Your mentors and business coaches understand how tough it is – they have “been there, done that”.  Now, they are eager to share their knowledge and experience with you.
                </li>
            </ul>
            </p>
        </div>
        <div class="row" id="AccelerationBenefitsInfoMobile" hidden>
            <h6 class="standard-header">Accelerate</h6>

            <p class="standard-paragraphs">
            <ul>
                <li>
                    <b>•	Avoiding failure through success</b>
                </li>
                <li>
                    Ironically, many small businesses fail because they do not know how to grow. Propella prepares you for growth by ensuring you have the systems in place to support acceleration in demand for your products.

                </li><br>
                <li>
                    <b>•	Establishing a network</b>
                </li>
                <li>
                    Ultimately, sustainability in business depends largely on who you know. Propella will help you build up a network of suppliers, partners, funders, and customers as you grow from being a new entrepreneur with a good idea to an entrepreneur with a viable business.

                </li><br>
                <li><b>•	Credibility</b></li>
                <li>Being selected by Propella helps to open doors to investors, customers, and partners. They know that Propella companies have substance and the support needed to succeed.</li>
            </ul>
            </p>

        </div>
        <div class="row" id="OnsiteInfoMobile" hidden>
            <h6 class="standard-header">Virtual incubation </h6>
            <p class="standard-paragraphs">
                If you already have facilities, then you may qualify as a “virtual incubatee”
                where you have access to all the support provided by Propella as indicated in the “Incubation Programme”
            </p>
            <br>
            <h6 class="standard-header">Propella Virtual ICT Programme</h6>
            <p class="standard-paragraphs">
                Our ICT Programme the Technology Innovation Programme is run completely virtually.
                This means that entrepreneurs from anywhere in South Africa can take part in the programme.
            </p>
        </div>
        <div class="row" id="Mentors1InfoMobile" hidden>
            Each venture has a hand in appointing their own Mentor.
            The Mentor’s role is to guide and assist you through the start-up phase of your business.
            They are the “Entrepreneur in the Room” having generally successfully started and run businesses of their own.
            This invaluable experience guides them in questioning, encouraging, motivating, and inspiring you to make decisions that will benefit you and your business.
            It’s important to remember that they aren’t there to build your business for you, that’s your role and that you have the final decision with regards to the direction that you want your business to take.
            Mentor appointments are reviewed every 3 months.
        </div>
        <div class="row" id="GraduateInfoMobile" hidden>
            <h6 class="standard-header">Graduate</h6><br>
            <h4 class="standard-header">Flying high after Propella</h4>
            <p class="standard-paragraphs">
                Much as we may like your personality and your energy, we want you to outgrow Propella, and to soar.
                The solid grounding you receive while working at Propella and the network you build up will help you have a sustainable business
                – and you are always welcome back for coffee and a chat.
            </p>
            <br />
            <h4 class="standard-header">After you’ve left the building</h4>
            <p class="standard-paragraphs">
                If you need or want it, Propella provides support to incubatees in the next phase of commercialisation after they have exited the formal incubation programme. The main offerings include:
            <ul>
                <li>
                    <b>•	Funding</b>
                </li>
                <li>One of the biggest challenges facing start-up businesses is funding. Finance, when it is made available,
                    is often at the maximum interest rate as you are seen as an unknown risk. Propella Business Incubator support includes:
                <li>o	Assistance with identifying potential funders and the formulation and presentation of the funding request</li>
                <li>o	Raising money through partnerships with venture capital funds (Engeli Section 12J VCC and others)</li>
                </li>
                <br>
                <li>
                    <b>•	Post Incubation Assistance</b>
                </li>
                <li>
                    Propella Business Incubator may offer an opportunity for entrepreneurs that have graduated to continue benefiting from the services and partnerships that are available through the Incubator for an extended period of six months while they settle into their new location.
                </li>
            </ul>
            </p>

        </div>
        <p style="font-style: italic;"><a href="#back">Go back</a></p>
    </div>





    <script>
    $(document).ready(function () {
        $('.materialboxed').materialbox();

        $('.modal').modal();
        //QUIZ SCRIPT
        var score = 0;

        $('#ReadyCheckBtn').on('click', function () {
            $('#QuestionContent').show();
            $('#ReadyCheckBtn').hide();
        });

        $('.scoreBtn').on('click', function () {
            score = score + 5;
        });

        $('.QuestionOneBtn').on('click', function () {
            $('.QuestionTwoDiv').show();
        });

        $('.QuestionTwoBtn').on('click', function () {
            $('.QuestionThreeDiv').show();
        });

        $('.QuestionThreeBtn').on('click', function () {
            $('.QuestionFourDiv').show();
        });

        $('.QuestionFourBtn').on('click', function () {
            $('.QuestionFiveDiv').show();
        });

        $('.QuestionFiveBtn').on('click', function () {
            $('.QuestionSixDiv').show();
        });

        $('.QuestionSixBtn').on('click', function () {
            $('.QuestionSevenDiv').show();
        });

        $('.QuestionSevenBtn').on('click', function () {
            $('.QuestionEightDiv').show();
        });

        $('.QuestionEightBtn').on('click', function () {
            $('.QuestionNineDiv').show();
        });

        $('.QuestionNineBtn').on('click', function () {
            $('.QuestionTenDiv').show();
        });

        $('.QuestionTenBtn').on('click', function () {
            $('.QuestionElevenDiv').show();
        });

        $('.QuestionElevenBtn').on('click', function () {
            $('.QuestionTwelveDiv').show();
        });

        $('.finalBtn').on('click', function () {
            console.log(score);

            if (score >= 45) {
                $('#HighParagraph').show();
            } else if ((score <35)&& (score >=35)){
                $('#AverageParagraph').show();
            } else if (score < 35) {
                $('#LowParagraph').show();
            }
        });


        //Venture button
        $('.ventureBlock').on('click', function () {
            location.href = '/Home/Categories';
        });
        //I have an idea
        $('.IdeaBlock ').on('click', function () {
            location.href = '#idea';
        });
        $('.FuturemakersBlock').on('click', function () {
            location.href = '#InnovatorsSixthSection';
        });

        //Programmes offered
        $('.ProgrammesBlock ').on('click', function () {
            location.href = '#programs';
        });
        //Idea ready to be incubated
        $('.IdeaReadyBlock').on('click', function () {
            location.href = '#ideaready';
        });
        //Funding options
        $('.FundingOptionsBlock ').on('click', function () {
            location.href = '#funding';
        });
        //Application process
        $('.ApplyBlock ').on('click', function () {
            location.href = '#apply';
        });
        $('.FAQBlock').on('click', function () {
            location.href = '#faq';
        });
        //Isuzu hub mobile
        // $('.IndustrialHubBlock ').on('click', function () {
        //     location.href = '#InnovatorsNinthSection';
        // });

        //So you have an idea Mobile
        $('.ICTBlockMobile ').on('click', function () {
            $('#ICTIdeaFirstOptionRowMobile').show();
            $('#IdeaFirstOptionRowMobile').hide();

        });
        //Cancel block 1
        $('.CancelBlock1').on('click', function () {
            $('#IdeaFirstOptionRowMobile').show();
            $('#ICTIdeaFirstOptionRowMobile').hide();
        });

        //Tech idea
        $('.ICTIdeaBlockMobile').on('click', function () {
            $('#ICTFinalOptionRowMobile').show();
            $('#ICTIdeaFirstOptionRowMobile').hide();
        });
        //Cancel block
        $('.CancelBlock').on('click', function () {
            $('#ICTIdeaFirstOptionRowMobile').show();
            $('#ICTFinalOptionRowMobile').hide();
        });
        //I have a Wire frame
        $('.ICTDesignBlockMobile').on('click', function () {
            $('#ICTFinalOptionRowMobile').show();
            $('#ICTIdeaFirstOptionRowMobile').hide();
        });
        //I  have a functioning platform
        $('.ICTPrototypeBlockMobile').on('click', function () {
            $('#ICTFinalOptionRowMobile').show();
            $('#ICTIdeaFirstOptionRowMobile').hide();
        });

        //Industrial idea
        $('.INDBlockMobile').on('click', function () {
            $('#INDIdeaFirstOptionRowMobile').show();
            $('#IdeaFirstOptionRowMobile').hide();

        });
        //Cancel Industrial first block
        $('.CancelBlockInd').on('click', function () {
            $('#IdeaFirstOptionRowMobile').show();
            $('#INDIdeaFirstOptionRowMobile').hide();

        });
        //Cancel Industrial first block 0
        $('.CancelBlockIND0').on('click', function () {
            $('#INDIdeaFirstOptionRowMobile').show();
            $('#INDTechIdeaRowMobile').hide();

        });
        //I HAVE AN Industrial idea BLOCK
        $('.INDIdeaBlockMobile').on('click', function () {
            $('#INDTechIdeaRowMobile').show();
            $('#INDIdeaFirstOptionRowMobile').hide();

        });
        //Cancel Industrial SECOND block
        $('.CancelBlockIND2').on('click', function () {
            $('#INDDesignRowMobile').hide();
            $('#INDIdeaFirstOptionRowMobile').show();

        });

        //Cancel Industrial THIRD block
        $('.CancelBlockINDM3').on('click', function () {
            $('#INDPrototypeRowMobile').hide();
            $('#INDIdeaFirstOptionRowMobile').show();

        });
        //I have a design Industrial
        $('.INDDesignBlockMobile').on('click', function () {
            $('#INDDesignRowMobile').show();
            $('#INDIdeaFirstOptionRowMobile').hide();

        });

        //I have a Prototype Industrial
        $('.INDPrototypeBlockMobile').on('click', function () {
            $('#INDPrototypeRowMobile').show();
            $('#INDIdeaFirstOptionRowMobile').hide();

        });




        //So you have an idea block for ICT
        $('.ICTBlock ').on('click', function () {
            $('#ICTIdeaFirstOptionRowDesktop').show();
            $('.ICTBlock').hide();
            $('.IndustrialHubBlock1').hide();
            $('.INDBlock').hide();
        });
        //So you have an idea block for IND
        $('.INDBlock').on('click', function () {
            $('#INDIdeaFirstOptionRowDesktop').show();
            $('.INDBlock').hide();
            $('.ICTBlock').hide();
            $('.IndustrialHubBlock').hide();
        });


        //Industrial Idea Block
        //I have an idea IND
        $('.INDIdeaBlock').on('click', function () {
            $('#INDTechIdeaRowDesktop').show();
            $('.INDIdeaBlock').hide();
            $('.HowAdvancedIND').hide();
            $('.INDDesignBlock').hide();
            $('.INDPrototypeBlock').hide();
            $('.INDFirstCancelBlock').hide();
        });

        //I have a design IND
        $('.INDDesignBlock').on('click', function () {
            $('#INDDesignRowDesktop').show();
            $('.INDIdeaBlock').hide();
            $('.HowAdvancedIND').hide();
            $('.INDDesignBlock').hide();
            $('.INDPrototypeBlock').hide();
            $('.INDFirstCancelBlock').hide();
        });
       //I have a prototype IND
        $('.INDPrototypeBlock').on('click', function () {
            $('#INDPrototypeRowDesktop').show();
            $('.INDIdeaBlock').hide();
            $('.HowAdvancedIND').hide();
            $('.INDDesignBlock').hide();
            $('.INDPrototypeBlock').hide();
            $('.INDFirstCancelBlock').hide();
        });

        //How advanced is the idea ICT
        //Tech idea
        $('.ICTIdeaBlock').on('click', function () {
            $('#ICTFinalOptionRowDesktop').show();
            $('.ICTIdeaBlock').hide();
            $('.HowAdvancedBlock').hide();
            $('.ICTPrototypeBlock').hide();
            $('.ICTFirstCancelBlock').hide();
            $('.ICTDesignBlock').hide();
        });



        //Wire frame idea ICT
        $('.ICTDesignBlock').on('click', function () {
            $('#ICTFinalOptionRowDesktop').show();
            $('.ICTIdeaBlock').hide();
            $('.HowAdvancedBlock').hide();
            $('.ICTPrototypeBlock').hide();
            $('.ICTFirstCancelBlock').hide();
            $('.ICTDesignBlock').hide();
        });
        //I have a functioning App block
        $('.ICTPrototypeBlock').on('click', function () {
            $('#ICTFinalOptionRowDesktop').show();
            $('.ICTIdeaBlock').hide();
            $('.HowAdvancedBlock').hide();
            $('.ICTPrototypeBlock').hide();
            $('.ICTFirstCancelBlock').hide();
            $('.ICTDesignBlock').hide();
        });



        //NOT SURE FIRST BUTTON ICT
        $('.ICTFirstCancelBlock').on('click', function () {
            $('#ICTIdeaFirstOptionRowDesktop').hide();
            $('.IndustrialHubBlock').show();
            $('.ICTBlock').show();
            $('.INDBlock').show();
        });
        //NOT SURE SECOND BUTTON ICT
        $('.ICTSecondCancelBlock').on('click', function () {
            $('#ICTFinalOptionRowDesktop').hide();
            $('.HowAdvancedBlock ').show();
            $('.ICTIdeaBlock').show();
            $('.ICTDesignBlock').show();
            $('.ICTPrototypeBlock').show();
            $('.ICTFirstCancelBlock').show();

        });

        //NOT SURE FIRST BUTTON IND
        $('.INDFirstCancelBlock').on('click', function () {
            $('#INDIdeaFirstOptionRowDesktop').hide();
            $('.IndustrialHubBlock').show();
            $('.ICTBlock').show();
            $('.INDBlock').show();

        });

        //NOT SURE SECOND BUTTON IND
        $('.INDSecondCancelBlock').on('click', function () {
            $('#INDTechIdeaRowDesktop').hide();
            $('.HowAdvancedIND').show();
            $('.INDIdeaBlock').show();
            $('.INDDesignBlock').show();
            $('.INDPrototypeBlock').show();
            $('.INDFirstCancelBlock').show();





        });
        //NOT SURE SECOND BUTTON 1 IND
        $('.INDSecondCancelBlock1').on('click', function () {
            $('#INDDesignRowDesktop').hide();
            $('.HowAdvancedIND').show();
            $('.INDIdeaBlock').show();
            $('.INDDesignBlock').show();
            $('.INDPrototypeBlock').show();
            $('.INDFirstCancelBlock').show();


        });
        //NOT SURE SECOND BUTTON 2 IND
        $('.INDSecondCancelBlock2').on('click', function () {
            $('#INDPrototypeRowDesktop').hide();
            $('.HowAdvancedIND').show();
            $('.INDIdeaBlock').show();
            $('.INDDesignBlock').show();
            $('.INDPrototypeBlock').show();
            $('.INDFirstCancelBlock').show();


        });



        $('.ICTBlock1 ').on('click', function () {
            location.href = '#hub';
        });
        $('.INDBlock1').on('click', function () {
            location.href = '#INDProg';
        });


        //ScrollSpy
        $('.scrollspy').scrollSpy();

        //Parallax
        $('.parallax').parallax();

        //Get Industrial Section
        $('#INDBtn').on('click', function () {
            $('#secondSection').hide();
            $('#IndustrialFirstRow').show();
            location.href = '#IndustrialFirstRow';
        });

        //Get ICT Section
        $('#ICTBtn').on('click', function () {
            $('#secondSection').hide();
            $('#ICTFirstRow').show();
            location.href = '#ICTFirstRow';
        });

        //Industrial Idea Button Click. Final Section Show
        $('#industrialIdeaBtn').on('click', function () {
            $('#IndustrialFirstRow').hide();
            $('#industrialDesignRow').hide();
            $('#industrialPrototypeRow').hide();
            $('#industrialIdeaRow').show();
            $('#industrialCancelSection').show();
            location.href = '#IdeaRow';
        });


        //ICT Idea Button Click. Final Section Show
        $('#ictIdeaBtn').on('click', function () {
            $('#ICTFirstRow').hide();
            $('#ictWireFrameRow').hide();
            $('#ictApplicationRow').hide();
            $('#ictIdeaRow').show();
            $('#ictCancelSection').show();
        });


        //Industrial Design Button Click. Final Section Show
        $('#industrialDesignBtn').on('click', function () {
            $('#IndustrialFirstRow').hide();
            $('#industrialIdeaRow').hide();
            $('#industrialPrototypeRow').hide();
            $('#industrialDesignRow').show();
            $('#industrialCancelSection').show();
            location.href = '#DesignRow';
        });

        //ICT WireFrame Button Click. Final Section Show
        $('#ictWireFrameBtn').on('click', function () {
            $('#ICTFirstRow').hide();
            $('#ictApplicationRow').hide();
            $('#ictIdeaRow').hide();
            $('#ictWireFrameRow').show();
            $('#ictCancelSection').show();
        });

        //Industrial Prototype Button Click. Final Section Show.
        $('#industrialPrototypeBtn').on('click', function () {
            $('#IndustrialFirstRow').hide();
            $('#industrialIdeaRow').hide();
            $('#industrialDesignRow').hide();
            $('#industrialPrototypeRow').show();
            $('#industrialCancelSection').show();
            location.href = '#PrototypeRow';
        });

        //ICT Application Button Click. Final Section Show
        $('#ictApplicationBtn').on('click', function () {
            $('#ICTFirstRow').hide();
            $('#ictWireFrameRow').hide();
            $('#ictIdeaRow').hide();
            $('#ictApplicationRow').show();
            $('#ictCancelSection').show();
        });

        //Industrial Cancel Button
        $('#industrialCancelBtn').on('click', function () {
            $('#IndustrialFirstRow').show();
            $('#industrialIdeaRow').hide();
            $('#industrialDesignRow').hide();
            $('#industrialPrototypeRow').hide();
            $('#industrialCancelSection').hide();
        });

        //ICT Cancel Button
        $('#ictCancelBtn').on('click', function () {
            $('#ICTFirstRow').show();
            $('#ictIdeaRow').hide();
            $('#ictWireFrameRow').hide();
            $('#ictApplicationRow').hide();
            $('#ictCancelSection').hide();
        });

        //SHOW AND HIDE

        //Hide Sections
        $('#Innovator8Section').hide();
        $('#Innovator9Section').hide();

        //SHow The Section onClick
        $('#bntInnovatorTwwo').on('click', function () {
            $('#Innovator8Section').show();
            $('#Innovator9Section').hide();
            $('#InnovatorSeventhSection').hide();
        });
        $('#bntInnovator3').on('click', function () {
            $('#Innovator9Section').show();
            $('#Innovator8Section').hide();
            $('#InnovatorSeventhSection').hide();
        });

        //<FUNDING OPTIONS>
        $('#InnovatorsCol3Para2').hide();
        $('#InnovatorsCol3Para3').hide();
        $('#InnovatorsCol3Para4').hide();
        $('#InnovatorsCol3Para5').hide();
        $('#InnovatorsCol3Para6').hide();

        //SHow The Section onClick
        $('#InnovatorsCol1Btn1').on('click', function () {
            $('#InnovatorsCol3Para1').show();
            $('#InnovatorsCol3Para2').hide();
            $('#InnovatorsCol3Para3').hide();
            $('#InnovatorsCol3Para4').hide();
            $('#InnovatorsCol3Para5').hide();
            $('#InnovatorsCol3Para6').hide();
        });
        $('#InnovatorsCol1Btn2').on('click', function () {
            $('#InnovatorsCol3Para2').show();
            $('#InnovatorsCol3Para1').hide();
            $('#InnovatorsCol3Para3').hide();
            $('#InnovatorsCol3Para4').hide();
            $('#InnovatorsCol3Para5').hide();
            $('#InnovatorsCol3Para6').hide();
        });
        $('#InnovatorsCol1Btn3').on('click', function () {
            $('#InnovatorsCol3Para3').show();
            $('#InnovatorsCol3Para1').hide();
            $('#InnovatorsCol3Para2').hide();
            $('#InnovatorsCol3Para4').hide();
            $('#InnovatorsCol3Para5').hide();
            $('#InnovatorsCol3Para6').hide();
        });
        $('#InnovatorsCol1Btn4').on('click', function () {
            $('#InnovatorsCol3Para4').show();
            $('#InnovatorsCol3Para1').hide();
            $('#InnovatorsCol3Para2').hide();
            $('#InnovatorsCol3Para3').hide();
            $('#InnovatorsCol3Para5').hide();
            $('#InnovatorsCol3Para6').hide();

        });
        $('#InnovatorsCol1Btn5').on('click', function () {
            $('#InnovatorsCol3Para5').show();
            $('#InnovatorsCol3Para1').hide();
            $('#InnovatorsCol3Para2').hide();
            $('#InnovatorsCol3Para3').hide();
            $('#InnovatorsCol3Para4').hide();
            $('#InnovatorsCol3Para6').hide();
        });
        $('#InnovatorsCol1Btn6').on('click', function () {
            $('#InnovatorsCol3Para6').show();
            $('#InnovatorsCol3Para1').hide();
            $('#InnovatorsCol3Para2').hide();
            $('#InnovatorsCol3Para3').hide();
            $('#InnovatorsCol3Para4').hide();
            $('#InnovatorsCol3Para5').hide();
        });

        //QUIZ SCRIPT
        var score = 0;

        $('#ReadyCheckBtn').on('click', function () {
            $('#QuestionContent').show();
            $('#ReadyCheckBtn').hide();
        });

        $('.scoreBtn').on('click', function () {
            score = score + 5;
        });

        $('.QuestionOneBtn').on('click', function () {
            $('.QuestionTwoDiv').show();
        });

        $('.QuestionTwoBtn').on('click', function () {
            $('.QuestionThreeDiv').show();
        });

        $('.QuestionThreeBtn').on('click', function () {
            $('.QuestionFourDiv').show();
        });

        $('.QuestionFourBtn').on('click', function () {
            $('.QuestionFiveDiv').show();
        });

        $('.QuestionFiveBtn').on('click', function () {
            $('.QuestionSixDiv').show();
        });

        $('.QuestionSixBtn').on('click', function () {
            $('.QuestionSevenDiv').show();
        });

        $('.QuestionSevenBtn').on('click', function () {
            $('.QuestionEightDiv').show();
        });

        $('.QuestionEightBtn').on('click', function () {
            $('.QuestionNineDiv').show();
        });

        $('.QuestionNineBtn').on('click', function () {
            $('.QuestionTenDiv').show();
        });

        $('.QuestionTenBtn').on('click', function () {
            $('.QuestionElevenDiv').show();
        });

        $('.QuestionElevenBtn').on('click', function () {
            $('.QuestionTwelveDiv').show();
        });

        $('.finalBtn').on('click', function () {
            console.log(score);

            if (score >= 45) {
                $('#HighParagraph').show();
            } else if ((score < 45) && (score >= 35)) {
                $('#AverageParagraph').show();
            } else if (score < 35) {
                $('#LowParagraph').show();
            }
        });

        //Application js
        $('#PointThreeBtn').on('click', function () {
            location.href = '#ThirdImage';
        });
    });
    $(document).ready(function () {
        $('#PointTwoBtn').on('click', function () {
            location.href = '#SecondImage';
        });
    });
    $(document).ready(function () {
        $('#PointOneBtn').on('click', function () {
            location.href = '#TopImage';
        });
    });
    $(document).ready(function () {
        $('#PointONEBtn').on('click', function () {
            location.href = '#TopImage';
        });
    });
    $(document).ready(function () {
        $('#PointTWOBtn').on('click', function () {
            location.href = '#SecondImage';
        });
    });
    $(document).ready(function () {
        $('#PointTHREEBtn').on('click', function () {
            location.href = '#ThirdImage';
        });
    });
    $(document).ready(function () {
        $('#Point1Btn').on('click', function () {
            location.href = '#TopImage';
        });
    });
    $(document).ready(function () {
        $('#Point2Btn').on('click', function () {
            location.href = '#SecondImage';
        });
    });
    $(document).ready(function () {
        $('#Point3Btn').on('click', function () {
            location.href = '#ThirdImage';
        });

        //Redirecting code
        $('#bntInnovatorOne').on('click', function () {
            location.href = '#InnovatorsFourthSection';
        });
        $('#bntInnovatorTwo').on('click', function () {
            location.href = '#InnovatorsSixthSection';
        });
        $('#bntInnovatorThree').on('click', function () {
            location.href = '#TopImage';
        });
        $('#bntInnovatorFour').on('click', function () {
            location.href = '#Innovator10Section';
        });
        $('#bntInnovatorFour').on('click', function () {
            location.href = '#Innovator10Section';
        });
        $('#bntInnovatorSix').on('click', function () {
            location.href = '#TopImage1';
        });


        //Application Process Desktop
        $('.ApplicationBlock1').on('click', function () {


        });
        $('.InterestedBlock').on('click', function () {
            $('#ApplicationProcessImageDesktop').show();
            $('#SubmittedApplicationInfoDesktop').hide();

        });
        $('.SubmittedApplicationBlock').on('click', function () {
            $('#SubmittedApplicationInfoDesktop').show();
            $('#ApplicationProcessImageDesktop').hide();

        });

        //FAQS
        $('.JourneyBlock').on('click', function () {
            $('#JourneyInfoDesktop').show();
            $('#IncubationBenefitsInfoDesktop').hide();
            $('#AccelerationBenefitsInfoDesktop').hide();
            $('#OnsiteInfoDesktop').hide();
            $('#Mentors1InfoDesktop').hide();
            $('#GraduateInfoDesktop').hide();
            $('#OnsiteInfoDesktop').hide();

        });
        $('.IncubationBenefitsBlock').on('click', function () {
            $('#IncubationBenefitsInfoDesktop').show();
            $('#JourneyInfoDesktop').hide();
            $('#AccelerationBenefitsInfoDesktop').hide();
            $('#Mentors1InfoDesktop').hide();
            $('#GraduateInfoDesktop').hide();
            $('#OnsiteInfoDesktop').hide();

        });
        $('.AccelerationBenefitsBlock').on('click', function () {
            $('#AccelerationBenefitsInfoDesktop').show();
            $('#JourneyInfoDesktop').hide();
            $('#IncubationBenefitsInfoDesktop').hide();
            $('#Mentors1InfoDesktop').hide();
            $('#GraduateInfoDesktop').hide();
            $('#OnsiteInfoDesktop').hide();

        });
        $('.OnsiteBlock').on('click', function () {
            $('#OnsiteInfoDesktop').show();
            $('#AccelerationBenefitsInfoDesktop').hide();
            $('#JourneyInfoDesktop').hide();
            $('#IncubationBenefitsInfoDesktop').hide();
            $('#Mentors1InfoDesktop').hide();
            $('#GraduateInfoDesktop').hide();

        });
        $('.MentorsBlock1').on('click', function () {
            $('#Mentors1InfoDesktop').show();
            $('#OnsiteInfoDesktop').hide();
            $('#AccelerationBenefitsInfoDesktop').hide();
            $('#JourneyInfoDesktop').hide();
            $('#IncubationBenefitsInfoDesktop').hide();
            $('#GraduateInfoDesktop').hide();

        });
        $('.GraduateBlock').on('click', function () {
            $('#GraduateInfoDesktop').show();
            $('#Mentors1InfoDesktop').hide();
            $('#OnsiteInfoDesktop').hide();
            $('#AccelerationBenefitsInfoDesktop').hide();
            $('#JourneyInfoDesktop').hide();
            $('#IncubationBenefitsInfoDesktop').hide();

        });
        //END FAQS

        //FAQSMOBILE
        $('.JourneyBlock').on('click', function () {
            $('#JourneyInfoMobile').show();
            var element = document.getElementById("JourneyInfoMobile");
            element.scrollIntoView({behavior: "smooth"});
            $('#IncubationBenefitsInfoMobile').hide();
            $('#AccelerationBenefitsInfoMobile').hide();
            $('#OnsiteInfoMobile').hide();
            $('#Mentors1InfoMobile').hide();
            $('#GraduateInfoMobile').hide();
            $('#OnsiteInfoMobile').hide();

        });
        $('.IncubationBenefitsBlock').on('click', function () {
            $('#IncubationBenefitsInfoMobile').show();
            var elmntToView = document.getElementById("IncubationBenefitsInfoMobile");
            elmntToView.scrollIntoView({behavior: "smooth"});
            $('#JourneyInfoMobile').hide();
            $('#AccelerationBenefitsInfoMobile').hide();
            $('#Mentors1InfoMobile').hide();
            $('#GraduateInfoMobile').hide();
            $('#OnsiteInfoMobile').hide();

        });
        $('.AccelerationBenefitsBlock').on('click', function () {
            $('#AccelerationBenefitsInfoMobile').show();
            var elmntToView = document.getElementById("AccelerationBenefitsInfoMobile");
            elmntToView.scrollIntoView({behavior: "smooth"});
            $('#JourneyInfoMobile').hide();
            $('#IncubationBenefitsInfoMobile').hide();
            $('#Mentors1InfoMobile').hide();
            $('#GraduateInfoMobile').hide();
            $('#OnsiteInfoMobile').hide();

        });
        $('.OnsiteBlock').on('click', function () {
            $('#OnsiteInfoMobile').show();
            var elmntToView = document.getElementById("OnsiteInfoMobile");
            elmntToView.scrollIntoView({behavior: "smooth"});
            $('#AccelerationBenefitsInfoMobile').hide();
            $('#JourneyInfoMobile').hide();
            $('#IncubationBenefitsInfoMobile').hide();
            $('#Mentors1InfoMobile').hide();
            $('#GraduateInfoMobile').hide();

        });
        $('.MentorsBlock1').on('click', function () {
            $('#Mentors1InfoMobile').show();
            var elmntToView = document.getElementById("Mentors1InfoMobile");
            elmntToView.scrollIntoView({behavior: "smooth"});
            $('#OnsiteInfoMobile').hide();
            $('#AccelerationBenefitsInfoMobile').hide();
            $('#JourneyInfoMobile').hide();
            $('#IncubationBenefitsInfoMobile').hide();
            $('#GraduateInfoMobile').hide();

        });
        $('.GraduateBlock').on('click', function () {
            $('#GraduateInfoMobile').show();
            var elmntToView = document.getElementById("GraduateInfoMobile");
            elmntToView.scrollIntoView({behavior: "smooth"});
            $('#Mentors1InfoMobile').hide();
            $('#OnsiteInfoMobile').hide();
            $('#AccelerationBenefitsInfoMobile').hide();
            $('#JourneyInfoMobile').hide();
            $('#IncubationBenefitsInfoMobile').hide();

        });
        //END FAQSMOBILE

        $('.ApplicationBlock').on('click', function () {


        });

        //Application Process Mobile
        $('.ApplicationBlock1').on('click', function () {


        });
        $('.InterestedBlock').on('click', function () {
            $('#ApplicationProcessImageMobile').show();
            $('#SubmittedApplicationInfoMobile').hide();

        });
        $('.SubmittedApplicationBlock').on('click', function () {
            $('#SubmittedApplicationInfoMobile').show();
            $('#ApplicationProcessImageMobile').hide();

        });

        $('.ApplicationBlock').on('click', function () {


        });





        //Funding Options Desktop
        $('.MoreInfoBlock').on('click', function () {
            $('#TIASeedFundInfoDesktop').show();
            $('#TIAProposalInfoDesktop').hide();
            $('#CriteriaInfoDesktop').hide();
            $('#FundedActivitiesInfoDesktop').hide();
            $('#ExcludedActivitiesInfoDesktop').hide();
            $('#ApplyForFundingInfoDesktop').hide();
        });
        $('.TIAQuestionsBlock').on('click', function () {
            $('#TIAProposalInfoDesktop').show();
            $('#TIASeedFundInfoDesktop').hide();
            $('#CriteriaInfoDesktop').hide();
            $('#FundedActivitiesInfoDesktop').hide();
            $('#ExcludedActivitiesInfoDesktop').hide();
            $('#ApplyForFundingInfoDesktop').hide();
        });
        $('.CriteriaBlock').on('click', function () {
            $('#CriteriaInfoDesktop').show();
            $('#TIAProposalInfoDesktop').hide();
            $('#TIASeedFundInfoDesktop').hide();
            $('#FundedActivitiesInfoDesktop').hide();
            $('#ExcludedActivitiesInfoDesktop').hide();
            $('#ApplyForFundingInfoDesktop').hide();
        });

        $('.FundedActivitiesBlock').on('click', function () {
            $('#FundedActivitiesInfoDesktop').show();
            $('#CriteriaInfoDesktop').hide();
            $('#TIAProposalInfoDesktop').hide();
            $('#TIASeedFundInfoDesktop').hide();
            $('#ExcludedActivitiesBlock').hide();
            $('#ApplyForFundingInfoDesktop').hide();
        });
        $('.ExcludedActivitiesBlock').on('click', function () {
            $('#ExcludedActivitiesInfoDesktop').show();
            $('#CriteriaInfoDesktop').hide();
            $('#FundedActivitiesInfoDesktop').hide();
            $('#TIAProposalInfoDesktop').hide();
            $('#TIASeedFundInfoDesktop').hide();
            $('#ApplyForFundingInfoDesktop').hide();
        });
        $('.TIAApplyBlock').on('click', function () {
            $('#ApplyForFundingInfoDesktop').show();
            $('#CriteriaInfoDesktop').hide();
            $('#FundedActivitiesInfoDesktop').hide();
            $('#TIAProposalInfoDesktop').hide();
            $('#TIASeedFundInfoDesktop').hide();
            $('#ExcludedActivitiesInfoDesktop').hide();

        });

        //Funding Options Mobile
        $('.MoreInfoBlock').on('click', function () {
            $('#TIASeedFundInfoMobile').show();
            $('#TIAProposalInfoMobile').hide();
            $('#CriteriaInfoMobile').hide();
            $('#FundedActivitiesInfoMobile').hide();
            $('#ExcludedActivitiesInfoMobile').hide();
            $('#ApplyForFundingInfoMobile').hide();
        });
        $('.TIAQuestionsBlock').on('click', function () {
            $('#TIAProposalInfoMobile').show();
            $('#TIASeedFundInfoMobile').hide();
            $('#CriteriaInfoMobile').hide();
            $('#FundedActivitiesInfoMobile').hide();
            $('#ExcludedActivitiesInfoMobile').hide();
            $('#ApplyForFundingInfoMobile').hide();
        });
        $('.CriteriaBlock').on('click', function () {
            $('#CriteriaInfoMobile').show();
            $('#TIAProposalInfoMobile').hide();
            $('#TIASeedFundInfoMobile').hide();
            $('#FundedActivitiesInfoMobile').hide();
            $('#ExcludedActivitiesInfoMobile').hide();
            $('#ApplyForFundingInfoMobile').hide();
        });

        $('.FundedActivitiesBlock').on('click', function () {
            $('#FundedActivitiesInfoMobile').show();
            $('#CriteriaInfoMobile').hide();
            $('#TIAProposalInfoMobile').hide();
            $('#TIASeedFundInfoMobile').hide();
            $('#ExcludedActivitiesBlockMobile').hide();
            $('#ApplyForFundingInfoMobile').hide();
        });
        $('.ExcludedActivitiesBlock').on('click', function () {
            $('#ExcludedActivitiesInfoMobile').show();
            $('#CriteriaInfoMobile').hide();
            $('#FundedActivitiesInfoMobile').hide();
            $('#TIAProposalInfoMobile').hide();
            $('#TIASeedFundInfoMobile').hide();
            $('#ApplyForFundingInfoMobile').hide();
        });
        $('.TIAApplyBlock').on('click', function () {
            $('#ApplyForFundingInfoMobile').show();
            $('#CriteriaInfoMobile').hide();
            $('#FundedActivitiesInfoMobile').hide();
            $('#TIAProposalInfoMobile').hide();
            $('#TIASeedFundInfoMobile').hide();
            $('#ExcludedActivitiesInfoMobile').hide();

        });
    });
</script>
    </body>

@endsection
