@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>
<link rel="stylesheet" type="text/css" href="/css/Categories/main.css" />


<div class="container-fluid" id="CurrentVenturesDesktop" style="margin-top: 10vh">
    <div class="row ICT">
       <div class=" col s6" style="cursor: pointer">
            <div class="parallax-container" id="ICTParallax">
                <div class="parallax">
                    <img src="/images/Innovators/2. ICT.jpg" id="ICTImageDesktop" />
                </div>
                <h2 class="ICTHeading center-align" style="color:white; margin-top:25vh;text-shadow: 5px 3px 1px #000000;">ICT Ventures</h2>
            </div>
        </div>
        <div class=" col s6" style="cursor: pointer">
            <div class="parallax-container IND" id="INDParallax">
                <div class="parallax">
                    <img src="/images/Innovators/1 Industrial.jpg" id="INDImageDesktop"/>
                </div>
                <h2 class="center-align" style="color:white;margin-top:25vh;text-shadow: 5px 3px 1px #000000;">Industrial Ventures</h2>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid" id="CurrentVenturesMobile">
    <div class="row ICT">
        <div class="row">
            <div class="parallax-container" id="ICTParallaxMobile">
                <div class="parallax">
                    <img src="/images/Innovators/2. ICT.jpg" id="ICTImage"style="height:30vh;" />
                </div>
                <h2 class="s" style="color:white;padding-left:75%; margin-top:65vh;text-shadow: 5px 3px 1px #000000;">ICT</h2>
            </div>
        </div>

        <div class="row">
            <div class="parallax-container" id="INDParallaxMobile">
                <div class="parallax">
                    <img src="/images/Innovators/1 Industrial.jpg" id="INDImage"style="height:30vh;" />
                </div>
                <h2 class="s" style="color:white;padding-left:37%; margin-top:65vh;text-shadow: 5px 3px 1px #000000;">Industrial</h2>
            </div>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function () {
            $('.parallax').parallax();
        });
        //Desktop
        $('#ICTParallax').on('click', function () {
            location.href = '/Home/ICTVentures';
        });
        $('#INDParallax').on('click', function () {
            location.href = '/Home/INDVentures';
        });
        $('#PTI').on('click', function () {
            location.href = '/pti-ventures';
        });

        //Mobile
        $('#ICTParallaxMobile').on('click', function () {
            location.href = '/Home/ICTVentures';
        });
        $('#INDParallaxMobile').on('click', function () {
            location.href = '/Home/INDVentures';
        });
        $('#PTIMobile').on('click', function () {
            location.href = '/pti-ventures';
        });
    </script>
@endsection
