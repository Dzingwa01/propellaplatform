@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/SmartCity/main.css"/>
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;500&family=Roboto:wght@100&display=swap" rel="stylesheet"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>

    <body>

    <div class="programpage container-fluid" id="Mobileprogrampage">
        <div class="section standard-top-section">
            <div class="row">
                <div class="parallax-container" id="SmartCityImage">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/plain.jpg"/>
                    </div>
                    <h1 class="top-smart-header" style="color:white;text-shadow: 5px 3px 1px #000000;">Smart City</h1>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align standard-section-with-margins" id="AboutSection">
            <div id="AboutSectionContent">
                <div class="row">
                    <h4 class="standard-header" align="center">About Smart City</h4>
                    <p class="standard-paragraphs" align="center">
                        Cities and towns have to get smart to manage a combination of influx from urban areas and
                        natural population growth in order to ensure that residents enjoy an increasing quality of life.
                    </p>
                    <h4 class=standard-header align="center">Mission</h4>
                    <p class="standard-paragraphs" align="center">
                        Our mission as a smart, hi-tech incubator is to support the successful development and
                        sustainability of innovation as the first choice for the
                        commercialisation of smart products and services for Smart Cities and Smart towns.
                    </p>
                    <br/>
                    <br/>
                    <div class="row">
                        <div class=" standard-blocks col s6 BusinessButton">
                            <h3 class="standard-block-headers">Smart <br/> Business</h3>
                        </div>
                        <div class="standard-blocks col s6 BuildingButton">
                            <h6 class="standard-block-headers">Smart <br/> Building</h6>
                        </div>
                        <div class=" standard-blocks col s6 CommunityButton">
                            <h3 class="standard-block-headers">Smart <br/>Community</h3>
                        </div>
                        <div class=" standard-blocks col s6 EducationButton">
                            <h3 class="standard-block-headers">Smart <br/> Education</h3>
                        </div>
                        <div class=" standard-blocks col s6 EnviromentButton">
                            <h3 class="standard-block-headers">Smart <br/>Environment</h3>
                        </div>
                        <div class=" standard-blocks col s6 GovernanceButton">
                            <h3 class="standard-block-headers">Smart <br/>Governance</h3>
                        </div>
                        <div class=" standard-blocks col s6 HealthCareButton">
                            <h3 class="standard-block-headers">Smart <br/>Healthcare</h3>
                        </div>
                        <div class=" standard-blocks col s6 InfrastrucureButton">
                            <h3 class="standard-block-headers">Smart <br/> Infrastructure</h3>
                        </div>
                        <div class=" standard-blocks col s6 MobilityButton">
                            <h3 class="standard-block-headers">Smart <br/>Mobility</h3>
                        </div>
                        <div class=" standard-blocks col s6 NjeButton">
                            <h3 class="standard-block-headers"></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--Image-->
        <div id="desktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="SmartSecondSection1">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/business.jpg" id="BusinessRow"/>
                    </div>
                    <h1 class="second-headers" style=" color:white;text-shadow: 5px 3px 1px #000000;">Smart
                        Business</h1>
                </div>
            </div>
        </div>
        </div>

        <div id="mobile">
        <div class="section">
            <div class="row">
                <div id="SmartSecondSection1">
                    <div>
                        <img src="/images/SmartCityImages/business.jpg" id="BusinessRow"/>
                    </div>
                    <h1 class="second-headers" style=" color:white;text-shadow: 5px 3px 1px #000000;">Smart
                        Business</h1>
                </div>
            </div>
        </div>
        </div>

        <div class="section center-align standard-section-with-margins " id="BusinessSection">
            <div class="Business">
                <div class="card blue darken-4 center">
                    <div class="card-content white-text hoverable">
                        <span class="center card-title" style="font-size:16px;color:white;"><b>SMART BUSINESS</b></span>
                    </div>
                </div>
            </div>
            <div class="white darken-1">
                <div class="standard-paragraphs center-align">
                    <p>
                        Investment is attracted by the ease of doing business and the quality of life of the people
                        working in the company. Propella supports the development of both industrial and IT products
                        which help businesses to take full advantage of the benefits of being situated in a Smart City
                        or town.
                    </p>
                </div>
            </div>

        </div>

        <div id="desktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="SmartThirdSection1">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/building.jpg" id="BuildImage"/>
                    </div>
                    <h1 class="third-headers" style=" color:white;text-shadow: 5px 3px 1px #000000;">Smart Building</h1>
                </div>
            </div>
        </div>
        </div>

        <div id="mobile"></div>
        <div class="section">
            <div class="row">
                <div id="SmartThirdSection1">
                    <div>
                        <img src="/images/SmartCityImages/building.jpg" id="BuildImage"/>
                    </div>
                    <h1 class="third-headers" style=" color:white;text-shadow: 5px 3px 1px #000000;">Smart Building</h1>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins " id="BuildingRow">
            <div class="Building">
                <div class="card blue darken-3">
                    <div class="card-content white-text hoverable">
                        <span class="center card-title" style="font-size:16px"><b>SMART BUILDING</b></span>
                    </div>
                </div>
            </div>
            <div class="white darken-3">
                <div class="standard-paragraphs center-align">
                    <p>
                        Propella has a Smart Building test bed on site. Solutions are developed for buildings of all
                        types and sizes, from small government (RDP) houses to multi-storey complexes.
                        A smart building intelligently integrates different physical management systems to ensure they
                        work together efficiently. Smart building management systems can reduce water and energy use,
                        minimise waste and improve security for residents and visitors.
                    </p>
                </div>
            </div>

        </div>

        <div id="desktop">
        <div class="section ">
            <div class="row">
                <div class="parallax-container" id="SmartFourthSection1">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/community.jpg" id="CommuImage"/>
                    </div>
                    <h1 class="fourth-headers" style=" color:white;text-shadow: 5px 3px 1px #000000;">Smart
                        Community</h1>
                </div>
            </div>
        </div>
        </div>

        <div id="mobile">
            <div class="section ">
                <div class="row">
                    <div id="SmartFourthSection1">
                        <div>
                            <img src="/images/SmartCityImages/community.jpg" id="CommuImage"/>
                        </div>
                        <h1 class="fourth-headers" style=" color:white;text-shadow: 5px 3px 1px #000000;">Smart
                            Community</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins " id="SmartCommunity">
            <div class="Community">
                <div class="card blue darken-3">
                    <div class="card-content white-text hoverable">
                        <span class="center card-title" style="font-size:16px"><b>SMART COMMUNITY</b></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">

            <div class="standard-paragraphs center-align">
                <p>
                    Technology has the power to enable municipalities to be more responsive to citizen needs in order to
                    enhance the quality of life of residents.Propella follows
                    a citizen-centric approach to the development of applications. Citizens expect transparent,
                    accessible and responsive services.
                    A survey by Accenture found that citizens increasingly want a personalised digital experience,
                    smartphone access and integration with social media.
                    More specifically, citizens want smartphone apps that provide convenient access to services and
                    information
                </p>
            </div>
        </div>

        <div id="desktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="SmartFifthSection1">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/education.jpg" id="SmartEducation"/>
                    </div>
                    <h1 class="fifth-headers" style="color:white;text-shadow: 5px 3px 1px #000000;">Smart Education</h1>
                </div>
            </div>
        </div>
        </div>

        <div id="mobile">
            <div class="section">
                <div class="row">
                    <div id="SmartFifthSection1">
                        <div>
                            <img src="/images/SmartCityImages/education.jpg" id="SmartEducation"/>
                        </div>
                        <h1 class="fifth-headers" style="color:white;text-shadow: 5px 3px 1px #000000;">Smart Education</h1>
                    </div>
                </div>
            </div>
        </div>


        <div class="section center-align standard-section-with-margins" id="SmartEducationBlock">
            <div class="card blue darken-3">
                <div class="card-content white-text hoverable" style="padding-left: 20%;">
                    <span class="center card-title"
                          style="font-size:16px;margin-right: 25px"><b>SMART EDUCATION</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    Smart Cities enable virtual learning and augmented reality to transform the way residents of all
                    ages and from all walks of life learn. Through its partnership with the Nelson Mandela University
                    Propella can connect entrepreneurs with internationally recognised education experts.
                </p>
            </div>
        </div>

        <div id="desktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="SmartEnvImage1">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/environment.jpg" id="EnviroImage"/>
                    </div>
                    <h1 class="sixth-headers" style="color:white;text-shadow: 5px 3px 1px #000000;">Smart
                        Environment</h1>
                </div>
            </div>
        </div>
        </div>
        <div id="mobile">
        <div class="section">
            <div class="row">
                <div id="SmartEnvImage1">
                    <div>
                        <img src="/images/SmartCityImages/environment.jpg" id="EnviroImage"/>
                    </div>
                    <h1 class="sixth-headers" style="color:white;text-shadow: 5px 3px 1px #000000;">Smart
                        Environment</h1>
                </div>
            </div>
        </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="SmartEnvinron">
            <div class="card blue darken-3">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>SMART ENVIRONMENT</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    Propella takes a holistic approach to developing the solutions needed to make cities and towns more
                    sustainable.

                </p>
            </div>
        </div>
        <div class="section center-align standard-section-with-margins" id="SmartEducationBlock">
            <div class="card blue darken-3">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>WASTE MANAGEMENT</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    Smart waste management systems reduce waste and sort waste at source. Propella supports the
                    development of Smart methods for
                    the proper handling of waste. This includes the conversion of waste into a resource for closed -
                    loop economies.

                </p>
            </div>
        </div>
        <div class="section center-align standard-section-with-margins" id="SmartEducationBlock">
            <div class="card blue darken-3">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>WATER MANAGEMENT</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    Almost all towns and cities around the world face challenges with the supply of potable water.
                    Propella supports the development and monetisation of Smart water management systems that use
                    digital technology to help save water, reduce costs and increase the reliability and transparency of
                    water distribution.

                </p>
            </div>
        </div>
        <div class="section center-align standard-section-with-margins" id="SmartEducationBlock">
            <div class="card blue darken-3">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>SMART ENERGY</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    Propella is supporting the development of smart energy innovations, such as distributed renewable
                    generation, microgrids, smart grid technologies, energy storage, automated demand response, virtual
                    power plants and demand - side innovations such as electric vehicles and smart appliances.
                </p>
            </div>
        </div>


        <div class="section center-align standard-top-section" id="GovImage">
            <div id="desktop">
            <div class="section standard-top-section">
                <div class="row">
                    <div class="parallax-container" id="SmartSeventhSection1">
                        <div class="parallax">

                            <img src="/images/SmartCityImages/governance 2.jpg" id="GovImage"/>
                        </div>
                        <h1 class="Seventh-headers" style="color: white;text-shadow: 5px 3px 1px #000000;">Smart Governance</h1>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <br/>
        <br/>

        <div id="mobile">
            <div class="row">
                <div id="SmartSeventhSection1">
                    <div>
                        <img src="/images/SmartCityImages/governance 2.jpg" id="GovImage"/>
                    </div>
                    <h1 class="Seventh-headers" style="color: white;text-shadow: 5px 3px 1px #000000;">Smart Governance</h1>
                </div>
            </div>
        </div>
        <div class="section center-align standard-section-with-margins" id="SmartEducationBlock">
            <div class="card blue darken-3">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>SMART GOVERNANCE</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    For developers of new technologies that improve urban governance through better use of information
                    and provide seamless communication between the municipality and the residents
                </p>
            </div>
        </div>


        <div id="desktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="SmartEigthSection1">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/health 1.jpg" id="HealthImage"/>
                    </div>
                    <h1 class="eighth-headers" style="text-shadow: 5px 3px 1px #000000; color:white;">Smart
                        Healthcare</h1>
                </div>
            </div>
        </div>
        </div>

        <div id="mobile">
        <div class="section">
            <div class="row">
                <div id="SmartEigthSection1">
                    <div>
                        <img src="/images/SmartCityImages/health 1.jpg" id="HealthImage"/>
                    </div>
                    <h1 class="eighth-headers" style="text-shadow: 5px 3px 1px #000000; color:white;">Smart
                        Healthcare</h1>
                </div>
            </div>
        </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card blue darken-3">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>SMART HEALTHCARE</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    Globally, the health sector is in transition as it seeks to address a myriad of challenges—from an
                    aging population and rising costs to outdated infrastructure and incompatible technologies and
                    protocols.
                    Managers of public health facilities in Smart Cities need ICT services that enable the provision of
                    intelligent,
                    data-driven medical services in order to make optimum use of the funds available.
                    Propella also supports the development of Smart medical devices that help with prevention and
                    treatment.
                </p>
            </div>
        </div>

        <div id="desktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="SmartNinethSection1">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/infrastructure.jpg" id="InfraImage"/>
                    </div>
                    <h1 class="nineth-headers" style=" color:white;text-shadow: 5px 3px 1px #000000; ">Smart
                        Infrastructure</h1>
                </div>
            </div>
        </div>
        </div>

        <div id="mobile">
        <div class="section">
            <div class="row">
                <div id="SmartNinethSection1">
                    <div>
                        <img src="/images/SmartCityImages/infrastructure.jpg" id="InfraImage"/>
                    </div>
                    <h1 class="nineth-headers" style=" color:white;text-shadow: 5px 3px 1px #000000; ">Smart
                        Infrastructure</h1>
                </div>
            </div>
        </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="Infrastructure">
            <div class="card blue darken-3">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>SMART INFRASTRUCTURE</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    Smart infrastructure provides the literal foundation for smart cities. The core underlying
                    characteristic of the elements that make up a Smart City (such as governance, mobility, buildings,
                    environmental management) is that they are connected and that they generate data. Propella helps
                    innovators to explore ways of using this data intelligently to ensure the optimal use of resources
                    and improve performance.
                </p>
            </div>
        </div>

        <div id="desktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="SmartTenthSection1">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/mobility.jpg" id="MobiImage"/>
                    </div>
                    <h1 class="tenth-headers" style=" color:white;text-shadow: 5px 3px 1px #000000; ">Smart
                        Mobility</h1>
                </div>
            </div>
        </div>
        </div>

        <div id="mobile">
        <div class="section">
            <div class="row">
                <div id="SmartTenthSection1">
                    <div>
                        <img src="/images/SmartCityImages/mobility.jpg" id="MobiImage"/>
                    </div>
                    <h1 class="tenth-headers" style=" color:white;text-shadow: 5px 3px 1px #000000; ">Smart
                        Mobility</h1>
                </div>
            </div>
        </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="SmartMob">
            <div class="card blue darken-3">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>SMART MOBILITY</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    Smart mobility solutions help reduce congestion and foster faster, greener and cheaper
                    transportation options. They include individual mobility systems such as bicycle sharing, ride
                    sharing (or carpooling), vehicle sharing and on - demand transportation.
                </p>
            </div>
        </div>


    </div>





    <!--SMART CITY DESKTOP SECTION-->
    <div class="programpage container-fluid" id="Desktopprogrampage">

        <div class="section standard-top-section">
            <div class="row">
                <div class="parallax-container" id="SmartTopSection">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/plain.jpg" id="SmartCityImage"/>
                    </div>
                    <h1 class="Top-smart-headers" style="color: white;">Smart City</h1>
                </div>
            </div>
        </div>


        <div class="section center-align standard-section-with-margins" id="AboutSection">
            <div id="AboutSectionContent">
                <div class="row">
                    <div class="col m4 left-align">
                        <h4 class="standard-header" align="center">About Smart City</h4>
                        <p class="standard-paragraphs" align="center">
                            Cities and towns have to get smart to manage a combination of influx from urban areas and
                            natural population growth in order to ensure that residents enjoy an increasing quality of
                            life.
                        </p>
                        <h4 class="standard-header" align="center">Mission</h4>
                        <p class="standard-paragraphs" align="center">
                            Our mission as a smart, hi-tech incubator is to support the successful development and
                            sustainability of innovation as the first choice for the
                            commercialisation of smart products and services for Smart Cities and Smart towns.
                        </p>
                    </div>
                    <div class="col m2"></div>
                    <div class="col m6 center-align">
                        <div class="row">
                            <div class=" standard-blocks col m4 BusinessButton">
                                <h3 class="standard-block-headers">Smart <br>Business </h3>

                            </div>
                            <div class="standard-blocks col m4 BuildingButton">
                                <h6 class="standard-block-headers">Smart<br/> Building</h6>
                            </div>
                            <div class=" standard-blocks col m4 CommunityButton">
                                <h3 class="standard-block-headers">Smart <br/>Community</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" standard-blocks col m4 EducationButton">
                                <h3 class="standard-block-headers">Smart <br/>Education</h3>
                            </div>
                            <div class=" standard-blocks col m4 EnviromentButton">
                                <h3 class="standard-block-headers">Smart Environment</h3>
                            </div>
                            <div class=" standard-blocks col m4 GovernanceButton">
                                <h3 class="standard-block-headers">Smart <br/> Governance</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" standard-blocks col m4 HealthCareButton">
                                <h3 class="standard-block-headers">Smart <br/>Healthcare</h3>
                            </div>
                            <div class=" standard-blocks col m4 InfrastrucureButton">
                                <h3 class="standard-block-headers">Smart Infrastructure</h3>
                            </div>
                            <div class=" standard-blocks col m4 MobilityButton">
                                <h3 class="standard-block-headers">Smart <br/>Mobility</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Image-->
        <div class="" id="business"></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section center-align standard-top-section">
            <div class="row">
                <div class="parallax-container" id="SmartSecondSection">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/business.jpg" id="BusinessRow"/>
                    </div>
                    <h1 class="Second-smart-header" style="color: white;">Smart Business</h1>

                </div>
            </div>
        </div>
        <div class="section center-align ">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="Bus">
                            <div class="">
                                <div class="card-content section center-align  ">
                                    <span class="" style="font-size:18px;color: white">Smart Business</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable">
                            <p>
                                &nbsp;&nbsp;&nbsp;Investment is attracted by the ease of doing business and the quality
                                of life of the people working in the company. Propella supports the development of both
                                industrial and IT products which help businesses to take full advantage of the benefits
                                of being situated in a Smart City or town.
                            </p>
                            <br>
                        </div>
                    </div>
                </div>

                @foreach($smart_business as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3">
                            <div class="card white smart hoverable" style="border-radius: 10px">
                                <div class="card-image">
                                    <img style="height: 30vh;border-radius: 10px"
                                         src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                </div>
                                <div class="card-content" style="height: 20vh">
                                    <h6 class="oswaldbody"
                                        style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                    <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                </div>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">

                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>



        {{--@* Building Section *@--}}
        <div class="" id="building"></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section center-align">
            <div class="row">
                <div class="parallax-container" id="SmartThirdSection">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/building.jpg" id="BuildImage"/>
                    </div>
                    <h1 class="Third-smart-header" style="color: white; ">Smart Building</h1>

                </div>
            </div>
        </div>

        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="Buildn">
                            <div class="">
                                <div class="card-content">
                                    <span class="card-title" style="font-size:18px;color: white">Smart Building</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable">
                            <p>
                                &nbsp;Propella has a Smart Building test bed on site. Solutions are developed for
                                buildings of all types and sizes, from small government (RDP) houses to multi-storey
                                complexes.
                                A smart building intelligently integrates different physical management systems to
                                ensure they work together efficiently. Smart building management systems can reduce
                                water and energy use, minimise waste and improve security for residents and visitors.
                            </p>
                            <br>
                        </div>
                    </div>
                    @foreach($smart_building as $venture)
                        @if($venture->status == 'Active')
                            <div class="col s12 m3">
                                <div class="card white smart hoverable" style="border-radius: 10px">
                                    <div class="card-image">
                                        <img style="height: 30vh;border-radius: 10px"
                                             src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                    </div>
                                    <div class="card-content" style="height: 20vh">
                                        <h6 class="oswaldbody"
                                            style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                        <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                    </div>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">

                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>


        {{--@* Community Section *@--}}
        <div class="" id="community"></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section center-align standard-top-section">
            <div class="section standard-top-section">
                <div class="row">
                    <div class="parallax-container" id="SmartFourthSection">
                        <div class="parallax">
                            <img src="/images/SmartCityImages/community.jpg" id="CommuImage"/>
                        </div>
                        <h1 class="Fourth-smart-header" style=" color: white;">Smart Community</h1>

                    </div>
                </div>
            </div>
        </div>
        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-3">
                        <div class="col s12 m3" id="Commune">
                            <div class="">
                                <div class="card-content white-text " style="height: 168px;">
                                    <span class="card-title" style="font-size:18px">Smart Community</span>

                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable">
                            <p>
                                Technology has the power to enable municipalities to be more responsive to citizen needs
                                in order to enhance the quality of life of residents. Propella follows a citizen-centric
                                approach to the development of applications. Citizens expect transparent, accessible and
                                responsive services. A survey by Accenture found that citizens increasingly want a
                                personalised digital experience, smartphone access and integration with social media.
                                More specifically, citizens want smartphone apps that provide convenient access to
                                services and information
                            </p>
                            <br>
                        </div>
                    </div>
                </div>
                @foreach($smart_community as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3">
                            <div class="card white smart hoverable" style="border-radius: 10px">
                                <div class="card-image">
                                    <img style="height: 30vh;border-radius: 10px"
                                         src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                </div>
                                <div class="card-content" style="height: 20vh">
                                    <h6 class="oswaldbody"
                                        style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                    <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                </div>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">

                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>


    <!--image-->
        <div class="" id="education"></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section center-align standard-top-section " id="SmartEduImage">
            <div class="section standard-top-section">
                <div class="row">
                    <div class="parallax-container" id="SmartFifthSection">
                        <div class="parallax">
                            <img src="/images/SmartCityImages/education.jpg" id="SmartEduImage"/>
                        </div>
                        <h1 class="Fifth-smart-headers" style="color: white; ">Smart Education</h1>

                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col 1"></div>
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m2" id="EducateC">
                            <div class="">
                                <div class="card-content black-text">
                                    <span class="card-title" style="font-size:18px;color: white">Smart Education</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable">
                            <p>
                                Smart Cities enable virtual learning and augmented reality to transform the way
                                residents of all ages and from all walks of life learn. Through its partnership with the
                                Nelson Mandela University Propella can connect entrepreneurs with internationally
                                recognised education experts.
                            </p>
                            <br>
                        </div>
                    </div>
                </div>
                @foreach($smart_education as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3">
                            <div class="card white smart hoverable" style="border-radius: 10px">
                                <div class="card-image">
                                    <img style="height: 30vh;border-radius: 10px"
                                         src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                </div>
                                <div class="card-content" style="height: 20vh">
                                    <h6 class="oswaldbody"
                                        style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                    <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                </div>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">

                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>


        {{--@* Envinronment Section *@--}}
        <div class="" id="enviro"></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section center-align standard-top-section " id="SmartEnvImage">
            <div class="section standard-top-section">
                <div class="row">
                    <div class="parallax-container" id="SmartSixthSection">
                        <div class="parallax">
                            <img src="/images/SmartCityImages/environment.jpg" id="SmartEnvImage"/>
                        </div>

                        <h1 class="Sixth-smart-headers" style="color: white; ">Smart Envinronment</h1>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align " id="Environ">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="EnviroDesktop">
                            <div class="">
                                <div class="card-content white-text ">
                                    <span class="card-title" style="font-size:18px">Smart Environment</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height:88px;">
                            <p>
                                Propella takes a holistic approach to developing the solutions needed to make cities and
                                towns more sustainable.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="EnviroDesktop">
                            <div class="">
                                <div class="card-content white-text ">
                                    <span class="card-title" style="font-size:18px; height: 65px; padding-top:5px;">Waste management</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content section standard-paragraphs hoverable">
                            <p>
                                Smart waste management systems reduce waste and sort waste at source. Propella supports
                                the development of Smart methods for
                                the proper handling of waste. This includes the conversion of waste into a resource for
                                closed - loop economies.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="EnviroDesktop">
                            <div class="">
                                <div class="card-content white-text ">
                                    <span class="card-title" style="font-size:18px;">Water management</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-contentsection standard-paragraphsstandard-paragraphs hoverable"
                             style="height: 90px;">
                            <p>
                                Almost all towns and cities around the world face challenges with the supply of potable
                                water. Propella supports the development and monetisation of Smart water management
                                systems that use digital technology to help save water, reduce costs and increase the
                                reliability and transparency of water distribution.
                                <br/>
                                <br/>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="EnviroDesktop">
                            <div class="">
                                <div class="card-content white-text ">
                                    <span class="card-title" style="font-size:18px; height:65px; padding-top:5px;">Smart energy </span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable">
                            <p>
                                Propella is supporting the development of smart energy innovations, such as distributed
                                renewable generation, microgrids, smart grid technologies, energy storage, automated
                                demand response, virtual power plants and demand - side innovations such as electric
                                vehicles and smart appliances.
                            </p>
                        </div>
                    </div>
                </div>
                @foreach($smart_governance as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3">
                            <div class="card white smart hoverable" style="border-radius: 10px">
                                <div class="card-image">
                                    <img style="height: 30vh;border-radius: 10px"
                                         src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                </div>
                                <div class="card-content" style="height: 20vh">
                                    <h6 class="oswaldbody"
                                        style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                    <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                </div>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">

                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>


        {{--@* GovernanceRow Section *@--}}
        <div class="" id="gov"></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section center-align standard-top-section" id="GovImage">
            <div class="section standard-top-section">
                <div class="row">
                    <div class="parallax-container" id="SmartSeventhSection">
                        <div class="parallax">

                            <img src="/images/SmartCityImages/governance 2.jpg" id="GovImage"/>
                        </div>
                        <h1 class="Seventh-smart-headers" style="color: white;">Smart Governance</h1>

                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="Goven">
                            <div class="">
                                <div class="card-content white-text ">
                                    <span class="card-title" style="font-size:18px">Smart Governance</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content black-text hoverable" style="height:120px;">
                            <p>
                                For developers of new technologies that improve urban governance through better use of
                                information and provide seamless communication between the municipality and the
                                residents
                            </p>
                        </div>
                    </div>
                </div>
                @foreach($smart_governance as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3">
                            <div class="card white smart hoverable" style="border-radius: 10px">
                                <div class="card-image">
                                    <img style="height: 30vh;border-radius: 10px"
                                         src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                </div>
                                <div class="card-content" style="height: 20vh">
                                    <h6 class="oswaldbody"
                                        style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                    <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                </div>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">

                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

        </div>


        {{--@* HealthCare Section *@--}}
        <div class="" id="health"></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section center-align standard-top-section " id="HealthImage">
            <div class="section standard-top-section">
                <div class="row">
                    <div class="parallax-container" id="SmartEigthSection">
                        <div class="parallax">
                            <img src="/images/SmartCityImages/health 1.jpg" id="HealthImage"/>
                        </div>
                        <h1 class="Eight-smart-headers" style="color: white; ">Smart HealthCare</h1>

                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="HealthDiv">
                            <div class="">
                                <div class="card-content white-text " style="height:130px">
                                    <span class="card-title" style="font-size:18px">Smart HealthCare</span>

                                </div>
                            </div>
                        </div>
                        <div class="card-content black-text hoverable">
                            <p>
                                Globally, the health sector is in transition as it seeks to address a myriad of
                                challenges—from an aging population and rising costs to outdated infrastructure and
                                incompatible technologies and protocols. Managers of public health facilities in Smart
                                Cities need ICT services that enable the provision of intelligent, data-driven medical
                                services in order to make optimum use of the funds available. Propella also supports the
                                development of Smart medical devices that help with prevention and treatment.
                            </p>
                        </div>
                    </div>
                </div>
                @foreach($smart_healthcare as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3">
                            <div class="card white smart hoverable" style="border-radius: 10px">
                                <div class="card-image">
                                    <img style="height: 30vh;border-radius: 10px"
                                         src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                </div>
                                <div class="card-content" style="height: 20vh">
                                    <h6 class="oswaldbody"
                                        style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                    <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                </div>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">

                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>


        {{--@* Infrastructure Section *@--}}
        <div class="" id="infra1"></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section center-align standard-top-section " id="InfraImage">
            <div class="section standard-top-section">
                <div class="row">
                    <div class="parallax-container" id="SmartNinethSection">
                        <div class="parallax">
                            <img src="/images/SmartCityImages/infrastructure.jpg" id="InfraImage"/>
                        </div>
                        <h1 class="Nineth-smart-headers" style="color: white;">Smart Infrastructure</h1>

                    </div>
                </div>
            </div>
        </div>
        <br/>

        <div class="section center-alig">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="infra">
                            <div class="">
                                <div class="card-content black-text ">
                                    <span class="card-title"
                                          style="font-size:18px;color:white;">Smart Infrastructure</span>

                                </div>
                            </div>
                        </div>
                        <div class="card-content black-text hoverable" style="height:120px">
                            <p>
                                Smart infrastructure provides the literal foundation for smart cities. The core
                                underlying characteristic of the elements that make up a Smart City (such as governance,
                                mobility, buildings, environmental management) is that they are connected and that they
                                generate data. Propella helps innovators to explore ways of using this data
                                intelligently to ensure the optimal use of resources and improve performance.
                            </p>
                        </div>
                    </div>
                </div>
                @foreach($smart_infrastructure as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3">
                            <div class="card white smart hoverable" style="border-radius: 10px">
                                <div class="card-image">
                                    <img style="height: 30vh;border-radius: 10px"
                                         src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                </div>
                                <div class="card-content" style="height: 20vh">
                                    <h6 class="oswaldbody"
                                        style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                    <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                </div>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">

                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>


        {{--@* MobilityRow Section *@--}}
        <div class="" id="mob"></div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section center-align standard-top-section " id="MobiImage">
            <div class="section standard-top-section">
                <div class="row">
                    <div class="parallax-container" id="SmartTenthSection">
                        <div class="parallax">
                            <img src="/images/SmartCityImages/mobility.jpg" id="MobiImage"/>
                        </div>
                        <h1 class="Tenth-smart-headers" style="color: white;">Smart Mobility</h1>

                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="Mob">
                            <div class="">
                                <div class="card-content white-text ">
                                    <span class="card-title" style="font-size:18px">Smart Mobility</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content black-text hoverable" style="height:120px">
                            <p>
                                Smart mobility solutions help reduce congestion and foster faster, greener and cheaper
                                transportation options. They include individual mobility systems such as bicycle
                                sharing, ride sharing (or carpooling), vehicle sharing and on - demand transportation.
                            </p>
                        </div>
                    </div>
                </div>
                @foreach($smart_mobility as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3">
                            <div class="card white smart hoverable" style="border-radius: 10px">
                                <div class="card-image">
                                    <img style="height: 30vh;border-radius: 10px"
                                         src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                </div>
                                <div class="card-content" style="height: 20vh">
                                    <h6 class="oswaldbody"
                                        style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                    <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                </div>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">

                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

        </div>
    </div>
    <script>
        //Card onclick
        $('.smart').each(function () {

            let venture_id = $(this).find('.venture_id').attr('data-value');

            $(this).on('click', function () {
                location.href = '/Home/Founders/' + venture_id;
            });
        });


        $(document).ready(function () {
            $('.parallax').parallax();

        });


        {{--$('#indFounder').on('click', function () {--}}
        {{--location.href="{{url('/Home/Founders/'.$venture->id)}}";--}}
        {{--});--}}

        $(document).ready(function () {
            $('.BusinessButton').on('click', function () {
                window.location.href = "#business";
            });
        });

        $(document).ready(function () {
            $('.BuildingButton').on('click', function () {
                location.href = "#building";
            });
        });

        $(document).ready(function () {
            $('.CommunityButton').on('click', function () {
                location.href = "#community";
            });
        });

        $(document).ready(function () {
            $('.EducationButton').on('click', function () {
                location.href = "#education";
            });
        });

        $(document).ready(function () {
            $('.EnviromentButton').on('click', function () {
                location.href = "#enviro";
            });

        });

        $(document).ready(function () {
            $('.GovernanceButton').on('click', function () {
                location.href = "#gov";
            });

        });

        $(document).ready(function () {
            $('.HealthCareButton').on('click', function () {
                location.href = "#health";
            });

        });

        $(document).ready(function () {
            $('.InfrastrucureButton').on('click', function () {
                location.href = "#infra1";
            });

        });

        $(document).ready(function () {
            $('.MobilityButton').on('click', function () {
                location.href = "#SmartTenthSection";
            });

        });

        //Mobile

        $(document).ready(function () {
            $('.BusinessButton').on('click', function () {
                window.location.href = "#SmartSecondSection1";
            });
        });

        $(document).ready(function () {
            $('.BuildingButton').on('click', function () {
                location.href = "#SmartThirdSection1";
            });
        });

        $(document).ready(function () {
            $('.CommunityButton').on('click', function () {
                location.href = "#SmartFourthSection1";
            });
        });

        $(document).ready(function () {
            $('.EducationButton').on('click', function () {
                location.href = "#SmartFifthSection1";
            });
        });

        $(document).ready(function () {
            $('.EnviromentButton').on('click', function () {
                location.href = "#SmartEnvImage1";
            });

        });

        $(document).ready(function () {
            $('.GovernanceButton').on('click', function () {
                location.href = "#SmartSeventhSection1";
            });

        });

        $(document).ready(function () {
            $('.HealthCareButton').on('click', function () {
                location.href = "#SmartEigthSection1";
            });

        });

        $(document).ready(function () {
            $('.InfrastrucureButton').on('click', function () {
                location.href = "#SmartNinethSection1";
            });

        });

        $(document).ready(function () {
            $('.MobilityButton').on('click', function () {
                location.href = "#mob";
            });

        });
    </script>
    </body>
@endsection

