@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/News/news.css"/>

    <div class="row" style="margin-top: 7vh" id="desktopNews">

        <div class="col m9" style="margin-left: 7em">
            <br>
            <div id="crouton" style="margin-left: 2em">
                <ul>
                    <li><a href="/news">News</a></li>
                    <li><a href="#">{{$propellaNew->title}}</a></li>
                </ul>
            </div>
            <br>
            <br>
            <div class="" style="margin-left: 3em;margin-right: 3em">
                <img class="materialboxed" style="width:100%;height: 50%;" src="/storage/{{isset($propellaNew->news_image_url)?$propellaNew->news_image_url:'Nothing Detected'}}">
                <p style="color: #039be5;font-size: 1.5em"><b>{{$propellaNew->title}}</b></p>
                <p class="date" style="color: gray"><b>Date : {{$propellaNew->added_date}}</b></p>
                <p>{{$propellaNew->description}}</p>

                <div class="row read"  style="margin-left: 0.1em;">
                    {!!$propellaNew->propellaNewsContent->content!!}
                </div>
            </div>
        </div>
    </div>


    <!--Mobile-->
    <div class="container-fluid" id="mobileNews">
        <div class="row" style="margin-top: 10vh;margin-left: 1em;margin-right: 1em">
            <div id="crouton">
                <ul>
                    <li><a href="/media#top-news">News</a></li>
                    <li><a href="#">{{$propellaNew->title}}</a></li>
                </ul>
            </div>
            <img style="width:100%;height: 250px;" src="/storage/{{isset($propellaNew->news_image_url)?$propellaNew->news_image_url:'Nothing Detected'}}">
            <p style="color: blue;"><b>{{$propellaNew->title}}</b></p>
            <p >{{$propellaNew->description}}</p>
        </div>
        <a class="redMore" style="cursor: pointer">Read more</a>

        <div class="row read" hidden style="margin-left: 0.1em;">
            {!!$propellaNew->propellaNewsContent->content!!}
        </div>
    </div>
    <style>
        #crouton ul {
            margin: 0;
            padding: 0;
            overflow: hidden;
            width: 100%;
            list-style: none;
        }

        #crouton li {
            float: left;
            margin: 0 10px;
        }

        #crouton a {
            background: #ddd;
            padding: .7em 1em;
            float: left;
            text-decoration: none;
            color: #444;
            text-shadow: 0 1px 0 rgba(255,255,255,.5);
            position: relative;
        }

        #crouton li:first-child a {
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        #crouton li:last-child a {
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        #crouton a:hover {
            background: #99db76;
        }

        #crouton li:not(:first-child) > a::before {
            content: "";
            position: absolute;
            top: 50%;
            margin-top: -1.5em;
            border-width: 1.5em 0 1.5em 1em;
            border-style: solid;
            border-color: #ddd #ddd #ddd transparent;
            left: -1em;
        }

        #crouton li:not(:first-child) > a:hover::before {
            border-color: #99db76 #99db76 #99db76 transparent;
        }

        #crouton li:not(:last-child) > a::after {
            content: "";
            position: absolute;
            top: 50%;
            margin-top: -1.5em;
            border-top: 1.5em solid transparent;
            border-bottom: 1.5em solid transparent;
            border-left: 1em solid #ddd;
            right: -1em;
        }

        #crouton li:not(:last-child) > a:hover::after {
            border-left-color: #99db76;
        }
        hr {
            position: relative;
            top: 40px;
            border: none;
            height: 6px;
            background: black;
            margin-bottom: 30px;
        }
    </style>


    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();
        });
        $('.redMore').on('click', function () {
            $('.read').show();
        });
        function navigateNews(obj){
            let news_id= obj.getAttribute('data-value');
            window.location.href = '/detailed-news/' + news_id;
        }

        $('.date').each(function () {
            var max_length = 20;

            if ($(this).html().length > max_length) {

                var short_content = $(this).html().substr(0, max_length);
                var long_content = $(this).html().substr(max_length);

                $(this).html(short_content +
                    '<i href="#" class="more_horiz material-icons small"></i>' +

                    '<span class="more_text" style="display:none;">' + long_content + '</span>');

            }

        });

    </script>

@endsection
