@extends('layouts.app')

@section('content')
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/css/About/AboutMain.css"/>

    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>
    </head>
    <!--About Top Section: Image-->
    <!--Top Section-->
    <body>

    <div id="pic2"></div>

    <div class="section ">
        <div class="row">
            <div class="parallax-container" id="aboutThirdSection">
                <div class="parallax">
                    <img class="cover " src="/images/About-Resized-Images/OUR-PEOPLE.jpg" width="242" height="403" id="thirdImage">
                </div>
                <h1 class="about-section-header3">Our People</h1>
            </div>
        </div>
    </div>
    <br />

    <div class="section center-align standard-section-with-margins" id="ourPeopleDesktop">
        <div class="row left-align">
            <div class="col s4" id="ourPeopleDesktop">
                <div class="standard-left-buttons  BoardnGuestsBtn">
                    <h6 class="standard-left-button-headers ">Board &amp; Guests</h6>
                </div>
                <div class="standard-left-buttons  ManagementBtn">
                    <h6 class="standard-left-button-headers ">Management</h6>
                </div>
                <div class="standard-left-buttons  StaffBtn">
                    <h6 class="standard-left-button-headers ">Team</h6>
                </div>
                <div class="standard-left-buttons MentorsBtn">
                    <h6 class="standard-left-button-headers ">Mentors</h6>
                </div>
            </div>

            <div class="col s1 "></div>

            <style>
                .container {
                    position: relative;
                    width: 20%;
                    margin-left: 40px;
                }

                .image {
                    opacity: 1;
                    display: block;
                    width: auto;
                    height: auto;
                    transition: .5s ease;
                    backface-visibility: hidden;
                }

                .middle {
                    transition: .5s ease;
                    opacity: 0;
                    position: absolute;
                    top: 50%;
                    left: 200%;
                    transform: translate(-50%, -50%);
                    -ms-transform: translate(-50%, -50%);
                    text-align: center;
                }

                .container:hover .image {
                    opacity: 0.3;
                }

                .container:hover .middle {
                    opacity: 1;
                    top:30vh;
                }

                .text {
                    backdrop-filter: blur(10px);
                    color: black;
                    font-size: 1em;
                    padding: 16px 32px;
                    width: 200px;

                }
            </style>

            <!---->
            <div class="col s7">
                <!--Board and guest info-->
                <div class="boardAndGuests" hidden>
                    <h4 style="text-align: center;color: black; background-color: lightgray;width: 677px;margin-left: 5.5%;height: 7vh"><b>Board and Guests</b></h4>
                    <div class="row">
                        <a href=" https://www.linkedin.com/in/barry-wiseman-04796a9/" target="_blank"><div class="col m4">
                                <div class="container">
                                    <img src="/images/BarryEdited.png" alt="Avatar" class="image"
                                         style="width:400%;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Barry Wiseman</div>
                                        <div class="text">Engeli Enterprise Development</div>

                                    </div>
                                </div>
                            </div></a>

                        <a href="https://www.linkedin.com/in/celeste-thomas-18854577//" target="_blank"><div class="col m4">
                                <div class="container">
                                    <img src="/images/Celeste_Thomas-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:520%;height: 36vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Celeste Thomas</div>
                                        <div class="text">Algoa FM</div>

                                    </div>
                                </div>
                            </div></a>
                        <a href="https://www.linkedin.com/in/celeste-thomas-18854577//" target="_blank"><div class="col m4">
                                <div class="container">
                                    <img src="/images/ProfilePictures/Propella Staff/Ricardo-remove.png" alt="Avatar" class="image"
                                         style="width:400%;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Dr Ricardo Dames</div>
                                        <div class="text">Engeli Enterprise Development</div>

                                    </div>
                                </div>
                            </div></a>
                    </div>
                    <br>

                    <div class="row">
                        <a href="https://www.linkedin.com/in/nqobile-gumede-m-inst-d-7b026030/" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Nqobile_Gumede-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Dr Nqobile Gumede</div>
                                        <div class="text">Nelson Mandela University</div>

                                    </div>
                                </div>
                            </div></a>

                        <a href="https://www.linkedin.com/in/gregory-wood-920a58a7/" target="_blank"><div class="col m4">
                                <div class="container">
                                    <img src="/images/Gregory_Wood_587_-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:480%;height: 38vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Gregory Wood</div>
                                        <p class="text">ISUZU</p>
                                    </div>
                                </div>
                            </div></a>
                        <a href=" https://www.linkedin.com/in/linda-brown-basf/" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Linda_594_-removebg-preview (1).png" alt="Avatar" class="image"
                                         style="width:450%;height: 38vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Linda Brown</div>
                                        <p class="text">BASF</p>
                                    </div>
                                </div>
                            </div></a>
                        <br>
                    </div>

                    <br>
                    <div class="row">
                        <a href="https://www.linkedin.com/in/mariam-jogee-jamal-ca-sa-0a6b0748/" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Mariam_Jogee-Jamal-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:450%;height: 38vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Mariam Jogee-Jamal</div>
                                        <p class="text">Nelson Mandela University</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <br>
                    </div>


                    <div class="row"hidden>
                        <h4 style="text-align: center;">Board and Guests</h4>
                        <div class="col m3" id="AndrewImage">
                            <div id="Details"style=" margin-top:40vh";>
                                <p class="detailsStyle"><b>Prof Andrew Leitch</b></p>
                                <p class="detailsStyle">Nelson Mandela University</p>
                            </div>
                        </div>
                        <div class="col m3" id="JohanImage">
                            <div id="Details"style=" margin-top:40vh";>
                                <p class="detailsStyle"><b>Johan Wasserman</b></p>
                                <p class="detailsStyle">Nelson Mandela University</p>
                            </div>
                        </div>
                        <div class="col m3" id="WayneImage">
                            <div id="Details"style=" margin-top:40vh";>
                                <p class="detailsStyle"><b>Wayne Oosthuizen</b></p>
                                <p class="detailsStyle">Engeli Enterprise Development</p>
                            </div>
                        </div>
                        <div class="col m3" id="RicardoImage">
                            <div id="Details"style=" margin-top:40vh";>
                                <p class="detailsStyle"><b>Dr Ricardo Dames</b></p>
                                <p class="detailsStyle">Engeli Enterprise Development</p>
                            </div>
                        </div>
                        <div class="col m3" id="BarryImage">
                            <div id="Details"style=" margin-top:40vh";>
                                <p class="detailsStyle"><b>Barry Wiseman</b></p>
                                <p class="detailsStyle">Engeli Enterprise Development</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" hidden>
                    <div class="col m3" id="GregImage">
                        <div id="Details">
                            <p>Greg Wood</p>
                            <p>Isuzu Motors South Africa</p>
                        </div>
                    </div>
                    <div class="col m3" id="StuartImage">
                        <div id="Details">
                            <p>Stuart Carliell</p>
                            <p>BASF</p>
                        </div>
                    </div>
                    <div class="col m3" id="PortiaImage">
                        <div id="Details">
                            <p>Portia Maurice</p>
                            <p>Telkom</p>
                        </div>
                    </div>
                    <div class="col m3" id="NqobileImage">
                        <div id="Details">
                            <p>Dr Nqobile Gumede</p>
                            <p>Innovation Office, NMU</p>
                        </div>
                    </div>
                </div>

                <div class="row" hidden>
                    <div class="col m3" id="BarryImage">
                        <div id="Details">
                            <p>Barry Wiseman</p>
                            <p>Engeli Enterprise Development</p>
                        </div>
                    </div>
                    <div class="col m3">
                        <div id="Details">

                        </div>
                    </div>
                    <div class="col m3">
                        <div id="Details">

                        </div>
                    </div>
                    <div class="col m3">
                        <div id="Details">

                        </div>
                    </div>
                </div>

                <!--MANAGEMENT-->
                <div class="managementInfo" hidden>
                    <h4 style="text-align: center;color: black; background-color: lightgray;width: 677px;margin-left: 5.5%;height: 7vh"><b>Management</b></h4>
                    <div class="row">
                        <a href="https://www.linkedin.com/in/anita-palmer-a659b07" target="_blank"><div class="col m4">
                            <div class="container">
                                <img src="/images/New_Team_Images/Anita.JPG"alt="Avatar" class="image" style="width:400%">
                                <div class="middle">
                                    <div class="text">Anita Palmer</div>
                                    <div class="text">Business Incubator Manager</div>

                                </div>
                            </div>
                        </div></a>
                        <a href="https://www.linkedin.com/in/errol-wills-13a30121" target="_blank"><div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Errol.JPG" alt="Avatar" class="image" style="width:400%">
                                    <div class="middle">
                                        <div class="text">Errol Wills</div>
                                        <div class="text">Senior ICT Advisor</div>

                                    </div>
                                </div>
                        </div></a>
                        <a href="https://www.linkedin.com/in/grant-minnie-85898719" target="_blank"><div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Grant.JPG" alt="Avatar" class="image" style="width:400%;height: 200%;">
                                    <div class="middle">
                                        <div class="text">Grant Minnie</div>
                                        <div class="text">Senior Industrial Advisor</div>
                                    </div>
                                </div>
                        </div></a>
                    </div>
                    <br>
                    <div class="row">
                        <a href="https://www.linkedin.com/in/lindalawriecoaching/" target="_blank"><div class="col m4">
                            <div class="container">
                                <img src="/images/New_Team_Images/Linda.JPG"alt="Avatar" class="image" style="width:400%">
                                <div class="middle">
                                    <div class="text">Linda Lawrie</div>
                                    <div class="text">Incubation Journey Facilitator</div>
                                </div>
                            </div>
                        </div></a>

                        <div class="col m4 ">
                            <div class="container">
                                <img src="/images/Nafeesa_Dinie-removebg-preview.png" alt="Avatar" class="image"
                                     style="width:400%;height: 36vh;background-color: lightgray">
                                <div class="middle">
                                    <div class="text">Nafeesa Dinie</div>
                                    <div class="text">PTI Manager</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!--TEAM-->
                <div class="staffInfo " hidden>
                    <h4 style="text-align: center;color: black; background-color: lightgray;width: 695px;margin-left: 4%;height: 7vh"><b>Team</b></h4>
                    <div class="row">
                        <div class="row">
                            <div class="col m4 ">
                                <div class="container  ">
                                    <img  src="/images/New_Team_Images/Mara.JPG" alt="Avatar" class="image"
                                          style="width:400%">
                                    <div class="middle">
                                        <div class="text">Mara Jacobs</div>
                                        <div class="text">Administrator</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Apelele.JPG" alt="Avatar" class="image"
                                         style="width:400%;height: 200%;">
                                    <div class="middle">
                                        <div class="text">Aphelele Jonas</div>
                                        <div class="text">Marketing</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Sinazo j.JPG" alt="Avatar" class="image"
                                         style="width:400%">
                                    <div class="middle">
                                        <div class="text">Sinazo Jack</div>
                                        <div class="text">Administrator</div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Unathi new.jpeg" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Unathi Mdliva</div>
                                        <div class="text">Software Engineer</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Sinazo q.JPG" alt="Avatar" class="image"
                                         style="width:400%">
                                    <div class="middle">
                                        <div class="text">Sinazo Qomve</div>
                                        <div class="text">Software Engineer</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Sinothando new.jpeg" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Sinothando Ncede</div>
                                        <div class="text">Software Engineer</div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Linamandla.jpeg" alt="Avatar" class="image"
                                         style="width:430%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Linamandla</div>
                                        <div class="text">Administrator</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Gwen.JPG" alt="Avatar" class="image"
                                         style="width:400%">
                                    <div class="middle">
                                        <div class="text">Gwen Assam</div>
                                        <div class="text">Facilities</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/image00001[3641].jpeg" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Sikhona Mbashe</div>
                                        <div class="text">Graphic Designer</div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <br>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Randy new.jpeg" alt="Avatar" class="image"
                                         style="width:430%;height: 35vh">
                                    <div class="middle">
                                        <div class="text">Randy Nqweniso</div>
                                        <div class="text">Receptionist</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Zenande new.jpeg" alt="Avatar" class="image"
                                         style="width:430%;height: 35vh">
                                    <div class="middle">
                                        <div class="text">Zenande Bhidli</div>
                                        <div class="text">Marketing</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!--Mentors-->
                <div class="mentorsInfo " id="pti">
                    <div class="row">
                        <h4 style="text-align: center;color: black; background-color: lightgray;width: 700px;margin-left: 4%">
                            <b>Mentors</b></h4>
                        <a href="https://www.linkedin.com/in/carl-liddle-b137a821/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/ProfilePictures/PTI Staff/CarlLiddle-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Carl Liddle</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="https://www.linkedin.com/in/daryl-mcwilliams-6b4b71b/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/darry-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Daryl McWilliams</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="https://www.linkedin.com/in/ed-richardson-894b021/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/Ed_Richardson___17__586_-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Ed Richardson</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <br>
                    <div class="row">
                        <a href=": https://www.linkedin.com/in/george-meiring-6469ba22/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/George-Meiring_2304_-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">George Meiring</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="https://www.linkedin.com/in/marvin-draai-b0477242/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/ProfilePictures/PTI Staff/MarvinDraai-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Marvin Draai</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="https://www.linkedin.com/in/nick-marriott-83649053/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/Mentors/NickMarriot3.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Nick	Marriott</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <br>
                    <div class="row">
                        <a href="https://www.linkedin.com/in/sanjiv-ranchod-1858046/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/ProfilePictures/PTI Staff/SanjivRanchod-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Sanjiv Ranchod</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="https://www.linkedin.com/in/wayne-oosthuizen-6149113/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/Mentors/WayneOosthuizen4.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Wayne	Oosthuizen</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="https://www.linkedin.com/in/lindalawriecoaching/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/LindaL.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Linda Lawrie</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <br>
                    <div class="row">
                        <a href="https://www.linkedin.com/in/sandisiwe-kiti-69106733/">
                            <div class="col m4">
                                <div class="container" style="height: 150%">
                                    <img src="/images/Sandisiwe.png" alt="Avatar" class="image"
                                         style="width:400%;height: 35vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Sandisiwe Kiti</div>
                                        <div class="text">Mentor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            //Board and guests button
            $('.BoardnGuestsBtn').on('click', function () {
                $('.boardAndGuests').show();
                $('.managementInfo').hide();
                $('.staffInfo ').hide();
                $('.mentorsInfo').hide();
            });

            //Management button click
            $('.ManagementBtn').on('click', function () {
                $('.managementInfo').show();
                $('.boardAndGuests').hide();
                $('.staffInfo ').hide();
                $('.mentorsInfo').hide();
            });
            //Staff button click
            $('.StaffBtn').on('click', function () {
                $('.staffInfo ').show();
                $('.managementInfo').hide();
                $('.boardAndGuests').hide();
                $('.mentorsInfo').hide();
            });
            //Mentors button click
            $('.MentorsBtn').on('click', function () {
                $('.mentorsInfo').show();
                $('.staffInfo ').hide();
                $('.managementInfo').hide();
                $('.boardAndGuests').hide();
            });
        });
    </script>
    </body>
@endsection
