@extends('layouts.booking-layout')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>

    <div class="row">
        @foreach($venues as $venue)
            <div class="col  l3 m3 s12 block-cards white-text" id="{{$venue->venue_name}}"
                 style=" border-left: 3px solid grey;border-bottom: 3px solid grey;height: 90%;background-color: forestgreen;">
                <div class="" style="margin-top: 16vh">
                    <p class="center-align" style="margin-bottom: 5vh"><b>{{$venue->venue_name}}</b></p>
                    <p class="center-align" style="margin-bottom: 5vh"><b>{{$venue->number_of_seats}}</b></p>
                </div>

            </div>
        @endforeach
    </div>

    <script>
        $(document).ready(function (data) {
            checkBooking();
        });
        function checkBooking(){
            $.ajax({
                url: '/get-booking-system/',
                type: 'GET',
                success: function(data) {
                    for(let i = 0; i < data.length; i ++){
                        console.log(data[i].venue_name)
                    }
                    let slides = document.getElementsByClassName("block-cards");
                    let BackgroundColor="blue";
                    for(let a = 0; a < slides.length; a++)
                    {
                        for (let i = 0; i < data.length; i++) {
                            if (data[i].venue_name === slides[a].id) {
                                console.log(data[i])
                                slides[a].style.backgroundColor=BackgroundColor;
                            }
                        }
                    }
                    setTimeout(function () {
                        checkBooking()
                    }, 60000);
                }
            });
        }

    </script>

@endsection
