@extends('layouts.app')

@section('content')
    <h4 class="center-align" style="margin-top: 6vh"><b>Testimonials</b></h4>

    <div class="row" style="margin-top: 5vh;margin-left: 3em;margin-right: 3em">
        <div class="col s6">
            <div class="card" style="height: 60vh">
                <div class="card-image">
                    <img  style="width: 100px;height: 100px;border-radius: 100px;margin: auto" src="images/Denise.png" alt="">
                </div>
                <div class="card-content">
                    <h3 class="title center-align">Denise van Huyssteen</h3>
                    <p class="center-align"><b>Isuzu Motors South Africa</b></p>
                    <p class="center-align">
                        As Isuzu Motors South Africa we value the support which we get from Propella for
                        some of
                        our key enterprise development programmes, as well with initiatives to further build
                        supplier capacity.
                        Propella has demonstrated excellent innovation in finding solutions to addressing
                        some
                        of the challenges which we face in our industry. Also very importantly,
                        their team members are very engaged, energetic and responsive to meeting the needs
                        of
                        our business.
                    </p>
                </div>
            </div>
        </div>
        <div class="col s6">
            <div class="card" style="height: 60vh">
                <div class="card-image">
                    <img  style="width: 100px;height: 100px;border-radius: 100px;margin: auto" src="images/Linda[594].jpg" alt="">
                </div>
                <div class="card-content">
                    <h3 class="title center-align">Linda Brown</h3>
                    <p class="post center-align"><b>BASF</b></p>
                    <p class="description center-align">
                        BASF South Africa is proud to be associated with the Propella Incubator in Port
                        Elizabeth. At BASF, we create chemistry for a sustainable future through encouraging
                        innovative and disruptive
                        thinking which then translates into practical solutions. Innovation is a key lens of
                        BASF’s global strategy, and it is only through innovation
                        that we can solve the challenges faced by business and society – today and into the
                        future. The Propella Incubator does exactly this – with a
                        sense of energy that is contagious. I would encourage any corporate that supports
                        creative entrepreneurship to get involved and be inspired by
                        the young minds and talent that are involved in the Propella programs.
                    </p>
                </div>
            </div>
        </div>
    </div>

<br>
    <br>

@endsection
