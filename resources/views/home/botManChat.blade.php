@extends('layouts.app')

@section('content')
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>COINS ChatBot</title>
         <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/assets/css/chat.min.css">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>
    <body>
    <!-- End Google Tag Manager (noscript) -->
    <script>
        // Feel free to change the settings on your need
        var botmanWidget = {
            frameEndpoint: '/chat.html',
            chatServer: 'chat.php',
            title: 'ChatBot',
            introMessage: 'Hi and welcome to our chatbot how can i help you ?',
            placeholderText: 'Ask Me Something',
            mainColor: '#F28240',
            bubbleBackground: '#F28240',
            aboutText: 'Powered By Eluert Mukja',
            aboutLink: 'mhdevelopment.gr',
            bubbleAvatarUrl: 'https://www.applozic.com/assets/resources/images/Chat-Bot-Icon@512px.svg',
        };
    </script>
    </body>
    @endsection
