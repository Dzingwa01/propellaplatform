@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/covidForm/covid.css"/>
    <br>
    <br>
    <form id="desktop" method="post" action="{{url('store-covid-form')}}" enctype="multipart/form-data"
          class="form-horizontal">
        @csrf
        @method('post')
        <div class="row"{{-- style="margin-top: 10vh"--}}>
            <div class="col s12 m8">
                <div class="card " {{--style="margin-left: 400px"--}}>
                    <br>
                    <div class="row" style="margin-left: 100px">
                        <div class="col s3">
                            <img src="/images/Propella_Logo.jpg">
                        </div>
                        <div class="col s1"></div>
                        <div class="col s3">
                            <img style="height: 110px;width:280px;" src="/images/About/LOGOS/ENGELI.jpg">
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row" style="margin-left: 2em;margin-right: 2em">
                            <div class="">
                                <p>The safety of all employees, visitors, suppliers, service providers, and the public in
                                    general are our priority.</p>
                                <p>Due to the coronavirus disease 2019 (COVID-19) outbreak and the spread thereof, we
                                    have
                                    implemented measures
                                    to monitor and reduce the risk of transmission</p>
                                <p>To prevent the spread of COVID-19, as well as the potential risk of explosureto our
                                    workforce and visitors, we request that you complete the below screening
                                    questionnaire</p>
                                <p>Your participation is essential to help us take precautionary measures to protect you
                                    and
                                    all in the building.
                                </p>
                                <p>Thank you for your time.</p>
                                <h5 class="center-align"><b>Self-Declaration by Visitor</b></h5>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <p><b>Biographical details</b></p>
                                </div>
                                <br>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <label for="fist_and_last_name">First name and Last name:</label>
                                    <input type="text" id="fist_and_last_name" name="fist_and_last_name">
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <label for="id_number">Id Number:</label>
                                    <input type="text" id="id_number" name="id_number">
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <label for="contact_number">Contact number:</label>
                                    <input type="number" id="contact_number" name="contact_number">
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <label for="street_address">Street address</label>
                                    <input type="text" id="street_address" name="street_address">
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <label for="employer">Employer</label>
                                    <input type="text" id="employer" name="employer">
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <label for="age">Age</label>
                                    <input type="text" id="age" name="age">
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <label for="temperature">Temperature</label>
                                    <input type="text" id="temperature" name="temperature">
                                </div>

                                <h5 class="center-align"><b>QUESTIONS</b></h5>


                                <br>

                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <div class="row">
                                        <div class="col s8">
                                            <label for="medical_history"><b>MEDICAL HISTORY</b><br>
                                                <span style="color: black">History of Asthma, Chronic obstructive airway disease ,Diabetes, HIV,TB,
                                            Kidney ailments.<br>
                                            <span style="color: black">Have you been incontact with anyone whot tested positive for COVID-19?</span></span></label>
                                        </div>
                                        <div class="col s2">
                                            <p>
                                                <label>
                                                    <input id="medical_history" value="Yes" name="medical_history"
                                                           type="checkbox"/>
                                                    <span id="medical_history">Yes</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input id="medical_history" value="No" name="medical_history"
                                                           type="checkbox"/>
                                                    <span id="medical_history">No</span>
                                                </label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <div class="row">
                                        <div class="col s8">
                                            <label for="medical_history"><b>TRAVEL</b><br>
                                                <span style="color: black">Have been out of SA since FEBUARY 2020?.<br>
                                            </span></label>

                                        </div>
                                        <div class="col s2">
                                            <p>
                                                <label>
                                                    <input id="travel" value="Yes" name="travel" type="checkbox"/>
                                                    <span id="travel">Yes</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input id="travel" value="No" name="travel" type="checkbox"/>
                                                    <span id="travel">No</span>
                                                </label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <div class="row">
                                        <div class="col s8">
                                            <label for="recent_care"><b>RECENT MEDICAL CARE</b><br>
                                                <span style="color: black">Have you beem in Clinic, visited GP or been to a Hospital recently.<br>
                                            </span></label>
                                        </div>
                                        <div class="col s2">
                                            <p>
                                                <label>
                                                    <input id="recent_care" value="Yes" name="recent_care"
                                                           type="checkbox"/>
                                                    <span id="recent_care">Yes</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input id="recent_care" value="No" name="recent_care"
                                                           type="checkbox"/>
                                                    <span id="recent_care">No</span>
                                                </label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <div class="row">
                                        <div class="col s8">
                                            <label for="signs_symptoms"><b>CURRENT SIGNS AND SYMPTOMS</b><br>
                                                <span style="color: black">Do you have any of the following?.<br>
                                                    Cough, shortness of breath, loss of smell, loss of taste, sore throat,diarrhoea,
                                                    vomiting, generally feel weak.
                                            </span></label>
                                        </div>
                                        <div class="col s2">
                                            <p>
                                                <label>
                                                    <input id="signs_symptoms" value="Yes" name="signs_symptoms"
                                                           type="checkbox"/>
                                                    <span id="signs_symptoms">Yes</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input id="signs_symptoms" value="No" name="signs_symptoms"
                                                           type="checkbox"/>
                                                    <span id="signs_symptoms">No</span>
                                                </label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 2em;margin-right: 2em">
                                    <span>If YES to any of the above, please wait for further instructions. <br>
                                    If NO - please sign to confirm the above and that you have sanitizes your hands upon entry, and that you will wear a mask at all times.</span>
                                    <br>
                                    <div class="row">
                                        <div class="col s6">
                                            <label for="signature">Signature</label>
                                            <input type="text" id="signature" name="signature">
                                        </div>
                                        <div class="col s6">
                                            <label for="date">Date</label>
                                            <input type="date" id="date" name="date">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light" id="save-user" name="action"
                            style="margin-left: 700px;">
                        Submit
                        <i class="material-icons right">send</i>
                    </button>
                    <br>
                    <br>
                </div>
            </div>
        </div>

    </form>
    <form id="mobile" method="post" action="{{url('store-covid-form')}}" enctype="multipart/form-data"
          class="form-horizontal">
        @csrf
        @method('post')
        <div class="row">
            <div class="col">
                <br>
                <div class="card col s6" style="width: 340px">
                    <br>
                    <div class="row">
                        <div class="col s3">
                            <img style="height: 60px;width:100px;" src="/images/Propella_Logo.jpg">
                        </div>
                        <div class="col s3">
                            <img style="height: 60px;width:100px;" src="/images/About/LOGOS/ENGELI.jpg">
                        </div>

                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="">
                                <p>The safety of all employees, visitors, suppliers, service providers, and the public in
                                    general are our priority.</p>
                                <p>Due to the coronavirus disease 2019 (COVID-19) outbreak and the spread thereof, we
                                    have
                                    implemented measures
                                    to monitor and reduce the risk of transmission</p>
                                <p>To prevent the spread of COVID-19, as well as the potential risk of explosureto our
                                    workforce and visitors, we request that you complete the below screening
                                    questionnaire</p>
                                <p>Your participation is essential to help us take precautionary measures to protect you
                                    and
                                    all in the building.
                                </p>
                                <p>Thank you for your time.</p>
                                <br>
                                <p class="center-align"><b>Self-Declaration by Visitor</b></p>
                                <br>
                                <div class="row">
                                    <p><b>Biographical details</b></p>
                                </div>
                                <br>
                                <div class="row">
                                    <label for="fist_and_last_name">First name and Last name:</label>
                                    <input type="text" id="fist_and_last_name" name="fist_and_last_name">
                                </div>
                                <div class="row">
                                    <label for="id_number">Id Number:</label>
                                    <input type="text" id="id_number" name="id_number">
                                </div>
                                <div class="row">
                                    <label for="contact_number">Contact number:</label>
                                    <input type="number" id="contact_number" name="contact_number">
                                </div>
                                <div class="row">
                                    <label for="street_address">Street address</label>
                                    <input type="text" id="street_address" name="street_address">
                                </div>
                                <div class="row">
                                    <label for="employer">Employer</label>
                                    <input type="text" id="employer" name="employer">
                                </div>
                                <div class="row">
                                    <label for="age">Age</label>
                                    <input type="text" id="age" name="age">
                                </div>
                                <div class="row">
                                    <label for="temperature">Temperature</label>
                                    <input type="text" id="temperature" name="temperature">
                                </div>

                                <p><b>QUESTIONS</b></p>

                                <br>

                                <div class="row">
                                    <div class="row">
                                        <label for="medical_history"><b>MEDICAL HISTORY</b><br>
                                            <span style="color: black">History of Asthma, Chronic obstructive airway disease ,Diabetes, HIV,TB,
                                            Kidney ailments.<br>
                                             Have you been incontact with anyone whot tested positive for COVID-19?</span></label>
                                        <br>
                                        <p>
                                            <label>
                                                <input id="medical_history" value="Yes" name="medical_history"
                                                       type="checkbox"/>
                                                <span id="medical_history">Yes</span>
                                            </label>
                                        </p>
                                        <p>
                                            <label>
                                                <input id="medical_history" value="No" name="medical_history"
                                                       type="checkbox"/>
                                                <span id="medical_history">No</span>
                                            </label>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="row">
                                        <label for="medical_history"><b>TRAVEL</b><br>
                                            <span style="color: black">Have been out of SA since FEBUARY 2020?.<br>
                                            </span></label>
                                        <br>
                                        <br>
                                        <p>
                                            <label>
                                                <input id="travel" value="Yes" name="travel" type="checkbox"/>
                                                <span id="travel">Yes</span>
                                            </label>
                                        </p>
                                        <p>
                                            <label>
                                                <input id="travel" value="No" name="travel" type="checkbox"/>
                                                <span id="travel">No</span>
                                            </label>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="row">
                                        <label for="recent_care"><b>RECENT MEDICAL CARE</b><br>
                                            <span style="color: black">Have you beem in Clinic, visited GP or been to a Hospital recently.<br>
                                            </span></label>

                                        <p>
                                            <label>
                                                <input id="recent_care" value="Yes" name="recent_care" type="checkbox"/>
                                                <span id="recent_care">Yes</span>
                                            </label>
                                        </p>
                                        <p>
                                            <label>
                                                <input id="recent_care" value="No" name="recent_care" type="checkbox"/>
                                                <span id="recent_care">No</span>
                                            </label>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="row">

                                        <label for="signs_symptoms"><b>CURRENT SIGNS AND SYMPTOMS</b><br>
                                            <span style="color: black">Do you have any of the following?.<br>
                                                    Cough, shortness of breath, loss of smell, loss of taste, sore throat,diarrhoea,
                                                    vomiting, generally feel weak.
                                            </span></label>
                                        <p>
                                            <label>
                                                <input id="signs_symptoms" value="Yes" name="signs_symptoms"
                                                       type="checkbox"/>
                                                <span id="signs_symptoms">Yes</span>
                                            </label>
                                        </p>
                                        <p>
                                            <label>
                                                <input id="signs_symptoms" value="No" name="signs_symptoms"
                                                       type="checkbox"/>
                                                <span id="signs_symptoms">No</span>
                                            </label>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <span>If YES to any of the above, please wait for further instructions. <br>
                                    If NO - please sign to confirm the above and that you have sanitizes your hands upon entry, and that you will wear a mask at all times.</span>
                                    <br>

                                    <label for="signature">Signature</label>
                                    <input type="text" id="signature" name="signature">

                                    <label for="date">Date</label>
                                    <input type="date" id="date" name="date">

                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light" id="save-user" name="action"
                            >
                        Submit
                        <i class="material-icons right">send</i>
                    </button>
                    <br>
                    <br>
                </div>
            </div>
        </div>

    </form>

    <style>
        label {
            float: left;
        }

        span {
            display: block;
            overflow: hidden;
            padding: 0px 4px 0px 6px;
        }

        input {
            width: 70%;
        }

    </style>
@endsection
