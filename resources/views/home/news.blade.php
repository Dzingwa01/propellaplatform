@extends('layouts.app')

@section('content')

    <link rel="stylesheet" type="text/css" href="/css/News/first-news.css"/>
    <style>
        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: black;
            font-size: 70px;
            line-height: 1.15em;
            font-weight: 600;
        }
    </style>
    <div class="blogDesktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/news-1172463_960_720.jpg" style="width: 100%;height: 662px;opacity: 0.2;">
                    </div>
                    <div class="centered"><b>Propella News</b></div>
                </div>
            </div>
        </div>
    </div>


    <div class="row" id="desktopNews" style="margin-left: 3em;margin-right: 3em">
        @for($i = 0; $i<count($news); $i++)
            @if($i === 0)
                <div class="col s12 m4">
                    <div class="hoverable" style="cursor: pointer;">
                        <div class="card" style="height: 60vh">
                            <div class="card-image container1">
                                <img style="width:100%;height: 30vh;object-fit: contain"  class="materialboxed" src="/storage/{{isset($news[$i]->news_image_url)?$news[$i]->news_image_url:'Nothing Detected'}}">
                               <span class="card-title" style="font-weight: bold;font-size: 1em;background-color: red;max-height: 1%">NEWS</span>
                            </div>
                            <div class="card-content learningCurve">
                                <h6 class="top-blog-description"> {{$news[$i]->description}} </h6>
                                <h6{{-- class="txt" style="color: #039be5"--}}><b>{{$news[$i]->title}}</b></h6>
                                <h6 class="date" style="color: gray"><b>Date : {{$news[$i]->added_date}}</b></h6>
                                <div style="margin-left: 140px" class="icon-preview col s6 m3 txt"><i class="material-icons small dp48"></i><span style="color: #039be5">read more</span></div>
                                <input hidden disabled class="news_id" data-value="{{$news[$i]->slug}}">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            @else
                <div class="col s12 m4">
                    <div class="hoverable " style="cursor: pointer;">
                        <div class="card" style="height: 60vh">
                            <div class="card-image container1">
                                <img style="width:100%;height: 30vh;object-fit: contain"  class="materialboxed" src="/storage/{{isset($news[$i]->news_image_url)?$news[$i]->news_image_url:'Nothing Detected'}}">
                                <span class="card-title" style="font-weight: bold;font-size: 1em;background-color: red;max-height: 1%">NEWS</span>
                            </div>
                            <div class="card-content learningCurve">
                                <h6 class="top-blog-description"> {{$news[$i]->description}} </h6>
                                <h6{{-- class="txt" style="color: #039be5"--}}><b>{{$news[$i]->title}}</b></h6>
                                <h6 class="date" style="color: gray"><b>Date : {{$news[$i]->added_date}}</b></h6>
                                <div style="margin-left: 140px" class="icon-preview col s6 m3 txt"><i class="material-icons small dp48"></i><span style="color: #039be5">read more</span></div>
                                <input hidden disabled class="news_id" data-value="{{$news[$i]->slug}}">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            @endif
        @endfor
    </div>

    <div class="" id="mobileNews">
        @for($i = 0; $i<count($news); $i++)
            @if($i === 0)
                <div class="col s8">
                    <div class="hoverable" style="cursor: pointer;">
                        <div class="card" style="height: 80vh">
                            <div class="card-image">
                                {{--<img class="materialboxed" style="width:100%;height: 100%;"   src="images/blog.jpg">--}}
                                <img style="width:100%;height: 300px;"  class="materialboxed" src="/storage/{{isset($news[$i]->news_image_url)?$news[$i]->news_image_url:'Nothing Detected'}}">
                                <span class="card-title" style="font-weight: bold;font-size: 1em;background-color: red;max-height: 1%">NEWS</span>
                            </div>
                            <div class="card-content learningCurve">
                                <h6 class="top-blog-description"> {{$news[$i]->description}} </h6>
                                <h6 class="txt" style="color: #039be5"><b>{{$news[$i]->title}}</b></h6>
                                <h6 class="date"
                                    style="color: gray"><b>Date : {{$news[$i]->added_date}}</b></h6>
                                <input hidden disabled class="news_id" data-value="{{$news[$i]->slug}}">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            @else
                <div class="col s8">
                    <div class="hoverable " style="cursor: pointer;">
                        <div class="card" style="height: 80vh">
                            <div class="card-image">
                                <img style="width:100%;height: 300px;" class="materialboxed" src="/storage/{{isset($news[$i]->news_image_url)?$news[$i]->news_image_url:'Nothing Detected'}}">
                                <span class="card-title" style="background-color: red;font-weight: bold;font-size: 1em;max-height: 1%">NEWS</span>
                            </div>
                            <div class="card-content learningCurve">
                                <h6 class="top-blog-description"> {{$news[$i]->description}} </h6>
                                <h6 class="txt" style="color: #039be5"><b>{{$news[$i]->title}}</b></h6>
                                <h6 class="date"
                                    style="color: gray"><b>Date : {{$news[$i]->added_date}}</b></h6>
                                <input hidden disabled class="news_id" data-value="{{$news[$i]->slug}}">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            @endif
        @endfor
    </div>


    <style>
        .txt:hover {
            color: #039be5;
        }
    </style>

    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();

            //Card onclick
            $('.learningCurve').each(function () {
                let news_id = $(this).find('.news_id').attr('data-value');

                $(this).on('click', function () {
                    location.href = '/detailed-news/' + news_id;
                });
            });
            $('.date').each(function () {
                var max_length = 20;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<i href="#" class="more_horiz material-icons small"></i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });

            $('.top-blog-description').each(function () {
                var max_length = 100;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<br><i href="#" class="more_horiz material-icons small"></i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });


            $('.bottom-blog-description').each(function () {
                var max_length = 40;

                if ($(this).html().length > max_length) {

                    var short_content = $(this).html().substr(0, max_length);
                    var long_content = $(this).html().substr(max_length);

                    $(this).html(short_content +
                        '<br><br><i href="#" class="more_horiz material-icons small">more_horiz</i>' +

                        '<span class="more_text" style="display:none;">' + long_content + '</span>');

                }

            });
        });
    </script>
@endsection
