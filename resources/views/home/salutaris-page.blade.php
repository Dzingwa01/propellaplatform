@extends('layouts.app')

@section('content')
    <link type="text/css" rel="stylesheet" href="/css/HedgeSA/hedgeSA.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <body>
    {{-- Salutaris--}}
    <div class="desktop">
        <div class="section standard-section-with-margins" style="margin-top: 5vh">
            <h6 style="margin-left: 0.1em"><b>Low-cost automated bag mask ventilator developed by Propella team</b></h6>
            <br>
            <div class="row">
                <div class="col l5 left-align">
                    <iframe  src="https://www.youtube.com/embed/Tf18e3UDAT4" width="550" height="300" allow="autoplay;" allowfullscreen></iframe>

                    <div class="product_feature">
                        <p><b>Salutaris Product Features</b></p>
                        <p>• Low cost – other units positioned at $ 300 and upwards</p>
                        <p>• Automatic</p>
                        <p>• Portable – can function on AC and DC</p>
                        <p>• Easy to assemble </p>
                        <p>• Easy to use – pictograms provided</p>
                        <p>• Easy to maintain and replace wearing / faulty components</p>
                        <p>• Measures important clinical parameters</p>
                        <p>• Compact and light </p>
                        <p>• Aesthetically pleasing</p>
                    </div>

                    <div class="funding" hidden>
                        <p>
                            <b>HedgeSA are seeking grant funding or ED contributions to cover the following costs as indicated below.
                                Such funding could be converted into BBBEE points due to the profile of HedgeSA.
                            </b>
                        </p>
                        <p><b>Funding requirements</b></p>
                        <p>• Infrastructure requirements to support value chain (project management, storage, admin.,
                            assembly, testing and verification, packaging, material's handling) R 550 000</p>
                        <p>• Training materials (brochures, video etc.) R 150 000</p>
                        <p>• Tooling for injection molding R 400 000</p>
                        <p>• Setting up a help desk R 50 000</p>
                        <p>• Working capital (for limited stock and Operating expenses) R 850 000</p>
                        <p>Seed capital required R 2 000 000 </p>
                    </div>

                </div>


                <div class="col l1"></div>


                <div class="col l6 center-align">
                    <div class="row">
                        <div class="row">
                            <div class="standard-blocks col l4 About">
                                <h6 class="standard-block-headers"> About Hedge SA</h6>
                            </div>
                            <div class="standard-blocks col l4 InvestmentBlock">
                                <h6 class="standard-block-headers">Interested in Funding </h6>
                            </div>
                            <div class="standard-blocks col l4 contactUs">
                                <h6 class="standard-block-headers">Contact Us</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="standard-blocks col l4 product">
                                <h6 class="standard-block-headers">Product Features</h6>
                            </div>
                            <div class="standard-blocks col l4 blank">
                                <h6 class="standard-block-headers"></h6>
                            </div>
                            <div class="standard-blocks col l4 story">
                                <h6 class="standard-block-headers">The Story behind the Story</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row" style="margin-left: 8em;margin-right: 8em">
            <p><b>Background</b></p>
            <p>Seeing the need for devices to assist people suffering from Covid-19 and other diseases, a
                multi-disciplinary team of innovative young
                engineers based at the Propella Business Incubator in Port Elizabeth have dropped all their other projects
                to develop a
                low-cost bag mask ventilator.</p>
            <p>The device fills the urgent need for a low-cost non-invasive ventilator for less serious cases, and frees up
                the expensive units used
                in intensive care units for those in need of advanced care.</p>
            <p>“I was impressed by the simplicity, yet effectivity of the design,”says Dr Hennie Smit,
                a Port Elizabeth general practitioner with extensive experience in anaesthesia.
                “It is meant to assist respiration and not full ventilation, and therefore only needs a tight-fitting face
                mask,
                and is suitable for use in general wards where it can be monitored by non-specialist nurses,” he said after
                evaluating the
                working prototype.</p>
            <p>Dr Smit’s assessment is supported by an award-winning Port Elizabeth-based pulmonologist, who has provided
                guidance on additional
                features.</p>
            <p>Propella incubatees and Nelson Mandela University engineering students Zain Imran and Neo Mabunda teamed up
                with Zain’s brother Zaahid
                and Kelvin Langwani to develop a working prototype within five days. “In anticipation of the lockdown we
                moved our 3D printer and
                other necessary equipment and components such as motor and microcontroller from Propella to Zain’s home for
                the lockdown,”
                says Mabunda.</p>
            <p>The team, which has complementary engineering skills, is now back at the Propella Business Incubator, with
                special permission.</p>
            <p>At the heart of the unit is an inexpensive plastic pouch called a bag-valve resuscitator, or Ambu bag, which
                most hospitals already keep –
                and, crucially, according to Zain Imran, already has the necessary medical certification.</p>
            <p>“We set the standards based on WHO (World Health Organisation) requirements for ventilators and ticked all
                the boxes, such as the volume of
                air delivered to the lungs, the breaths per minute, Inspiration/Expiration Ratio (IE) and control/fail-safe
                capabilities,” says Imran.
            </p>
            <p>“The result is a pre-intensive care ventilator that ticks many of the requirements of a high-end ventilator,”
                he adds. The Salutaris (Latin for
                life-saving) device is powered by a servo motor that expands and contracts two arms. Rapid prototyping was
                possible thanks to the 3D
                printer.</p>
            <p>It can be powered by mains or a car battery. Durability testing is underway. </p>
            <p>The Propella team is also linked into the Massachusetts Institute of Technology (MIT) in the United States,
                which is working on a similar
                concept. “A number of teams around the world have announced ventilators which appear to be functional, but
                where the Salutaris
                differs is that it is a highly engineered solution designed from the outset for manufacture with full
                production and
                cost optimisation in mind,” says Engeli Enterprise Development operations director Wayne Oosthuizen.</p>
            <p>Engeli, which founded the Propella incubator together with the NMU, is assisting with fund-raising and
                commercialisation of the bag
                ventilator to ensure it is made available to hospitals and clinics as soon as possible. </p>
            <p>“We are aiming to start production as soon as the tooling for the injection moulded parts is complete. All
                that is holding us back
                is the finalisation of the funding needed for the tooling and initial investment in components,” says
                Oosthuizen.</p>
            <p>Another Propella incubatee Clifford Hamilton is working on the moulds and helping with the design to make it
                as efficient as
                possible for manufacturing.</p>
            <p>Final pricing is not yet available as some components will have to be imported, “but we are setting our
                pricing benchmark at
                R5 000 or less. “This isn’t about maximising profits, but getting an operational and cost-effective
                ventilator
                into hospitals,” says Oosthuizen.</p>
            <p>A production facility is being designed with the help of Grant Minnie of Propella, who is an industrial
                engineer. “We have the factory
                space and are sourcing certified reusable face masks, which will both bring down the operating costs of the
                machines and speed up
                delivery. “The plan is to produce up to 20 000 units a month, if the market demands this,” he says.</p>
            <p>“This fast-track rollout from concept to production shows the power of the Propella ecosystem which we have
                crafted over the past few years,”
                says Propella manager Anita Palmer.</p>
            <br>
        </div>


        <div id="contact-form-row-desktop">
            <!--Contact Us Form-->
            <div class="card col m6 pull-m6 s6 z-depth-4 hoverable" id="ContactUs" style="margin-top: 0;width: 500px;margin-left: 500px">
                <br>
                <h2 style="font-size: 2em;margin-left: 5em;"><strong>Contact Us</strong></h2>
                <div id="contact-desktop" style="margin-right: 2em;margin-left: 2em">
                    <div class="input-field col s12">
                        <select id="title-input-desktop">
                            <option value="" disabled selected>Choose your title</option>
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Ms">Ms</option>
                            <option value="Dr">Dr</option>
                        </select>
                    </div>

                    <div class="input-field">
                        <input id="name-input-desktop" type="text" class="validate" required>
                        <label for="name-input-desktop">First Name</label>
                    </div>
                    <div class="input-field">
                        <input id="surname-input-desktop" type="text" class="validate" required>
                        <label for="surname-input-desktop" >Surname</label>
                    </div>
                    <div class="input-field">
                        <input id="company-input-desktop" type="text" class="validate" required>
                        <label for="company-input-desktop" >Company</label>
                    </div>
                    <div class="input-field">
                        <input id="contact-number-input-desktop" type="text" class="validate" required>
                        <label for="contact-number-input-desktop">Contact number</label>
                    </div>
                    <div class="input-field">
                        <input id="city-input-desktop" type="text" class="validate" required>
                        <label for="city-input-desktop">City</label>
                    </div>
                    <div class="input-field">
                        <input id="email-input-desktop" type="email" class="validate" required>
                        <label for="email-input-desktop" data-error="incorrect email address">Email</label>
                    </div>
                    <div class="input-field col">
                        <select id="enquiry-category-input-desktop">
                            <option value="" disabled selected>What is your interest?</option>
                            @foreach($enquiry_categories as $e_category)
                                <option value="{{$e_category->id}}">{{$e_category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-field col">
                        <select id="heard-about-us-input-desktop">
                            <option value="" disabled selected>Where did you hear about us?</option>
                            <option value="Radio">Radio</option>
                            <option value="Email Campaign">Email Campaign</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Website">Website</option>
                            <option value="LinkedIn">LinkedIn</option>
                            <option value="Twitter">Twitter</option>
                            <option value="Print Media">Print Media</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <br>
                    <div class="input-field">
                        <textarea id="message-input-desktop" rows="4" cols="50"></textarea>
                    </div>

                    <div>
                        <div class="row" style="margin-left: 40px;">
                            <a class="waves-effect waves-light btn" style="background-color: #5a6268"
                               id="enquiry-submit-button-desktop">Submit</a>
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>


    <div class="mobile">
        <div class="section standard-section-with-margins" style="margin-top: 10vh">
            <h5><b>Low-cost automated bag mask ventilator developed by Propella team</b></h5>
            <iframe  src="https://www.youtube.com/embed/Tf18e3UDAT4" width="300" height="300" allow="autoplay;" allowfullscreen></iframe>
            <div class="card About">
                <p class="center-align" style="font-size: 1.3em;color: white">About Hedge SA</p>
            </div>
            <div class="card fundingMobile">
                <p class="center-align" style="font-size: 1.3em;color: white">Interested in Funding</p>
            </div>
            <div class="card contactUs" id="contactMobile">
                <p class="center-align" style="font-size: 1.3em;color: white">Contact us</p>
            </div>
            <div class="card productMobile" id="feature">
                <p class="center-align" style="font-size: 1.3em;color: white">Product Features</p>
            </div>
            <div class="card storyMobile">
                <p class="center-align" style="font-size: 1.3em;color: white">The story behind the Story</p>
            </div>
        </div>

        <div class="row" style="margin-right: 2em;margin-left: 2em">
            <div class="" id="products" >
                <p style="font-size: 1.2em"><b>Low-cost automated bag mask ventilator developed by Propella team</b></p>
                <p><b>Salutaris Product Features</b></p>
                <p>• Low cost – other units positioned at $ 300 and upwards</p>
                <p>• Automatic</p>
                <p>• Portable – can function on AC and DC</p>
                <p>• Easy to assemble </p>
                <p>• Easy to use – pictograms provided</p>
                <p>• Easy to maintain and replace wearing / faulty components</p>
                <p>• Measures important clinical parameters</p>
                <p>• Compact and light </p>
                <p>• Aesthetically pleasing</p>
            </div>

            <div class="" id="fundings" >
                <p><b>HedgeSA are seeking grant funding or ED contributions to cover the following costs as indicated below. Such funding could be
                        converted into BBBEE points due to the profile of HedgeSA.</b></p>
                <p><b>Funding requirements</b></p>
                <p>• Infrastructure requirements to support value chain (project management, storage, admin., assembly, testing and verification, packaging, material's handling) R 550 000</p>
                <p>• Training materials (brochures, video etc.) R 150 000</p>
                <p>• Tooling for injection molding R 400 000</p>
                <p>• Setting up a help desk R 50 000</p>
                <p>• Working capital (for limited stock and Operating expenses) R 850 000</p>
                <p>Seed capital required R 2 000 000 </p>
            </div>

        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <p><b>Background</b></p>
            <p>Seeing the need for devices to assist people suffering from Covid-19 and other diseases, a multi-disciplinary team of innovative young
                engineers based at the Propella Business Incubator in Port Elizabeth have dropped all their other projects to develop a
                low-cost bag mask ventilator.</p>
            <p>The device fills the urgent need for a low-cost non-invasive ventilator for less serious cases, and frees up the expensive units used
                in intensive care units for those in need of advanced care.</p>
            <p>“I was impressed by the simplicity, yet effectivity of the design,”says  Dr Hennie Smit, a Port Elizabeth general practitioner with extensive experience in anaesthesia.
                “It is meant to assist respiration and not full ventilation, and therefore only needs a tight-fitting face mask,
                and is suitable for use in general wards where it can be monitored by non-specialist nurses,” he said after evaluating the
                working prototype.</p>
            <p>Dr Smit’s assessment is supported by an award-winning Port Elizabeth-based pulmonologist, who has provided guidance on additional
                features.</p>
            <p>Propella incubatees and Nelson Mandela University engineering students Zain Imran and Neo Mabunda teamed up with Zain’s brother Zaahid
                and Kelvin Langwani to develop a working prototype within five days.  “In anticipation of the lockdown we moved our 3D printer and
                other necessary equipment and components such as motor and microcontroller from Propella to Zain’s home for the lockdown,”
                says Mabunda.</p>
            <p>The team, which has complementary engineering skills, is now back at the Propella Business Incubator, with special permission.</p>
            <p>At the heart of the unit is an inexpensive plastic pouch called a bag-valve resuscitator, or Ambu bag, which most hospitals already keep –
                and, crucially, according to Zain Imran, already has the necessary medical certification.</p>
            <p>“We set the standards based on WHO (World Health Organisation) requirements for ventilators and ticked all the boxes, such as the volume of
                air delivered to the lungs, the breaths per minute, Inspiration/Expiration Ratio (IE) and control/fail-safe capabilities,” says Imran.
            </p>
            <p>“The result is a pre-intensive care ventilator that ticks many of the requirements of a high-end ventilator,” he adds. The Salutaris (Latin for
                life-saving) device is powered by a servo motor that expands and contracts two arms. Rapid prototyping was possible thanks to the 3D
                printer.</p>
            <p>It can be powered by mains or a car battery.  Durability testing is underway. </p>
            <p>The Propella team is also linked into the Massachusetts Institute of Technology (MIT) in the United States, which is working on a similar
                concept.  “A number of teams around the world have announced ventilators which appear to be functional, but where the Salutaris
                differs is that it is a highly engineered solution designed from the outset for manufacture with full production and
                cost optimisation in mind,” says Engeli Enterprise Development operations director Wayne Oosthuizen.</p>
            <p>Engeli, which founded the Propella incubator together with the NMU, is assisting with fund-raising and commercialisation of the bag
                ventilator to ensure it is made available to hospitals and clinics as soon as possible. </p>
            <p>“We are aiming to start production as soon as the tooling for the injection moulded parts is complete. All that is holding us back
                is the finalisation of the funding needed for the tooling and initial investment in components,” says Oosthuizen.</p>
            <p>Another Propella incubatee Clifford Hamilton is working on the moulds and helping with the design to make it as efficient as
                possible for manufacturing.</p>
            <p>Final pricing is not yet available as some components will have to be imported, “but we are setting our pricing benchmark at
                R5 000 or less.  “This isn’t about maximising profits, but getting an operational and cost-effective ventilator
                into hospitals,” says Oosthuizen.</p>
            <p>A production facility is being designed with the help of Grant Minnie of Propella, who is an industrial engineer.  “We have the factory
                space and are sourcing certified reusable face masks, which will both bring down the operating costs of the machines and speed up
                delivery. “The plan is to produce up to 20 000 units a month, if the market demands this,” he says.</p>
            <p>“This fast-track rollout from concept to production shows the power of the Propella ecosystem which we have crafted over the past few years,”
                says Propella manager Anita Palmer.</p>
            <br>
        </div>

        <div id="contact-form-row-mobile">
            <!--Contact Us Form-->
            <div class="card col m6 pull-m6 s6 z-depth-4 hoverable " id="ContactUsMobile"
                 style="margin-top: 0;width: 280px;margin-left: 3em">
                <br>
                <h2 style="font-size: 2em;margin-left: 2em;"><strong>Contact Us</strong></h2>
                <div id="contact-desktop" style="margin-right: 2em;margin-left: 2em">
                    <div class="input-field col s12">
                        <select id="title-input-mobile">
                            <option value="" disabled selected>Choose your title</option>
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Ms">Ms</option>
                            <option value="Dr">Dr</option>
                        </select>
                    </div>

                    <div class="input-field">
                        <input id="name-input-mobile" type="text" class="validate" required>
                        <label for="name-input-mobile">First Name</label>
                    </div>
                    <div class="input-field">
                        <input id="surname-input-mobile" type="text" class="validate" required>
                        <label for="surname-input-mobile" >Surname</label>
                    </div>
                    <div class="input-field">
                        <input id="company-input-mobile" type="text" class="validate" required>
                        <label for="company-input-mobile" >Company</label>
                    </div>
                    <div class="input-field">
                        <input id="contact-number-input-mobile" type="text" class="validate" required>
                        <label for="contact-number-input-mobile">Contact number</label>
                    </div>
                    <div class="input-field">
                        <input id="city-input-mobile" type="text" class="validate" required>
                        <label for="city-input-mobile">City</label>
                    </div>
                    <div class="input-field">
                        <input id="email-input-mobile" type="email" class="validate" required>
                        <label for="email-input-mobile" data-error="incorrect email address">Email</label>
                    </div>
                    <div class="input-field col">
                        <select id="enquiry-category-input-mobile">
                            <option value="" disabled selected>What is your interest?</option>
                            @foreach($enquiry_categories as $e_category)
                                <option value="{{$e_category->id}}">{{$e_category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-field col">
                        <select id="heard-about-us-input-mobile">
                            <option value="" disabled selected>Where did you hear about us?</option>
                            <option value="Radio">Radio</option>
                            <option value="Email Campaign">Email Campaign</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Website">Website</option>
                            <option value="LinkedIn">LinkedIn</option>
                            <option value="Twitter">Twitter</option>
                            <option value="Print Media">Print Media</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <br>
                    <div class="input-field">
                        <textarea id="message-input-mobile" placeholder="Enquiry goes here" rows="4" cols="50"></textarea>
                    </div>

                    <div>
                        <div class="row" style="margin-left: 40px;">
                            <a class="waves-effect waves-light btn" style="background-color: #5a6268"
                               id="enquiry-submit-button-mobile">Submit</a>
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>

    <div class="row center" id="message-sent-row-mobile" hidden style="margin-left: 20em; margin-right: 20em;">
        <div class="col l12 m12 s12" style="background-color: #0f9d58">
            <h5 style="color: white;">Thank you for your enquiry.</h5>
        </div>
    </div>



    <script>
        $(document).ready(function(){
            $('select').formSelect();
            $('.About').on('click', function () {
                location.href = '/Home/Founders/fa3c30e0-d3a7-11e9-a7fe-85160e25377e';
            });
            $('.contactUs').on('click', function () {
                location.href = '#ContactUs';
            });
            $('.InvestmentBlock').on('click', function () {
                $('.funding').show();
                $('.product_feature').hide();
            });
            $('.story').on('click', function(){
                location.href = '/show-blog-summernote/14d10670-7990-11ea-b5f4-2fc30f26c6de'
            });
            $('.product').on('click', function () {
                $('.funding').hide();
                $('.product_feature').show();
            });
            $('.fundingMobile').on('click',function () {
                location.href = '#fundings'
            });
            $('.productMobile').on('click', function () {
                location.href = '#products'
            });

            $('#contactMobile').on('click',function () {
                location.href = '#ContactUsMobile'
            });
            $('.storyMobile').on('click',function () {
                location.href = '/show-blog-summernote/14d10670-7990-11ea-b5f4-2fc30f26c6de\n'
            });


        });

        $('#enquiry-submit-button-desktop').on('click', function(){
            let title = $('#title-input-desktop').val();
            let name = $('#name-input-desktop').val();
            let surname = $('#surname-input-desktop').val();
            let email = $('#email-input-desktop').val();
            let company = $('#company-input-desktop').val();
            let contact_number = $('#contact-number-input-desktop').val();
            let city = $('#city-input-desktop').val();
            let enquiry_category_id = $('#enquiry-category-input-desktop').val();
            let heard_about_us = $('#heard-about-us-input-desktop').val();
            let enquiry_message = $('#message-input-desktop').val();
            let formData = new FormData();

            formData.append('title', title);
            formData.append('name', name);
            formData.append('surname', surname);
            formData.append('email', email);
            formData.append('company', company);
            formData.append('contact_number', contact_number);
            formData.append('city', city);
            formData.append('enquiry_category_id', enquiry_category_id);
            formData.append('heard_about_us', heard_about_us);
            formData.append('enquiry_message', enquiry_message);

            let url = 'send-enquiry';
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success: function (response, a, b) {
                    $('#contact-form-row-desktop').hide();
                    $('#message-sent-row-desktop').show();
                },
                error: function (response) {
                    let message = response.message;
                    alert(message);
                }
            });
        });



        $('#enquiry-submit-button-mobile').on('click', function(){
            let title = $('#title-input-mobile').val();
            let name = $('#name-input-mobile').val();
            let surname = $('#surname-input-mobile').val();
            let email = $('#email-input-mobile').val();
            let company = $('#company-input-mobile').val();
            let contact_number = $('#contact-number-input-mobile').val();
            let city = $('#city-input-mobile').val();
            let enquiry_category_id = $('#enquiry-category-input-mobile').val();
            let heard_about_us = $('#heard-about-us-input-mobile').val();
            let enquiry_message = $('#message-input-mobile').val();
            let formData = new FormData();

            formData.append('title', title);
            formData.append('name', name);
            formData.append('surname', surname);
            formData.append('email', email);
            formData.append('company', company);
            formData.append('contact_number', contact_number);
            formData.append('city', city);
            formData.append('enquiry_category_id', enquiry_category_id);
            formData.append('heard_about_us', heard_about_us);
            formData.append('enquiry_message', enquiry_message);

            let url = 'send-enquiry';
            $.ajax({
                url: url,
                processData: false,
                contentType: false,
                data: formData,
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                success: function (response, a, b) {
                    $('#contact-form-row-mobile').hide();
                    $('#message-sent-row-mobile').show();
                },
                error: function (response) {
                    let message = response.message;
                    alert(message);
                }
            });

        });
    </script>
    </body>

@endsection
