@extends('layouts.app')

@section('content')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/Blog/blog.css"/>
    <link rel="stylesheet" type="text/css" href="/css/Media/media.css"/>
    <style>
        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            font-size: 70px;
            line-height: 1.15em;
            font-weight: 600;
        }
    </style>
    <div class="blogDesktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/facebook-424521_960_720.jpg" style="width: 100%;height: 662px;opacity: 0.2;">
                    </div>
                    <div class="centered"><b>Propella Media</b></div>
                </div>
            </div>
        </div>
        <div class="row" id="desktop-media" style="margin-top: 5vh">
            <div class="col s10">
                {{--BLOG ROW--}}
                <div class="row" id="blog">
                    <h2 style="margin-left: 3.5em"><b>Blogs</b></h2>
                    <div class="col m12" style="margin-left: 10em">
                        @for($i = 0; $i<$blogs->count(); $i++)
                            @if($i <= 2)
                                <div class="container col l4 blog card1  ">
                                    @if($blogs[$i]->blog_image_url != null)
                                        <div class="card" >
                                            <div class="card-image">
                                                <img style="width:100%;height: 40vh;"
                                                     src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                                <div class="overlay">
                                                    <p class="center-align"><b>{{$blogs[$i]->title}}</b></p>
                                                    <h6 class="center-align">{{$blogs[$i]->description}}</h6>
                                                    <input hidden  class="news_id" data-value="{{$blogs[$i]->slug}}">
                                                </div>
                                                <span class="card-title"
                                                      style="background-color: #1a237e;font-weight: bold;font-size: 1em">BLOG</span>
                                                <input hidden class="blog_id" data-value="{{$blogs[$i]->slug}}">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        @endfor
                    </div>
                </div>
                {{-- BLOG NEXT AND PREVIOUS--}}
                <div class="blog-next-previous" style="margin-left: 50em">
                    <a href="/blogs" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>View More</a>
                </div>
                {{--NEWS ROW--}}
                <div class="row" id="news" style="margin-top: 10vh">
                    <h2 style="margin-left: 3em"><b>News</b></h2>
                    <div class="col m12" style="margin-left: 10em">
                        @for($i = 0; $i<$news->count(); $i++)
                            @if($i <= 2)
                                <div class="container col l4 ourNews card1  ">
                                    @if($news[$i]->news_image_url != null)
                                        <div class="card" >
                                            <div class="card-image">
                                                <img style="width:100%;height: 40vh;"
                                                     src="/storage/{{isset($news[$i]->news_image_url)?$news[$i]->news_image_url:'Nothing Detected'}}">
                                                <div class="overlay">
                                                    <p class="center-align"><b>{{$news[$i]->title}}</b></p>
                                                    <h6 class="center-align">{{$news[$i]->description}}</h6>
                                                    <input hidden  class="news_id" data-value="{{$news[$i]->slug}}">
                                                </div>
                                                <span class="card-title"
                                                      style="background-color: red;font-weight: bold;font-size: 1em">NEWS</span>
                                                <input hidden  class="news_id" data-value="{{$news[$i]->slug}}">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        @endfor
                    </div>
                </div>
                {{-- NEWS NEXT AND PREVIOUS--}}
                <div class="news-next-previous" style="margin-left: 50em">
                    <a href="/news" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>view More</a>
                </div>
                {{-- VIDEO GALLERY--}}
                <div class="row" id="videoGallery">
                    <h2 style="margin-left: 3em"><b>Video Gallery</b></h2>
                    <div class="col m12" style="margin-left: 9em">
                        @for($i = 0; $i<$video->count(); $i++)
                            @if($i <= 2)
                                <div class="col l4">
                                    <div style="border-style: hidden; height: 100%; width: 120%; ">
                                        <iframe heigh="350" class="materialboxed" style="margin-left: 1em"
                                                src="{{isset($video[$i]->video)?$video[$i]->video:''}}" width="400" height="200"
                                                allow="autoplay;" allowfullscreen></iframe>
                                    </div>
                                </div>
                            @endif
                        @endfor
                    </div>
                </div>
                {{-- VIDEO GALLERY NEXT AND PREVIOUS--}}
                <div class="video-gallery-next-previous" style="margin-left: 50em">
                    <a href="/video-gallery" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>View More</a>
                </div>
                <!--Detailed Events-->
                <div class="row" id="events">
                    <h2 style="margin-left: 3em"><b>Events</b></h2>
                    <div class="col m12" style="margin-left: 10em">
                        @foreach ($events_array as $event_list)

                            <div class="col s12 m3">
                                <div class="card white hoverable center" style="border-radius: 20px;">
                                    <div class="circle">
                                        <div class="card-content black-text" style="height: 100%;">
                                            <div class="user-view" align="center">
                                                <img class="fullscreen materialboxed" style="width: 100%; height: 200px;"
                                                     src="{{isset($event_list->event->event_image_url)?'/storage/'.$event_list->event->event_image_url:''}}">
                                            </div>
                                            <h6 class="">{{$event_list->event->title}}</h6>
                                            <h6 class="" id="start"
                                                value="{{$event_list->start_time}}">{{$event_list->start_time}} </h6>
                                            <h6>{{$event_list->event->start->toDateString()}}</h6>
                                            @if(isset($event_list->event->EventVenue->venue_name))
                                                <h6 class=""> {{$event_list->event->EventVenue->venue_name}} </h6>
                                            @else
                                                <h6 class=""> Old Building </h6>
                                            @endif
                                            @if($event_list->event->type == 'Private')
                                                <h6 class="private-event-type-header" style="color:red;">
                                                    <b>{{$event_list->event->type}}</b></h6>
                                            @else
                                                <h6 class="event-type-header">{{$event_list->event->type}} </h6>
                                            @endif

                                        </div>
                                        <div class="card-action">
                                            @if($event_list->event->start > new DateTime())
                                                @if($event_list->participant_count > $event_list->registered_participants)
                                                    <div class="row" style="margin-left:1em;">
                                                        <a href="#register-modal" class="modal-trigger"
                                                           data-value="{{$event_list->event->id}}"
                                                           data-type="{{$event_list->event->type}}"
                                                           style="color: orange;" onclick="saveEventDetails(this)">Register
                                                            | Deregister</a>
                                                    </div>
                                                @else
                                                    <div class="row" style="margin-left:1em;">
                                                        <a href="#participant-notification-modal" class="modal-trigger"
                                                           style="color: orange;">Register
                                                            | Deregister</a>
                                                    </div>
                                                @endif
                                            @endif

                                            <div class="row" style="margin-left: 1em;">
                                                <a href="#event-detail-modal" class="modal-trigger"
                                                   data-value="{{$event_list->event->invite_image_url}}" onclick="showModal(this)"
                                                   style="color: orange;">Event Details</a>
                                            </div>

                                            @if(new DateTime() > $event_list->event->end)
                                                <div class="row" style="margin-left: 1em;">
                                                    <div class="view_media" id="photo">
                                                        <a style="color: orange;"
                                                           href="{{url('/full_calendar/show-gallery/'.$event_list->event->slug)}}">View
                                                            Gallery</a>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="events-next-previous" style="margin-left: 50em">
                    <a href="/show-events" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>View More</a>
                </div>
                <!--Gallery-->
                <div class="row" id="gallery">
                    <h2 style="margin-left: 3em"><b>Gallery</b></h2>
                    <div class="col m12" style="margin-left: 10em">
                        @foreach ($events as $event_list)
                            <div class="col s12 m3">
                                <div class="card white hoverable center" style="border-radius: 20px;">
                                    <div class="circle">
                                        <div class="card-content black-text" style="height: 100%;">
                                            <div class="user-view" align="center">
                                                <img class="fullscreen materialboxed" style="width: 100%; height: 200px;"
                                                     src="{{isset($event_list->event_image_url)?'/storage/'.$event_list->event_image_url:''}}">
                                            </div>
                                            <h6 class="">{{$event_list->title}}</h6>
                                            <h6>{{$event_list->start->toDateString()}}</h6>
                                            <div class="row" style="margin-left: 1em;">
                                                <div class="view_media" id="photo">
                                                    <a style="color: orange;"
                                                       href="{{url('/full_calendar/show-gallery/'.$event_list->slug)}}">View
                                                        Gallery</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="gallery-next-previous" style="margin-left: 50em">
                    <a href="/home/gallery" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>View More</a>
                </div>
            </div>
        </div>
    </div>
    <div class="blogMobile">
        <div class="section">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/facebook-424521_960_720.jpg" style="width: 100%;height: 662px;opacity: 0.2;">
                    </div>
                    <div class="centered"><b>Propella Media</b></div>
                </div>
            </div>
        </div>
        <div class="row" id="mobile-media" style="margin-top: 2vh">
            <div class="col s10">
                {{--BLOG ROW--}}
                <div class="row" id="blog">
                    <h2 style="margin-left: 1.5em"><b>Blogs</b></h2>
                    <div class="col m12" style="margin-left: 5em">
                        @for($i = 0; $i<$blogs->count(); $i++)
                            @if($i <= 2)
                                <div class="container col l4 blog card1">
                                    @if($blogs[$i]->blog_image_url != null)
                                        <div class="card" >
                                            <div class="card-image">
                                                <img style="width:100%;height: 40vh;"
                                                     src="/storage/{{isset($blogs[$i]->blog_image_url)?$blogs[$i]->blog_image_url:'Nothing Detected'}}">
                                                <div class="overlay">
                                                    <h6 class="center-align"><b>{{$blogs[$i]->title}}</b></h6>
                                                    <p class="center-align" style="font-size: 11px">{{$blogs[$i]->description}}</p>
                                                    <input hidden  class="news_id" data-value="{{$blogs[$i]->slug}}">
                                                </div>
                                                <span class="card-title"
                                                      style="background-color: #1a237e;font-weight: bold;font-size: 1em">BLOG</span>
                                                <input hidden class="blog_id" data-value="{{$blogs[$i]->slug}}">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        @endfor
                    </div>
                </div>
                {{-- BLOG NEXT AND PREVIOUS--}}
                <div class="blog-next-previous" style="margin-left: 8em">
                    <a href="/blogs" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>View More</a>
                </div>
                {{--NEWS ROW--}}
                <div class="row" id="news">
                    <h2 style="margin-left: 1.5em"><b>News</b></h2>
                    <div class="col m12" style="margin-left: 5em">
                        @for($i = 0; $i<$news->count(); $i++)
                            @if($i <= 2)
                                <div class="container col l4 ourNews card1  ">
                                    @if($news[$i]->news_image_url != null)
                                        <div class="card" >
                                            <div class="card-image">
                                                <img style="width:100%;height: 40vh;"
                                                     src="/storage/{{isset($news[$i]->news_image_url)?$news[$i]->news_image_url:'Nothing Detected'}}">
                                                <div class="overlay">
                                                    <h6 class="center-align"><b>{{$news[$i]->title}}</b></h6>
                                                    <p class="center-align" style="font-size: 11px">{{$news[$i]->description}}</p>
                                                    <input hidden  class="news_id" data-value="{{$news[$i]->slug}}">
                                                </div>
                                                <span class="card-title"
                                                      style="background-color: red;font-weight: bold;font-size: 1em">NEWS</span>
                                                <input hidden  class="news_id" data-value="{{$news[$i]->slug}}">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        @endfor
                    </div>
                </div>
                {{-- NEWS NEXT AND PREVIOUS--}}
                <div class="news-next-previous" style="margin-left: 8em">
                    <a href="/news" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>view More</a>
                </div>
                {{-- VIDEO GALLERY--}}
                <div class="row" id="videoGallery">
                    <h2 style="margin-left: 1.5em"><b>Videos</b></h2>
                    <div class="col m12" style="margin-left: 5em">
                        @for($i = 0; $i<$video->count(); $i++)
                            @if($i <= 2)
                                <div class="col l4">
                                    <div style="border-style: hidden;">
                                        <iframe  class="materialboxed"
                                                src="{{isset($video[$i]->video)?$video[$i]->video:''}}" width="200" height="200"
                                                allow="autoplay;" allowfullscreen></iframe>
                                    </div>
                                </div>
                            @endif
                        @endfor
                    </div>
                </div>
                {{-- VIDEO GALLERY NEXT AND PREVIOUS--}}
                <div class="video-gallery-next-previous" style="margin-left: 8em">
                    <a href="/video-gallery" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>View More</a>
                </div>
                <!--Detailed Events-->
                <div class="row" id="events">
                    <h2 style="margin-left: 1.5em"><b>Events</b></h2>
                    <div class="col m12" style="margin-left: 5em">
                        @foreach ($events_array as $event_list)

                            <div class="col s12 m3">
                                <div class="card white hoverable center" style="border-radius: 20px;">
                                    <div class="circle">
                                        <div class="card-content black-text" style="height: 100%;">
                                            <div class="user-view" align="center">
                                                <img class="fullscreen materialboxed" style="width: 100%; height: 200px;"
                                                     src="{{isset($event_list->event->event_image_url)?'/storage/'.$event_list->event->event_image_url:''}}">
                                            </div>
                                            <h6 class="">{{$event_list->event->title}}</h6>
                                            <h6 class="" id="start"
                                                value="{{$event_list->start_time}}">{{$event_list->start_time}} </h6>
                                            <h6>{{$event_list->event->start->toDateString()}}</h6>
                                            @if(isset($event_list->event->EventVenue->venue_name))
                                                <h6 class=""> {{$event_list->event->EventVenue->venue_name}} </h6>
                                            @else
                                                <h6 class=""> Old Building </h6>
                                            @endif
                                            @if($event_list->event->type == 'Private')
                                                <h6 class="private-event-type-header" style="color:red;">
                                                    <b>{{$event_list->event->type}}</b></h6>
                                            @else
                                                <h6 class="event-type-header">{{$event_list->event->type}} </h6>
                                            @endif

                                        </div>
                                        <div class="card-action">
                                            @if($event_list->event->start > new DateTime())
                                                @if($event_list->participant_count > $event_list->registered_participants)
                                                    <div class="row" style="margin-left:1em;">
                                                        <a href="#register-modal" class="modal-trigger"
                                                           data-value="{{$event_list->event->id}}"
                                                           data-type="{{$event_list->event->type}}"
                                                           style="color: orange;" onclick="saveEventDetails(this)">Register
                                                            | Deregister</a>
                                                    </div>
                                                @else
                                                    <div class="row" style="margin-left:1em;">
                                                        <a href="#participant-notification-modal" class="modal-trigger"
                                                           style="color: orange;">Register
                                                            | Deregister</a>
                                                    </div>
                                                @endif
                                            @endif

                                            <div class="row" style="margin-left: 1em;">
                                                <a href="#event-detail-modal" class="modal-trigger"
                                                   data-value="{{$event_list->event->invite_image_url}}" onclick="showModal(this)"
                                                   style="color: orange;">Event Details</a>
                                            </div>

                                            @if(new DateTime() > $event_list->event->end)
                                                <div class="row" style="margin-left: 1em;">
                                                    <div class="view_media" id="photo">
                                                        <a style="color: orange;"
                                                           href="{{url('/full_calendar/show-gallery/'.$event_list->event->slug)}}">View
                                                            Gallery</a>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="events-next-previous" style="margin-left: 8em">
                    <a href="/show-events" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>View More</a>
                </div>
                <!--Gallery-->
                <div class="row" id="gallery">
                    <h2 style="margin-left: 1.5em"><b>Gallery</b></h2>
                    <div class="col m12" style="margin-left: 5em">
                        @foreach ($events as $event_list)
                            <div class="col s12 m3">
                                <div class="card white hoverable center" style="border-radius: 20px;">
                                    <div class="circle">
                                        <div class="card-content black-text" style="height: 100%;">
                                            <div class="user-view" align="center">
                                                <img class="fullscreen materialboxed" style="width: 100%; height: 200px;"
                                                     src="{{isset($event_list->event_image_url)?'/storage/'.$event_list->event_image_url:''}}">
                                            </div>
                                            <h6 class="">{{$event_list->title}}</h6>
                                            <h6>{{$event_list->start->toDateString()}}</h6>
                                            <div class="row" style="margin-left: 1em;">
                                                <div class="view_media" id="photo">
                                                    <a style="color: orange;"
                                                       href="{{url('/full_calendar/show-gallery/'.$event_list->slug)}}">View
                                                        Gallery</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="gallery-next-previous" style="margin-left: 8em">
                    <a href="/home/gallery" class="waves-effect waves-light btn blue"><i class="material-icons right">arrow_forward</i>View More</a>
                </div>
            </div>
        </div>
    </div>

    <div id="deregister-modal" class="modal">
        <div class="modal-content">
            <h6>Sorry to see you go :(</h6>

            <input id="deregister-input" type="text" placeholder="What is your reason for deregister?">

        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat" onclick="deregisterUser()"
               id="submit-deregister-info-button">Submit</a>
        </div>
    </div>

    <div id="register-modal" class="modal">
        <div class="modal-content">
            <h5>One more step!</h5>

            <select id="found-us-via-input">
                <option value="" disabled selected>How did you hear about us?</option>
                <option value="Radio">Radio</option>
                <option value="Email Campaign">Email Campaign</option>
                <option value="Facebook">Facebook</option>
                <option value="Website">Website</option>
                <option value="LinkedIn">LinkedIn</option>
                <option value="Twitter">Twitter</option>
                <option value="Print Media">Print Media</option>
                <option value="Other">Other</option>
            </select>
            <input id="found-us-via-other-input" hidden placeholder="Please specify?" type="text">

        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-green btn-flat" onclick="saveFoundOutDetails()">Submit</a>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.modal').modal();
            $('#found-us-via-input').change(function () {
                if ($('#found-us-via-input').val() === 'Other') {
                    $('#found-us-via-other-input').show();
                } else {
                    $('#found-us-via-other-input').hide();
                }
            });
        });


        function viewMedia(obj) {
            let event_id = obj.getAttribute('data-value');

            location.href = '/home/event-show-gallery/' + event_id;
        }

        function saveEventDetails(obj) {
            let event_id = obj.getAttribute('data-value');
            let event_type = obj.getAttribute('data-type');
            sessionStorage.setItem('stored_event_id', event_id);
            sessionStorage.setItem('stored_event_type', event_type);
        }

        function saveFoundOutDetails() {
            if ($('#found-us-via-input').val() === "") {
                alert('Please choose an option.');
            } else {
                let found_out_via;
                let event_type = sessionStorage.getItem('stored_event_type');

                if ($('#found-us-via-input').val() === 'Other') {
                    found_out_via = $('#found-us-via-other-input').val();
                } else {
                    found_out_via = $('#found-us-via-input').val();
                }

                sessionStorage.setItem('stored_found_out_via', found_out_via);

                if (event_type === 'Public') {
                    window.location.href = '/visitors/visitor-login';
                } else if (event_type === 'Private') {
                    window.location.href = '/login';
                } else if (event_type === 'View Only') {
                    alert('Sorry, this event is for viewing purposes only.');
                }

            }
        }

        function showModal(obj) {
            $('.modal').modal({
                onOpenStart: function () {
                    let image_source = obj.getAttribute('data-value');
                    $('#modal-image-div').html(
                        '<div class="row" style="height: 100%; width: 100%">'
                        +
                        '<img id="event-detail-modal-image" src="/storage/' + image_source + '" alt="No Image Uploaded" style="height: 100%; width: 100%;">'
                        +
                        '</div>'
                    );
                }
            });
        }

        $('.private-event-type-header').hover(function () {
            $(this).html(
                '<p>' + 'So sorry, this is an internal event.' + '</p>'
            )
        });
            //NEXT EVENTS MOBILE
            $('.next-events-mobile').on('click', function () {
                $('#blog2-mobile').hide();
                $('#events2-mobile').show();
                $('#blog-mobile').hide();
                $('#news-mobile').hide();
                $('#news2-mobile').hide();
                $('#videoGallery-mobile').hide();
                $('#videoGallery2-mobile').hide();
                $('#lerningCurves-mobile').hide();
                $('#lerningCurves2-mobile').hide();
                $('#gallery-mobile').hide();
                $('#gallery2mobile').hide();
                $('.blog-next-previous').hide();
                $('.events-next-previous').show();
                $('.news-next-previous').hide();
                $('.events-next-previous-mobile').hide();
                $('.video-gallery-next-previous').hide();
                $('.learning-curves-next-previous').hide();
                $('.gallery-next-previous').hide();
                $('.next-blog').hide();
                $('#events').hide();
                $('#events2').hide();

            });

            //NEWS NEXT CKICK
            $('.next-news-mobile').on('click', function () {
                $('#news2-mobile').show();
                $('#events2-mobile').hide();
                $('#events-mobile').hide();
                $('#news-mobile').hide();
                $('#blog2-mobile').hide();
                $('#blog-mobile').hide();
                $('#videoGallery-mobile').hide();
                $('#videoGallery2-mobile').hide();
                $('#lerningCurves-mobile').hide();
                $('#lerningCurves2-mobile').hide();
                $('#gallery-mobile').hide();
                $('#gallery2-mobile').hide();
                $('.blog-next-previous').hide();
                $('.news-next-previous').show();
                $('.events-next-previous').show();
                $('.events-next-previous-mobile').show();
                $('.video-gallery-next-previous').hide();
                $('.learning-curves-next-previous').hide();
                $('.gallery-next-previous').hide();
                $('.next-blog').hide();
                $('.next-news').hide();
                $('.previous-news').show();


            });



            //VIDEO GALLERY MOBILE NEXT CKICK
            $('.next-video-gallery-mobile').on('click', function () {
                $('#videoGallery2-mobile').show();
                $('#videoGallery-mobile').hide();
                $('#news2-mobile').hide();
                $('#events2-mobile').hide();
                $('#events-mobile').hide();
                $('#news-mobile').hide();
                $('#blog2-mobile').hide();
                $('#blog-mobile').hide();
                $('#lerningCurves-mobile').hide();
                $('#lerningCurves2-mobile').hide();
                $('#gallery-mobile').hide();
                $('#gallery2-mobile').hide();
                $('.blog-next-previous').hide();
                $('.news-next-previous').hide();
                $('.video-gallery-next-previous').show();
                $('.learning-curves-next-previous').hide();
                $('.gallery-next-previous').hide();
                $('.next-blog').hide();
                $('.next-news').hide();
                $('.previous-news').hide();
                $('.next-video-gallery').hide();


            });



            //LEARNING CURVES MOBILE NEXT CKICK
            $('.next-learning-curve-mobile').on('click', function () {
                $('#lerningCurves2-mobile').show();
                $('#lerningCurves-mobile').hide();
                $('#videoGallery2-mobile').hide();
                $('#videoGallery-mobile').hide();
                $('#news2-mobile').hide();
                $('#news-mobile').hide();
                $('#blog2-mobile').hide();
                $('#blog-mobile').hide();
                $('#gallery-mobile').hide();
                $('#gallery2-mobile').hide();
                $('.blog-next-previous').hide();
                $('.news-next-previous').hide();
                $('.video-gallery-next-previous').hide();
                $('.learning-curves-next-previous').show();
                $('.gallery-next-previous').hide();
                $('.next-blog').hide();
                $('.next-news').hide();
                $('.previous-news').hide();
                $('.next-video-gallery').hide();
                $('.next-learning-curve').hide();
                $('#events').hide();
                $('#events-mobile').hide();
                $('.events-next-previous').hide();
                $('#events2').hide();
                $('#events2-mobile').hide();

            });
            //LEARNING CURVES PREVIOUS CLICK
            $('.previous-learning-curve').on('click', function () {
                $('#lerningCurves').show();
                //GALARY NEXT CKICK
                $('#lerningCurves2').hide();
            });


            $('.next-gallery-mobile').on('click', function () {
                $('#gallery-mobile').show();
                $('#lerningCurves2-mobile').hide();
                $('#lerningCurves-mobile').hide();
                $('#videoGallery2-mobile').hide();
                $('#videoGallery-mobile').hide();
                $('#news2-mobile').hide();
                $('#news-mobile').hide();
                $('#blog2-mobile').hide();
                $('#blog-mobile').hide();
                $('.blog-next-previous').hide();
                $('.news-next-previous').hide();
                $('.video-gallery-next-previous').hide();
                $('.learning-curves-next-previous').hide();
                $('.gallery-next-previous').show();
                $('.next-blog').hide();
                $('.next-news').hide();
                $('.previous-news').hide();
                $('.next-video-gallery').hide();
                $('.next-learning-curve').hide();
                $('.next-gallery').hide();
                $('#events').hide();
                $('.events-next-previous').hide();
                $('#events2').hide();
            });


            //GALARY PREVIOUS CKICK
            $('.previous-gallery').on('click', function () {
                $('#gallery2').hide();
                $('#gallery').show();
            });

            $(document).ready(function () {
                $('.modal').modal();
                //Card onclick
                $('.blog').each(function () {
                    let blog_id = $(this).find('.blog_id').attr('data-value');

                    $(this).on('click', function () {
                        location.href = '/blog/' + blog_id;
                    });
                });

                //Card onclick
                $('.ourNews').each(function () {
                    let news_id = $(this).find('.news_id').attr('data-value');


                    $(this).on('click', function () {
                        location.href = '/detailed-news/' + news_id;
                    });
                });

                $('.learningCurve').each(function () {
                    let learning_curve_id = $(this).find('.learning_curve_id').attr('data-value');


                    $(this).on('click', function () {
                        location.href = '/get-detailed-learning-curve/' + learning_curve_id;
                    });
                });


                $('.date').each(function () {
                    var max_length = 10;

                    if ($(this).html().length > max_length) {

                        var short_content = $(this).html().substr(0, max_length);
                        var long_content = $(this).html().substr(max_length);

                        $(this).html(short_content +
                            '<i href="#" class="more_horiz material-icons small"></i>' +

                            '<span class="more_text" style="display:none;">' + long_content + '</span>');

                    }

                });

                $('.top-blog-description').each(function () {
                    var max_length = 70;

                    if ($(this).html().length > max_length) {

                        var short_content = $(this).html().substr(0, max_length);
                        var long_content = $(this).html().substr(max_length);

                        $(this).html(short_content +
                            '<br><br><br><i href="#" class="more_horiz material-icons small">more_horiz</i>' +

                            '<span class="more_text" style="display:none;">' + long_content + '</span>');

                    }

                });

                $('.bottom-blog-description').each(function () {
                    var max_length = 50;

                    if ($(this).html().length > max_length) {

                        var short_content = $(this).html().substr(0, max_length);
                        var long_content = $(this).html().substr(max_length);

                        $(this).html(short_content +
                            '<br><br><br><i href="#" class="more_horiz material-icons small"></i>' +

                            '<span class="more_text" style="display:none;">' + long_content + '</span>');

                    }

                });
            });
        </script>
@endsection
