@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/SmartCity1/main.css"/>
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;500&family=Roboto:wght@100&display=swap" rel="stylesheet"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>

    <body>

    <div class="programpage container-fluid" id="Mobileprogrampage">
        <div class="section standard-top-section">
            <div class="row">
                <div class="parallax-container" id="SmartCityImage">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/plain.jpg"/>
                    </div>
                    <h1 class="top-smart-header" style="color:white;text-shadow: 5px 3px 1px #000000;">Sustainable Devlopment Goals</h1>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align standard-section-with-margins" id="AboutSection">
            <div id="AboutSectionContent">
                <div class="row" style="margin-top: 15vh">
                    <h4 class="standard-header" align="center">What Are Sustainable Development Goals</h4>
                    <p class="standard-paragraphs justtext">
                        Cities and towns have to get smart to manage a combination of influx from urban areas and
                        natural population growth in order to ensure that residents enjoy an increasing quality of life.
                    </p>
                    <h4 class=standard-header align="center">Mission</h4>
                    <p class="standard-paragraphs justtext">
                        Our mission as a smart, hi-tech incubator is to support the successful development and
                        sustainability of innovation as the first choice for the
                        commercialisation of smart products and services for Smart Cities and Smart towns.
                    </p>
                    <br/>
                    <br/>
                </div>
                <div class="row">
                    <div class=" standard-blocks col s6 noPovBtn">
                        <img src="/images/SDG/SDG1.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class="standard-blocks col s6 zeroHungerBtn">
                        <img src="/images/SDG/SDG2.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 goodHealthBtn">
                        <img src="/images/SDG/SDG3.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 educationBtn">
                        <img src="/images/SDG/SDG4.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 equalityBtn">
                        <img src="/images/SDG/SDG5.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 waterBtn">
                        <img src="/images/SDG/SDG6.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 energyBtn">
                        <img src="/images/SDG/SDG7.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 workBtn">
                        <img src="/images/SDG/SDG8.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 industryBtn">
                        <img src="/images/SDG/SDG9.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 inequalitiesBtn">
                        <img src="/images/SDG/SDG10.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 sustainableBtn">
                        <img src="/images/SDG/SDG11.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 productionBtn">
                        <img src="/images/SDG/SDG12.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 climateBtn">
                        <img src="/images/SDG/SDG13.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 belowWaterBtn">
                        <img src="/images/SDG/SDG14.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 onLandBtn">
                        <img src="/images/SDG/SDG15.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 peaceBtn">
                        <img src="/images/SDG/SDG16.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 partnershipButton">
                        <img src="/images/SDG/SDG17.png" style="height: 100%; width: 118%; margin-left: -9%"/>
                    </div>
                    <div class=" standard-blocks col s6 njeButton"></div>
                </div>
            </div>
        </div>



        <!--Image-->
{{--SDG1--}}
        <div class="section col s12" id="mobnopov">
            <div class="row">
                <div>
                    <div style="background: #e5243b">
                        <img src="/images/SDG/SDG1.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins " id="BusinessSection">
            <div class="Business">
                <div class="card" style="background: #e5243b">
                    <div class="card-content white-text hoverable">
                        <span class="center card-title" style="font-size:16px;color:white;"><b>NO POVERTY</b></span>
                    </div>
                </div>
            </div>
            <div class="white darken-1">
                <div class="standard-paragraphs center-align">
                    <p style="margin-top: 1.7%">
                        &nbsp;&nbsp;&nbsp;
                        End poverty in all its forms everywhere.
                    </p>
                </div>
            </div>

        </div>

{{--SDG2--}}
        <div class="section col s12" id="mobzerohunger">
            <div class="row">
                <div>
                    <div style="background: #dda63a;">
                        <img src="/images/SDG/SDG2.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins " id="BuildingRow">
            <div class="Building">
                <div class="card" style="background: #dda63a;">
                    <div class="card-content white-text hoverable">
                        <span class="center card-title" style="font-size:16px"><b>ZERO HUNGER</b></span>
                    </div>
                </div>
            </div>
            <div class="white darken-3">
                <div class="standard-paragraphs center-align">
                    <p style="margin-top: 1.7%">
                        &nbsp;&nbsp;&nbsp;
                        End poverty in all its forms everywhere.
                    </p>
                </div>
            </div>
        </div>

{{--SDG3--}}
        <div class="section col s12" id="mobgoodhealth">
            <div class="row">
                <div>
                    <div style="background: #4c9f38;">
                        <img src="/images/SDG/SDG3.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins">
            <div>
                <div class="card" style="background: #4c9f38;">
                    <div class="card-content white-text hoverable">
                        <span class="center card-title" style="font-size:16px"><b>GOOD HEALTH AND WELL-BEING</b></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">

            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    Ensure healthy lives and promote well-being for all at all ages
                </p>
            </div>
        </div>

{{--SDG4--}}
        <div class="section col s12" id="mobeducation">
            <div class="row">
                <div>
                    <div style="background: #c5192d;">
                        <img src="/images/SDG/SDG4.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="SmartEducationBlock">
            <div class="card" style="background: #c5192d;">
                <div class="card-content white-text hoverable" style="padding-left: 20%;">
                    <span class="center card-title"
                          style="font-size:16px;margin-right: 25px"><b>QUALITY EDUCATION</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    Ensure inclusive and equitable quality education and promote lifelong
                    learning opportunities for all
                </p>
            </div>
        </div>

{{--SDG5--}}
        <div class="section col s12" id="mobequality">
            <div class="row">
                <div>
                    <div style="background: #ff3a21;">
                        <img src="/images/SDG/SDG5.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="SmartEnvinron">
            <div class="card" style="background: #ff3a21;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>GENDER EQUALITY</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    Achieve gender equality and empower all women and girls
                </p>
            </div>
        </div>

{{--SDG6--}}
        <div class="section col s12" id="mobwater">
            <div class="row">
                <div>
                    <div style="background: #26bde2;">
                        <img src="/images/SDG/SDG6.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="SmartEducationBlock">
            <div class="card" style="background: #26bde2;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>CLEAN WATER AND SANITATION</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p>
                    Ensure availability and sustainable management of water and sanitation for all
                </p>
            </div>
        </div>


{{--SDG7--}}
        <div class="section col s12" id="mobenergy">
            <div class="row">
                <div>
                    <div style="background: #fcc30b;">
                        <img src="/images/SDG/SDG7.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #fcc30b;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>AFFORDABLE AND CLEAN ENERGY</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Ensure access to affordable, reliable, sustainable and modern energy for all.
                </p>
            </div>
        </div>

{{--SDG8--}}
        <div class="section col s12" id="mobwork">
            <div class="row">
                <div>
                    <div style="background: #a21942;">
                        <img src="/images/SDG/SDG8.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #a21942;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>DECENT WORK AND ECONOMIC GROWTH</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Promote sustained, inclusive and sustainable economic growth, full and productive employment and decent work for all.
                </p>
            </div>
        </div>

{{--SDG9--}}
        <div class="section col s12" id="mobindustry">
            <div class="row">
                <div>
                    <div style="background: #fd6925">
                        <img src="/images/SDG/SDG9.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #fd6925;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>INDUSTRY, INNOVATION AND INFRASTRUCTURE</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Build resilient infrastructure, promote inclusive and sustainable industrialization and foster innovation.
                </p>
            </div>
        </div>

        {{--SDG10--}}
        <div class="section col s12" id="mobinequalities">
            <div class="row">
                <div>
                    <div style="background: #dd1367">
                        <img src="/images/SDG/SDG10.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #dd1367;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>REDUCED INEQUALITIES</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Reduce inequality within and among countries.
                </p>
            </div>
        </div>

        {{--SDG11--}}
        <div class="section col s12" id="mobsustainable">
            <div class="row">
                <div>
                    <div style="background: #fd9d24">
                        <img src="/images/SDG/SDG11.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #fd9d24;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>SUSTAINABLE CITIES AND COMMUNITIES</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Make cities and human settlements inclusive, safe, resilient and sustainable.
                </p>
            </div>
        </div>

        {{--SDG12--}}
        <div class="section col s12" id="mobproduction">
            <div class="row">
                <div>
                    <div style="background: #bf8b2f">
                        <img src="/images/SDG/SDG12.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #bf8b2f;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>Responsible Consumption And Production</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Ensure sustainable consumption and production patterns.
                </p>
            </div>
        </div>

        {{--SDG13--}}
        <div class="section col s12" id="mobclimate">
            <div class="row">
                <div>
                    <div style="background: #3f7e44;">
                        <img src="/images/SDG/SDG13.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #3f7e44;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>CLIMATE ACTION</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Take urgent action to combat climate change and its impacts*
                </p>
            </div>
        </div>

        {{--SDG14--}}
        <div class="section col s12" id="mobbelowWater">
            <div class="row">
                <div>
                    <div style="background: #0a97d9">
                        <img src="/images/SDG/SDG14.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #0a97d9;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>LIFE BELOW WATER</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Conserve and sustainably use the oceans, seas and marine resources for sustainable development
                </p>
            </div>
        </div>

        {{--SDG15--}}
        <div class="section col s12" id="mobonLand">
            <div class="row">
                <div>
                    <div style="background: #56c02b">
                        <img src="/images/SDG/SDG15.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #56c02b;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>LIFE ON LAND</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Protect, restore and promote sustainable use of terrestrial ecosystems, sustainably manage forests,
                    combat desertification, and halt and reverse land degradation and halt biodiversity loss
                </p>
            </div>
        </div>

        {{--SDG16--}}
        <div class="section col s12" id="mobpeace">
            <div class="row">
                <div>
                    <div style="background: #00689d">
                        <img src="/images/SDG/SDG16.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #00689d;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>PEACE, JUSTICE AND STRONG INSTITUTIONS</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Promote peaceful and inclusive societies for sustainable development, provide access to justice for
                    all and build effective, accountable and inclusive institutions at all levels
                </p>
            </div>
        </div>

        {{--SDG17--}}
        <div class="section col s12" id="mobpartnership">
            <div class="row">
                <div>
                    <div style="background: #19486a">
                        <img src="/images/SDG/SDG17.png" style="height: 30%; width: 30%; margin-left: 65%; margin-top: 3%;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="section center-align standard-section-with-margins" id="HealthCareRow">
            <div class="card" style="background: #19486a;">
                <div class="card-content white-text hoverable">
                    <span class="center card-title" style="font-size:16px"><b>PARTNERSHIPS FOR THE GOALS</b></span>
                </div>
            </div>
        </div>
        <div class="white darken-3 standard-section-with-margins">
            <div class="standard-paragraphs center-align">
                <p style="margin-top: 1.7%">
                    &nbsp;&nbsp;&nbsp;
                    Strengthen the means of implementation and revitalize the global partnership for sustainable
                    development
                </p>
            </div>
        </div>
    </div>






    <!--SDG DESKTOP SECTION-->
    <div class="programpage container-fluid" id="Desktopprogrampage">

        <div class="section standard-top-section">
            <div class="row">
                <div class="parallax-container" id="SmartTopSection">
                    <div class="parallax">
                        <img src="/images/SmartCityImages/plain.jpg" id="SmartCityImage"/>
                    </div>
                    <h1 class="Top-smart-headers" style="color: white;">Sustainable Development Goals</h1>
                </div>
            </div>
        </div>


        <div class="section center-align standard-section-with-margins" id="AboutSection">
            <div id="AboutSectionContent">
                <div class="row" >
                    <div class="col m4 left-align">
                        <h4 class="standard-header" align="center" style="padding-right: 25px">What Are Sustainable Development Goals</h4>
                        <p class="standard-paragraphs justtext" style="padding-right: 25px">
                            The Sustainable Development Goals or Global Goals are a collection of 17 interlinked global
                            goals designed to be a "blueprint to achieve a better and more sustainable future for all".
                            The SDGs were set up in 2015 by the United Nations General Assembly and are intended to be
                            achieved by the year 2030.
                        </p>
                        <h4 class="standard-header" align="center" style="padding-right: 25px">Mission</h4>
                        <p class="standard-paragraphs justtext" style="padding-right: 25px">
                            Our mission as a smart, hi-tech incubator is to support the successful development and
                            sustainability of innovation as the first choice for the
                            commercialisation of smart products and services for Smart Cities and Smart towns.
                        </p>
                    </div>
                    <div class="col m1"></div>
                    <div class="col m6 center-align" style="width: 66%;">
                        <div class="row">
                            <div class=" standard-blocks col m2 NoPovBtn" style="height: 150px">
                                <img src="/images/SDG/SDG1.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class="standard-blocks col m2 ZeroHungerBtn" style="height: 150px">
                                <img src="/images/SDG/SDG2.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 GoodHealthBtn" style="height: 150px">
                                <img src="/images/SDG/SDG3.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 EducationBtn" style="height: 150px">
                                <img src="/images/SDG/SDG4.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 EqualityBtn" style="height: 150px">
                                <img src="/images/SDG/SDG5.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 WaterBtn" style="height: 150px">
                                <img src="/images/SDG/SDG6.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class=" standard-blocks col m2 EnergyBtn" style="height: 150px">
                                <img src="/images/SDG/SDG7.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 WorkBtn" style="height: 150px">
                                <img src="/images/SDG/SDG8.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 IndustryBtn" style="height: 150px">
                                <img src="/images/SDG/SDG9.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 InequalitiesBtn" style="height: 150px">
                                <img src="/images/SDG/SDG10.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 SustainableBtn" style="height: 150px">
                                <img src="/images/SDG/SDG11.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 ProductionBtn" style="height: 150px">
                                <img src="/images/SDG/SDG12.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="standard-blocks col m2 ClimateBtn" style="height: 150px">
                                <img src="/images/SDG/SDG13.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 BelowWaterBtn" style="height: 150px">
                                <img src="/images/SDG/SDG14.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 OnLandBtn" style="height: 150px">
                                <img src="/images/SDG/SDG15.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 PeaceBtn" style="height: 150px">
                                <img src="/images/SDG/SDG16.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                            <div class=" standard-blocks col m2 PartnershipBtn" style="height: 150px">
                                <img src="/images/SDG/SDG17.png" style="height: 100%; width: 120%; margin-left: -9%"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--No Poverty Section-->
        <div class="" id="nopov"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div class="row">
                <div>
                    <div style="background: #e5243b;">
                        <img src="/images/SDG/SDG1.png" class="sdgBanner"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="section center-align ">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG1">
                            <div>
                                <div class="card-content section center-align">
                                    <span class="" style="font-size:18px;color: white">No Poverty</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                End poverty in all its forms everywhere.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>

                @foreach($no_poverty as $venture)
                    @if($venture->status == 'Active')
                    <div class="col s12 m3" style="width: 370px;">
                        <div class="card white smart hoverable">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 250px;">
                                    <div class="user-view center-align">
                                        <a href="#user">
                                            <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">

                                        </a>
                                    </div>
                                    <br/>

                                    <div>
                                        <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                        <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                    </div>
                                </div>
                                <br>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>

        {{--@* Zero Hunger Section *@--}}
        <div class="" id="zerohunger"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div class="row">
                <div>
                    <div style="background: #dda63a;">
                        <img src="/images/SDG/SDG2.png" class="sdgBanner"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="section center-align ">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG2">
                            <div>
                                <div class="card-content section center-align">
                                    <span style="font-size:18px;color: white">Zero Hunger</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                End hunger, achieve food security and improved nutrition and promote sustainable agriculture
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($zero_hunger as $venture)
                    @if($venture->status == 'Active')
                <div class="col s12 m3" style="width: 370px;">
                    <div class="card white smart hoverable">
                        <div class="circle">
                            <div class="card-content black-text " style="height: 250px;">
                                <div class="user-view center-align">
                                    <a href="#user">
                                        <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                    </a>
                                </div>
                                <br/>
                                <div>
                                    <div>
                                        <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                        <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                        </div>
                    </div>
                </div>
                    @endif
                @endforeach
            </div>
        </div>


        {{--@* Good Health Section *@--}}
        <div class="" id="goodhealth"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #4c9f38;">
                    <img src="/images/SDG/SDG3.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-3">
                        <div class="col s12 m3" id="SDG3">
                            <div>
                                <div class="card-content section center-align">
                                    <span style="font-size:18px;color: white">Good Health And <br>Well-Being</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Ensure healthy lives and promote well-being for all at all ages.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($good_health as $venture)
                    @if($venture->status == 'Active')
                <div class="col s12 m3" style="width: 370px;">
                    <div class="card white smart hoverable">
                        <div class="circle">
                            <div class="card-content black-text " style="height: 250px;">
                                <div class="user-view center-align">
                                    <a href="#user">
                                        <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                    </a>
                                </div>
                                <br/>
                                <div>
                                    <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                    <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                </div>
                            </div>
                            <br>
                            <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                        </div>
                    </div>
                </div>
                    @endif
                @endforeach
            </div>
        </div>


    <!--Quality Education Section-->
        <div class="" id="education"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #c5192d;">
                    <img src="/images/SDG/SDG4.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col 1"></div>
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m2" id="SDG4">
                            <div class="">
                                <div class="card-content black-text">
                                    <span class="card-title" style="font-size:18px;color: white">Quality Education</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Ensure inclusive and equitable quality education and promote lifelong learning opportunities for all.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($quality_education as $venture)
                @if($venture->status == 'Active')
                <div class="col s12 m3" style="width: 370px;">
                    <div class="card white smart hoverable">
                        <div class="circle">
                            <div class="card-content black-text " style="height: 250px;">
                                <div class="user-view center-align">
                                    <a href="#user">
                                        <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                    </a>
                                </div>
                                <br/>
                                <div>
                                    <div>
                                        <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                        <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                        </div>
                    </div>
                </div>
                    @endif
                @endforeach
            </div>
        </div>

        {{--@* Gender Equality Section *@--}}
        <div class="" id="equality"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div class="row">
                <div>
                    <div style="background: #ff3a21;">
                        <img src="/images/SDG/SDG5.png" class="sdgBanner"/>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG5">
                            <div>
                                <div class="card-content section center-align white-text">
                                    <span style="font-size:18px">Gender Equality</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Achieve gender equality and empower all women and girls.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>


            @foreach($gender_equality as $venture)
                @if($venture->status == 'Active')
                <div class="col s12 m3" style="width: 370px;">
                    <div class="card white smart hoverable">
                        <div class="circle">
                            <div class="card-content black-text " style="height: 250px;">
                                <div class="user-view center-align">
                                    <a href="#user">
                                        <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                    </a>
                                </div>
                                <br/>
                                <div>
                                    <div>
                                        <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                        <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                        </div>
                    </div>
                </div>
                @endif
            @endforeach
        </div>
    </div>


        {{--@* Clean Water Section *@--}}
        <div class="" id="water"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #26bde2;">
                    <img src="/images/SDG/SDG6.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG6">
                            <div>
                                <div class="card-content white-text ">
                                    <span style="font-size:18px">Clean Water And Sanitation</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Ensure availability and sustainable management of water and sanitation for all.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($clean_water as $venture)
                    @if($venture->status == 'Active')
                    <div class="col s12 m3" style="width: 370px;">
                        <div class="card white smart hoverable">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 250px;">
                                    <div class="user-view center-align">
                                        <a href="#user">
                                            <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                        </a>
                                    </div>
                                    <br/>
                                    <div>
                                        <div>
                                            <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                            <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>

        </div>


        {{--@* HealthCare Section *@--}}
        <div class="" id="energy"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #fcc30b;">
                    <img src="/images/SDG/SDG7.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG7">
                            <div>
                                <div class="card-content white-text ">
                                    <span style="font-size:18px">Affordable And <br>Clean Energy</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Ensure access to affordable, reliable, sustainable and modern energy for all.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($clean_energy as $venture)
                    @if($venture->status == 'Active')
                    <div class="col s12 m3" style="width: 370px;">
                        <div class="card white smart hoverable">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 250px;">
                                    <div class="user-view center-align">
                                        <a href="#user">
                                            <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                        </a>
                                    </div>
                                    <br/>
                                    <div>
                                        <div>
                                            <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                            <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>

        {{--@* Infrastructure Section *@--}}
        <div class="" id="work"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #a21942;">
                    <img src="/images/SDG/SDG8.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>

        <div class="section center-align">
            <div class="row" style="margin-right: 3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG8">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Decent Work And <br> Economic Growth</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Promote sustained, inclusive and sustainable economic growth, full and productive employment and decent work for all.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($decent_work as $venture)
                    @if($venture->status == 'Active')
                    <div class="col s12 m3" style="width: 370px;">
                        <div class="card white smart hoverable">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 250px;">
                                    <div class="user-view center-align">
                                        <a href="#user">
                                            <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                        </a>
                                    </div>
                                    <br/>
                                    <div>
                                        <div>
                                            <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                            <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>


        {{--@* Industry Section *@--}}
        <div class="" id="industry"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #fd6925">
                    <img src="/images/SDG/SDG9.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG9">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Industry, Innovation<br>And Infrastructure</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Build resilient infrastructure, promote inclusive and sustainable industrialization and foster innovation.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($industry as $venture)
                    @if($venture->status == 'Active')
                    <div class="col s12 m3" style="width: 370px;">
                        <div class="card white smart hoverable">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 250px;">
                                    <div class="user-view center-align">
                                        <a href="#user">
                                            <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                        </a>
                                    </div>
                                    <br/>
                                    <div>
                                        <div>
                                            <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                            <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    {{--@* Inequalities Section *@--}}
        <div class="" id="inequalities"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #dd1367">
                    <img src="/images/SDG/SDG10.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG10">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Reduced<br>Inequalities</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Reduce inequality within and among countries.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($inequalities as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3" style="width: 370px;">
                            <div class="card white smart hoverable">
                                <div class="circle">
                                    <div class="card-content black-text " style="height: 250px;">
                                        <div class="user-view center-align">
                                            <a href="#user">
                                                <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">

                                            </a>
                                        </div>
                                        <br/>
                                        <div>
                                            <div>
                                                <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                                <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    {{--@* Sustainable Cities Section *@--}}
        <div class="" id="sustainable"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #fd9d24">
                    <img src="/images/SDG/SDG11.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG11">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Sustainable Cities<br>And Communities</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Make cities and human settlements inclusive, safe, resilient and sustainable.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($communities as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3" style="width: 370px;">
                            <div class="card white smart hoverable">
                                <div class="circle">
                                    <div class="card-content black-text " style="height: 250px;">
                                        <div class="user-view center-align">
                                            <a href="#user">
                                                <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                            </a>
                                        </div>
                                        <br/>
                                        <div>
                                            <div>
                                                <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                                <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    {{--@* Responsible Production Section *@--}}
        <div class="" id="production"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #bf8b2f">
                    <img src="/images/SDG/SDG12.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG12">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Responsible<br>Consumption<br>And Production</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Ensure sustainable consumption and production patterns.
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($production as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3" style="width: 370px;">
                            <div class="card white smart hoverable">
                                <div class="circle">
                                    <div class="card-content black-text " style="height: 250px;">
                                        <div class="user-view center-align">
                                            <a href="#user">
                                                <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">

                                            </a>
                                        </div>
                                        <br/>
                                        <div>
                                            <div>
                                                <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                                <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    {{--@* Climate Action Section *@--}}
        <div class="" id="climate"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #3f7e44">
                    <img src="/images/SDG/SDG13.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG13">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Climate<br>Action</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Take urgent action to combat climate change and its impacts*
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($climate as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3" style="width: 370px;">
                            <div class="card white smart hoverable">
                                <div class="circle">
                                    <div class="card-content black-text " style="height: 250px;">
                                        <div class="user-view center-align">
                                            <a href="#user">
                                                <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                            </a>
                                        </div>
                                        <br/>
                                        <div>
                                            <div>
                                                <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                                <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    {{--@* Life Below Water Section *@--}}
        <div class="" id="belowWater"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #0a97d9">
                    <img src="/images/SDG/SDG14.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG14">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Life Below<br>Water</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Conserve and sustainably use the oceans, seas and marine resources for sustainable development
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($below_water as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3" style="width: 370px;">
                            <div class="card white smart hoverable">
                                <div class="circle">
                                    <div class="card-content black-text " style="height: 250px;">
                                        <div class="user-view center-align">
                                            <a href="#user">
                                                <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                            </a>
                                        </div>
                                        <br/>
                                        <div>
                                            <div>
                                                <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                                <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    {{--@* Life On Land Section *@--}}
        <div class="" id="onLand"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #56c02b">
                    <img src="/images/SDG/SDG15.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG15">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Life<br>On Land</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1%">
                                &nbsp;&nbsp;&nbsp;
                                Protect, restore and promote sustainable use of terrestrial ecosystems, sustainably manage
                                forests, combat desertification, and halt and reverse land degradation and halt biodiversity loss
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($on_land as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3" style="width: 370px;">
                            <div class="card white smart hoverable">
                                <div class="circle">
                                    <div class="card-content black-text " style="height: 250px;">
                                        <div class="user-view center-align">
                                            <a href="#user">
                                                <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                            </a>
                                        </div>
                                        <br/>
                                        <div>
                                            <div>
                                                <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                                <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    {{--@* Peace Section *@--}}
        <div class="" id="peace"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #00689d">
                    <img src="/images/SDG/SDG16.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG16">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Peace, Justice<br>And Strong<br>Institutions</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Promote peaceful and inclusive societies for sustainable development, provide access to
                                justice for all and build effective, accountable and inclusive institutions at all levels
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($peace as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3" style="width: 370px;">
                            <div class="card white smart hoverable">
                                <div class="circle">
                                    <div class="card-content black-text " style="height: 250px;">
                                        <div class="user-view center-align">
                                            <a href="#user">
                                                <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                            </a>
                                        </div>
                                        <br/>
                                        <div>
                                            <div>
                                                <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                                <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    {{--@* Partnership Section *@--}}
        <div class="" id="partnership"></div>
        <br/>
        <br/>
        <div class="section center-align">
            <div>
                <div style="background: #19486A">
                    <img src="/images/SDG/SDG17.png" class="sdgBanner"/>
                </div>
            </div>
        </div>
        <br/>
        <div class="section center-align">
            <div class="row" style="margin-right:3em;margin-left: 3em;">
                <div class="col s12 m12">
                    <div class="card white darken-1">
                        <div class="col s12 m3" id="SDG17">
                            <div>
                                <div class="card-content black-text ">
                                    <span style="font-size:18px;color:white;">Partnerships<br>For The Goals</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-content standard-paragraphs hoverable" style="height: 110px">
                            <p style="margin-top: 1.7%">
                                &nbsp;&nbsp;&nbsp;
                                Strengthen the means of implementation and revitalize the global partnership for sustainable
                                development
                            </p>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
                @foreach($partnerships as $venture)
                    @if($venture->status == 'Active')
                        <div class="col s12 m3" style="width: 370px;">
                            <div class="card white smart hoverable">
                                <div class="circle">
                                    <div class="card-content black-text " style="height: 250px;">
                                        <div class="user-view center-align">
                                            <a href="#user">
                                                <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                            </a>
                                        </div>
                                        <br/>
                                        <div>
                                            <div>
                                                <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}} </h6>
                                                <p class="robotobody limit"> {{$venture->elevator_pitch}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

    </div>

        <script>
            //Card onclick
            $('.smart').each(function () {

                let venture_id = $(this).find('.venture_id').attr('data-value');

                $(this).on('click', function () {
                    location.href = '/Home/Founders/' + venture_id;
                });
            });


            $(document).ready(function () {
                $('.parallax').parallax();

            });


            $(document).ready(function () {
                $('.NoPovBtn').on('click', function () {
                    window.location.href = "#nopov";
                });
            });

            $(document).ready(function () {
                $('.ZeroHungerBtn').on('click', function () {
                    location.href = "#zerohunger";
                });
            });

            $(document).ready(function () {
                $('.GoodHealthBtn').on('click', function () {
                    location.href = "#goodhealth";
                });
            });

            $(document).ready(function () {
                $('.EducationBtn').on('click', function () {
                    location.href = "#education";
                });
            });

            $(document).ready(function () {
                $('.EqualityBtn').on('click', function () {
                    location.href = "#equality";
                });
            });

            $(document).ready(function () {
                $('.WaterBtn').on('click', function () {
                    location.href = "#water";
                });
            });

            $(document).ready(function () {
                $('.EnergyBtn').on('click', function () {
                    location.href = "#energy";
                });
            });

            $(document).ready(function () {
                $('.WorkBtn').on('click', function () {
                    location.href = "#work";
                });
            });

            $(document).ready(function () {
                $('.IndustryBtn').on('click', function () {
                    location.href = "#industry";
                });
            });

            $(document).ready(function () {
                $('.InequalitiesBtn').on('click', function () {
                    location.href = "#inequalities";
                });
            });

            $(document).ready(function () {
                $('.SustainableBtn').on('click', function () {
                    location.href = "#sustainable";
                });
            });

            $(document).ready(function () {
                $('.ProductionBtn').on('click', function () {
                    location.href = "#production";
                });
            });

            $(document).ready(function () {
                $('.ClimateBtn').on('click', function () {
                    location.href = "#climate";
                });
            });

            $(document).ready(function () {
                $('.BelowWaterBtn').on('click', function () {
                    location.href = "#belowWater";
                });
            });

            $(document).ready(function () {
                $('.OnLandBtn').on('click', function () {
                    location.href = "#onLand";
                });
            });

            $(document).ready(function () {
                $('.PeaceBtn').on('click', function () {
                    location.href = "#peace";
                });
            });

            $(document).ready(function () {
                $('.PartnershipBtn').on('click', function () {
                    location.href = "#partnership";
                });
            });

            //----------------------------------------------------------------
            //Mobile Buttons
            $(document).ready(function () {
                $('.noPovBtn').on('click', function () {
                    window.location.href = "#mobnopov";
                });
            });

            $(document).ready(function () {
                $('.zeroHungerBtn').on('click', function () {
                    location.href = "#mobzerohunger";
                });
            });

            $(document).ready(function () {
                $('.goodHealthBtn').on('click', function () {
                    location.href = "#mobgoodhealth";
                });
            });

            $(document).ready(function () {
                $('.educationBtn').on('click', function () {
                    location.href = "#mobeducation";
                });
            });

            $(document).ready(function () {
                $('.equalityBtn').on('click', function () {
                    location.href = "#mobequality";
                });
            });

            $(document).ready(function () {
                $('.waterBtn').on('click', function () {
                    location.href = "#mobwater";
                });
            });

            $(document).ready(function () {
                $('.energyBtn').on('click', function () {
                    location.href = "#mobenergy";
                });
            });

            $(document).ready(function () {
                $('.workBtn').on('click', function () {
                    location.href = "#mobwork";
                });
            });

            $(document).ready(function () {
                $('.industryBtn').on('click', function () {
                    location.href = "#mobindustry";
                });
            });

            $(document).ready(function () {
                $('.inequalitiesBtn').on('click', function () {
                    location.href = "#mobinequalities";
                });
            });

            $(document).ready(function () {
                $('.sustainableBtn').on('click', function () {
                    location.href = "#mobsustainable";
                });
            });

            $(document).ready(function () {
                $('.productionBtn').on('click', function () {
                    location.href = "#mobproduction";
                });
            });

            $(document).ready(function () {
                $('.climateBtn').on('click', function () {
                    location.href = "#mobclimate";
                });
            });

            $(document).ready(function () {
                $('.belowWaterBtn').on('click', function () {
                    location.href = "#mobbelowWater";
                });
            });

            $(document).ready(function () {
                $('.onLandBtn').on('click', function () {
                    location.href = "#mobonLand";
                });
            });

            $(document).ready(function () {
                $('.peaceBtn').on('click', function () {
                    location.href = "#mobpeace";
                });
            });

            $(document).ready(function () {
                $('.partnershipBtn').on('click', function () {
                    location.href = "#mobpartnership";
                });
            });

        </script>
    </body>
@endsection
