@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/LandingPage/landingPage.css"/>

     <div class="section landingDesktop " id="back">
        <br>
        <p class="p2" id="demo"></p>
         <p class="center-align" style="color: white;font-style: italic;font-size: 1.3em">We hope to see you at our new office soon !!!</p>
         <div class="row" style="margin-top: 25vh;">
             <div class="col s2">
                 <img style="width: 300px;;height: 30vh" src="/images/outline-of-houses.png">
             </div>

             <div class="col s3">
                 <marquee  direction="right">
                     <img style="width: 105px;;height: 20vh;margin-top: 5vh;" src="/images/Carriers.png">
                 </marquee>
             </div>
             <div class="col s4" style="margin-right: 3em">
                 <img style="width: 200px;height: 30vh;margin-right:  3em" src="/images/Jet-removebg-preview.png">
             </div>
             <div class="col s2">
                 <img style="width: 300px;height: 30vh;" src="/images/kopetski-house-front_750xx1095-616-29-0-removebg-preview.png">
             </div>
         </div>
    </div>


    <div class="section landingMobile " id="back">
        <br>
        <iframe style="margin-left: 1.3em" src="https://free.timeanddate.com/countdown/i70ip72f/n1485/cf105/cm0/cu4/ct0/cs0/ca0/cr0/ss0/cac000/cpc000/pcfff/tcf1d8e7/fs100/szw320/szh135/tac000/tpc000/macfff/mpc000/iso2019-12-02T10:10:10" allowTransparency="true" frameborder="0" width="320" height="135"></iframe>

        <br>
        <p class="p2" id="demo"></p>
        <p class="center-align" style="color: white;font-style: italic;font-size: 1em">We hope to see you at our new office soon !!!</p>


        <div class="row" style="margin-top: 15vh;">
            <div class="col s2">
                <img style="width: 50px;height: 10vh" src="/images/outline-of-houses.png">
            </div>

            <div class="col s3">
                <marquee  direction="right">
                    <img style="width: 20px;margin-left: 20px;height: 5vh;margin-top: 3vh" src="/images/Carriers.png">
                </marquee>
            </div>
            <div class="col s4">
                <img style="width: 50px;height: 10vh;" src="/images/Jet.png">
            </div>
            <div class="col s2">
                <img style="width: 50px;height: 10vh;" src="/images/kopetski-house-front_750xx1095-616-29-0-removebg-preview.png">
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });
        let countDownDate = new Date("Dec 01, 2019 15:37:25").getTime();

        // Update the count down every 1 second
        let x = setInterval(function() {

            // Get today's date and time
            let now = new Date().getTime();

            // Find the distance between now and the count down date
            let distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("demo").innerHTML = days  + " &nbsp; &nbsp;:  &nbsp; &nbsp;" + hours + "&nbsp; &nbsp;:  &nbsp; &nbsp"
                + minutes + "&nbsp; &nbsp;:  &nbsp; &nbsp" + seconds+"<br>" +"Day  &nbsp; &nbsp;"+"Hours &nbsp; &nbsp;"+"Min  &nbsp; &nbsp;"+"Sec";

            // If the count down is over, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>

@endsection
