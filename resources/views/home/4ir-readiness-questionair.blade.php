@extends('layouts.app')

@section('content')

    <div class="row center-align " style="margin-top: 9vh;margin-left: 3em;margin-right: 3em">
        <h4><strong>4IR readiness questionnaire</strong></h4>
        <br>
        <p>Download</p>
        <a href="/files/4IR_readiness_questionnaire.pdf" target="_blank">
            <img style="margin-left: 1em" class="hoverable" src="/images/pdf_image.png" width="80" height="100">
        </a>

        <br>

    </div>

    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });
    </script>
@endsection
