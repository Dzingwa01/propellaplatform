@extends('layouts.app')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>

    <div class="section" style="margin-top: 2em;">
        <div class="card white" style="width: 800px;margin: 0 auto">
            <br>
            <h4 style="margin-left: 4em">WORKSHOP EVALUATION FORM</h4>
            <br>
            <div class="row" style="margin-left: 4em; margin-right: 4em;">
                <ol>
                    @foreach ($workshop_questions as $question)
                        <p>{{ $question->question_text}} </p>
                        <div class="input-field">
                            <input id="{{$question->id}}" class="materialize-textarea answer-text-desktop"
                                   style="width:90%">
                        </div>
                    @endforeach
                </ol>
            </div>

            <div class="row center">
                <div class="col l6 m5 s12">
                    <button class="btn waves-effect waves-light" id="upload-answers-form-desktop"> Save <i
                            class="material-icons right">send</i></button>
                </div>
            </div>
        </div>
    </div>



    <script>

        $(document).ready(function () {
            $('.tooltipped').tooltip();

            //Desktop Form
            $('#upload-answers-form-desktop').on('click', function () {

                let answersArray = [];

                $('.answer-text-desktop').each(function () {
                    let model = {
                        w_question_id: this.id,
                        answer_text: this.value,
                    };

                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));

                $.ajax({
                    url: "store-workshop-answers",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });


    </script>

@endsection
