@extends('layouts.app')

@section('content')

    <body>

    <div class="row" style="margin-top: 14vh;margin-left: 320px">
        <h4><strong>Benguela Current Convention Youth Innovation Challenge</strong></h4>
        <br>
        <div class="" style="margin-left: 270px">
        <div class="col s3">
            <p>Who is Propella</p>
            <a href="/files/Who is Propella v2.pdf" target="_blank">
                <img style="margin-left: 1em" class="hoverable" src="/images/pdf.png" width="80" height="100">
            </a>
        </div>
        <div class="col s3">
            <p>Programme Methodology</p>
            <a href="/files/20210505 Bengula Current Convention.pdf" target="_blank">
                <img style="margin-left: 3em" class="hoverable" src="/images/pdf.png" width="80" height="100">
            </a>
        </div>
        </div>
    </div>

    </body>
    @endsection
