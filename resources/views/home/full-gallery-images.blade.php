@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>
    <br>

    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTWSFMC"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="container" style="width: 100%;">
        <div class="row">
            <h4>{{$propellaHubGallery->description}}</h4>
        </div>
        <div class="row center" style="width: 100%;">
            @foreach($media_images_array as $media_image)
                @if(isset($media_image->image_url))
                    <div class="col s12 m6 l3" style="height: 250px; margin-bottom: 2em;">
                        <div style="border-style: hidden; height: 100%; width: 100%; ">
                            <img style="width:100%; height:100%;" class="materialboxed"
                                 src="/storage/{{$media_image->image_url}}">
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

    </div>
    </body>
    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();
        });
    </script>

    @endsection
