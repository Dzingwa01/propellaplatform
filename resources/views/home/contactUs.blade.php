@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <link rel="stylesheet" type="text/css" href="/css/contactUs/contactUs.css"/>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>

            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>
    <br />
    <br />

    <div>

{{--DesktopContactUs--}}
<div class="contactUsDesktop" style="margin-top: 10vh">
    <div class="row" style="margin-left: 200px" id="desktopContactUs">
        <!--<div class="card" style="width: 1100px;margin-left: 1em">-->
            <h4 class="center-align"><b>Propella Business Incubator Address</b></h4>
        <br>
        <!--</div>-->
        <div class="col s4">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13235.449931524925!2d25.63019166828253!3d-33.97037390865128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e7ad29062e8535f%3A0x5e1e63b2b2e72c48!2s7%20Oakworth%20Rd%2C%20South%20End%2C%20Port%20Elizabeth%2C%206001!5e0!3m2!1sen!2sza!4v1575629299897!5m2!1sen!2sza" width="600" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
        <div class="col s1"></div>

    <div class="col s4" style="color: white; margin-left: 8em; border: solid; border-color: lightgrey; border-width: thin; border-radius: 10px; height: 300px; background-color: rgba(0, 47, 95, 1); font-weight: bolder">
        <p>Address :  7 Oakworth Road <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;South End <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Port Elizabeth <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6001</p>
        <p>Tel: +27 41 502 3700</p>
        <p>Contact number : 063 022 8233</p>
        <p>Fax : 0861 55 55 33</p>
        <p>Email : <a href="mailto:pbimarketing@propellaincubator.co.za">pbimarketing@propellaincubator.co.za</a></p>
    </div>
    </div>
    <br>
    </div>
    <!--Mobile-->
    <div class="contactUsMobile">
    <div class="row" style="margin-left: 0.2px;margin-right: 0.2px" id="mobileContactUs">
        <div class="card" style="width: 98%;margin-left: 1px">
            <h5 style="text-align: center"><b>Propella Business Incubator Address</b></h5>
        </div>
        <div class="col s4">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13235.449931524925!2d25.63019166828253!3d-33.97037390865128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e7ad29062e8535f%3A0x5e1e63b2b2e72c48!2s7%20Oakworth%20Rd%2C%20South%20End%2C%20Port%20Elizabeth%2C%206001!5e0!3m2!1sen!2sza!4v1575629299897!5m2!1sen!2sza" width="340%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
        <div class="col s1"></div>
    </div>
        <div class="col s4" style="color: white;margin-left: 0em; border: solid; border-color: lightgrey; border-width: thin; border-radius: 10px; height: 300px; background-color: rgba(0, 47, 95, 1); font-weight: bolder">
            <p>Address :  7 Oakworth Road <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;South End <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Port Elizabeth <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6001</p>
            <p>Tel: +27 41 502 3700</p>
            <p>Contact number : 068 181 9918</p>
            <p>Fax : 0861 55 55 33</p>
            <p>Email : <a href="mailto:info@propellaincubator.co.za">info@propellaincubator.co.za</a></p>
        </div>
    <br>
    </div>
    </div>
    <br>
    <br>
    @endsection
