@extends('layouts.app')

@section('content')

    <div class="row" style="margin-top: 10vh;margin-left: 80px;margin-right: 80px">
        <h4 class="center-align"><b>Propella Careers</b></h4>
        @for($i = 0; $i<count($careers); $i++)
            <ul class="collapsible">
                <li>
                    <div class="collapsible-header" id="txt"><i class="material-icons">business_center</i>Job Title : {{isset($careers[$i]->title) ? $careers[$i]->title : 'No title'}}</div>
                    <div class="collapsible-body">
                        <span>
                            {{isset($careers[$i]->description) ? $careers[$i]->description : 'No description'}}.
                        </span>
                        <span>
                            @if(isset($careers[$i]->careersSummernote))
                                {!!$careers[$i]->careersSummernote->content!!}
                            @endif
                        </span>
                    </div>
                </li>
            </ul>
        @endfor
    </div>

    <script>
        $(document).ready(function(){
            $('.collapsible').collapsible();
        });
    </script>

    <style>
        .collapsible-header:hover {
            background-color: lightslategray;
        }
        #txt:hover {
            text-decoration: underline;
            color: white;
        }
    </style>

@endsection
