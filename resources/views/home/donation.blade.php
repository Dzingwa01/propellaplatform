@extends('layouts.app')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <!-- jQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

    </head>
    <link rel="stylesheet" type="text/css" href="/css/Donation/donate.css"/>
    <form id="desktop" enctype="multipart/form-data" class="form-horizontal">
        @csrf
        <br>
        <div class="container-fluid">
            <div class="card" style="margin-left: 30em;margin-right: 30em">
            <div class="row" style="margin-left: 2em;margin-right: 2em">
                <br>
                <h4 class="center-align"><b>Donation Form</b></h4>
                <p style="font-style: italic" class="center-align">Propella is transforming the entrepreneurial landscape one innovative venture at a time.
                    Help/Partner with us to do more and reach more entrepreneurs, today.
                </p>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="name" type="text" class="validate">
                        <label for="name">Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" class="validate">
                        <label for="email">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="contact_number" type="text" class="validate">
                        <label for="contact_number">Contact number</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="message" type="text" class="validate">
                        <label for="message">Message</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col offset-m4">
                        <button id="save-donation-form" style="margin-left: 2em"
                                class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
            </div>
            <br>
            </div>
        </div>
    </form>
    <br>
    <br>

    <form id="mobile" enctype="multipart/form-data" class="form-horizontal">
        @csrf
        <br>
        <div class="container-fluid">
            <div class="card">
                <div class="row" style="margin-left: 2em;margin-right: 2em">
                    <br>
                    <h4 class="center-align"><b>Donation Form</b></h4>
                    <p style="font-style: italic" class="center-align">Propella is transforming the entrepreneurial landscape one innovative venture at a time.
                        Help/Partner with us to do more and reach more entrepreneurs, today.
                    </p>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="name-mobile" type="text" class="validate">
                            <label for="name-mobile">Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="email-mobile" type="email" class="validate">
                            <label for="email-mobile">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="contact_number-mobile" type="text" class="validate">
                            <label for="contact_number-mobile">Contact number</label>
                        </div>
                    </div>
                    <div class="row" id="save-mobile">
                        <div class="input-field col s12">
                            <input id="message-mobile" type="text" class="validate">
                            <label for="message-mobile">Message</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col offset-m4">
                            <button id="save-donation-form-mobile" style="margin-left: 2em"
                                    class="btn waves-effect waves-light" type="submit" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </form>


    <script>
        $(document).ready(function () {
            $('#save-donation-form').on('click', function () {

                let formData = new FormData();
                formData.append('name', $('#name').val());
                formData.append('email', $('#email').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('message', $('#message').val());
                $.ajax({
                    url: '{{route('store-donation')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},


                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
                swal(
                    'Success',
                    'User saved <b style="color:green;">Successfully. Thank you</b>',
                    'success',
                    setTimeout(function(){
                        window.location.href = '{{url('/')}}';
                    }, )
                )
            });


            // Alert With Custom Icon and Background Image
            $(document).on('click', '#icon', function(event) {
                swal({
                    title: 'Custom icon!',
                    text: 'Alert with a custom image.',
                    imageUrl: 'https://image.shutterstock.com/z/stock-vector--exclamation-mark-exclamation-mark-hazard-warning-symbol-flat-design-style-vector-eps-444778462.jpg',
                    imageWidth: 200,
                    imageHeight: 200,
                    imageAlt: 'Custom image',
                    animation: false
                })
            });


            $('#save-donation-form-mobile').on('click', function () {

                let formData = new FormData();
                formData.append('name', $('#name-mobile').val());
                formData.append('email', $('#email-mobile').val());
                formData.append('contact_number', $('#contact_number-mobile').val());
                formData.append('message', $('#message-mobile').val());
                $.ajax({
                    url: '{{route('store-donation')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
                swal(
                    'Success',
                    'User saved <b style="color:green;">Successfully. Thank you</b>',
                    'success',
                    setTimeout(function(){
                        window.location.href = '{{url('/')}}';
                    }, )
                )
            });

        });
    </script>

@endsection
