@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Events/showEvents.css"/>
    <style>
        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            font-size: 70px;
            line-height: 1.15em;
            font-weight: 600;
        }
    </style>
    <div class="blogDesktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/film-1668918_960_720.jpg" style="width: 100%;height: 662px;opacity: 0.2;">
                    </div>
                    <div class="centered"><b>Propella Event Gallery</b></div>
                </div>
            </div>
        </div>
    </div>
    <!--Desktop-->
    <div class="row desktopShowEvent" style="text-align: center; margin-left: 3em; margin-right: 3em;">
        @foreach ($events as $event_list)
            @if(new DateTime() > $event_list->start)
                @if(isset($event_list->EventsMedia))
                    @if(count($event_list->EventsMedia->eventsMediaImage) > 0)
                    <div class="col s12 m3">
                        <div class="card white hoverable" style="border-radius: 20px;">
                            <div class="circle">
                                <div class="card-content black-text" style="height: 100%;">
                                    <div class="user-view" align="center">
                                        <a href="#user">
                                            <img class="fullscreen" style="height: 100px;width: 250px;"
                                                 src="{{isset($event_list->event_image_url)?'/storage/'.$event_list->event_image_url:''}}">
                                        </a>
                                    </div>
                                    <h6 class="">{{$event_list->title}}</h6>
                                    <h6 class="" id="start"
                                        value="{{$event_list->start->toDateString()}}">{{$event_list->start->toDateString()}} </h6>
                                </div>
                                <div class="card-action">
                                    @if(count($event_list->EventsMedia->eventsMediaImage) > 0)
                                        <div class="view_media" id="photo"
                                             style="width:100%; border-radius: 10px; margin-top: 5px;">
                                            <a style="color: orange;"
                                               href="{{url('/full_calendar/show-gallery/'.$event_list->slug)}}"
                                               onclick="viewMedia(this)">View Gallery</a>
                                        </div>
                                    @else
                                        <div class="view_media" id="photo"
                                             style="width:100%; border-radius: 10px; margin-top: 5px;">
                                            <a style="color: white;">Empty</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                        @else
                        @endif
                @endif
            @endif
        @endforeach
    </div>

    <!--Mobile-->
    <div class="row mobileShowEvent" style="text-align: center;">
        @foreach ($events as $event_list)
            @if(new DateTime() > $event_list->start)
                @if(isset($event_list->EventsMedia))
                    @if(count($event_list->EventsMedia->eventsMediaImage) > 0)
                        <div class="col s12 m3">
                            <div class="card white hoverable" style="border-radius: 20px;">
                                <div class="circle">
                                    <div class="card-content black-text" style="height: 100%;">
                                        <div class="user-view" align="center">
                                            <a href="#user">
                                                <img class="fullscreen" style="height: 100px;width: 250px;"
                                                     src="{{isset($event_list->event_image_url)?'/storage/'.$event_list->event_image_url:''}}">
                                            </a>
                                        </div>
                                        <h6 class="">{{$event_list->title}}</h6>
                                        <h6 class="" id="start"
                                            value="{{$event_list->start->toDateString()}}">{{$event_list->start->toDateString()}} </h6>
                                    </div>
                                    <div class="card-action">
                                        @if(count($event_list->EventsMedia->eventsMediaImage) > 0)
                                            <div class="view_media" id="photo"
                                                 style="width:100%; border-radius: 10px; margin-top: 5px;">
                                                <a style="color: orange;"
                                                   href="{{url('/full_calendar/show-gallery/'.$event_list->slug)}}"
                                                   onclick="viewMedia(this)">View Gallery</a>
                                            </div>
                                        @else
                                            <div class="view_media" id="photo"
                                                 style="width:100%; border-radius: 10px; margin-top: 5px;">
                                                <a style="color: white;">Empty</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                    @endif
                @endif
            @endif
        @endforeach
    </div>

    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            $('.modal').modal();

            $('#found-us-via-input').change(function () {
                if ($('#found-us-via-input').val() === 'Other') {
                    $('#found-us-via-other-input').show();
                } else {
                    $('#found-us-via-other-input').hide();
                }
            });
        });


        function viewMedia(obj) {
            let event_id = obj.getAttribute('data-value');

            location.href = '/home/gallery/' + event_id;
        }

        function saveEventDetails(obj) {
            let event_id = obj.getAttribute('data-value');
            let event_type = obj.getAttribute('data-type');
            sessionStorage.setItem('stored_event_id', event_id);
            sessionStorage.setItem('stored_event_type', event_type);
        }
 </script>
@endsection

