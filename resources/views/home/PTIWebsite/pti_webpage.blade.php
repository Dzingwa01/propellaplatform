@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>
    </head>
    <link rel="stylesheet" type="text/css" href="/css/PTIAbout/ptiAbout.css"/>

    <div class="row">
        <div class="parallax-container" id="aboutImageDesktop">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/PTI Team.jpg">
            </div>
            <h3 style="margin-left: 400px;margin-top: 45vh;color: white;text-shadow: 5px 3px 1px #404040; font-size: 4rem;font-family: Arial;">
                <div class="">
                        <span>Propella Township Hub</span><br>
                    <span style="margin-left: 240px">About Us</span><br>
                </div>
            </h3>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="visionDesktop">
        <div>
            <div class="row">
                <div class="col m6">
                    <div class="row">
                        <div class="col m12" style="margin-left: -50px;">
                            <h3 class="standard-header">Expanding Our Footprint</h3>
                            <p class="standard-paragraphs justtext">
                                The Propella Township Incubator offers access to an ICT support platform to anyone with a viable digital
                                technology idea who wants to commercialise their business idea. The Township Incubator will give you access
                                to required resources to transform your ICT and digital business idea into a viable business. Propella Business
                                Incubator and SEDA (Small Enterprise Development Agency) has embarked on a process of establishing the Propella
                                Township Incubator (PTI) in the Neave Industrial Satellite in the Bay. The aim is to ensure that local communities
                                have access to services which will help them to become part of the local and mainstream ICT economy. The Satellite
                                Incubator will offer a range of incubation and business development support services. The incubator will assist start-up and
                                small businesses in mitigating the risk of failure in the early stages of start-up and will provide both physical and virtual
                                incubation support. The aim is increasing the rate of start-up success.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m12" style="margin-left: -50px">
                            <h3 class="standard-header">4-Key Areas Of New Venture Development:</h3>
                            <p class="standard-paragraphs keyareas">
                                <br>
                                &#187;  Entrepreneurial Development
                                <br>
                                <br>
                                &#187;  Commercialization of Technology
                                <br>
                                <br>
                                &#187;  Business Model Development
                                <br>
                                <br>
                                &#187;  Customer Validation
                                <br>
                                <br>
                            </p>
                        </div>
                    </div>
                </div>


                <div class="col m2"></div>
                <div class="col m6">
                    <div class="row center">
                        <div class="row">
                            <div class="standard-blocks col l4 servicesPTI hoverable">
                                <h3 class="standard-block-headers">Services</h3>
                            </div>
                            <div class="standard-blocks col l4 programmePTI hoverable">
                                <h3 class="standard-block-headers">Programmes</h3>
                            </div>
                            <div class="standard-blocks col l4 mentorsPTI hoverable">
                                <h3 class="standard-block-headers">Mentors</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="standard-blocks col l4 applyPTI hoverable">
                                <h3 class="standard-block-headers">Apply</h3>
                            </div>
                            <div class="standard-blocks col l4 contactUs hoverable">
                                <h3 class="standard-block-headers">Contact</h3>
                            </div>
                            <div class="standard-blocks col l4 mediaPTI hoverable">
                                <h3 class="standard-block-headers">Gallery</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="standard-blocks col l4 partnerPTI hoverable">
                                <h3 class="standard-block-headers">Partners</h3>
                            </div>
                            <div class="standard-blocks col l4 venturePTI hoverable">
                                <h3 class="standard-block-headers">Ventures</h3>
                            </div>
                            <div class="standard-blocks col l4 contactUs hoverable">
                                <h3 class="standard-block-headers"></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="pic1"></div>
    <div class="row" id="servicesImageDesktop">
        <div class="parallax-container" id="servImg" style="height: 70vh">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/Doc Presenting.jpg">
            </div>
            <h3 style="margin-left: 400px;margin-top: 45vh;color: white;text-shadow: 5px 3px 1px #404040; font-size: 4rem;font-family: Arial;">
                <div class="">
                    <span>Propella Township Incubator</span><br>
                    <span style="margin-left: 230px">Services</span><br>
                </div>
            </h3>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="servicesDesktop">
        <div class="row left-align">
            <div class="col s4" id="propInfoDesktop">
                <div class="standard-left-buttons pageTop">
                    <h6 class="standard-left-button-headers ">About us</h6>
                </div>
                <div class="standard-left-buttons programme">
                    <h6 class="standard-left-button-headers ">Programmes</h6>
                </div>
                <div class="standard-left-buttons mentors">
                    <h6 class="standard-left-button-headers ">Mentors</h6>
                </div>
                <div class="standard-left-buttons apply">
                    <h6 class="standard-left-button-headers ">Apply</h6>
                </div>
                <div class="standard-left-buttons contact">
                    <h6 class="standard-left-button-headers ">Contact</h6>
                </div>


            </div>
            <div class="col s1"></div>

            <div class="col s7">
                <p><b>PROPELLA SATELLITE  INCUBATOR SERVICES</b></p>
                <p>• It aims to provide a safe and conducive environment for local entrepreneurs and innovators to test and grow their business idea.</p>
                <p>• It provides access to business development support by knowledgeable and experienced expert personnel.</p>
                <p>• Provides an immersive learning experience for an entrepreneur to surround themselves
                        with like-minded collaborators in a fun and creative learning environment.</p>
                <p>• Offers a period of two years (24 months) physical or virtual incubation support.</p>
                <p>• Access to co-working space and a makerspace for design thinking and prototype development.</p>
                <p>• Access to a range of workshops and bootcamps to help you grow your business.</p>
                <p>• Working with experts who will assist you to navigate your enterprise development journey.</p>
            </div>
        </div>
    </div>

{{--    Next Section--}}
    <div id="pic2"></div>
    <div class="row" id="programmeImageDesktop">
        <div class="parallax-container" id="progImg" style="height: 70vh">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/PTI Tour.jpg">
            </div>
            <h3 style="margin-left: 400px;margin-top: 45vh;color: white;text-shadow: 5px 3px 1px #404040; font-size: 4rem;font-family: Arial;">
                <div class="">
                    <span>Propella Township Incubator</span><br>
                    <span style="margin-left: 210px">Programmes</span>
                </div>
            </h3>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="programmeDesktop">
        <div class="row left-align">
            <div class="col s4" id="propInfoDesktop">
                <div class="standard-left-buttons pageTop">
                    <h6 class="standard-left-button-headers ">About us</h6>
                </div>
                <div class="standard-left-buttons services">
                    <h6 class="standard-left-button-headers ">Services</h6>
                </div>
                <div class="standard-left-buttons mentors">
                    <h6 class="standard-left-button-headers ">Mentors</h6>
                </div>
                <div class="standard-left-buttons apply">
                    <h6 class="standard-left-button-headers ">Apply</h6>
                </div>
                <div class="standard-left-buttons contact">
                    <h6 class="standard-left-button-headers ">Contact</h6>
                </div>
            </div>

            {{--Propella Satellite Program details--}}
            <div class="col s1"></div>
            <div class="col s7">
                <p><b>PROPELLA SATELLITE INCUBATOR PROGRAMME</b></p>
                <p>All aspiring tech / ICT entrepreneurs are invited to join the ICT bootcamp at the Propella Satellite Incubator (Neave Industrial Park).
                    All you need is an innovative business idea in the ICT or Tech space. The Bootcamp is designed to help entrepreneurs develop and grow
                    their ideas into potentially viable and profitable businesses.
                <div>
                    <span id="text">The call for ideas (must be innovative and within the ICT/Technology/4IR space).<br>
                Applications are open to all members of the public, but preference will be given to the following:<br>
                • Youth (18 to 35)<br>
                • South African residents<br>
                • Female<br>
                • Disabled<br>
                • Township based<br>
                <br>
                (Hot Tip – Be sure to complete ALL the information requested on the form … the last thing you want is to be disqualified for an incomplete form!)
                    <br>
                    Entries received will be reviewed and the top 30 submissions will be invited, via email, to attend an intensive two-day bootcamp.
                    <br>
                    The bootcamp will provide the entrepreneur with the opportunity to refine and polish their idea by addressing business fundamentals,
                    <br>
                    customer validation and getting them ready to pitch their idea to potential funders.
                    <br>
                    The entrepreneurs will pitch their revised business ideas and we will select the top ideas for the Propella Satellite Incubator cohort.
                    <br>
                    <br>
                This may be the opportunity you have been looking for to turn that big idea into reality!<br>
                For any additional information or queries contact Aphelele at agjonas@propellaincubator.co.za on 041 502 3700.
                    </span>
                </div>
                <div class="btn-container">
                    <button id="toggle">Read More</button>
                </div>
            </div>
            <br>
            {{--            Propella RAP Program details--}}
            <div class="col s1"></div>
            <div class="col s7">
                <p><b>PROPELLA TOWNSHIP INCUBATOR RAP PROGRAMME</b></p>
                <p>The Propella Township Incubator Rapid Accelerator Programme (RAP) is designed to support existing businesses who are
                    struggling to survive and / or grow due to difficult and challenging economic times.
                <div>
                    <span id="text2">The programme aims to assist existing entrepreneurs to either sustain or grow their business by offering a range of identified and required expertise specific to
                    each business’s unique needs.
                    <br>
                    The aim is to take the business to the next level in a short period of time where growth and sustainability
                    can be achieved and measured with the right support and the right team of coaches assigned to assist the business.
                    <br>

                    <br>The Rapid Accelerator Programme (RAP) will run over a 10 to 12-week timeframe and offers selected SMME’s
                        virtual incubation support as well as access to a pool of experts who will provide advisory support
                        and guidance for the entrepreneurs to fast-track their business development and growth.<br><br>
                    It is a fast-paced enterprise development support programme which requires participants to be
                        committed and be willing to action rapid business turnaround or re-engineering or business
                        strategies and solutions which can produce results over a short-period of time.<br>
                    <br><b>To participate in Propella RAP, the following criteria must be met:</b><br>
                    •	Business should be operational for at least 6 months (minimum)<br>
                    •	Must be a formally registered business with CIPC.<br>
                    •	Able to provide proof of turn-over.<br>
                    •	Employ a minimum of 2 staff members.<br>
                    •	Provide proof of revenue-generation and customer base.<br>
                    •	Must be based in the Nelson Mandela Bay Metropole.<br>
                    <br>
                    <b>Benefits of the Participating in Propella RAP:</b><br>
                    •	Help you to grow your business through a structured support programme.<br>
                    •	Fast paced business development and growth programme.<br>
                    •	Provide you with solutions to improve your business processes and reduce operating costs.<br>
                    •	Assist you to increase your customer and revenue base.<br>
                    •	Provide you with expert coaching and mentorship for improved business management.<br>
                    •	Assist you in unlocking new products and services to stimulate product or process diversification.<br>
                    •	Opportunity for you to expand your eco system and stakeholder networks within your sector.<br>
                    •	Offer you support for business turnaround solutions as businesses adapt to a new- norm.<br>
                    •	Be part of innovative and driven ventures seeking knowledge and open to knowledge transfer and sharing.<br>
                    •	Facilitate access to market linkages opportunities.<br>
                    •	Facilitate access and linkage to potential funding partners and investors.<br>
                    •	Access to a Technical Advisor.<br>
                    •	Access to a fulltime Business development Officer.<br>
                    •	Access to Makerspace Product Development Laboratory with available technologies for new product exploration and development.<br>
                    •	Access and support to develop a Minimal Viable Product (MVP for new product discovery and development.<br>

                    <br>
                    If you feel that you meet the criteria as mentioned above, kindly complete the linked RAP Application Form. .
                    </span>
                </div>
                <div class="btn-container">
                    <button id="toggle2">Read More</button>
                </div>
            </div>
        </div>
    </div>


   {{-- Mobile--}}
    <div class="row" id="aboutImageMobile">
        <div class="parallax-container">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/PTI Team.jpg">
            <h5 style="margin-left: 1em;margin-top: 40vh;color: white;text-shadow: 5px 3px 1px #404040;font-style: italic">
                <div class="">
                    <span><b>Propella Township Hub </b></span><br>
                    <span style="margin-left: 4em"><b>About Us</b></span><br>
                </div>
            </h5>

        </div>
    </div>
    <div class="section center-align standard-section-with-margins" id="aboutMobile">
        <div class="col s12" id="propInfoDesktop">
            <div class="standard-left-buttons servicesmob">
                <h6 class="standard-left-button-headers ">Services</h6>
            </div>
            <div class="standard-left-buttons programmemob">
                <h6 class="standard-left-button-headers ">Programme</h6>
            </div>
            <div class="standard-left-buttons mentorsmob">
                <h6 class="standard-left-button-headers ">Mentors</h6>
            </div>
            <div class="standard-left-buttons apply">
                <h6 class="standard-left-button-headers ">Apply</h6>
            </div>
            <div class="standard-left-buttons contact">
                <h6 class="standard-left-button-headers ">Contact</h6>
            </div>
        </div>
        <div class="row ">
            <div class="col s12">
                <p><b>EXPANDING OUR FOOTPRINT </b></p>
                <p class="justtext">The Propella Satellite Incubator offers access to an ICT support platform to anyone with a viable digital technology idea who wants to commercialise
                    their business idea. The Satellite Incubator will give you access to required resources to transform your ICT and digital business idea into a viable
                    business. Propella Business Incubator and SEDA (Small Enterprise Development Agency) has embarked on a process of establishing the Propella Satellite
                    Incubator (PSI) in the Neave Industrial Satellite in the Bay. The aim is to ensure that local communities have access to services which will help them
                    to become part of the local and mainstream ICT economy. The Satellite Incubator will offer a range of incubation and business development support services.
                    The incubator will assist start-up and small businesses in mitigating the risk of failure in the early stages of start-up and will provide both physical
                    and virtual incubation support. The aim is increasing the rate of start-up success.
                </p>
                <br>
                <p><b>4-KEY AREAS OF NEW VENTURE DEVELOPMENT:</b></p>
                <p>&#187; Entrepreneurial Development</p>
                <p>&#187; Commercialization of Technology</p>
                <p>&#187; Business Model Development</p>
                <p>&#187; Customer Validation</p>
            </div>
        </div>
    </div>

    <script>
        $('.pageTop').on('click', function () {
            location.href = '#top';
        });
        $('.services').on('click', function () {
            location.href = '#pic1';
        });
        $('.programme').on('click', function () {
            location.href = '#pic2';
        });
        $('.mentors').on('click', function () {
            location.href = '/Home/Mentors';
        });
        $('.apply').on('click', function () {
            location.href = '/Application-Process/question-category';
        });
        $('.contact').on('click', function () {
            location.href = '/Home/contactUs';
        });

        $('.servicesmob').on('click', function () {
            location.href = '/home/Services';
        });
        $('.programmemob').on('click', function () {
            location.href = '/home/Programme';
        });
        $('.mentorsmob').on('click', function () {
            location.href = 'home/Mentors';
        });

        $('.servicesPTI').on('click', function () {
            location.href = '#pic1';
        });
        $('.programmePTI').on('click', function () {
            location.href = '#pic2';
        });

        $('.mentorsPTI').on('click', function () {
            location.href = '/Home/Mentors';
        });

        $('.applyPTI').on('click', function () {
            location.href = '/Application-Process/question-category';

        });
        //Who we Incubate
        $('.contactUs').on('click', function () {
            location.href = '/Home/contactUs';
        });
        //How to Participate
        $('.mediaPTI').on('click', function () {
            location.href = '/home/show-satellite-gallery';
        });
        $('.partnerPTI').on('click', function () {
            location.href = '/Home/Partners';
        });
        $('.venturePTI').on('click', function () {
            location.href = '/pti-ventures';
        });
        $(document).ready(function () {
            $("#toggle").click(function () {
                var elem = $("#toggle").text();
                if (elem == "Read More") {
                    //when btn is in read more state
                    $("#toggle").text("Read Less");
                    $("#text").slideDown();
                }
                else {
                    //when btn is in read less state
                    $("#toggle").text("Read More");
                    $("#text").slideUp();
                }
            });
        });
        $(document).ready(function () {
            $("#toggle2").click(function () {
                var elem2 = $("#toggle2").text();
                if (elem2 == "Read More") {
                    //when btn is in read more state
                    $("#toggle2").text("Read Less");
                    $("#text2").slideDown();
                }
                else {
                    //when btn is in read less state
                    $("#toggle2").text("Read More");
                    $("#text2").slideUp();
                }
            });
        });

    </script>



    @endsection
