@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>
    </head>
    <link rel="stylesheet" type="text/css" href="/css/PTIAbout/ptiAbout.css"/>

    <div class="row">
        <div class="parallax-container" style="height: 85vh" id="contactImageDesktop">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/PTI reception.jpg">
            </div>
            <h3 style="margin-left: 830px;margin-top: 60vh;color: white;text-shadow: 5px 3px 1px black; font-size: 4rem;font-family: Arial;">
            <div class="">
                    <span>Propella Township Hub</span><br>
                    <span style="margin-left: 400px">Contact</span><br>
                </div>
            </h3>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="contactDesktop">
        <div class="row left-align">
            <div class="col s4" id="propInfoDesktop">
                <div class="standard-left-buttons services">
                    <h6 class="standard-left-button-headers ">About us</h6>
                </div>
                <div class="standard-left-buttons programme">
                    <h6 class="standard-left-button-headers ">Services</h6>
                </div>
                <div class="standard-left-buttons mentors">
                    <h6 class="standard-left-button-headers ">Mentors</h6>
                </div>
                <div class="standard-left-buttons apply">
                    <h6 class="standard-left-button-headers ">Programme</h6>
                </div>
                <div class="standard-left-buttons contact">
                    <h6 class="standard-left-button-headers ">Apply</h6>
                </div>
            </div>
            <div class="col s1"></div>
            <div class="col s7">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3310.7623708198835!2d25.580837315003794!3d-33.92151498064168!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e7ad3efad3085d3%3A0x35fed8daed39e69!2sInsucon%20Pty%20Ltd!5e0!3m2!1sen!2sza!4v1592828905906!5m2!1sen!2sza" width="600" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                <p>Address :  07 Bennett Road, Neave Industrial Park, Korsten, 6020</p>
                <p>Tel: 041 502 3700</p>
                <p>Email: nafeesa@propellaincubator.co.za</p>
            </div>
        </div>
    </div>

    <!--Mobile-->
    <div class="row">
        <div class="parallax-container" id="contactImageMobile">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/PTI reception.jpg">
            </div>
            <h5 style="margin-left: 1em;margin-top: 40vh;color: white;text-shadow: 5px 3px 1px #404040;font-style: italic">
                <div class="">
                    <span><b>Propella Township Hub</b></span><br>
                    <span style="margin-left: 4em"><b>Contact</b></span><br>
                </div>
            </h5>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="contactMobile">
        <div class="col s4" id="propInfoDesktop">
            <div class="standard-left-buttons services">
                <h6 class="standard-left-button-headers ">About us</h6>
            </div>
            <div class="standard-left-buttons programme">
                <h6 class="standard-left-button-headers ">Services</h6>
            </div>
            <div class="standard-left-buttons mentors">
                <h6 class="standard-left-button-headers ">Mentors</h6>
            </div>
            <div class="standard-left-buttons apply">
                <h6 class="standard-left-button-headers ">Programme</h6>
            </div>
            <div class="standard-left-buttons contact">
                <h6 class="standard-left-button-headers ">Apply</h6>
            </div>
        </div>
        <div class="col s7">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3310.7623708198835!2d25.580837315003794!3d-33.92151498064168!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e7ad3efad3085d3%3A0x35fed8daed39e69!2sInsucon%20Pty%20Ltd!5e0!3m2!1sen!2sza!4v1592828905906!5m2!1sen!2sza"
                width="200" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                tabindex="0"></iframe>
            <p>Address : 07 Bennett Road, Neave Industrial Park, Korsten, 6020</p>
            <p>Tel: 041 502 3700</p>
            <p>Email: nafeesa@propellaincubator.co.za</p>
        </div>
    </div>

    <script>
        $('.services').on('click', function () {
            location.href = '/home/AboutUs';
        });
        $('.programme').on('click', function () {
            location.href = '/home/Services';
        });
        $('.apply').on('click', function () {
            location.href = '/home/Programme';
        });
        $('.mentors').on('click', function () {
            location.href = '/home/Mentors';
        });
        $('.contact').on('click', function () {
            location.href = '/home/Apply';
        });
    </script>


@endsection
