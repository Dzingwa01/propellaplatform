@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>
        <style>
            .containerM {
                position: relative;
                width: 20%;
                margin-left: 40px;
            }
            .containerMobile {
                position: relative;
                width: 20%;
                margin-left: 6%;
            }

            .imageM {
                opacity: 1;
                display: block;
                width: auto;
                height: auto;
                transition: .5s ease;
                backface-visibility: hidden;
            }
            .imageMobile {
                opacity: 1;
                display: block;
                width: auto;
                height: auto;
                transition: .5s ease;
                backface-visibility: hidden;
            }

            .middleM {
                transition: .5s ease;
                opacity: 0;
                position: absolute;
                top: 50%;
                left: 200%;
                transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                text-align: center;
            }
            .middleMobile {
                transition: .5s ease;
                opacity: 0;
                position: absolute;
                top: 50%;
                left: 200%;
                transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                text-align: center;
            }

            .containerM:hover .imageM {
                opacity: 0.3;
            }
            .containerMobile:hover .imageMobile {
                opacity: 0.3;
            }

            .containerM:hover .middleM {
                opacity: 1;
                top:30vh;
            }
            .containerMobile:hover .middleMobile {
                opacity: 1;
                top:30vh;
            }

            .textM {
                backdrop-filter: blur(10px);
                color: black;
                font-size: 1em;
                padding: 16px 32px;
                width: 200px;

            }
            .textMobile {
                backdrop-filter: blur(10px);
                color: black;
                font-size: 1em;
                padding: 16px 32px;
                width: 200px;

            }
        </style>
    </head>
    <link rel="stylesheet" type="text/css" href="/css/PTIAbout/ptiAbout.css"/>

    <div class="row" id="mentorsImageDesktop">
        <div class="parallax-container" style="height: 85vh">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/white board.jpg">
            </div>
            <h3 style="margin-left: 830px;margin-top: 60vh;color: white;text-shadow: 5px 3px 1px black; font-size: 4rem;font-family: Arial;">
                <div class="">
                    <span>Propella Township Hub</span><br>
                    <span style="margin-left: 390px">Mentors</span>
                </div>
            </h3>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="mentorsDesktop">
        <div class="row left-align">
            <div class="col s4" id="propInfoDesktop">
                <div class="standard-left-buttons services">
                    <h6 class="standard-left-button-headers ">About us</h6>
                </div>
                <div class="standard-left-buttons programme">
                    <h6 class="standard-left-button-headers ">Services</h6>
                </div>
                <div class="standard-left-buttons mentors">
                    <h6 class="standard-left-button-headers ">Programme</h6>
                </div>
                <div class="standard-left-buttons apply">
                    <h6 class="standard-left-button-headers ">Apply</h6>
                </div>
                <div class="standard-left-buttons contact">
                    <h6 class="standard-left-button-headers ">Contact</h6>
                </div>
            </div>
            <div class="col s1"></div>

            <div class="col s7">
                <div class="mentorsInfo ">
                    <div class="row">
                        <h4 style="text-align: center;color: black; background-color: lightgray;width: 100%">
                            <b>Mentors</b></h4>
                        <div class="col m4">
                            <div class="containerM">
                                <img src="/images/ProfilePictures/PTI Staff/SanjivRanchod.jpg" alt="Avatar" class="imageM"
                                     style="width:400%">
                                <div class="middleM">
                                    <div class="textM">Sanjiv Ranchod</div>
                                    <div class="textM">Mentor</div>
                                </div>
                            </div>
                        </div>
                        <div class="col m4">
                            <div class="containerM">
                                <img src="/images/ProfilePictures/PTI Staff/CarlLiddle.jpg" alt="Avatar" class="imageM"
                                     style="width:400%">
                                <div class="middleM">
                                    <div class="textM">Carl Liddle</div>
                                    <div class="textM">Mentor</div>
                                </div>
                            </div>
                        </div>
                        <div class="col m4">
                            <div class="containerM">
                                <img src="/images/ProfilePictures/PTI Staff/MarvinDraai.jpg" alt="Avatar" class="imageM"
                                     style="width:400%">
                                <div class="middleM">
                                    <div class="textM">Marvin Draai</div>
                                    <div class="textM">Mentor</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>

    <!--Mobile-->
    <div class="row" id="mentorsImageMobile">
        <div class="parallax-container">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/white board.jpg">
            </div>
            <h5 style="margin-left: 1em;margin-top: 40vh;color: white;text-shadow: 5px 3px 1px #404040;font-style: italic">
                <div class="">
                    <span><b>Propella Township Hub</b></span><br>
                    <span style="margin-left: 0em"><b>Mentors</b></span><br>
                </div>
            </h5>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="mentorsMobile">
        <div class="col s4" id="propInfoDesktop" style="margin-right: 5%">
            <div class="standard-left-buttons mentorsmob">
                <h6 class="standard-left-button-headers ">About us</h6>
            </div>
            <div class="standard-left-buttons programmemob">
                <h6 class="standard-left-button-headers ">Programme</h6>
            </div>
            <div class="standard-left-buttons servicesmob">
                <h6 class="standard-left-button-headers ">Services</h6>
            </div>
            <div class="standard-left-buttons apply">
                <h6 class="standard-left-button-headers ">Apply</h6>
            </div>
            <div class="standard-left-buttons contact">
                <h6 class="standard-left-button-headers ">Contact</h6>
            </div>
        </div>
        <div class="col s12">
            <div class="mentorsInfo ">
                <div class="row">
                    <h4 style="text-align: center;color: black; background-color: lightgray;width: 95%" >
                        <b>Mentors</b></h4>
                    <div class="col m4">
                        <div class="containerMobile">
                            <img src="/images/ProfilePictures/PTI Staff/SanjivRanchod.jpg" alt="Avatar" class="imageMobile"
                                 style="width:450%">
                            <div class="middleMobile">
                                <div class="textMobile">Sanjiv Ranchod</div>
                                <div class="textMobile">Mentor</div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="col m4">
                        <div class="containerMobile">
                            <img src="/images/ProfilePictures/PTI Staff/CarlLiddle.jpg" alt="Avatar" class="imageMobile"
                                 style="width:450%">
                            <div class="middleMobile">
                                <div class="textMobile">Carl Liddle</div>
                                <div class="textMobile">Mentor</div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="col m4">
                        <div class="containerMobile">
                            <img src="/images/ProfilePictures/PTI Staff/MarvinDraai.jpg" alt="Avatar" class="imageMobile"
                                 style="width:450%">
                            <div class="middleMobile">
                                <div class="textMobile">Marvin Draai</div>
                                <div class="textMobile">Mentor</div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

    <script>
        $('.servicesmob').on('click', function () {
            location.href = '/home/Services';
        });
        $('.programmemob').on('click', function () {
            location.href = '/home/Programme';
        });
        $('.mentorsmob').on('click', function () {
            location.href = '/home/AboutUs';
        });
        $('.apply').on('click', function () {
            location.href = '/Application-Process/question-category';
        });
        $('.contact').on('click', function () {
            location.href = '/Home/contactUs';
        });
    </script>

@endsection

