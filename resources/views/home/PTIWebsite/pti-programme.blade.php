@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>
        <style>
            #more{
                display: none;
            }
            #more2{
                display: none;
            }
            #moreMobile{
                display: none;
            }
            #moreMobile2{
                display: none;
            }
        </style>
    </head>
    <link rel="stylesheet" type="text/css" href="/css/PTIAbout/ptiAbout.css"/>
{{--Header Image--}}
    <div class="row" id="programmeImageDesktop">
        <div class="parallax-container" style="height: 85vh">
            <div class="parallax">
                <img style="height: 100%" src="/images/PTI Resized/PTI Tour.jpg">
            </div>
            <h3 style="margin-left: 830px;margin-top: 60vh;color: white;text-shadow: 5px 3px 1px black; font-size: 4rem;font-family: Arial;">
            <div class="">
                    <span>Propella Township Hub</span><br>
                    <span style="margin-left: 300px">Programme</span>
                </div>
            </h3>
        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="programmeDesktop">
        <div class="row left-align">
            <div class="col s4" id="propInfoDesktop">
                <div class="standard-left-buttons services">
                    <h6 class="standard-left-button-headers ">About us</h6>
                </div>
                <div class="standard-left-buttons programme">
                    <h6 class="standard-left-button-headers ">Services</h6>
                </div>
                <div class="standard-left-buttons mentors">
                    <h6 class="standard-left-button-headers ">Mentors</h6>
                </div>
                <div class="standard-left-buttons apply">
                    <h6 class="standard-left-button-headers ">Apply</h6>
                </div>
                <div class="standard-left-buttons contact">
                    <h6 class="standard-left-button-headers ">Contact</h6>
                </div>
            </div>

            {{--Propella Satellite Program details--}}
            <div class="col s1"></div>
            <div class="col s7">
                <p><b>PROPELLA TOWNSHIP HUB PROGRAMME</b></p>
                <p>All aspiring tech / ICT entrepreneurs are invited to join the ICT bootcamp at the Propella Township Hub (Neave Industrial Park).
                    All you need is an innovative business idea in the ICT or Tech space. The Bootcamp is designed to help entrepreneurs develop and grow their ideas into potentially viable and profitable businesses.
                    The call for ideas (must be innovative and within the ICT/Technology/4IR space).
                <br>
                    <span id="dots">...</span><span id="more">
                Applications are open to all members of the public, but preference will be given to the following:<br>
                • Youth (18 to 35)<br>
                • South African residents<br>
                • Female<br>
                • Disabled<br>
                • Township based<br>
                <br>
                (Hot Tip – Be sure to complete ALL the information requested on the form … the last thing you want is to be disqualified for an incomplete form!)
                    <br>
                    Entries received will be reviewed and the top 30 submissions will be invited, via email, to attend an intensive two-day bootcamp.
                    <br>
                    The bootcamp will provide the entrepreneur with the opportunity to refine and polish their idea by addressing business fundamentals,
                    <br>
                    customer validation and getting them ready to pitch their idea to potential funders.
                    <br>
                    The entrepreneurs will pitch their revised business ideas and we will select the top ideas for the Propella Township Hub cohort.
                    <br>
                    <br>
                This may be the opportunity you have been looking for to turn that big idea into reality!<br>
                        For any additional information or queries contact Aphelele at agjonas@propellaincubator.co.za on 041 502 3700.</span>
                </p>
                <button onclick="read_more_less()" class="btn btn-success" id="mybtn">Read more</button>
            </div>

            {{--Propella RAP Program details--}}
            <div class="col s1"></div>
            <div class="col s7">
                <p><b>PROPELLA TOWNSHIP INCUBATOR RAP PROGRAMME</b></p>
                <p>The Propella Township Incubator Rapid Accelerator Programme (RAP) is designed to support existing businesses who are
                    struggling to survive and / or grow due to difficult and challenging economic times.
                    The programme aims to assist existing entrepreneurs to either sustain or grow their business by
                    offering a range of identified and required expertise specific to each business’s unique needs.
                    The aim is to take the business to the next level in a short period of time where growth and sustainability
                    can be achieved and measured with the right support and the right team of coaches assigned to assist the business.
                    <br>
                    <span id="dots2">...</span><span id="more2">
                    <br>The Rapid Accelerator Programme (RAP) will run over a 10 to 12-week timeframe and offers selected SMME’s
                        virtual incubation support as well as access to a pool of experts who will provide advisory support
                        and guidance for the entrepreneurs to fast-track their business development and growth.<br><br>
                    It is a fast-paced enterprise development support programme which requires participants to be
                        committed and be willing to action rapid business turnaround or re-engineering or business
                        strategies and solutions which can produce results over a short-period of time.<br>
                    <br><b>To participate in Propella RAP, the following criteria must be met:</b><br>
                    •	Business should be operational for at least 6 months (minimum)<br>
                    •	Must be a formally registered business with CIPC.<br>
                    •	Able to provide proof of turn-over.<br>
                    •	Employ a minimum of 2 staff members.<br>
                    •	Provide proof of revenue-generation and customer base.<br>
                    •	Must be based in the Nelson Mandela Bay Metropole.<br>
                    <br>
                    <b>Benefits of the Participating in Propella RAP:</b><br>
                    •	Help you to grow your business through a structured support programme.<br>
                    •	Fast paced business development and growth programme.<br>
                    •	Provide you with solutions to improve your business processes and reduce operating costs.<br>
                    •	Assist you to increase your customer and revenue base.<br>
                    •	Provide you with expert coaching and mentorship for improved business management.<br>
                    •	Assist you in unlocking new products and services to stimulate product or process diversification.<br>
                    •	Opportunity for you to expand your eco system and stakeholder networks within your sector.<br>
                    •	Offer you support for business turnaround solutions as businesses adapt to a new- norm.<br>
                    •	Be part of innovative and driven ventures seeking knowledge and open to knowledge transfer and sharing.<br>
                    •	Facilitate access to market linkages opportunities.<br>
                    •	Facilitate access and linkage to potential funding partners and investors.<br>
                    •	Access to a Technical Advisor.<br>
                    •	Access to a fulltime Business development Officer.<br>
                    •	Access to Makerspace Product Development Laboratory with available technologies for new product exploration and development.<br>
                    •	Access and support to develop a Minimal Viable Product (MVP for new product discovery and development.<br>

                    <br>
                    If you feel that you meet the criteria as mentioned above, kindly complete the linked RAP Application Form. .</span>
                </p>

                <button onclick="read_more_less2()" class="btn btn-success" id="mybtn2">Read more</button>
            </div>
        </div>
    </div>

    <!--Mobile-->
    <div class="row" id="programmeImageMobile">
        <div class="parallax-container">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/PTI Tour.jpg">
            </div>
            <h5 style="margin-left: 1em;margin-top: 40vh;color: white;text-shadow: 5px 3px 1px #404040;font-style: italic">
                <div class="">
                    <span><b>Propella Township Hub</b></span><br>
                    <span style="margin-left: 4em"><b>Programme</b></span><br>
                </div>
            </h5>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="programmeMobile">
        <div class="col s4" id="propInfoDesktop">
            <div class="standard-left-buttons programmemob">
                <h6 class="standard-left-button-headers ">About us</h6>
            </div>
            <div class="standard-left-buttons servicesmob">
                <h6 class="standard-left-button-headers ">Services</h6>
            </div>
            <div class="standard-left-buttons mentorsmob">
                <h6 class="standard-left-button-headers ">Mentors</h6>
            </div>
            <div class="standard-left-buttons apply">
                <h6 class="standard-left-button-headers ">Apply</h6>
            </div>
            <div class="standard-left-buttons contact">
                <h6 class="standard-left-button-headers ">Contact</h6>
            </div>
        </div>
        <div class="col s12">
            <p><b>PROPELLA SATELLITE INCUBATOR PROGRAMME</b></p>
            <p>All aspiring tech / ICT entrepreneurs are invited to join the ICT bootcamp at the Propella Township Hub (Neave Industrial Park).
                All you need is an innovative business idea in the ICT or Tech space. <span id="dotsMobile">...</span><span id="moreMobile">
                    The Bootcamp is designed to help entrepreneurs develop and grow their ideas into potentially viable and profitable businesses.
                The call for ideas (must be innovative and within the ICT/Technology/4IR space).
                <br>

                Applications are open to all members of the public, but preference will be given to the following:<br>
                • Youth (18 to 35)<br>
                • South African residents<br>
                • Female<br>
                • Disabled<br>
                • Township based<br>
                <br>
                (Hot Tip – Be sure to complete ALL the information requested on the form … the last thing you want is to be disqualified for an incomplete form!)
                    <br>
                    Entries received will be reviewed and the top 30 submissions will be invited, via email, to attend an intensive two-day bootcamp.
                    <br>
                    The bootcamp will provide the entrepreneur with the opportunity to refine and polish their idea by addressing business fundamentals,
                    <br>
                    customer validation and getting them ready to pitch their idea to potential funders.
                    <br>
                    The entrepreneurs will pitch their revised business ideas and we will select the top ideas for the Propella Township Hub cohort.
                    <br>
                    <br>
                This may be the opportunity you have been looking for to turn that big idea into reality!<br>
                        For any additional information or queries contact Aphelele at agjonas@propellaincubator.co.za on 041 502 3700.</span>
            </p>
            <button onclick="read_more_less_mobile()" class="btn btn-success" id="mybtnMobile">Read more</button>
        </div>
        <br>
{{--RAP Program Details--}}
        <p><b>PROPELLA TOWNSHIP INCUBATOR RAP PROGRAMME</b></p>
        <p>The Propella Township Incubator Rapid Accelerator Programme (RAP) is designed to support existing businesses who are
            struggling to survive and / or grow due to difficult and challenging economic times.
            The programme aims to assist existing entrepreneurs to either sustain or grow their business by
            offering a range of identified and required expertise specific to each business’s unique needs.
            <span id="dotsMobile2">...</span><span id="moreMobile2">
            The aim is to take the business to the next level in a short period of time where growth and sustainability
            can be achieved and measured with the right support and the right team of coaches assigned to assist the business.
            <br>
                    <br>The Rapid Accelerator Programme (RAP) will run over a 10 to 12-week timeframe and offers selected SMME’s
                        virtual incubation support as well as access to a pool of experts who will provide advisory support
                        and guidance for the entrepreneurs to fast-track their business development and growth.<br><br>
                    It is a fast-paced enterprise development support programme which requires participants to be
                        committed and be willing to action rapid business turnaround or re-engineering or business
                        strategies and solutions which can produce results over a short-period of time.<br>
                    <br><b>To participate in Propella RAP, the following criteria must be met:</b><br>
                    •	Business should be operational for at least 6 months (minimum)<br>
                    •	Must be a formally registered business with CIPC.<br>
                    •	Able to provide proof of turn-over.<br>
                    •	Employ a minimum of 2 staff members.<br>
                    •	Provide proof of revenue-generation and customer base.<br>
                    •	Must be based in the Nelson Mandela Bay Metropole.<br>
                    <br>
                    <b>Benefits of the Participating in Propella RAP:</b><br>
                    •	Help you to grow your business through a structured support programme.<br>
                    •	Fast paced business development and growth programme.<br>
                    •	Provide you with solutions to improve your business processes and reduce operating costs.<br>
                    •	Assist you to increase your customer and revenue base.<br>
                    •	Provide you with expert coaching and mentorship for improved business management.<br>
                    •	Assist you in unlocking new products and services to stimulate product or process diversification.<br>
                    •	Opportunity for you to expand your eco system and stakeholder networks within your sector.<br>
                    •	Offer you support for business turnaround solutions as businesses adapt to a new- norm.<br>
                    •	Be part of innovative and driven ventures seeking knowledge and open to knowledge transfer and sharing.<br>
                    •	Facilitate access to market linkages opportunities.<br>
                    •	Facilitate access and linkage to potential funding partners and investors.<br>
                    •	Access to a Technical Advisor.<br>
                    •	Access to a fulltime Business development Officer.<br>
                    •	Access to Makerspace Product Development Laboratory with available technologies for new product exploration and development.<br>
                    •	Access and support to develop a Minimal Viable Product (MVP for new product discovery and development.<br>

                    <br>
                    If you feel that you meet the criteria as mentioned above, kindly complete the linked RAP Application Form. .</span>
        </p>

        <button onclick="read_more_less_mobile2()" class="btn btn-success" id="mybtnMobile2">Read more</button>
    </div>

    <script>
        $('.servicesmob').on('click', function () {
            location.href = '/home/Services';
        });
        $('.programmemob').on('click', function () {
            location.href = '/home/AboutUs';
        });
        $('.mentorsmob').on('click', function () {
            location.href = '/home/Mentors';
        });
        $('.apply').on('click', function () {
            location.href = '/Application-Process/question-category';
        });
        $('.contact').on('click', function () {
            location.href = '/Home/contactUs';
        });
        //functions to extend and retract paragraph using "Read More/Less" button
        function read_more_less(){
            var dots = document.getElementById('dots');
            var moretext = document.getElementById('more');
            var mybtn = document.getElementById('mybtn');

            if(dots.style.display === 'none'){
                dots.style.display = 'inline';
                moretext.style.display = 'none';
                mybtn.innerHTML = "Read more";
            }
            else{
                dots.style.display='none';
                moretext.style.display = 'inline';
                mybtn.innerHTML = "Read less";
            }
        }
        function read_more_less2(){
            var dots2 = document.getElementById('dots2');
            var moretext2 = document.getElementById('more2');
            var mybtn2 = document.getElementById('mybtn2');

            if(dots2.style.display === 'none'){
                dots2.style.display = 'inline';
                moretext2.style.display = 'none';
                mybtn2.innerHTML = "Read more";
            }
            else{
                dots2.style.display='none';
                moretext2.style.display = 'inline';
                mybtn2.innerHTML = "Read less";
            }
        }
        function read_more_less_mobile(){
            var dotsMobile = document.getElementById('dotsMobile');
            var moretextMobile = document.getElementById('moreMobile');
            var mybtnMobile = document.getElementById('mybtnMobile');

            if(dotsMobile.style.display === 'none'){
                dotsMobile.style.display = 'inline';
                moretextMobile.style.display = 'none';
                mybtnMobile.innerHTML = "Read more";
            }
            else{
                dotsMobile.style.display='none';
                moretextMobile.style.display = 'inline';
                mybtnMobile.innerHTML = "Read less";
            }
        }
        function read_more_less_mobile2(){
            var dotsMobile2 = document.getElementById('dotsMobile2');
            var moretextMobile2 = document.getElementById('moreMobile2');
            var mybtnMobile2 = document.getElementById('mybtnMobile2');

            if(dotsMobile2.style.display === 'none'){
                dotsMobile2.style.display = 'inline';
                moretextMobile2.style.display = 'none';
                mybtnMobile2.innerHTML = "Read more";
            }
            else{
                dotsMobile2.style.display='none';
                moretextMobile2.style.display = 'inline';
                mybtnMobile2.innerHTML = "Read less";
            }
        }

    </script>

@endsection

