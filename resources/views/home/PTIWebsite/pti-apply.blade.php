@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/PTIAbout/ptiAbout.css"/>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>
    </head>
    <div class="row">
        <div class="parallax-container" style="height: 85vh" id="applyImageDesktop">
            <div class="parallax">
                <img  style="height: 100%;" src="/images/PTI Resized/PTI Class.jpg">
            </div>
            <h3 style="margin-left: 830px;margin-top: 60vh;color: white;text-shadow: 5px 3px 1px black; font-size: 4rem;font-family: Arial;">
            <div class="">
                    <span>Propella Township Hub</span><br>
                    <span style="margin-left: 450px">Apply</span><br>
                </div>
            </h3>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="applyDesktop">
        <div class="row left-align">
            <div class="col s4" id="propInfoDesktop">
                <div class="standard-left-buttons services">
                    <h6 class="standard-left-button-headers ">About us</h6>
                </div>
                <div class="standard-left-buttons programme">
                    <h6 class="standard-left-button-headers ">Services</h6>
                </div>
                <div class="standard-left-buttons mentors">
                    <h6 class="standard-left-button-headers ">Mentors</h6>
                </div>
                <div class="standard-left-buttons apply">
                    <h6 class="standard-left-button-headers ">Programme</h6>
                </div>
                <div class="standard-left-buttons contact">
                    <h6 class="standard-left-button-headers ">Contact</h6>
                </div>
            </div>
            <div class="col s1"></div>
            <div class="col s7" style="bottom: 0">
                @foreach($category as $categories)
                    <div class="col l4 m4 s4">
                        <p style="margin-left: 4em"><b>Apply here:</b></p>
                        <div class="" style="margin-top: 10vh;width: 400px;margin-left: 4em" id="{{$categories->id}}" onclick="selectCategory(this)">
                            <div class="card-content">
                                <a href="#user">
                                    <img class="fullscreen z-depth-4" style="height: 300px;width: 400px;border:3px solid white;border-radius: 10px"
                                         src="{{isset($categories->category_image_url)?'/storage/'.$categories->category_image_url:''}}">
                                </a>
                            </div>
                            <p style="margin-left: 12em"><b>{{$categories->category_description}}</b></p>
                            <div class="card-action">
                                <a class="txt" id="{{$categories->id}}" onclick="selectCategory(this)"
                                   style="color: green;margin-left: 6em;cursor: pointer"><b>{{$categories->category_name}}</b></a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                @endforeach
            </div>
        </div>
    </div>

   {{-- Mobile--}}
    <div class="row">
        <div class="parallax-container" id="applyImageMobile">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/PTI Class.jpg"
            </div>
            <h5 style="margin-left: 1em;margin-top: 40vh;color: white;text-shadow: 5px 3px 1px #404040;font-style: italic">
                <div class="">
                    <span><b>Propella Township Hub</b></span><br>
                    <span style="margin-left: 4em"><b>Apply</b></span><br>
                </div>
            </h5>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="applyMobile">
        <div class="col s4" id="propInfoDesktop">
            <div class="standard-left-buttons services">
                <h6 class="standard-left-button-headers ">About us</h6>
            </div>
            <div class="standard-left-buttons programme">
                <h6 class="standard-left-button-headers ">Programme</h6>
            </div>
            <div class="standard-left-buttons services">
                <h6 class="standard-left-button-headers ">Services</h6>
            </div>
            <div class="standard-left-buttons mentors">
                <h6 class="standard-left-button-headers ">Mentors</h6>
            </div>
            <div class="standard-left-buttons contact">
                <h6 class="standard-left-button-headers ">Contact</h6>
            </div>
        </div>
        <div class="col s7" style="bottom: 0">
            @foreach($category as $categories)
                <div class="">
                    <p style="margin-left: 1em"><b>Apply here:</b></p>
                    <div class="" style="margin-top: 10vh;width: 300px;" id="{{$categories->id}}"
                         onclick="selectCategory(this)">
                        <div class="card-content">
                            <a href="#user">
                                <img class="fullscreen z-depth-4"
                                     style="height: 300px;width: 300px;border:3px solid white;border-radius: 10px"
                                     src="{{isset($categories->category_image_url)?'/storage/'.$categories->category_image_url:''}}">
                            </a>
                        </div>
                        <div class="card-action">
                            <a class="txt" id="{{$categories->id}}" onclick="selectCategory(this)"
                               style="color: green;cursor: pointer"><b>{{$categories->category_name}}</b></a>
                        </div>
                    </div>
                </div>
                <br>
                <br>
            @endforeach
        </div>
    </div>

    <style>
        .txt:hover {
            text-decoration: underline;
        }
    </style>



    <script>
        $('.services').on('click', function () {
            location.href = '/home/AboutUs';
        });
        $('.programme').on('click', function () {
            location.href = '/home/Services';
        });
        $('.apply').on('click', function () {
            location.href = '/home/Programme';
        });
        $('.mentors').on('click', function () {
            location.href = '/home/Mentors';
        });
        $('.contact').on('click', function () {
            location.href = '/home/Contact';
        });
        function selectCategory(obj) {
            sessionStorage.setItem('category_id', obj.id);
            window.location = '/Application-Process/Basic-info';
        }
    </script>

@endsection

