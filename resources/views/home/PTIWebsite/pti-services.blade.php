@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>
    </head>
    <link rel="stylesheet" type="text/css" href="/css/PTIAbout/ptiAbout.css"/>

    <div class="row" id="servicesImageDesktop">
        <div class="parallax-container" style="height: 85vh">
            <div class="parallax">
                <img style="height: 70%" src="/images/PTI Resized/Doc Presenting.jpg">
            </div>
            <h3 style="margin-left: 830px;margin-top: 60vh;color: white;text-shadow: 5px 3px 1px black; font-size: 4rem;font-family: Arial;">
                <div class="">
                    <span>Propella Township Hub</span><br>
                    <span style="margin-left: 390px">Services</span><br>
                </div>
            </h3>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="servicesDesktop">
        <div class="row left-align">
            <div class="col s4" id="propInfoDesktop">
                <div class="standard-left-buttons services">
                    <h6 class="standard-left-button-headers ">About us</h6>
                </div>
                <div class="standard-left-buttons programme">
                    <h6 class="standard-left-button-headers ">Programme</h6>
                </div>
                <div class="standard-left-buttons mentors">
                    <h6 class="standard-left-button-headers ">Mentors</h6>
                </div>
                <div class="standard-left-buttons apply">
                    <h6 class="standard-left-button-headers ">Apply</h6>
                </div>
                <div class="standard-left-buttons contact">
                    <h6 class="standard-left-button-headers ">Contact</h6>
                </div>
            </div>
            <div class="col s1"></div>
            <div class="col s7">
                <p><b>PROPELLA TOWNSHIP HUB SERVICES</b></p>
                <p>• It aims to provide a safe and conducive environment for local entrepreneurs and innovators to test and grow their business idea.</p>
                <p>• It provides access to business development support by knowledgeable and experienced expert personnel.</p>
                <p> • Provides an immersive learning experience for an entrepreneur to surround themselves
                    with like-minded collaborators in a fun and creative learning environment.</p>
                <p>• Offers a period of two years (24 months) physical or virtual incubation support.</p>
                <p>• Access to co-working space and a makerspace for design thinking and prototype development.</p>
                <p>• Access to a range of workshops and bootcamps to help you grow your business.</p>
                <p>• Working with experts who will assist you to navigate your enterprise development journey.</p>
            </div>
        </div>
    </div>

   {{-- Mobile--}}
    <div class="row" id="servicesImageMobile">
        <div class="parallax-container">
            <div class="parallax">
                <img style="height: 100%;" src="/images/PTI Resized/Doc Presenting.jpg">
            </div>
            <h5 style="margin-left: 1em;margin-top: 40vh;color: white;text-shadow: 5px 3px 1px #404040;font-style: italic">
                <div class="">
                    <span><b>Propella Township Hub</b></span><br>
                    <span style="margin-left: 4em"><b>Services</b></span><br>
                </div>
            </h5>

        </div>
    </div>

    <div class="section center-align standard-section-with-margins" id="servicesMobile">
        <div class="col s4" id="propInfoDesktop">
            <div class="standard-left-buttons servicesmob">
                <h6 class="standard-left-button-headers ">About us</h6>
            </div>
            <div class="standard-left-buttons programmemob">
                <h6 class="standard-left-button-headers ">Programme</h6>
            </div>
            <div class="standard-left-buttons mentorsmob">
                <h6 class="standard-left-button-headers ">Mentors</h6>
            </div>
            <div class="standard-left-buttons apply">
                <h6 class="standard-left-button-headers ">Apply</h6>
            </div>
            <div class="standard-left-buttons contact">
                <h6 class="standard-left-button-headers ">Contact</h6>
            </div>
        </div>
        <div class="col s7">
            <p><b>PROPELLA TOWNSHIP HUB SERVICES</b></p>
            <p>• It aims to provide a safe and conducive environment for local entrepreneurs and innovators to test
                and growth their business idea.</p>
            <p>• It provides access to business development support by knowledgeable and experienced expert
                personnel.</p>
            <p> • Provides an immersive learning experience for an entrepreneur to surround themselves with
                like-minded collaborators in a fun and creative learning environment.</p>
            <p>• Offers a period of two years (24 months) physical or virtual incubation support.</p>
            <p>• Access to co-working space and a makerspace for design thinking and prototype development.</p>
            <p>• Access to a range of workshops and bootcamps to help you grow your business.</p>
            <p>• Working with experts who will assist you to navigate your enterprise development journey.</p>
        </div>
    </div>

    <script>
        $('.servicesmob').on('click', function () {
            location.href = '/home/AboutUs';
        });
        $('.programmemob').on('click', function () {
            location.href = '/home/Programme';
        });
        $('.mentorsmob').on('click', function () {
            location.href = '/home/Mentors';
        });
        $('.apply').on('click', function () {
            location.href = '/Application-Process/question-category';
        });
        $('.contact').on('click', function () {
            location.href = '/Home/contactUs';
        });
    </script>

@endsection
