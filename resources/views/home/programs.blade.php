@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>
<link rel="stylesheet" type="text/css" href="/css/Programs/main.css" />

    <body>

<div class="section topimage  container-fluid">
    <div class="row">
        <div class="parallax-container" id="ProgrammesTopSection">
            <div class="parallax">
                <img src="/images/Programmes Images/2.3 Programmes.jpg" id="topimage">
            </div>
            <h2 class="programs-section-headers">Programmes</h2>
        </div>
    </div>
</div>
<br />
<!--- Stage0 Mobile-->
<div class="row center-Align" id="ButtonStage0">
    <div class="">
        <h5 class="standard-block-headers hoverable" id="Zero " style="color:#ffffff;font-size:large;text-align:center">Stage 0</h5>
    </div>
</div>
<!---Infor on Stage0 Mobile-->
<div class="row" id="Stage0InfoMob">
    <div class="card white darken-1 hoverable">
        <div class="card-content black-text">
            <p>
                This is the recruitment stage, to find out more about where we are involved in our community, click through to our diary
            </p>
        </div>
    </div>
</div>
<!---Infor on Stage0 Desktop-->
<div class="standard-section-with-margins">
    <div class="row" id="Stage0RowDesk">
        <div class="col l4">
            <div class="row">
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND01.JPG" style="width: 100%; height: 100%;" />
                </div>
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND02.JPG" style="width: 100%; height: 100%;" />
                </div>
            </div>
            <div class="row">
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND03.JPG" style="width: 100%; height: 100%" />
                </div>
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND04.JPG" style="width: 100%; height: 100%" />
                </div>
            </div>
        </div>
        <div class="col 10"></div>

        <div id="Zero"></div>
        <div class="RowMargins">
            <div class="col s12 m8">
                <div class="card white darken-1 hoverable">
                    <div class="card-content black-text">
                        <h4>STAGE 0</h4>
                        <p>
                            This is the recruitment stage, to find out more about where we are involved in our community, click through to our diary

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!--Programmes Third Image Mobile -->
<div class="section thirdimage standard-top-section " id="ProgramsThirdImage">

</div>
<!--Programmes Third Image Desktop -->
<div class="section thirdimage standard-top-section " id="ProgramsThirdImageDesk">
    <div class="row">
        <div class="parallax">

        </div>
    </div>
</div>
<br />
<!--- Stage1 mobile-->
<div class="row center-align" id="ButtonStage1">
    <div class=" ">
        <h5 class="standard-block-headers hoverable" id="One" style="color:#ffffff;font-size:large;text-align:center" ;>Stage 1</h5>
    </div>
</div>

<div class="row" id="Stage1InforMob">
    <div class="card white darken-1 hoverable">
        <div class="card-content black-text">
            <p>
                Is referred to as Ideation. This will typically be where the entrepreneur merely has a business idea.  When the entrepreneurs apply to the programme, the idea will not have been validated and the product will not have been developed.  The purpose of Stage 1 is to find the right jockey and to validate his/her business idea.

            </p>
        </div>
    </div>
</div>
<!--- Stage1 mobile more information on stage 1-->
<div class="section standard-section-with-margins" id="Stage1Information">
    <div class="row">
        <p class="standard-paragraphs">

            The innovator is taken on an 8-week journey that covers 4 very specific areas of business development.   Time commitment is 1 day per week where innovators attend a series of workshops and one-on-one interventions.  Some of the topics include:
        </p>
        <p class="standard-paragraphs">
            <i class="tiny material-icons">chevron_right</i>Design Thinking
        </p>
        <p class="standard-paragraphs">
            Value Proposition
        </p>
        <p class="standard-paragraphs">
            SWOT
        </p>
        <p class="standard-paragraphs">
            Customer Validation
        </p>
        <p class="standard-paragraphs">
            Revenue Streams
        </p>
        <p class="standard-paragraphs">

            Pitch Preparation
        </p>


    </div>
</div>

<!--- Stage1 Desktop-->
<div class="standard-section-with-margins">
    <div class="row" id="Stage1RowDesk">
        <div class="col l4">
            <div class="row">
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND01.JPG" style="width: 100%; height: 100%;" />
                </div>
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND02.JPG" style="width: 100%; height: 100%;" />
                </div>
            </div>
            <div class="row">
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND03.JPG" style="width: 100%; height: 100%" />
                </div>
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND04.JPG" style="width: 100%; height: 100%" />
                </div>
            </div>

        </div>
        <div class="col 8"></div>
        <div id="One"> </div>
        <div class="RowMargins">
            <div class="col s12 m8" id="Stage1InfoDesk">
                <div class="card white darken-1 hoverable">
                    <div class="card-content black-text">
                        <h4>STAGE 1</h4>
                        <p>
                            is referred to as Ideation. This will typically be where the entrepreneur merely has a business idea.  When the entrepreneurs apply to the programme, the idea will not have been validated and the product will not have been developed.  The purpose of Stage 1 is to find the right jockey and to validate his/her business idea.

                        </p>
                    </div>
                </div>
                <p class="standard-paragraphs">
                    The innovator is taken on an 8-week journey that covers 4 very specific areas of business development.Time commitment is 1 day per week where innovators attend a series of workshops and one-on-one interventions.  Some of the topics include:
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Design thinking

                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Value Proposition
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>SWOT
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Customer Validation
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Revenue Streams
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Pitch Preparation
                </p>
            </div>
        </div>
    </div>
    <!--- Stage1 Desktop more Informatio on it-->
    <div class="section standard-section-with-margins" id="Stage1MoreInfo">
        <div class="row">


        </div>
    </div>
</div>

<!--Programmes Fourth Image Mobile -->
<div class="section fourthimage standard-top-section " id="ProgramsFourthImage">
</div>
<br />
<!--Programmes Fourth Image Desktop -->
<div class="section fourthimage standard-top-section " id="ProgramsFourthImageDesk">
</div>

<!--Stage2 Button  mobile-->

<div class="row center-Align" id="ButtonStage2">
    <div class="">
        <h5 class="standard-block-headers" id="Two" style="color:#ffffff;font-size:large;text-align:center;">Stage 2</h5>
    </div>
</div>
<!--Stage2 more infor Button  mobile-->
<div class="row" id="Stage2InforMobile">
    <div class="card white darken-1 hoverable">
        <div class="card-content black-text">
            <p>
                is referred to as Incubation.  At this stage, the entrepreneur will have completed the ideation process or entered the programme with market research that proves a well-defined business problem and solution and (or) product proof of concept in development and (or) pre-revenue stage.  The purpose of Stage 2 is to validate the business, proving it to be a viable opportunity.
            </p>
        </div>
    </div>
</div>
<!--Stage2 more infor Button  mobile-->
<div class="section standard-section-with-margins" id="Stage2Information">
    <div class="row">
        <p class="standard-paragraphs">
            Stage 2 is slightly more intensive and spans over a 12-week period.  Time commitment is still 1 day per week.  More in-depth work is done on the idea converting it into a validated wire frame.  Some of the topics covered are:
        </p>
        <p class="standard-paragraphs">
            Marketing
        <p>
        <p class="standard-paragraphs">
            Online presence
        </p>
        <p class="standard-paragraphs">
            Business compliance
        </p>
        <p class="standard-paragraphs">
            Revenue Canvas
        </p>
        <p class="standard-paragraphs">
            Customer Validation
        </p>
        <p class="standard-paragraphs">
            Wireframing
        </p>
        <p class="standard-paragraphs">
            Innovator development

        </p>
    </div>
</div>

<!--Stage2 button Desktop-->
<div class="standard-section-with-margins">
    <div class="row" id="Stage2Row">
        <div class="col l4">
            <div class="row">
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND01.JPG" style="width: 100%; height: 100%;" />
                </div>
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND02.JPG" style="width: 100%; height: 100%;" />
                </div>
            </div>
            <div class="row">
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND03.JPG" style="width: 100%; height: 100%" />
                </div>
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND04.JPG" style="width: 100%; height: 100%" />
                </div>
            </div>
        </div>
        <div id="Two"> </div>

        <div class="RowMargins">

            <div class="col s12 m8">
                <div class="card white darken-1 hoverable">
                    <div class="card-content black-text">
                        <h4>STAGE 2</h4>
                        <p>
                            is referred to as Incubation.  At this stage, the entrepreneur will have completed the ideation process or entered the programme with market research that proves a well-defined business problem and solution and (or) product proof of concept in development and (or) pre-revenue stage.  The purpose of Stage 2 is to validate the business, proving it to be a viable opportunity.
                        </p>
                    </div>
                </div>
                <p class="standard-paragraphs">

                    Stage 2 is slightly more intensive and spans over a 12-week period.  Time commitment is still 1 day per week.  More in-depth work is done on the idea converting it into a validated wire frame.  Some of the topics covered are:
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Marketing
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Online presence
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i> Business compliance
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Revenue Canvas
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Customer Validation
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Wireframing
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i>Innovator development

                </p>
            </div>
        </div>
    </div>
</div>
{{--@*@<a style="margin-left:50%" class="black-text" href="#AboutSectionContent"><i class="material-icons large scroll-icon">arrow_drop_up_circle</i></a>*@--}}


<!--Stage2 More Information  Desktop-->
<div class="section standard-section-with-margins" id="Stage2InformationDesk">
    <div class="row">

    </div>
</div>

<!--Programmes last Image Mobile -->
<div class="section lastimage standard-top-section " id="ProgramsLastImage">
</div>
<br />
<!--Programmes last Image Desktop-->
<div class="section lastimage standard-top-section " id="ProgramsLastImageDesk">
    <div class="row">
        <div class="parallax-container" id="ProgrammesSecondSection">
            <div class="parallax">
                <img src="~/images/Programmes Images/One.jpg" id="thirdmage" />
            </div>
        </div>
    </div>
</div>

<!--Stage 3 Mobile-->
<div class="row center-Align" id="ButtonStage3">
    <div class="">
        <h5 class="standard-block-headers" id="Three" style="color:#ffffff;font-size:large;text-align:center;">Stage 3</h5>
    </div>
</div>
<!--Stage 3 Mobile card Information -->
<div class="row" id="Stage3InforMob">
    <div class="card white darken-1 hoverable">
        <div class="card-content black-text">
            <p>
                The purpose of Stage 3 is to ensure the product and business foundations are solid and that possible seed investment can flow.  The end of the acceleration journey is marked by the opportunity to apply for further commercialisation support.  Stage 3 business and product development activities will 32 weeks.  Co-working space will be made available for a maximum of 12 months.
            </p>
        </div>
    </div>

</div>
<!--Stage 3 Mobile more  Information on it -->
<div class="section standard-section-with-margins" id="Stage3InformationMob">
    <div class="row">
        <p class="standard-paragraphs">

            Stage 3 takes place over 8 months where the innovator is given access to a Propella advisor and business mentor.  Sessions are bespoke with one-on-one interactions twice monthly.  These are scheduled to suit the innovators time frames.  Here there is a focus on:
        </p>
        <p class="standard-paragraphs">
            <i class="tiny material-icons">chevron_right</i>The business
        </p>
        <p class="standard-paragraphs">
            <i class="tiny material-icons">chevron_right</i>The entrepreneur
        </p>
        <p class="standard-paragraphs">
            <i class="tiny material-icons">chevron_right</i>The tech

        </p>
    </div>
</div>

<!--Stage 3 Desktop-->
<div class="standard-section-with-margins">
    <div class="row" id="Stage3Row">
        <div class="col l4">
            <div class="row">
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND01.JPG" style="width: 100%; height: 100%;" />
                </div>
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND02.JPG" style="width: 100%; height: 100%;" />
                </div>
            </div>
            <div class="row">
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND03.JPG" style="width: 100%; height: 100%" />
                </div>
                <div class="col l6">
                    <img src="/images/Innovators/Industrial/IND04.JPG" style="width: 100%; height: 100%" />
                </div>
            </div>
        </div>
        <div id="Three"></div>
        <br />
        <div class="RowMargins">

            <div class="col s12 m8">
                <div class="card white darken-1 hoverable">
                    <div class="card-content black-text">
                        <h4>STAGE 3</h4>
                        <p>
                            The purpose of Stage 3 is to ensure the product and business foundations are solid and that possible seed investment can flow.  The end of the acceleration journey is marked by the opportunity to apply for further commercialisation support.  Stage 3 business and product development activities will 32 weeks.  Co-working space will be made available for a maximum of 12 months.
                        </p>
                    </div>
                </div>
                <p class="standard-paragraphs">

                    Stage 3 takes place over 8 months where the innovator is given access to a Propella advisor and business mentor.  Sessions are bespoke with one-on-one interactions twice monthly.  These are scheduled to suit the innovators time frames.  Here there is a focus on:
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i> The business
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i> The entrepreneur
                </p>
                <p class="standard-paragraphs">
                    <i class="tiny material-icons">chevron_right</i> The tech
                </p>
            </div>
        </div>
    </div>
</div>
<!--Stage 3 more information  Desktop-->
<div class="section standard-section-with-margins" id="Stage3InformationDesk">
    <div class="row">

    </div>
</div>

<script>
    $(document).ready(function () {
        $('#Stage0').on('click', function () {
            location.href = "#Stage0Row";
        });

    });

    $(document).ready(function () {
        $('#Stage1').on('click', function () {
            location.href = "#Stage1Row";
        });

    });

    $(document).ready(function () {
        $('#Stage2').on('click', function () {
            location.href = "#Stage2Row";
        });

    });

    $(document).ready(function () {
        $('#Stage3').on('click', function () {
            location.href = "#Stage3Row";
        });

    });

</script>
    </body>
@endsection
