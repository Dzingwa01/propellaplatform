@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Incubatees/main.css"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;500&family=Roboto:wght@100&display=swap" rel="stylesheet">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>
    <br/>

    <body>
    <div id="VenturesDesktop">
        <div class="row">
            <div class="parallax-container" id="ictTopSection">
                <div class="parallax">
                    <img src="/images/About/aeroplane-aircraft-airplane-638698.jpg" id="topImageICT"/>
                </div>
                <h1 class="ict-header" style="text-shadow: 5px 3px 1px #000000;">Exited</h1>
            </div>
        </div>
        <br/>
        <div class="row">
            @foreach ($ventures as $venture)
                @if($venture->hub == 'Industrial')
                @if(is_null($venture->stage))
                    <div class="col s12 m3" style="width: 370px;">
                        <div class="card white hoverable alumni">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 250px">
                                    <div class="user-view center-align">
                                        <a href="#user">
                                            <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                        </a>
                                    </div>
                                    <br />
                                    <div>
                                        <h6 class="oswaldbody"> {{$venture->company_name}}</h6>
                                        <p class="robotobody limit">{{$venture->venture_profile_information}}</p>
                                        <div class="flexposition">
                                            @foreach($venture->incubatee as $incubatee)
                                                <img id="u131_img" style="margin-left: -15px;"
                                                     src="/storage/{{isset($incubatee->uploads->profile_picture_url)?$incubatee->uploads->profile_picture_url:'images/ProfilePictures/NoProfilePic.jpg'}}"
                                                />
                                            @endforeach
                                            <p><span style="border: 1px; border-style:solid; border-color: #9e9e9e; padding: 1px; font-size: 10px; margin-left: 10px;">{{$venture->smart_city_tags}}</span></p>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                            </div>
                        </div>
                    </div>
                @elseif(!is_null($venture->stage))
                    <div class="col s12 m3" style="width: 370px;">
                        <div class="card white hoverable alumni">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 250px">
                                    <div class="user-view center-align">
                                        <a href="#user">
                                            <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                        </a>
                                    </div>
                                    <br />
                                    <div>
                                        <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}}</h6>
                                        <p class="robotobody limit">{{$venture->elevator_pitch}}</p>
                                    </div>
                                </div>
                                <br>
                                <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                            </div>
                        </div>
                    </div>
                @endif
                @else
                    @endif
            @endforeach
        </div>
    </div>

    {{--///////////////////////////////////////////////////////////////////////////////////////////////////////////--}}
    {{--    Mobile   --}}
    <div id="VenturesMobile">
        <div class="row">
            <div class="parallax-container" id="ictTopSection">
                <div class="parallax">
                    <img src="/images/About/aeroplane-aircraft-airplane-638698.jpg" id="topImageICT"/>
                </div>
                <h1 class="ict-header">ICT Alumni</h1>
            </div>
        </div>
        <br/>

        <div class="row">
            @foreach ($ventures as $venture)
                @if(!is_null($venture->stage))
                    <div class="col s12 m3" style="width: 355px;">
                        <div class="card white hoverable alumni">
                            <div class="circle">
                                <div class="card-content black-text " style="height: 250px">
                                    <div class="user-view center-align">
                                        <a href="#user">
                                            <img class="logo" src="{{isset($venture->ventureUploads->company_logo_url)?'/storage/'.$venture->ventureUploads->company_logo_url:''}}">
                                        </a>
                                    </div>
                                    <br />
                                    <div>
                                        <h6 class="oswaldbody" style="padding-bottom: 10px"> {{$venture->company_name}}</h6>
                                        <p class="robotobody limit">{{$venture->elevator_pitch}}</p>
                                    </div>
                                    <br>
                                    <input hidden disabled class="venture_id" data-value="{{$venture->slug}}">
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>

    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>

        $(document).ready(function () {
//        $('.parallax').parallax();


            //Card onclick
            $('.alumni').each(function () {

                let venture_id = $(this).find('.venture_id').attr('data-value');

                $(this).on('click', function () {
                    location.href = '/Home/Founders/' + venture_id;
                });
            });
        });


    </script>
    </body>
@endsection
