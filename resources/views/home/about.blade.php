@extends('layouts.app')

@section('content')
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/css/About/AboutMain.css"/>

    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>
    </head>
    <!--About Top Section: Image-->
    <!--Top Section-->
    <body>


    <div class="section ">
        <div class="row">
            <div class="parallax-container" id="aboutTopSection">
                <div class="parallax">
                    <img src="/images/About-Resized-Images/AirplanePropella (1).jpg" id="topImage">
                </div>
                <h1 class="about-section-headers">Future Thinking, Today</h1>
            </div>
        </div>
    </div>
    {{--<br />--}}

    <div class="section center-align standard-section-with-margins" id="visionMobile">
        <div class="row">
            <h4 class="standard-header">Our Vision</h4>
            <p class="standard-paragraphs">
                To be the “Go To”, High-Tech Incubator and Accelerator for innovators and ventures delivering Smart City
                Solutions and Industry 4.0 Technologies.
            </p>
        </div>
        <div class="row">
            <h4 class="standard-header">Our Mission</h4>
            <p class="standard-paragraphs">
                To support the successful development and sustainability of new innovation-
                based businesses.
            </p>
        </div>
        <div class="row">
            <h4 class="standard-header">Our Goal</h4>
            <p class="standard-paragraphs">
                To be the first choice for the commercialisation of smart products and services
                in our targeted sectors.
            </p>
        </div>
        <div class="row">
            <h4 class="standard-header">Values</h4>
            <p class="standard-paragraphs">
                Innovation
                Success
                Fun
            </p>
        </div>

        <div class="section center-align standard-section-with-margins">
            <div class="row">
                <div class="standard-blocks col s6 whatWeDo">
                    <h3 class="standard-block-headers">What We Do</h3>
                </div>
                <div class="standard-blocks col s6 ourPeople">
                    <h3 class="standard-block-headers">Our People</h3>
                </div>
            </div>
            <div class="row">
                <div class="standard-blocks col s6 ourPartners">
                    <h3 class="standard-block-headers">Our Partners</h3>
                </div>
                <div class="standard-blocks col s6 partnering">
                    <h3 class="standard-block-headers">Partnering with Propella</h3>
                </div>
            </div>
            <div class="row">
                <div class="standard-blocks col s6 whoWeIncubate">
                    <h3 class="standard-block-headers">Who we Incubate</h3>
                </div>
                <div class="standard-blocks col s6 participate">
                    <h3 class="standard-block-headers">How to Participate</h3>
                </div>
            </div>
        </div>
    </div>
    <!--About Vision & Mission & What We Do Desktop-->
    <div class="section center-align standard-section-with-margins" id="visionDesktop">
        <div id="">
            <div class="row">
                <div class="col m6">
                    <div class="row">
                        <div class="col m6">
                            <h3 class="standard-header">Our Vision</h3>
                            <p class="standard-paragraphs">
                                To be the “Go To”, High-Tech Incubator and Accelerator for innovators and ventures
                                delivering Smart City Solutions and Industry 4.0 Technologies. </p>
                        </div>
                        <div class="col m6">
                            <h3 class="standard-header">Our mission</h3>
                            <p class="standard-paragraphs">
                                To provide a significant entrepreneurial experience and presence in Nelson Mandela Bay
                                where innovation and technology convergence is facilitated to create sustainable and
                                profitable ventures, through exceptional management, leadership & infrastructure in
                                specific sectors.
                            </p>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col m6">
                            <h3 class="standard-header">Our goal</h3>
                            <p class="standard-paragraphs">
                                To be the first choice for the commercialisation of smart products and services
                                in our targeted sectors.
                            </p>
                        </div>
                        <div class="col m6">
                            <h4 class="standard-header">Values</h4>
                            <p class="standard-paragraphs">
                                Innovation<br/>
                                Success<br/>
                                Fun<br/>
                            </p>

                        </div>
                    </div>
                </div>


                <div class="col m2"></div>
                <div class="col m6">
                    <div class="row center">
                        <div class="row">
                            <div class="standard-blocks col l4 whatWeDo hoverable">
                                <h3 class="standard-block-headers">What We Do</h3>
                            </div>
                            <div class="standard-blocks col l4 ourPeople hoverable">
                                <h3 class="standard-block-headers">Our People</h3>
                            </div>
                            <div class="standard-blocks col l4 ourPartners hoverable">
                                <h3 class="standard-block-headers">Our Partners</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="standard-blocks col l4 partnering hoverable">
                                <h3 class="standard-block-headers">Partnering with Propella</h3>
                            </div>
                            <div class="standard-blocks col l4 whoWeIncubate hoverable">
                                <h3 class="standard-block-headers">Who we Incubate</h3>
                            </div>
                            <div class="standard-blocks col l4 participate hoverable">
                                <h3 class="standard-block-headers">How to Participate</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--About Second Image -->
    <div class="" id="pic1"></div>
    <div id="desktop">
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="aboutFourthSection">
                    <div class="parallax">
                        <img src="/images/Propella_resized_images/reception4.jpg" id="fourthImage">
                    </div>
                    <h1 class="about-section-header2">What We Do</h1>
                </div>
            </div>
        </div>
        <br/>
    </div>

    <div id="mobile">
        <div class="section">
            <div class="row">
                <div id="aboutFourthSection">
                    <div>
                        <img src="/images/Propella_resized_images/reception4.jpg" id="fourthImage">
                    </div>
                    <h1 class="about-section-header2">What We Do</h1>
                </div>
            </div>
            <br>
        </div>
    </div>
    <!--What we do Mobile-->
    <div class="mobileWhatWeDo">
        <div class="section center-align standard-section-with-margins">
            <div class="row">
                <div class="standard-blocks col s6 propella">
                    <h3 class="standard-block-headers">Propella</h3>
                </div>
                <div class="standard-blocks col s6 infra">
                    <h6 class="standard-block-headers">Location &amp; Infrastructure</h6>
                </div>
            </div>
            <div class="row">
                <div class="standard-blocks col s6 preIncubation">
                    <h6 class="standard-block-headers">Pre-incubation Support</h6>
                </div>
                <div class="standard-blocks col s6 incubationService">
                    <h6 class="standard-block-headers">Incubation Services</h6>
                </div>
            </div>
            <div class="row">
                <div class="standard-blocks col s6 accelerate">
                    <h6 class="standard-block-headers">Acceleration</h6>
                </div>
                <div class="standard-blocks col s6 nothing">
                    <h6 class="standard-block-headers"></h6>
                </div>
            </div>
        </div>

        <!--Propella Info-->
        <div class=" section standard-section-with-margins " id="MobileWhatWeDo">
            <div class=" propellaInfo" hidden>
                <div class="row center-align">
                    <h4 class="standard-header">Propella</h4>
                    <p class="standard-paragraphs">Propella is one of the first business hubs in South Africa to be
                        fully geared for Business 4.0.</p>
                    <p class="standard-paragraphs">
                        Exciting new IT and advanced manufacturing businesses share the same building to encourage
                        cooperation between innovators in both sectors.
                    </p>
                    <p class="standard-paragraphs">
                        Support for the development of technologies for Smart Cities come through a partnership between
                        Engeli Enterprise Development (EDD), a private sector business support company and Innovolve,
                        the
                        wholly owned commercialisation company of Nelson Mandela University (NMU).
                    </p>
                    <p class="standard-paragraphs">
                        Propella helps commercialise the research and development being conducted at the NMU,
                        as well as other technology entrepreneurs who either approach Propella for support or who are
                        identified
                        by Propella.
                    </p>
                    <p class="standard-paragraphs">
                        Prospective incubatees go through a rigorous selection process to qualify for access to the
                        Propella
                        and its support services.
                    </p>
                    <p class="standard-paragraphs">Once they are selected Propella facilitators assist with mentoring
                        and the evaluation of business concepts.</p>
                </div>
            </div>

            <!--Location Info-->
            <div class="infraMobile center-align" hidden>
                <h4>Location &amp; Infrastructure </h4>
                <p>Propella's modern hive of innovation, overlooking the Port Elizabeth harbour offers:</p>
                <ul class="center-align">
                    <li>• Office and light manufacturing space to allow for growth.</li>
                    <li>• Flexible rental options.</li>
                    <li>• Security - of both equipment and intellectual property.</li>
                    <li>• Central location - close to NMU campuses and the commercial and main industrial areas of
                        Nelson Mandela Bay.
                    </li>
                    <li>• High speed data connectivity - a "supersonic" 79mbps data link.</li>
                    <li>
                        • In-house 3D printing, machining, robotic welding, general purpose workshop and other
                        equipment suitable for prototyping and production of low-volume batches.
                    </li>
                </ul>
            </div>

            <!--PreIncubation Info-->
            <div class="incMobile center-align " hidden>
                <h4> Pre-incubation support (Restricted to ICT) </h4>
                <p>
                    All successful businesses and innovations start with an idea. Propella helps to
                    ignite the ideas to become actions through its pre-incubation support for innovators
                    who have a concept with potential, but need guidance on the first steps to
                    commercialisation.
                </p>
                <p>
                    Propella doors are open to inventors or entrepreneurs who are in that
                    all-important and delicate concept stage – the one where there is an idea but no
                    plan.
                </p>
                <p>
                    Propella's doors are open to inventors or entrepreneurs who are in that
                    all-important and delicate concept stage – the one where there is an idea but no plan.
                </p>
                <ul>
                    <li>• Desktop feasibility study of the proposed business idea</li>
                    <li>• Market assessment, feasibility planning and business planning</li>
                    <li>• Entrepreneurial assessment and feedback session</li>
                    <li>• Counselling and pre-incubation business advice</li>
                    <li>• Specific business start-up training to support the fledgling enterprise</li>
                    <li>• Coaching to guide them through proof of concept and business model canvas</li>
                </ul>
                <p>Entrepreneurs will be ready for selection for the next phase if they have:</p>
                <ul>
                    <li>• Completed the business model canvas</li>
                </ul>
            </div>

            <!--Incubation Info-->
            <div class="serviceMobile center-align" hidden>
                <h4>Incubation Services</h4>
                <p>
                    Propella measures its success by the number of companies which have been
                    commercialised – in other words, they have left the building as sustainable
                    businesses with both a product and a market. Of course, when pioneering new
                    products, processes and businesses, not all incubatees make it to market.
                    That failure is analysed to ensure that the lessons learned contribute to the success
                    of another new venture. By the way, we also celebrate failure, since it’s part of a
                    successful journey. Look out for our FUN2FAIL events.
                </p>
                <p><b>Propella provides four types of support:</b></p>
                <ul>
                    <li>• Technology and/or technology platform development - prototyping testing and preparation for
                        scale
                    </li>
                    <li>• Business Support – Guidance to establish and run a business</li>
                    <li>• Customer – Customer insights and access to markets within the growing Propella network</li>
                    <li>• Entrepreneur - Working on growing entrepreneurial ability in order to grow the business.</li>
                </ul>
                <p><b>Business toolbox</b></p>
                <p>
                    Engeli Enterprise Development, which manages Propella, has developed a
                    range of entrepreneurial, business and technology tools for the various facets
                    of the incubation/acceleration stages. These have been customised for Propella.
                </p>
                <p>
                    Entrepreneurs accepted by Propella sign a development contract based on an
                    assessment of the company using the diagnostic tools. The contract includes
                    development milestones and commitments from the entrepreneur, as well as
                    the anticipated date on which the enterprise will move into its own premises –
                    making space for the next potentially disruptive new business.
                </p>
            </div>

            <!--Accelaration Info-->
            <div class="accelerateMobile center-align" hidden>
                <h4>Acceleration</h4>
                <br/>
                <p>
                    Once they have successfully proved they have a viable business concept during
                    the incubation programme Propella is able to provide support to the owners
                    and managers of companies ready to growth their business to the next level.
                </p>
                <p>The main offerings include:</p>
                <p><b>Funding</b></p>
                <p>
                    Few financial institutions and venture capitalists in South Africa provide funding for new
                    technology or early stage venture capital, as they are seen as high risk.
                </p>
                <p>
                    Successful Propella incubatees who have established the potential to develop a successful and
                    sustainable business are assisted in applying to various funders.
                </p>
                <p>
                    Partnerships with other venture capital funds (Engeli Section 12J VCC and others) and financial
                    institutions are among the options for start-up debt and equity funding.
                </p>
                <p>
                    Some start-ups may also qualify for BBBEE enterprise development funding grants or loans.
                </p>
                <p>
                    Propella also facilitates the creation of partnerships (like JV’s) with established companies.
                </p>
                <br/>
                <p><b>Extended tenure</b></p>
                <p>
                    Space permitting, Propella may offer companies ready for commercialisation
                    the opportunity to continue making use of the Propella facilities and support
                    structure.
                </p>
            </div>
        </div>

    </div>
    <!--What we do Desktop-->
    <div class="section center-align standard-section-with-margins">
        <div class="row left-align">
            <div class="col s4" id="propInfoDesktop">
                <div class="standard-left-buttons propella">
                    <h6 class="standard-left-button-headers ">Propella</h6>
                </div>
                <div class="standard-left-buttons infra">
                    <h6 class="standard-left-button-headers ">Location &amp; Infrastructure</h6>
                </div>
                <div class="standard-left-buttons preIncubation">
                    <h6 class="standard-left-button-headers ">Pre-incubation Support</h6>
                </div>
                <div class="standard-left-buttons incubationService">
                    <h6 class="standard-left-button-headers ">Incubation Services</h6>
                </div>
                <div class="standard-left-buttons accelerate">
                    <h6 class="standard-left-button-headers ">Acceleration</h6>
                </div>
            </div>

            <div class="col s1"></div>
            <div class="col s7" id="DesktopWhatWeDo">
                <div class=" propellaInfo ">
                    <h4 class="standard-header">Propella</h4>

                    <p class="standard-paragraphs">Propella is one of the first business hubs in South Africa to be
                        fully geared for Business 4.0.</p>

                    <p class="standard-paragraphs">
                        Exciting new IT and advanced manufacturing businesses share the same building to encourage
                        cooperation between innovators in both sectors.
                    </p>

                    <p class="standard-paragraphs">
                        Support for the development of technologies for Smart Cities come through a partnership between
                        Engeli Enterprise Development (EDD), a private sector business support company and Innovolve,
                        the
                        wholly owned commercialisation company of Nelson Mandela University (NMU).
                    </p>

                    <p class="standard-paragraphs">
                        Propella helps commercialise the research and development being conducted at the NMU,
                        as well as other technology entrepreneurs who either approach Propella for support or who are
                        identified
                        by Propella.
                    </p>

                    <p class="standard-paragraphs">
                        Prospective incubatees go through a rigorous selection process to qualify for access to the
                        Propella
                        and its support services.
                    </p>

                    <p class="standard-paragraphs">Once they are selected Propella facilitators assist with mentoring
                        and the evaluation of business concepts.</p>
                </div>

                <!--Location Info-->
                <div class=" propellaInfrastructure " hidden>
                    <h4 class="standard-header">Location &amp; Infrastructure </h4>

                    <p class="standard-paragraphs">Propella's modern hive of innovation, overlooking the Port Elizabeth
                        harbour offers:</p>

                    <ul class="left-align">
                        <li class="standard-paragraphs">• Office and light manufacturing space to allow for growth.</li>
                        <li class="standard-paragraphs">• Flexible rental options.</li>
                        <li class="standard-paragraphs">• Security - of both equipment and intellectual property.</li>
                        <li class="standard-paragraphs">• Central location - close to NMU campuses and the commercial
                            and main industrial areas of Nelson Mandela Bay.
                        </li>
                        <li class="standard-paragraphs">• High speed data connectivity - a "supersonic" 79mbps data
                            link.
                        </li>
                        <li class="standard-paragraphs">
                            • In-house 3D printing, machining, robotic welding, general purpose workshop and other
                            equipment suitable for prototyping and production of low-volume batches.
                        </li>
                    </ul>
                </div>

                <!--PreIncubation Info-->
                <div class=" propellaPreIncubation" hidden>
                    <h4 class="standard-header"> Pre-incubation support (Restricted to ICT) </h4>

                    <p class="standard-paragraphs">
                        All successful businesses and innovations start with an idea. Propella helps to
                        ignite the ideas to become actions through its pre-incubation support for innovators
                        who have a concept with potential, but need guidance on the first steps to
                        commercialisation.
                    </p>

                    <p class="standard-paragraphs">
                        Propella doors are open to inventors or entrepreneurs who are in that
                        all-important and delicate concept stage – the one where there is an idea but no
                        plan.
                    </p>

                    <p class="standard-paragraphs">
                        Propella's doors are open to inventors or entrepreneurs who are in that
                        all-important and delicate concept stage – the one where there is an idea but no plan.
                    </p>
                    <ul>
                        <li class="standard-paragraphs">• Desktop feasibility study of the proposed business idea</li>
                        <li class="standard-paragraphs">• Market assessment, feasibility planning and business
                            planning
                        </li>
                        <li class="standard-paragraphs">• Entrepreneurial assessment and feedback session</li>
                        <li class="standard-paragraphs">• Counselling and pre-incubation business advice</li>
                        <li class="standard-paragraphs">• Specific business start-up training to support the fledgling
                            enterprise
                        </li>
                        <li class="standard-paragraphs">• Coaching to guide them through proof of concept and business
                            model canvas
                        </li>
                    </ul>

                    <p class="standard-paragraphs">Entrepreneurs will be ready for selection for the next phase if they
                        have:</p>
                    <ul>
                        <li class="standard-paragraphs">• Completed the business model canvas</li>
                    </ul>
                </div>

                <!--Incubation Info-->
                <div class="incubationServices" hidden>
                    <h4 class="standard-header">Incubation Services</h4>

                    <p class="standard-paragraphs">
                        Propella measures its success by the number of companies which have been
                        commercialised – in other words, they have left the building as sustainable
                        businesses with both a product and a market. Of course, when pioneering new
                        products, processes and businesses, not all incubatees make it to market.
                        That failure is analysed to ensure that the lessons learned contribute to the success
                        of another new venture. By the way, we also celebrate failure, since it’s part of a
                        successful journey. Look out for our FUN2FAIL events.
                    </p>

                    <p class="standard-paragraphs"><b>Propella provides four types of support:</b></p>
                    <ul>
                        <li class="standard-paragraphs">• Technology and/or technology platform development -
                            prototyping testing and preparation for scale
                        </li>
                        <li class="standard-paragraphs">• Business Support – Guidance to establish and run a business
                        </li>
                        <li class="standard-paragraphs">• Customer – Customer insights and access to markets within the
                            growing Propella network
                        </li>
                        <li class="standard-paragraphs">• Entrepreneur - Working on growing entrepreneurial ability in
                            order to grow the business.
                        </li>
                    </ul>

                    <p class="standard-paragraphs"><b>Business toolbox</b></p>
                    <p class="standard-paragraphs">
                        Engeli Enterprise Development, which manages Propella, has developed a
                        range of entrepreneurial, business and technology tools for the various facets
                        of the incubation/acceleration stages. These have been customised for Propella.
                    </p>
                    <p class="standard-paragraphs">
                        Entrepreneurs accepted by Propella sign a development contract based on an
                        assessment of the company using the diagnostic tools. The contract includes
                        development milestones and commitments from the entrepreneur, as well as
                        the anticipated date on which the enterprise will move into its own premises –
                        making space for the next potentially disruptive new business.
                    </p>
                </div>

                <!--Accelaration Info-->
                <div class="propellaAccelerate " hidden>
                    <h4 class="standard-header">Acceleration</h4>

                    <p class="standard-paragraphs">
                        Once they have successfully proved they have a viable business concept during
                        the incubation programme Propella is able to provide support to the owners
                        and managers of companies ready to growth their business to the next level.
                    </p>
                    <p class="standard-paragraphs">The main offerings include:</p>
                    <p><b>Funding</b></p>
                    <p class="standard-paragraphs">
                        Few financial institutions and venture capitalists in South Africa provide funding for new
                        technology or early stage venture capital, as they are seen as high risk.
                    </p>
                    <p class="standard-paragraphs">
                        Successful Propella incubatees who have established the potential to develop a successful and
                        sustainable business are assisted in applying to various funders.
                    </p>
                    <p class="standard-paragraphs">
                        Partnerships with other venture capital funds (Engeli Section 12J VCC and others) and financial
                        institutions are among the options for start-up debt and equity funding.
                    </p>
                    <p class="standard-paragraphs">
                        Some start-ups may also qualify for BBBEE enterprise development funding grants or loans.
                    </p>
                    <p class="standard-paragraphs">
                        Propella also facilitates the creation of partnerships (like JV’s) with established companies.
                    </p>

                    <p><b>Extended tenure</b></p>
                    <p class="standard-paragraphs">
                        Space permitting, Propella may offer companies ready for commercialisation
                        the opportunity to continue making use of the Propella facilities and support
                        structure.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!--Third Image-->
    <div class="" id="pic2"></div>
    <div id="desktop">
        <br/>
        <br/>
        <br/>
        <br/>
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="aboutThirdSection">
                    <div class="parallax">
                        <img class="cover " src="/images/About-Resized-Images/OUR-PEOPLE.jpg" width="242" height="403"
                             id="thirdImage">
                    </div>
                    <h1 class="about-section-header3">Our People</h1>
                </div>
            </div>
        </div>
        <br/>
    </div>

    <div id="mobile">
        <div class="section">
            <div class="row">
                <div id="aboutThirdSection">
                    <div>
                        <img class="cover " src="/images/About-Resized-Images/OUR-PEOPLE.jpg" width="242" height="403"
                             id="thirdImage">
                    </div>
                    <h1 class="about-section-header3">Our People</h1>
                </div>
            </div>
            <br>
        </div>
    </div>
    <!--Our People Section Mobile-->
    <div class="section center-align standard-section-with-margins" id="ourPeopleMobile">
        <div class="">
            <div class="row">
                <div class="standard-blocks col s6 BoardnGuestsBtn">
                    <h4 class="standard-block-headers">Board &amp; Guests</h4>
                </div>
                <div class="standard-blocks col s6 ManagementBtn">
                    <h4 class="standard-block-headers">Management</h4>
                </div>
            </div>
            <div class="row">
                <div class="standard-blocks col s6 StaffBtn">
                    <h4 class="standard-block-headers">Team</h4>
                </div>
                <div class="standard-blocks col s6 MentorsBtn">
                    <h4 class="standard-block-headers">Mentors</h4>
                </div>
            </div>
        </div>
        <div class="managementInfo">
            <h4 style="text-align: center;font-size: 2rem;">Management</h4>
            <div class="row" style="padding-left: 20px;">
                <a href="https://www.linkedin.com/in/lindalawriecoaching/">
                    <div class="col m4 hoverable" id="anitaFirstImage">
                        <div id="Details" style=" margin-top:40vh" ;>
                            <p class="detailsStyle"><b>Linda Lawrie</b></p>
                            <p class="detailsStyle">Incubation Journey Facilitator</p>
                        </div>
                    </div>
                </a>
                <a href="https://www.linkedin.com/in/grant-minnie-85898719">
                    <div class="col m4 hoverable" id="grantSecondImage">
                        <div id="Details" style=" margin-top:40vh" ;>
                            <p class="detailsStyle"><b>Grant Minnie</b></p>
                            <p class="detailsStyle">Senior Industrial Advisor</p>
                        </div>
                    </div>
                </a>
                <a href="https://www.linkedin.com/in/errol-wills-13a30121">
                    <div class="col m4 hoverable" id="errolThirdImage">
                        <div class="col s14" id="Details" style=" margin-top:50vh" ;>
                            <p class="detailsStyle"><b>Errol Wills</b></p>
                            <p class="detailsStyle">Senior ICT Advisor</p>
                        </div>
                    </div>
                </a>


            </div>
        </div>
        <div class="staffInfo " hidden>
            <h4 style="text-align: center;font-size: 2rem;">Team</h4>
            <div class="row" style="padding-left: 20px;">

                <div class="col m3 hoverable" id="apheleleSecondImage">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <p class="detailsStyle"><b>Aphelele Jonas</b></p>
                        <p class="detailsStyle">Marketing &amp; Public Relations</p>
                    </div>
                </div>
                <div class="col s12 hoverable" id="sinazoThirdImage">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <p class="detailsStyle"><b>Sinazo Jack</b></p>
                        <p class="detailsStyle">Administrator</p>
                    </div>
                </div>
                <div class="col s12 hoverable" id="gwenFouthImage">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <ul>
                            <li><b class="detailsStyle"><b></b> Gwen Assam</b></li>
                            <li><p class="detailsStyle">Facilities</p></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 20px;">
                <div class="col s12 hoverable" id="SinazoQ">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <p class="detailsStyle"><b>Sinazo Qomve</b></p>
                    </div>
                </div>
                <div class="col s12 hoverable" id="Sikhona">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <ul>
                            <li><b class="detailsStyle"><b></b> Sikhona</b></li>
                        </ul>
                    </div>
                </div>

                <div class="col s12 hoverable" id="Kris">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <ul>
                            <li><b class="detailsStyle"><b></b> Kris</b></li>
                        </ul>
                    </div>
                </div>


                <div class="col s12 hoverable" id="Sokwakhana">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <ul>
                            <li><b class="detailsStyle"><b></b> Sokwakhana</b></li>
                        </ul>
                    </div>
                </div>
                <div class="col s12 hoverable" id="Nelisa">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <ul>
                            <li><b class="detailsStyle"><b></b>Nelisa Tshayana</b></li>
                        </ul>
                    </div>
                </div>
                <div class="col s12 hoverable" id="Ongezwa">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <ul>
                            <li><b class="detailsStyle"><b></b>Ongezwa Ntliziyombi</b></li>
                        </ul>
                    </div>
                </div>
                <div class="col s12 hoverable" id="Sino">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <ul>
                            <li><b class="detailsStyle"><b></b>Sinovuyo Mfengwana</b></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="mentorsInfo " hidden>
            <div class="row" style="padding-left: 20px;font-size: 2rem;">
                <h4 style="text-align: center;">Mentors</h4>
                <div class="col ms12 hoverable" id="darryFirstImage">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <p class="detailsStyle"><b>Daryl McWilliams</b></p>
                    </div>
                </div>
                <div class="col s12 hoverable" id="wayneFourthImage">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <p class="detailsStyle"><b>Wayne Oosthuizen</b></p>
                    </div>
                </div>
                <div class="col s12 hoverable" id="edFifthImage">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <p class="detailsStyle"><b>Ed Richardson</b></p>
                    </div>
                </div>
                <div class="col s12 hoverable" id="georgeSixthImage">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <p class="detailsStyle"><b>George Meiring</b></p>
                    </div>
                </div>

                <div class="col s12 hoverable" id="sanjivNinthImage">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <p class="detailsStyle"><b>Sanjiv Ranchod</b></p>
                    </div>
                </div>
                <div class="col s12 hoverable" id="nickMarriotImage">
                    <div id="Details" style=" margin-top:40vh" ;>
                        <p class="detailsStyle"><b>Nick Marriott</b></p>
                    </div>
                </div>

            </div>
        </div>
        <div class="boardAndGuests" hidden>
            <div class="row">
                <h4>Board and Guests</h4>
                <div class="col m4">
                    <div class="container">
                        <img src="/images/BarryEdited.png" alt="Avatar" class="image"
                             style="width:400%">
                        <div class="middle">
                            <div class="text">Barry Wiseman</div>
                            <p class="detailsStyle">Engeli Enterprise Development</p>

                        </div>
                    </div>
                </div>

                <div class="col m4">
                    <div class="container">
                        <img src="/images/Celeste_Thomas-removebg-preview.png" alt="Avatar" class="image"
                             style="width:520%;height: 36vh;">
                        <div class="middle">
                            <div class="text">Celeste Thomas</div>
                        </div>
                    </div>
                </div>
                <div class="col m4">
                    <div class="container">
                        <img src="/images/ProfilePictures/Propella Staff/Ricardo-remove.png" alt="Avatar" class="image"
                             style="width:400%">
                        <div class="middle">
                            <div class="text">Dr Ricardo Dames</div>
                            <p class="detailsStyle">Engeli Enterprise Development</p>

                        </div>
                    </div>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col m4">
                    <div class="container">
                        <img src="/images/Nqobile_Gumede-removebg-preview.png" alt="Avatar" class="image"
                             style="width:400%;height: 37vh">
                        <div class="middle">
                            <div class="text">Dr Nqobile Gumede</div>
                        </div>
                    </div>
                </div>

                <div class="col m4">
                    <div class="container">
                        <img src="/images/Gregory_Wood_587_-removebg-preview.png" alt="Avatar" class="image"
                             style="width:480%;height: 38vh">
                        <div class="middle">
                            <div class="text">Gregory Wood</div>
                        </div>
                    </div>
                </div>
                <div class="col m4">
                    <div class="container">
                        <img src="/images/Linda_594_-removebg-preview (1).png" alt="Avatar" class="image"
                             style="width:450%;height: 38vh">
                        <div class="middle">
                            <div class="text">Linda Brown</div>
                            <p class="detailsStyle">BASF</p>

                        </div>
                    </div>
                </div>
                <br>
            </div>

            <br>
            <div class="row">
                <div class="col m4">
                    <div class="container">
                        <img src="/images/Mariam_Jogee-Jamal-removebg-preview.png" alt="Avatar" class="image"
                             style="width:450%;height: 38vh">
                        <div class="middle">
                            <div class="text">Mariam Jogee-Jamal</div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

    <!--Our People Section Desktop-->
    <div class="section center-align standard-section-with-margins" id="ourPeopleDesktop">
        <div class="row left-align">
            <div class="col s4" id="ourPeopleDesktop">
                <div class="standard-left-buttons  BoardnGuestsBtn">
                    <h6 class="standard-left-button-headers ">Board &amp; Guests</h6>
                </div>
                <div class="standard-left-buttons  ManagementBtn">
                    <h6 class="standard-left-button-headers ">Management</h6>
                </div>
                <div class="standard-left-buttons  StaffBtn">
                    <h6 class="standard-left-button-headers ">Team</h6>
                </div>
                <div class="standard-left-buttons MentorsBtn">
                    <h6 class="standard-left-button-headers ">Mentors</h6>
                </div>
            </div>
            <style>
                .container {
                    position: relative;
                    width: 20%;
                    margin-left: 40px;
                }

                .image {
                    opacity: 1;
                    display: block;
                    width: auto;
                    height: auto;
                    transition: .5s ease;
                    backface-visibility: hidden;
                }

                .middle {
                    transition: .5s ease;
                    opacity: 0;
                    position: absolute;
                    top: 50%;
                    left: 200%;
                    transform: translate(-50%, -50%);
                    -ms-transform: translate(-50%, -50%);
                    text-align: center;
                }

                .container:hover .image {
                    opacity: 0.3;
                }

                .container:hover .middle {
                    opacity: 1;
                    top: 30vh;
                }

                .text {
                    backdrop-filter: blur(10px);
                    color: black;
                    font-size: 1em;
                    padding: 16px 32px;
                    width: 200px;

                }
            </style>

            <!---->
            <div class="col s7">
                <!--Board and guest info-->
                <div class="boardAndGuests" hidden>
                    <h4 style="text-align: center;color: black; background-color: lightgray;width: 677px;margin-left: 5.5%;height: 7vh">
                        <b>Board and Guests</b></h4>
                    <div class="row">
                        <a href=" https://www.linkedin.com/in/barry-wiseman-04796a9/" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/BarryEdited.png" alt="Avatar" class="image"
                                         style="width:400%;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Barry Wiseman</div>
                                        <div class="text">Engeli Enterprise Development</div>

                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="https://www.linkedin.com/in/celeste-thomas-18854577//" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Celeste_Thomas-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:520%;height: 36vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Celeste Thomas</div>
                                        <div class="text">Algoa FM</div>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="https://www.linkedin.com/in/celeste-thomas-18854577//" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/ProfilePictures/Propella Staff/Ricardo-remove.png" alt="Avatar"
                                         class="image"
                                         style="width:400%;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Dr Ricardo Dames</div>
                                        <div class="text">Engeli Enterprise Development</div>

                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <br>

                    <div class="row">
                        <a href="https://www.linkedin.com/in/nqobile-gumede-m-inst-d-7b026030/" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Nqobile_Gumede-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Dr Nqobile Gumede</div>
                                        <div class="text">Nelson Mandela University</div>

                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="https://www.linkedin.com/in/gregory-wood-920a58a7/" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Gregory_Wood_587_-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:480%;height: 38vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Gregory Wood</div>
                                        <p class="text">ISUZU</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href=" https://www.linkedin.com/in/linda-brown-basf/" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Linda_594_-removebg-preview (1).png" alt="Avatar" class="image"
                                         style="width:450%;height: 38vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Linda Brown</div>
                                        <p class="text">BASF</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <br>
                    </div>

                    <br>
                    <div class="row">
                        <a href="https://www.linkedin.com/in/mariam-jogee-jamal-ca-sa-0a6b0748/" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Mariam_Jogee-Jamal-removebg-preview.png" alt="Avatar"
                                         class="image"
                                         style="width:450%;height: 38vh;background-color: grey">
                                    <div class="middle">
                                        <div class="text">Mariam Jogee-Jamal</div>
                                        <p class="text">Nelson Mandela University</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <br>
                    </div>


                    <div class="row" hidden>
                        <h4 style="text-align: center;">Board and Guests</h4>
                        <div class="col m3" id="AndrewImage">
                            <div id="Details" style=" margin-top:40vh" ;>
                                <p class="detailsStyle"><b>Prof Andrew Leitch</b></p>
                                <p class="detailsStyle">Nelson Mandela University</p>
                            </div>
                        </div>
                        <div class="col m3" id="JohanImage">
                            <div id="Details" style=" margin-top:40vh" ;>
                                <p class="detailsStyle"><b>Johan Wasserman</b></p>
                                <p class="detailsStyle">Nelson Mandela University</p>
                            </div>
                        </div>
                        <div class="col m3" id="WayneImage">
                            <div id="Details" style=" margin-top:40vh" ;>
                                <p class="detailsStyle"><b>Wayne Oosthuizen</b></p>
                                <p class="detailsStyle">Engeli Enterprise Development</p>
                            </div>
                        </div>
                        <div class="col m3" id="RicardoImage">
                            <div id="Details" style=" margin-top:40vh" ;>
                                <p class="detailsStyle"><b>Dr Ricardo Dames</b></p>
                                <p class="detailsStyle">Engeli Enterprise Development</p>
                            </div>
                        </div>
                        <div class="col m3" id="BarryImage">
                            <div id="Details" style=" margin-top:40vh" ;>
                                <p class="detailsStyle"><b>Barry Wiseman</b></p>
                                <p class="detailsStyle">Engeli Enterprise Development</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--MANAGEMENT-->
                <div class="managementInfo">
                    <h4 style="text-align: center;color: black; background-color: lightgray;width: 677px;margin-left: 5.5%;height: 7vh">
                        <b>Management</b></h4>
                    <div class="row">
                        <a href="https://www.linkedin.com/in/grant-minnie-85898719" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Grant.JPG" alt="Avatar" class="image"
                                         style="width:400%;height: 200%;">
                                    <div class="middle">
                                        <div class="text">Grant Minnie</div>
                                        <div class="text">Senior Industrial Advisor</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="https://www.linkedin.com/in/lindalawriecoaching/" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Linda.JPG" alt="Avatar" class="image"
                                         style="width:400%">
                                    <div class="middle">
                                        <div class="text">Linda Lawrie</div>
                                        <div class="text">Incubation Journey Facilitator</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="col m4 ">
                            <div class="container">
                                <img src="/images/New_Team_Images/Siphosethu Martins.jpeg" alt="Avatar" class="image"
                                     style="width:400%;height: 37vh">
                                <div class="middle">
                                    <div class="text">Siphosethu Martins</div>
                                    <div class="text">Finance and M&E</div>
                                </div>
                            </div>
                        </div>
                        <a href="https://www.linkedin.com/in/errol-wills-13a30121" target="_blank">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Errol.JPG" alt="Avatar" class="image"
                                         style="width:400%">
                                    <div class="middle">
                                        <div class="text">Errol Wills</div>
                                        <div class="text">Senior ICT Advisor</div>

                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <br>

                </div>
                <!--TEAM-->
                <div class="staffInfo " hidden>
                    <h4 style="text-align: center;color: black; background-color: lightgray;width: 695px;margin-left: 4%;height: 7vh">
                        <b>Team</b></h4>
                    <div class="row">
                        <div class="row">
                            <div class="col m4 ">
                                <div class="container  ">
                                    <img src="/images/New_Team_Images/Sinazo q.JPG" alt="Avatar" class="image"
                                         style="width:400%; height: 37vh">
                                    <div class="middle">
                                        <div class="text">Sinazo Qomve</div>
                                        <div class="text">Software Engineer</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Apelele.JPG" alt="Avatar" class="image"
                                         style="width:400%;height: 200%;">
                                    <div class="middle">
                                        <div class="text">Aphelele Jonas</div>
                                        <div class="text">Marketing</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Sinazo j.JPG" alt="Avatar" class="image"
                                         style="width:400%">
                                    <div class="middle">
                                        <div class="text">Sinazo Jack</div>
                                        <div class="text">Administrator</div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/image00001[3641].jpeg" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Sikhona Mbashe</div>
                                        <div class="text">Graphic Designer</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Gwen.JPG" alt="Avatar" class="image"
                                         style="width:400%; height: 37vh">
                                    <div class="middle">
                                        <div class="text">Gwen Assam</div>
                                        <div class="text">Facilities</div>

                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Kris Swartbooi.jpeg" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Kris Swartbooi</div>
                                        <div class="text">Videographer</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Sokwakhana Badi.jpeg" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Sokwakhana Badi</div>
                                        <div class="text">PR & Marketing</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Sinovuyo Mfengwana.jpeg" alt="Avatar"
                                         class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Sinovuyo Mfengwana</div>
                                        <div class="text">Receptionist</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/New_Team_Images/Ongezwa Ntliziyombi.jpeg" alt="Avatar"
                                         class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Ongezwa Ntliziyombi</div>
                                        <div class="text">Monitoring and Evaluation</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col m4">
                                <div class="container">

                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Mentors-->
                <div class="mentorsInfo" hidden>
                    <h4 style="text-align: center;color: black; background-color: lightgray;width: 695px;margin-left:4%;height: 7vh">
                        <b>Mentors</b></h4>
                    <div class="row">
                        <div class="row">
                            <div class="col m4">
                                <div class="container  ">
                                    <img src="/images/Ronelle.jpg" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Ronelle Plaatjes</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container  ">
                                    <img src="/images/JODENE STILL.png" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Jodene Still</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4 ">
                                <div class="container  ">
                                    <img src="/images/George-Meiring_2304_-removebg-preview.png" alt="Avatar"
                                         class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">George Meiring</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col m4 ">
                                <div class="container  ">
                                    <img src="/images/Kavita Govindjee.jpg" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Kavita Govindjee</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Ed_Richardson___17__586_-removebg-preview.png" alt="Avatar"
                                         class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Ed Richardson</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/nick-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Nick Marriott</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col m4 ">
                                <div class="container  ">
                                    <img src="/images/darry-removebg-preview.png" alt="Avatar" class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Daryl Williams</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col m4">
                                <div class="container">
                                    <img src="/images/Wayne_Oosthuizen_2303_-removebg-preview.png" alt="Avatar"
                                         class="image"
                                         style="width:400%;height: 37vh">
                                    <div class="middle">
                                        <div class="text">Wayne Oosthuizen</div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!--Funding Partners-->
            <div class="fundingPartner" hidden>
                <h4 style="text-align: center;">Funding Partners</h4>
                <div class="row">
                    <a href="https://www.bcx.co.za/" target="_blank">
                        <div class="col s5" id="BCXLogo" style="width: 30%;"></div>
                    </a>

                    <a href="https://www.isuzu.co.za/" target="_blank">
                        <div class="col s5" id="IsuzuLogo" style="width: 33%;"></div>
                    </a>

                    <a href="https://www.mandela.ac.za" target="_blank">
                        <div class="col s5" id="NMULogo" style="width: 35%"></div>
                    </a>

                </div>
                <div class="row">
                    <a href="https://www.algoafm.co.za/" target="_blank">
                        <div class="col s5" id="AlgoaFMLogo" style="width: 30%;"></div>
                    </a>
                    <a href="https://www.basf.com/za/en.html/" target="_blank">
                        <div class="col s5" id="BASFLogo" style="width: 30%;"></div>
                    </a>
                    <a href="http://www.sefa.org.za/" target="_blank">
                        <div class="col s5" id="sefa" style="width: 30%;"></div>
                    </a>
                </div>
                <div class="row">
                    <a href="http://www.seda.org.za/" target="_blank">
                        <div class="col s5" id="seda" style="width: 30%;"></div>
                    </a>
                    <a href="https://www.solidworks.com/" target="_blank">
                        <div class="col s5" id="solidworks" style="width: 30%;"></div>
                    </a>
                </div>
            </div>
        </div>

    </div>


    <!--Image-->
    <div id="desktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container" id="aboutFifthSection">
                    <div class="parallax">
                        <img src="/images/About/New propellabuilding.jpg" id="fifthImage">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mobile">
        <div class="section">
            <div class="row">
                <div id="aboutFifthSection">
                    <div>
                        <img src="/images/About/New propellabuilding.jpg" id="fifthImage">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .standard-header {
            text-transform: capitalize;
        }
    </style>
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

    <script>

        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.parallax');
            var instances = M.Parallax.init(elems, []);
            $('.materialboxed').materialbox();
        });
        var options = {
            duration: 200,
            padding: 0,
            numVisible: 4
        }
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.carousel');
            var instances = M.Carousel.init(elems, options);
        });


        $(document).ready(function () {


            $('.parallax').parallax();

            //WHAT WE DO :CLICK
            $('.whatWeDo').on('click', function () {
                location.href = '#pic1';
            });
            //Our People
            $('.ourPeople').on('click', function () {
                location.href = '#pic2';
            });
            //Partners
            $('.ourPartners').on('click', function () {
                location.href = '#pic3';
            });
            //Partnering with Propella
            $('.partnering').on('click', function () {
                location.href = '/Home/Partners';
            });
            //Who we Incubate
            $('.whoWeIncubate').on('click', function () {
                location.href = '/Home/Categories';
            });
            //How to Participate
            $('.participate').on('click', function () {
                location.href = '/Home/Partners#talk';
            });


            $('.propella').on('click', function () {
                $('.propellaInfoMobile').show();
            });
            $('.infra').on('click', function () {
                $('.infraMobile').show();
            });
            $('.preIncubation').on('click', function () {
                $('.incMobile').show();
            });
            $('.incubationService').on('click', function () {
                $('.serviceMobile').show();
            });
            $('.accelerate').on('click', function () {
                $('.accelerateMobile').show();
            });


            //Propella Button Desktop click
            $('.propella').on('click', function () {
                $('.propellaInfo').show();
                $('.propellaInfrastructure').hide();
                $('.propellaPreIncubation').hide();
                $('.incubationServices').hide();
                $('.propellaAccelerate').hide();
            });
            //Infrastructure button click
            $('.infra').on('click', function () {
                $('.propellaInfrastructure').show();
                $('.propellaInfo').hide();
                $('.propellaPreIncubation').hide();
                $('.incubationServices').hide();
                $('.propellaAccelerate').hide();
            });
            //Pre-Incubation button click
            $('.preIncubation').on('click', function () {
                $('.propellaPreIncubation').show();
                $('.propellaInfrastructure').hide();
                $('.propellaInfo').hide();
                $('.incubationServices').hide();
                $('.propellaAccelerate').hide();
            });
            //Incubation service button
            $('.incubationService').on('click', function () {
                $('.incubationServices').show();
                $('.propellaPreIncubation').hide();
                $('.propellaInfrastructure').hide();
                $('.propellaInfo').hide();
                $('.propellaAccelerate').hide();
            });
            //Acceleration button
            $('.accelerate').on('click', function () {
                $('.propellaAccelerate').show();
                $('.incubationServices').hide();
                $('.propellaPreIncubation').hide();
                $('.propellaInfrastructure').hide();
                $('.propellaInfo').hide();
            });
            //Board and guests button
            $('.BoardnGuestsBtn').on('click', function () {
                $('.boardAndGuests').show();
                $('.managementInfo').hide();
                $('.staffInfo ').hide();
                $('.mentorsInfo').hide();
            });

            //Management button click
            $('.ManagementBtn').on('click', function () {
                $('.managementInfo').show();
                $('.boardAndGuests').hide();
                $('.staffInfo ').hide();
                $('.mentorsInfo').hide();
            });
            //Staff button click
            $('.StaffBtn').on('click', function () {
                $('.staffInfo ').show();
                $('.managementInfo').hide();
                $('.boardAndGuests').hide();
                $('.mentorsInfo').hide();
            });
            //Mentors button click
            $('.MentorsBtn').on('click', function () {
                $('.mentorsInfo').show();
                $('.staffInfo ').hide();
                $('.managementInfo').hide();
                $('.boardAndGuests').hide();
            });
            //Shareholder button click
            $('.shareholder').on('click', function () {
                $('.ourShareholders').show();
                $('.foundingPartner').hide();
                $('.fundingPartner').hide();
            });
            //Founding button click
            $('.founding').on('click', function () {
                $('.foundingPartner').show();
                $('.ourShareholders').hide();
                $('.fundingPartner').hide();

            });
            //Funding button click
            $('.funding').on('click', function () {
                $('.fundingPartner').show();
                $('.foundingPartner').hide();
                $('.ourShareholders').hide();
            });
        });
    </script>
    </body>
@endsection
