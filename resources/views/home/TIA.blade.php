@extends('layouts.app')

@section('content')

    <div class="row center-align " style="margin-top: 9vh;margin-left: 3em;margin-right: 3em">
        <h4><strong>CALL FOR PROPOSALS – TIA SMME SEED FUND</strong></h4>
        <br>
        <!-- Modal Trigger -->
        <a class="waves-effect waves-light btn modal-trigger"  href="#modal1">VIEW TIA SMME SEED FUND CRITERIA</a>
        <br>

        <!-- Modal Structure -->
        <div id="modal1" class="modal">
            <div class="modal-content">
                <h4><strong>CALL FOR PROPOSALS – TIA SMME SEED FUND</strong></h4>
                <p> Propella Business Incubator (Pty) Ltd is an Implementing Partner for the Technology Innovation Agency (TIA) to implement
                    the SMME Seed Fund Programme in Nelson Mandela Bay, aimed at assisting knowledge economy-based SMMEs to access funding
                    to advance their innovative ideas to market.</p>
                <p>The focus of this Proposal Call will be all TIA sectors, however priority will be given to the following areas: <b>Biosimilars;
                        Smart Agriculture; Industrial Biotech (Bioremediation, Biomaterials and Biocatalysis); and Indigenous Knowledge System
                        related technologies (African Traditional Medicines, Cosmeceuticals, Nutraceuticals and Health Infusions).</b>
                    Other sectors include Advanced Manufacturing and Engergy.</p>
                <p>The broad parameters of the funding are:</p>
                <ul>
                    <li>•	Grant Funding up to R650 000 per application</li>
                    <li>•   TRL 3 to 7</li>
                    <li>•   Alignment with fundable activities (see below)</li>
                    <li>•   Valid Tax Clearance Certificates for company or individual applying</li>
                    <li>•   TIA reserves the right of First Refusal for follow on funding</li>
                </ul>
                <p>Funding will be directed at SMMEs / entrepreneurs requiring assistance to establish and validate their
                    commercial and technical proof-of-concept via the following activities:</p>
                <ul>
                    <li>•   Prototype development and evaluating prototypes against customer requirements</li>
                    <li>•   Turning prototypes into pre-production products (scale up and piloting)</li>
                    <li>•   Activities leading to technology co-development</li>
                    <li>•   Licensing of technology to manufacturers and for distribution</li>
                    <li>•   Transfer of technology for development and manufacture</li>
                    <li>•   Design development and support of certification activities through SABS or equivalent</li>
                    <li>•   Purchase of hardware for scale-up from prototypes</li>
                    <li>•   Supporting IP protection maintenance costs</li>
                    <li>•   Detailed primary market research and</li>
                    <li>•   Business plan development</li>
                </ul>
                <p>The following activities will not qualify for funding support:</p>
                <ul>
                    <li>•   Working Capital</li>
                    <li>•   Human resource costs except with software development – human capacity needed for project duration</li>
                    <li>•   Purchasing of equipment that is not within the applicant’s scope of work and relevant to the allowable
                        activities and objective of their application.</li>
                </ul>
                <p>Projects should have the potential for further investment and development by funders.</p>
                <p>An application template can be obtained from the website www.propellaincubator.co.za or via email request to Sinazo Jack at  sinazo@propellaincubator.co.za</p>
                <p>Please note that completed applications must be submitted via email to Sinazo Jack by 12h00 on Friday, 17 September.  Short listed candidates will be required to prepare a short Zoom presentation to a selection panel during the last week of September.</p>


            </div>
            <div class="modal-footer">
                <a href="/files/TIA_Seed_Fund_Proposal_Call_Sept_2021.docx" target="_blank">
                    <img style="margin-left: 1em" class="hoverable" src="/images/word doc.png" width="80" height="100">
                </a>
            </div>
            <br>
            <br>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
            </div>
        </div>
        <br>
        <a href="/files/Tia_application.doc" target="_blank">
            <img style="margin-left: 1em" class="hoverable" src="/images/word doc.png" width="80" height="100">
        </a>
        <p>Download Application form</p>
        <br>

    </div>

    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });
    </script>
    @endsection
