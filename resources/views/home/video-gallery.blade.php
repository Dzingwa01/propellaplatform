@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Blog/blog.css"/>
    <style>
        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
            font-size: 70px;
            line-height: 1.15em;
            font-weight: 600;
        }
    </style>
    <div class="blogDesktop" >
        <div class="section">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/Website Banners.png" style="width: 100%;height: 662px;opacity: 0.2;">
                    </div>
{{--                    <div class="centered"><b>Propella Video Gallery</b></div>--}}
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 5em;margin-right: 5em;">
            @foreach($video as $videos)
                <div class="col s12 m6 l3" style="height: 250px; margin-bottom: 2em;">
                    <div style="border-style: hidden; height: 100%; width: 100%; ">
                        <iframe class="materialboxed" style="margin-left: 1em"
                                src="{{isset($videos->video)?$videos->video:''}}" width="300" height="200"
                                allow="autoplay;" allowfullscreen></iframe>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="blogMobile">
        <div class="section">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/Website Banners.png" style="width: 100%;height: 662px;opacity: 0.2;">
                    </div>
{{--                    <div class="centered"><b>Propella Video Gallery</b></div>--}}
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($video as $videos)
                <div class="col s12 m6 l3" style="height: 250px; margin-bottom: 2em;">
                    <div style="border-style: hidden; height: 100%; width: 100%; ">
                        <iframe class="materialboxed" style="margin-left: 1em"
                                src="{{isset($videos->video)?$videos->video:''}}" width="300" height="200"
                                allow="autoplay;" allowfullscreen></iframe>
                    </div>
                </div>
            @endforeach
        </div>
    </div>



    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();
        });
    </script>
@endsection
