@extends('layouts.app')

@section('content')
    <!--Quiz-->

    <div id="QuestionContent" class="app-mobile" style="margin-top: 10vh">
        <h5 class="center-align"><b>APPLICATION QUIZ</b></h5>
        <br>
        <div class="row center-align white-text" >
            <div class="col s12 QuestionOneDiv" style="background-color:rgba(0, 121, 52, 1)">
                <p>Have you been in business before (even if it was selling cookies)?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionOneBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionOneBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionTwoDiv " style="background-color: rgba(0, 47, 95, 1)" hidden>
                <p>Is your idea or product ICT smart?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionTwoBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionTwoBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionThreeDiv"style="background-color: #454242" hidden>
                <p>Do you have a working prototype?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionThreeBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionThreeBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionFourDiv"style="background-color: rgba(0, 121, 52, 1)" hidden>
                <p>Have you tested the idea or product in the market?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionFourBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionFourBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionFiveDiv"style="background-color: rgba(0, 47, 95, 1)" hidden>
                <p>Will it stand out from the crowd?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionFiveBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionFiveBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionSixDiv" style="background-color: #454242" hidden>
                <p>Will it improve the lives of those living in towns and cities?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionSixBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionSixBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionSevenDiv "style="background-color: rgba(0, 121, 52, 1)" hidden>
                <p>Can it be protected by copyright or patent?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionSevenBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionSevenBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionEightDiv"style="background-color: rgba(0, 47, 95, 1)" hidden>
                <p>Do you work well with others?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionEightBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionEightBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionNineDiv"style="background-color: #454242" hidden>
                <p>Do you listen to advice?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionNineBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionNineBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionTenDiv"style="background-color: rgba(0, 121, 52, 1)" hidden>
                <p>Are you ready to work long hours?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn QuestionTenBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn QuestionTenBtn  white black-text">No</button>
                    </div>
                </div>
            </div>
            <div class="col s12 QuestionElevenDiv"style="background-color: rgba(0, 47, 95, 1)" hidden>
                <p>Are you ready to fly through the storms that face all innovators?</p>
                <div class="row" style="margin-left: 6em">
                    <div class="col m6">
                        <button class="btn scoreBtn finalBtn QuestionElevenBtn white black-text">Yes</button>
                    </div>
                    <div class="col m6">
                        <button class="btn finalBtn QuestionElevenBtn white black-text">No</button>
                    </div>
                </div>
            </div>
            <br>

            <div class="col s12 QuestionTwelveDiv"style="background-color: #454242" hidden>
                <a class="" href="/Application-Process/question-category"><p id="HighParagraph" style=" color: white" hidden>
                        That’s amazing, it’s time to have a chat.  Please tell us more
                        <i class=""style="color: red">Click here to apply</i>
                    </p></a>
                <p id="AverageParagraph" hidden>Promising, but you may need to refine your ideas and approach a little more before sharing your idea with us. </p>
                <p id="LowParagraph" hidden>Congratulations – You have made a great start on the journey to be a smart entrepreneur. However, there’s a way to go before you’re likely to be ready for incubation. </p>
            </div>
        </div>
    </div>
        <br>
        <br>
        <br>
    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });

        //QUIZ SCRIPT
        var score = 0;

        $('#ReadyCheckBtn').on('click', function () {
            $('#QuestionContent').show();
            $('#ReadyCheckBtn').hide();
        });

        $('.scoreBtn').on('click', function () {
            score = score + 5;
        });

        $('.QuestionOneBtn').on('click', function () {
            $('.QuestionTwoDiv').show();
        });

        $('.QuestionTwoBtn').on('click', function () {
            $('.QuestionThreeDiv').show();
        });

        $('.QuestionThreeBtn').on('click', function () {
            $('.QuestionFourDiv').show();
        });

        $('.QuestionFourBtn').on('click', function () {
            $('.QuestionFiveDiv').show();
        });

        $('.QuestionFiveBtn').on('click', function () {
            $('.QuestionSixDiv').show();
        });

        $('.QuestionSixBtn').on('click', function () {
            $('.QuestionSevenDiv').show();
        });

        $('.QuestionSevenBtn').on('click', function () {
            $('.QuestionEightDiv').show();
        });

        $('.QuestionEightBtn').on('click', function () {
            $('.QuestionNineDiv').show();
        });

        $('.QuestionNineBtn').on('click', function () {
            $('.QuestionTenDiv').show();
        });

        $('.QuestionTenBtn').on('click', function () {
            $('.QuestionElevenDiv').show();
        });

        $('.QuestionElevenBtn').on('click', function () {
            $('.QuestionTwelveDiv').show();
        });

        $('.finalBtn').on('click', function () {
            console.log(score);

            if (score >= 45) {
                $('#HighParagraph').show();
            } else if ((score <35)&& (score >=35)){
                $('#AverageParagraph').show();
            } else if (score < 35) {
                $('#LowParagraph').show();
            }
        });

    </script>
    @endsection
