@extends('layouts.app')

@section('content')
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-179203712-1');
        </script>

    </head>

    <body>

    <div class="row" style="text-align: center; margin-left: 3em; margin-right: 3em;top: 10vh">
        @foreach ($gallery as $galleries)
            <div class="col s12 m3" style="margin-top: 10vh">
                <div class="card white hoverable" style="border-radius: 20px;">
                    <div class="circle">
                        <div class="card-content black-text" style="height: 100%;">
                            <div class="user-view" align="center">
                                <a href="#user">
                                    <img class="fullscreen" style="height: 100px;width: 250px;"
                                         src="{{isset($galleries->single_image)?'/storage/'.$galleries->single_image:''}}">
                                </a>
                            </div>
                            <h6 class="top-description">{{$galleries->description}}</h6>
                            <h6 class="date">{{$galleries->date_added}}</h6>
                        </div>
                    </div>
                    <div class="card-action" style="border-radius: 20px;">
                        <div class="view_media" id="photo"
                             style="width:100%; border-radius: 10px; margin-top: 5px;">
                            <a style="color: orange;"
                               href="{{url('/gallery/'.$galleries->id)}}"
                               onclick="viewMedia(this)">View Gallery</a>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>



    <script>

        function viewMedia(obj) {
            let gallery_id = obj.getAttribute('data-value');

            location.href = '/gallery/' + gallery_id;
        }
        $('.top-description').each(function () {
            var max_length = 25;

            if ($(this).html().length > max_length) {

                var short_content = $(this).html().substr(0, max_length);
                var long_content = $(this).html().substr(max_length);

                $(this).html(short_content +
                    '<br><br><i href="#" class="more_horiz material-icons small">more_horiz</i>' +

                    '<span class="more_text" style="display:none;">' + long_content + '</span>');

            }

        });
        $('.date').each(function () {
            var max_length = 10;

            if ($(this).html().length > max_length) {

                var short_content = $(this).html().substr(0, max_length);
                var long_content = $(this).html().substr(max_length);

                $(this).html(short_content +
                    '<i href="#" class="more_horiz material-icons small"></i>' +

                    '<span class="more_text" style="display:none;">' + long_content + '</span>');

            }

        });
    </script>

    </body>
    @endsection
