@extends('layouts.app')

@section('content')
    <br>
    <br>
    <br>
    <br>
    <br>
<div class="container" style="margin-right: 550px; margin-left: 500px;">
    <div class="cogwheel cw-1">
        <span></span>
    </div>
    <div class="cogwheel cw-2">
        <span></span>
    </div>
    <div class="cogwheel cw-3">
        <span></span>
    </div>
</div>
    <br>
    <br>
    <br>
    <br>
    <br>

    <style>
        body {
            /*background-color: #44403f;*/
        }
        h1 {
            font-family: helvetica;
            color: #FEFAFA;
            font-weight: normal;
            text-align: center;
            margin: 2rem 0;
        }
        section {
            width: 300px;
            margin: 0 auto;
            text-align: center;
            padding: 0 3.5rem;
        }
        .cogwheel {
            display: inline-block;
            border-radius: 100%;
            box-sizing: border-box;
            position: relative;
            text-align: center;
            z-index: 1;
            width: 100px;
            height: 100px;
            -webkit-animation: spin-to-r 3s infinite linear;
            -moz-animation: spin-to-r 3s infinite linear;
            -o-animation: spin-to-r 3s infinite linear;
            animation: spin-to-r 3s infinite linear;
        }
        .cogwheel:before {
            content: '';
            position: absolute;
            box-sizing: border-box;
            border-radius: 100%;
            left: -15px;
            top: -15px;
            width: 110px;
            height: 110px;
        }
        .cogwheel:after {
            content: '+';
            position: absolute;
            /* font-weight: bold; */
            font-family: arial;
            box-sizing: border-box;
            z-index: 0;
            left: -0.95rem;
            top: -3.8rem;
            font-size: 11rem;
            width: 110px;
            height: 110px;
        }
        .cogwheel span {
            border-radius: 100%;
            display: inline-block;
            background-color: #44403f;
            position: relative;
            z-index: 10;
            width: 20px;
            height: 20px;
            margin-top: 1.5rem;
        }
        .cw-1 {
            color: #E75152;
            border: 10px solid #E75152;
            vertical-align: 50px;
            margin-right: -2px;
        }
        .cw-1:before {
            border: 6px dashed #E75152;
        }
        .cw-1 span {
            border: 5px solid #E75152;
        }
        .cw-2 {
            color: #F5B9BD;
            border: 10px solid #F5B9BD;
            -webkit-animation: spin-to-l 3s infinite linear;
            -moz-animation: spin-to-l 3s infinite linear;
            -o-animation: spin-to-l 3s infinite linear;
            animation: spin-to-l 3s infinite linear;
            vertical-align: 20px;
        }
        .cw-2:before {
            border: 6px dashed #F5B9BD;
        }
        .cw-2 span {
            border: 5px solid #F5B9BD;
        }
        .cw-3 {
            vertical-align: 50px;
            color: #9F9E9E;
            margin-top: -7px;
            border: 10px solid #9F9E9E;
        }
        .cw-3:before {
            border: 6px dashed #9F9E9E;
        }
        .cw-3 span {
            border: 5px solid #9F9E9E;
        }
        @-webkit-keyframes spin-to-r {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
        @-moz-keyframes spin-to-r {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
        @-o-keyframes spin-to-r {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
        @keyframes spin-to-r {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
        @-webkit-keyframes spin-to-l {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
        @-moz-keyframes spin-to-l {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
        @-o-keyframes spin-to-l {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
        @keyframes spin-to-l {
            from {
                transform: rotate(360deg);
            }
            to {
                transform: rotate(0deg);
            }
        }

    </style>
@endsection
