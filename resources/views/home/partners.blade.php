@extends('layouts.app')

@section('content')
<link type="text/css" rel="stylesheet" href="/css/Partners/PartnerMain.css" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179203712-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-179203712-1');
    </script>

</head>

<body>

<div class="section  container-fluid">
    <div class="row">
        <div class="parallax-container" id="PartnerTopSection">
            <div class="parallax">
                <img src="/images/PartnerPageImages/3 Partners.jpg" id="PartnerTopSectionImage">
            </div>
            <h1 class="partner-section-headers">Partner with Propella</h1>
            <h1 id="DesktopButtons"></h1>
        </div>
    </div>
</div>

<!--First Section Desktop-->
<div class="section standard-section-with-margins" id="PartnerSecondSectionDesktop">
    <div class="row">
        <div class="col l5 left-align">
            <h4 class="standard-header">Global Competitive Advantage</h4>
            <p class="standard-paragraphs">
                Providing support for innovators with disruptive technology which
                gives manufacturing in the Eastern Cape and the rest of South Africa a
                competitive global advantage.
            </p>

            <ul>
                <li>
                    <p class="standard-paragraphs">• Propella is a partnership between the Nelson Mandela Metropolitan
                        University, the Industrial Development Corporation and the private sector.

                    </p>
                </li>
                <li>
                    <p class="standard-paragraphs">• Our main areas of focus are on renewable energy generation, energy
                        efficiency and related technologies, advanced manufacturing and supply
                        chain optimisation.

                    </p>
                </li>
            </ul>

            <h4 class="standard-header">Who We Incubate</h4>
            <p class="standard-paragraphs">A business or a business idea has been tested in the market and is:</p>
            <ul>
                <li>
                    <p class="standard-paragraphs">•	Disruptive (Smart, out of the ordinary thinking, unique, innovative)</p>
                </li>
                <li>
                    <p class="standard-paragraphs">•	Will help a smart city to improve the lives of its residents  </p>
                </li>
                <li>
                    <p class="standard-paragraphs">• Scalable</p>
                </li>
                <li>
                    <p class="standard-paragraphs">• Sustainable</p>
                </li>
                <li>
                    <p class="standard-paragraphs">• Creates jobs or has the ability to create jobs</p>
                </li>
            </ul>
        </div>

        <div class="col l1"></div>


        <div class="col l6 center-align">
            <div class="row">
                <div class="standard-blocks col l4 BBBEBlock">
                    <h6 class="standard-block-headers">  Maximise your <br /> B-BBEE</h6>
                </div>
                <div class="standard-blocks col l4 InvestmentBlock">
                    <h6 class="standard-block-headers">ESD Funding Options </h6>
                </div>
                <div class="standard-blocks col l4 MaximiseBlock">
                    <h6 class="standard-block-headers">Enterprise and Supplier Development Solutions </h6>
                </div>
            </div>

            <div class="row">
                <div class="standard-blocks col l4 IPBlock">
                    <h6 class="standard-block-headers">Equity Opportunity</h6>
                </div>
                <div class="standard-blocks col l4 TalkBlock2">
                    <h6 class="standard-block-headers">IoT</h6>
                </div>
                <div class="standard-blocks col l4 EmptyBlock">
                    <h6 class="standard-block-headers">  Commercialise your Idea</h6>
                </div>
            </div>
            <div class="row">
                <div class="standard-blocks col l4 ProjectsBlock">
                    <h6 class="standard-block-headers"> Propella Projects </h6>
                </div>
                <div class="standard-blocks col l4 BlockEmpty">
                    <h6 class="standard-block-headers"></h6>
                </div>
                <div class="standard-blocks col l4 TalkBlock">
                    <h6 class="standard-block-headers"> Let's Talk </h6>
                </div >
            </div>
        </div>
    </div>
</div>

<!--First section Mobile-->
<div class="section standard-section-with-margins" id="PartnerSecondSectionMobile">
    <div class="row text-justify">
        <h4 class="standard-header">Global Competitive Advantage</h4>
        <p class="standard-paragraphs">
            Providing support for innovators with disruptive technology which
            gives manufacturing in the Eastern Cape and the rest of South Africa a
            competitive global advantage.
        </p>

        <ul>
            <li>
                <p class="standard-paragraphs">• Propella is a partnership between the Nelson Mandela Metropolitan
                    University, the Industrial Development Corporation and the private sector.

                </p>
            </li>
            <li>
                <p class="standard-paragraphs">• Our main areas of focus are on renewable energy generation, energy
                    efficiency and related technologies, advanced manufacturing and supply
                    chain optimisation.

                </p>
            </li>
        </ul>

        <h4 class="standard-header" >Who We Incubate</h4>
        <p class="standard-paragraphs">A business or a business idea has been tested in the market and is:</p>
        <ul>
            <li>
                <p class="standard-paragraphs">•	Disruptive (Smart, out of the ordinary thinking, unique, innovative)</p>
            </li>
            <li>
                <p class="standard-paragraphs">•	Will help a smart city to improve the lives of its residents  </p>
            </li>
            <li>
                <p class="standard-paragraphs" id="FirstButtonsMobile">• Scalable</p>
            </li>
            <li>
                <p class="standard-paragraphs">• Sustainable</p>
            </li>
            <li >
                <p class="standard-paragraphs">• Creates jobs or has the ability to create jobs</p>
            </li>
        </ul>
    </div>

    <div class="row center-align" >
        <div class="row">
            <div class="standard-blocks col s6 BBBEBlock">
                <h6 class="standard-block-headers">  Maximise your B-BBEE</h6>
            </div>
            <div class="standard-blocks col s6 MaximiseBlock">
                <h5 class="standard-block-headers">Enterprise and Supplier development Solution </h5>
            </div>
        </div>

        <div class="row">
            <div class="standard-blocks col s6 TalkBlock2">
                <h6 class="standard-block-headers">IoT</h6>
            </div>
            <div class="standard-blocks col s6 InvestmentBlock">
                <h5 class="standard-block-headers">ESD Funding Option  </h5>
            </div>

        </div>
        <div class="row">
            <div class="standard-blocks col s6 IPBlock">
                <h5 class="standard-block-headers"> Equity Opportunity </h5>
            </div>
            <div class="standard-blocks col s6 ProjectsBlock">
                <h5 class="standard-block-headers">Propella Projects</h5>
            </div>
        <div class="row">
            <div class="standard-blocks col s6 TalkBlock">
                <h5 class="standard-block-headers"> Let's Talk</h5>
            </div>
            <div class="standard-blocks col s6 EmptyBlock">
                <h5 class="standard-block-headers">Commercialise your Idea  </h5>
            </div>
        </div>
        </div>
    </div>
</div>

<!--Image Header 2-->
<div class=""id="partner1"></div>
<div id="desktop">
<br />
<br />
<br />
<br />
<div class="section ">
    <div class="row">
        <div class="parallax-container" id="PartnerThirdSection">
            <div class="parallax">
                <img src="/images/Propella_resized_images/redbullNew.jpg" id="PartnerThirdSectionImage">
            </div>
            <h1 class="partner-section-header2">Maximise your B-BBEE</h1>
        </div>
    </div>
</div>
</div>

<div id="mobile">
    <div class="section ">
        <div class="row">
            <div id="PartnerThirdSection">
                <div>
                    <img src="/images/Propella_resized_images/redbullNew.jpg" id="PartnerThirdSectionImage">
                </div>
                <h1 class="partner-section-header2">Maximise your B-BBEE</h1>
            </div>
        </div>
    </div>
</div>

<!--Second sectionDesktop-->
<div class="section standard-section-with-margins" id="PartnerFourthSectionDesktop">
    <div class="row">
        <div class="col s4 left-align">
            <div class="standard-left-buttons yourNeedBlock">
                <h6 class="standard-left-button-headers">Your Needs </h6>
            </div>
            <div class="standard-left-buttons BenefitBlock">
                <h6 class="standard-left-button-headers"> Your Solutions</h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier Development Solutions  </h6>
            </div>
            <div class="standard-left-buttons FundingBlock">
                <h6 class="standard-left-button-headers"> Funding Options</h6>
            </div>

        </div>


        <div class="col s1"></div>

        <div class="col s7 left-align"id="DesktopMaximiseBBBEE">
            <!--Enterprise and Supplier development Solution -->
           <div class="Maximise">
               <p class="standard-paragraphs">Propella allows corporate and business partners to maximise their BBBEE
                   scorecard, and to add value to their own operations.
               </p>
               <p class="standard-paragraphs"style="color:blue;font-style: italic;">Click on the left buttons to view more</p>
           </div>
            <!--Your need-->
            <div class="yourNeed"hidden>
                <h4 class="standard-header">Your Needs</h4>
                <p class="standard-paragraphs">Spend 5% Net Profit After Tax (NPAT) for up to 25 points on B-BBEE scorecard on the Enterprise
                    & Supplier Development (ESD) line </p>
                <p class="standard-paragraphs"><b>Enterprise Development:</b> </p>
                <p class="standard-paragraphs">3% NPAT for ICT company = 15 points</p>
                <p class="standard-paragraphs">1% NPAT for all other companies = 5 points</p>
                <p class="standard-paragraphs"><b>Supplier Development:</b> </p>
                <p class="standard-paragraphs">2% NPAT for all companies = 10 points </p>
                <p class="standard-paragraphs">Potential beneficiary companies of this ESD contribution must be 51% Black Owned with an annual
                    turnover below R50 million </p>
                <p class="standard-paragraphs"><b>Our Solution</b></p>
                <p class="standard-paragraphs">Propella is a 51% Black Owned ICT company* which has a turnover below </p>
                <p class="standard-paragraphs">R50 million/year and qualifies for:</p>
                <p class="standard-paragraphs"><b>Enterprise Development contributions</b></p>
                <p class="standard-paragraphs">3% of NPAT (ICT) to achieve 15 points or 1% NPAT for all other companies to achieve 5 points</p>
                <p class="standard-paragraphs">OR</p>
                <p class="standard-paragraphs"><b>Supplier Development Contributions</b></p>
                <p class="standard-paragraphs"> 2% of NPAT to achieve 10 points</p>
                <p class="standard-paragraphs">Propella is classified as a Level 2 ICT company due to our core business in the development of ICT companies. Propella is
                    also a qualifying ESD beneficiary for the General Codes.</p>
            </div>
            <!--Your solution/Benefit-->
            <div class="yourSolution"hidden>
                <h4 class="standard-header">Your Solutions</h4>
                <p class="standard-paragraphs">Money currently budgeted and spent on adhering to B-BBEE Codes of Good Practice used to:</p>
                <p class="standard-paragraphs">•	Monetise your IP</p>
                <p class="standard-paragraphs">•	For Investment Opportunities</p>
                <p class="standard-paragraphs">•	Stimulate Innovation</p>
                <p class="standard-paragraphs">•	Develop your Supply Chain</p>
                <p class="standard-paragraphs">•	Create Jobs </p>
                <p class="standard-paragraphs">•	Meet Client Requirements</p>
                <p class="standard-paragraphs">•	Fill Skills Gaps</p>
                <p class="standard-paragraphs">•	Enhanced ROI (B-BBEE spend)</p>
                <p class="standard-paragraphs">Provide a bonus point if an enterprise development beneficiary graduates to become a supplier
                    to your organisation.</p>
                <p class="standard-paragraphs"><b>Our Benefits</b></p>
                <p class="standard-paragraphs">Provide support to South African innovators or ventures developing technologies with commercial value that are:</p>
                <p class="standard-paragraphs">•	Appropriate</p>
                <p class="standard-paragraphs">•	Innovative</p>
                <p class="standard-paragraphs">•	Disruptive</p>
                <p class="standard-paragraphs">•	Sector specific (ICT, IoT, Renewable Energy, Energy Efficiency, Industry 4.0, Smart City) </p>
                <p class="standard-paragraphs"><b>Supplier Development</b></p>
                <p class="standard-paragraphs">As a supplier to your organisation, through: </p>
                <p class="standard-paragraphs">•	Technology transfer and development </p>
                <p class="standard-paragraphs">•	Provision of support for the successful development of sustainable, new suppliers </p>
            </div>

        </div>
    </div>
</div>

<!--Second section Mobile-->
<div class="section standard-section-with-margins" id="PartnerFourthSectionMobile">
    <div class="row center-align">
        <!--Maximise you B-BBEE -->
        <div class="Maximise">
            <p class="standard-paragraphs">Propella is an ESD Beneficiary in its own right.  Enterprise and Supplier Development solution is
                tailor-made to cater to businesses in both the ICT and General B-BBEE codes looking to improve their Enterprise and
                Supplier Development contributions in a more financially efficient manner. </p>
        </div>
        <div class="row">
            <div class="standard-left-buttons BenefitBlock">
                <h6 class="standard-left-button-headers"> Your Solution</h6>
            </div>
            <div class="standard-left-buttons yourNeedBlock">
                <h6 class="standard-left-button-headers">Your Need </h6>
            </div>
        </div>

        <div class="row">
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier development Solutions </h6>
            </div>
            <div class="standard-left-buttons FundingBlock">
                <h6 class="standard-left-button-headers"> Funding Options</h6>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="row left-align">

            <!--Your need-->
            <div class="yourNeed"hidden>
                <h4 class="standard-header">Your Need</h4>
                <p class="standard-paragraphs">Spend 5% Net Profit After Tax (NPAT) for up to 25 points on B-BBEE scorecard on the Enterprise
                    & Supplier Development (ESD) line </p>
                <p class="standard-paragraphs"><b>Enterprise Development:</b> </p>
                <p class="standard-paragraphs">3% NPAT for ICT company = 15 points</p>
                <p class="standard-paragraphs">1% NPAT for all other companies = 5 points</p>
                <p class="standard-paragraphs"><b>Supplier Development:</b> </p>
                <p class="standard-paragraphs">2% NPAT for all companies = 10 points </p>
                <p class="standard-paragraphs">Potential beneficiary companies of this ESD contribution must be 51% Black Owned with an annual
                    turnover below R50 million </p>
                <p class="standard-paragraphs"><b>Our Solution</b></p>
                <p class="standard-paragraphs">Propella is a 51% Black Owned ICT company* which has a turnover below </p>
                <p class="standard-paragraphs">R50 million/year and qualifies for:</p>
                <p class="standard-paragraphs"><b>Enterprise Development contributions</b></p>
                <p class="standard-paragraphs">3% of NPAT (ICT) to achieve 15 points or 1% NPAT for all other companies to achieve 5 points</p>
                <p class="standard-paragraphs">OR</p>
                <p class="standard-paragraphs"><b>Supplier Development Contributions</b></p>
                <p class="standard-paragraphs"> 2% of NPAT to achieve 10 points</p>
                <p class="standard-paragraphs">Propella is classified as a Level 2 ICT company due to our core business in the development of ICT companies. Propella is
                    also a qualifying ESD beneficiary for the General Codes.</p>
            </div>
            <!--Your solution/Benefit-->
            <div class="yourSolution"hidden>
                <h4 class="standard-header">Your Benefits</h4>
                <p class="standard-paragraphs">Money currently budgeted and spent on adhering to B-BBEE Codes of Good Practice used to:</p>
                <p class="standard-paragraphs">•	Monetise your IP</p>
                <p class="standard-paragraphs">•	For Investment Opportunities</p>
                <p class="standard-paragraphs">•	Stimulate Innovation</p>
                <p class="standard-paragraphs">•	Develop your Supply Chain</p>
                <p class="standard-paragraphs">•	Create Jobs </p>
                <p class="standard-paragraphs">•	Meet Client Requirements</p>
                <p class="standard-paragraphs">•	Fill Skills Gaps</p>
                <p class="standard-paragraphs">•	Enhanced ROI (B-BBEE spend)</p>
                <p class="standard-paragraphs">Provide a bonus point if an enterprise development beneficiary graduates to become a supplier
                    to your organisation.</p>
                <p class="standard-paragraphs"><b>Our Benefits</b></p>
                <p class="standard-paragraphs">Provide support to South African innovators or ventures developing technologies with commercial value that are:</p>
                <p class="standard-paragraphs">•	Appropriate</p>
                <p class="standard-paragraphs">•	Innovative</p>
                <p class="standard-paragraphs">•	Disruptive</p>
                <p class="standard-paragraphs">•	Sector specific (ICT, IoT, Renewable Energy, Energy Efficiency, Industry 4.0, Smart City) </p>
                <p class="standard-paragraphs"><b>Supplier Development</b></p>
                <p class="standard-paragraphs">As a supplier to your organisation, through: </p>
                <p class="standard-paragraphs">•	Technology transfer and development </p>
                <p class="standard-paragraphs">•	Provision of support for the successful development of sustainable, new suppliers </p>
            </div>
            <!--Monetise your IP-->
            <div class="monetiseIP"hidden>
                <h4 class="standard-header">Benefit to your company</h4>
                <p class="standard-paragraphs">Propella will help you to bring your good idea for a new product or IT application to market. </p>
                <p class="standard-paragraphs">Virtual incubation at your factory / offices.</p>
                <p class="standard-paragraphs">Your developers work out of the Propella Innovation Hub, where they have access to resources like manufacturing
                    equipment for fast prototyping and super-fast internet.</p>
                <p class="standard-paragraphs">Propella uses its own team to help you monetize your intellectual property</p>
                <p class="standard-paragraphs"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Ability to partner with like-minded businesses to create appropriate technology-based solutions</p>
                <p class="standard-paragraphs">Act as a bridge between industry and academia </p>
                <p class="standard-paragraphs">Ready market for the product, application or platform </p>
            </div>
            <!--iNVEST OPP-->
            <div class="InvestOpp"hidden="">
                <h4 class="standard-header">Benefit to your company</h4>
                <p class="standard-paragraphs">Propella has carefully selected its incubatees and participants for their commercialisation potential</p>
                <p class="standard-paragraphs">They may well be working on a product or application that will give your business a competitive edge, or save you time
                    and money on in-house development costs and provide you with an equity opportunity</p>
                <p class="standard-paragraphs"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Access to funding is a critical component to commercialising a sustainable start-up venture.   </p>
                <p class="standard-paragraphs">Providing our ventures with access to finance shortens the incubation or acceleration period</p>
            </div>
            <!--New Innovation-->
            <div class="newInnovation"hidden>
                <h4 class="standard-header">Benefit to your company</h4>
                <p class="standard-paragraphs">To explore opportunities arising from New Ideas in a safe environment for trials or testing</p>
                <p class="standard-paragraphs">Research & Development capabilities</p>
                <p class="standard-paragraphs">Exploring new innovations/ ideas/concepts in your Industry enabling you to be at the forefront     of your competitors</p>
                <p class="standard-paragraphs"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Showcase new technologies and appropriate ideas from ventures currently in incubation / acceleration within Propella
                    who have ready made  “Go to Market” opportunities</p>
            </div>
            <!--Supply chain-->
            <div class="supplyChain"hidden>
                <h4 class="standard-header">Benefit to your company</h4>
                <p class="standard-paragraphs">Determine what requirements, programs and training are needed to allow prospective ventures to become certified suppliers
                    (ensure compliance with your existing process)</p>
                <p class="standard-paragraphs">Enterprise development spend on a venture that converts into a SUPPLIER gains you A BONUS POINT. </p>
                <p class="standard-paragraphs"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Understand the gaps within your organisations supply chain to identify and incubate or accelerate a potential match </p>
                <p class="standard-paragraphs">Ultimate success story of a commercialised venture with guaranteed access to market </p>
            </div>
            <!--Job Creation-->
            <div class="jobCreation"hidden>
                <h4 class="standard-header">Benefit to your company</h4>
                <p class="standard-paragraphs">Facilitate establishment and growth of new ventures to create additional employment</p>
                <p class="standard-paragraphs">Have your company associated with ground breaking new ground breaking technology leading to meaningful employment
                    opportunities or spin-off start-up ventures</p>
                <p class="standard-paragraphs"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">We receive ”base” funding to undertake developing technologies that creates sustainable jobs</p>
                <p class="standard-paragraphs">We have the pleasure of “releasing” industry resources into the market should any ventures fail</p>
            </div>
            <!--Client requirements-->
            <div class="clientReq"hidden>
                <h4 class="standard-header">Benefit to your company</h4>
                <p class="standard-paragraphs">Propella could serve as an Alternative Supplier (procurement requirements) as prescribed by your clients</p>
                <p class="standard-paragraphs"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Propella, individual resources from Propella or start-up ventures with desirable credentials can fulfil this demand as an approved supplier to your company  </p>
            </div>
            <!--Skills Gap-->
            <div class="skillsGap"hidden>
                <h4 class="standard-header">Benefit to your company</h4>
                <p class="standard-paragraphs">Where new skills are required by your business:</p>
                <p class="standard-paragraphs">•	Augment skills shortages</p>
                <p class="standard-paragraphs">•	Short term development support through our incubatees or interns</p>
                <p class="standard-paragraphs">•	30% subcontracting requirement for Public Sector</p>
                <p class="standard-paragraphs"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Through training Propella can create these skills based on client demand (eg Power BI competencies), and benefit while
                    utilising the resources on internal projects during their training period </p>
            </div>
        </div>
    </div>
</div>

<!--Enterprise and supply development solution Desktop-->
<div class=""id="enterprise"></div>
<div id="desktop">
<br />
<br />
<br />
<br />
<div class="section ">
    <div class="row">
        <div class="parallax-container" id="EnterpriceThirdSection">
            <div class="parallax">
                <img src="/images/PartnerPageImages/ESD solutions.JPG" id="EnterpriceImage">
            </div>
            <h1 class="enterprise-header2">Enterprise and Supplier Development Solutions</h1>
        </div>
    </div>
</div>
</div>

<div id="mobile">
    <div class="section">
        <div class="row">
            <div id="EnterpriceThirdSection">
                <div>
                    <img src="/images/PartnerPageImages/ESD solutions.JPG" id="EnterpriceImage">
                </div>
                <h1 class="enterprise-header2">Enterprise and Supplier Development Solutions</h1>
            </div>
        </div>
    </div>
</div>
<!--Enterprise and supply development solution Desktop-->
<div class="section standard-section-with-margins" id="EnterpriseSectionDesktop">
    <div class="row">
        <div class="col s4 left-align">
            <div class="standard-left-buttons IBlock">
                <h6 class="standard-left-button-headers"> Monetize Your Intellectual Property </h6>
            </div>
            <div class="standard-left-buttons InvestBlock">
                <h6 class="standard-left-button-headers">Investment Opportunity </h6>
            </div>
            <div class="standard-left-buttons INNOVBlock">
                <h6 class="standard-left-button-headers"> New Innovation</h6>
            </div>
            <div class="standard-left-buttons SuppChainBlock">
                <h6 class="standard-left-button-headers"> Supply Chain </h6>
            </div>
            <div class="standard-left-buttons JobBlock">
                <h6 class="standard-left-button-headers"> Job Creation </h6>
            </div>
            <div class="standard-left-buttons ClientBlock">
                <h6 class="standard-left-button-headers"> Client Requirements </h6>
            </div>
            <div class="standard-left-buttons SkillsBlock">
                <h6 class="standard-left-button-headers"> Skills Gap </h6>
            </div>
        </div>
            <div class="col s1"></div>

            <div class="col s7 left-align"id="DesktopEnterprise">
                <!--Enterprise and Supplier development Solution -->
                <div class="Enterprise">
                    <p class="standard-paragraphs">Propella is an ESD Beneficiary in its own right.  Enterprise and Supplier Development solution is
                        tailor-made to cater to businesses in both the ICT and General B-BBEE codes looking to improve their Enterprise and
                        Supplier Development contributions in a more financially efficient manner. </p>
                </div>
                <!--Monetise your IP-->
                <div class="monetiseIP"hidden>
                    <h4 class="standard-header">Monetize your Intellectual Property</h4>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                    <p class="standard-paragraphs">Propella will help you to bring your good idea for a new product or IT application to market. </p>
                    <p class="standard-paragraphs">Virtual incubation at your factory / offices.</p>
                    <p class="standard-paragraphs">Your developers work out of the Propella Innovation Hub, where they have access to resources like manufacturing
                        equipment for fast prototyping and super-fast internet.</p>
                    <p class="standard-paragraphs">Propella uses its own team to help you monetize your intellectual property</p>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                    <p class="standard-paragraphs">Ability to partner with like-minded businesses to create appropriate technology-based solutions</p>
                    <p class="standard-paragraphs">Act as a bridge between industry and academia </p>
                    <p class="standard-paragraphs">Ready market for the product, application or platform </p>
                    <br />

                </div>
                <!--iNVEST OPP-->
                <div class="InvestOpp"hidden="">
                    <h4 class="standard-header">Investment Opportunity</h4>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                    <p class="standard-paragraphs">Propella has carefully selected its incubatees and participants for their commercialisation potential</p>
                    <p class="standard-paragraphs">They may well be working on a product or application that will give your business a competitive edge, or save you time
                        and money on in-house development costs and provide you with an equity opportunity</p>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                    <p class="standard-paragraphs">Access to funding is a critical component to commercialising a sustainable start-up venture.   </p>
                    <p class="standard-paragraphs">Providing our ventures with access to finance shortens the incubation or acceleration period</p>
                    <br />

                </div>
                <!--New Innovation-->
                <div class="newInnovation"hidden>
                    <h4 class="standard-header">New Innovation</h4>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                    <p class="standard-paragraphs">To explore opportunities arising from New Ideas in a safe environment for trials or testing</p>
                    <p class="standard-paragraphs">Research & Development capabilities</p>
                    <p class="standard-paragraphs">Exploring new innovations/ ideas/concepts in your Industry enabling you to be at the forefront     of your competitors</p>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                    <p class="standard-paragraphs">Showcase new technologies and appropriate ideas from ventures currently in incubation / acceleration within Propella
                        who have ready made  “Go to Market” opportunities</p>
                    <br />

                </div>
                <!--Supply chain-->
                <div class="supplyChain"hidden>
                    <h4 class="standard-header">Supply chain</h4>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                    <p class="standard-paragraphs">Determine what requirements, programs and training are needed to allow prospective ventures to become certified suppliers
                        (ensure compliance with your existing process)</p>
                    <p class="standard-paragraphs">Enterprise development spend on a venture that converts into a SUPPLIER gains you A BONUS POINT. </p>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                    <p class="standard-paragraphs">Understand the gaps within your organisations supply chain to identify and incubate or accelerate a potential match </p>
                    <p class="standard-paragraphs">Ultimate success story of a commercialised venture with guaranteed access to market </p>
                    <br />

                </div>
                <!--Job Creation-->
                <div class="jobCreation"hidden>
                    <h4 class="standard-header">Job Creation</h4>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                    <p class="standard-paragraphs">Facilitate establishment and growth of new ventures to create additional employment</p>
                    <p class="standard-paragraphs">Have your company associated with ground breaking new ground breaking technology leading to meaningful employment
                        opportunities or spin-off start-up ventures</p>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                    <p class="standard-paragraphs">We receive ”base” funding to undertake developing technologies that creates sustainable jobs</p>
                    <p class="standard-paragraphs">We have the pleasure of “releasing” industry resources into the market should any ventures fail</p>
                    <br />

                </div>
                <!--Client requirements-->
                <div class="clientReq"hidden>
                    <h4 class="standard-header">Client Requirements</h4>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                    <p class="standard-paragraphs">Propella could serve as an Alternative Supplier (procurement requirements) as prescribed by your clients</p>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                    <p class="standard-paragraphs">Propella, individual resources from Propella or start-up ventures with desirable credentials can fulfil this demand as an approved supplier to your company  </p>
                    <br />

                </div>
                <!--Skills Gap-->
                <div class="skillsGap"hidden>
                    <h4 class="standard-header">Benefit to your company</h4>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                    <p class="standard-paragraphs">Where new skills are required by your business:</p>
                    <p class="standard-paragraphs">•	Augment skills shortages</p>
                    <p class="standard-paragraphs">•	Short term development support through our incubatees or interns</p>
                    <p class="standard-paragraphs">•	30% subcontracting requirement for Public Sector</p>
                    <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                    <p class="standard-paragraphs">Through training Propella can create these skills based on client demand (eg Power BI competencies), and benefit while
                        utilising the resources on internal projects during their training period </p>
                    <br />

                </div>
            </div>
    </div>
</div>

<!--Enterprise and supply development solution Mobile-->
<div class="section standard-section-with-margins" id="EnterpriseSectionMobile">
    <div class="row center-align">
        <!--Enterprise and Supplier development Solution -->
        <div class="Enterprise">
            <p class="standard-paragraphs">Propella is an ESD Beneficiary in its own right.  Enterprise and Supplier Development solution is
                tailor-made to cater to businesses in both the ICT and General B-BBEE codes looking to improve their Enterprise and
                Supplier Development contributions in a more financially efficient manner. </p>
        </div>
            <div class="standard-left-buttons IBlock">
                <h6 class="standard-left-button-headers"> Monetize Your Intellectual Property </h6>
            </div>
            <div class="standard-left-buttons InvestBlock">
                <h6 class="standard-left-button-headers">Investment Opportunity </h6>
            </div>
            <div class="standard-left-buttons INNOVBlock">
                <h6 class="standard-left-button-headers"> New Innovation</h6>
            </div>
            <div class="standard-left-buttons SuppChainBlock">
                <h6 class="standard-left-button-headers"> Supply Chain </h6>
            </div>
            <div class="standard-left-buttons JobBlock">
                <h6 class="standard-left-button-headers"> Job Creation </h6>
            </div>
            <div class="standard-left-buttons ClientBlock">
                <h6 class="standard-left-button-headers"> Client Requirements </h6>
            </div>
            <div class="standard-left-buttons SkillsBlock">
                <h6 class="standard-left-button-headers"> Skills Gap </h6>
            </div>


        <div class="row"id="DesktopEnterprise">

            <!--Monetise your IP-->
            <div class="monetiseIP "hidden>
                <h4 class="standard-header">Monetise your Intellectual Property</h4>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                <p class="standard-paragraphs">Propella will help you to bring your good idea for a new product or IT application to market. </p>
                <p class="standard-paragraphs">Virtual incubation at your factory / offices.</p>
                <p class="standard-paragraphs">Your developers work out of the Propella Innovation Hub, where they have access to resources like manufacturing
                    equipment for fast prototyping and super-fast internet.</p>
                <p class="standard-paragraphs">Propella uses its own team to help you monetize your intellectual property</p>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Ability to partner with like-minded businesses to create appropriate technology-based solutions</p>
                <p class="standard-paragraphs">Act as a bridge between industry and academia </p>
                <p class="standard-paragraphs">Ready market for the product, application or platform </p>
            </div>
            <!--iNVEST OPP-->
            <div class="InvestOpp"hidden="">
                <h4 class="standard-header">Investment Opportunity</h4>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                <p class="standard-paragraphs">Propella has carefully selected its incubatees and participants for their commercialisation potential</p>
                <p class="standard-paragraphs">They may well be working on a product or application that will give your business a competitive edge, or save you time
                    and money on in-house development costs and provide you with an equity opportunity</p>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Access to funding is a critical component to commercialising a sustainable start-up venture.   </p>
                <p class="standard-paragraphs">Providing our ventures with access to finance shortens the incubation or acceleration period</p>
            </div>
            <!--New Innovation-->
            <div class="newInnovation"hidden>
                <h4 class="standard-header">New Innovation</h4>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                <p class="standard-paragraphs">To explore opportunities arising from New Ideas in a safe environment for trials or testing</p>
                <p class="standard-paragraphs">Research & Development capabilities</p>
                <p class="standard-paragraphs">Exploring new innovations/ ideas/concepts in your Industry enabling you to be at the forefront     of your competitors</p>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Showcase new technologies and appropriate ideas from ventures currently in incubation / acceleration within Propella
                    who have ready made  “Go to Market” opportunities</p>
            </div>
            <!--Supply chain-->
            <div class="supplyChain"hidden>
                <h4 class="standard-header">Supply chain</h4>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                <p class="standard-paragraphs">Determine what requirements, programs and training are needed to allow prospective ventures to become certified suppliers
                    (ensure compliance with your existing process)</p>
                <p class="standard-paragraphs">Enterprise development spend on a venture that converts into a SUPPLIER gains you A BONUS POINT. </p>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Understand the gaps within your organisations supply chain to identify and incubate or accelerate a potential match </p>
                <p class="standard-paragraphs">Ultimate success story of a commercialised venture with guaranteed access to market </p>
            </div>
            <!--Job Creation-->
            <div class="jobCreation"hidden>
                <h4 class="standard-header">Job Creation</h4>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                <p class="standard-paragraphs">Facilitate establishment and growth of new ventures to create additional employment</p>
                <p class="standard-paragraphs">Have your company associated with ground breaking new ground breaking technology leading to meaningful employment
                    opportunities or spin-off start-up ventures</p>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">We receive ”base” funding to undertake developing technologies that creates sustainable jobs</p>
                <p class="standard-paragraphs">We have the pleasure of “releasing” industry resources into the market should any ventures fail</p>
            </div>
            <!--Client requirements-->
            <div class="clientReq"hidden>
                <h4 class="standard-header">Client Requirements</h4>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                <p class="standard-paragraphs">Propella could serve as an Alternative Supplier (procurement requirements) as prescribed by your clients</p>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Propella, individual resources from Propella or start-up ventures with desirable credentials can fulfil this demand as an approved supplier to your company  </p>
            </div>
            <!--Skills Gap-->
            <div class="skillsGap"hidden>
                <h4 class="standard-header">Skills Gap</h4>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to your company</b></p>
                <p class="standard-paragraphs">Where new skills are required by your business:</p>
                <p class="standard-paragraphs">•	Augment skills shortages</p>
                <p class="standard-paragraphs">•	Short term development support through our incubatees or interns</p>
                <p class="standard-paragraphs">•	30% subcontracting requirement for Public Sector</p>
                <p class="standard-paragraphs"style="color: #0f9d58"><b>Benefit to Propella</b></p>
                <p class="standard-paragraphs">Through training Propella can create these skills based on client demand (eg Power BI competencies), and benefit while
                    utilising the resources on internal projects during their training period </p>
            </div>

        </div>
    </div>
</div>

<!--Image-->
<div class=""id="partner2"></div>
<div id="desktop">
<br />
<br />
<br />
<br />
<div class="section " id="PartnerFifthSectionImage">
    <div class="row">
        <div class="parallax-container" id="PartnerFifthSection">
            <div class="parallax">
                <img src="/images/Propella_resized_images/frontNew.jpg"  id="PartnerFifthSectionImage">
            </div>
            <h1 class="partners-section-header3"> ESD Funding Option  </h1>
        </div>
    </div>
</div>
</div>

<div id="mobile">
    <div class="section" id="PartnerFifthSectionImage">
        <div class="row">
            <div id="PartnerFifthSection">
                <div>
                    <img src="/images/Propella_resized_images/frontNew.jpg"  id="PartnerFifthSectionImage">
                </div>
                <h1 class="partners-section-header3"> ESD Funding Option  </h1>
            </div>
        </div>
    </div>
</div>

<!--ESD Funding options Desktop-->
<div class="section standard-section-with-margins" id="PartnerFifthSectionDesktop">
    <div class="row">
        <div class="col s4 left-align">
            <div class="standard-left-buttons GrantsBlock">
                <h6 class="standard-left-button-headers"> Grants </h6>
            </div>

            <div class="standard-left-buttons LoanBlock">
                <h6 class="standard-left-button-headers">Loans </h6>
            </div>

            <div class="standard-left-buttons HybridBlock">
                <h6 class="standard-left-button-headers"> Hybrid </h6>
            </div>
        </div>

        <div class="col s1"></div>

        <div class="col s7 left-align"id="desktopESD">
            <div class="ESD">
                <p class="standard-paragraphs">Propella is an ESD Beneficiary in its own right</p>
                <p class="standard-paragraphs">Your Organisation will obtain their points as soon as a contribution is made to Propella</p>
                <p class="standard-paragraphs">This reduces the complexity & management of the ESD spend, particularly of larger organisations</p>
            </div>
            <div class="grants"hidden>
                <p class="standard-paragraphs">Grants in the form of cash and or equipment (in kind)</p>
                <p class="standard-paragraphs">•	Once off or annual lump sum (ED contribution) of 3% of net profit after tax for ICT companies and 1% for all other companies</p>
                <p class="standard-paragraphs">•	Once off or annual lump sum (SD contribution) of 2% of net profit after tax for ICT companies and all other companies</p>
            </div>
            <div class="loans"hidden>
                <p class="standard-paragraphs">A Loan offers a once off investment with recurring B-BBEE benefits </p>
                <p class="standard-paragraphs">•	As long as the loan is outstanding, your organisation will receive ESD points</p>
                <p class="standard-paragraphs">•	Your capital remains intact</p>
                <p class="standard-paragraphs">•	It is a cost-effective way of achieving ESD points</p>
                <p class="standard-paragraphs">•	Very little risk to the organisation</p>
                <p class="standard-paragraphs">•	Usually a 1 to 5-year term </p>
                <p class="standard-paragraphs">How it works …</p>
                <p class="standard-paragraphs">When calculating the cost of the points, it is not equivalent to the capital amount (eg 3% of NPAT) but the
                    cost of capital on this amount (assume 10% per annum)  </p>
                <p class="standard-paragraphs">You provide an interest free & surety free (optional) loan to Propella</p>
                <p class="standard-paragraphs">For this:</p>
                <p class="standard-paragraphs">•	You receive 70% ED recognition for an unsecured loan
                    (eg R 100 000 spend required, loan amount of R142 857)
                </p>
                <p class="standard-paragraphs">•	You receive 50% ED recognition for a secured loan
                    (eg R 100 000 spend required, loan amount of R 200 000)
                </p>
                <p class="standard-paragraphs">The capital amount is paid back at the end of the term or reinvested</p>
            </div>
            <div class="hybrid"hidden>
                <p class="standard-paragraphs">
                    Generally, this option has a direct impact on supply chain & procurement elements
                </p>
                <p class="standard-paragraphs">
                    This requires a fixed term commitment from your company and this could be a combination of grants, loans, equity and direct support from your company or consultant
                </p>
            </div>
        </div>
    </div>
</div>

<!--ESD Funding options Mobile-->
<div class="section standard-section-with-margins" id="PartnerFifthSectionMobile">
    <div class="row center-align">
        <div class="ESD">
            <p class="standard-paragraphs">Propella is an ESD Beneficiary in its own right</p>
            <p class="standard-paragraphs">Your Organisation will obtain their points as soon as a contribution is made to Propella</p>
            <p class="standard-paragraphs">This reduces the complexity & management of the ESD spend, particularly of larger organisations</p>
        </div>
            <div class="standard-left-buttons GrantsBlock">
                <h6 class="standard-left-button-headers"> Grants </h6>
            </div>

            <div class="standard-left-buttons LoanBlock">
                <h6 class="standard-left-button-headers">Loans </h6>
            </div>

            <div class="standard-left-buttons HybridBlock">
                <h6 class="standard-left-button-headers"> Hybrid </h6>
            </div>

        <div class="row center-align"id="desktopESD">

            <div class="grants"hidden>
                <p class="standard-paragraphs">Grants in the form of cash and or equipment (in kind)</p>
                <p class="standard-paragraphs">•	Once off or annual lump sum (ED contribution) of 3% of net profit after tax for ICT companies and 1% for all other companies</p>
                <p class="standard-paragraphs">•	Once off or annual lump sum (SD contribution) of 2% of net profit after tax for ICT companies and all other companies</p>
            </div>
            <div class="loans"hidden>
                <p class="standard-paragraphs">A Loan offers a once off investment with recurring B-BBEE benefits </p>
                <p class="standard-paragraphs">•	As long as the loan is outstanding, your organisation will receive ESD points</p>
                <p class="standard-paragraphs">•	Your capital remains intact</p>
                <p class="standard-paragraphs">•	It is a cost-effective way of achieving ESD points</p>
                <p class="standard-paragraphs">•	Very little risk to the organisation</p>
                <p class="standard-paragraphs">•	Usually a 1 to 5-year term </p>
                <p class="standard-paragraphs">How it works …</p>
                <p class="standard-paragraphs">When calculating the cost of the points, it is not equivalent to the capital amount (eg 3% of NPAT) but the
                    cost of capital on this amount (assume 10% per annum)  </p>
                <p class="standard-paragraphs">You provide an interest free & surety free (optional) loan to Propella</p>
                <p class="standard-paragraphs">For this:</p>
                <p class="standard-paragraphs">•	You receive 70% ED recognition for an unsecured loan
                    (eg R 100 000 spend required, loan amount of R142 857)
                </p>
                <p class="standard-paragraphs">•	You receive 50% ED recognition for a secured loan
                    (eg R 100 000 spend required, loan amount of R 200 000)
                </p>
                <p class="standard-paragraphs">The capital amount is paid back at the end of the term or reinvested</p>
            </div>
            <div class="hybrid"hidden>
                <p class="standard-paragraphs">
                    Generally, this option has a direct impact on supply chain & procurement elements
                </p>
                <p class="standard-paragraphs">
                    This requires a fixed term commitment from your company and this could be a combination of grants, loans, equity and direct support from your company or consultant
                </p>
            </div>
        </div>
    </div>

</div>

<!--Image-->
<div class=""id="IPB"></div>
<div id="desktop">
<br />
<br />
<br />
<br />
<div class="section ">
    <div class="row">
        <div class="parallax-container" id="PartnerSixthSection">
            <div class="parallax">
                <img src="/images/PartnerPageImages/3.4 Monetise IP.jpg" id="PartnerSixthSectionImage">
            </div>
            <h1 class="partner-section-header4"> Equity Opportunity </h1>
        </div>
    </div>
</div>
</div>

<div id="mobile">
<div class="section">
    <div class="row">
        <div id="PartnerSixthSection">
            <div>
                <img src="/images/PartnerPageImages/3.4 Monetise IP.jpg" id="PartnerSixthSectionImage">
            </div>
            <h1 class="partner-section-header4"> Equity Opportunity </h1>
        </div>
    </div>
</div>
</div>

<!--Equity Opportunity Desktop-->
<div class="section standard-section-with-margins" id="PartnerSixthSectionDesktop">
    <div class="row">
        <div class="col s4 left-align">
            <div class="standard-left-buttons EmptyBlock">
                <h6 class="standard-left-button-headers">  Commercialise Your Idea</h6>
            </div>
            <div class="standard-left-buttons ProjectsBlock">
                <h6 class="standard-left-button-headers"> Propella Projects </h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier development Solutions </h6>
            </div>
            <div class="standard-left-buttons TalkBlock">
                <h6 class="standard-left-button-headers"> Let's Talk </h6>
            </div>
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>

            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>

        </div>

        <div class="col s1"></div>

        <div class="col s7">
            <div class="equity">
                <h4 class="standard-header">Invest in new technologies</h4>
                <p class="standard-paragraphs">Who wouldn’t want to have been an early investor in Google, Facebook or Uber?  </p>
                <p class="standard-paragraphs">Speak to us about equity opportunities in the ventures currently in incubation or acceleration programmes at Propella.  With our focus on </p>
                <p class="standard-paragraphs">Smart City solutions you could be pleasantly surprised by the technologies on offer by our ventures.</p>
                <p style="font-style: italic;"><a href="/Home/Categories">View Our Ventures</a></p>
                <br />
                <p style="font-style: italic;"><a href="#DesktopButtons">Go back</a></p>
            </div>
        </div>
    </div>
</div>

<!--Equity Opportunity Mobile-->
<div class="section standard-section-with-margins" id="PartnerSixthSectionMobile">
    <div class="row center-align">
        <div class="row">
            <div class="row center-align">
                <div class="equity">
                    <h4 class="standard-header">Invest in new technologies</h4>
                    <p class="standard-paragraphs">Who wouldn’t want to have been an early investor in Google, Facebook or Uber?  </p>
                    <p class="standard-paragraphs">Speak to us about equity opportunities in the ventures currently in incubation or acceleration programmes at Propella.  With our focus on </p>
                    <p class="standard-paragraphs">Smart City solutions you could be pleasantly surprised by the technologies on offer by our ventures.</p>
                    <p style="font-style: italic;"><a href="/Home/Categories">View Our Ventures</a></p>
                    <br />

                </div>
            </div>
            <div class="standard-left-buttons EmptyBlock">
                <h6 class="standard-left-button-headers">  Commercialise your Idea</h6>
            </div>
            <div class="standard-left-buttons ProjectsBlock">
                <h6 class="standard-left-button-headers"> Propella Projects </h6>
            </div>
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier development Solutions </h6>
            </div>
            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>
            <div class="standard-left-buttons TalkBlock">
                <h6 class="standard-left-button-headers"> Let's Talk </h6>
            </div>
            <p style="font-style: italic;"><a href="#FirstButtonsMobile">Go back</a></p>
        </div>
    </div>
</div>

<!--iOt Image-->
<div class="" id="iot"></div>
<div id="desktop">
<br />
<br />
<br />
<br />
<div class="section">
    <div class="row">
        <div class="parallax-container" id="iotSection">
            <div class="parallax">
                <img src="/images/Partners-Resized-Images/IOT 2.JPG" id="iotSectionImage">
            </div>
            <h1 class="iot-header"style="color:white;text-shadow: 5px 3px 1px #000000;">IoT</h1>
        </div>
    </div>
</div>
</div>

<div id="mobile">
<div class="section">
    <div class="row">
        <div id="iotSection">
            <div>
                <img src="/images/Partners-Resized-Images/IOT 2.JPG" id="iotSectionImage">
            </div>
            <h1 class="iot-header"style="color:white;text-shadow: 5px 3px 1px #000000;">IoT</h1>
        </div>
    </div>
</div>
</div>

<!--iOt Desktop-->
<div class="section standard-section-with-margins" id="iotDesktop">
    <div class="row">
        <div class="col s4 left-align">
            <div class="standard-left-buttons ProjectsBlock">
                <h6 class="standard-left-button-headers"> Propella Projects </h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier development Solutions </h6>
            </div>
            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>
            <div class="standard-left-buttons IPBlock">
                <h6 class="standard-left-button-headers">Equity Opportunity</h6>
            </div>
            <div class="standard-left-buttons TalkBlock">
                <h6 class="standard-left-button-headers"> Let's Talk </h6>
            </div>
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>

        </div>
        <div class="col s1"></div>
        <div class="col s7">

            <div class="center">
                <a href="https://www.mvnxmobile.com/iot/"><img style="width: 40%" src="/images/PartnerPageImages/MVN-X-logo.png"/></a>
            </div>
            <h4 class="standard-header ">Delivering enterprise ready end-to-end IoT solutions throughout Africa</h4>
            <p class="standard-paragraphs">We are proud to announce that MVN-X has partnered with the Propella Business Incubator.</p>
            <p class="standard-paragraphs">Propella assists Entrepreneurs and Businesses with the development of prototypes and the commercialization thereof.</p>
            <p class="standard-paragraphs"><b>Together, we intend pushing the boundaries of technology advancement, with the purpose of creating digital traceability and visibility solutions within supply chains, giving the African market a global advantage.</b></p>
            <p class="standard-paragraphs"><b>With our new system, you can get connected to almost anything, from anywhere.</b></p>
            <p class="standard-paragraphs">We have collaborated to develop both the hardware and software technology platforms, for showcasing a demonstration project that collects, monitors and analyses data in real-time. All metrics are displayed via a remote dashboard.
            </p>
            <p class="standard-paragraphs"><b>The electronic platforms, sensors and actuators collect data, transmit data, analyse data, and feed back to devices which actuate responses, when certain parameters are out of control.
                </b></p>
            <p class="standard-paragraphs">Our development leverages pre-existing global network infrastructure, and as a result, this panel can track any variable from anywhere in the world, with the benefit of being adaptable enough to take to market almost immediately.
            </p>
            <p class="standard-paragraphs">We have implemented a “single wallet” system, whereby all your SIM cards work off one bulk data purchase. This low-cost communication platform is integrated into the overall solution.
            </p>
            <h4 class="standard-header">IoT CONCEPT</h4>
            <p class="standard-paragraphs"><b>Although we have conceptually applied this system within the poultry and fresh produce industry, this infrastructure can be used in any trade where traceability and visibility promotes competitive edge.</b></p>
            <p class="standard-paragraphs">Practical solutions would include the monitoring and control of chemical parameters in pools, ponds and aquaculture ventures, the position of livestock, the environmental status of agricultural environments and the organic supply chain of consumer products etc. The possibilities are infinite.
            </p>
            <p class="standard-paragraphs">The demo egg production system monitors the geo-location; temperature; air-flow, humidity; barometric pressure, CO2 levels, Volatiles, light and water pump status. These are all key parameters that need to be controlled to ensure the productive output of any such facility.
            </p>
            <p class="standard-paragraphs">These variables are monitored, with an easily accessible dashboard, alerts being sent out when key parameters are being encroached, or when any intervention is required. These remote notifications mean that producers are always kept in the loop, with the power to control their machinery from any location, – together with implementing automated responses.
            </p>
            <p class="standard-paragraphs">This innovative technology ties in with <a href="https://www.iotforumafrica.com/">The Internet of Things Forum Africa Exhibition (IoT)</a> between 26 – 27 March 2019 at Gallagher Convention Centre in Johannesburg.</p>
            <p class="standard-paragraphs">Simply put, IoT aims to take everything we do and put it online – so we can integrate technology on a mass scale.<a href="https://www.iotforall.com/what-is-iot-simple-explanation/">(https://www.iotforall.com/what-is-iot-simple-explanation/)</a>
            </p>
        </div>
    </div>
</div>

<!--iOt Mobile-->
<div class="section standard-section-with-margins center-align" id="iotMobile">
        <div class="col s1"></div>
        <div class="col s7">
            <div class="center">
                <a href="https://www.mvnxmobile.com/iot/"><img style="width: 60%" src="/images/PartnerPageImages/MVN-X-logo.png"/></a>
            </div>
            <h4 class="standard-header ">Delivering enterprise ready end-to-end IoT solutions throughout Africa</h4>
            <p class="standard-paragraphs">We are proud to announce that MVN-X has partnered with the Propella Business Incubator.</p>
            <p class="standard-paragraphs">Propella assists Entrepreneurs and Businesses with the development of prototypes and the commercialization thereof.</p>
            <p class="standard-paragraphs"><b>Together, we intend pushing the boundaries of technology advancement, with the purpose of creating digital traceability and visibility solutions within supply chains, giving the African market a global advantage.</b></p>
            <p class="standard-paragraphs"><b>With our new system, you can get connected to almost anything, from anywhere.</b></p>
            <p class="standard-paragraphs">We have collaborated to develop both the hardware and software technology platforms, for showcasing a demonstration project that collects, monitors and analyses data in real-time. All metrics are displayed via a remote dashboard.
            </p>
            <p class="standard-paragraphs"><b>The electronic platforms, sensors and actuators collect data, transmit data, analyse data, and feed back to devices which actuate responses, when certain parameters are out of control.
                </b></p>
            <p class="standard-paragraphs">Our development leverages pre-existing global network infrastructure, and as a result, this panel can track any variable from anywhere in the world, with the benefit of being adaptable enough to take to market almost immediately.
            </p>
            <p class="standard-paragraphs">We have implemented a “single wallet” system, whereby all your SIM cards work off one bulk data purchase. This low-cost communication platform is integrated into the overall solution.
            </p>
            <h4 class="standard-header">IoT CONCEPT</h4>
            <p class="standard-paragraphs"><b>Although we have conceptually applied this system within the poultry and fresh produce industry, this infrastructure can be used in any trade where traceability and visibility promotes competitive edge.</b></p>
            <p class="standard-paragraphs">Practical solutions would include the monitoring and control of chemical parameters in pools, ponds and aquaculture ventures, the position of livestock, the environmental status of agricultural environments and the organic supply chain of consumer products etc. The possibilities are infinite.
            </p>
            <p class="standard-paragraphs">The demo egg production system monitors the geo-location; temperature; air-flow, humidity; barometric pressure, CO2 levels, Volatiles, light and water pump status. These are all key parameters that need to be controlled to ensure the productive output of any such facility.
            </p>
            <p class="standard-paragraphs">These variables are monitored, with an easily accessible dashboard, alerts being sent out when key parameters are being encroached, or when any intervention is required. These remote notifications mean that producers are always kept in the loop, with the power to control their machinery from any location, – together with implementing automated responses.
            </p>
            <p class="standard-paragraphs">This innovative technology ties in with <a href="https://www.iotforumafrica.com/">The Internet of Things Forum Africa Exhibition (IoT)</a> between 26 – 27 March 2019 at Gallagher Convention Centre in Johannesburg.</p>
            <p class="standard-paragraphs">Simply put, IoT aims to take everything we do and put it online – so we can integrate technology on a mass scale.<a href="https://www.iotforall.com/what-is-iot-simple-explanation/">(https://www.iotforall.com/what-is-iot-simple-explanation/)</a>
            </p>
        </div>
    <div class="row">
        <div class="">
            <div class="standard-left-buttons ProjectsBlock">
                <h6 class="standard-left-button-headers"> Propella Projects </h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier development Solutions </h6>
            </div>
            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>
            <div class="standard-left-buttons IPBlock">
                <h6 class="standard-left-button-headers">Equity Opportunity</h6>
            </div>
            <div class="standard-left-buttons TalkBlock">
                <h6 class="standard-left-button-headers"> Let's Talk </h6>
            </div>
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>
        </div>
        <p style="font-style: italic;"><a href="#FirstButtonsMobile">Go back</a></p>
    </div>
</div>

<!--Cormmecialise opportunity section-->
<div id="desktop">
<div class="section ">
    <div class="row">
        <div class="parallax-container" id="PartnerSeventhSection">
            <div class="parallax">
                <img src="/images/Propella_resized_images/redbull 3.jpg" id="PartnerSeventhSectionImage">
            </div>
            <h1 class="comm-header"style="color:white;">Commercialise your Idea or Prototype</h1>
        </div>
    </div>
</div>
</div>

<div id="mobile">
<div class="section">
    <div class="row">
        <div id="PartnerSeventhSection">
            <div>
                <img src="/images/Propella_resized_images/redbull 3.jpg" id="PartnerSeventhSectionImage">
            </div>
            <h1 class="comm-header"style="color:white;">Commercialise your Idea or Prototype</h1>
        </div>
    </div>
</div>
</div>

<!--Cormmecialise opportunity section Desktop-->
<div class="section standard-section-with-margins" id="CommSectionDesktop">
    <div class="row">
        <div class="col s4 left-align">
            <div class="standard-left-buttons ProjectsBlock">
                <h6 class="standard-left-button-headers"> Propella Projects </h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier development Solutions </h6>
            </div>
            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>
            <div class="standard-left-buttons IPBlock">
                <h6 class="standard-left-button-headers">Equity Opportunity</h6>
            </div>
            <div class="standard-left-buttons TalkBlock">
                <h6 class="standard-left-button-headers"> Let's Talk </h6>
            </div>
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>

        </div>
        <div class="col s1"></div>
        <div class="col s7">
            <div class="Commercialise">
                <h4 class="standard-header">Invention x Commercialisation = Innovation</h4>
                <p class="standard-paragraphs">At Propella we are passionate about innovation engaging with like-minded entrepreneurs with a top-notch, hi-tech ideas.  </p>
                <p class="standard-paragraphs">If you have an idea and are wondering if it has commercial legs, we invite you to give us a call.  We’d be only too happy to
                    have a discussion with you (remember our business is helping take your business idea to new heights).</p>
                <p class="standard-paragraphs">Add our experienced and sort after bank of mentors and experts, our local and national networks within corporate,
                    government and academia together with our expertise in new venture start-up’s and you have a great shot at commercialising your idea or prototype.</p>
                <p class="standard-paragraphs">Why not give us a call to find out if your idea fits our scope?</p>
                <br />
                <p style="font-style: italic;"><a href="#DesktopButtons">Go back</a></p>
            </div>
        </div>
    </div>
</div>

<!--Cormmecialise opportunity section Mobile-->
<div class="section standard-section-with-margins" id="CommSectionMobile">
    <div class="row center-align">
        <div class="row">
            <div class="Commercialise">
                <p class="standard-header">Invention x Commercialisation = Innovation</p>
                <p class="standard-paragraphs">At Propella we are passionate about innovation engaging with like-minded entrepreneurs with a top-notch, hi-tech ideas.  </p>
                <p class="standard-paragraphs">If you have an idea and are wondering if it has commercial legs, we invite you to give us a call.  We’d be only too happy to
                    have a discussion with you (remember our business is helping take your business idea to new heights).</p>
                <p class="standard-paragraphs">Add our experienced and sort after bank of mentors and experts, our local and national networks within corporate,
                    government and academia together with our expertise in new venture start-up’s and you have a great shot at commercialising your idea or prototype.</p>
                <p class="standard-paragraphs">Why not give us a call to find out if your idea fits our scope?</p>
                <br />

            </div>
        </div>
        <div class="col s1"></div>
        <div class="row">
            <div class="standard-left-buttons ProjectsBlock">
                <h6 class="standard-left-button-headers"> Propella Projects </h6>
            </div>
            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>
            <div class="standard-left-buttons TalkBlock">
                <h6 class="standard-left-button-headers"> Let's Talk </h6>
            </div>
            <div class="standard-left-buttons IPBlock">
                <h6 class="standard-left-button-headers">Equity Opportunity</h6>
            </div>
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier development Solutions </h6>
            </div>
            <p style="font-style: italic;"><a href="#FirstButtonsMobile">Go back</a></p>
        </div>
    </div>
</div>


<!--Propella projects-->
<div class=""id="projects"></div>
<div id="desktop">
<br />
<br />
<br />
<br />

<div class="section ">
    <div class="row">
        <div class="parallax-container" id="PartnerSeventhSection">
            <div class="parallax">
                <img src="/images/Partners-Resized-Images/Propella Projects.JPG" id="PartnerSeventhSectionImage">
            </div>
            <h1 class="partner-section-header5"> Propella Projects </h1>
        </div>
    </div>
</div>
</div>

<div id="mobile">
    <div class="section ">
        <div class="row">
            <div id="PartnerSeventhSection">
                <div>
                    <img src="/images/Partners-Resized-Images/Propella Projects.JPG" id="PartnerSeventhSectionImage">
                </div>
                <h1 class="partner-section-header5"> Propella Projects </h1>
            </div>
        </div>
    </div>
</div>

<!--Propella projects Desktop-->
<div class="section standard-section-with-margins" id="PartnerSeventhSectionDesktop">
    <div class="row">
        <div class="col s4 left-align">
            <div class="standard-left-buttons TalkBlock">
                <h6 class="standard-left-button-headers"> Let's Talk </h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier Development Solutions </h6>
            </div>
            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>

            <div class="standard-left-buttons IPBlock">
                <h6 class="standard-left-button-headers">Equity Opportunity</h6>
            </div>
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>
            <div class="standard-left-buttons EmptyBlock">
                <h6 class="standard-left-button-headers">  Commercialise your Idea</h6>
            </div>
        </div>

        <div class="col s1"></div>

        <div class="col s7">
            <h4 class="standard-header">Propella Projects</h4>
            <p class="standard-paragraphs">Propella is working on a number of its own Smart City projects.</p>
            <p class="standard-paragraphs"><b>Sensors</b></p>
            <p class="standard-paragraphs">Propella has developed a wide range of miniaturised electronic platforms that will significantly enhance their
                ability to fast-track the development of prototypes. When these platforms are integrated into an environment,
                using a range of sensors, the ability to monitor, control and predict the behaviour of machinery, equipment, products and services remotely,
                becomes a reality.  </p>

            <p class="standard-paragraphs"><b>Cirrus Water</b></p>
            <p class="standard-paragraphs">Propella have a demonstration setup of an ‘air-to-water’ machine that produces very high-quality potable water,
                from the environment. For a refreshing experience, why not come and taste our water?</p>

            <p class="standard-paragraphs"><b>Damsak</b></p>
            <p class="standard-paragraphs">During times of drought, the demand for water harvesting increases dramatically. Water is typically however
                stored in large tanks that are very expensive and cumbersome to transport. The ‘damsak’ is folded like a bag and may be economically
                transported due to it light weight and minimal volume. They can also carry very large volumes of water.</p>
            <p class="standard-paragraphs"><b>NMU / ITD Project</b></p>
            <p class="standard-paragraphs">Engeli were approached by ITD to assist in developing 40 pulse counters and a gateway to assist in communicating
                water consumption of water meters located at the NMU. Engeli in turn approached Propella to assist in the development of these pulse sensors.
                The outcome of the pilot is proving to be a success, with stable measurements being recorded on a consistent basis.</p>
            <br />
            <p style="font-style: italic;"><a href="#DesktopButtons">Go back</a></p>
        </div>
    </div>
</div>

<!--Propella projects Mobile-->
<div class="section standard-section-with-margins" id="PartnerSeventhSectionMobile">
    <div class="row center-align">
        <div class="row">
            <div class="row">
                <p class="standard-header">Propella Projects</p>
                <p class="standard-paragraphs">Propella is working on a number of its own Smart City projects.</p>
                <p class="standard-paragraphs"><b>Sensors</b></p>
                <p class="standard-paragraphs">Propella has developed a wide range of miniaturised electronic platforms that will significantly enhance their
                    ability to fast-track the development of prototypes. When these platforms are integrated into an environment,
                    using a range of sensors, the ability to monitor, control and predict the behaviour of machinery, equipment, products and services remotely,
                    becomes a reality.  </p>

                <p class="standard-paragraphs"><b>Cirrus Water</b></p>
                <p class="standard-paragraphs">Propella have a demonstration setup of an ‘air-to-water’ machine that produces very high-quality potable water,
                    from the environment. For a refreshing experience, why not come and taste our water?</p>

                <p class="standard-paragraphs"><b>Damsak</b></p>
                <p class="standard-paragraphs">During times of drought, the demand for water harvesting increases dramatically. Water is typically however
                    stored in large tanks that are very expensive and cumbersome to transport. The ‘damsak’ is folded like a bag and may be economically
                    transported due to it light weight and minimal volume. They can also carry very large volumes of water.</p>
                <br />

            </div>
            <div class="standard-left-buttons TalkBlock">
                <h6 class="standard-left-button-headers"> Let's Talk </h6>
            </div>
            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier Development Solutions </h6>
            </div>
            <div class="standard-left-buttons EmptyBlock">
                <h6 class="standard-left-button-headers">  Commercialise your Idea</h6>
            </div>
            <div class="standard-left-buttons IPBlock">
                <h6 class="standard-left-button-headers">Equity Opportunity</h6>
            </div>
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>
            <p style="font-style: italic;"><a href="#FirstButtonsMobile">Go back</a></p>
        </div>

    </div>
</div>

<!--Lets Talk-->
<div class=""id="talk"></div>
<div id="desktop">
<br />
<br />
<br />
<br />
<div class="section">
    <div class="row">
        <div class="parallax-container" id="PartnerEigthSection">
            <div class="parallax">
                <img src="/images/PartnerPageImages/3.5 Talk.jpg" id="PartnerEigthSectionImage">
            </div>
            <h1 class="partner-section-header6"> Let's Talk</h1>
        </div>
    </div>
</div>
</div>

<div id="mobile">
    <div class="section">
        <div class="row">
            <div id="PartnerEigthSection">
                <div>
                    <img src="/images/PartnerPageImages/3.5 Talk.jpg" id="PartnerEigthSectionImage">
                </div>
                <h1 class="partner-section-header6"> Let's Talk</h1>
            </div>
        </div>
    </div>
</div>

<!--Lets Talk Desktop-->
<div class="section standard-section-with-margins" id="PartnerEigthSectionDesktop">
    <div class="row">
        <div class="col s4 left-align">
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>
            <div class="standard-left-buttons IPBlock">
                <h6 class="standard-left-button-headers">Equity Opportunity</h6>
            </div>
            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>
            <div class="standard-left-buttons EmptyBlock">
                <h6 class="standard-left-button-headers">  Commercialise your Idea</h6>
            </div>
            <div class="standard-left-buttons ProjectsBlock">
                <h6 class="standard-left-button-headers"> Propella Projects </h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier Development Solutions </h6>
            </div>
        </div>

        <div class="col s1"></div>

        <div class="col s7">
            <form id="ContactUsFormDesktop">

                <div class="input-field">
                    <input placeholder="Full Name" id="name-desktop" type="text" />
                </div>


                <div class="input-field">
                    <select id="subject-desktop">
                        <option value="Subject" disabled selected>Subject</option>
                        <option value="General Discussion" >General Discussion</option>
                        <option value="Invest" >Invest</option>
                        <option value="Entrepreneurs" >Entrepreneurs</option>
                        <option value="Apply" >Apply</option>
                    </select>
                </div>


                <div class="input-field">
                    <input placeholder="Email" id="email-desktop" type="email" />
                </div>


                <form class="col s12">
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Message" id="message-desktop">
                        </div>
                    </div>
                </form>

                <div class="input-field">
                    <input id="phone-desktop" type="tel" />
                    <label for="phone-desktop">Phone Number</label>
                </div>

                <div>
                    <button id="send-partner-form" style="margin-left: 2em"
                            class="btn waves-effect waves-light" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
            <br />
            <p style="font-style: italic;"><a href="#DesktopButtons">Go back</a></p>
        </div>
    </div>
</div>

<!--Lets Talk Mobile-->
<div class="section standard-section-with-margins" id="PartnerEigthSectionMobile">
    <div class="row center-align">
        <div class="row">
            <div class="standard-left-buttons BBBEBlock">
                <h6 class="standard-left-button-headers">  Maximise your B-BBEE</h6>
            </div>
            <div class="standard-left-buttons MaximiseBlock">
                <h6 class="standard-left-button-headers">Enterprise and Supplier Development Solutions </h6>
            </div>

            <div class="standard-left-buttons InvestmentBlock">
                <h6 class="standard-left-button-headers">ESD Funding Options </h6>
            </div>

            <div class="standard-left-buttons IPBlock">
                <h6 class="standard-left-button-headers">Equity Opportunity</h6>
            </div>
            <div class="standard-left-buttons EmptyBlock">
                <h6 class="standard-left-button-headers">  Commercialise your Idea</h6>
            </div>
            <div class="standard-left-buttons ProjectsBlock">
                <h6 class="standard-left-button-headers"> Propella Projects </h6>
            </div>

        </div>

        <div class="col s1"></div>

        <div class="row">
            <form id="ContactUsFormMobile">

                <div class="input-field">
                    <input placeholder="Full Name" id="name" type="text" />
                </div>


                <div class="input-field">
                    <select>
                        <option value="Subject" disabled selected>Subject</option>
                        <option value="1" >General Discussion</option>
                        <option value="2" >Invest</option>
                        <option value="3" >Entrepreneurs</option>
                        <option value="4" >Apply</option>
                    </select>
                </div>


                <div class="input-field">
                    <input placeholder="Email" id="email" type="email" />
                </div>


                <form class="col s12">
                    <div class="row">
                        <div class="input-field col s12">
                            {{--<i class="material-icons prefix">mode_edit</i>--}}
                            <input placeholder="Message">
                        </div>
                    </div>
                </form>

                <div class="input-field">
                    <input id="phone" type="tel" />
                    <label for="phone">Phone Number</label>
                </div>

                <div>
                    <button class="btn waves-effect waves-light float-right" type="submit" name="action">Send
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
            <br />
            <p style="font-style: italic;"><a href="#FirstButtonsMobile">Go back</a></p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.parallax').parallax();
        $('select').formSelect();

        $('.TalkBlock2').on('click', function () {
            location.href = '#iot';
        });
        $('.TalkBlock2').on('click', function () {
            location.href = '#iot';
        });

        $('.MaximiseBlock').on('click', function () {
            location.href = '#enterprise';
        });

        $('.BBBEBlock').on('click', function () {
            location.href = '#partner1';
        });
        $('.InvestmentBlock').on('click', function () {
            location.href = '#partner2';
        });
        $('.IPBlock').on('click', function () {
            location.href = '#IPB';
        });
        $('.ProjectsBlock').on('click', function () {
            location.href = '#projects';
        });
        $('.TalkBlock').on('click', function () {
            location.href = '#talk';
        });
        //Commercialise your Idea or Prototype
        $('.EmptyBlock').on('click', function () {
            location.href = '#CommercializeSection';
        });

        //Your need button
        $('.yourNeedBlock').on('click', function () {
            $('.yourNeed').show();
            $('.yourSolution').hide();
            $('.Maximise').hide();
        });

        //Your Solution/Benefit button
        $('.BenefitBlock').on('click', function () {
            $('.yourSolution').show();
            $('.yourNeed').hide();
            $('.Maximise').hide();
        });

        //Monetise your IP button
        $('.IBlock').on('click', function () {
            $('.monetiseIP').show();
            $('.Enterprise').hide();
            $('.InvestOpp').hide();
            $('.newInnovation').hide();
            $('.supplyChain').hide();
            $('.jobCreation').hide();
            $('.clientReq').hide();
            $('.skillsGap').hide();
            $('.yourNeed').hide();
            $('.yourSolution').hide();
        });

        //Investment opp button
        $('.InvestBlock').on('click', function () {
            $('.InvestOpp').show();
            $('.Enterprise').hide();
            $('.monetiseIP').hide();
            $('.newInnovation').hide();
            $('.supplyChain').hide();
            $('.jobCreation').hide();
            $('.clientReq').hide();
            $('.skillsGap').hide();
            $('.yourNeed').hide();
            $('.yourSolution').hide()
        });

        //New innovation button
        $('.INNOVBlock').on('click', function () {
            $('.newInnovation').show();
            $('.monetiseIP').hide();
            $('.Enterprise').hide();
            $('.InvestOpp').hide();
            $('.supplyChain').hide();
            $('.jobCreation').hide();
            $('.clientReq').hide();
            $('.skillsGap').hide();
            $('.yourNeed').hide();
            $('.yourSolution').hide();
        });
        //Supply chain button
        $('.SuppChainBlock').on('click', function () {
            $('.supplyChain').show();
            $('.newInnovation').hide();
            $('.monetiseIP').hide();
            $('.Enterprise').hide();
            $('.InvestOpp').hide();
            $('.jobCreation').hide();
            $('.clientReq').hide();
            $('.skillsGap').hide();
            $('.yourNeed').hide();
            $('.yourSolution').hide();
        });
        //Job creation  button
        $('.JobBlock').on('click', function () {
            $('.jobCreation').show();
            $('.supplyChain').hide();
            $('.newInnovation').hide();
            $('.monetiseIP').hide();
            $('.Enterprise').hide();
            $('.InvestOpp').hide();
            $('.clientReq').hide();
            $('.skillsGap').hide();
            $('.yourNeed').hide();
            $('.yourSolution').hide();
        });
        //Client requirements  button
        $('.ClientBlock').on('click', function () {
            $('.clientReq ').show();
            $('.jobCreation').hide();
            $('.supplyChain').hide();
            $('.newInnovation').hide();
            $('.monetiseIP').hide();
            $('.Enterprise').hide();
            $('.InvestOpp').hide();
            $('.skillsGap').hide();
            $('.yourNeed').hide();
            $('.yourSolution').hide();
        });
        //Skills gap button
        $('.SkillsBlock').on('click', function () {
            $('.skillsGap').show();
            $('.jobCreation').hide();
            $('.supplyChain').hide();
            $('.newInnovation').hide();
            $('.monetiseIP').hide();
            $('.Enterprise').hide();
            $('.InvestOpp').hide();
            $('.clientReq').hide();
            $('.yourNeed').hide();
            $('.yourSolution').hide();
        });
        //Grants button
        $('.GrantsBlock').on('click', function () {
            $('.grants').show();
            $('.ESD').hide();
            $('.loans').hide();
            $('.hybrid').hide();
        });
        //Loans button
        $('.LoanBlock').on('click', function () {
            $('.loans').show();
            $('.ESD').hide();
            $('.grants').hide();
            $('.hybrid').hide();
        });
        //Hybrid button
        $('.HybridBlock').on('click', function () {
            $('.hybrid').show();
            $('.ESD').hide();
            $('.grants').hide();
            $('.loans').hide();
        });
        //Funding Options
        $('.FundingBlock').on('click', function () {
            location.href = '#PartnerFifthSectionImage';
        });
        //Loans button
        $('.LoanBlock').on('click', function () {
            $('.loans').show();
            $('.ESD').hide();
            $('.grants').hide();
            $('.hybrid').hide();
        });
        //Hybrid button
        $('.HybridBlock').on('click', function () {
            $('.hybrid').show();
            $('.ESD').hide();
            $('.grants').hide();
            $('.loans').hide();
        });
        //Funding Options
        $('.FundingBlock').on('click', function () {
            location.href = '#PartnerFifthSectionImage';
        });

         $('#send-partner-form').on('click', function(){
             let formData = new FormData();
             formData.append('name', $('#name-desktop').val());
             formData.append('subject', $('#subject-desktop').val());
             formData.append('email', $('#email-desktop').val());
             formData.append('message', $('#message-desktop').val());
             formData.append('phone', $('#phone-desktop').val());



              let url = '/Home/Partners/form-submit';

               $.ajax({
                   url: url,
                   processData: false,
                   contentType: false,
                   data: formData,
                   type: 'post',
                   headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },

                   success: function (response, a, b) {
                       console.log("success", response);
                       alert(response.message);
                   },
                   error: function (response) {
                       console.log("error", response);
                       let message = error.response.message;
                       let errors = error.response.errors;

                       for (var error in   errors) {
                           console.log("error", error)
                           if (errors.hasOwnProperty(error)) {
                               message += errors[error] + "\n";
                           }
                       }
                       alert(message);
                   }
               });
         });

        // $('#ContactUsFormDesktop').on('submit', function(e){
        //     e.preventDefault();
        //
        //     console.log('testing');
        //
        //     let formData = new FormData();
        //     formData.append('name', $('#name-desktop').val());
        //     formData.append('subject', $('#subject-desktop').val());
        //     formData.append('email', $('#email-desktop').val());
        //     formData.append('message', $('#message-desktop').val());
        //     formData.append('phone', $('#phone-desktop').val());
        //
        //
        //     let url = 'partner.mail';
        //
        //     $.ajax({
        //         url: url,
        //         contentType: 'json',
        //         data: formData,
        //         type: 'post',
        //
        //         success: function (response, a, b) {
        //             console.log("success", response);
        //             alert(response.message);
        //         },
        //         error: function (response) {
        //             console.log("error", response);
        //             let message = error.response.message;
        //             let errors = error.response.errors;
        //
        //             for (var error in   errors) {
        //                 console.log("error", error)
        //                 if (errors.hasOwnProperty(error)) {
        //                     message += errors[error] + "\n";
        //                 }
        //             }
        //             alert(message);
        //         }
        //     });
        // });
    });
</script>
</body>
@endsection
