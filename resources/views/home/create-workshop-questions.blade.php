@extends('layouts.app')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>

    <div class="row" style="width: 950px;padding-left:9%;">
        <br>
        <div class="card white" style="padding-left:5%;top: 10vh">
            <br>
            <img style="margin-left: 500px" src="images/Propella_Logo.jpg">
            <h5><b>EVALUATION FORM</b></h5>
            <br>
            <div class="col s5">
                <div class="input-field">
                    <input style="width:90%" id="title" type="text">
                    <label for="title">Workshop Title</label>
                </div>
            </div>
            <div class="col s5">
                <div class="input-field">
                    <input style="width:90%" id="date" type="date">
                    <label for="date">Workshop Date</label>
                </div>
            </div>
            <div class="col s5">
                <div class="input-field">
                    <input style="width:90%" id="venue" type="text">
                    <label for="venue">Venue</label>
                </div>
            </div>
            <div class="col s5">
                <div class="input-field">
                    <input style="width:90%" id="facilitator" type="text">
                    <label for="facilitator">Facilitator</label>
                </div>
            </div>
            <div class="row">
                <div class="col s10">
                    <p> Please rate the following items on a scale of one to ten, with one being abysmal, five being acceptable, and ten being perfect.
                    </p>
                    <div class="input-field">
                        <input style="width:40%" id="workshop_venue" type="number">
                        <label for="workshop_venue">WORKSHOP VENUE - (rate 1 - 10)</label>
                    </div>
                    <div class="input-field">
                        <input style="width:90%" id="venue_comment" type="text">
                        <label for="venue_comment">Comment</label>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col s10">
                    <div class="input-field">
                        <input style="width:40%" id="workshop_facilities" type="number">
                        <label for="workshop_facilities">WORKSHOP FACILITIES - (rate 1 - 10)</label>
                    </div>
                    <div class="input-field">
                        <input style="width:90%" id="facility_comment" type="text">
                        <label for="facility_comment">Comment</label>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col s10">
                    <div class="input-field">
                        <input style="width:40%" id="instructor_knowledge" type="number">
                        <label for="instructor_knowledge">INSTRUCTOR KNOWLEDGE - (rate 1 - 10)</label>
                    </div>
                    <div class="input-field">
                        <input style="width:90%" id="instructor_comment" type="text">
                        <label for="instructor_comment">Comment</label>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col s10">
                    <div class="input-field">
                        <input style="width:40%" id="quality_of_presentation" type="number">
                        <label for="quality_of_presentation">QUALITY OF PRESENTATION MATERIALS & HANDOUTS - (rate 1 - 10)</label>
                    </div>
                    <div class="input-field">
                        <input style="width:90%" id="quality_comment" type="text">
                        <label for="quality_comment">Comment</label>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col s10">
                    <div class="input-field">
                        <input style="width:90%" id="main_take_away" type="text">
                        <label for="main_take_away"> 1.	What is your main take away?</label>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col s10">
                    <div class="input-field">
                        <input style="width:90%" id="recommendation_of_course" type="text">
                        <label for="recommendation_of_course"> 2.	Would you recommend this course to others? Why or why not?</label>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col s10">
                    <div class="input-field">
                        <input style="width:90%" id="thoughts" type="text">
                        <label for="thoughts"> 3.	Other thoughts you would like to share?</label>
                    </div>

                </div>
            </div>

            <div class="row" style="margin-left: 500px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn" id="question-submit-button">Save</a>
                </div>
            </div>
            <br/>
        </div>
    </div>
    <br>
    <br>
    <br>

    <script>

        $(document).ready(function() {

            $('select').formSelect();

            $('#question-submit-button').on('click',function () {

                let formData = new FormData();
                formData.append('title', $('#title').val());
                formData.append('date', $('#date').val());
                formData.append('venue', $('#venue').val());
                formData.append('facilitator', $('#facilitator').val());
                formData.append('workshop_venue', $('#workshop_venue').val());
                formData.append('venue_comment', $('#venue_comment').val());
                formData.append('instructor_knowledge', $('#instructor_knowledge').val());
                formData.append('instructor_comment', $('#instructor_comment').val());
                formData.append('quality_of_presentation', $('#quality_of_presentation').val());
                formData.append('quality_comment', $('#quality_comment').val());
                formData.append('main_take_away', $('#main_take_away').val());
                formData.append('recommendation_of_course', $('#recommendation_of_course').val());
                formData.append('thoughts', $('#thoughts').val());
                formData.append('workshop_facilities', $('#workshop_facilities').val());
                formData.append('facility_comment', $('#facility_comment').val());



                $.ajax({
                    url: '{{route('upload-workshop-questions')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });

    </script>
@endsection
