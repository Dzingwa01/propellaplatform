@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    <input id="user-id-input" disabled hidden value="{{$company->id}}">
    {{ Breadcrumbs::render('edit-companies',$company)}}
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Edit Company</h6>
             </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">

            <div class="row">
                <form class="col s12">
                    @csrf
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="company_name" value="{{$company->company_name}}" type="text" class="validate">
                            <label for="company_name">Company Name</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="contact_number" value="{{$company->contact_number}}" type="tel" class="validate">
                            <label for="contact_number">Contact Number</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col m6">
                            <select id="company_sector_id">
                                <option>Select Sector</option>
                                @foreach($company_sector as $sector)
                                    <option value="{{$sector->id}}">{{$sector->sector_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="input-field col m6">
                            <input id="turnover" type="text" class="validate">
                            <label for="turnover">Company Turnover</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col m6">
                            <input id="website_url" value="{{$company->website_url}}" type="text" class="validate">
                            <label for="website_url">Website URL</label>
                        </div>
                        <div class="input-field col m6">
                            <select id="category_id" multiple>
                                <option>Select Primary Interest</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{$company->category_id==$category->id?'selected':''}}>{{$category->category_name}}</option>
                                @endforeach
                            </select>
                            <label>Company Primary Interest</label>
                        </div>
                    </div>
                    <div class="row">

                        <div class="input-field col m6">
                            <select id="secondary_category_id" multiple>
                                <option>Select Secondary Interests</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{$company->category_id==$category->id?'selected':''}}>{{$category->category_name}}</option>
                                @endforeach
                            </select>
                            <label>Company Secondary Interest/s</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <textarea id="address_one" class="materialize-textarea">{{$company->address_one}}</textarea>
                            <label for="address_one">Address 1</label>
                        </div>
                        <div class="input-field col m6">
                            <textarea id="address_two" class="materialize-textarea">{{$company->address_two}}</textarea>
                            <label for="address_two">Address 2</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <textarea id="address_three" class="materialize-textarea">{{$company->address_three}}</textarea>
                            <label for="address_three">Address 3</label>
                        </div>
                        <div class="input-field col m6">
                            <input id="city" value="{{$company->city}}" type="text" class="validate">
                            <label for="city">City</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m6">
                            <input id="postal_code" value="{{$company->postal_code}}" type="text" class="validate">
                            <label for="postal_code">Postal Code</label>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col offset-m4 col m8">
                        <button class="btn waves-effect waves-light" style="margin-left:2em;" id="back" name="action">Back
                            <i class="material-icons right">arrow_back</i>
                        </button>
                        <button class="btn waves-effect waves-light" style="margin-left:2em;" id="update-company" name="action">Update
                            <i class="material-icons right">send</i>
                        </button>
                    </div>

                </div>

            </div>
        </div>


    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                $('#back').on('click',function(){
                   window.history.back();
                });
                $('#update-company').on('click',function(){
                    let formData = new FormData();
                    formData.append('company_name', $('#company_name').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('address_one', $('#address_one').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    formData.append('city', $('#city').val());
                    formData.append('notes_text', $('#notes_text').val());
                    formData.append('postal_code', $('#postal_code').val());
                    formData.append('website_url', $('#website_url').val());
                    formData.append('category_id', $('#category_id').val());
                    formData.append('secondary_category_id',$('#secondary_category_id').val());
                    formData.append('company_sector_id', $('#company_sector_id').val());
                    formData.append('turnover', $('#turnover').val());



                    let company = {!! $company !!};
                    $.ajax({
                        url: "/company/"+company.id+'/update',
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success",response);
                            alert(response.message);
                            window.location.href = '/companies';
                        },
                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            console.log("error",message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error",error)
                                if( errors.hasOwnProperty(error) ) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            // $("#modal1").close();
                        }
                    });
                });

            });

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
