@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>

    <input id="user-id-input" disabled hidden value="{{$company->id}}">

    {{ Breadcrumbs::render('view-companies',$company)}}
    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <div class="row center" style="margin-left: 2em;margin-right: 2em;">
                <input hidden disabled value="{{$company->id}}" id="company-id-input">
                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Company Information</h6>
                <a class="btn btn-success purple"
                   onclick="exportCompanyDetails()"
                >Details
                </a>
                <a class="btn btn-success purple"
                   onclick="exportCompanyEmployees()"
                >Employees
                </a>
                <table>
                    <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Contact Number</th>
                        <th>Category</th>
                        <th>Address 1</th>
                        <th>Address 2</th>
                        <th>City</th>
                        <th>Notes</th>
                        <th>Postal Code</th>
                        <th>Website URL</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr><td>{{$company->company_name}}</td>
                        <td> {{$company->contact_number}}</td>
                        <td>{{$company->category->category_name}}</td>
                        <td>{{$company->address_one}}</td>
                        <td>{{$company->address_two}}</td>
                        <td>{{$company->city}}</td>
                        <td>{{$company->notes_text}}</td>
                        <td>{{$company->postal_code}}</td>
                        <td>{{$company->website_url}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            {{--<a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>--}}
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Company Employees</h6>
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="company-employees-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div id="modal1" class="modal modal-fixed-footer">
                <div class="modal-content">
                    <h4>Add New Employee</h4>
                    <div class="row">
                        <form class="col s12">
                            @csrf
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="name" type="text" class="validate">
                                    <label for="name">Name</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="surname" type="text" class="validate">
                                    <label for="surname">Surname</label>
                                </div>

                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <input id="email" type="email" class="validate">
                                    <label for="email">Email</label>
                                </div>
                                <div class="input-field col m6">
                                    <input id="contact_number" type="tel" class="validate">
                                    <label for="contact_number">Contact Number</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <select id="company_id">
                                        <option value="{{$company->id}}">{{$company->company_name}}</option>
                                    </select>
                                    <label>Company Company</label>
                                </div>
                                <div class="input-field col m6">
                                    <select id="category_id" multiple>

                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                                        @endforeach
                                    </select>
                                    <label>Company Category</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <textarea id="address" class="materialize-textarea"></textarea>
                                    <label for="address">Address 1</label>
                                </div>
                                <div class="input-field col m6">
                                    <textarea id="address_two" class="materialize-textarea"></textarea>
                                    <label for="address">Address 2</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col m6">
                                    <textarea id="address_three" class="materialize-textarea"></textarea>
                                    <label for="address">Address 3</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn">Cancel<i class="material-icons right">close</i> </a>
                    <button class="btn waves-effect waves-light" style="margin-left:2em;" id="save-company-employee" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Add New Company Employee" href="{{url('company-employee-create/'.$company->id)}}">
                <i class="large material-icons">add</i>
            </a>

        </div>

    </div>
    @push('custom-scripts')

        <script>
            $(document).ready(function () {
                $('select').formSelect();
                let company = {!! $company !!}
                $(function () {
                    $('#company-employees-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: {
                            'url':'/get-specific-company-employees/'+company.id
                        },
                        columns: [
                            {data: 'name', name: 'name'},
                            {data: 'surname', name: 'surname'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'email', name: 'email'},
                            // {data: "company.company_name",name:'company.company_name'},
                            {data: "address",name:"address"},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
                    });
                    $('select[name="company-employees-table_length"]').css("display","inline");
                });

                $('#save-company-employee').on('click',function(){
                    let formData = new FormData();
                    formData.append('name', $('#name').val());
                    formData.append('surname', $('#surname').val());
                    formData.append('contact_number', $('#contact_number').val());
                    formData.append('email', $('#email').val());
                    formData.append('company_id', $("#company_id").val());
                    formData.append('category_id', $('#category_id').val());
                    formData.append('address', $('#address').val());
                    formData.append('address_two', $('#address_two').val());
                    formData.append('address_three', $('#address_three').val());
                    console.log("company ", formData);

                    $.ajax({
                        url: "{{ route('company-employees.store') }}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        type: 'post',

                        success: function (response, a, b) {
                            console.log("success",response);
                            alert(response.message);
                            window.location.reload();
                        },
                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            console.log("error",message);
                            let errors = response.responseJSON.errors;

                            for (var error in   errors) {
                                console.log("error",error)
                                if( errors.hasOwnProperty(error) ) {
                                    message += errors[error] + "\n";
                                }
                            }
                            alert(message);
                            $("#modal1").close();
                        }
                    });
                });

            });

            function confirm_delete_company(obj){
                var r = confirm("Are you sure want to delete this company?");
                if (r == true) {
                    $.get('/company/delete/'+obj.id,function(data,status){
                        console.log('Data',data);
                        console.log('Status',status);
                        if(status=='success'){
                            alert(data.message);
                            window.location.reload();
                        }

                    });
                } else {
                    alert('Delete action cancelled');
                }
            }

            function exportCompanyDetails(){
                let company_id = $('#company-id-input').val();
                window.location.href = '/export-company-details/' + company_id;
            }

            function exportCompanyEmployees(){
                let company_id = $('#company-id-input').val();
                window.location.href = '/export-company-employees/' + company_id;
            }

        </script>
    @endpush
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
        }
    </style>
@endsection
