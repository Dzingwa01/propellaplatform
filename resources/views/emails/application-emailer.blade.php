<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear <b>{{ $name }}</b>
    <br/>
    <br/>
    Thank you for applying to our program, your application is being processed you can use your email <b>{{ $email }}</b> and password <b>{{ $password }}</b> to login
    and check your application status.
    <br/>
    <br/>
    This is the link you will use to login to your account <a href="{{url('login')}}">Login</a>
    <br/>
    <div class="col s12 card" style="border-radius: 10px;">
        <br>
        <h4 class="center-align"><b>APPLICATION DETAILS</b></h4>
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            @foreach($userQuestionAnswers as $question_answer)
                <h6>{{$question_answer->question_number}} - {{ $question_answer->question_text}} </h6>
                <div class="input-field">
                    <textarea disabled>{{$question_answer->answer_text}}</textarea>
                </div>
            @endforeach
        </div>
    </div>
    <br/>
    Warm regards<br>
    The Propella Team

</div>

</body>
</html>
