
<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear <b>{{ $name }}</b>
    <br/>
    <br/>
    Thank you for applying to our program, your application is being processed you can use your email <b>{{ $email }}</b> and password <b>{{ $password }}</b> to login
    and check your application status.
    <br/>
    <br/>
    This is the link you will use to login to your account <a href="{{url('login')}}">Login</a>
    <br/>
    <br/>
    Warm regards<br>
    The Propella Team

</div>

</body>
</html>
