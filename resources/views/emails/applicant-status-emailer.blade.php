<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear <b>{{ $name }}</b>
    <br/>
    <br/>
    You applied to Propella Business Incubator ICT Program  {{ $email_comment }}.
    Make contact with Propella by with emailing us on reception@propellaincubator.co.za or calling us on 041 502 3700.
    <br/>

    <p>To log into your profile please follow this link.</p>
    <br>
    <p><a href="www.propellaincubator.co.za/login"></a></p>
    <br>
    <p><b>Log in details</b></p>
    <p>User name:  Please use the email address that you applied with</p>
    <p>Password: Password_1234</p>
    <br/>
    <p>With many thanks.</p>
    The Propella Team

</div>

</body>
</html>

