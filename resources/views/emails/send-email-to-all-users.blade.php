<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear <b>{{ $name }}</b>
    <br/>
    <p>We are running a workshop on (name of the workshop) via Zoom which we thought may be of interest to you.</p>
    <br/>
    <p>To view/register this workshop click here: {{$link}}</p>
    <br>
    <p>Once registered you will receive the zoom link to the workshop</p>

    <br>
    <p>With many thanks.</p>
    The Propella Team

</div>

</body>
</html>

