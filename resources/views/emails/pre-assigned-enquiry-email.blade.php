<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <p>Dear {{ $assigned_person }}</p>
    <p>You have assigned to one of HedgeSA's enquiries.</p>
    <p>Please view the enquiry details below:</p>
    <div class="row center">
        <span>
            <b>Title:</b> {{$title}}
            <br>
            <b>Name:</b> {{$name}}
            <br>
            <b>Surname:</b> {{$surname}}
            <br>
            <b>Email:</b> {{$email}}
            <br>
            <b>Contact Number:</b> {{$contact_number}}
            <br>
            <b>Company:</b> {{$company}}
            <br>
            <b>Enquiry Category:</b> {{$category}}
            <br>
            <b>Message:</b> {{$enquiry_message}}
        </span>
    </div>
    <p>Please log in to your account on the Propella Platform to follow up on this enquiry.</p>
    <p>Kind Regards</p>
    <p>Propella</p>
</div>

</body>
</html>
