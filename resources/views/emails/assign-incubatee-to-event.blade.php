<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div class="row">
    <h5>Hi {{$name}}</h5>
    <p> You have now been assigned to {{$event_title}}. This event will take place on {{$event_start}}.  </p>
    <br>
    <p>Please make sure to log in to your Propella dashboard by visiting {{$link}} and then accepting / declining your allocated time slots.</p>
    <br>
    <p>You can use these credentials to log in.</p>
    <p>Email: {{$email}}</p>
    <p>Password: {{$password}}</p>
    <br>
    <br>
    <p>Kind regards,</p>
    <p><b>Future Makers Team at Propella</b></p>
</div>
</body>
</html>

