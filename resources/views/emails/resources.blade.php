<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear <b>{{ $name }}</b>
    <br/>
    <br/>
    A new grant/competition/funding has been loaded onto the platform and you may be interested in it for your venture.
    Have a look by clicking here: <a href="{{url('login')}}">Login</a>

    <br/>

    <br/>
    <br/>
    Kind regards<br>
    Incubator Journey Facilitator

</div>

</body>
</html>

