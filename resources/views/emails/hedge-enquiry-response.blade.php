<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <h6>Dear {{ $name }}</h6>
    <p>We have received your communication and appreciate your interest in Hedge SA.</p>
    <p>We will respond to your contact as soon as possible.</p>
    <p>Kind Regards</p>
    <p>The Team at Hedge SA</p>
</div>

</body>
</html>
