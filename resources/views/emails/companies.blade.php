<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear <b>{{ $name }}</b>
    <br/>
    <br/>
    Message here
    <br/>
    <br/>
    This is the link you will use to login to your account <a href="{{url('login')}}">Login</a>
    <br/>
    <br/>
    Warm regards<br>
    The Propella Team

</div>
