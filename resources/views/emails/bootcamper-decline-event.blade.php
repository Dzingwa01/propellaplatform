<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div class="row">
    <p><b>To whom it may concern</b></p>
    <br>
    <p>{{$user_name}} {{$user_surname}} has declined {{$event}}  </p>
    <br>
    <p>Could you please contact them on {{$user_phone}} or {{$user_email}}.</p>
    <br>
    <p>Once you have arranged for a new time slot, please don't forget to delete the current registration and add a new once. This will
        allow the platform to keep track of the data.</p>
    <br>
    <p>Kind regards,</p>
    <br>
    <p><b>Propella Platform</b></p>
</div>
</body>
</html>
