<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear <b>{{ $name }}</b>
    <br/>
    <p>PROPELLA ICT PROGRAMME – MARCH 2021 </p>
    <br/>
    <p>We thank you for your interest in
        the programme and coming in to present your business idea to our selection panel.
        was lovely to meet you and have you on the bootcamp. </p>
    <br/>
    <p>Unfortunately, based on the funders criteria, your application did not make the top 50 spots prescribed by our funder.</p>
    <br/>
    <p>Please do not let this demotivate you from pursuing your vision and ultimately reaching the goals you have set out for yourself and your team.</p>
    <br/>
    <p>We wish you the best of luck in the further development of your idea.</p>
    <br/>
    <p>We would love you to stay connected with Propella, so please follow our social media channels and join our
        future social workshops and events.</p>
    <br>
    Kind regards<br>
    The Propella Team

</div>

</body>
</html>

