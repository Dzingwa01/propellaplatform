<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    <p>Dear {{ $assigned_person }}</p>
    <p>You have assigned to one of HedgeSA's enquiries.</p>
    <p>Please view the enquiry details below:</p>
    <div class="row center">
        <span>
            <b>Title:</b> {{$title}}
            <br>
            <b>Name:</b> {{$name}}
            <br>
            <b>Surname:</b> {{$surname}}
            <br>
            <b>Email:</b> {{$email}}
            <br>
            <b>Contact Number:</b> {{$contact_number}}
            <br>
            <b>Company:</b> {{$company}}
            <br>
            <b>City:</b> {{$city}}
            <br>
            <b>Heard About Us:</b> {{$heard_about_us}}
            <br>
            <b>Enquiry Category:</b> {{$category}}
            <br>
            <b>Message:</b> {{$enquiry_message}}
            <br>
            <b>Initial Contact Person:</b> {{$contacted_by}}
            <br>
            <b>Initial Report:</b> {{$report}}
            <br>
            <b>Next Step:</b> {{$next_step}}
            <br>
            <b>Date Assigned:</b> {{$date_assigned}}
        </span>
    </div>
    <p>Kind Regards</p>
    <p>The Team at Hedge SA</p>
</div>

</body>
</html>
