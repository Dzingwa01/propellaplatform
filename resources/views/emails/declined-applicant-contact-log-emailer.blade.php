<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear <b>{{ $name }}</b>
    <br/>
    <br/>
    Thank you for applying for a Propella programme. {{ $comment }} .
    <br/>
    <br/>
    We wish you everything of the best with your idea.
    <br/>
    <br/>
    Kind regards<br>
    The Propella Team

</div>

</body>
</html>

