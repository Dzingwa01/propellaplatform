@component('mail:subject')
<h5>{{$data->name}}</h5>
@endcomponent

@component('mail:message')
    <h5>Hi Propella,</h5>

    <p>{{$data->message}}</p>
@endcomponent