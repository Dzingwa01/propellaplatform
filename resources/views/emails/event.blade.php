<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>

    Dear <b>{{ $name }}</b>
    <br/>
    <br/>
    Thank you for registering for the {{ $event_name }} on {{ $event_date }}.
    <br/>
    <br/>
    We look forward to seeing you.
    <br/>
    <br/>
    Warm regards<br>
    The Propella Team

</div>

</body>
</html>
