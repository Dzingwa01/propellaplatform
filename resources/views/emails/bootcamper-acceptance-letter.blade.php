<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div class="row center">
    <h5><b>START UP BOOTCAMP AT PROPELLA - FEBRUARY 2022</b></h5>
</div>

<div class="row">
    <p>Congratulations, it is with great pleasure that we wish to inform you that your entry to the Start Up Bootcamp has been successful.</p>
    <br>
    <p>The Boot Camp is facilitated via Zoom over 2 days (Monday’s and Tuesday’s).  Dates have been allocated to you.  To find out what they are login to the Propella Platform… BUT WAIT, please read on first….</p>
    <br>
    <p>Day 1 starts at 08h30 sharp for registration and 09h00 on Day 2.  It runs until 16h00 on both days, therefore if you are working full time, this programme is unfortunately not for you</p>
    <br>
    <p>During the Bootcamp we will assist you in refining your business idea, address business fundamentals, customer validation amongst others and get you pitch ready.</p>
    <br>
    <p>Have a pen and paper handy, ensure you have a quiet place, a reliable laptop, phone or tablet to access the zoom workshop and of course bring your “A” game.  </p>
    <br>
    <p>We will be providing data for duration of Bootcamp <span style="color: red">(only if you log into the Propella Platform and provide us with all the information that we need)</span> but you will need to have your own data to join us on Day 1.</p>
    <br>
    <p>All work and no play is not allowed so there will be some fun interactions and comfort breaks.</p>
    <br>
    <p>Please confirm that you are willing and available to join us for the journey that lies ahead:</p>
    <br>
    <p>•	<span><b>Login to the platform</b></span> <a href="http://www.thepropella.co.za"></a> Username: <span><b>Your email address</b></span> and your password: <span><b>Password_1234</b></span> </p>
    <p>•	Accept your Bootcamp </p>
    <p>•	Next confirm your cell phone number and service provider on the data button – for data</p>
    <p>•	Then upload a copy of your id</p>
    <p>•	We will also need your proof of address not older than 3 months</p>
    <p>•	Read and accept that Non-Disclosure Agreement that protects each other’s ideas</p>
    <p>•	For those with existing companies upload your CIPC documents onto the platform.  Having a registered company is not a pre-requisite</p>
    <br>
    <p>If the dates don’t suit you, then please call us on 041 502 3700 so that we can re-assign you.</p>
    <br>
    <p>If you are no longer interested in attending then please email  mailto:reception@propellaincubator.co.za </p>
    <br>
    <p>For any queries contact  reception@propellaincubator.co.za</p>
    <br>
    <p>To log into your profile please follow this link. <a href="{{url('login')}}">Login</a></p>
    <br>
    <br>
    <p><b>Log in details</b></p>
    <p>User name:  Please use the email address that you applied with</p>
    <p>Password: Password_1234</p>
    <br>
    <p>Congrats again!</p>
    <br>
    <p>Kind regards,</p>
    <br>
    <p><b>Team at propella</b></p>
    <br>
</div>
</body>
</html>
