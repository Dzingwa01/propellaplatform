<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Dear <b>{{ $name }}</b>
    <br/>
    <p>Your Shadow Board is due to occur tomorrow.</p>
    <p>Please ensure you have competed your<span><b>3 Monthly Evaluation Form on your Mentee prior to your Shadow Board</b></span> by logging in to : www.thepropella.co.za .</p>
    <p>If you have not completed it then please ensure you arrive 10 minutes early for your Shadow Board and complete on our Propella tablet before your Shadow Board starts.</p>
    <p>This is important to ensure the success of the Mentoring Relationship and to enable the Mentee to keep moving forward.</p>
    <p>Your Mentee is required to provide feedback on <span><b>Action Items</b></span> from previous Shadow Board.
    </p>
    <p>The agenda for the Shadow Board is below in case you require it:
    </p>
    <p><b> SHADOW BOARD MEETING</b></p>
    <p>1)	Present: Incubatee / Mentor / Advisor / Incubation Manager / AN Other</p>
    <p>2)	Meets quarterly at Propella</p>
    <p>3)	Incubatee reviews roadmap and discusses goals set for next quarter </p>
    <p><b>Agenda</b></p>
    <p> 1)	Strategy & Stakeholders</p>
    <p> 2)	Marketing</p>
    <p> 3)	Sales (achieved and pipeline)</p>
    <p> 4)	Finance (performance against budget)</p>
    <p> 5)	Legal & Compliance (including HR)</p>
    <p> 6)	Operations (Process / Admin)</p>
    <p> 7)	Product / Technology Development</p>
    <p> 8)	Entrepreneur / Personal Development</p>
    <p> 9)	General</p>
    <p> 10)	Dates for the next 3 months</p>
    <p> -	Incubatee / Mentor</p>
    <p> -	Propella Advisor</p>
    <p> -	Next Shadow Board</p>
    <br>
    <p>With many thanks.</p>
    The Propella Team

</div>

</body>
</html>

