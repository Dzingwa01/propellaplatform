@extends('layouts.panelist-layout')

@section('content')

    @foreach($user->roles as $user_role)
            <div class="col s12 m4" style="height: 400px;">
                <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: #1a237e; ">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i> Panel Interview </span>
                        <p style="align-content: center; ">This is where you will see the applicants assigned to your panel interview. </p>
                        <br/>
                        <br/>
                    </div>
                    <div class="card-action center" style="border-radius: 15px;">
                        <a class="btn" href="/panelist-show-industrial-applicants"
                           style="cursor: pointer; color: white;background-color: #1a237e;"><b>View Applicants</b></a>
                        <a class="btn" href="/panelist-show-ict-applicants"
                           style="cursor: pointer; color: white;background-color: #1a237e;"><b>View Bootcampers</b></a>
                        <a class="btn" href="/panelist-show-ventures"
                           style="cursor: pointer; color: white;background-color: #1a237e;"><b>View Ventures</b></a>
                    </div>
                </div>
            </div>
    @endforeach

@endsection
