@extends('layouts.clerk-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('emailer-index')}}
    <div class="container-fluid">
        <div id="top-section">
            <div class="row " >
                <div style="float: right; margin-top:2em;margin-right: 2em;" >
                    <span id="employees-selected-count">No recipients selected</span>
                    <button class="btn waves-effect waves-light"  disabled style="margin-right:2em;" id="send-mail">Send Email
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Propella Contacts</h6>
                <div class="col s12">
                    <table class="table table-bordered" style="width: 100%!important;" id="company-employees-table">
                        <thead>
                        <tr>
                            <th>Select</th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Contact Number</th>
                            <th>Email</th>
                            <th>Interests</th>
                            <th>Company</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div id="bottom-section" hidden  >
            <h6 style="text-transform: uppercase;font-weight: bolder;margin-top:2em;text-align: center;">Mail Send</h6>
            <div class="row" >
                <form id="send-bulk-email-form" class="col s12 card hoverable" style=" margin-right:2em;margin-right: 2em;">
                    @csrf
                    <div class="row">
                        <div class="input-field col m12" style=" margin-right: 2em!important;">
                            <i class="material-icons prefix">people_outline</i>
                            <textarea id="selected-recipients" class="materialize-textarea"></textarea>
                            {{--<label for="selected-recipients">Selected Recipients</label>--}}
                        </div>

                    </div>
                    <div class="row">
                        <div class="input-field col m12" style="margin-right: 2em!important;;">
                            <i class="material-icons prefix">mode_edit</i>
                            <input id="email_subject" type="text" class="validate">
                            <label for="email_subject">Email Subject</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field" style="bottom:0px!important;margin-left: 1em;margin-right: 2em!important;;">
                            <div class="btn">
                                <span>Attachments</span>
                                <input id="attachments" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                            <span id="message"></span>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 1em;margin-right: 2em;">
                        <label for="editor">Message</label>
                        <div id="editor" >

                        </div>

                        {{--<textarea style="margin-left:1em;" id="summernote" name="message" class="materialize-textarea"></textarea>--}}
                    </div>
                    <div class="row" style="margin-top:2em;">
                        <div class="col offset-m4">
                            <a id="cancel-send" class="waves-effect waves-green btn modal-close">Cancel<i class="material-icons right">close</i> </a>
                            <button class="btn waves-effect waves-light" style="margin-left:2em;" id="send-bulk-email" name="action">Save
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <style>
            .check {
                opacity: 1 !important;
                pointer-events: auto !important;
            }

            table.dataTable tbody tr.selected {
                background-color: #B0BED9;
            }
            nav {
                                margin-bottom: 0;
                                background-color: grey;
                                padding: 5px 16px;
                            }
        </style>

    </div>
    @push('custom-scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.14.2/highlight.min.js"></script>
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>


        <script>


            $(document).ready(function () {
                $('select').formSelect();
                // $('#summernote').summernote();
                // var Bold = Quill.import('formats/bold');
                // Bold.tagName = 'B';   // Quill uses <strong> by default
                // Quill.register(Bold, true);
                // var FontAttributor = Quill.import('attributors/class/font');
                // FontAttributor.whitelist = [
                //     'sofia', 'slabo', 'roboto', 'inconsolata', 'ubuntu'
                // ];
                // Quill.register(FontAttributor, true);
                // var ColorClass = Quill.import('attributors/class/color');
                // var SizeStyle = Quill.import('attributors/style/size');
                // Quill.register(ColorClass, true);
                // Quill.register(SizeStyle, true);
                var toolbarOptions = [
                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                    ['blockquote', 'code-block'],

                    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
                    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                    [{ 'direction': 'rtl' }],                         // text direction

                    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                    [{ 'font': [] }],
                    [{ 'align': [] }],
                    ['image'],
                    ['clean']                                         // remove formatting button
                ];

                var quill = new Quill('#editor', {
                    modules: {
                        'history': {          // Enable with custom configurations
                            'delay': 2500,
                            'userOnly': true
                        },
                        'syntax':true,
                        toolbar: toolbarOptions
                    },
                    theme: 'snow'
                });
                $(function () {
                    $('#company-employees-table').DataTable({
                        processing: true,
                        serverSide: true,
                        paging: true,
                        responsive: true,
                        scrollX: 640,
                        ajax: {
                            'url':'/get-company-employees-mailer'
                        },
                        columns: [
                            {data: 'action', name: 'action', orderable: false, searchable: false},
                            {data: 'name', name: 'name'},
                            {data: 'surname', name: 'surname'},
                            {data: 'contact_number', name: 'contact_number'},
                            {data: 'email', name: 'email'},
                            {data: "categories[0].category_name",name:"categories[0].category_name"},
                            {data: "company.company_name",name:'company.company_name'},

                        ]
                    });
                    $('select[name="company-employees-table_length"]').css("display","inline");
                });

                $('#company-employees-table tbody').on('click', 'tr', function () {
                    $(this).toggleClass('selected');
                    count_selected();
                });

                $('#send-mail').on('click',function(){
                    $('#top-section').hide();
                    $('#bottom-section').show();
                    $('#employees-selected-count').empty();
                    let selected_employees = $("table input:checkbox:checked").map(function () {
                        return $(this).val();
                    }).get();
                    let employees = {!! $companiesEmployees !!};
                    let recipients ="";

                    for(var i=0;i<selected_employees.length;i++){
                        let employee = employees.find(employee => employee.id == employees[i].id);
                        console.log('Employee',employee);
                        recipients = recipients + employee.email +";";
                    }
                    $('#selected-recipients').val(recipients);
                    M.textareaAutoResize($('#selected-recipients'));
                });

                $('#cancel-send').on('click',function(){
                    $('#top-section').show();
                    $('#bottom-section').hide();
                });

                $('#assign-stock').on('click', function () {
                    $('#selected-products').empty();
                    let selected_employees = $("table input:checkbox:checked").map(function () {
                        return $(this).val();
                    }).get();
                    let employees = {!! $companiesEmployees !!};

                    for(var i=0;i<selected_employees.length;i++){
                        let employee = employees.find(employee => employee.id == employees[i].id);
                        console.log('Employee',employee);
                        // $('#selected-products').append('<tr><td>'+product.barcode+'<td>'+product.product_name+' - '+ product.quantity+' Available</td><td> <input style="width:80px!important;" class="qty" min="0" max="'+product.quantity+'" required id="'+product.id+'" type="number" class="validate"/></td></tr>');
                    }

                });

                function count_selected() {
                    var count = $("table input[type=checkbox]:checked").length;
                    $('#employees-selected-count').empty();
                    if (count <= 0) {
                        $('#send-mail').prop("disabled", true);
                        $('#employees-selected-count').append(' No Recipients Selected');
                    } else {
                        $('#send-mail').prop("disabled", false);
                        $('#employees-selected-count').append(count + ' Recipients Selected');
                    }
                }
            });
//admin
        </script>
    @endpush
@endsection
