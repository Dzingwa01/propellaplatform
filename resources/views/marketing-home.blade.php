@extends('layouts.marketing-layout')

@section('content')
    <br>
    <br>
    {{ Breadcrumbs::render('marketing')}}
    <input hidden disabled id="user-id-input" value="{{$user->id}}">

    <div class="row" style="margin-top:4em;">
        <div class="col s12 m4" style="height: 400px">
            <div class="card  darken-1 hoverable" style="border-radius: 15px;background-color:rgba(0, 47, 95, 1); height: 340px">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">people</i>-->&nbsp;&nbsp;All Incubatees</span>
                    <br/>
                    <br/>
                    <table>
                        <thead>
                        <tr>
                            <th>Incubatees</th>
                            <th>Pending Applicants</th>
                            <th>Declined Applicants</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{count($incubatees)}}</td>
                            <td>{{count($pending_applicants)}}</td>
                            <td>{{count($declined_applicants)}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-action" style="border-radius: 15px">
                    <a class="btn" href="{{url('venture')}}" style="color: white;background-color:rgba(0, 47, 95, 1)"><b>View Incubatees</b></a>
                    <a class="btn" href="{{url('applicants')}}" style="color: white;background-color:rgba(0, 47, 95, 1)"><b>View Applicants</b></a>
                </div>
            </div>
        </div>
        <div class="col s12 m4" style="height: 400px">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color:#454242; height: 340px">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">people</i>-->&nbsp;&nbsp;ICT</span>
                    <br/>
                    <p style="font-weight: bolder">{{'Total ICT Incubatees - '.$ict_count}}</p>
                    <br/>
                    <table>
                        <thead>
                        <tr>
                            <th>S 1</th>
                            <th>S 2</th>
                            <th>S 3</th>
                            <th>S 4 </th>
                            <th>S 5</th>
                            <th>S 6</th>
                            <th>Exit</th>
                            <th>Alumni</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$ict_stage_1}}</td>
                            <td>{{$ict_stage_2}}</td>
                            <td>{{$ict_stage_3}}</td>
                            <td>{{$ict_stage_4}}</td>
                            <td>{{$ict_stage_5}}</td>
                            <td>{{$ict_stage_5}}</td>
                            <td>{{$ict_stage_Alumni}}</td>
                            <td>{{$ict_stage_Exit}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-action center" style="border-radius: 15px">
                    <a class="btn" href="{{url('ict-venture')}}" style="color: white;background-color:#454242;"><b>View ICT Incubatees</b></a>
                </div>
            </div>
        </div>
        <div class="col s12 m4" style="height: 400px">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: rgba(0, 121, 52, 1); height: 340px">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">people</i>-->&nbsp;&nbsp;Industrial</span>
                    <br/>
                    <p style="font-weight: bolder">{{'Total Industrial Incubatees - '.$ind_count}}</p>
                    <br/>
                    <table>
                        <thead>
                        <tr>
                            <th>S 1</th>
                            <th>S 2</th>
                            <th>S 3</th>
                            <th>S 4</th>
                            <th>S 5</th>
                            <th>S 6</th>
                            <th>Exit</th>
                            <th>Alumni</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$ind_stage_1}}</td>
                            <td>{{$ind_stage_2}}</td>
                            <td>{{ $ind_stage_3}}</td>
                            <td>{{$ind_stage_4}}</td>
                            <td>{{$ind_stage_5}}</td>
                            <td>{{ $ind_stage_6}}</td>
                            <td>{{$ind_stage_Alumni}}</td>
                            <td>{{ $ind_stage_Exit}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-action center" style="border-radius: 15px">
                    <a class="btn" href="{{url('ind-only-venture')}}" style="color: white;background-color: rgba(0, 121, 52, 1)"><b>View Industrial Incubatees</b></a>
                </div>
            </div>
        </div>
        <div class="col s12 m4" style="height: 400px;">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: rgba(0, 47, 95, 1); height: 340px">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder"><!--<i class="small material-icons">perm_contact_calendar</i>-->   Events</span>
                    <p style="font-weight: bolder">Total number of Events - {{$all_events}}</p>
                    <br/>
                    <p style="align-content: center; font-weight: bolder">This is where you view your events</p>
                    <br/>

                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="btn" id="user-events-button" style="cursor: pointer; color: white;background-color: rgba(0, 47, 95, 1);"><b>View Events</b></a>
                </div>
            </div>
        </div>
        <div class="col s12 m4" style="height: 400px;">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: rgba(0, 121, 52, 1); height: 340px">
                <div class="card-content white-text" style="height: 180px">
                    <span class="card-title" style="font-weight: bolder"><!--<i
                            class="small material-icons">border_color</i>-->   Book Venues</span>
                    <br/>
                    <p style="align-content: center; font-weight: bolder">This is where you view your booked venues</p>
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="card-action center" style="border-radius: 15px;">
                    <a class="btn" id="user-venue-button" href="{{url('/show-booking/'.$user->id)}}"
                       style="cursor: pointer; color: white;background-color: rgba(0, 121, 52, 1); "><b>View Venues</b></a>
                </div>
            </div>
        </div>
        <div class="col s12 m4" style="height: 400px">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color:#454242; height: 330px">
                <div class="card-content white-text">
                    <span class="card-title" style="font-weight: bolder">Reporting section</span>
                    <br/>
                    <p style="align-content: center; font-weight: bolder">Here we will have pre-designed reports.</p>
                    <br/>

                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="card-action center" style="border-radius: 10px;">
                    <a class="btn"  href="{{url('/show-reports')}}" style="cursor: pointer; color: white;background-color:#454242;"><b>Show reports</b></a>
                </div>
            </div>
        </div>
        <!--<div class="col s12 m4" style="height: 400px">
            <div class="card darken-1 hoverable" style="border-radius: 15px;background-color:#454242;">
            </div>
            <div class="card-action" style="border-radius: 15px">
                <a class="btn" id="user-events-button" style="color: white;background-color:#454242;"><b>View Events</b></a>
            </div>
        </div>-->
    </div>

    @foreach($user->roles as $user_role)
        @if($user_role->name == 'panelist')
            <div class="col s12 m4" style="height: 400px;">
                <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: #1a237e; ">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i> Panel Interview </span>
                        <p style="align-content: center; ">This is where you will see the applicants assigned to your panel interview. </p>
                        <br/>
                        <br/>
                    </div>
                    <div class="card-action center" style="border-radius: 15px;">
                        <a class="btn" href="/panelist-show-industrial-applicants"
                           style="cursor: pointer; color: white;background-color: #1a237e; "><b>View Applicants</b></a>
                        <a class="btn" href="/panelist-show-ict-applicants"
                           style="cursor: pointer; color: white;background-color: #1a237e; "><b>View Bootcampers</b></a>
                        <a class="btn" href="/panelist-show-ventures"
                           style="cursor: pointer; color: white;background-color: #1a237e;"><b>View Ventures</b></a>
                    </div>
                </div>
            </div>
        @endif
    @endforeach

    @if($user_enquiries != null or $user_pre_assigned_enquiries != null)
        @if(count($user_enquiries) > 0 or count($user_pre_assigned_enquiries) > 0)
            <div class="col s12 m4" style="height: 400px;">
                <div class="card darken-1 hoverable" style="border-radius: 15px;background-color: #1a237e; ">
                    <div class="card-content white-text">
                        <span class="card-title" style="font-weight: bolder"><i class="small material-icons">border_color</i> Manage General Enquiries </span>
                        <p style="align-content: center; ">This is where you will see the enquiries that you have been assigned to. </p>
                        <br/>
                        <br/>
                    </div>
                    <div class="card-action center" style="border-radius: 15px;">
                        <a href="/pre-assigned-enquiry-index"
                           style="cursor: pointer; color: white;"><b>View Pre-Assigned Enquirires</b></a>
                        <a href="/assigned-enquiry-index"
                           style="cursor: pointer; color: white;"><b>View Assigned Enquirires</b></a>
                    </div>
                </div>
            </div>
        @endif
    @endif

    {{--<button id="user-events-button">Calendar</button>--}}

    <script>
        $(document).ready(function () {
            let stored_event_id = sessionStorage.getItem('stored_event_id');
            let stored_found_out = sessionStorage.getItem('stored_found_out_via');
            let user_id = $('#user-id-input').val();

            if (stored_event_id !== "" && stored_found_out !== "") {
                let formData = new FormData();
                formData.append('event_id', stored_event_id);
                formData.append('found_out_via', stored_found_out);
                formData.append('user_id', user_id);

                let url = "{{route('user.storeUserEventViaPublicEventPage')}}";
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        sessionStorage.setItem('stored_event_id', "");
                        sessionStorage.setItem('stored_found_out_via', "");
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            }
        });

        $('#user-events-button').on('click', function () {
            let user_id = $('#user-id-input').val();
            window.location.href = '/users/show-events/' + user_id;
        });
    </script>
    <style>
        nav {
            margin-bottom: 0;
            background-color: grey;
            padding: 5px 16px;
        }
    </style>

@endsection
