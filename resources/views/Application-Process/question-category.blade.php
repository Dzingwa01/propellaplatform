@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Questions-category/Questions-category.css"/>
    <div id="categoryDesktop">
        <div class="heading">
            <br>
            <br>
            <h4 class="heading__title">Which Programme Should You Apply For?</h4>
            <p class="heading__credits"><a class="heading__link" target="_blank" href="#">Apply Below</a></p>
        </div>
        <div class="main-container">
            <!-- flip-card-container -->
            <div class="flip-card-container" style="--hue: 220">
                <div class="flip-card">

                    <div class="card-front" style="background: radial-gradient(#007934, #00b34d);">
                        <div class="card__icon"><i class="fas fa-briefcase"></i></div>
                        <h2 class="card__title">Already have a business?</h2>
                        <p class="card__apply">
                            <a class="card__link" href="#">Hover Card <i class="fas fa-arrow-right"></i></a>
                        </p>
                    </div>

                    <div class="card-back" style="background: radial-gradient(#007934, #00b34d);">
                        <h5 class="back-title">Rapid Accelerator Programme</h5>
                        <p class="back-text">
                            The Propella Township Incubator Rapid Accelerator Programme (RAP) is designed to support
                            existing businesses who are struggling to survive and / or grow due to difficult and
                            challenging economic times.</p>
                        <p  class="back-text">
                            The Programme will run over a 10 to 12-week time frame and offers selected SMME’s virtual incubation support
                            as well as access to a pool of experts who will provide advisory...
                            <a class="modal-trigger" style="color: navy" href="#modalrap"><b>Read More</b></a>
                        </p>


                        <button class="applybutton"><a href="/Application-Process/rap-application">APPLY</a></button>
                    </div>

                </div>
            </div>
            <!-- /flip-card-container -->

            <!-- flip-card-container -->
            <div class="flip-card-container" style="--hue: 170">
                <div class="flip-card">

                    <div class="card-front" style="background: radial-gradient(#002f5f, #0054a8);">
                        <div class="card__icon"><i class="fas fa-bolt"></i></div>
                        <h2 class="card__title">Have a prototype?</h2>
                        <p class="card__apply">
                            <a class="card__link" href="#">Hover Card <i class="fas fa-arrow-right"></i></a>
                        </p>
                    </div>

                    <div class="card-back" style="background: radial-gradient(#002f5f, #0054a8);">
                        <h5 class="back-title">Propella Industrial Programme</h5>
                        <p class="back-text">
                            This programme aids applicants that are seeking assistance in developing their innovative products, processes and/or services within the following areas:
                            <br>
                            <br>
                            • Renewable Energy
                            <br>
                            • Energy Efficiency
                            <br>
                            • Internet of Things
                            <br>
                            • Smart City solutions...
                            <a class="modal-trigger" style="color: red" href="#modalindustrial"><b>Read More</b></a>
                        </p>
                        <button class="applybutton"><a href="https://www.f6s.com/tia-seed-fund-proposal/apply" target="_blank">APPLY</a></button>
                    </div>

                </div>
            </div>
            <!-- /flip-card-container -->

            <!-- flip-card-container -->
            <div class="flip-card-container" style="--hue: 350">
                <div class="flip-card">

                    <div class="card-front" style="background: radial-gradient(#454242, #5e5a5a);">
                        <div class="card__icon"><i class="fas fa-lightbulb"></i></div>
                        <h2 class="card__title">Just an idea?</h2>
                        <p class="card__apply">
                            <a class="card__link" href="#">Hover Card <i class="fas fa-arrow-right"></i></a>
                        </p>
                    </div>

                    <div class="card-back" style="background: radial-gradient(#454242, #5e5a5a);">
                        <div id="pbi">
                            <h5 class="back-title">Technology Innovation Programme</h5>
                            <p class="back-text">
                                The Propella ICT Programme, is an intensive "from concept to market" process run...
                                <br>
                                <a class="modal-trigger" style="color: navy" href="#modalpbi"><b>Read More</b></a>
                            </p>
                            <button class="applybutton"><a href='/Application-Process/pbi-ict-application'>APPLY</a></button>
                            <h5 style="margin-top: 5vh" class="back-title">Propella Township Innovation</h5>
                            <p class="back-text">
                                Propella Township Incubator is calling all aspiring tech / ICT entrepreneurs to join our first..
                                <br>
                                <a class="modal-trigger" style="color: navy" href="#modalict"><b>Read More</b></a>
                            </p>
                            <button class="applybutton"><a href="/Application-Process/pti-ict-application/">APPLY</a></button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /flip-card-container -->
        </div>
    </div>
    {{--        Modals       --}}

    <div id="modalrap" class="modal">
        <div class="modal-content">
            <h4><b>RAPID ACCELERATOR PROGRAMME</b></h4>
            <br>
            <p>The Rapid Accelerator Programme (RAP) will run over a 10 to 12-week time frame and offers selected SMME’s virtual incubation support
                as well as access to a pool of experts who will provide advisory support and guidance for the entrepreneurs to fast-track
                their business development and growth. </p>

            <p>It is a fast-paced enterprise development support programme which requires participants to be committed and be willing to action rapid business
                turnaround or re-engineering or business strategies and solutions which can produce results over a short-period of time.</p>
            <br>
            <p><b>To participate in Propella RAP, the following criteria must be met:</b> </p>
            <br>
            <p>• Business should be operational for at least 6 months (minimum). </p>
            <p>• Must be a formally registered business with CIPC.</p>
            <p>• Able to provide proof of turn-over. </p>
            <p>• Employ a minimum of 2 staff members.</p>
            <p>• Provide proof of revenue-generation and customer base.</p>
            <p>• Must be based in the Nelson Mandela Bay Metropole.</p>
            <br>
            <p><b>Benefits of the Participating in Propella RAP:</b></p>
            <br>
            <p>• Help you to grow your business through a structured support programme.</p>
            <p>• Fast paced business development and growth programme.</p>
            <p>• Provide you with solutions to improve your business processes and reduce operating costs.</p>
            <p>• Assist you to increase your customer and revenue base.</p>
            <p>• Provide you with expert coaching and mentorship for improved business management.</p>
            <p>• Assist you in unlocking new products and services to stimulate product or process diversification.</p>
            <p>• Opportunity for you to expand your eco system and stakeholder networks within your sector.</p>
            <p>• Offer you support for business turnaround solutions as businesses adapt to a new- norm.</p>
            <p>• Be part of innovative and driven ventures seeking knowledge and open to knowledge transfer and sharing.</p>
            <p>• Facilitate access to market linkages opportunities.</p>
            <p>• Facilitate access and linkage to potential funding partners and investors.</p>
            <p>• Access to a Technical Advisor.</p>
            <p>• Access to a fulltime Business development Officer.</p>
            <p>• Access to Makerspace Product Development Laboratory with available technologies for new product exploration and development.</p>
            <p>• Access and support to develop a Minimal Viable Product (MVP for new product discovery and development.</p>
            <br>
            <p>If you feel that you meet the criteria as mentioned above, kindly complete the linked RAP Application Form. </p>
        </div>
    </div>

    <div id="modalindustrial" class="modal">
        <div class="modal-content">
            <h4><b>Propella Industrial Programme </b></h4>
            <br>
            <p>This programme aids applicants that are seeking assistance in developing their innovative products, processes and/or services within the following areas:</p>
            <br>
            <p>• Renewable Energy </p>
            <p>• Energy Efficiency </p>
            <p>• Advanced Manufacturing, Automation, and Industry 4.0</p>
            <p>• Internet of Things</p>
            <p>• Smart City solutions</p>
            <br>
            <p>The industrial incubator will consider applicants who already have a proof-of-concept or early stage prototype.
                Applicants are required to make an online submission on the website. Once the applications are screened internally
                and align to Propella’s vision and focus areas, then applicants would be required to present their innovations to a panel.
                Should the panel endorse the application, the start of a 3-year journey with Propella would commence. </p>
            <p>Propella offers both virtual and physical incubation services. Prototyping support facilities such as 3D printing,
                3D scanning and small part machining and moulding are available, together with a well-resourced workshop that has all
                the basic tools required for manufacturing. There is also a network of skilled technical and business support.
                If there are products that demonstrate great growth / market potential, Propella would also be willing to engage in
                ‘cash for equity’ discussions. </p>
            <br>
            <p>Content covered for Industrial innovators is determined by an in-depth diagnostic conducted on the business and technology as
                the venture enters the programme. A journey is charted and agreed on that best utilises the resources made available by
                Propella and the objective and times frames of the innovator. </p>
            <p>Typically, an advisor and mentor are assigned to the innovator who also receives access to the Propella Content Platform
                and bi-weekly workshops offered by Propella.</p>
            <br>
            <p>The following key development stages are covered over a maximum period of 3 years.</p>
            <p>• Prototype finalisation testing and trialling</p>
            <p>• Go to market preparation</p>
            <p>• Growth scenarios, preparation, and scaling of the business</p>
        </div>
    </div>

    <div id="modalict" class="modal">
        <div class="modal-content">
            <h4><b>Calling for Innovative Tech or ICT Ideas!</b></h4>
            <br>
            <p>Propella Township Incubator is calling all aspiring tech / ICT entrepreneurs to join our first cohort of
                incubatees at our new Propella Township Incubator (Neave Township).</p>
            <br>
            <p>  All you need is an innovative business idea in the ICT or Tech space, and we will help the selected
                entrepreneurs develop and grow their ideas into viable and profitable businesses.</p>
            <br>
            <p>The call for ideas (must be innovative and within the ICT/Technology/4IR space) is open to all members of
                the public but preference will be given to the following:</p>
            <br>
            <p>• Youth (18 to 35) </p>
            <p>• South African residents </p>
            <p>• Female</p>
            <p>• Disabled</p>
            <p>• Township based</p>
            <br>
            <br>

            <p>(Hot Tip – Be sure to complete ALL the information requested on the form … the last thing you want is
                to be disqualified for an incomplete form!) </p>
            <br>
            <p>Entries received will be reviewed and the top 30 submissions will be invited, via email, to attend an intensive
                two-day bootcamp. The bootcamp will provide the entrepreneur with the opportunity to refine and polish
                their idea by addressing business fundamentals, customer validation and getting them ready to pitch
                their idea to potential funders. </p>
            <br>
            <p>The bootcamp will take place via Zoom and attendance of both days is required.</p>
            <p>This may be the opportunity you have been looking for to turn that big idea into reality!</p>
            <br>
            <p>For any additional information or queries contact Mara at: <a href="mailto:mjacobs@propellaincubator.co.za">mjacobs@propellaincubator.co.za </a> or on 041 502 3700. </p>
        </div>
    </div>
    <div id="modalpbi" class="modal">
        <div class="modal-content">
            <h4><b>Propella ICT Programme</b></h4>
            <br>
            <p>Propella ICT Programme, is an intensive programme "from concept to market" process run by Propella
                Business Incubator for IT innovators.</p>
            <br>
            <p>• You need a great idea and some determination</p>
            <br>
            <p>• An app, a service or an IT application that no-one else has thought of yet or requires help to
                commercialise and scale</p>
            <br>
            <p>• OR, you need a product or service that will facilitate the commercialisation and growth of a
                tech business eg social media, bookkeeping, CAD CAM design, software development etc</p>
            <br>
            <p>• Idea must fit into the operations of a Smart City or Community, such as Industry 4.0, IoT, etc.</p>
            <br>
            <p>• Approximately 100 ventures are selected to attend a two day Stage 1 “Bootcamp”</p>
            <br>
            <p>• No cost to participate</p>
            <br>
            <p>• You don’t need to be trading yet</p>
            <br>
            <p>• You need to be in start up phase and require incubation support</p>
            <br>
            <p>• But there are also no free lunches – you will be expected to invest at least eight hours a week
                in Propella workshops, and even longer doing your research and groundwork.</p>
            <br>
            <p>• Convince the panel, via a video pitch, that your idea has potential to turn into a business to become
                one of the 40 ventures who progress to Stage 2</p>
            <br>
            <p>• Stage 2 is eight weeks on a business incubation journey to validate and prove that it’s a
                viable opportunity.</p>
            <br>
            <p>• Crack the nod for Stage 3 for an additional three months of business acceleration to obtain your first
                revenue paying customer.</p>
            <br>
            <p>This is the chance you've been looking for to make your big idea a reality.</p>
        </div>
    </div>
    <br>
    <br>

    {{--       Mobile Side      --}}

    <div id="categoryMobile">
        <div class="heading">
            <br>
            <br>
            <h4 class="heading__title">Which Programme Should You Apply For?</h4>
            <p class="heading__credits"><a class="heading__link" target="_blank" href="#">Apply Below</a></p>
        </div>
        <div class="main-container">
            <!-- flip-card-container -->
            <div class="flip-card-container" style="--hue: 220">
                <div class="flip-card">

                    <div class="card-front" style="background: radial-gradient(#007934, #00b34d);">
                        <div class="card__icon alignment1"><i class="fas fa-briefcase"></i></div>
                        <h4 class="card__title alignment1" style="margin-top: 10px">Already have a business?</h4>
                        <p class="card__apply alignment1">
                            <a class="card__link" href="#">Click Card <i class="fas fa-arrow-right"></i></a>
                        </p>
                    </div>

                    <div class="card-back" style="background: radial-gradient(#007934, #00b34d);">
                        <div id="rapmob">
                            <h6 class="back-title"><b>Rapid Accelerator Programme</b></h6>
                            <p class="back-text">
                                The Propella Township Incubator Rapid Accelerator Programme (RAP) is designed to support
                                existing...
                                <a class="modal-trigger" style="color: navy" href="#modalrap"><b>Read More</b></a>
                            </p>
                            <button class="applybutton"><a href="/Application-Process/rap-application">APPLY</a></button>
                        </div>
                        <div id="industrialmob" style="display: none">
                            <h6 class="back-title"><b>Propella Industrial Programme</b></h6>
                            <p class="back-text">
                                This programme aids applicants that are seeking assistance in developing their
                                innovative products...
                                <a class="modal-trigger" style="color: navy" href="#modalindustrial"><b>Read More</b></a>
                            </p>
                            <button class="applybutton"><a href="/Application-Process/ind-application">APPLY</a></button>
                        </div>


                    </div>

                </div>
            </div>
            <!-- /flip-card-container -->

            <!-- flip-card-container -->
            <div class="flip-card-container" style="--hue: 170">
                <div class="flip-card">

                    <div class="card-front" style="background: radial-gradient(#002f5f, #0054a8);">
                        <div class="card__icon"><i class="fas fa-bolt"></i></div>
                        <h4 class="card__title">Have a prototype?</h4>
                        <p class="card__apply">
                            <a class="card__link" href="#">Click Card <i class="fas fa-arrow-right"></i></a>
                        </p>
                    </div>

                    <div class="card-back" style="background: radial-gradient(#002f5f, #0054a8);">
                        <br>
                        <h6 class="back-title"><b>Propella Industrial Programme</b></h6>
                        <p class="back-text">
                            This programme aids applicants that are seeking assistance in developing their innovative
                            products, processes and/or services within...
                            <a class="modal-trigger" style="color: red" href="#modalindustrial"><b>Read More</b></a>
                        </p>
                        <button class="applybutton"><a href="/Application-Process/ind-application">APPLY</a></button>
                    </div>

                </div>
            </div>
            <!-- /flip-card-container -->

            <!-- flip-card-container -->
            <div class="flip-card-container" style="--hue: 350">
                <div class="flip-card">

                    <div class="card-front" style="background: radial-gradient(#454242, #5e5a5a);">
                        <div class="card__icon alignment2"><i class="fas fa-lightbulb"></i></div>
                        <h4 class="card__title alignment2">Just an idea?</h4>
                        <p class="card__apply alignment2">
                            <a class="card__link" href="#">Click Card <i class="fas fa-arrow-right"></i></a>
                        </p>
                    </div>

                    <div class="card-back" style="background: radial-gradient(#454242, #5e5a5a);">
                        <div id="pbi">
                            <h6 class="back-title">Technology Innovation Programme</h6>
                            <p class="back-text">
                                <a class="modal-trigger" style="color: navy" href="#modalpbi"><b>Read More</b></a>
                            </p>
                            <button class="applybutton"><a href='/Application-Process/pbi-ict-application'>APPLY</a></button>
                            <h6 style="margin-top: 5vh" class="back-title">Propella Township Innovation</h6>
                            <p class="back-text">
                                <a class="modal-trigger" style="color: navy" href="#modalict"><b>Read More</b></a>
                            </p>
                            <button class="applybutton"><a href="/Application-Process/pti-ict-application/">APPLY</a></button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /flip-card-container -->
        </div>
        <br>
    </div>



    <script>
        $(document).ready(function () {
            $('.modal').modal();
        });

        function ShowHideDiv() {
            var option1 = document.getElementById("option-1");
            var rap = document.getElementById("rap");

            var option2 = document.getElementById("option-2");
            var industrial = document.getElementById("industrial");

            rap.style.display = option1.checked ? "block" : "none";
            industrial.style.display = option2.checked ? "block" : "none";
        }
        function ShowHideDiv2() {
            var option3 = document.getElementById("option-3");
            var pbi = document.getElementById("pbi");

            var option4 = document.getElementById("option-4");
            var pti = document.getElementById("pti");

            pbi.style.display = option3.checked ? "block" : "none";
            pti.style.display = option4.checked ? "block" : "none";
        }

        //----------------------------------------------

        //MOBILE

        //----------------------------------------------
        function ShowHideDivMobile() {
            var option01 = document.getElementById("option-01");
            var rapmob = document.getElementById("rapmob");

            var option02 = document.getElementById("option-02");
            var industrialmob = document.getElementById("industrialmob");

            rapmob.style.display = option01.checked ? "block" : "none";
            industrialmob.style.display = option02.checked ? "block" : "none";
        }
        function ShowHideDiv2Mobile() {
            var option03 = document.getElementById("option-03");
            var pbimob = document.getElementById("pbimob");

            var option04 = document.getElementById("option-04");
            var ptimob = document.getElementById("ptimob");

            pbimob.style.display = option03.checked ? "block" : "none";
            ptimob.style.display = option04.checked ? "block" : "none";
        }
    </script>

@endsection
