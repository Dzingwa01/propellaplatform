@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Questions-category/Questions-category.css"/>


    <div class="row center categoryDesktop" style="margin-right: 10em; margin-left: 10em; max-width: 100%">
        <?php $count = 1; ?>
        @foreach($questionCategorie as $category)
            @if($category->category_name != 'Panel Questions')
                <div class="col l4 m4 s12" style="width: 50%">
                    <div class="card" style="margin-top: 10vh; margin-right: 4em; margin-left: 4em" id="{{$category->id}}" onclick="selectCategory(this)">
                        <div class="card-content" style="padding: 0;">
                            <a href="#user">
                                <img class="fullscreen" style="height: 210px;width: 100%;"
                                     src="{{isset($category->category_image_url)?'/storage/'.$category->category_image_url:''}}">
                            </a>

                            <p>{{$category->category_description}}</p>
                        </div>
                        <div class="card-action">
                            <a id="{{$category->id}}" onclick="selectCategory(this)"
                               style="color: black;">{{$category->category_name}}</a>
                        </div>
                    </div>
                </div>
                <?php $count = $count+ 1; ?>
            @endif

        @endforeach
    </div>

    <div class="row center categoryMobile" style="margin-right: 4em; margin-left: 4em;">
        <?php $count = 1; ?>
        @foreach($questionCategorie as $category)
            @if($category->category_name != 'Panel Questions')
                <div class="col l4 m4 s12">
                    <div class="card" style="margin-top: 10vh;">
                        <div class="card-content" style="padding: 0;">
                            <a href="#user">
                                <img class="fullscreen" style="height: 150px;width: 100%;"
                                     src="{{isset($category->category_image_url)?'/storage/'.$category->category_image_url:''}}">
                            </a>

                            <p>{{$category->category_description}}</p>
                        </div>
                        <div class="card-action">
                            <a id="{{$category->id}}" onclick="selectCategory(this)" style="color: black;">{{$category->category_name}}</a>
                        </div>

                    </div>
                </div>
                <?php $count = $count+ 1; ?>
            @endif
        @endforeach
    </div>


    <script>
        $(document).ready(function () {
            $('.modal').modal();
        });


        function selectCategory(obj) {
            sessionStorage.setItem('category_id', obj.id);
            window.location = '/Application-Process/Basic-info';
        }
    </script>
@endsection
