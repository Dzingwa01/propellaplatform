@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/ThankYouPage/ThankYouPage.css"/>

    <br />
    <br />
    <div class="card "style="width:50%;margin-left: 400px;height:100%;" id="ThanksDesktop">
        <br />
        <br />
        <div class="icon-preview  col s6 m3 center-align">
            <i  class ="large  material-icons  pd48">done</i>
        </div>
        <h4 style="text-align: center">Thank you!</h4>
        <br />
        <h5 style="font-size: 1em;text-align: center;font-style: italic">Your application was successfully submitted.We'll contact you when the decision is made.</h5>
        <br />
        <br />
        <a class="waves-effect waves-light btn-small login-btn" style="margin-left: 600px"><i class="material-icons left">logout</i>Login</a>
        <br />
        <br />
    </div>

    <div class="card "style="width:90%;margin-left: 20px;height:100%;"id="ThanksMobile">
        <br />
        <br />
        <div class="icon-preview  col s6 m3 center-align">
            <i  class ="large  material-icons  pd48">done</i>
        </div>
        <h4 style="text-align: center">Thank you!</h4>
        <br />
        <h5 style="font-size: 1em;text-align: center;font-style: italic">Your application was successfully submitted.We'll contact you when the decision is made.</h5>
        <br />
        <br />
        <a class="waves-effect waves-light btn-small login-btn" style="margin-left: 600px" ><i class="material-icons left">login</i>Login</a>
        <br />
        <br />
    </div>


    <script>
        $(document).ready(function() {
           sessionStorage.clear();


           $('.login-btn').on('click', function(){

               console.log('test');

              location.href = "/login";
           });
        });
    </script>
@endsection
