@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Questions-category/Questions-category.css"/>


    <div class="row center categoryDesktop" style="margin-right: 10em; margin-left: 10em; max-width: 100%">
        <?php $count = 1; ?>
        @foreach($questionCategorie as $category)
            @if($category->category_name != 'Panel Questions')
                <div class="col l4 m4 s12" style="width: 50%">
                    <div class="card" style="margin-top: 10vh; margin-right: 4em; margin-left: 4em" id="{{$category->id}}" onclick="selectCategory(this)">
                        <div class="card-content" style="padding: 0;">
                            <a href="#user">
                                <img class="fullscreen" style="height: 210px;width: 100%;"
                                     src="{{isset($category->category_image_url)?'/storage/'.$category->category_image_url:''}}">
                            </a>

                            <p>{{$category->category_description}}</p>
                        </div>
                        <div class="card-action">
                            <a id="{{$category->id}}" onclick="selectCategory(this)"
                               style="color: black;">{{$category->category_name}}</a>
                        </div>
                    </div>
                    <div class="row center">
                        <p><b>Click here to view our</b></p>
                        <p><a class="modal-trigger red-text large" href="#modal4">{{$category->category_description}} Overview</a></p>
                    </div>
                </div>
                <?php $count = $count+ 1; ?>
            @endif

        @endforeach
    </div>

    <div class="row center categoryMobile" style="margin-right: 4em; margin-left: 4em;">
        <?php $count = 1; ?>
        @foreach($questionCategorie as $category)
            @if($category->category_name != 'Panel Questions')
                <div class="col l4 m4 s12">
                    <div class="card" style="margin-top: 10vh;">
                        <div class="card-content" style="padding: 0;">
                            <a href="#user">
                                <img class="fullscreen" style="height: 150px;width: 100%;"
                                     src="{{isset($category->category_image_url)?'/storage/'.$category->category_image_url:''}}">
                            </a>

                            <p>{{$category->category_description}}</p>
                        </div>
                        <div class="card-action">
                            <a id="{{$category->id}}" onclick="selectCategory(this)" style="color: black;">{{$category->category_name}}</a>
                        </div>
                    </div>
                    <div class="row center">
                        <p><b>Click here to view our</b></p>
                        <p><a class="modal-trigger red-text large" href="#modal4">{{$category->category_description}} Overview</a></p>
                    </div>
                </div>
                <?php $count = $count+ 1; ?>
            @endif
        @endforeach
    </div>


    <div id="modal4" class="modal industrialDesktop">
        <div class="modal-content">
            <p style="font-size: 1.4em"><b>Propella Future Makers Programme </b></p>
            <p> Future Makers, is an intensive programme "from concept to market" process run by Propella for IT
                innovators.
            </p>
            <p>• You need a great idea and some determination
            </p>
            <p>• An app, a service or an IT application that no-one else has thought of yet or requires help to
                commercialise and scale</p>
            <p> • OR, you need a product or service that will facilitate the commercialisation and growth of a tech
                business eg social media, bookkeeping, CAD CAM design, software development etc
            </p>
            <p> • Idea must fit into the operations of a Smart City or Community, such as Industry 4.0, IoT, etc.
            </p>
            <p> • Approximately 100 ventures are selected to attend a two day Stage 1 “Bootcamp”
            </p>
            <p> • No cost to participate
            </p>
            <p> • You don’t need to be trading yet
            </p>
            <p> • You need to be in start up phase and require incubation support
            </p>
            <p> • But there are also no free lunches – you will be expected to invest at least eight hours a week in
                Propella workshops, and even longer doing your research and groundwork.
            </p>
            <p> • Convince the panel, via a video pitch, that your idea has potential to turn into a business to become
                one of the 40 ventures who progress to Stage 2
            </p>
            <p>• Stage 2 is eight weeks on a business incubation journey to validate and prove that it’s a viable
                opportunity.
            </p>
            <p>• Crack the nod for Stage 3 for an additional three months of business acceleration to obtain your first
                revenue paying customer.
            </p>
            <p> This is the chance you've been looking for to make your big idea a reality.
            </p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.modal').modal();
        });


        function selectCategory(obj) {
            sessionStorage.setItem('category_id', obj.id);
            window.location = '/Application-Process/Basic-info';
        }
    </script>
@endsection
