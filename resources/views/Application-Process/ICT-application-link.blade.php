@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Questions-category/Questions-category.css"/>
    <div id="categoryDesktop">
        <div class="heading">
            <br>
            <br>
            <h4 class="heading__title"> The Propella ICT Programme</h4>
            <p class="heading__credits"><a class="heading__link" target="_blank" href="#">Apply Below</a></p>
        </div>
        <div class="main-container">

            <!-- flip-card-container -->
            <div class="flip-card-container" style="--hue: 350">
                <div class="flip-card">

                    <div class="card-front" style="background: radial-gradient(#454242, #5e5a5a);">
                        <div class="card__icon"><i class="fas fa-lightbulb"></i></div>
                        <h2 class="card__title">Just an idea?</h2>
                        <p class="card__apply">
                            <a class="card__link" href="#">Hover Card <i class="fas fa-arrow-right"></i></a>
                        </p>
                    </div>

                    <div class="card-back" style="background: radial-gradient(#454242, #5e5a5a);">
                        <div id="pbi">
                            <h5 class="back-title">Technology Innovation Programme</h5>
                            <p class="back-text">
                                The Propella ICT Programme, is an intensive "from concept to market" process run by Propella
                                Business Incubator for IT innovators.
                            </p>
                            <p class="back-text">• You need a great idea and some determination</p>
                            <p class="back-text">• An app, a service or an IT application that no-one else has thought of yet or requires help to
                                commercialise and scale
                                <a  class="modal-trigger" style="color: navy" href="#modalpbi"><b>Read More</b></a>
                            </p>
                            <br>
                            <button class="applybutton"><a href='/Application-Process/pbi-ict-application'>APPLY</a></button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /flip-card-container -->
        </div>
    </div>
    {{--        Modals       --}}

    <div id="modalpbi" class="modal">
        <div class="modal-content">
            <h4><b>Propella ICT Programme</b></h4>
            <br>
            <p>Propella ICT Programme, is an intensive programme "from concept to market" process run by Propella
                Business Incubator for IT innovators.</p>
            <br>
            <p>• You need a great idea and some determination</p>
            <br>
            <p>• An app, a service or an IT application that no-one else has thought of yet or requires help to
                commercialise and scale</p>
            <br>
            <p>• OR, you need a product or service that will facilitate the commercialisation and growth of a
                tech business eg social media, bookkeeping, CAD CAM design, software development etc</p>
            <br>
            <p>• Idea must fit into the operations of a Smart City or Community, such as Industry 4.0, IoT, etc.</p>
            <br>
            <p>• Approximately 100 ventures are selected to attend a two day Stage 1 “Bootcamp”</p>
            <br>
            <p>• No cost to participate</p>
            <br>
            <p>• You don’t need to be trading yet</p>
            <br>
            <p>• You need to be in start up phase and require incubation support</p>
            <br>
            <p>• But there are also no free lunches – you will be expected to invest at least eight hours a week
                in Propella workshops, and even longer doing your research and groundwork.</p>
            <br>
            <p>• Convince the panel, via a video pitch, that your idea has potential to turn into a business to become
                one of the 40 ventures who progress to Stage 2</p>
            <br>
            <p>• Stage 2 is eight weeks on a business incubation journey to validate and prove that it’s a
                viable opportunity.</p>
            <br>
            <p>• Crack the nod for Stage 3 for an additional three months of business acceleration to obtain your first
                revenue paying customer.</p>
            <br>
            <p>This is the chance you've been looking for to make your big idea a reality.</p>
        </div>
    </div>
    <br>
    <br>

    {{--       Mobile Side      --}}

    <div id="categoryMobile">
        <div class="heading">
            <br>
            <br>
            <h4 class="heading__title"> The Propella ICT Programme</h4>
            <p class="heading__credits"><a class="heading__link" target="_blank" href="#">Apply Below</a></p>
        </div>
        <div class="main-container">
            <!-- flip-card-container -->
            <div class="flip-card-container" style="--hue: 350">
                <div class="flip-card">

                    <div class="card-front" style="background: radial-gradient(#454242, #5e5a5a);">
                        <div class="card__icon alignment2"><i class="fas fa-lightbulb"></i></div>
                        <h4 class="card__title alignment2">Just an idea?</h4>
                        <p class="card__apply alignment2">
                            <a class="card__link" href="#">Click Card <i class="fas fa-arrow-right"></i></a>
                        </p>
                    </div>

                    <div class="card-back" style="background: radial-gradient(#454242, #5e5a5a);">
                        <div id="pbi">
                            <h5 class="back-title">Technology Innovation Programme</h5>
                            <p class="back-text">
                                The Propella ICT Programme, is an intensive "from concept to market" process run by Propella
                                ...
                                <a  class="modal-trigger" style="color: navy" href="#modalpbi"><b>Read More</b></a>
                            </p>
                            <button class="applybutton"><a href='/Application-Process/pbi-ict-application'>APPLY</a></button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /flip-card-container -->
        </div>
    </div>



    <script>
        $(document).ready(function () {
            $('.modal').modal();
        });

        function ShowHideDiv() {
            var option1 = document.getElementById("option-1");
            var rap = document.getElementById("rap");

            var option2 = document.getElementById("option-2");
            var industrial = document.getElementById("industrial");

            rap.style.display = option1.checked ? "block" : "none";
            industrial.style.display = option2.checked ? "block" : "none";
        }
        function ShowHideDiv2() {
            var option3 = document.getElementById("option-3");
            var pbi = document.getElementById("pbi");

            var option4 = document.getElementById("option-4");
            var pti = document.getElementById("pti");

            pbi.style.display = option3.checked ? "block" : "none";
            pti.style.display = option4.checked ? "block" : "none";
        }

        //----------------------------------------------

        //MOBILE

        //----------------------------------------------
        function ShowHideDivMobile() {
            var option01 = document.getElementById("option-01");
            var rapmob = document.getElementById("rapmob");

            var option02 = document.getElementById("option-02");
            var industrialmob = document.getElementById("industrialmob");

            rapmob.style.display = option01.checked ? "block" : "none";
            industrialmob.style.display = option02.checked ? "block" : "none";
        }
        function ShowHideDiv2Mobile() {
            var option03 = document.getElementById("option-03");
            var pbimob = document.getElementById("pbimob");

            var option04 = document.getElementById("option-04");
            var ptimob = document.getElementById("ptimob");

            pbimob.style.display = option03.checked ? "block" : "none";
            ptimob.style.display = option04.checked ? "block" : "none";
        }
    </script>

@endsection
