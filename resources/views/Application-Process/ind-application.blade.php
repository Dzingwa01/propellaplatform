@extends('layouts.app')
@section('content')
         <head>
            <meta name="csrf-token" content="{{ csrf_token() }}" />
         </head>
         <br />
         <br>
         <br>
         <div class="container-fluid">
             <form id="industrial-form" name="pleasevalidateme">
                 <div class="card" style="margin-left: 4em;margin-right: 4em">
                     <br />
                     <div class="row center">
                         <h4>Industrial Programme Application</h4>
                         <p>In order to create an account for you to start the application process you are required to complete all the questions below</p>
                         <p>Your information will not be shared with any third parties.</p>
                         <div class="col l12 m12 s12" >
                             <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                 <input id="role-desktop" value="{{$role->id}}" hidden>
                                 <div class="input-field col l6 m6 s12">
                                     <select id="title-desktop">
                                         <option value="" disabled selected>Choose Title</option>
                                         <option value="Mr">Mr</option>
                                         <option value="Mrs">Mrs</option>
                                         <option value="Ms">Ms</option>
                                         <option value="Dr">Dr</option>
                                     </select>
                                     <label>Title</label>
                                 </div>
                                 <div class="input-field col l6 m6 s12">
                                     <input id="initials-desktop" type="text" class="validate">
                                     <label for="initials-desktop">Initials</label>
                                 </div>
                             </div>
                             <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                 <div class="input-field col l6 m6 s12">
                                     <input id="first_name-desktop" type="text" class="validate" required>
                                     <label for="first_name-desktop">First Name</label>
                                 </div>
                                 <div class="input-field col l6 m6 s12">
                                     <input id="last_name-desktop" type="text" class="validate" required>
                                     <label for="last_name-desktop">Last Name</label>
                                 </div>
                             </div>
                             <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                 <div class="input-field col l6 m6 s12">
                                     <input id="age-desktop" min="16" type="number" class="validate" required>
                                     <label for="age-desktop">Age</label>
                                 </div>
                                 <div class="input-field col l6 m6 s12">
                                     <select id="gender-desktop" required>
                                         <option value="" disabled selected>Choose Gender</option>
                                         <option value="Male">Male</option>
                                         <option value="Female">Female</option>
                                         <option value="Other">Other</option>
                                     </select>
                                     <label for="gender-desktop">Gender</label>
                                 </div>
                             </div>
                             <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                 <div class="input-field col l6 m6 s12">
                                     <input id="venture_name-desktop" type="text" class="validate">
                                     <label for="venture_name-desktop">Venture Name</label>
                                 </div>
                                 <div class="input-field col l6 m6 s12">
                                     <input id="ID-desktop" type="text" class="validate" maxlength="13" minlength="13" required>
                                     <label for="ID-desktop">ID Number</label>
                                 </div>
                             </div>
                             <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                 <div class="input-field col l6 m6 s12">
                                     <input id="email-desktop" type="email" class="validate" required>
                                     <label for="email-desktop">Email address</label>
                                 </div>
                                 <div class="input-field col l6 m6 s12">
                                     <input id="cell_number-desktop" type="text" class="validate" maxlength="10" required>
                                     <label for="cell_number-desktop">Cell Number</label>
                                 </div>
                             </div>
                             <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                 <div class="input-field col l6 m6 s12">
                                     <input id="whatsapp_number-desktop" type="text" class="validate" maxlength="10" required>
                                     <label for="whatsapp_number-desktop">WhatsApp Number</label>
                                 </div>
                                 <div class="input-field col l6 m6 s12">
                                     <input id="address-desktop" type="text" class="validate">
                                     <label for="address-desktop">Physical Address 1</label>
                                 </div>
                             </div>
                             <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                 <div class="input-field col l6 m6 s12">
                                     <input id="address_two-desktop" type="text" class="validate">
                                     <label for="address_two-desktop">Physical Address 2</label>
                                 </div>
                                 <div class="input-field col l6 m6 s12">
                                     <input id="address_three-desktop" type="text" class="validate">
                                     <label for="address_three-desktop">Physical Address 3</label>
                                 </div>
                             </div>
                             <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                 <div class="input-field col l6 m6 s12">
                                     <input id="city-desktop" type="text" class="validate">
                                     <label for="city-desktop">City</label>
                                 </div>
                                 <div class="input-field col l6 m6 s12">
                                     <input id="code-desktop" type="text" class="validate">
                                     <label for="code-desktop">Postal Code</label>
                                 </div>
                             </div>
                             <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                 <div class="input-field col l6 m6 s12">
                                     <select id="race-desktop" required>
                                         <option value="" disabled selected>Choose Race</option>
                                         <option value="Black">African</option>
                                         <option value="White">White</option>
                                         <option value="Indian">Indian</option>
                                         <option value="Coloured">Coloured</option>
                                         <option value="Other">Other</option>
                                     </select>
                                     <label for="race-desktop">Race</label>
                                 </div>
                             </div>
                            <p>
                                 <label>
                                     <input id="popi_act_agreement-desktop" value="Yes" required name="popi_act_agreement-desktop"
                                            type="checkbox"/>
                                     <span id="popi_act_agreement-desktop">I agree to all <a class="modal-trigger" href="#modal1">terms</a></span>
                                 </label>
                             </p>
                             <p>
                                 <label>
                                     <input id="data_protected" value="Yes" required name="data_protected"
                                            type="checkbox"/>
                                     <span id="data_protected">I understand that my data is protected.</span>
                                 </label>
                             </p>
                             <!-- Modal Structure -->
                             <div id="modal1" class="modal">
                                 <div class="modal-content">
                                     <p><b>Website Privacy Notice</b></p>
                                     <p>The purpose of this information privacy strategy is to enhance the information privacy capabilities within the
                                         Propella application services, including (without limitation and to align with the requirements of the
                                         in country’s privacy legislation, namely the Protection of Personal Information Act (POPIA 2013) our website
                                         and other interactive properties through which the services are delivered (collectively, the “Service”) are
                                         owned, operated and distributed by Propella (referred to in this Privacy Notice as “Propella” or “we” and
                                         through similar words such as “us,” “our,” etc.).. This Privacy Notice outlines the personal information that
                                         Propella may collect, how Propella uses and safeguards that information, and with whom we
                                         may share it.
                                     </p>
                                     <p>Propella encourages our customers, visitors, business associates, and other interested parties to read
                                         this Privacy Notice, which applies to all users. By using our Service (our website and other interactive
                                         properties through which the services are delivered)  or submitting personal information to
                                         Propella by any other means, you acknowledge that you understand and agree to be bound by
                                         this Privacy Notice, and agree that Propella may collect, process, transfer, use,
                                         and disclose your personal information as described in this Notice. Further, by accessing any part of
                                         the Service, you are agreeing to .<span><b>THE TERMS AND CONDITIONS OF OUR TERMS OF SERVICE (the “Terms of Service”).
                                         IF YOU DO NOT AGREE WITH ANY PART OF THIS PRIVACY NOTICE OR OUR TERMS OF SERVICE, PLEASE DO NOT USE ANY OF
                                                 THE SERVICES.</b></span>
                                     </p>
                                     <p><b>What personal information do we collect about you?</b></p>
                                     <p>Personal information (also commonly known as personally identifiable information (PII) or personal data)
                                         is information that can be used to identify you, or any other individual to whom the information may relate.
                                     </p>
                                     <p>The personal information that we collect directly from those registering for the Service, includes the
                                         following categories:
                                     </p>
                                     <ul>
                                         Name and contact information (e.g. address; phone number; email, Proof of Address
                                         Billing Information (e.g. business bank account, billing contact information)
                                         Company/employer information
                                         Geographic or location information
                                         Information contained in posts you may on the public forums and interactive features of the Service.
                                         Other information that may be exchanged in the course of engaging with the Service.  You will be aware
                                         of any subsequently collected information because it will come directly from you.
                                     </ul>
                                     <p><b>Collection of User Generated Content</b></p>
                                     <p>We may invite you to post content on the Service, including your comments and any other information that you
                                         would like to be available on the Service, which may become public (“User Generated Content”).
                                         If you post User Generated Content, all of the information that you post will be available to
                                         authorized personnel of Propella. You expressly acknowledge and agree that we may access
                                         in real-time, record and store archives of any User Generated Content on our servers to make use of
                                         them in connection with the Service. If you submit a review, recommendation, endorsement,
                                         or other User Generated Content through the Service, or through other websites including Facebook,
                                         Instagram, Google, Yelp, and other similar channels, we may share that review, recommendation,
                                         endorsement or content publicly on the Service.
                                     </p>
                                     <p><b>What are the sources of personal information collected by Propella</b></p>
                                     <p>When providing personal information to Propella as described in this Notice, that personal information
                                         is collected directly from you, and you will know the precise personal information being collected by us.
                                         Propella does not collect personal information from any other sources, except where it may automatically
                                         be collected as described in the section titled “Cookies, Device Data, and How it is Used, if the information
                                         in that section is considered personal information.</p>
                                     <p><b>Why does Propella collect your personal information?</b></p>
                                     <p>Subject to the terms of this Privacy Notice, Propella uses the above described categories of personal
                                         information in several ways. Unless otherwise stated specifically, the above information may be used
                                         for any of the following purposes:
                                     </p>
                                     <ul>
                                         to administer the Service to you;
                                         to respond to your requests;
                                         to distribute communications relevant to your use of the Service, such as system updates or information about your use of the Service;
                                         as may be necessary to support the operation of the Service, such as for billing, account maintenance, and record-keeping purposes;
                                         to send to you Propella solicitations, product announcements, and the like that we feel may be of interest to  you. <span style="color: red">Please note that you may “opt out” of receiving these marketing materials</span>
                                         in other manners after subsequent notice is provided to you and/or your consent is obtained, if necessary.
                                     </ul>
                                     <p><b>Propella does not sell, re-sell, or distribute for re-sale your personal information.</b></p>
                                     <p><b>How do we share your Personal Information with third parties?</b></p>
                                     <p>If you agree to this in writing, we may provide any of the described categories of personal information
                                         to Propella employees, funders consultants, affiliates or other businesses or persons for the purpose
                                         of processing such information on our behalf in order to provide the Service to you. In such circumstances,
                                         we require that these parties agree to protect the confidentiality of such information consistent with the
                                         terms of this Privacy Notice.</p>
                                     <p>We will not share your personal information with other, third-party companies for their commercial or
                                         marketing use without your consent or except as part of a specific program or feature which you will
                                         specifically be able to <span style="color: red">opt-out of</span>.</p>
                                     <p>In addition, we may release personal information: (i) to the extent we have a good-faith belief that such
                                         action is necessary to comply with any applicable law; (ii) to enforce any provision of the Terms of Service ,
                                         protect ourselves against any liability, defend ourselves against any claims, protect the rights, property
                                         and personal safety of any user, or protect the public welfare; (iii) when disclosure is required to
                                         maintain the security and integrity of the Service, or to protect any user’s security or the security
                                         of other persons, consistent with applicable laws (iv) to respond to a court order, subpoena, search
                                         warrant, or other legal process, to the extent permitted and as restricted by law; or (v) in the event
                                         that we go through a business transition, such as a merger, divestiture, acquisition, liquidation or
                                         sale of all or a portion of our assets.
                                     </p>
                                     <p><b>Direct Marketing Communications</b></p>
                                     <p>We may communicate with you using email, SMS,whatsapp and other channels (sometimes through automated means)
                                         as part of our effort to market our products or services, administer or improve our products or services,
                                         or for other reasons stated in this Privacy Notice. You have an opportunity to withdraw consent to receive
                                         such direct marketing communications, as permitted by law.
                                     </p>
                                     <p>If you no longer wish to receive correspondence, emails, or other communications from us, you may opt-out by
                                         submitting a request through to the following email address reception@propellaincubator.co.za</p>
                                     <p>Please note that you may continue to receive non-marketing communications as may be required to maintain
                                         your relationship with Propella</p>
                                     <p>In addition to the communication described here, you may receive third-party marketing communications from
                                         providers we have engaged to market or promote our products and services. These third-party providers may
                                         be using communications lists they have acquired on their own, and you may have opted-in to those lists
                                         through other channels.  If you no longer wish to receive emails, SMSs, or other communications from
                                         such third parties, you may need to contact that third party directly.</p>
                                     <p><b>Retention of Data</b></p>
                                     <p>Propella will retain your personal information only for as long as is necessary for the purposes set
                                         out in this Notice. We will retain and use personal information to the extent necessary to comply
                                         with our legal obligations (for example, if we are required to retain your data to comply with applicable laws),
                                         resolve disputes and enforce our legal agreements and policies.
                                     </p>
                                     <p>Propella will also retain usage data for internal analysis purposes. Usage data is generally retained for
                                         a shorter period of time, except when this data is used to strengthen the security or to improve the
                                         functionality of our Sites and/or Portals, or we are legally obligated to retain this data for longer periods.
                                     </p>
                                     <p><b>Cookies, Device Data, and How it is Used</b></p>
                                     <p>When you use our Service, we may record unique identifiers associated with your device
                                         (such as the device ID and IP address), your activity within the Service, and your network location.
                                         Propella uses aggregated information (such as anonymous user usage information, cookies,
                                         IP addresses, browser type, clickstream information, etc.) to improve the quality and design
                                         of the Service and to create new features, promotions, functionality, and services by storing,
                                         tracking, and analyzing user preferences and trends. Specifically, we may automatically collect
                                         the following information about your use of Service through cookies, web beacons,
                                         and other technologies: </p>
                                     <ul>
                                         domain name
                                         browser type and operating system
                                         web pages you view
                                         links you click
                                         IP address
                                         the length of time you visit the Sites, Portals, and/or Services
                                         the referring URL or the webpage that led you to the Sites

                                     </ul>
                                     <p>We may also collect information regarding application-level events, such as crashes, and associate
                                         that temporarily with your account to provide customer service. In some circumstances,
                                         we may combine this information with personal information collected from you
                                         (and third-party service providers may do so on behalf of us).
                                     </p>
                                     <p>In addition, we may use "cookies," clear gifs, and log file information that help us determine the type of
                                         content and pages to which you link, the length of time you spend at any particular area of the Service,
                                         and the portion of the Service you choose to use. A cookie is a small text file that is sent by a website
                                         to your computer or mobile device where it is stored by your web browser. A cookie contains limited
                                         information, usually a unique identifier and the name of the site. Your browser has options to accept,
                                         reject or provide you with notice when a cookie is sent. Our cookies can only be read by Propella
                                         they do not execute any code or virus; and they do not contain any personal information.
                                         Cookies allow Propella to serve you better and more efficiently, and to personalize your experience
                                         with the Service. We may use cookies for many purposes, including (without limitation) to save
                                         your password so you don’t have to re-enter it each time you visit the Service, and to deliver content
                                         (which may include third party advertisements) specific to your interests.
                                     </p>
                                     <p>We may use third party service providers to help us analyze certain online activities. For example, these
                                         service providers may help us measure the performance of our online campaigns or analyze visitor activity
                                         on the Service. We may permit these service providers to use cookies and other technologies to perform these
                                         services for Propella. We do not share any personal information about our customers with these third-party service
                                         providers, and these service providers do not collect such personal information on our behalf. Our third-party
                                         service providers are required to comply fully with this Privacy Notice.</p>
                                     <p><b>South African Privacy Rights</b></p>
                                     <p>If you are a South African resident, South Africa law may provide you with certain rights with regard to your
                                         personal information under the Protection of Personal Information Act (“POPIA”) and Promotion of Access to
                                         Information Act (“PAIA”).as well the Consumer Protection Act Throughout this Privacy Notice you will find
                                         information required by POPIA regarding the categories of personal information collected from you; the
                                         purposes for which we use personal information, and the categories of third parties your data may be
                                         shared with.  This information is current as of the date of the Notice and is applicable in the 12 months
                                         preceding the effective date of the Notice.
                                     </p>
                                     <p>As a South African resident, the POPIA and PAIA provide you the ability to make inquiries regarding to your
                                         personal information. Specifically, the degree to which the information is not already provided in this
                                         Privacy Notice, you have the right to request disclosure or action your personal information, including:
                                     </p>
                                     <ul>
                                         If your personal information is collected by us.
                                         The specific pieces of personal information collected about you.
                                         The ability to correct or delete certain personal information collected about you.
                                         The ability to delete all the personal information collected about you, subject to certain exceptions.
                                         To opt-in or opt-out of direct marketing to you.
                                         To object to processing of your personal information, or
                                         Appeal any rejection of access to your personal information
                                     </ul>
                                     <p>You may submit a request regarding your rights under POPIA or PAIA by submitting a request through the
                                         following formreception@propellaincubator.co.za  or by contacting us at one of the following:
                                         +27 41 502 3700 .
                                     </p>
                                     <p>If we receive a POPIA request from you, we will first make a determination regarding the applicability of the law,
                                         and we will then take steps to verify your identity prior to responding. The steps to verify your identity may
                                         vary based on our relationship with you, but, at a minimum, it will take the form of confirming and matching
                                         the information submitted in the request with information already held by Propella and/or contacting you through
                                         previously used channels to confirm that you submitted the request (i.e. confirming identity through contact
                                         information that we have on file, and/or the contact information submitted to make the request).
                                     </p>
                                     <p>Propella does not knowingly collect or process the special personal information such as your religious or
                                         philosophical beliefs, race or ethnic origins, trade union memberships, political persuasion,
                                         health or sex life, or your criminal behaviour or biometric information.
                                     </p>
                                     <p>If you have a comment, question, or complaint about how we are processing your personal information,
                                         we hope that you contact us at reception@propellaincubator.co.za[INSERT FORM]  in order to allow us
                                         to resolve the matter. In addition, if you are located in the Republic of South Africa, you may submit
                                         a complaint regarding the processing of your personal information to the Information Regulator at the following link:
                                         <a href="https://www.justice.gov.za/inforeg/contact.html" target="_blank">  <span style="color: blue">https://www.justice.gov.za/inforeg/contact.html</span></a>
                                     </p>
                                     <p><b>Third Party Advertisers</b></p>
                                     <p>We may allow other companies, called third-party ad servers or ad networks, to serve advertisements within the
                                         Service. These third-party ad servers or ad networks use technology to send, directly to your device,
                                         the advertisements and links that appear on the Service. They automatically receive your device ID and
                                         IP address when this happens. They may also use other technologies (such as cookies, JavaScript, or Web Beacons)
                                         to measure the effectiveness of their advertisements and to personalize the advertising content you see.
                                         You should consult the respective privacy policies of these third-party ad servers or ad networks for
                                         more information on their practices and for instructions on how to opt-out of certain practices.
                                         This Privacy Notice does not apply to them, and we cannot control their activities.
                                     </p>
                                     <p><b>Information Storage and Security</b></p>
                                     <p>We employ industry-standard and/or generally accepted security measures designed to secure the integrity and
                                         confidentiality of all information submitted through the Service. However, the security of information transmitted
                                         through the internet or via a mobile device can never be guaranteed. We are not responsible for any interception
                                         or interruption of any communications through the internet or for changes to or losses of data.</p>
                                     <p>Users of the Service are responsible for maintaining the security of any password, user ID or other form of
                                         authentication involved in obtaining access to password protected or secure areas of the Service.
                                         In order to protect you and your information, we may suspend your use of any of the Service,
                                         without notice, pending an investigation, if any breach of security is suspected.</p>
                                     <p><b>External Links</b></p>
                                     <p>The Service may contain links to other websites maintained by third parties. Please be aware that we exercise
                                         no control over linked sites and Propella is not responsible for the privacy practices or the content
                                         of such sites. Each linked site maintains its own independent privacy and data collection policies and procedures,
                                         and you are encouraged to view the privacy policies of these other sites before providing any personal
                                         information.
                                     </p>
                                     <p>You hereby acknowledge and agree that Propella is not responsible for the privacy practices, data collection
                                         policies and procedures, or the content of such third-party sites, and you hereby release Propella
                                         from any and all claims arising out of or related to the privacy practices, data collection policies
                                         and procedures, and/or the content of such third-party sites.
                                     </p>
                                     <p><b>Changes to this Privacy Notice</b></p>
                                     <p>Proeplla reserves the right to modify this Privacy Notice from time to time in order that it accurately reflects
                                         the regulatory environment and our data collection principles. When material changes are made to this Privacy
                                         Notice, Propella will post the revised Notice on our website. This Privacy Notice was last modified as of 11 June 2021</p>
                                     <p><b>Contact Us</b></p>
                                     <p>If you have any questions or comments about this Privacy Notice or the Service provided by Propella,
                                         please contact us at: reception@propellaincubator.co.za</p>


                                 </div>
                                 <div class="modal-footer">
                                     <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                 </div>
                             </div>

                             <button class="btn waves-effect waves-light" id="industrial-form" name="action" style="margin-right: auto;">Submit
                                 <i class="material-icons right">send</i>
                             </button>
                             <p class="center-align"><b>Page 1 of 2</b></p>

                             <p class="center-align" style="color: red"><b>Please ensure you have completed all the required fields.</b></p>
                             <br />
                             <br />
                         </div>
                     </div>
                 </div>
             </form>
         </div>


    <script>
        let x = document.getElementById("popi_act_agreement-desktop").required;
        let y = document.getElementById("data_protected").required;
        $(document).ready(function () {
            $('.modal').modal();
            $('select').formSelect();
            $('#industrial-form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();
                let category_id = sessionStorage.getItem('category_id');

                formData.append('name', $('#first_name-desktop').val());
                formData.append('surname', $('#last_name-desktop').val());
                formData.append('id_number', $('#ID-desktop').val());
                formData.append('email', $('#email-desktop').val());
                formData.append('contact_number', $('#cell_number-desktop').val());
                formData.append('address_one', $('#address-desktop').val());
                formData.append('title', $('#title-desktop').val());
                formData.append('initials', $('#initials-desktop').val());
                formData.append('venture_name', $('#venture_name-desktop').val());
                formData.append('address_two', $('#address_two-desktop').val());
                formData.append('address_three', $('#address_three-desktop').val());
                formData.append('city', $('#city-desktop').val());
                formData.append('code', $('#code-desktop').val());
                formData.append('role_id', $('#role-desktop').val());
                formData.append('category_id', category_id);
                formData.append('age', $('#age-desktop').val());
                formData.append('gender', $('#gender-desktop').val());
                formData.append('race', $('#race-desktop').val());
                formData.append('whatsapp_number', $('#whatsapp_number-desktop').val());
                formData.append('popi_act_agreement', $('#popi_act_agreement-desktop').val());
                formData.append('data_protected', $('#data_protected').val());

                $.ajax({
                    url: "{{ route('Application-Process.storeIndApplication') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        if(response.message === "Email"){
                            $('#empty-div').notify('This email is already in use.', "error");
                        } else {
                            let user_id = response.user['id'];
                            window.location = "/Application-Process/questions/" + user_id;
                        }
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });

        function validateID() {

            var ex = /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/;

            var theIDnumber = document.forms["pleasevalidateme"]["ID-desktop"].value;
            if (ex.test(theIDnumber) == false) {
                // alert code goes here
                alert('Please supply a valid ID number');
                return false;
            }else {

                // alert(theIDnumber + ' a valid ID number');
                // here you would normally obviously
                return true;
                // but for the sake of this Codepen:
                //return false;
            }
        }

        function ValidateEmail()
        {
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var email = document.forms["pleasevalidateme"]["email-desktop"].value;
            if(email.match(mailformat))
            {
                return true;
            }
            else
            {
                alert("You have entered an invalid email address!");
                return false;
            }
        }
    </script>
@endsection
