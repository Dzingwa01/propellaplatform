@extends('layouts.app')

@section('content')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <br>
    {{--questionsDesktop--}}
    <div id="QuestionsDesktop">
    <div class="section" style="margin-top: 2em;">
        <input hidden disabled id="user-id-input" value="{{$user->id}}">
        <div class="card white"  style="margin-left: 4em;margin-right: 4em">
            <div class="row center">
                <br>
                <h4>{{$question_category->category_name}}</h4>
                <p style="color: #ff0000; !important;"><strong> PLEASE NOTE that you need to fill in all the questions in order to FINALIZE your application,
                        an incomplete application form may affect your acceptance on to the program.</strong>
            </div>
           {{-- comment--}}

            @if($question_category->category_name == 'AA Application form - Propella Township Incubator'
                or  $question_category->category_name == 'AA Application form - ICT Bootcamp'
                 or  $question_category->category_name == 'AA Application form - RAP Programme')
            <div class="row" style="margin-left: 4em; margin-right: 4em;">
                <ol>
                    @foreach ($questions as $question)
                        <li value="{{$question->question_number}}">  {{ $question->question_text}} </li>
                        <div class="input-field">
                            <input type="text" id="{{$question->id}}"  class="validate answer-text-desktop" required="" aria-required="true">
                            <span class="helper-text" data-error="Please answer all the questions"></span>
                        </div>
                    @endforeach
                </ol>
            </div>
            <div class="row center">
            <button class="btn waves-effect waves-light" id="upload-answers-form-desktop" name="action" style="margin-right: auto;">Finalize
                <i class="material-icons right">send</i>
            </button>
            </div>
            <p class="center-align"><b>Page 2 of 2</b></p>
            <p class="center-align" style="color: red"><b>Please ensure you have completed all the required questions.</b></p>
            <br>
                @else
                <div class="row" style="margin-left: 4em; margin-right: 4em;">
                    <ol>
                        @foreach ($questions as $question)
                            <li value="{{$question->question_number}}">  {{ $question->question_text}} </li>
                            <div class="input-field">
                                <input type="text" id="{{$question->id}}"  class="answer-text-desktop-industrial" >
                            </div>
                        @endforeach
                    </ol>
                </div>
                <div class="row center">
                    <button class="btn waves-effect waves-light" id="upload-answers-form-desktop-industrial" name="action" style="margin-right: auto;">Finalize
                        <i class="material-icons right">send</i>
                    </button>
                </div>
                <p class="center-align"><b>Page 2 of 2</b></p>
                <p class="center-align" style="color: red"><b>Please ensure you have completed all the required questions.</b></p>
                <br>
                @endif

        </div>
    </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large tooltipped waves-effect  btn modal-trigger red" data-tooltip="Save your progress" data-position="left" id="save-progress-button">Save your progress</a>
    </div>


    <script>

        $(document).ready(function () {
            $('.tooltipped').tooltip();

           /* autoSaveQuestions();*/
            // $('#save-progress-button-mobile').on('click', function(){
            //     //To store collection of all the answers
            //     let answersArray = [];

            $('#save-progress-button').on('click', function(){

                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#user-id-input').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        question_id: this.id,
                        user_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));

                //To push the answersArray as data to the controller and save all the answers to the db
                $.ajax({
                    url: "/Application-Process/questions",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        let user = response.user;
                        alert("Thank you for submitting your application.Please use the following credentials to view your dashboard: \nEmail: " + user.email + "\nPassword: " + "secret");
                        window.location.href = "/login";
                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
            $('#save-progress-button-mobile').on('click', function(){
                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#user-id-input').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        question_id: this.id,
                        user_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));

                //To push the answersArray as data to the controller and save all the answers to the db
                $.ajax({
                    url: "/Application-Process/questions",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        let user = response.user;
                        alert("Please use the following credentials to view your dashboard: \nEmail: " + user.email + "\nPassword: " + "secret");
                        window.location.href = "/login";
                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            //Desktop Form

            $('#upload-answers-form-desktop').on('click', function () {
                let approve = prompt('Did you answer all the questions ' + ' ? Yes/No');
                if (approve === 'Yes') {
                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#user-id-input').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        question_id: this.id,
                        user_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));

                //To push the answersArray as data to the controller and save all the answers to the db
                $.ajax({
                    url: "/Application-Process/questions",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        let user = response.user;
                        alert("Please use the following credentials to view your dashboard: \nEmail: " + user.email + "\nPassword: " + "Password_1234");
                        window.location.href = "/login";
                    },

                    error: function (response) {
                        console.log("error",response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
                } else {
                    alert(' Please NOTE that you need to fill in all the questions in order to FINALIZE your application....');
                }
            });

            $('#upload-answers-form-desktop-industrial').on('click', function () {

                    //To store collection of all the answers
                    let answersArray = [];

                    //To get current user
                    let data = $('#user-id-input').val();

                    //Loop through all inputs created
                    $('.answer-text-desktop-industrial').each(function () {
                        let model = {
                            question_id: this.id,
                            user_id: data,
                            answer_text: this.value,
                        };

                        //Add to list of answers
                        answersArray.push(model);
                    });

                    let formData = new FormData();
                    formData.append('questions_answers',JSON.stringify(answersArray));
                    formData.append('application_completed', 'true');

                    //To push the answersArray as data to the controller and save all the answers to the db
                    $.ajax({
                        url: "/Application-Process/questions",
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        data: formData,
                        type: 'post',
                        headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                        success: function (response, a, b) {
                            let user = response.user;
                            alert("Please use the following credentials to view your dashboard: \nEmail: " + user.email + "\nPassword: " + "Password_1234");
                            window.location.href = "/login";
                        },

                        error: function (response) {
                            console.log("error",response);
                            let message = response.responseJSON.message;
                            alert(message);
                        }
                    });
            });

        });

      /*  function autoSaveQuestions(){
            setTimeout(function(){
                //To store collection of all the answers
                let answersArray = [];

                //To get current user
                let data = $('#user-id-input').val();

                //Loop through all inputs created
                $('.answer-text-desktop').each(function () {
                    let model = {
                        question_id: this.id,
                        user_id: data,
                        answer_text: this.value,
                    };

                    //Add to list of answers
                    answersArray.push(model);
                });

                let formData = new FormData();
                formData.append('questions_answers',JSON.stringify(answersArray));

                //To push the answersArray as data to the controller and save all the answers to the db
                $.ajax({
                    url: "/Application-Process/questions",
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
                    success: function (response, a, b) {
                        autoSaveQuestions();
                    },
                    error: function (response) {
                        autoSaveQuestions();
                    }
                });
            }, 30000)
        }*/
    </script>

@endsection
