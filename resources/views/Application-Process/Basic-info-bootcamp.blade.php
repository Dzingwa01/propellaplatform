@extends('layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="/css/Basic-info/BasicInfoBootcamp.css"/>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <br />
    <br>
    <br>
    {{--Desktop Application--}}
    <div id="DesktopBasicInfoBootcamp">
        <div class="container-fluid">
            <form id="basic-info-form-bootcamp" name="pleasevalidateme">
                <div class="card" style="margin-left: 4em;margin-right: 4em">
                    <br />
                    <div class="row center">
                        <h4>RAP Application Form</h4>
                        <p>In order to complete your registration into the Rapid Acceleration Programme. You are required to enter your personal details below</p>
                        <p>Your personal information will not be shared with any third parties.</p>
                        <div class="col l12 m12 s12" >
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <input id="role-desktop" value="{{$role->id}}" hidden>
                                <div class="input-field col l6 m6 s12">
                                    <select id="title-desktop">
                                        <option value="" disabled selected>Choose Title</option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>

                                        <option value="Ms">Ms</option>
                                        <option value="Dr">Dr</option>
                                    </select>
                                    <label>Title</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="initials-desktop" type="text" class="validate">
                                    <label for="initials-desktop">Initials</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="first_name-desktop" type="text" class="validate" required>
                                    <label for="first_name-desktop">First Name</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="last_name-desktop" type="text" class="validate" required>
                                    <label for="last_name-desktop">Last Name</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="age-desktop" min="16" type="number" class="validate" required>
                                    <label for="age-desktop">Age</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <select id="gender-desktop" required>
                                        <option value="" disabled selected>Choose Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                    <label for="gender-desktop">Gender</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="venture_name-desktop" type="text" class="validate">
                                    <label for="venture_name-desktop">Venture Name</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="ID-desktop" type="text" class="validate" maxlength="13" minlength="13" required>
                                    <label for="ID-desktop">ID Number</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="email-desktop" type="email" class="validate" required>
                                    <label for="email-desktop">Email address</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="cell_number-desktop" type="text" class="validate" maxlength="10" required>
                                    <label for="cell_number-desktop">Cell Number</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="address-desktop" type="text" class="validate">
                                    <label for="address-desktop">Physical Address 1</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="address_two-desktop" type="text" class="validate">
                                    <label for="address_two-desktop">Physical Address 2</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="address_three-desktop" type="text" class="validate">
                                    <label for="address_three-desktop">Physical Address 3</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="city-desktop" type="text" class="validate">
                                    <label for="city-desktop">City</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="code-desktop" type="text" class="validate">
                                    <label for="code-desktop">Postal Code</label>
                                </div>

                                <div class="input-field col l6 m6 s12">
                                    <select id="race-desktop" required>
                                        <option value="" disabled selected>Choose Race</option>
                                        <option value="Black">African</option>
                                        <option value="White">White</option>
                                        <option value="Indian">Indian</option>
                                        <option value="Coloured">Coloured</option>
                                        <option value="Other">Other</option>
                                    </select>
                                    <label for="race-desktop">Race</label>
                                </div>

                            </div>
                            <button class="btn waves-effect waves-light" id="basic-info-form-bootcamp" name="action" style="margin-right: auto;">Submit
                                <i class="material-icons right">send</i>
                            </button>
                            <p class="center-align"><b>Page 1 of 2</b></p>
                            <br />
                            <p class="center-align" style="color: red"><b>Please ensure you have entered all the required fields.</b></p>
                            <br />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{--MobileApplication--}}
    <div id="MobileBasicInfoBootcamp">
        <div class="container-fluid">
            <form id="basic-info-form-bootcamp-mobile" name="pleasevalidateme">
                <div class="card" style="margin-left: 1.5em;margin-right: 1.5em;">
                    <br />
                    <div class="row center">
                        <h4>RAP Application Form</h4>
                        <p>In order to complete your registration into the Rapid Acceleration Programme. You are required to enter your personal details below</p>
                        <p>Your personal information will not be shared with any third parties.</p>
                        <div class="col l12 m12 s12" >
                            <div class="row" style="margin-left: 1em;margin-right: 1em;">
                                <input id="role-desktop" value="{{$role->id}}" hidden>
                                <div class="input-field col l6 m6 s12">
                                    <select id="title-mobile">
                                        <option value="" disabled selected>Choose Title</option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>

                                        <option value="Ms">Ms</option>
                                        <option value="Dr">Dr</option>
                                    </select>
                                    <label>Title</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="initials-mobile" type="text" class="validate">
                                    <label for="initials-mobile">Initials</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="first_name-mobile" type="text" class="validate" required>
                                    <label for="first_name-mobile">First Name</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="last_name-mobile" type="text" class="validate" required>
                                    <label for="last_name-mobile">Last Name</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="age-mobile" min="16" type="number" class="validate" required>
                                    <label for="age-mobile">Age</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <select id="gender-mobile" required>
                                        <option value="" disabled selected>Choose Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                    <label for="gender-mobile">Gender</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="venture_name-mobile" type="text" class="validate">
                                    <label for="venture_name-mobile">Venture Name</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="ID-mobile" type="text" class="validate" maxlength="13" minlength="13" required>
                                    <label for="ID-mobile">ID Number</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="email-mobile" type="email" class="validate" required>
                                    <label for="email-mobile">Email address</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="cell_number-mobile" type="text" class="validate" maxlength="10" required>
                                    <label for="cell_number-mobile">Cell Number</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="address-mobile" type="text" class="validate">
                                    <label for="address-mobile">Physical Address 1</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="address_two-mobile" type="text" class="validate">
                                    <label for="address_two-mobile">Physical Address 2</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="address_three-mobile" type="text" class="validate">
                                    <label for="address_three-mobile">Physical Address 3</label>
                                </div>
                                <div class="input-field col l6 m6 s12">
                                    <input id="city-mobile" type="text" class="validate">
                                    <label for="city-mobile">City</label>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2em;margin-right: 2em;">
                                <div class="input-field col l6 m6 s12">
                                    <input id="code-mobile" type="text" class="validate">
                                    <label for="code-mobile">Postal Code</label>
                                </div>

                                <div class="input-field col l6 m6 s12">
                                    <select id="race-mobile" required>
                                        <option value="" disabled selected>Choose Race</option>
                                        <option value="Black">African</option>
                                        <option value="White">White</option>
                                        <option value="Indian">Indian</option>
                                        <option value="Coloured">Coloured</option>
                                        <option value="Other">Other</option>
                                    </select>
                                    <label for="race-mobile">Race</label>
                                </div>

                            </div>
                            <button class="btn waves-effect waves-light" id="basic-info-form-bootcamp-mobile" name="action" style="margin-right: auto;">Submit
                                <i class="material-icons right">send</i>
                            </button>
                            <p class="center-align"><b>Page 1 of 2</b></p>
                            <p class="center-align" style="color: red"><b>Please ensure you have entered all the required fields.</b></p>
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            $('select').formSelect();
            $('#basic-info-form-bootcamp').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();

                let category_id = "c5673ef0-6aaf-11eb-99c2-bb714b6c5453";

                formData.append('name', $('#first_name-desktop').val());
                formData.append('surname', $('#last_name-desktop').val());
                formData.append('id_number', $('#ID-desktop').val());
                formData.append('email', $('#email-desktop').val());
                formData.append('contact_number', $('#cell_number-desktop').val());
                formData.append('address_one', $('#address-desktop').val());
                formData.append('title', $('#title-desktop').val());
                formData.append('initials', $('#initials-desktop').val());
                formData.append('venture_name', $('#venture_name-desktop').val());
                formData.append('address_two', $('#address_two-desktop').val());
                formData.append('address_three', $('#address_three-desktop').val());
                formData.append('city', $('#city-desktop').val());
                formData.append('code', $('#code-desktop').val());
                formData.append('role_id', $('#role-desktop').val());
                formData.append('category_id', category_id);
                formData.append('age', $('#age-desktop').val());
                formData.append('gender', $('#gender-desktop').val());
                formData.append('race', $('#race-desktop').val());

                $.ajax({
                    url: "{{ route('Application-Process.storeBasicInfoBootcamp') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        if(response.message === "Email"){
                            $('#empty-div').notify('This email is already in use.', "error");
                        } else {
                            let user_id = response.user['id'];
                            window.location = "/Application-Process/questions/" + user_id;
                        }
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
        $(document).ready(function () {
            // renders select input type
            $('select').formSelect();
            $('#basic-info-form-bootcamp-mobile').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();
                let category_id = "c5673ef0-6aaf-11eb-99c2-bb714b6c5453";

                formData.append('name', $('#first_name-mobile').val());
                formData.append('surname', $('#last_name-mobile').val());
                formData.append('id_number', $('#ID-mobile').val());
                formData.append('email', $('#email-mobile').val());
                formData.append('contact_number', $('#cell_number-mobile').val());
                formData.append('address_one', $('#address-mobile').val());
                formData.append('title', $('#title-mobile').val());
                formData.append('initials', $('#initials-mobile').val());
                formData.append('venture_name', $('#venture_name-mobile').val());
                formData.append('address_two', $('#address_two-mobile').val());
                formData.append('address_three', $('#address_three-mobile').val());
                formData.append('city', $('#city-mobile').val());
                formData.append('code', $('#code-mobile').val());
                formData.append('role_id', $('#role-mobile').val());
                formData.append('category_id', category_id);
                formData.append('age', $('#age-mobile').val());
                formData.append('gender', $('#gender-mobile').val());
                formData.append('race', $('#race-mobile').val());

                $.ajax({
                    url: "{{ route('Application-Process.storeBasicInfoBootcamp') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        if(response.message === "Email"){
                            $('#empty-div').notify('This email is already in use.', "error");
                        } else {
                            let user_id = response.user['id'];
                            window.location = "/Application-Process/questions/" + user_id;
                        }
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });

        function validateID() {

            var ex = /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/;

            var theIDnumber = document.forms["pleasevalidateme"]["ID-desktop"].value;
            if (ex.test(theIDnumber) == false) {
                // alert code goes here
                alert('Please supply a valid ID number');
                return false;
            }else {

                // alert(theIDnumber + ' a valid ID number');
                // here you would normally obviously
                return true;
                // but for the sake of this Codepen:
                //return false;
            }
        }

        function ValidateEmail()
        {
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var email = document.forms["pleasevalidateme"]["email-desktop"].value;
            if(email.match(mailformat))
            {
                return true;
            }
            else
            {
                alert("You have entered an invalid email address!");
                return false;
            }
        }
    </script>
@endsection

