@extends('layouts.app')
@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <br>
    <br>
    <link rel="stylesheet" type="text/css" href="/css/TiaApplication/TiaApplication.css"/>
    <div class="container-fluid" id="TiaDesktop">
        <form id="basic-info-form" name="pleasevalidateme">
            <div class="card" style="margin-left: 25em;margin-right: 25em">
                <br/>
                <div class="row center">
                    <h5><b>Are you an innovator in Industry 4.0</b></h5>
                    <p>Apply below</p>
                    <div class="col l12 m12 s12">
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="full_name" type="text" class="validate" required>
                                <label for="full_name">Full Name</label>
                            </div>
                        </div>

                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="contact_number" type="text" class="validate" required>
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="email" type="text" class="validate" required>
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <label for="innovation_description">Innovation Description (no more than 50 words)</label>
                            <textarea id="innovation_description" name="innovation_description" rows="4"
                                      cols="50"></textarea>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="proof_of_concept" type="text" class="validate" required>
                                <label for="proof_of_concept">Do you have a proof of concept?</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="competitors" type="text" class="validate" required>
                                <label for="competitors">Are there competitors and/or similar products already in the
                                    market?</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="if_yes" type="text" class="validate" required>
                                <label for="if_yes">If yes, what makes your product different?</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="market_size" type="text" class="validate" required>
                                <label for="market_size">What is the current market size?</label>
                            </div>
                        </div>

                        <button class="btn waves-effect waves-light" id="basic-info-form" name="action"
                                style="margin-right: auto;">Submit
                            <i class="material-icons right">send</i>
                        </button>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="container-fluid" id="TiaMobile">
        <form id="basic-info-form-mobile" name="pleasevalidateme">
            <div class="card">
                <br/>
                <div class="row center">
                    <h5><b>Are you an innovator in Industry 4.0</b></h5>
                    <p>Apply below</p>
                    <div class="col l12 m12 s12">
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="full_name-mobile" type="text" class="validate" required>
                                <label for="full_name-mobile">Full Name</label>
                            </div>
                        </div>

                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="contact_number-mobile" type="text" class="validate" required>
                                <label for="contact_number-mobile">Contact Number</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="email-mobile" type="text" class="validate" required>
                                <label for="email-mobile">Email</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <label for="innovation_description-mobile">Innovation Description (no more than 50 words)</label>
                            <textarea id="innovation_description-mobile" name="innovation_description" rows="4"
                                      cols="50"></textarea>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="proof_of_concept-mobile" type="text" class="validate" required>
                                <label for="proof_of_concept-mobile">Do you have a proof of concept?</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="competitors-mobile" type="text" class="validate" required>
                                <label for="competitors-mobile">Are there competitors and/or similar products already in the
                                    market?</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="if_yes-mobile" type="text" class="validate" required>
                                <label for="if_yes-mobile">If yes, what makes your product different?</label>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 2em;margin-right: 2em;">
                            <div class="input-field col l12 m12 s12">
                                <input id="market_size-mobile" type="text" class="validate" required>
                                <label for="market_size-mobile">What is the current market size?</label>
                            </div>
                        </div>

                        <button class="btn waves-effect waves-light" id="basic-info-form-mobile" name="action"
                                style="margin-right: auto;">Submit
                            <i class="material-icons right">send</i>
                        </button>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <br>
    <br>
    <br>


    <script>
        $(document).ready(function () {
            $('.modal').modal();
            $('select').formSelect();
            $('#basic-info-form').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();

                formData.append('full_name', $('#full_name').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('email', $('#email').val());
                formData.append('innovation_description', $('#innovation_description').val());
                formData.append('proof_of_concept', $('#proof_of_concept').val());
                formData.append('competitors', $('#competitors').val());
                formData.append('if_yes', $('#if_yes').val());
                formData.append('market_size', $('#market_size').val());

                $.ajax({
                    url: "{{ route('store-tia-application') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#market_size').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/';
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });

            });

            $('#basic-info-form-mobile').on('submit', function (e) {
                e.preventDefault();
                let formData = new FormData();

                formData.append('full_name', $('#full_name-mobile').val());
                formData.append('contact_number', $('#contact_number-mobile').val());
                formData.append('email', $('#email-mobile').val());
                formData.append('innovation_description', $('#innovation_description-mobile').val());
                formData.append('proof_of_concept', $('#proof_of_concept-mobile').val());
                formData.append('competitors', $('#competitors-mobile').val());
                formData.append('if_yes', $('#if_yes-mobile').val());
                formData.append('market_size', $('#market_size-mobile').val());

                $.ajax({
                    url: "{{ route('store-tia-application') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        $('#market_size-mobile').notify(response.message, "success");

                        setTimeout(function () {
                            window.location.href = '/';
                        }, 3000);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });

            });
        });


    </script>
@endsection
