<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends BaseModel
{
    protected $fillable = ['title','description'];

    public function careersSummernote(){
        return $this->hasOne(CareerSummernote::class,'career_id');
    }
}
