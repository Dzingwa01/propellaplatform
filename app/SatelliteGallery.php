<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SatelliteGallery extends BaseModel
{
    protected $fillable = ['image_url', 'propella_hub_id'];

    public function propellaHubGallery(){
        return $this->belongsTo(PropellaHubGallery::class, 'propella_hub_id');
    }
}
