<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Visitor extends BaseModel
{
    use Notifiable;

    protected $fillable = [
        'first_name','last_name', 'email','cell_number','company','date_in','time_in','title', 'dob', 'designation','is_vaccinated'
    ];

    public function visitor_backup()
    {
        return $this->hasMany(VisitorBackUp::class, 'visitor_id');
    }

    public function events(){
        return $this->hasMany(EventVisitor::class, 'visitor_id');
    }

    public function visitorDeregisters(){
        return $this->hasMany(VisitorDeregister::class, 'visitor_id');
    }
}
