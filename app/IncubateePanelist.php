<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateePanelist extends BaseModel
{
    protected $fillable = ['incubatee_id', 'panelist_id', 'panel_selection_date', 'panel_selection_time', 'question_category_id'];

    public function incubatee(){
        return $this->belongsTo(Incubatee::class, 'incubatee_id');
    }

    public function panelist(){
        return $this->belongsTo(Panelist::class, 'panelist_id');
    }

    public function panelistQuestionAnswers(){
        return $this->hasMany(IncPanelistQuestionAnswer::class, 'incubatee_panelist_id');
    }

    public function questionCategory(){
        return $this->hasOne(QuestionsCategory::class, 'question_category_id');
    }
}
