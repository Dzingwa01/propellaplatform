<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventsMedia extends BaseModel
{
    //

    protected $fillable = ['event_media_url', 'media_description', 'events_id'];

    public function Event()
    {
        return $this->belongsTo(Event::class, 'events_id');
    }

    public function eventsMediaImage(){
        return $this->hasMany(EventMediaImage::class, 'event_media_id');
    }
}
