<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionType extends BaseModel
{
    protected $fillable = ['datatype'];


    public function questions() {
        return $this ->hasMany(Question::class, 'question_type_id');
    }
}

