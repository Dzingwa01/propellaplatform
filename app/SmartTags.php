<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmartTags extends BaseModel
{
    protected $fillable = ['smart_tag_name'];
}
