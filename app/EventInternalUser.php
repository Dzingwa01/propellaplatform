<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class EventInternalUser extends BaseModel
{
    protected $fillable = ['event_id', 'user_id', 'attended', 'registered', 'de_registered', 'de_register_reason',
        'date_time_de_register', 'date_time_registered', 'found_out_via'];

    public function events(){
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function users(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
