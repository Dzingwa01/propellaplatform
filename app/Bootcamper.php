<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bootcamper extends BaseModel
{
    //
    protected $fillable = ['user_id', 'pitch_video_link', 'pitch_video_date_time',
        'venture_category_id','pitch_video_link_two','contacted_via','result_of_contact','is_incubatee',
        'total_panel_score', 'average_panel_score','status','cipc','proof_of_address','bootcamp_nda_agreement',
        'cipc_uploaded','proof_of_address_uploaded','id_copy_uploaded','evaluation_form_completed',
        'pitch_video_uploaded','bootcamper_contract_upload','contract_uploaded','beginning_evaluation_complete',
        'post_evaluation_complete'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function ventureCategory(){
        return $this->belongsTo(VentureCategory::class, 'venture_category_id');
    }

    public function events(){
        return $this->hasMany(EventBootcamper::class, 'bootcamper_id');
    }

    public function panelists(){
        return $this->hasMany(BootcamperPanelist::class, 'bootcamper_id');
    }

    //Contact Log
    public function bootcampercontactLog(){
        return $this->hasMany(BootcamperContactLog::class, 'bootcamper_id');
    }

}
