<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venture extends BaseModel
{
   protected $table = 'venture';

    protected $fillable =['user_id',
        'elevator_pitch',
        'hub',
        'smart_city_tags',
        'business_type',
        'company_name',
        'slug',
        'stage',
        'cohort',
        'venture_profile_information',
        'business_linkedin_url',
        'business_facebook_url',
        'business_twitter_url',
        'business_instagram_url',
        'venture_email',
        'company_website',
        'contact_number',
        'physical_address_one',
        'physical_address_two',
        'physical_address_three',
        'physical_city',
        'physical_postal_code',
        'postal_address_one',
        'postal_address_two',
        'postal_address_three',
        'postal_city',
        'postal_code',
        'virtual_or_physical',
        'pricinct_telephone_code',
        'pricinct_keys',
        'pricinct_office_position',
        'pricinct_printing_code',
        'pricinct_warehouse_position',
        'rent_turnover',
        'rent_date',
        'rent_amount',
        'status',
        'alumni_date',
        'exit_date',
        'resigned_date',
        'mentor',
        'advisor',
        'venture_funding',
        'date_awarded',
        'fund_value',
        'date_closedout',
        'venture_type',
        'reason_for_leaving',
        'incubatee_id',
        'mentor_id',
        'mentor_venture_id',
        'sustainable_development_goals','oct_spreadsheet_uploaded',
        'nov_spreadsheet_uploaded',
        'dec_spreadsheet_uploaded',
        'jan_spreadsheet_uploaded',
        'oct_date_submitted',
        'nov_date_submitted',
        'dec_date_submitted',
        'jan_date_submitted','feb2022_spreadsheet_uploaded','feb2022_date_submitted','march2022_spreadsheet_uploaded','march2022_date_submitted','april2022_spreadsheet_uploaded',
        'april2022_date_submitted','may2022_spreadsheet_uploaded','may2022_date_submitted','june2022_spreadsheet_uploaded','june2022_date_submitted','july2022_spreadsheet_uploaded',
        'july2022_date_submitted','august2022_spreadsheet_uploaded','august2022_date_submitted','sept2022_spreadsheet_uploaded','sept2022_date_submitted','oct2022_spreadsheet_uploaded',
        'oct2022_date_submitted','nov2022_spreadsheet_uploaded','nov2022_date_submitted','dec2022_spreadsheet_uploaded','dec2022_date_submitted','gap_analysis'];

    public function ventureOwnership(){
        return $this->hasOne(VentureOwnership::class,'venture_id');
    }

    public function ventureUploads(){
        return $this->hasOne(VentureUpload::class, 'venture_id');
    }

    public function ventureTestimonials(){
        return $this->hasMany(VentureTestimonial::class, 'venture_id');
    }

    public function ventureGallery(){
        return $this->hasMany(VentureGallery::class, 'venture_id');
    }

    public function ventureEmployee(){
        return $this->hasMany(VentureEmployee::class, 'venture_id');
    }

    public function incubatee(){
        return $this->hasMany(Incubatee::class,'venture_id');
    }

    public function mentor(){
        return $this->belongsTo(Mentor::class,'mentor_id');
    }

    public function questionAnswers(){
        return $this->hasMany(VentureQuestionAnswer::class, 'venture_id');
    }

    public function ventureShadow()
    {
        return $this->hasMany(MentorVentureShadowboard::class, 'venture_id');
    }


    public function venturePanelInterviews(){
        return $this->hasMany(VenturePanelInterview::class, 'venture_id');
    }
    public function incubatee_venture_spreadsheet(){
        return $this->hasMany(IncubateeVentureSpreadsheetUpload::class, 'venture_id');
    }

    public function contactLog(){
        return $this->hasMany(VentureContactLog::class, 'venture_id');
    }
}
