<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropellaLearningCurveSummernote extends BaseModel
{
    //
    public $fillable = ['content','learning_curve_id'];

    public function learningCurve(){
        return $this->belongsTo(PropellaLearningCurve::class,'learning_curve_id');
    }
}
