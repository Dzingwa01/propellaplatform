<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentation extends BaseModel
{
    protected  $fillable = ['title','handout','presentation','audio','status','video'];

    public function presentation(){
        return $this->belongsTo(IncubateePresentation::class, 'presentation_id');
    }

    public function handouts_uploads(){
        return $this->hasMany(HandoutUpload::class, 'presentation_id');
    }

    public function presentation_uploads(){
        return $this->hasMany(PresentationUpload::class, 'presentation_id');
    }

}
