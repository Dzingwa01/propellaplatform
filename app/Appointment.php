<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends BaseModel
{
    protected $fillable = ['user_id','title','mentor_name','date','time','message'];
}
