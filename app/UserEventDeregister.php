<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEventDeregister extends BaseModel
{
    protected $fillable = ['event_id', 'user_id', 'de_registered', 'de_register_reason', 'date_time_de_register', 'event_name'];

    public function event(){
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
