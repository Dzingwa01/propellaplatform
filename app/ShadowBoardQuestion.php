<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShadowBoardQuestion extends BaseModel
{
    protected $fillable = ['question_number','question_text','question_type','question_category_id','shadow_question_answer_id'];

    public function shadowBoardQuestionCategory(){
        return $this->belongsTo(ShadowBoardQuestionCategory::class,'question_category_id');
    }

    public function shadowBoardQuestionSubQuestionText(){
        return $this->hasMany(ShadowBoardSubQuestionText::class,'shadow_board_question_id');
    }


}
