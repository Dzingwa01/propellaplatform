<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HandoutUpload extends BaseModel
{
    protected $fillable = ['presentation_id','handout_url'];
}
