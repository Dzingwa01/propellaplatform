<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LatestInfoSummernote extends BaseModel
{
    public $fillable = ['content','latest_info_id'];

    public function latestInfp(){
        return $this->belongsTo(LatestInfo::class,'latest_info_id');
    }
}
