<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LatestInfo extends BaseModel
{
    protected $fillable = ['title','description','date','word_doc_url','info_image_url','pdf_url'];

    public function latestInfoSummernote(){
        return $this->hasOne(LatestInfoSummernote::class,'latest_info_id');
    }
}
