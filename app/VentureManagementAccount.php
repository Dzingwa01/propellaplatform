<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class VentureManagementAccount extends BaseModel
{
    protected $fillable = ['management_account_url', 'venture_upload_id','management_account_expiry_date'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
