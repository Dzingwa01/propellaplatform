<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RinpApplicantQuestionAnswer extends BaseModel
{
    protected $fillable = ['question_text', 'answer_text', 'rinp_applicant_id'];

    public function rinpApplicant(){
        return $this->belongsTo(RinpApplicant::class, 'rinp_applicant_id');
    }
}
