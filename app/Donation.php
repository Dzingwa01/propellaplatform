<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends BaseModel
{
    protected $fillable = ['name','contact_number','message','email'];
}
