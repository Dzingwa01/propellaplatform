<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RinpApplicantPanelist extends BaseModel
{
    protected $fillable = ['rinp_applicant_id', 'panelist_id', 'panel_selection_date', 'panel_selection_time',
        'question_category_id'];

    public function rinpApplicant(){
        return $this->belongsTo(RinpApplicant::class, 'rinp_applicant_id');
    }

    public function panelist(){
        return $this->belongsTo(Panelist::class, 'panelist_id');
    }

    public function rinpPanelistQuestionAnswers(){
        return $this->hasMany(RinpPanelistQuestionAnswer::class, 'rinp_applicant_panelist_id');
    }

    public function questionCategory(){
        return $this->hasOne(QuestionsCategory::class, 'question_category_id');
    }
}
