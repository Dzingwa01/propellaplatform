<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateeContactLog extends BaseModel
{
    protected $fillable = ['comment', 'date','incubatee_id','incubatee_contact_results'];
}
