<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferredApplicantQuestionAnswer extends BaseModel
{
    //
    protected $fillable = ['question_id','answer_text','p_r_a_id'];

    public function referred_applicant(){
        return $this->belongsTo(PropellaReferredApplicant::class, 'p_r_a_id');
    }

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }
}
