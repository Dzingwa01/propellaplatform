<?php

namespace App\Console;

use App\ApplicantPanelist;
use App\Panelist;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $today = Carbon::now()->toDateString();
            $panelists = Panelist::all();
            $detach_panelists = [];

            foreach ($panelists as $panelist){
                $panelist->load('applicants');
                $latest_date = "";
                $panelist_id = $panelist->id;

                foreach ($panelist->applicants as $panelist_applicant){
                    if($panelist_applicant->panel_selection_date > $latest_date)
                        $latest_date = $panelist_applicant->panel_selection_date;
                }

                $detach_panelist = DB::table('applicant_panelists')
                    ->where('panel_selection_date', $latest_date)
                    ->where('panelist_id', $panelist_id)->first();

                array_push($detach_panelists, $detach_panelist);
            }

            foreach($detach_panelists as $detach_panelist){
                $detach_panelist->load('panelist');
                $panelist = $detach_panelist->panelist;
                $panelist->load('user');
                $user = $panelist->user;
                $panelist_role = DB::table('roles')
                    ->where('name', 'panelist')->first();

                if($today == $detach_panelist->panel_selection_date){
                    $user->roles()->detach($panelist_role->id);
                }
            }
        })->daily();
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
