<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panelist extends BaseModel
{
    //
    protected $fillable = ['user_id'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function applicants(){
        return $this->hasMany(ApplicantPanelist::class, 'panelist_id');
    }

    public function bootcamper(){
        return $this->hasMany(BootcamperPanelist::class, 'panelist_id');
    }

    public function declined_bootcamper(){
        return $this->hasMany(DeclinedBootcamperPanelist::class, 'panelist_id');
    }

    public function venturePanelInterviews(){
        return $this->hasMany(VenturePanelInterview::class, 'panelist_id');
    }

    public function rinpApplicants(){
        return $this->hasMany(RinpApplicantPanelist::class, 'panelist_id');
    }

    public function incubatee(){
        return $this->hasMany(IncubateePanelist::class, 'panelist_id');
    }
}
