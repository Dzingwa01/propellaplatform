<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyEmployee extends BaseModel
{
    //
    protected $fillable = ['name','surname','email','contact_number_two','contact_number','contact_number_two','address','company_id','address_two','address_three','is_vaccinated'];

    public function categories(){
        return $this->belongsToMany(Category::class,'category_company_employee','company_employee_id');
    }

    public  function company(){
        return $this->belongsTo(Company::class,'company_id','id');
    }

    public function recieved_logs(){
        return $this->hasMany(ContactLog::class,'receiver_id');
    }
}
