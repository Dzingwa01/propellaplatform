<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceSummernote extends BaseModel
{
    public $fillable = ['content','fundingResource_id'];

    public function fundingResource(){
        return $this->belongsTo(FundingResource::class,'fundingResource_id');
    }
}
