<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateeUpload extends BaseModel
{
    //
    protected $fillable = ['company_logo_url',
        'profile_picture_url',
        'video_shot_url',
        'product_shot_url',
        'infographic_url',
        'crowdfunding_url',
        'video_links_id',
        'testimonials_id',
        'gallery_id',
        'incubatee_id',
        'id_document',
        'file_description',
        'stage_2_pitch_video',
        'stage_2_pitch_video_title',
        'proof_of_address',
        'disc_assessment_report',
        'strength_upload',
        'talent_dynamics_upload',
        'disk_results',
        'talent_results',
        'strength_results',
        'founder_video_shot',
        'founder_video_title'
        ];



    public function incubatee(){
        return $this->belongsTo(Incubatee::class,'incubatee_id');
    }
}
