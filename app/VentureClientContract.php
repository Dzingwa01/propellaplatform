<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureClientContract extends BaseModel
{
    protected $fillable = ['client_contract_url', 'date_of_signature', 'venture_upload_id','contract_stage',
        'contract_expiry_date'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
