<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureSpreadsheetUpload extends BaseModel
{
    protected $fillable = ['venture_upload_id','spreadsheet_upload','spreadsheet_uploaded','month','year','date_submitted'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
