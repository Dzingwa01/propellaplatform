<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactType extends BaseModel
{
    //
    protected $fillable = ['type_name'];

    public function contact_logs(){
        return $this->hasMany(ContactLog::class,'contact_type_id');
    }
}
