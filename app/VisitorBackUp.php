<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorBackUp extends BaseModel
{
    public $table = "visitor_back_up";
    protected $fillable = [
        'date_in','visit_reason', 'visitor_id', 'visit_category', 'found_us_via'
    ];

    public function visitor()
    {
        return $this->belongsTo(Visitor::class, 'visitor_id');
    }


}
