<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenturePanelistQuestionAnswer extends BaseModel
{
    protected $fillable = ['question_id', 'score', 'comment', 'venture_panel_interview_id'];

    public function venturePanelInterview(){
        return $this->belongsTo(VenturePanelInterview::class, 'venture_panel_interview_id');
    }

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }
}
