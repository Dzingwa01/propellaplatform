<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventMediaImage extends BaseModel
{
    protected $fillable = ['event_media_url', 'event_media_id'];

    public function EventsMedia(){
        return $this->belongsTo(EventsMedia::class, 'event_media_id');
    }
}
