<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferredApplicantCategory extends BaseModel
{
    //
    protected $fillable = ['category_name'];

    public function referred_applicants(){
        return $this->hasMany(PropellaReferredApplicant::class, 'referred_applicant_category_id');
    }
}
