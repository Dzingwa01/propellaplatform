<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessCategory extends BaseModel
{
    protected $fillable = ['business_category_name'];
}
