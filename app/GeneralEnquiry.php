<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralEnquiry extends BaseModel
{
    protected $fillable = ['general_enquiry_category_id', 'title', 'name', 'surname', 'company', 'email',
        'contact_number', 'city', 'heard_about_us', 'enquiry_message', 'date_enquired', 'initial_contact_person', 'initial_contact_method', 'initial_report',
        'enquiry_next_step', 'enquiry_assigned_person', 'date_assigned', 'is_assigned'];

    public function enquiry_category(){
        return $this->belongsTo(GeneralEnquiryCategory::class, 'general_enquiry_category_id');
    }

    public function assigned_enquiry(){
        return $this->hasOne(GeneralAssignedEnquiry::class, 'general_enquiry_id');
    }

    public function pre_assigned_enquiries(){
        return $this->hasMany(GeneralPreAssignedEnquiry::class, 'general_enquiry_id');
    }
}
