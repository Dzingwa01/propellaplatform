<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclinedApplicantQuestionAnswer extends BaseModel
{
    //
    protected $fillable = ['question_id','d_a_id','answer_text'];

    public function declined_applicant(){
        return $this->belongsTo(DeclinedApplicant::class, 'd_a_id');
    }

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }
}
