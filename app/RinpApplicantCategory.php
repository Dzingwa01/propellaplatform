<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RinpApplicantCategory extends BaseModel
{
    protected $fillable = ['category_name'];

    public function rinpApplicants(){
        return $this->hasMany(RinpApplicant::class, 'rinp_applicant_category_id');
    }
}
