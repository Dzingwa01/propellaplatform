<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionSubText extends BaseModel
{
    protected $fillable = ['question_sub_text','question_id'];

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

}
