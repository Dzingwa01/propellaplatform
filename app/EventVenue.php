<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventVenue extends BaseModel
{
     protected $fillable = ['venue_name'];

    public function Event() {
        return $this->hasMany(Event::class, 'event_venue_id');
    }
}
