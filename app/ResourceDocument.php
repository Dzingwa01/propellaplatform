<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceDocument extends BaseModel
{
    protected $fillable = ['documents','fundingResource_id'];

    public function fundingResource(){
        return $this->belongsTo(FundingResource::class,'fundingResource_id');
    }
}
