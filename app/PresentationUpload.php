<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresentationUpload extends BaseModel
{
    protected $fillable = ['presentation_id','presentation_url'];
}
