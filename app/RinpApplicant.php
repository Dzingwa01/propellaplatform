<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RinpApplicant extends BaseModel
{
    protected $fillable = ['name', 'surname', 'email', 'contact_number', 'id_number', 'address_one',
        'address_two', 'address_three', 'title', 'initials', 'date_of_birth', 'age', 'gender', 'city', 'postal_code', 'race', 'venture_name',
        'rinp_applicant_category_id'];

    public function applicantQuestionAnswers(){
        return $this->hasMany(RinpApplicantQuestionAnswer::class, 'rinp_applicant_id');
    }

    public function panelists(){
        return $this->hasMany(RinpApplicantPanelist::class, 'rinp_applicant_id');
    }

    public function rinpApplicantCategory(){
        return $this->belongsTo(RinpApplicantCategory::class, 'rinp_applicant_category_id');
    }
}
