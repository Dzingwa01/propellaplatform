<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureBbbeeCertificate extends BaseModel
{
    protected $fillable = ['bbbee_certificate_url', 'date_of_bbbee_certificate', 'venture_upload_id',
        'bbbee_expiry_date'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
