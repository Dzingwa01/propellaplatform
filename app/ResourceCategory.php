<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceCategory extends BaseModel
{
    protected $fillable =['category_name'];

    public function fundingResource(){
        return $this->hasMany(FundingResource::class, 'resource_category_id');
    }
}
