<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureCategory extends BaseModel
{
    //
    protected $fillable = ['category_name'];

    public function incubatees(){
        return $this->hasMany(Incubatee::class, 'venture_category_id');
    }

    public function bootcampers(){
        return $this->hasMany(Bootcamper::class, 'venture_category_id');
    }

    public function ventures(){
        return $this->hasMany(Venture::class, 'venture_category_id');
    }

    public function declined_bootcampers(){
        return $this->hasMany(DeclinedBootcamper::class, 'venture_category_id');
    }
}
