<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RinpPanelistQuestionAnswer extends BaseModel
{
    protected $fillable = ['question_id', 'score', 'comment', 'rinp_applicant_panelist_id'];

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function applicantPanelists(){
        return $this->belongsTo(RinpApplicantPanelist::class, 'rinp_applicant_panelist_id');
    }
}
