<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffCovidSymptom extends BaseModel
{
    protected $fillable = [
        'user_id','staff_covid_symptoms_one', 'staff_covid_symptoms_two','staff_declaration','staff_digitally_signed_declaration',
        'date','staff_health_declaration','staff_temperature','is_vaccinated'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
