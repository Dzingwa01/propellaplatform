<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateeVentureSpreadsheetUpload extends BaseModel
{
    protected $fillable = ['venture_upload_id','venture_id','spreadsheet_upload','spreadsheet_uploaded','month','year','date_submitted'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
    public function venture(){
        return $this->belongsTo(Venture::class, 'venture_id');
    }
}
