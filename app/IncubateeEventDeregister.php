<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateeEventDeregister extends BaseModel
{
    protected $fillable = ['event_id', 'incubatee_id' ,'de_registered', 'de_register_reason', 'date_time_de_register', 'event_name'];

    public function event(){
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function incubatee(){
        return $this->belongsTo(Incubatee::class, 'incubatee_id');
    }
}
