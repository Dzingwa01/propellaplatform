<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class DeclinedBootcamperEvent extends BaseModel
{
    protected $fillable = ['event_id', 'declined_bootcamper_id', 'accepted', 'date_registered', 'attended', 'declined'];

    public function event(){
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function declined_bootcamper(){
        return $this->belongsTo(DeclinedBootcamper::class, 'declined_bootcamper_id');
    }
}
