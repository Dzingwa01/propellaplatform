<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureEmployee extends BaseModel
{
    //

    protected $fillable = ['employee_name','position','start_date','end_date','contract_url','employee_surname',
        'title','initials','id_number','gender','contact_number','email', 'venture_id', 'status', 'resigned_date'];

    public function Venture(){
        return $this->belongsTo(Venture::class,'venture_id');
    }
}
