<?php

namespace App;

use App\Http\Controllers\QuestionController;
use Illuminate\Database\Eloquent\Model;

class VenturePanelInterview extends BaseModel
{
    protected $fillable = ['venture_id', 'venture_panel_interview_category_id', 'panelist_id', 'question_category_id',
        'total_panel_score', 'average_panel_score', 'panel_selection_date', 'panel_selection_time'];

    public function venturePanelInterviewCategory(){
        return $this->belongsTo(VenturePanelInterviewCategory::class, 'venture_panel_interview_category_id');
    }

    public function venture(){
        return $this->belongsTo(Venture::class, 'venture_id');
    }

    public function panelist(){
        return $this->belongsTo(Panelist::class, 'panelist_id');
    }

    public function venturePanelInterviewQuestionAnswers(){
        return $this->hasMany(VenturePanelistQuestionAnswer::class, 'venture_panel_interview_id');
    }

    public function questionCategory(){
        return $this->belongsTo(QuestionsCategory::class, 'question_category_id');
    }
}
