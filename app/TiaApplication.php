<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiaApplication extends BaseModel
{
    protected $fillable = ['full_name','contact_number','email','innovation_description',
        'proof_of_concept','if_yes','competitors','market_size'];

}
