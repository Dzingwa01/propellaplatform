<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclinedBootcamper extends BaseModel
{
    protected $fillable = ['name', 'surname', 'email', 'contact_number', 'contact_number_two', 'id_number',
        'address_one', 'address_two', 'address_three', 'title', 'initials', 'date_of_birth', 'age', 'gender', 'city', 'code',
        'company_name', 'position', 'race', 'referred', 'declined', 'venture_name', 'chosen_category', 'contacted_via', 'result_of_contact',
        'declined_reason', 'referred_company', 'pitch_video_link', 'pitch_video_date_time', 'pitch_video_link_two', 'total_panel_score', 'average_panel_score',
        'venture_category_id','id_document_url','proof_of_address','cipc','bootcamper_contract_upload'];

    public function declined_bootcamper_events(){
        return $this->hasMany(DeclinedBootcamperEvent::class, 'declined_bootcamper_id');
    }

    public function declined_bootcamper_application_question_answers(){
        return $this->hasMany(DeclinedBootcamperQuestionAnswer::class, 'd_b_id');
    }

    public function declined_bootcamper_panelists(){
        return $this->hasMany(DeclinedBootcamperPanelist::class, 'declined_bootcamper_id');
    }

    public function declined_bootcamper_venture_category(){
        return $this->belongsTo(VentureCategory::class, 'venture_category_id');
    }

    //Contact Log
    public function contactLog(){
        return $this->hasMany(DeclinedBootcamperContactLog::class, 'declined_bootcamper_id');
    }
}
