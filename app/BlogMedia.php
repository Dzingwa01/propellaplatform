<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogMedia extends BaseModel
{
    //
    protected $fillable = ['image_url','video','blog_id'];

    public function blog(){
        return $this->belongsTo(Blog::class,'blog_id');
    }
}
