<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureUpload extends BaseModel
{
    //
    /*protected  $table = 'venture_uploads';*/
    protected $fillable = ['company_logo_url',
        'profile_picture_url',
        'video_shot_url',
        'product_shot_url',
        'infographic_url',
        'crowdfunding_url',
        'video_links_id',
        'testimonials_id',
        'gallery_id',
        'client_contract',
        'date_of_signature',
        'tax_clearance_url',
        'date_of_clearance_tax',
        'bbbee_certificate_url',
        'date_of_bbbee',
        'ck_documents_url',
        'ckDocument_url',
        'vat_registration_number_url',
        'cipc_document_url',
        'management_account_url',
        'dormant_affidavit_url',
        'video_title',
        'venture_id',
        'market_research_upload',
        'project_plan_upload',
        'business_plan',
        'stage_two_to_three_video_url',
        'stage3_client_contract',
        'stage3_date_of_signature',
        'stage4_client_contract',
        'stage4_date_of_signature',
        'stage5_client_contract',
        'stage5_date_of_signature',
        'stage6_client_contract',
        'stage6_date_of_signature',
        'corporate_id_url','stage_three_to_four_video',
        'stage_four_to_five_video','product_video_url'];


    public function venture(){
        return $this->belongsTo(Venture::class,'venture_id');
    }

    public function venture_video_shots(){
        return $this->hasMany(VentureVideoShot::class, 'venture_upload_id');
    }

    public function venture_product_shots(){
        return $this->hasMany(VentureProductShot::class, 'venture_upload_id');
    }

    public function venture_client_contract(){
        return $this->hasMany(VentureClientContract::class, 'venture_upload_id');
    }

    public function venture_tax_clearances(){
        return $this->hasMany(VentureTaxClearance::class, 'venture_upload_id');
    }

    public function venture_bbbee_certificates(){
        return $this->hasMany(VentureBbbeeCertificate::class, 'venture_upload_id');
    }

    public function venture_ck_documents(){
        return $this->hasMany(VentureCkDocument::class, 'venture_upload_id');
    }

    public function venture_management_accounts(){
        return $this->hasMany(VentureManagementAccount::class, 'venture_upload_id');
    }

    public function venture_dormant_affidavits(){
        return $this->hasMany(VentureDormantAffidavit::class, 'venture_upload_id');
    }
    public function venture_spreadsheet(){
        return $this->hasMany(VentureSpreadsheetUpload::class, 'venture_upload_id');
    }
    public function incubatee_venture_spreadsheet(){
        return $this->hasMany(IncubateeVentureSpreadsheetUpload::class, 'venture_upload_id');
    }
    public function venture_infographic(){
        return $this->hasMany(VentureInfographic::class, 'venture_upload_id');
    }


}
