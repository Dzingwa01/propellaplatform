<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclinedBootcamperQuestionAnswer extends BaseModel
{
    protected $fillable = ['question_id','d_b_id','answer_text'];

    public function declined_bootcamper(){
        return $this->belongsTo(DeclinedBootcamper::class, 'd_b_id');
    }

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }
}
