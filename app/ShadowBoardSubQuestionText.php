<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShadowBoardSubQuestionText extends BaseModel
{
    protected $fillable = ['question_sub_text','shadow_board_question_id'];

    public function shadowBoardQuestion(){
        return $this->belongsTo(ShadowBoardQuestion::class,'shadow_board_question_id');
    }
}
