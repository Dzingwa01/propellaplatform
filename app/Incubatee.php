<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incubatee extends BaseModel
{
    protected $fillable =['user_id',
        'venture_profile_information',
        'short_bio',
        'stage',
        'elevator_pitch',
        'startup_name',
        'hub',
        'Virtual_or_Physical',
        'smart_city_tags',
        'b_type',
        'cohort',
        'company_website',
        'personal_linkedIn_url',
        'personal_facebook_url',
        'personal_instagram_url',
        'personal_twitter_url',
        'business_linkedIn_url',
        'business_facebook_url',
        'business_instagram_url',
        'contact_number',
        'physical_address_1',
        'physical_address_2',
        'physical_address_3',
        'physical_city',
        'physical_code',
        'postal_address_1',
        'postal_address_2',
        'postal_address_3',
        'postal_city',
        'postal_code',
        'client_contact',
        'date_of_signature',
        'id_document',
        'tax_clearance',
        'date_of_clearance',
        'bbbee_certificate',
        'date_of_bbbee',
        'ck_document',
        'vat_reg',
        'telephone_code',
        'precinct_info',
        'office_position',
        'printing_code',
        'warehouse_position',
        'venture_email',
        'status',
        'advisor_name',
        'mentor_name',
        'incubatee_start_date',
        'incubatee_end_date',
        'nmu_graduate',
        'disabled',
        'residence',
        'home_language',
        'business_twitter_url',
        'venture_id',
        'venture_category_id',
        'workshop_evaluation_complete',
        'result_of_contact',
        'contacted_via',
        'total_panel_score',
        'average_panel_score',
        'incubatee_popi_act_agreement',
        'popi_is_agreed',
        'stage3_workshop_complete',
        'stage4_workshop_complete',
        'stage5_workshop_complete',
        'stage6_workshop_complete',
        'end_of_stage3_complete',
        'pre_stage_two_complete',
        'post_stage_two_complete',
        'pre_stage_three_complete',
        'post_stage_three_complete',
        'pre_stage_four_complete',
        'post_stage_four_complete',
        'pre_stage_five_complete',
        'post_stage_five_complete','nda_agreement','nda_agreed'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function ownership(){
        return $this->hasOne(IncubateeOwnership::class,'incubatee_id');
    }

    public function uploads(){
        return $this->hasOne(IncubateeUpload::class, 'incubatee_id');
    }

    public function testimonials(){
        return $this->hasMany(IncubateeTestimonial::class, 'incubatee_id');
    }

    public function incubateeGallery(){
        return $this->hasMany(IncubateeGallery::class, 'incubatee_id');
    }

    public function incubateeEmployee(){
        return $this->hasMany(IncubateeEmployee::class, 'incubatee_id');
    }

    public function venture(){
        return $this->belongsTo(Venture::class,'venture_id');
    }

    public function events(){
        return $this->hasMany(EventIncubatee::class, 'incubatee_id');
    }

    public function incubateeDeregisters(){
        return $this->hasMany(IncubateeEventDeregister::class, 'incubatee_id');
    }

    public function venture_category(){
        return $this->belongsTo(VentureCategory::class, 'venture_category_id');
    }

    public function stages(){
        return $this->belongsToMany(IncubateeStage::class, 'stage_incubatees', 'incubatee_id');
    }

    public function panelists(){
        return $this->hasMany(IncubateePanelist::class, 'incubatee_id');
    }

    //Contact Log
    public function incubateeContactLog(){
        return $this->hasMany(IncubateeContactLog::class, 'incubatee_id');
    }

    public function presentations(){
        return $this->hasMany(IncubateePresentation::class,'incubatee_id');
    }

}
