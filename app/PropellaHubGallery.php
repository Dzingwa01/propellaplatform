<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropellaHubGallery extends BaseModel
{
    protected $fillable = ['single_image','image_url','description','date_added'];

    public function satelliteMedia(){
        return $this->hasMany(SatelliteGallery::class, 'propella_hub_id');
    }
}
