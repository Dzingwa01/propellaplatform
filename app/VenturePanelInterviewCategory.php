<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenturePanelInterviewCategory extends BaseModel
{
    protected $fillable = ['category_name'];

    public function venturePanelInterviews(){
        return $this->hasMany(VenturePanelInterview::class, 'venture_panel_interview_category_id');
    }
}
