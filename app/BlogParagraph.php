<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogParagraph extends BaseModel
{
    protected $fillable =['paragraph_number','paragraph_title','paragraph_text','blog_section_id'];

    public function blogSection(){
        return $this->belongsTo(BlogSection::class,'blog_section_id');
    }
}
