<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateeStage extends BaseModel
{
    protected $fillable = ['stage_name'];

    public function incubatees(){
        return $this->belongsToMany(Incubatee::class, 'stage_incubatees', 'incubatee_stage_id');
    }
}
