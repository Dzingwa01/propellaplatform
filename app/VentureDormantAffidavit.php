<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureDormantAffidavit extends BaseModel
{
    protected $fillable = ['dormant_affidavit_url', 'venture_upload_id','affidavity_expiry_date'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
