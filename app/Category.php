<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends BaseModel
{
    //
    
    protected $fillable = ['category_name','category_description'];

    public function companies(){
        return $this->hasMany(Company::class,'category_id');
    }

    public function employees(){
        return $this->belongsToMany(CompanyEmployee::class,'category_company_employee','category_id');
    }
}
