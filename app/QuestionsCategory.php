<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionsCategory extends BaseModel
{
    //populate database field
    protected $fillable = ['category_name','category_description','category_image_url'];

    public function questions(){
        return $this->hasMany(Question::class,'question_category_id');
    }

    public function applicant_panelists(){
        return $this->hasMany(ApplicantPanelist::class, 'question_category_id');
    }

    public function bootcamper_panelists(){
        return $this->hasMany(BootcamperPanelist::class, 'question_category_id');
    }

    public function declined_bootcamper_panelists(){
        return $this->hasMany(DeclinedBootcamperPanelist::class, 'question_category_id');
    }

    public function venturePanelInterviews(){
        return $this->hasMany(VenturePanelInterview::class, 'question_category_id');
    }

    public function rinp_applicant_panelists(){
        return $this->hasMany(RinpApplicantPanelist::class, 'question_category_id');
    }
}















