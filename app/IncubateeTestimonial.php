<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateeTestimonial extends BaseModel
{
    //
    protected $fillable = ['testimonial_header', 'testimonial_text', 'incubatee_id'];


    public function incubatee(){
        return $this->belongsTo(Incubatee::class, 'incubatee_id');
    }
}
