<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SafetyForm extends BaseModel
{
    protected $fillable = ['fist_and_last_name','id_number','contact_number','street_address','employer',
    'age','temperature','medical_history','travel','recent_care','signs_symptoms','signature','date'];
}
