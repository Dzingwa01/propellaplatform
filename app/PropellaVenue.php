<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropellaVenue extends BaseModel
{
    public $table = "propella_venues";
    protected $fillable = ['venue_name', 'number_of_seats'];

    public function venueBooking()
    {
        return $this->hasMany(UserVenueBooking::class, 'propella_venue_id');
    }
}
