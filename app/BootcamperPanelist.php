<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BootcamperPanelist extends BaseModel
{
    protected $fillable = ['bootcamper_id', 'panelist_id', 'panel_selection_date', 'panel_selection_time', 'question_category_id'];

    public function bootcamper(){
        return $this->belongsTo(Bootcamper::class, 'bootcamper_id');
    }

    public function panelist(){
        return $this->belongsTo(Panelist::class, 'panelist_id');
    }

    public function panelistQuestionAnswers(){
        return $this->hasMany(IctPanelistQuestionAnswer::class, 'bootcamper_panelist_id');
    }

    public function questionCategory(){
        return $this->hasOne(QuestionsCategory::class, 'question_category_id');
    }
}
