<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorDeregister extends BaseModel
{
    //
    protected $fillable = ['event_id', 'event_name', 'visitor_id', 'de_registered', 'de_register_reason', 'date_time_de_register'];

    public function event(){
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function visitor(){
        return $this->belongsTo(Visitor::class, 'visitor_id');
    }
}
