<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class EventBootcamper extends BaseModel
{
    protected $fillable = ['event_id', 'bootcamper_id', 'accepted', 'date_registered', 'attended', 'declined','cell_number','network'];

    public function event(){
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function bootcamper(){
        return $this->belongsTo(Bootcamper::class, 'bootcamper_id');
    }
}
