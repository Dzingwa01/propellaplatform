<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends BaseModel
{
    protected $fillable = ['email','fullname'];
}
