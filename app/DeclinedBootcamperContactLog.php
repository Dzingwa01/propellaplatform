<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclinedBootcamperContactLog extends BaseModel
{
    protected $fillable = ['comment', 'date','declined_bootcamper_id','bootcamper_contact_results'];
}
