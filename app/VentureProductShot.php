<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureProductShot extends BaseModel
{
    protected $fillable = ['product_shot_url', 'venture_upload_id'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
