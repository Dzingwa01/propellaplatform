<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclinedBootcamperPanelist extends BaseModel
{
    protected $fillable = ['declined_bootcamper_id', 'panelist_id', 'panel_selection_date', 'panel_selection_time', 'question_category_id'];

    public function declined_bootcamper(){
        return $this->belongsTo(DeclinedBootcamper::class, 'declined_bootcamper_id');
    }

    public function declined_bootcamper_panelist(){
        return $this->belongsTo(Panelist::class, 'panelist_id');
    }

    public function declined_bootcamper_panelist_question_answers(){
        return $this->hasMany(DeclinedBootcamperPanelistQuestionAnswers::class, 'declined_bootcamper_panelist_id');
    }

    public function declined_bootcamper_question_category(){
        return $this->hasOne(QuestionsCategory::class, 'question_category_id');
    }
}
