<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends BaseModel
{
    //populate fields

    protected $fillable = ['question_text', 'percentage', 'question_category_id', 'question_number','question_type'];


    public function category()
    {
        return $this->belongsTo(QuestionsCategory::class, 'question_category_id');
    }

    public function userQuestionAnswers(){
        return $this->hasMany(UserQuestionAnswer::class, 'question_id');
    }

    public function questionSubTexts()
    {
        return $this->hasMany(QuestionSubText::class, 'question_id');
    }

    public function panelistQuestionAnswers(){
        return $this->hasMany(PanelistQuestionAnswer::class, 'question_id');
    }

    public function ictPanelistQuestionAnswers(){
        return $this->hasMany(IctPanelistQuestionAnswer::class, 'question_id');
    }

    public function declined_applicant_question_answers(){
        return $this->hasMany(DeclinedApplicantQuestionAnswer::class, 'question_id');
    }

    public function declined_bootcamper_question_answers(){
        return $this->hasMany(DeclinedBootcamperQuestionAnswer::class, 'question_id');
    }

    public function declined_bootcamper_panelist_question_answers(){
        return $this->hasMany(DeclinedBootcamperPanelistQuestionAnswers::class, 'question_id');
    }

    public function venturePanelInterviewQuestionAnswers(){
        return $this->hasMany(VenturePanelistQuestionAnswer::class, 'question_id');
    }

    public function referred_applicant_question_answer(){
        return $this->hasMany(ReferredApplicantQuestionAnswer::class,'question_id');
    }

    public function rinp_applicant_question_answers(){
        return $this->hasMany(RinpPanelistQuestionAnswer::class, 'question_id');
    }
}
