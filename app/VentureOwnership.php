<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureOwnership extends BaseModel
{
    //
    protected  $table = 'venture_ownership';
    protected $fillable = ['registration_number',
        'BBBEE_level',
        'ownership',
        'venture_id'];



    public function venture(){
        return $this->belongsTo(Venture::class,'venture_id');
    }
}
