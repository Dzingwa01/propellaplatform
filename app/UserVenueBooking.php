<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVenueBooking extends BaseModel /*implements \MaddHatter\LaravelFullcalendar\UserVenueBooking*/
{
    public $table = "user_venue_bookings";
    protected $fillable = ['venue_date', 'venue_start_time', 'venue_end_time', 'booking_reason',
        'booking_person', 'user_id', 'propella_venue_id', 'duration_time'];

    public function propellaVenue()
    {
        return $this->hasOne(PropellaVenue::class, 'propella_venue_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /*public function getId()
    {
        return $this->id;
    }

    public function getVenueDate()
    {
        return $this->venue_date;
    }

    public function getStartTime()
    {
        return $this->venue_start_time;
    }

    public function getEndTime()
    {
        return $this->venue_end_time;
    }*/
}
