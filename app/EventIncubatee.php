<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventIncubatee extends BaseModel
{
    protected $fillable = ['event_id', 'incubatee_id', 'attended', 'registered', 'de_registered', 'de_register_reason',
        'date_time_de_register', 'date_time_registered', 'found_out_via'];

    public function events(){
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function incubatees(){
        return $this->belongsTo(Incubatee::class, 'incubatee_id');
    }
}
