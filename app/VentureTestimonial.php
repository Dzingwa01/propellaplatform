<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureTestimonial extends BaseModel
{
    //
    protected $fillable = ['testimonial_header', 'testimonial_text', 'venture_id'];


    public function venture(){
        return $this->belongsTo(Incubatee::class,'venture_id');
    }
}
