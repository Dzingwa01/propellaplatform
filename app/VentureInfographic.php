<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureInfographic extends  BaseModel
{
    protected $fillable = ['infographic_url', 'date_of_infographic', 'venture_upload_id','infographic_url_multiple'];

    public function venture_upload()
    {
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
