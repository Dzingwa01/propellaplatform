<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureVideoShot extends BaseModel
{
    protected $fillable = ['video_shot_url', 'venture_upload_id'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
