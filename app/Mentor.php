<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mentor extends BaseModel
{
    protected $fillable =['user_id','mentor_venture_id'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function venture(){
        return $this->hasMany(Venture::class, 'mentor_id');
    }

    public function questionAnswers(){
        return $this->hasMany(ShadowBoardQuestionAnswer::class, 'mentor_id');
    }

    public function mentorVentureShadow()
    {
        return $this->hasMany(MentorVentureShadowboard::class, 'mentor_id');
    }

}
