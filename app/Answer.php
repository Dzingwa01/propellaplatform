<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class Answer extends BaseModel
{
    //populate fields
    protected $fillable = ['answer_text', 'question_id', 'user_id'];


    public function question(){
        return $this->hasOne(Question::class, 'question_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'user_id');
    }
}
