<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->first_name = $data['first_name'];
        $this->email = $data['email'];
        $this->event_name = $data['event_name'];
        $this->event_date = $data['event_date'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->first_name)
            ->to($this->email)
            ->subject('Thank you for visiting' . $this->first_name)
            ->view('emails.appointment');
        /*return $this->view('emails.contact');*/
    }
}
