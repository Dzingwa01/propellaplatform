<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBulkMail extends Mailable
{
    use Queueable, SerializesModels;

    public $message;
    public $subject;
    public $path;

    public function __construct($message,$subject,$path)
    {
        $this->message = $message;
        $this->subject = $subject;
        $this->path = $path;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->path))
        return $this->subject($this->subject)->markdown('emails.bulk-mailer.send-email')->attach($this->path);
        else
            return $this->subject($this->subject)->markdown('emails.bulk-mailer.send-email');
    }
}
