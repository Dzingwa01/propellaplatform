<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropellaReferredApplicant extends BaseModel
{
    //
    protected $fillable = ['name', 'surname', 'email', 'contact_number', 'contact_number_two',
        'id_number', 'address_one', 'address_two', 'address_three', 'title', 'initials',
        'dob', 'age', 'gender', 'city', 'code', 'declined', 'referred', 'chosen_category',
        'contacted_via', 'result_of_contact', 'referred_reason','venture_name', 'referred_company',
        'referred_applicant_category_id'
    ];

    public function question_answers(){
        return $this->hasMany(ReferredApplicantQuestionAnswer::class, 'p_r_a_id');
    }

    public function referred_applicant_category(){
        return $this->belongsTo(ReferredApplicantCategory::class, 'referred_applicant_category_id');
    }

}
