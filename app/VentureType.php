<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureType extends BaseModel
{
    protected $fillable = ['type_name'];
}
