<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclinedApplicant extends BaseModel
{
    //
    protected $fillable = ['name', 'surname', 'email', 'contact_number', 'contact_number_two',
        'id_number', 'address_one', 'address_two', 'address_three', 'title', 'initials',
        'dob', 'age', 'gender', 'city', 'code', 'declined', 'referred', 'chosen_category',
        'contacted_via', 'result_of_contact', 'declined_reason','venture_name', 'referred_company',
        'declined_applicant_category_id','declined_reason_two','race'
    ];

    public function question_answers(){
        return $this->hasMany(DeclinedApplicantQuestionAnswer::class, 'd_a_id');
    }

    public function declined_applicant_category(){
        return $this->belongsTo(DeclinedApplicantCategory::class, 'declined_applicant_category_id');
    }

    //Contact Log
    public function contactLog(){
        return $this->hasMany(DeclinedApplicantContactLog::class, 'declined_applicant_id');
    }
}
