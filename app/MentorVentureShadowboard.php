<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MentorVentureShadowboard extends BaseModel
{
    protected $fillable = ['mentor_id', 'venture_id', 'shadow_board_date', 'shadow_board_time'];

    public function ventureAnswers(){
        return $this->hasMany(VentureQuestionAnswer::class, 'mentor_venture_id');
    }
    public function mentorAnswers(){
        return $this->hasMany(ShadowBoardQuestionAnswer::class, 'mentor_venture_id');
    }
    public function venture(){
        return $this->belongsTo(Venture::class, 'venture_id');
    }
    public function mentor(){
        return $this->belongsTo(Mentor::class, 'mentor_id');
    }
    public function mentorComments(){
        return $this->hasMany(ShadowBoardComment::class, 'mentor_venture_id');
    }
}
