<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends BaseModel
{
    protected $fillable = ['video','video_title','date'];
}
