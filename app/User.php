<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use Notifiable;
    use Uuids;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $keyType = 'string';
    protected $guard_name = 'web';
    public $incrementing = false;

    protected $fillable = [
        'name', 'surname', 'email', 'contact_number', 'password', 'contact_number_two', 'profile_picture_url', 'password','land_line',
        'id_number', 'address_one', 'address_two', 'address_three', 'title', 'initials', 'dob', 'age', 'gender', 'city', 'code',
        'company_name', 'position', 'race','whatsapp_number','is_vaccinated',
        'service_provider_network','data_cellnumber'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function called_logs()
    {
        return $this->hasMany(ContactLog::class, 'caller_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users', 'user_id');
    }

    /**
     * Checks if User has access to $permissions.
     */
    public function hasAccess(array $permissions): bool
    {
        // check if the permission is available in any role
        foreach ($this->roles as $role) {
            if ($role->hasAccess($permissions)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the user belongs to role.
     */
    public function inRole(string $roleSlug)
    {
        return $this->roles()->where('slug', $roleSlug)->count() == 1;
    }

    public function events(){
        return $this->hasMany(EventInternalUser::class, 'user_id');
    }

    public function userDeregisters(){
        return $this->hasMany(UserEventDeregister::class, 'user_id');
    }

    public function userQuestionAnswers(){
        return $this->hasMany(UserQuestionAnswer::class, 'user_id');
    }

    public function incubatee(){
        return $this->hasOne(Incubatee::class, 'user_id');
    }

    public function applicant(){
        return $this->hasOne(Applicant::class, 'user_id');
    }

    public function bootcamper(){
        return $this->hasOne(Bootcamper::class, 'user_id');
    }

    public function venueBooking()
    {
        return $this->hasMany(UserVenueBooking::class, 'user_id')->orderBy('venue_date','desc');
    }

    public function panelist(){
        return $this->hasOne(Panelist::class, 'user_id');
    }

    public function mentor(){
        return $this->hasOne(Mentor::class, 'user_id');
    }

    public function assigned_enquiry(){
        return $this->hasMany(GeneralAssignedEnquiry::class, 'user_id');
    }

    public function pre_assigned_enquiries(){
        return $this->hasMany(GeneralPreAssignedEnquiry::class, 'user_id');
    }

    public function covid(){
        return $this->hasMany(StaffCovidSymptom::class, 'user_id');
    }
}
