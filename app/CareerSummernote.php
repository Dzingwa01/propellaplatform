<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerSummernote extends BaseModel
{
    public $fillable = ['content','career_id'];

    public function career(){
        return $this->belongsTo(Career::class,'career_id');
    }
}
