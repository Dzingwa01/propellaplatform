<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclinedBootcamperPanelistQuestionAnswers extends BaseModel
{
    protected $fillable = ['question_id', 'score', 'comment', 'declined_bootcamper_panelist_id'];

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function declined_bootcamper_panelists(){
        return $this->belongsTo(DeclinedBootcamperPanelist::class, 'declined_bootcamper_panelist_id');
    }
}
