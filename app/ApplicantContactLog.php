<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantContactLog extends BaseModel
{
    protected $fillable = ['comment', 'date','applicant_id','applicant_contact_results','email_comment'];

}

