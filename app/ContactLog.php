<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactLog extends BaseModel
{
    //
    protected $fillable = ['reference_number','contact_date','caller_id','receiver_id','description','contact_type_id'];

    public function contact_type(){
        return $this->belongsTo(ContactType::class,'contact_type_id');
    }

    public function receiver(){
        return $this->belongsTo(CompanyEmployee::class,'receiver_id');
    }

    public function caller(){
        return $this->belongsTo(User::class,'caller_id');
    }
}
