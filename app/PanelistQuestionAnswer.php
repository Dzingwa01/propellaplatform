<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PanelistQuestionAnswer extends BaseModel
{
    //
    protected $fillable = ['question_id', 'score', 'comment', 'applicant_panelist_id'];

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function applicantPanelists(){
        return $this->belongsTo(ApplicantPanelist::class, 'applicant_panelist_id');
    }
}
