<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralEnquiryCategory extends BaseModel
{
    protected $fillable = ['category_name'];

    public function general_enquiries(){
        return $this->hasMany(GeneralEnquiry::class, 'general_enquiry_category_id');
    }
}
