<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropellaNewsletterContent extends BaseModel
{
    public $fillable = ['content','propella_news_id'];

    public function propellaNews(){
        return $this->belongsTo(PropellaNew::class,'propella_news_id');
    }
}
