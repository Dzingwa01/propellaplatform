<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Cviebrock\EloquentSluggable\Sluggable;

class PropellaLearningCurve extends BaseModel
{
//    use Sluggable;
    protected $fillable = ['title','description','added_date','learning_image_url'];
//    protected $fillable = ['title','description','slug','added_date','learning_image_url'];

    public function learningCurveSummernote(){
        return $this->hasOne(PropellaLearningCurveSummernote::class,'learning_curve_id');
    }

//    public function sluggable(): array
//    {
//        return [
//            'slug' => [
//                'source' => 'title'
//            ]
//        ];
//    }
}
