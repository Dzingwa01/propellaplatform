<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateeGallery extends Model
{
    protected $fillable = ['image_url', 'video_url'];

    public function incubatee(){
        return $this->belongsTo(Incubatee::class, 'incubatee_id');
    }
}
