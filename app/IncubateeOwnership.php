<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateeOwnership extends BaseModel
{
    //
    protected $fillable = ['registration_number',
        'BBBEE_level',
        'ownership',
        'incubatee_id'];



    public function incubatee(){
        return $this->belongsTo(Incubatee::class,'incubatee_id');
    }
}
