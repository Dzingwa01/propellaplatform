<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclinedApplicantCategory extends BaseModel
{
    protected $fillable = ['category_name'];

    public function declined_applicants(){
        return $this->hasMany(DeclinedApplicant::class, 'declined_applicant_category_id');
    }
}
