<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclinedApplicantContactLog extends BaseModel
{
    protected $fillable = ['comment', 'date','declined_applicant_id','applicant_contact_results'];
}
