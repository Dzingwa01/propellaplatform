<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralAssignedEnquiry extends BaseModel
{
    protected $fillable = ['user_id', 'general_enquiry_id', 'initial_contact_person', 'initial_contact_method',
        'initial_report', 'enquiry_next_step', 'enquiry_assigned_person', 'date_assigned'];

    public function general_enquiry(){
        return $this->belongsTo(GeneralEnquiry::class, 'general_enquiry_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
