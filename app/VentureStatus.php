<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureStatus extends BaseModel
{
    protected $fillable = ['status_name'];
}
