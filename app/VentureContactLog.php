<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureContactLog extends BaseModel
{
    protected $fillable = ['comment', 'date','venture_id','venture_contact_results','comment_email'];
}
