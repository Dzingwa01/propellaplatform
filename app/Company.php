<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends BaseModel
{
    //
    protected $fillable = ['company_name',
        'address_one',
        'address_two',
        'address_three',
        'city',
        'postal_code',
        'contact_number',
        'website_url',
        'category_id',
        'company_sector_id',
        'turnover'];

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }

    public function secondary_interests(){
        return $this->belongsToMany(Category::class,'secondary_interests','company_id');
    }

    public function employees(){
        return $this->hasMany(CompanyEmployee::class,'company_id');
    }

    public function sector() {
        return $this->belongsTo(CompanySector::class, 'company_sector_id');
    }
}
