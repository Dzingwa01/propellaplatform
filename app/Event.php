<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Cviebrock\EloquentSluggable\Sluggable;

class Event extends BaseModel implements \MaddHatter\LaravelFullcalendar\Event
{
//    use Sluggable;
    //
    public $table = "events";
    protected $dates =['start','end'];
    protected $fillable = ['title','start','end','all_day','color','slug', 'description','type','participants',
        'venue_types','user_id', 'event_venue_id', 'start_time', 'end_time', 'eventStatus' ,'event_image_url','invite_image_url','eventShow'];


    public function EventsMedia(){
        return $this->hasOne(EventsMedia::class, 'events_id');
    }

    public function getEventOptions()
    {
        return [
            'color' => $this->color,
            //etc
        ];
    }

    public function visitors(){
        return $this->hasMany(EventVisitor::class, 'event_id');
    }

    public function visitorDeregisters(){
        return $this->hasMany(VisitorDeregister::class, 'event_id');
    }

    public function incubatees(){
        return $this->hasMany(EventIncubatee::class, 'event_id');
    }

    public function incubateeDeregisters(){
        return $this->hasMany(IncubateeEventDeregister::class, 'event_id');
    }

    public function internalUsers(){
        return $this->hasMany(EventInternalUser::class, 'event_id');
    }

    public function internalUserDeregisters(){
        return $this->hasMany(UserEventDeregister::class, 'event_id');
    }

    public function bootcampers(){
        return $this->hasMany(EventBootcamper::class, 'event_id');
    }
    public function declinedBootcamperEvent(){
        return $this->hasMany(DeclinedBootcamperEvent::class, 'event_id');
    }

    public function declined_bootcampers(){
        return $this->hasMany(DeclinedBootcamper::class, 'event_id');
    }

    public function EventVenue(){
        return $this->belongsTo(EventVenue::class, 'event_venue_id');
    }
//    public function sluggable(): array
//    {
//        return [
//            'slug' => [
//                'source' => 'title'
//            ]
//        ];
//    }
    /**
     * Get the event's id number
     *
     * @return int
     */

    public function getId() {
        return $this->id;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return (bool)$this->all_day;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    public function getColor()
    {
        return ['color'=>$this->color,];
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function isType()
    {
        return (bool)$this->type;
    }

    public function startTime()
    {
        return (bool)$this->start_time;
    }

    public function endTime()
    {
        return (bool)$this->end_time;
    }
}
