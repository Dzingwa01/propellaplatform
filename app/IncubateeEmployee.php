<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateeEmployee extends Model
{
    protected $dates =['start_date','end_date'];
    protected $fillable = ['employee_name','position','start_date','end_date','contract_url', 'incubatee_id'];

    public function incubatee(){
        return $this->belongsTo(Incubatee::class, 'incubatee_id');
    }
}
