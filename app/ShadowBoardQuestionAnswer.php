<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShadowBoardQuestionAnswer extends BaseModel
{
    protected $fillable = ['shadow_board_question_id','answer_text','mentor_id', 'mentor_venture_id'];

    public function shadowQuestion(){
        return $this->belongsTo(ShadowBoardQuestion::class, 'shadow_board_question_id');
    }
    public function mentor(){
        return $this->belongsTo(Mentor::class,'mentor_id');
    }

    public function mentorShadowboard(){
        return $this->belongsTo(MentorVentureShadowboard::class, 'mentor_venture_id');
    }
}
