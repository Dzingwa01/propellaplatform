<?php

namespace App;

//use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Blog extends BaseModel
{
//    use Sluggable;
    //
    protected $fillable = ['title','body','description','blog_date','slug','blog_image_url','author','pdf_url','pdf_description'];


    public function blogMedia(){
        return $this->hasMany(BlogMedia::class,'blog_id');
    }

    public function blogSection(){
        return $this->hasMany(BlogSection::class,'blog_id');
    }

    public function summernote(){
        return $this->hasOne(Summernote::class,'blog_id');
    }

//    public function sluggable(): array
//    {
//        return [
//            'slug' => [
//                'source' => 'title'
//            ]
//        ];
//    }

}
