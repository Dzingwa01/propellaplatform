<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShadowBoardComment extends BaseModel
{
    protected $fillable = ['comment_section','mentor_id', 'mentor_venture_id','venture_id'];

    public function mentor(){
        return $this->belongsTo(Mentor::class,'mentor_id');
    }

    public function mentorShadowboard(){
        return $this->belongsTo(MentorVentureShadowboard::class, 'mentor_venture_id');
    }
}
