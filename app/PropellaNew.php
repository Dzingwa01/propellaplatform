<?php

namespace App;

//use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class PropellaNew extends BaseModel
{
//    use Sluggable;
//
    protected $fillable = ['title','description','slug','added_date','news_image_url'];

    public function propellaNewsContent(){
        return $this->hasOne(PropellaNewsletterContent::class,'propella_news_id');
    }

//    public function sluggable(): array
//    {
//        return [
//            'slug' => [
//                'source' => 'title'
//            ]
//        ];
//    }
}
