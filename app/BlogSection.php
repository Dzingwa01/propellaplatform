<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogSection extends BaseModel
{
    protected $fillable = ['section_number','section_header','section_image_url','section_video_url','blog_id'];

    public function blogParagraph(){
        return $this->hasMany(BlogParagraph::class,'blog_section_id');
    }

    public function blog(){
        return $this->belongsTo(Blog::class,'blog_id');
    }
}
