<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncPanelistQuestionAnswer extends BaseModel
{
    protected $fillable = ['question_id', 'score', 'comment','incubatee_panelist_id'];

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function incubateePanelists(){
        return $this->belongsTo(IncubateePanelist::class, 'incubatee_panelist_id');
    }
}
