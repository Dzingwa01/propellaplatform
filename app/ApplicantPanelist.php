<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class ApplicantPanelist extends BaseModel
{
    //
    protected $fillable = ['applicant_id', 'panelist_id', 'panel_selection_date', 'panel_selection_time', 'question_category_id'];

    public function applicant(){
        return $this->belongsTo(Applicant::class, 'applicant_id');
    }

    public function panelist(){
        return $this->belongsTo(Panelist::class, 'panelist_id');
    }

    public function panelistQuestionAnswers(){
        return $this->hasMany(PanelistQuestionAnswer::class, 'applicant_panelist_id');
    }

    public function questionCategory(){
        return $this->hasOne(QuestionsCategory::class, 'question_category_id');
    }
}
