<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuestionAnswer extends BaseModel
{
    protected $fillable = ['question_id','user_id','answer_text'];

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
