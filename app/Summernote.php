<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Summernote extends BaseModel
{
   public $fillable = ['content','id','title','description','blog_image_url','blog_id'];

    public function blog(){
        return $this->belongsTo(Blog::class,'blog_id');
    }
}
