<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundingResource extends BaseModel
{
    protected $fillable = ['title','closing_date','text','image_url','resource_category_id'];

    public function fundingResourceDocuments(){
        return $this->hasMany(ResourceDocument::class,'fundingResource_id');
    }

    public function fundingResourceCategory(){
        return $this->belongsTo(ResourceCategory::class,'resource_category_id');
    }

    public function resourceSummernote(){
        return $this->hasOne(ResourceSummernote::class,'fundingResource_id');
    }

}
