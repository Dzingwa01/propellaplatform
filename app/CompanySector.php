<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySector extends BaseModel
{
    //
    protected $fillable = ['sector_name'];

    public function companies(){
        return $this->hasMany(Company::class, 'company_sector_id');
    }
}
