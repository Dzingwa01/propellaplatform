<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyEmployeeCovidForm extends BaseModel
{
    protected $fillable = [
        'company_employee_id','employee_covid_symptoms_one', 'employee_covid_symptoms_two',
        'employee_declaration','employee_digitally_signed_declaration',
        'date','employee_health_declaration','employee_temperature','is_vaccinated'];

    public function companyEmployees()
    {
        return $this->belongsTo(CompanyEmployee::class, 'company_employee_id');
    }
}
