<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class IctPanelistQuestionAnswer extends BaseModel
{
    protected $fillable = ['question_id', 'score', 'comment', 'bootcamper_panelist_id'];

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function bootcamperPanelists(){
        return $this->belongsTo(BootcamperPanelist::class, 'bootcamper_panelist_id');
    }
}
