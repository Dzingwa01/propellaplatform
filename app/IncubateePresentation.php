<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncubateePresentation extends BaseModel
{
    protected $fillable = ['incubatee_id','presentation_id','start_date','end_date'];

    public function incubatee(){
        return $this->hasMany(Incubatee::class, 'incubatee_id');
    }

    public function presentation(){
        return $this->belongsTo(Presentation::class, 'presentation_id');
    }

}
