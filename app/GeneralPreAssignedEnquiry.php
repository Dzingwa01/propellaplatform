<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralPreAssignedEnquiry extends BaseModel
{
    protected $fillable = ['user_id', 'general_enquiry_id', 'date_pre_assigned'];

    public function general_enquiry(){
        return $this->belongsTo(GeneralEnquiry::class, 'general_enquiry_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
