<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventVisitor extends BaseModel
{
    //
    protected $fillable = ['visitor_id', 'event_id', 'attended', 'registered', 'date_time_registered', 'found_out_via'];

    public function visitor(){
        return $this->belongsTo(Visitor::class, 'visitor_id');
    }

    public function event(){
        return $this->belongsTo(Event::class, 'event_id');
    }
}
