<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureCkDocument extends BaseModel
{
    protected $fillable = ['ck_documents_url', 'venture_upload_id','ck_expiry_date'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
