<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BootcamperContactLog extends BaseModel
{
    protected $fillable = ['comment', 'date','bootcamper_id','bootcamper_contact_results'];
}
