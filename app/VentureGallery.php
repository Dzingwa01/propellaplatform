<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureGallery extends BaseModel
{
    //
    protected $fillable = ['image_url', 'video_url', 'venture_id'];

    public function venture(){
        return $this->belongsTo(Incubatee::class,'venture_id');
    }
}
