<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureTaxClearance extends BaseModel
{
    protected $fillable = ['tax_clearance_url', 'date_of_tax_clearance', 'venture_upload_id',
    'tax_expiry_date'];

    public function venture_upload(){
        return $this->belongsTo(VentureUpload::class, 'venture_upload_id');
    }
}
