<?php

namespace App\Exports;

use App\Visitor;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VisitorSummaryExport implements FromArray, WithHeadings
{
    protected $visitor_summary;

    public function __construct(Visitor $visitor)
    {
        $visitor->load('visitor_backup');
        $visitor_summary_array = [];

        foreach($visitor->visitor_backup as $visitor_backup){
            $object = (object)[
                'visit_category' => $visitor_backup->visit_category,
                'visit_reason' => $visitor_backup->visit_reason,
                'date_in' => $visitor_backup->date_in,
                'found_us_via' => $visitor_backup->found_us_via
            ];
            array_push($visitor_summary_array, $object);
        }
        $this->visitor_summary = $visitor_summary_array;
    }

    public function array(): array
    {
        return $this->visitor_summary;
    }

    public function headings(): array
    {
        return [
            'Visit Category',
            'Visit Reason',
            'Date In',
            'Found Us Via'
        ];
    }
}
