<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomSQLExport implements FromArray
{
    public function __construct($results){
        //Dont know what the values are coming through, so giving it a dynamic name
        $valuesArray = [];

        //Looping through the unknown
        foreach ($results as $key => $value) {
            //Unknown object
            $object = (object)[
                $key => $value
            ];
            //Push to array
            array_push($valuesArray, $object);
        }

        $this->array = $valuesArray;
    }

    public function array(): array{
        return $this->array;
    }

    /*public function headings(): array
    {
        //Set unknown headers array
        $headerArray = [];

        //Loop through unknown values and set each header = key (will have same values)
        //Looping through the unknown
        foreach ($this->array as $key => $value) {
            //Push to array
            array_push($headerArray, $key);
        }

        //Return unknown header array
        return $headerArray;
    }*/
}
