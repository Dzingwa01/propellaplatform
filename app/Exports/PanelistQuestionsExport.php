<?php

namespace App\Exports;

use App\Applicant;
use App\ApplicantPanelist;
use App\PanelistQuestionAnswer;
use App\Question;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PanelistQuestionsExport implements FromArray, WithHeadings
{
    protected $applicant_panelist;

    public function __construct(Applicant $applicant)
    {
        $applicant->load('panelists');
        $applicant_panelists = $applicant->panelists;
        $final_panelist_question_score_array = [];
        $panelists_question_score_collection = [];
        $temp_question_numbers = [];

        foreach ($applicant_panelists as $applicant_panelist) {
            $applicant_panelist->load('panelist');
            $cur_panelist = $applicant_panelist->panelist;
            $cur_panelist->load('user');
            $cur_user = $cur_panelist->user;
            $cur_user_name = $cur_user->name;
            $cur_user_surname = $cur_user->surname;

            $panelist_question_answers = PanelistQuestionAnswer::where('applicant_panelist_id', $applicant_panelist->id)->get();

            foreach ($panelist_question_answers as $panelist_question_answer) {
                $question_id = $panelist_question_answer->question_id;
                $cur_question = Question::find($question_id);
                $cur_question_number = $cur_question->question_number;
                $cur_score = $panelist_question_answer->score;
                $object = (object)['question_number' => $cur_question_number,
                    'question_score' => $cur_score];

                $object = (object)['name' => $cur_user_name, 'surname' => $cur_user_surname,
                    'question_number' => $cur_question->question_number, 'question_text' => $cur_question->question_text,
                    'score' => $panelist_question_answer->score, 'comment' => $panelist_question_answer->comment];
                array_push($final_panelist_question_score_array, $object);
            }
        }


        $this->array = $final_panelist_question_score_array;
    }

    public function array(): array
    {
        return $this->array;
    }

    public function headings(): array
    {
        return [
            'First Name', 'Surname',
            'Question Number', 'Question Text',
            'User Score', 'User Comment'
        ];
    }

}
