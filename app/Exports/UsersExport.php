<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromArray, WithHeadings
{
    protected $users;

    public function __construct()
    {
        $all_users = User::all();
        $users_array = [];

        foreach($all_users as $user){

            $object = (object)['title' => $user->title,
                'initials' => $user->initials,
                'name' => $user->name,
                'surname' => $user->surname,
                'email' => $user->email,
                'contact_number' => $user->contact_number,
                'contact_number_two' => $user->contact_number_two,
                'landline' => $user->landline,
                'id_number' => $user->id_number,
                'gender' => $user->gender,
                'dob' => $user->dob,
                'address_one' => $user->address_one,
                'address_two' => $user->address_two,
                'address_three' => $user->address_three,
                'code' => $user->code,
                'city' => $user->city,
            ];
            array_push($users_array, $object);
        }
        $this->users = $users_array;
    }

    public function array(): array
    {
        return $this->users;
    }

    public function headings(): array
    {
        return [
            'Title',
            'Initials',
            'Name',
            'Surname',
            'Email',
            'Contact Number',
            'Alternative Contact Number',
            'Landline',
            'ID Number',
            'Gender',
            'Date of Birth',
            'Address One',
            'Address Two',
            'Address Three',
            'Postal Code',
            'City',
        ];
    }
}
