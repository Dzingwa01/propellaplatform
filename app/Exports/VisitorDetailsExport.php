<?php

namespace App\Exports;

use App\Visitor;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VisitorDetailsExport implements FromArray, WithHeadings
{
    protected $visitor;

    public function __construct(Visitor $visitor)
    {
        $visitor_details_array = [];

        $object = (object)[
            'title' => $visitor->title,
            'first_name' => $visitor->first_name,
            'last_name' => $visitor->last_name,
            'email' => $visitor->email,
            'cell_number' => $visitor->cell_number,
            'dob' => $visitor->dob,
            'company' => $visitor->company,
            'designation' => $visitor->designation
        ];
        array_push($visitor_details_array, $object);
        $this->visitor = $visitor_details_array;
    }

    public function array(): array
    {
        return $this->visitor;
    }

    public function headings(): array
    {
        return [
            'Title',
            'Name',
            'Surname',
            'Email',
            'Contact Number',
            'Date of Birth',
            'Company',
            'Designation'
        ];
    }
}
