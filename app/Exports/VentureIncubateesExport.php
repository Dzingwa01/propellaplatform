<?php

namespace App\Exports;

use App\Venture;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VentureIncubateesExport implements FromArray, WithHeadings
{
    protected $venture_incubatees;

    public function __construct(Venture $venture)
    {
        $venture->load('incubatee');
        $venture_incubatee_array = [];

        foreach($venture->incubatee as $venture_incubatee){
            $venture_incubatee->load('user');
            $incubatee_user = $venture_incubatee->user;

            $object = (object)['title' => $incubatee_user->title,
                'initials' => $incubatee_user->initials,
                'name' => $incubatee_user->name,
                'surname' => $incubatee_user->surname,
                'email' => $incubatee_user->email,
                'gender' => $incubatee_user->gender,
                'contact_number' => $incubatee_user->contact_number,
                'contact_number_two' => $incubatee_user->contact_number_two,
                'age' => $incubatee_user->age,
                'dob' => $incubatee_user->dob,
                'id_number' => $incubatee_user->id_number,
                'address_one' => $incubatee_user->address_one,
                'address_two' => $incubatee_user->address_two,
                'address_three' => $incubatee_user->address_three,
                'city' => $incubatee_user->city,
                'short_bio' => $incubatee_user->short_bio,
                'elevator_pitch' => $incubatee_user->elevator_pitch
            ];
            array_push($venture_incubatee_array, $object);
        }
        $this->venture_incubatees = $venture_incubatee_array;
    }

    public function array(): array
    {
        return $this->venture_incubatees;
    }

    public function headings(): array
    {
        return [
            'Title',
            'Initials',
            'Name',
            'Surname',
            'Email',
            'Gender',
            'Contact Number',
            'Alternative Contact Number',
            'Age',
            'Date of Birth'   ,
            'ID Number',
            'Address One',
            'Address Two',
            'Address Three',
            'City',
            'Short Bio',
            'Elevator Pitch'
        ];
    }
}
