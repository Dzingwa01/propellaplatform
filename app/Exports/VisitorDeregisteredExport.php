<?php

namespace App\Exports;

use App\Event;
use App\Visitor;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VisitorDeregisteredExport implements FromArray, WithHeadings
{
    protected $visitor_deregistered_events;

    public function __construct(Visitor $visitor)
    {
        $visitor->load('visitorDeregisters');
        $visitor_deregistered_events_array = [];

        foreach($visitor->visitorDeregisters as $visitor_deregistered_event){
            $event_id = $visitor_deregistered_event->event_id;
            $cur_event = Event::find($event_id);

            $object = (object)[
                'name' => $visitor->first_name,
                'surname' => $visitor->last_name,
                'event_title' => $cur_event->title,
                'de_registered' => $visitor_deregistered_event->de_registered,
                'de_registered_reason' => $visitor_deregistered_event->de_registered_reason,
                'date_time_de_register' => $visitor_deregistered_event->date_time_de_register
            ];
            array_push($visitor_deregistered_events_array, $object);
        }
        $this->visitor_deregistered_events = $visitor_deregistered_events_array;
    }

    public function array(): array
    {
        return $this->visitor_deregistered_events;
    }

    public function headings(): array
    {
        return [
            'First Name',
            'Surname',
            'Event Title',
            'De-registered',
            'De-registered Reason',
            'Date / Time De-registered'
        ];
    }
}
