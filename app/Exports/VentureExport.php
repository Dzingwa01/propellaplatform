<?php

namespace App\Exports;

use App\Venture;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VentureExport implements FromArray, WithHeadings
{
    protected $venture;

    public function __construct(Venture $venture)
    {
        $venture->load('ventureOwnership', 'ventureUploads');
        $venture_array = [];
        $object = (object)['company_name' => $venture->company_name,
            'contact_number' => $venture->contact_number,
            'smart_city_tags' => $venture->smart_city_tags,
            'business_type' => $venture->business_type,
            'hub' => $venture->hub,
            'elevator_pitch' => $venture->elevator_pitch,
            'stage' => $venture->stage,
            'cohort' => $venture->cohort,
            'venture_profile_information' => $venture->venture_profile_information,
            'business_linkedin_url' => $venture->business_linkedin_url,
            'business_facebook_url' => $venture->business_facebook_url,
            'business_twitter_url' => $venture->business_twitter_url,
            'business_instagram_url' => $venture->business_instagram_url,
            'physical_address_one' => $venture->physical_address_one,
            'physical_address_two' => $venture->physical_address_two,
            'physical_address_three' => $venture->physical_address_three,
            'physical_city' => $venture->physical_city,
            'physical_postal_code' => $venture->physical_postal_code,
            'postal_address_one' => $venture->postal_address_one,
            'postal_address_two' => $venture->postal_address_two,
            'postal_address_three' => $venture->postal_address_three,
            'postal_city' => $venture->postal_city,
            'postal_code' => $venture->postal_code,
            'virtual_or_physical' => $venture->virtual_or_physical,
            'pricinct_telephone_code' => $venture->pricint_telephone_code,
            'pricinct_keys' => $venture->pricint_keys,
            'pricinct_office_position' => $venture->pricint_office_position,
            'pricinct_printing_code' => $venture->pricint_printing_code,
            'rent_turnover' => $venture->rent_turnover,
            'rent_date' => $venture->rent_date,
            'rent_amount' => $venture->rent_amount,
            'status' => $venture->status,
            'alumni_date' => $venture->alumni_date,
            'exit_date' => $venture->exit_date,
            'resigned_date' => $venture->resigned_date,
            'mentor' => $venture->mentor,
            'advisor' => $venture->advisor,
            'venture_funding' => $venture->veture_funding,
            'date_awarded' => $venture->date_awarded,
            'fund_value' => $venture->fund_value,
            'date_closedout' => $venture->date_closedout,
            'venture_type' => $venture->venture_type,
            'company_website' => $venture->company_website,
            'venture_email' => $venture->venture_email,
            'reason_for_leaving' => $venture->reason_for_leaving,
            'regisration_number' => $venture->ventureOwnership->registration_number,
            'BBBEE_level' => $venture->ventureOwnership->BBBEE_level,
            'ownership' => $venture->ventureOwnership->ownership,
            'company_logo_url' => $venture->ventureUploads->company_logo_url,
            'profile_picture_url' => $venture->ventureUploads->profile_picture_url,
            'infographic_url' =>  $venture->ventureUploads->infographic_url
        ];
        array_push($venture_array, $object);

        $this->venture = $venture_array;
    }


    public function array(): array
    {
        return $this->venture;
    }


    public function headings(): array
    {
        return [
            'Company Name',
            'Contact Number',
            'Smart City Tags',
            'Business Type',
            'Hub',
            'Elevator Pitch',
            'Stage',
            'Cohort',
            'Venture Profile Information',
            'LinkedIn Url',
            'Facebook Url',
            'Twitter Url',
            'Instagram Url',
            'Physical Address One',
            'Physical Address Two',
            'Physical Address Three',
            'Physical City',
            'Postal Code',
            'Postal Address One',
            'Postal Address Two',
            'Postal Address Three',
            'Postal City',
            'Postal Code',
            'Virtual / Physical',
            'Pricinct Telephone Code',
            'Pricinct Keys',
            'Pricinct Office Position',
            'Pricinct Print Code',
            'Rent Turnover',
            'Rent Date',
            'Rent Amount',
            'Status',
            'Alumni Date',
            'Exit Date',
            'Resigned Date',
            'Mentor',
            'Advisor',
            'Venture Funding',
            'Date Awarded',
            'Fund Value',
            'Date Closed Out',
            'Venture Type',
            'Website',
            'Email',
            'Reason for Leaving',
            'Registration Number',
            'BBBEE Level',
            'Ownership',
            'Company Logo Url',
            'Profile Picture Url',
            'Infographic Url',
        ];
    }
}
