<?php

namespace App\Exports;

use App\Visitor;
use App\Event;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VisitorEventsExport implements FromArray, WithHeadings
{
    protected $visitor_events;

    public function __construct(Visitor $visitor)
    {
        $visitor->load('events');
        $visitor_events_array = [];

        foreach($visitor->events as $visitor_event){
            $event_id = $visitor_event->event_id;
            $cur_event = Event::find($event_id);

            $object = (object)[
                'name' => $visitor->first_name,
                'surname' => $visitor->last_name,
                'event_title' => $cur_event->title,
                'registered' => $cur_event->registered,
                'attended' => $cur_event->attended
            ];
            array_push($visitor_events_array, $object);
        }
        $this->visitor_events = $visitor_events_array;
    }

    public function array(): array
    {
        return $this->visitor_events;
    }

    public function headings(): array
    {
        return [
            'First Name',
            'Surname',
            'Event Title',
            'Registered',
            'Attended'
        ];
    }
}
