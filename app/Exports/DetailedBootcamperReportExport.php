<?php

namespace App\Exports;

use App\Event;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DetailedBootcamperReportExport implements FromArray, WithHeadings
{
    public function __construct(Event $event)
    {
        $event->load('bootcampers');
        $bootcamperDetailsArray = [];

        if(count($event->bootcampers) > 0){
            foreach ($event->bootcampers as $eventBootcamper){
                $eventBootcamper->load('bootcamper');
                $bootcamper = null;
                $user = null;
                $applicant = null;

                if(isset($eventBootcamper->bootcamper)){
                    $bootcamper = $eventBootcamper->bootcamper;
                    $bootcamper->load('user');
                }
                if(isset($bootcamper->user)){
                    $user = $bootcamper->user;
                    $user->load('applicant');
                }
                if(isset($user->applicant)){
                    $applicant = $user->applicant;
                }

                $object = (object)[
                    'name' => $user->name,
                    'surname' => $user->surname,
                    'email' => $user->email,
                    'venture_name' => $applicant->venture_name,
                    'contact_number' => $user->contact_number,
                    'whatsapp_number' => $user->whatsapp_number,
                    'data_cellnumber' => $user->data_cellnumber,
                    'service_provider_network' => $user->service_provider_network,
                    'id_number' => $user->id_number,
                    'age' => $user->age,
                    'gender' => $user->gender,
                    'race' => $user->race,
                    'address_one' => $user->address_one,
                    'address_two' => $user->address_two,
                    'address_three' => $user->address_three,
                    'city' => $user->city,
                    'event' => $event->title,
                    'accepted' => $eventBootcamper->accepted,
                    'attended' => $eventBootcamper->attended,
                    'nda_agreement' => $bootcamper->bootcamp_nda_agreement,
                    'cipc' => $bootcamper->cipc_uploaded,
                    'proof_of_address' => $bootcamper->proof_of_address_uploaded,
                    'id_copy' => $bootcamper->id_copy_uploaded,
                    'evaluation_form' => $bootcamper->evaluation_form_completed,
                    'pitch_video' => $bootcamper->pitch_video_uploaded
                ];
                array_push($bootcamperDetailsArray, $object);
            }
        }

        $this->array = $bootcamperDetailsArray;
    }

    public function array(): array
    {
        return $this->array;
    }

    public function headings(): array
    {
        return [
            'First Name',
            'Surname',
            'Email',
            'Venture Name',
            'ID Number',
            'Age',
            'Gender',
            'Race',
            'Event',
            'Accepted',
            'Attended',
            'NDA Agreement',
            'CIPC Uploaded',
            'POA Uploaded',
            'ID Uploaded',
            'Evaluation Form',
            'Pitch video'
        ];
    }
}
