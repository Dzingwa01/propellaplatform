<?php

namespace App\Exports;

use App\Visitor;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VisitorsExport implements FromArray, WithHeadings
{
    protected $visitors;

    public function __construct()
    {
        $all_visitors = Visitor::all();
        $visitors_array = [];

        foreach($all_visitors as $visitor){

            $object = (object)['title' => $visitor->title,
                'name' => $visitor->first_name,
                'surname' => $visitor->last_name,
                'email' => $visitor->email,
                'contact_number' => $visitor->cell_number,
                'dob' => $visitor->dob,
                'designation' => $visitor->designation
            ];
            array_push($visitors_array, $object);
        }
        $this->visitors = $visitors_array;
    }

    public function array(): array
    {
        return $this->visitors;
    }

    public function headings(): array
    {
        return [
            'Title',
          'First Name',
          'Last Name',
          'Email',
          'Contact Number',
          'Company',
          'Date of Birth',
          'Designation'
        ];
    }
}
