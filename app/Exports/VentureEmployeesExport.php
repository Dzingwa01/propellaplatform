<?php

namespace App\Exports;

use App\Venture;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VentureEmployeesExport implements FromArray, WithHeadings
{
    protected $venture_employees;

    public function __construct(Venture $venture)
    {
        $venture->load('ventureEmployee');
        $venture_employee_array = [];

        foreach($venture->ventureEmployee as $venture_employee){
            $object = (object)['title' => $venture_employee->title,
                'initials' => $venture_employee->initials,
                'name' => $venture_employee->employee_name,
                'surname' => $venture_employee->employee_surname,
                'email' => $venture_employee->email,
                'gender' => $venture_employee->gender,
                'contact_number' => $venture_employee->contact_number,
                'id_number' => $venture_employee->id_number,
                'position' => $venture_employee->position,
                'start_date' => $venture_employee->start_date,
                'end_date' => $venture_employee->end_date,
                'resigned_date' => $venture_employee->resigned_date,
                'contract_url' => $venture_employee->contract_url,
                'status' => $venture_employee->status
            ];
            array_push($venture_employee_array, $object);
        }
        $this->venture_employees = $venture_employee_array;
    }

    public function array(): array
    {
        return $this->venture_employees;
    }

    public function headings(): array
    {
        return [
            'Title',
            'Initials',
            'Name',
            'Surname',
            'Email',
            'Gender',
            'Contact Number',
            'ID Number',
            'Position',
            'Start Date',
            'End Date',
            'Resigned Date',
            'Contract Url',
            'Status'
        ];
    }
}
