<?php

namespace App\Exports;

use App\Company;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompanyDetailsExport implements FromArray, WithHeadings
{
    protected $company_details;

    public function __construct(Company $company)
    {
        $company_details_array = [];
        $company->load('category', 'sector');

        $object = (object)[
            'company_name' => $company->company_name,
            'company_category' => $company->category->category_name,
            'sector' => $company->sector->sector_name,
            'address_one' => $company->address_one,
            'address_two' => $company->address_two,
            'address_three' => $company->address_three,
            'city' => $company->city,
            'postal_code' => $company->postal_code,
            'website_url' => $company->website_url,
            'turnover' => $company->turnover
        ];
        array_push($company_details_array, $object);

        $this->company_details = $company_details_array;
    }

    public function array(): array
    {
        return $this->company_details;
    }

    public function headings(): array
    {
        return [
            'Company Name',
            'Category',
            'Sector',
            'Address One',
            'Address Two',
            'Address Three',
            'City',
            'Postal Code',
            'Website',
            'Turnover'
        ];
    }
}
