<?php

namespace App\Exports;

use App\Company;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompaniesExport implements FromArray, WithHeadings
{
    protected $companies;

    public function __construct()
    {
        $all_companies = Company::all();
        $companies_array = [];

        foreach($all_companies as $company){
            $company->load('category', 'sector');

            $object = (object)[
                'company_name' => $company->company_name,
                'company_category' => $company->category->category_name,
                'sector' => $company->sector->sector_name,
                'address_one' => $company->address_one,
                'address_two' => $company->address_two,
                'address_three' => $company->address_three,
                'city' => $company->city,
                'postal_code' => $company->postal_code,
                'website_url' => $company->website_url,
                'turnover' => $company->turnover
            ];
            array_push($companies_array, $object);
        }
        $this->companies = $companies_array;
    }

    public function array(): array
    {
        return $this->companies;
    }

    public function headings(): array
    {
        return [
            'Company Name',
            'Category',
            'Sector',
            'Address One',
            'Address Two',
            'Address Three',
            'City',
            'Postal Code',
            'Website',
            'Turnover'
        ];
    }
}
