<?php

namespace App\Exports;

use App\Company;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompanyEmployeesExport implements FromArray, WithHeadings
{
    protected $company_employees;

    public function __construct(Company $company)
    {
        $company_employees_array = [];
        $company->load('employees');

        foreach($company->employees as $c_employee){
        $object = (object)[
            'name' => $c_employee->name,
            'surname' => $c_employee->surname,
            'email' => $c_employee->email,
            'contact_number' => $c_employee->contact_number,
            'contact_number_two' => $c_employee->contact_number_two,
            'address_one' => $c_employee->address_one,
            'address_two' => $c_employee->address_two,
            'address_three' => $c_employee->address_three
        ];
        array_push($company_employees_array, $object);
        }

        $this->company_employees = $company_employees_array;
    }

    public function array(): array
    {
        return $this->company_employees;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Surname',
            'Email',
            'Contact Number',
            'Contact Number Two',
            'Address One',
            'Address Two',
            'Address Three'
        ];
    }
}
