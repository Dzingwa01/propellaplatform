<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorCovidSymptom extends BaseModel
{
    protected $fillable = [
        'visitor_id','covid_symptoms_one', 'covid_symptoms_two','declaration','digitally_signed_declaration',
        'date_visited','health_declaration','temperature','is_vaccinated'];

    public function visitor()
    {
        return $this->belongsTo(Visitor::class, 'visitor_id');
    }
}
