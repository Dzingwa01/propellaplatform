<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users|max:100',
            'name'=>'required|max:100',
            'surname'=>'required|max:100',
            'contact_number'=>'required|max:15',
            'role_id'=>'required',
            'contact_number_two'=>'nullable',
            'id_number'=>'nullable',
            'address_one'=>'nullable',
            'address_two'=>'nullable',
            'address_three'=>'nullable',
            'title',
            'initials',
            'dob',
            'age',
            'gender'
        ];
    }
}
