<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:company_employees|max:100',
            'name'=>'required|max:100',
            'surname'=>'required|max:100',
            'contact_number'=>'required|max:15',
            'contact_number_two'=>'nullable|max:15',
            'category_id'=>'required',
            'company_id'=>'required',
            'address'=>'nullable',
            'address_two'=>'nullable',
            'address_three'=>'nullable'
        ];
    }
}
