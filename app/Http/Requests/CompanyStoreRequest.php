<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required',
            'contact_number'=>'required',
            'address_one'=>'nullable|max:100',
            'address_two'=>'nullable',
            'address_three'=>'nullable',
            'website_url'=>'nullable',
            'city'=>'nullable',
            'postal_code'=>'nullable',
            'category_id'=>'required',
            'city'=>'required',
            'secondary_category_id'=>'nullable',
            'company_sector_id'=>'required',
            'turnover'=>'nullable',
            'notes_text'=>'nullable'
        ];
    }
}
