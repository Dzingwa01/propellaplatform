<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $event_media_url = count($this->input('event_media_url'));
        foreach(range(0, $event_media_url) as $index) {
            $rules['event_media_url.' . $event_media_url] = 'image|mimes:jpeg,bmp,png|max:2000';
        }
        return $rules;

        /*return [
            'event_media_url' => 'nullable',
            'media_description' => 'nullable',
            'events_id'=>'required'

        ];*/
    }
}
