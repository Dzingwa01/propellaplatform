<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'nullable',
            'start' => 'nullable',
            'end' => 'nullable',
            'all_day' => 'nullable',
            'type' => 'nullable',
            'description' => 'nullable',
            'color' => 'nullable',
            'start_time' =>'nullable',
            'end_time' =>'nullable',
            'event_media_url'=>'nullable',
            'media_description'=>'nullable'

        ];
    }
}
