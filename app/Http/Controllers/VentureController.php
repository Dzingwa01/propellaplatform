<?php

namespace App\Http\Controllers;

use App\ApplicantContactLog;
use App\Blog;
use App\Company;
use App\Event;
use App\Incubatee;
use App\IncubateeEmployee;
use App\IncubateeOwnership;
use App\IncubateeUpload;
use App\IncubateeVentureSpreadsheetUpload;
use App\Mentor;
use App\Panelist;
use App\Question;
use App\QuestionsCategory;
use App\Role;
use App\User;
use App\Venture;
use App\VentureBbbeeCertificate;
use App\VentureCategory;
use App\VentureContactLog;
use App\VentureInfographic;
use App\VentureSpreadsheetUpload;
use App\VentureStatus;
use App\VentureType;
use App\VentureCkDocument;
use App\VentureClientContract;
use App\VentureDormantAffidavit;
use App\VentureEmployee;
use App\VentureManagementAccount;
use App\VentureOwnership;
use App\VenturePanelInterview;
use App\VenturePanelInterviewCategory;
use App\VentureTaxClearance;
use App\VentureUpload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use PDF;
use Illuminate\Support\Facades\Mail;

class VentureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ventureCategoryIndex(){
        $user = Auth::user()->load('roles');

        if($user->roles[0]->name == 'app-admin'){
            return view('users.venture.venture-category-index');
        } elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.venture-category-index');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }
    public function ventureTypeIndex(){
        $user = Auth::user()->load('roles');

        if($user->roles[0]->name == 'app-admin'){
            return view('users.venture.venture-type-index');
        } elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.venture-type-index');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }


    public function getVentureCategories()
    {
        $venture_categories = VentureCategory::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($venture_categories)->addColumn('action', function ($venture_category) {
                $re = '/venture-category-edit/' . $venture_category->id;
                $del = $venture_category->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture_category(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($venture_categories)->addColumn('action', function ($venture_category) {
                $re = '/venture-category-edit/' . $venture_category->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }
    public function getVentureTypes()
    {
        $venture_types = VentureType::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($venture_types)->addColumn('action', function ($venture_type) {
                $re = '/venture-type-edit/' . $venture_type->id;
                $del = $venture_type->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture_type(this)" title="Delete Venture Type" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($venture_types)->addColumn('action', function ($venture_type) {
                $re = '/venture-type-edit/' . $venture_type->id;
                return '<a href=' . $re . ' title="Edit Venture Type" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }


    public function createVentureCategory(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.create-venture-category');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.create-venture-category');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }
    public function createVentureType(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.create-venture-type');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.create-venture-type');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }


    public function storeVentureCategory(Request $request){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $venture_category = VentureCategory::create(['category_name' => $input['category_name']]);
            DB::commit();

            return response()->json(['message' => 'Venture category added.']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }
    public function storeVentureType(Request $request){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $venture_type = VentureType::create(['type_name' => $input['type_name']]);
            DB::commit();

            return response()->json(['message' => 'Venture type added.']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    public function editVentureCategory(VentureCategory $ventureCategory){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.edit-venture-category', compact('ventureCategory'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.edit-venture-category', compact('ventureCategory'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }
    public function editVentureType(VentureType $ventureType){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.edit-venture-type', compact('ventureType'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.edit-venture-type', compact('ventureType'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }


    public function updateVentureCategory(Request $request, VentureCategory $ventureCategory){
        $input = $request->all();

        try{
            DB::beginTransaction();
            $ventureCategory->update(['category_name' => $input['category_name']]);
            $ventureCategory->save();
            DB::commit();
            return response()->json(['message' => 'Venture category updated.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }
    public function updateVentureType(Request $request, VentureType $ventureType){
        $input = $request->all();

        try{
            DB::beginTransaction();
            $ventureType->update(['type_name' => $input['type_name']]);
            $ventureType->save();
            DB::commit();
            return response()->json(['message' => 'Venture type updated.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    public function deleteVentureCategory(VentureCategory $ventureCategory){
        try{
            $ventureCategory->forceDelete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }
    public function deleteVentureType(VentureType $ventureType){
        try{
            $ventureType->forceDelete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function allPropellaVentures()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.show-all-ventures');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.show-all-ventures');
        } elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture.show-all-ventures');
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.ventures.show-all-ventures');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.show-all-ventures');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ventures.show-all-ventures');
        }
    }

    public function gePropellatVenture()
    {
        $ventures = Venture::with('incubatee')->get();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/venture-account-overview/' . $venture->id;
                $re = '/venture-edit/' . $venture->slug;
                $del = $venture->id;
                $paw = '/venture-panel-access-window/' . $venture->id;
                $venture_interviews = '/show-venture-interviews-overview/' . $venture->id;
                return '<a href=' . $view . ' title="View Venture"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a>
                <a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>
                <a href=' . $paw . ' title="Venture Panel Access Window"><i class="material-icons" style="color: orange;">account_balance</i></a>
                <a href=' . $venture_interviews . ' title="View Venture Interviews"><i class="material-icons" style="color: rebeccapurple;">face</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/venture-account-overview/' . $venture->id;
                $re = '/venture-edit/' . $venture->slug;
                $del = $venture->id;
                $paw = '/venture-panel-access-window/' . $venture->id;
                return '<a href=' . $view . ' title="View Venture"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a>
                <a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>
                <a href=' . $paw . ' title="Venture Panel Access Window"><i class="material-icons" style="color: orange;">account_balance</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/venture-account-overview/' . $venture->id;
                $re = '/venture-edit/' . $venture->slug;
                return '<a href=' . $view . ' title="View Venture"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/venture-account-overview/' . $venture->id;
                $re = '/venture-edit/' . $venture->slug;
                $del = $venture->id;
                $paw = '/venture-panel-access-window/' . $venture->id;
                return '<a href=' . $view . ' title="View Venture"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a>
                <a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>
                <a href=' . $paw . ' title="Venture Panel Access Window"><i class="material-icons" style="color: orange;">account_balance</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->slug;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response("You are not authorized!!");
        }
    }

    public function ventureIndex()
    {
        $venture_count = count(Venture::all());

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.index', compact('venture_count'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.index');
        } elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture.index');
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.ventures.index');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.index');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ventures.index');
        }
    }

    public function ictOnlyIndex()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.ict-venture-index');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.ict-venture-index');
        }elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.ventures.ict-venture-index');
        }
    }

    public function indOnlyIndex()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.ind-venture-index');
        }elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.ind-venture-index');
        }elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.ventures.ind-venture-index');
        }
    }

    public function alumniIndex()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.alumni-Index');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.alumni-Index');
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.ventures.alumni-Index');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.alumni-Index');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ventures.alumni-Index');
        }
    }

    public function exitedIndex(){
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.exited-ventures.exited-ventures');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.exited-ventures.exited-ventures');
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.exited-ventures.exited-ventures');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.exited-ventures.exited-ventures');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ventures.alumni-Index');
        }
    }

    public function getExited()
    {

        $ventures = Venture::with('ventureOwnership', 'ventureUploads')->where('status', 'Exit')->get();

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id= ' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a href='/* . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>'*/ ;
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function getINDOnly()
    {
        $ventures = Venture::with('ventureOwnership', 'ventureUploads')->where('hub', 'Industrial');

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id= ' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'advisor') {
        return Datatables::of($ventures)->addColumn('action', function ($venture) {
            $view = '/incubatee-index/' . $venture->id;
            $re = '/venture-edit/' . $venture->id;
            $del = $venture->id;
            return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id= ' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
        })
            ->make(true);
        }elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id= ' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
    }
    }

    public function getICTOnly()
    {
        $ventures = Venture::with('ventureOwnership', 'ventureUploads')->where('hub', 'ICT');

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id= ' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id= ' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
        })
            ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id= ' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
    }
    }

    public function getAlumni()
    {

        $ventures = Venture::with('ventureOwnership', 'ventureUploads')->where('status', 'Alumni')->get();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id= ' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a href='/* . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>'*/ ;
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/incubatee-index/' . $venture->id;
                $re = '/venture-edit/' . $venture->id;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function getVenture()
    {
        $ventures = Venture::with('incubatee')->where('status','Active');
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/venture-account-overview/' . $venture->id;
                $re = '/venture-edit/' . $venture->slug;
                $del = $venture->id;
                return '<a href=' . $view . ' title="View venture"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/venture-account-overview/' . $venture->id;
                $re = '/venture-edit/' . $venture->slug;
                $del = $venture->id;
                return '<a href=' . $view . ' title="View venture"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/venture-account-overview/' . $venture->id;
                $re = '/venture-edit/' . $venture->slug;
                return '<a href=' . $view . ' title="View venture"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a href=' /*. $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>'*/ ;
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $view = '/venture-account-overview/' . $venture->id;
                $re = '/venture-edit/' . $venture->slug;
                $del = $venture->id;
                return '<a href=' . $view . ' title="View venture"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($ventures)->addColumn('action', function ($venture) {
                $re = '/venture-edit/' . $venture->slug;
                $del = $venture->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture(this)" title="Delete Venture" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function ventureAccountOverview(Venture $venture)
    {
        $venture->load( 'ventureOwnership', 'ventureUploads','ventureEmployee','ventureShadow','mentor')->get();
        $ventureShadowBoard = $venture->ventureShadow;
        $ventureShadowBoard->load('mentor','venture');

        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('venture_video_shots', 'venture_product_shots', 'venture_client_contract',
            'venture_tax_clearances', 'venture_bbbee_certificates', 'venture_ck_documents',
            'venture_management_accounts', 'venture_dormant_affidavits','incubatee_venture_spreadsheet');

;        $shadow_array = [];

        foreach($ventureShadowBoard as $ventureShadowBoards){
            $ventureShadowBoards->load('mentor','venture');
            $object = (object)[
                'id' => $ventureShadowBoards->id,
                'company_name' => isset($ventureShadowBoards->venture->company_name) ? $ventureShadowBoards->venture->company_name : 'No name set',
                'shadow_board_date' => isset($ventureShadowBoards->shadow_board_date) ? $ventureShadowBoards->shadow_board_date : 'No date set',
                'shadow_board_time' => isset($ventureShadowBoards->shadow_board_time) ? $ventureShadowBoards->shadow_board_time : 'No time set',

            ];
            array_push($shadow_array, $object);
        }

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.venture-account-overview', compact('venture','shadow_array','venture_upload'));
        }elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.venture-account-overview', compact('venture','shadow_array','venture_upload'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.venture-account-overview', compact('venture','shadow_array','venture_upload'));
        }elseif($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.ventures.venture-account-overview', compact('venture','shadow_array','venture_upload'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function incubateeIndex(Venture $ventures)
    {
        $ventures->load('incubatee', 'ventureOwnership', 'ventureUploads', 'user')->get();
        $get_incubatee = Incubatee::with('venture', 'user')->where('venture_id', $ventures->id)->get();

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.incIndex', compact('venture', 'get_incubatee'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.incIndex', compact('venture', 'get_incubatee'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.ventures.incIndex');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.incIndex');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ventures.incIndex');
        }
    }

    public function getIncubatee(Venture $venture)
    {
        $venture->load('incubatee', 'ventureOwnership', 'ventureUploads');

        $venture_incubatees = $venture->incubatee;

        $single_user_array = [];

        for ($i = 0; $i < count($venture_incubatees); $i++) {
            $current_incubatee = $venture_incubatees[$i];
            $current_incubatee_with_user = $current_incubatee->user;
            array_push($single_user_array, $current_incubatee_with_user);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($single_user_array)->addColumn('action', function ($user) {
                $show_overview = '/show-venture-incubatee/' . $user->id;
                $re = '/venture-edit-incubatee/' . $user->id;
                $del = $user->id;
                return '</a><a href=' . $show_overview . ' title="View Incu"><i class="material-icons">remove_red_eye</i></a>
                <a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_incubatee_in_venture(this)" title="Delete Venture" style="color:red; cursor: pointer;"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($single_user_array)->addColumn('action', function ($user) {
                $show_overview = '/show-venture-incubatee/' . $user->id;
                $re = '/venture-edit-incubatee/' . $user->id;
                return '</a><a href=' . $show_overview . ' title="View Incu"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($single_user_array)->addColumn('action', function ($user) {
                $show_overview = '/show-venture-incubatee/' . $user->id;
                $re = '/venture-edit-incubatee/' . $user->id;
                return '</a><a href=' . $show_overview . ' title="View Incu"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($single_user_array)->addColumn('action', function ($user) {
                $show_overview = '/show-venture-incubatee/' . $user->id;
                $re = '/venture-edit-incubatee/' . $user->id;
                $del = $user->id;
                return '</a><a href=' . $show_overview . ' title="View Incu"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_incubatee_in_venture(this)" title="Delete Venture" style="color:red; cursor: pointer;"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function createVenture(){
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.create-venture');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.create-venture');
        } elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture.create-venture');
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.ventures.create-venture');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.create-venture');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ventures.create-venture');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function create()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.create');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.incubatees.create');
        } elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture.create');
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.ventures.create');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.create');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ventures.create');
        }
    }

    public function createIncubatee(Venture $venture)
    {
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'incubatee');})->orderby('name','asc')->get();

        $incubatee = Incubatee::with('user')->get();

        $incubatee_array = [];
        foreach($incubatee as $incubatees){
            $incubatees->load('user');
            if(isset($incubatees->user)){
                $object = (object)[
                    'id' => $incubatees->id,
                    'name' => isset($incubatees->user->name) ? $incubatees->user->name : 'No name set',
                    'surname' => isset($incubatees->user->surname) ? $incubatees->user->surname : 'No surname set',
                ];
                array_push($incubatee_array, $object);
            }
        }

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/venture/venture-create-incubatee', compact('venture','incubatee','incubatee_array','users'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.venture-create-incubatee', compact('venture','incubatee','incubatee_array','users'));
        } elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture.venture-create-incubatee', compact('venture','incubatee','incubatee_array'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.ventures.venture-create-incubatee', compact('venture','incubatee','incubatee_array'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.venture-create-incubatee');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ventures.venture-create-incubatee');
        }
    }

    //Assign incubatee to Venture
    public function assignIncubateeToVenture(Request $request)
    {
        $input = $request->all();
        $selected_ventures = json_decode($input['selected_ventures']);
        $venture_id = $input['venture_id'];
        $ven = Venture::find($venture_id);

        try {
            DB::beginTransaction();

            foreach ($selected_ventures as $selected_venture) {
                $incubatee_id = $selected_venture;
                $venture = Incubatee::find($incubatee_id);

                $venture->update(['venture_id' => $ven->id]);
                $venture->save();
            }

            DB::commit();
            return response()->json(['message' => 'Successfully assigned incubatee(s).'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }


    public function StoreIncubatee(Request $request)
    {
        $input = $request->all();
        $venture_id = $input['venture_id'];
        $current_venture = Venture::findOrFail($venture_id);


        try {
            DB::beginTransaction();
            $incubatee_role = Role::where('name', 'incubatee')->first();

            $user = User::create(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'], 'email' => $input['email'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'dob' => $input['dob'], 'age' => $input['age'],
                'password' => Hash::make('secret')]);

            $user->roles()->attach($incubatee_role->id);

            $incubatee = Incubatee::create(['user_id' => $user->id,
                'nmu_graduate' => $input['nmu_graduate'],
                'disabled' => $input['disabled'],
                'home_language' => $input['home_language'],
                'residence' => $input['residence'],
                'personal_facebook_url' => $input['personal_facebook_url'],
                'personal_linkedIn_url' => $input['personal_linkedIn_url'],
                'personal_twitter_url' => $input['personal_twitter_url'],
                'personal_instagram_url' => $input['personal_instagram_url']]);


            if ($request->has('venture_id')) {
                if (isset($current_venture)) {
                    $incubatee->update(['venture_id' => $current_venture->id]);
                }
            }

            DB::commit();

            return response()->json(['message' => 'Incubatee has been successfully saved. REFRESH THE PAGE TO ADD ANOTHER!', 'user' => $user, 'incubatee' => $incubatee, 'venture' => $current_venture]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function ventureIncStoreMedia(Request $request)
    {
        $input = $request->all();

        try {
            DB::beginTransaction();

            if ($request->hasFile('profile_picture_url')) {
                $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                $create_incubatee_uploads = IncubateeUpload::create(['profile_picture_url' => $profile_picture_path,
                    'incubatee_id' => $input['incubatee_id']]);

                if ($request->hasFile('id_document')) {
                    $id_document_path = $request->file('id_document')->store('incubatee_uploads');
                    $create_incubatee_uploads->update(['id_document' => $id_document_path]);
                }

            } else if ($request->hasFile('id_document')){
                $id_document_path = $request->file('id_document')->store('incubatee_uploads');
                $create_incubatee_uploads = IncubateeUpload::create(['id_document' => $id_document_path,
                    'incubatee_id' => $input['incubatee_id']]);

                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $create_incubatee_uploads->update(['profile_picture_url' => $profile_picture_path]);
                }
            }

            DB::commit();

            return response()->json(['message' => 'Incubatee media has been saved.']);

        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured: ' . $e->getMessage()]);
        }
    }

    /*Storing Venture Information*/
    public function storeBusiness(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();

        try {

            $venture = Venture::create([
                'hub' => $input['hub'],
                'smart_city_tags' => $input['smart_city_tags'],
                'business_type' => $input['business_type'],
                'company_name' => $input['company_name'],
                'slug' => Str::slug($input['company_name'], '-'),
                'stage' => $input['stage'],
                'cohort' => $input['cohort'],
                'contact_number' => $input['contact_number'],
                'physical_address_one' => $input['physical_address_one'],
                'physical_address_two' => $input['physical_address_two'],
                'physical_address_three' => $input['physical_address_three'],
                'physical_city' => $input['physical_city'],
                'physical_postal_code' => $input['physical_postal_code'],
                'postal_address_one' => $input['postal_address_one'],
                'postal_address_two' => $input['postal_address_two'],
                'postal_address_three' => $input['postal_address_three'],
                'postal_city' => $input['postal_city'],
                'postal_code' => $input['postal_code'],
                'virtual_or_physical' => $input['virtual_or_physical'],
                'pricinct_telephone_code' => $input['pricinct_telephone_code'],
                'pricinct_keys' => $input['pricinct_keys'],
                'pricinct_office_position' => $input['pricinct_office_position'],
                'pricinct_printing_code' => $input['pricinct_printing_code'],
                'pricinct_warehouse_position' => $input['pricinct_warehouse_position'],
                'rent_turnover' => $input['rent_turnover'],
                'rent_date' => $input['rent_date'],
                'rent_amount' => $input['rent_amount'],
                'status' => $input['status'],
                'alumni_date' => $input['alumni_date'],
                'exit_date' => $input['exit_date'],
                'resigned_date' => $input['resigned_date'],
                'mentor' => $input['mentor'],
                'advisor' => $input['advisor'],
                'venture_funding' => $input['venture_funding'],
                'date_awarded' => $input['date_awarded'],
                'fund_value' => $input['fund_value'],
                'date_closedout' => $input['date_closedout'],
                'venture_type' => $input['venture_type']]);

            $ownership = VentureOwnership::create(['venture_id' => $venture->id,
                'ownership' => $input['ownership'],
                'registration_number' => $input['registration_number'],
                'BBBEE_level' => $input['BBBEE_level']]);


            $create_venture_upload = VentureUpload::create(['venture_id' => $venture->id]);

            if ($request->has('vat_registration_number_url')) {
                if (isset($create_venture_upload)) {
                    $create_venture_upload->update(['vat_registration_number_url' => $input['vat_registration_number_url']]);
                }
            }

            if ($request->hasFile('client_contract')) {
                $client_contract_path = $request->file('client_contract')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_client_contract = VentureClientContract::create(['client_contract_url' => $client_contract_path,
                        'date_of_signature' => $input['date_of_signature'], 'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('tax_clearance_url')) {
                $tax_clearance_path = $request->file('tax_clearance_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_tax_clearance = VentureTaxClearance::create(['tax_clearance_url' => $tax_clearance_path,
                        'date_of_tax_clearance' => $input['date_of_clearance_tax'], 'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('bbbee_certificate_url')) {
                $bbbee_certificate_path = $request->file('bbbee_certificate_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_bbbee_certificate = VentureBbbeeCertificate::create(['bbbee_certificate_url' => $bbbee_certificate_path,
                        'date_of_bbbee_certificate' => $input['date_of_bbbee'], 'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('ckDocument_url')) {
                $ck_document_path = $request->file('ckDocument_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_ck_documents = VentureCkDocument::create(['ck_documents_url' => $ck_document_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('management_account_url')) {
                $management_account_path = $request->file('management_account_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_management_account = VentureManagementAccount::create(['management_account_url' => $management_account_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('dormant_affidavit_url')) {
                $dormant_affidavit_path = $request->file('dormant_affidavit_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_dormant_affidavit = VentureDormantAffidavit::create(['dormant_affidavit_url' => $dormant_affidavit_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Venture details has been saved, please update Media Tab.', 'venture'
            => $venture, 'venture_upload' => $create_venture_upload]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function editIncubatee($id){
        $user = User::find($id);
        $user->load('incubatee');
        $incubatee =$user->incubatee;
        $current_user = $incubatee->user;
        $incubatee_ownership = $incubatee->ownership;
        $incubatee_uploads = $incubatee->uploads;
        $current_venture = $incubatee->venture;

        if (Auth::user()->load('roles')->roles[0]->name == 'app-admin') {
            return view('users.venture.venture-edit-incubatee', compact('incubatee_ownership', 'incubatee_uploads', 'user','incubatee',
                'current_user','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'administrator') {
            return view('users.administrators.incubatees.venture-edit-incubatee', compact('incubatee_ownership', 'incubatee_uploads', 'user','incubatee',
            'current_user','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.incubatee.edit', compact('incubatee_ownership', 'incubatee_uploads', 'user','incubatee',
                'current_user','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.incubatees.venture-edit-incubatee', compact('incubatee_ownership', 'incubatee_uploads', 'user','incubatee',
                'current_user','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.incubatees.venture-edit-incubatee', compact('incubatee_ownership', 'incubatee_uploads', 'user','incubatee',
                'current_user','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.incubatees.edit', compact('incubatee_ownership', 'incubatee_uploads', 'user','incubatee',
                'current_user','current_venture'));
        }
    }

    public function updateVenturePersonal(Request $request, User $user)
    {
        $this->validate($request, [
            'short_bio' => 'max:2000'
        ]);

        DB::beginTransaction();
        $input = $request->all();
        try {
            $current_user = $user->load('incubatee');
            $current_incubatee = Incubatee::with('user')->where('user_id', $current_user->id)->get()->first();

            $current_user->update(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'], 'email' => $input['email'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'dob' => $input['dob'], 'age' => $input['age']]);


            $current_incubatee->update(['nmu_graduate' => $input['nmu_graduate'],
                'disabled' => $input['disabled'],
                'home_language' => $input['home_language'],
                'residence' => $input['residence'],
                'short_bio' => $input['short_bio'],
                'personal_facebook_url' => $input['personal_facebook_url'],
                'personal_linkedIn_url' => $input['personal_linkedIn_url'],
                'personal_twitter_url' => $input['personal_twitter_url'],
                'personal_instagram_url' => $input['personal_instagram_url']]);


            DB::commit();
            $current_user->fresh()->load('incubatee');
            return response()->json(['message' => 'Incubatee Details has been successfully updated yabo.']);
        } catch (Exception $e) {
            return response()->json(['message' => 'Something went wrong!']);
        }
    }

    public function updateVentureIncubateeMedia(Request $request, User $user)
    {
        try {
            DB::beginTransaction();

            $user->load('incubatee');
            $current_incubatee = $user->incubatee;
            $current_incubatee->load('uploads');
            $current_incubatee_uploads = $current_incubatee->uploads;

            if (isset($current_incubatee_uploads)) {
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $current_incubatee_uploads->update(['profile_picture_url' => $profile_picture_path]);
                }

                if ($request->hasFile('id_document')) {
                    $id_document_path = $request->file('id_document')->store('incubatee_uploads');
                    $current_incubatee_uploads->update(['id_document' => $id_document_path]);
                }
            } else {
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $create_incubatee_uploads = IncubateeUpload::create(['profile_picture_url' => $profile_picture_path,
                        'incubatee_id' => $current_incubatee->id]);

                    if ($request->hasFile('id_document')) {
                        $id_document_path = $request->file('id_document')->store('incubatee_uploads');
                        $create_incubatee_uploads->update(['id_document' => $id_document_path]);
                    }

                } else if ($request->hasFile('id_document')){
                    $id_document_path = $request->file('id_document')->store('incubatee_uploads');
                    $create_incubatee_uploads = IncubateeUpload::create(['id_document' => $id_document_path,
                        'incubatee_id' => $current_incubatee->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_incubatee_uploads->update(['profile_picture_url' => $profile_picture_path]);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Incubatee Media has been successfully updated.']);

        } catch (\Exception $e) {
            return response()->json(['message' => 'Media could not be updated at the moment ' . $e->getMessage()], 400);

        }
    }

    public function storeVentureMedia(Request $request, Venture $venture)
    {
        $input = $request->all();
        $venture->load('ventureUploads');
        $request->validate([
            'venture_profile_information' => 'max:16777215',
            'elevator_pitch' => 'max:16777215'
        ]);

        try {
            DB::beginTransaction();

            if ($request->has('venture_profile_information')) {
                $venture->update(['venture_profile_information' => $input['venture_profile_information'],
                    'venture_email' => $input['venture_email'],
                    'company_website' => $input['company_website'],
                    'business_facebook_url' => $input['business_facebook_url'],
                    'business_twitter_url' => $input['business_twitter_url'],
                    'business_linkedin_url' => $input['business_linkedin_url'],
                    'business_instagram_url' => $input['business_instagram_url'],
                    'elevator_pitch' => $input['elevator_pitch']]);
            }

            //See if the Venture Upload relationship has been set before
            if(isset($venture->ventureUploads)){
                $venture_upload = $venture->ventureUploads;
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $venture_upload->profile_picture_url = $profile_picture_path;
                }

                if ($request->hasFile('company_logo_url')) {
                    $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                    $venture_upload->company_logo_url = $company_logo_path;
                }

                if ($request->hasFile('video_shot_url')) {
                    $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                    $venture_upload->video_shot_url = $video_shot_path;
                }

                if ($request->hasFile('infographic_url')) {
                    $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                    $venture_upload->infographic_url = $infographic_path;
                }

                if ($request->hasFile('crowdfunding_url')) {
                    $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                    $venture_upload->crowdfunding_url = $crowdfunding_path;
                }

                if ($request->hasFile('product_shot_url')) {
                    $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                    $venture_upload->product_shot_url = $product_shot_path;
                }
            } else {
                //If Venture Upload has not been set. Create it based on file coming in.
                //Then update the same instance if there are any other files incoming.
                //There is a LOT of if statements here to check different file types and to update the same
                //instance of Venture Uploads that we create.
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['profile_picture_url' => $profile_picture_path, 'venture_id'
                    => $venture->id]);

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->company_logo_url = $company_logo_path;
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->video_shot_url = $video_shot_path;
                    }

                    if ($request->hasFile('infographic_url')) {
                        $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                        $create_venture_upload->infographic_url = $infographic_path;
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->crowdfunding_url = $crowdfunding_path;
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->product_shot_url = $product_shot_path;
                    }
                } else if($request->hasFile('company_logo_url')){
                    $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['company_logo_url' => $company_logo_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->profile_picture_url = $profile_picture_path;
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->video_shot_url = $video_shot_path;
                    }

                    if ($request->hasFile('infographic_url')) {
                        $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                        $create_venture_upload->infographic_url = $infographic_path;
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->crowdfunding_url = $crowdfunding_path;
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->product_shot_url = $product_shot_path;
                    }
                } else if ($request->hasFile('video_shot_url')){
                    $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['video_shot_url' => $video_shot_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->profile_picture_url = $profile_picture_path;
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->company_logo_url = $company_logo_path;
                    }

                    if ($request->hasFile('infographic_url')) {
                        $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                        $create_venture_upload->infographic_url = $infographic_path;
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->crowdfunding_url = $crowdfunding_path;
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->product_shot_url = $product_shot_path;
                    }
                } else if ($request->hasFile('infographic_url')){
                    $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['infographic_url' => $infographic_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->profile_picture_url = $profile_picture_path;
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->company_logo_url = $company_logo_path;
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->video_shot_url = $video_shot_path;
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->crowdfunding_url = $crowdfunding_path;
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->product_shot_url = $product_shot_path;
                    }
                } else if ($request->hasFile('crowdfunding_url')){
                    $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['crowdfunding_url' => $crowdfunding_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->profile_picture_url = $profile_picture_path;
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->company_logo_url = $company_logo_path;
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->video_shot_url = $video_shot_path;
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->crowdfunding_url = $crowdfunding_path;
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->product_shot_url = $product_shot_path;
                    }
                } else if ($request->hasFile('product_shot_url')){
                    $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['product_shot_url' => $product_shot_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->profile_picture_url = $profile_picture_path;
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->company_logo_url = $company_logo_path;
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->video_shot_url = $video_shot_path;
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->crowdfunding_url = $crowdfunding_path;
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->product_shot_url = $product_shot_path;
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Venure Media Media has been saved.', 'venture' => $venture]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured: ' . $e->getMessage()]);
        }
    }

    public function editVenture($slug){
        $venture = Venture::where('slug',$slug)->first();
        $venture->load('ventureOwnership', 'ventureUploads', 'incubatee', 'ventureEmployee');
        $venture_ownership = $venture->ventureOwnership;
        $venture_employee = $venture->ventureEmployee;
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('venture_video_shots', 'venture_product_shots', 'venture_client_contract',
            'venture_tax_clearances', 'venture_bbbee_certificates', 'venture_ck_documents',
            'venture_management_accounts', 'venture_dormant_affidavits','venture_spreadsheet',
            'incubatee_venture_spreadsheet','venture_infographic');
        $ventureStatus = VentureStatus::all();


        if (Auth::user()->load('roles')->roles[0]->name == 'app-admin') {
            return view('users.venture.edit-venture', compact('venture', 'venture_upload', 'venture_ownership', 'venture_employee','ventureStatus'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.ventures.edit-venture', compact('venture', 'venture_upload', 'venture_ownership', 'venture_employee','ventureStatus'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.edit-venture', compact('venture', 'venture_upload', 'venture_ownership', 'venture_employee','ventureStatus'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture.edit-venture', compact('venture', 'venture_upload', 'venture_ownership','ventureStatus'));
        }elseif (Auth::user()->load('roles')->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.edit-venture', compact('venture', 'venture_upload', 'venture_ownership','ventureStatus'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateBusiness(Request $request, Venture $venture){
        $request->validate([
            'client_contract' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'tax_clearance_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'bbbee_certificate_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'ck_documents_url' => '|mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'management_account_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'dormant_affidavit_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'vat_registration_number_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
        ]);

        try {
            DB::beginTransaction();
            $input = $request->all();
            $venture->load('ventureOwnership', 'ventureUploads');


            if ($request->has('company_name')) {
                $venture->update(['hub' => $input['hub'],
                    'smart_city_tags' => $input['smart_city_tags'],
                    'business_type' => $input['business_type'],
                    'company_name' => $input['company_name'],
                    'slug' => Str::slug($input['company_name'], '-'),
                    'stage' => $input['stage'],
                    'cohort' => $input['cohort'],
                    'contact_number' => $input['contact_number'],
                    'physical_address_one' => $input['physical_address_one'],
                    'physical_address_two' => $input['physical_address_two'],
                    'physical_address_three' => $input['physical_address_three'],
                    'physical_city' => $input['physical_city'],
                    'physical_postal_code' => $input['physical_postal_code'],
                    'postal_address_one' => $input['postal_address_one'],
                    'postal_address_two' => $input['postal_address_two'],
                    'postal_address_three' => $input['postal_address_three'],
                    'postal_city' => $input['postal_city'],
                    'postal_code' => $input['postal_code'],
                    'virtual_or_physical' => $input['virtual_or_physical'],
                    'pricinct_telephone_code' => $input['pricinct_telephone_code'],
                    'pricinct_keys' => $input['pricinct_keys'],
                    'pricinct_office_position' => $input['pricinct_office_position'],
                    'pricinct_printing_code' => $input['pricinct_printing_code'],
                    'pricinct_warehouse_position' => $input['pricinct_warehouse_position'],
                    'rent_turnover' => $input['rent_turnover'],
                    'rent_date' => $input['rent_date'],
                    'rent_amount' => $input['rent_amount'],
                    'status' => $input['status'],
                    'reason_for_leaving' => $input['reason_for_leaving'],
                    'alumni_date' => $input['alumni_date'],
                    'exit_date' => $input['exit_date'],
                    'resigned_date' => $input['resigned_date'],
                    'mentor' => $input['mentor'],
                    'advisor' => $input['advisor'],
                    'venture_funding' => $input['venture_funding'],
                    'date_awarded' => $input['date_awarded'],
                    'fund_value' => $input['fund_value'],
                    'date_closedout' => $input['date_closedout'],
                    'venture_type' => $input['venture_type'],
                    'sustainable_development_goals' => $input['sustainable_development_goals']]);
                $venture->save();

            }

            /*Ownership*/
            if(isset($venture->ventureOwnership)){
                $venture_ownership = $venture->ventureOwnership;
                if ($request->has('ownership')) {
                    $venture_ownership->update(['ownership' => $input['ownership']]);
                }

                if ($request->has('registration_number')) {
                    $venture_ownership->update(['registration_number' => $input['registration_number']]);
                }

                if ($request->has('BBBEE_level')) {
                    $venture_ownership->update(['BBBEE_level' => $input['BBBEE_level']]);
                }
            } else {
                if ($request->has('ownership')) {
                    $create_venture_ownership = VentureOwnership::create(['ownership' => $input['ownership'],
                        'venture_id' => $venture->id]);

                    if ($request->has('registration_number')) {
                        $create_venture_ownership->update(['registration_number' => $input['registration_number']]);
                    }

                    if ($request->has('BBBEE_level')) {
                        $create_venture_ownership->update(['BBBEE_level' => $input['BBBEE_level']]);
                    }
                } else if ($request->has('registration_number')){
                    $create_venture_ownership = VentureOwnership::create(['registration_number' => $input['registration_number'],
                        'venture_id' => $venture->id]);

                    if ($request->has('ownership')) {
                        $create_venture_ownership->update(['ownership' => $input['ownership']]);
                    }

                    if ($request->has('BBBEE_level')) {
                        $create_venture_ownership->update(['BBBEE_level' => $input['BBBEE_level']]);
                    }
                } else if ($request->has('BBBEE_level')){
                    $create_venture_ownership = VentureOwnership::create(['BBBEE_level' => $input['BBBEE_level'],
                        'venture_id' => $venture->id]);

                    if ($request->has('ownership')) {
                        $create_venture_ownership->update(['ownership' => $input['ownership']]);
                    }

                    if ($request->has('registration_number')) {
                        $create_venture_ownership->update(['registration_number' => $input['registration_number']]);
                    }
                }
            }


            /*uploads*/
            if(isset($venture->ventureUploads)){
                $venture_upload = $venture->ventureUploads;

                //Check the venture uploads' client contracts
                if ($request->hasFile('client_contract')) {
                    $client_contract_path = $request->file('client_contract')->store('incubatee_uploads');
                    $date_of_signature = $input['date_of_signature'];
                    $create_venture_client_contract = VentureClientContract::create(['client_contract_url' => $client_contract_path,
                        'date_of_signature' => $date_of_signature, 'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' tax clearances
                if ($request->hasFile('tax_clearance_url')) {
                    $tax_clearance_path = $request->file('tax_clearance_url')->store('incubatee_uploads');
                    $date_of_clearance_tax = $input['date_of_clearance_tax'];
                    $create_venture_tax_clearance = VentureTaxClearance::create(['tax_clearance_url' => $tax_clearance_path,
                        'date_of_tax_clearance' => $date_of_clearance_tax, 'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' bbbee certificates
                if ($request->hasFile('bbbee_certificate_url')) {
                    $bbbee_certificate_path = $request->file('bbbee_certificate_url')->store('incubatee_uploads');
                    $date_of_bbbee_certificate = $input['date_of_bbbee'];
                    $create_venture_bbbee_certificate = VentureBbbeeCertificate::create(['bbbee_certificate_url' => $bbbee_certificate_path,
                        'date_of_bbbee_certificate' => $date_of_bbbee_certificate, 'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' ck documents
                if ($request->hasFile('ck_documents_url')) {
                    $ck_document_path = $request->file('ck_documents_url')->store('incubatee_uploads');
                    $create_venture_ck_document = VentureCkDocument::create(['ck_documents_url' => $ck_document_path,
                        'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' management account
                if ($request->hasFile('management_account_url')) {
                    $management_account_path = $request->file('management_account_url')->store('incubatee_uploads');
                    $create_venture_management_account = VentureManagementAccount::create(['management_account_url' => $management_account_path,
                        'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' dormant affidavit
                if ($request->hasFile('dormant_affidavit_url')) {
                    $dormant_affidavi_path = $request->file('dormant_affidavit_url')->store('incubatee_uploads');
                    $create_venture_dormant_affidavit = VentureDormantAffidavit::create(['dormant_affidavit_url' => $dormant_affidavi_path,
                        'venture_upload_id' => $venture_upload->id]);
                }

                //Vat registration number
                if ($request->has('vat_registration_number_url')) {
                    $venture_upload->update(['vat_registration_number_url' => $input['vat_registration_number_url']]);
                }
            } else {
                $create_venture_upload = VentureUpload::create(['venture_id' => $venture->id]);

                //Check the venture uploads' client contracts
                if ($request->hasFile('client_contract')) {
                    $client_contract_path = $request->file('client_contract')->store('incubatee_uploads');
                    $date_of_signature = $input['date_of_signature'];
                    $create_venture_client_contract = VentureClientContract::create(['client_contract_url' => $client_contract_path,
                        'date_of_signature' => $date_of_signature, 'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' tax clearances
                if ($request->hasFile('tax_clearance_url')) {
                    $tax_clearance_path = $request->file('tax_clearance_url')->store('incubatee_uploads');
                    $date_of_clearance_tax = $input['date_of_clearance_tax'];
                    $create_venture_tax_clearance = VentureTaxClearance::create(['tax_clearance_url' => $tax_clearance_path,
                        'date_of_tax_clearance' => $date_of_clearance_tax, 'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' bbbee certificates
                if ($request->hasFile('bbbee_certificate_url')) {
                    $bbbee_certificate_path = $request->file('bbbee_certificate_url')->store('incubatee_uploads');
                    $date_of_bbbee_certificate = $input['date_of_bbbee'];
                    $create_venture_bbbee_certificate = VentureBbbeeCertificate::create(['bbbee_certificate_url' => $bbbee_certificate_path,
                        'date_of_bbbee_certificate' => $date_of_bbbee_certificate, 'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' ck documents
                if ($request->hasFile('ck_documents_url')) {
                    $ck_document_path = $request->file('ck_documents_url')->store('incubatee_uploads');
                    $create_venture_ck_document = VentureCkDocument::create(['ck_documents_url' => $ck_document_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' management account
                if ($request->hasFile('management_account_url')) {
                    $management_account_path = $request->file('management_account_url')->store('incubatee_uploads');
                    $create_venture_management_account = VentureManagementAccount::create(['management_account_url' => $management_account_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' dormant affidavit
                if ($request->hasFile('dormant_affidavit_url')) {
                    $dormant_affidavi_path = $request->file('dormant_affidavit_url')->store('incubatee_uploads');
                    $create_venture_dormant_affidavit = VentureDormantAffidavit::create(['dormant_affidavit_url' => $dormant_affidavi_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }

                //Vat registration number
                if ($request->has('vat_registration_number_url')) {
                    $create_venture_upload->update(['vat_registration_number_url' => $input['vat_registration_number_url']]);
                }
            }

            DB::commit();

            return response()->json(['message' => 'Venture Business has been successfully updated.']);

        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function updateMedia(Request $request, Venture $venture){
        $venture->load('ventureOwnership', 'ventureUploads');
        $request->validate([
            'venture_profile_information' => 'max:16777215',
            'elevator_pitch' => 'max:16777215',
            'profile_picture_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'company_logo_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'corporate_id_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'infographic_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'crowdfunding_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'product_shot_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'market_research_upload' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'project_plan_upload' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
            'business_plan' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
        ]);

        try {
            DB::beginTransaction();


            $input = $request->all();
            if ($request->has('venture_profile_information')) {
                $venture->update(['venture_profile_information' => $input['venture_profile_information'],
                    'venture_email' => $input['venture_email'],
                    'company_website' => $input['company_website'],
                    'business_facebook_url' => $input['business_facebook_url'],
                    'business_twitter_url' => $input['business_twitter_url'],
                    'business_linkedin_url' => $input['business_linkedin_url'],
                    'business_instagram_url' => $input['business_instagram_url'],
                    'elevator_pitch' => $input['elevator_pitch']]);
            }
            if ($request->hasFile('gap_analysis')) {
                $gapAnalysis = $request->file('gap_analysis')->store('incubatee_uploads');
                $venture->update(['gap_analysis' => $gapAnalysis]);
            }


            //See if the Venture Upload relationship has been set before
            if(isset($venture->ventureUploads)){
                $venture_upload = $venture->ventureUploads;
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                }

                if ($request->hasFile('company_logo_url')) {
                    $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                    $venture_upload->update(['company_logo_url' => $company_logo_path]);
                }
                if ($request->hasFile('corporate_id_url')) {
                    $corporate_id__path = $request->file('corporate_id_url')->store('incubatee_uploads');
                    $venture_upload->update(['corporate_id_url' => $corporate_id__path]);
                }
                $venture_upload->update(['stage_three_to_four_video'=>$input['stage_three_to_four_video']]);
                $venture_upload->update(['stage_four_to_five_video'=>$input['stage_four_to_five_video']]);
                $venture_upload->update(['video_shot_url'=>$input['video_shot_url']]);
                $venture_upload->update(['stage_two_to_three_video_url'=>$input['stage_two_to_three_video_url']]);
                $venture_upload->update(['stage3_date_of_signature'=>$input['stage3_date_of_signature']]);
                $venture_upload->update(['stage4_date_of_signature'=>$input['stage4_date_of_signature']]);
                $venture_upload->update(['stage5_date_of_signature'=>$input['stage5_date_of_signature']]);
                $venture_upload->update(['stage6_date_of_signature'=>$input['stage6_date_of_signature']]);
                $venture_upload->update(['product_video_url'=>$input['product_video_url']]);

                if ($request->hasFile('stage3_client_contract')) {
                    $stage3_contract = $request->file('stage3_client_contract')->store('incubatee_uploads');
                    $venture_upload->update(['stage3_client_contract' => $stage3_contract]);
                }

                if ($request->hasFile('stage4_client_contract')) {
                    $stage4_contract = $request->file('stage4_client_contract')->store('incubatee_uploads');
                    $venture_upload->update(['stage4_client_contract' => $stage4_contract]);
                }

                if ($request->hasFile('stage5_client_contract')) {
                    $stage5_contract = $request->file('stage5_client_contract')->store('incubatee_uploads');
                    $venture_upload->update(['stage5_client_contract' => $stage5_contract]);
                }

                if ($request->hasFile('stage6_client_contract')) {
                    $stage6_contract = $request->file('stage6_client_contract')->store('incubatee_uploads');
                    $venture_upload->update(['stage6_client_contract' => $stage6_contract]);
                }

                if ($request->hasFile('infographic_url')) {
                    $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                    $venture_upload->update(['infographic_url' => $infographic_path]);
                }

                if ($request->hasFile('crowdfunding_url')) {
                    $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                    $venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                }

                if ($request->hasFile('product_shot_url')) {
                    $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                    $venture_upload->update(['product_shot_url' => $product_shot_path]);
                }

                if ($request->hasFile('market_research_upload')) {
                    $market_research_path = $request->file('market_research_upload')->store('incubatee_uploads');
                    $venture_upload->update(['market_research_upload' => $market_research_path]);
                }

                if ($request->hasFile('project_plan_upload')) {
                    $project_plan_path = $request->file('project_plan_upload')->store('incubatee_uploads');
                    $venture_upload->update(['project_plan_upload' => $project_plan_path]);
                }

                if ($request->hasFile('business_plan')) {
                    $business_plan_path = $request->file('business_plan')->store('incubatee_uploads');
                    $venture_upload->update(['business_plan' => $business_plan_path]);
                }

            } else {
                //If Venture Upload has not been set. Create it based on file coming in.
                //Then update the same instance if there are any other files incoming.
                //There is a LOT of if statements here to check different file types and to update the same
                //instance of Venture Uploads that we create.
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['profile_picture_url' => $profile_picture_path, 'venture_id'
                    => $venture->id]);

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                        $create_venture_upload->update(['video_title' => $input['video_title']]);
                    }

                    if ($request->hasFile('infographic_url')) {
                        $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['infographic_url' => $infographic_path]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if($request->hasFile('company_logo_url')){
                    $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['company_logo_url' => $company_logo_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                        $create_venture_upload->update(['video_title' => $input['video_title']]);
                    }

                    if ($request->hasFile('infographic_url')) {
                        $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['infographic_url' => $infographic_path]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if ($request->hasFile('video_shot_url')){
                    $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['video_shot_url' => $video_shot_path,
                        'venture_id' => $venture->id, 'video_title' => $input['video_title']]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('infographic_url')) {
                        $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['infographic_url' => $infographic_path]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if ($request->hasFile('infographic_url')){
                    $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['infographic_url' => $infographic_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                        $create_venture_upload->update(['video_title' => $input['video_title']]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if ($request->hasFile('crowdfunding_url')){
                    $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['crowdfunding_url' => $crowdfunding_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                        $create_venture_upload->update(['video_title' => $input['video_title']]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if ($request->hasFile('product_shot_url')){
                    $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['product_shot_url' => $product_shot_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                        $create_venture_upload->update(['video_title' => $input['video_title']]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Incubatee Media has been successfully updated.']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Media could not be updated at the moment ' . $e->getMessage()], 400);

        }
    }

    public function destroy(User $incubatee){
        DB::beginTransaction();
        try {
            $user = $incubatee->load('user');
            $incubatee_uploads = $incubatee->load('uploads');
            $incubatee_ownership = $incubatee->load('ownership');

            if (isset($incubatee_ownership)) {
                $incubatee_ownership->delete();
            }

            if (isset($incubatee_uploads)) {
                $incubatee_uploads->delete();
            }

            if (isset($incubatee)) {
                $incubatee->delete();
            }

            if (isset($user)) {
                $user->delete();
            }

            DB::commit();
            return redirect('venture');

        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function destroyVenture($id)
    {
        $venture = Venture::find($id);
        $venture->delete();

        return view('users.venture.index');
    }

    public function destroyIncubatee(Incubatee $incubatee, $id)
    {
        $incubatee_user = $incubatee->user;


        //
        DB::beginTransaction();
        try {
            if (isset($incubatee)) {
                $incubatee->delete();
                /*} elseif (isset($user)) {
                    $user->delete();*/
            }
            DB::commit();
            return redirect('incubatees');

        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function downloadVentureDocument($id){
        $client_contract = VentureClientContract::find($id);
        $bbbee_certificates = VentureBbbeeCertificate::find($id);
        $ck_documents = VentureCkDocument::find($id);
        $dormant_affidavit = VentureDormantAffidavit::find($id);
        $management_account = VentureManagementAccount::find($id);
        $tax_clearance = VentureTaxClearance::find($id);
        $venture_spreadsheet = VentureSpreadsheetUpload::find($id);
        $venture_incubatee_spreadsheet = IncubateeVentureSpreadsheetUpload::find($id);
        $venture_infographic = VentureInfographic::find($id);

        if(isset($client_contract)){
            return Storage::download($client_contract->client_contract_url);
        } else if (isset($bbbee_certificates)){
            return Storage::download($bbbee_certificates->bbbee_certificate_url);
        } else if (isset($ck_documents)){
            return Storage::download($ck_documents->ck_document_url);
        } else if (isset($dormant_affidavit)){
            return Storage::download($dormant_affidavit->dormant_affidavit_url);
        } else if(isset($management_account)){
            return Storage::download($management_account->management_account_url);
        } else if(isset($tax_clearance)){
            return Storage::download($tax_clearance->tax_clearance_url);
        }  else if(isset($venture_spreadsheet)){
            return Storage::download($venture_spreadsheet->spreadsheet_upload);
        } else if(isset($venture_incubatee_spreadsheet)){
            return Storage::download($venture_incubatee_spreadsheet->spreadsheet_upload);
        }else if(isset($venture_infographic)){
            return Storage::download($venture_infographic->infographic_url);
        }else {
            return response("No file!");
        }
    }

    public function deleteVentureDocument($id){
        $client_contract = VentureClientContract::find($id);
        $bbbee_certificates = VentureBbbeeCertificate::find($id);
        $ck_documents = VentureCkDocument::find($id);
        $dormant_affidavit = VentureDormantAffidavit::find($id);
        $management_account = VentureManagementAccount::find($id);
        $tax_clearance = VentureTaxClearance::find($id);
        $venture_infographic = VentureInfographic::find($id);

        try{
            if(isset($client_contract)){
                $client_contract->forceDelete();
                return response()->json(['message' => 'success'], 200);
            } else if (isset($bbbee_certificates)){
                $bbbee_certificates->forceDelete();
                return response()->json(['message' => 'success'], 200);
            } else if (isset($ck_documents)){
                $ck_documents->forceDelete();
                return response()->json(['message' => 'success'], 200);
            } else if (isset($dormant_affidavit)){
                $dormant_affidavit->forceDelete();
                return response()->json(['message' => 'success'], 200);
            } else if(isset($management_account)){
                $management_account->forceDelete();
                return response()->json(['message' => 'success'], 200);
            } else if(isset($tax_clearance)){
                return $tax_clearance->forceDelete();
            } else if(isset($venture_infographic)){
                return $venture_infographic->forceDelete();
            } else {
                return response("No file!");
            }
        } catch (\Exception $e){
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    //Delete venture spreadsheet
    public function deleteVentureSpreadsheet($id){
        $venture_spreadsheet = VentureSpreadsheetUpload::find($id);

        try{
            if(isset($venture_spreadsheet)){
                $venture_spreadsheet->forceDelete();
                return response()->json(['message' => 'Spreadsheet Delete Successfully'], 200);
            } else {
                return response("No file!");
            }
        } catch (\Exception $e){
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    public function uploadVentureClientContract(Request $request, Venture $venture)
    {
        $input = $request->all();
        $venture->load('ventureUploads');
        $venture_upload = $venture->ventureUploads;
        $request->validate([
            'client_contract_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
        ]);


        try{
            DB::beginTransaction();

            if ($request->hasFile('client_contract_url')) {
                $client_contract_path = $request->file('client_contract_url')->store('incubatee_uploads');
                $create_venture_client_contract = VentureClientContract::create(['client_contract_url' => $client_contract_path,
                    'contract_expiry_date' => $input['contract_expiry_date'],
                    'date_of_signature' => $input['date_of_signature'], 'venture_upload_id' => $venture_upload->id,
                    ]);
            }

            DB::commit();
            return response()->json(['message' => 'Uploaded client contract.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    /*m + e spreadsheet upload on incubatee side*/
    public function incubateeuploadVentureSpreadsheet(Request $request, Venture $venture)
    {
        $input = $request->all();
        $venture->load('ventureUploads');
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('venture_spreadsheet','incubatee_venture_spreadsheet');
        $date = Carbon::today();
        $date2 = date('Y-m-d', strtotime('+1 month'));
        $current_month = Carbon::now();
        $current_month->month;

        try{
            DB::beginTransaction();

            if ($input['month'] > $current_month) {
                echo 'greater than';
            }else

            if ($input['month'] == '2021-10') {
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['oct_spreadsheet_uploaded' => true,'oct_date_submitted'=>$date]);
            }elseif ($input['month'] == '2021-11'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['nov_spreadsheet_uploaded' => true,'nov_date_submitted'=>$date]);
            }elseif ($input['month'] == '2021-12'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['dec_spreadsheet_uploaded' => true,'dec_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-01'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['jan_spreadsheet_uploaded' => true,'jan_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-02'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['feb2022_spreadsheet_uploaded' => true,'feb2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-03'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['march2022_spreadsheet_uploaded' => true,'march2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-04'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['april2022_spreadsheet_uploaded' => true,'april2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-05'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['may2022_spreadsheet_uploaded' => true,'may2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-06'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['june2022_spreadsheet_uploaded' => true,'june2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-07'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['july2022_spreadsheet_uploaded' => true,'july2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-08'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['august2022_spreadsheet_uploaded' => true,'august2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-09'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['sept2022_spreadsheet_uploaded' => true,'sept2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-10'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['oct2022_spreadsheet_uploaded' => true,'oct2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-11'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['nov2022_spreadsheet_uploaded' => true,'nov2022_date_submitted'=>$date]);
            }elseif ($input['month'] == '2022-12'){
                if ($request->hasFile('spreadsheet_upload')) {
                    $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                    $create_venture_client_contract = IncubateeVentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                        'date_submitted' => $date, 'venture_upload_id' => $venture_upload->id,
                        'spreadsheet_uploaded' => true, 'year' => $input['year'], 'month' => $input['month'], 'venture_id' => $venture->id]);
                }
                $venture_upload->venture_spreadsheet()->update(['year' => $date2, 'month' => $date2]);
                $venture->update(['dec2022_spreadsheet_uploaded' => true,'dec2022_date_submitted'=>$date]);
            }

            DB::commit();
            return response()->json(['message' => 'Uploaded venture spreedsheet.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }


   /* spreadsheets spreadsheet upload*/
    public function uploadVentureSpreadsheet(Request $request, Venture $venture)
    {
        $input = $request->all();
        $venture->load('ventureUploads');
        $venture_upload = $venture->ventureUploads;

        try{
            DB::beginTransaction();

            if ($request->hasFile('spreadsheet_upload')) {
                $spreadsheet_path = $request->file('spreadsheet_upload')->store('incubatee_uploads');
                $create_venture_client_contract = VentureSpreadsheetUpload::create(['spreadsheet_upload' => $spreadsheet_path,
                    'month' => $input['month'], 'year' => $input['year'], 'venture_upload_id' => $venture_upload->id]);
            }

            DB::commit();
            return response()->json(['message' => 'Uploaded venture spreedsheet.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    public function uploadVentureTaxClearance(Request $request, Venture $venture){
        $input = $request->all();
        $venture->load('ventureUploads');
        $venture_upload = $venture->ventureUploads;
        $request->validate([
            'tax_clearance_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
        ]);
        try{
            DB::beginTransaction();

            if ($request->hasFile('tax_clearance_url')) {
                $tax_clearance_path = $request->file('tax_clearance_url')->store('incubatee_uploads');
                $create_venture_tax_clearance = VentureTaxClearance::create(['tax_clearance_url' => $tax_clearance_path,
                    'tax_expiry_date' => $input['tax_expiry_date'],
                    'date_of_tax_clearance' => $input['date_of_clearance_tax'],
                   'venture_upload_id' => $venture_upload->id]);
            }

            DB::commit();
            return response()->json(['message' => 'Uploaded tax clearance.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    public function uploadVentureBbbeeCertificate(Request $request, Venture $venture){
        $input = $request->all();
        $venture->load('ventureUploads');
        $venture_upload = $venture->ventureUploads;
        $request->validate([
            'bbbee_certificate_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
        ]);
        try{
            DB::beginTransaction();

            if ($request->hasFile('bbbee_certificate_url')) {
                $bbbee_certificate_path = $request->file('bbbee_certificate_url')->store('incubatee_uploads');
                $create_venture_bbbee_certificate = VentureBbbeeCertificate::create(['bbbee_certificate_url' => $bbbee_certificate_path,
                    'bbbee_expiry_date' => $input['bbbee_expiry_date'],
                    'date_of_bbbee_certificate' => $input['date_of_bbbee'], 'venture_upload_id' => $venture_upload->id]);
            }

            DB::commit();
            return response()->json(['message' => 'Uploaded bbbee certificate.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    public function uploadVentureCkDocuments(Request $request, Venture $venture){
        $input = $request->all();
        $venture->load('ventureUploads');
        $venture_upload = $venture->ventureUploads;
        $request->validate([
            'ckDocument_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
        ]);
        try{
            DB::beginTransaction();

            if ($request->hasFile('ckDocument_url')) {
                $ck_document_path = $request->file('ckDocument_url')->store('incubatee_uploads');
                $create_venture_ck_documents = VentureCkDocument::create(['ck_documents_url' => $ck_document_path,
                    'venture_upload_id' => $venture_upload->id,'ck_expiry_date' => $input['ck_expiry_date']]);
            }

            DB::commit();
            return response()->json(['message' => 'Uploaded ck document.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    public function uploadVentureManagementAccount(Request $request, Venture $venture){
        $input = $request->all();
        $venture->load('ventureUploads');
        $venture_upload = $venture->ventureUploads;

        try{
            DB::beginTransaction();

            if ($request->hasFile('management_account_url')) {
                $management_account_path = $request->file('management_account_url')->store('incubatee_uploads');
                $create_venture_management_account = VentureManagementAccount::create(['management_account_url' => $management_account_path,
                    'venture_upload_id' => $venture_upload->id,
                    'management_account_expiry_date'=>$input['management_account_expiry_date']]);
            }

            DB::commit();
            return response()->json(['message' => 'Uploaded management account.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    //Upload multiple infographic
    public function uploadVentureInfographic(Request $request, Venture $venture){
        $input = $request->all();
        $venture->load('ventureUploads');
        $venture_upload = $venture->ventureUploads;

        try{
            DB::beginTransaction();

            if ($request->hasFile('infographic_url_multiple')) {
                $infographic_url_path = $request->file('infographic_url_multiple')->store('incubatee_uploads');
                $create_venture_management_account = VentureInfographic::create(['infographic_url_multiple' => $infographic_url_path,
                    'venture_upload_id' => $venture_upload->id]);
            }

            DB::commit();
            return response()->json(['message' => 'Uploaded infographic.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    public function uploadVentureDormantAffidavit(Request $request, Venture $venture){
        $input = $request->all();
        $venture->load('ventureUploads');
        $venture_upload = $venture->ventureUploads;
        $request->validate([
            'dormant_affidavit_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048',
        ]);
        try{
            DB::beginTransaction();

            if ($request->hasFile('dormant_affidavit_url')) {
                $dormant_affidavit_path = $request->file('dormant_affidavit_url')->store('incubatee_uploads');
                $create_venture_dormant_affidavit = VentureDormantAffidavit::create(['dormant_affidavit_url' => $dormant_affidavit_path,
                    'venture_upload_id' => $venture_upload->id,'affidavity_expiry_date'=>$input['affidavity_expiry_date']]);
            }

            DB::commit();
            return response()->json(['message' => 'Uploaded dormant affidavit.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }



    /////////////////////////////////////////////////////////////////////////////////////////////////
    /// Venture Panel Interviews

    public function venturePanelInterviewCategoryIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/venture/venture-panel-interview-categories-index');
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/ventures/venture-panel-interview-categories-index');
        } else {
            return response("You are not authorized!!");
        }
    }

    public function getVenturePanelInterviewCategories(){
        $logged_in_user = Auth::user()->load('roles');
        $categories = VenturePanelInterviewCategory::all();
        $categories_array = [];
        foreach ($categories as $category){
            $category->load('venturePanelInterviews');
            $interview_count = 0;
            if (count($category->venturePanelInterviews) > 0){
                $interview_count = count($category->venturePanelInterviews);
            }

            $object = (object)['category_name' => $category->category_name, 'interview_count' => $interview_count,  'id' => $category->id,];
            array_push($categories_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'advisor'
            or $logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($categories_array)->addColumn('action', function ($category) {
                $re = '/edit-venture-panel-interview-category/' . $category->id;
                $del = '/delete-venture-interview-category/' .$category->id;
                return '<a href=' . $re . ' title="Edit user" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete_users(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($logged_in_user->roles[0]->name == 'marketing'){
            return Datatables::of($categories_array)
               ->addColumn('action', function ($applicant) {
                $sh = '/applicant-show/' . $applicant->id;
                return '<a href=' . $sh . ' title="View Applicant"><i class="material-icons">remove_red_eye</i></a>';
            })
                ->make(true);
        } else {
            return response('You are not authorized!');
        }
    }

    //Edit venture panel interview
    public function editVentureInterviewCategory(VenturePanelInterviewCategory $venturePanelInterviewCategory)
    {
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.edit-venture-interview-category', compact('venturePanelInterviewCategory'));
        }

    }
    //Update venture panel interview
    public function updatePanelInterviewCategory(Request $request, VenturePanelInterviewCategory $venturePanelInterviewCategory){
        $input = $request->all();

        try{
            DB::beginTransaction();
            $venturePanelInterviewCategory->update(['category_name' => $input['category_name']]);
            $venturePanelInterviewCategory->save();
            DB::commit();
            return response()->json(['message' => ' category updated.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }
    //Delete venture panel interview
    public function deletePanelInterviewCategory(VenturePanelInterviewCategory $venturePanelInterviewCategory)
    {
        try {
            $venturePanelInterviewCategory->forceDelete();
            return response()->json(['message' => ' Deleted Successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()], 400);
        }
    }

    public function adminCreateVenturePanelInterviewCategory(Request $request){
        $input = $request->all();
        $category_name = $input['category_name'];
        try{
            DB::beginTransaction();

            $create_venture_panel_interview_category = VenturePanelInterviewCategory::create(['category_name' => $category_name]);
            DB::commit();
            return response()->json(['message' => 'Category created.'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    public function adminViewVenturePanelAccessWindow(Venture $venture){
        $venture->load('incubatee', 'venturePanelInterviews');
        $logged_in_user = Auth::user()->load('roles');
        $question_categories = QuestionsCategory::all();
        $venturePanelInterviewCategories = VenturePanelInterviewCategory::all();

        $venture_panel_array = [];

        foreach ($venture->venturePanelInterviews as $venturePanel) {
            $venture_panel_interview_category_id = $venturePanel->venture_panel_interview_category_id;
            $venture_panel_interview_category = VenturePanelInterviewCategory::findOrFail($venture_panel_interview_category_id);
            $object = (object)[
                'id' => $venturePanel->id,
                'panel_selection_date' => isset($venturePanel->panel_selection_date) ? $venturePanel->panel_selection_date : 'No date',
                'panel_selection_time' => isset($venturePanel->panel_selection_time) ? $venturePanel->panel_selection_time : 'No time',
                'category_name' => $venture_panel_interview_category->category_name
            ];
            array_push($venture_panel_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/venture/venture-panel-access-window', compact('venture', 'question_categories',
                'venturePanelInterviewCategories','venture_panel_array'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.venture-panel-access-window', compact('venture', 'question_categories', 'venturePanelInterviewCategories',
                'venture_panel_array'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.venture-panel-access-window', compact('venture', 'question_categories',
                'venturePanelInterviewCategories','venture_panel_array'));
        } else {
            return response("You are not authorized!");
        }
    }

    //EDIT VENTURE PANEL INTERVIEW
    public function adminEditVenturePanelInterview(VenturePanelInterview $venturePanelInterview){
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.update-venture-panel-interview-time', compact('venturePanelInterview'));
        }elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.update-venture-panel-interview-time', compact('venturePanelInterview'));
        }elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.update-venture-panel-interview-time', compact('venturePanelInterview'));
        }
    }


    public function adminUpdatePanelInterviewDetailsVenture(Request $request, VenturePanelInterview $venturePanelInterview){
        $input = $request->all();

        try{
            DB::beginTransaction();
            $venturePanelInterview->update(['panel_selection_date' => $input['panel_selection_date'],
                'panel_selection_time' => $input['panel_selection_time']]);


            DB::commit();
            return response()->json(['message' => 'Updated successfully!']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: '.$e->getMessage()], 400);
        }
    }

    public function adminStoreVenturePanelInterview(Request $request, Venture $venture){
        $input = $request->all();
        $selected_panelists = json_decode($input['selected_panelists']);
        $guest_panelists = json_decode($input['guest_panelists']);
        $panelist_role = Role::where('name', 'panelist')->first();
        $question_category_id = $input['question_category_id'];
        $venture_panel_interview_category_id = $input['venture_panel_interview_category_id'];

        try {
            DB::beginTransaction();

            foreach ($selected_panelists as $selected_panelist) {
                //See if the actual user exists
                $check_user = User::where('email', $selected_panelist)->first();

                //if user does exist
                if (isset($check_user)) {
                    //load relationships to be used
                    $check_user->load('roles');
                    //boolean to see if the user has panelist role
                    $check_role = false;

                    //check if user has role panelist
                    foreach ($check_user->roles as $user_role) {
                        if ($user_role->name == 'panelist') {
                            $check_role = true;
                        }
                    }

                    //if user has role panelist, do nothing, else attach role
                    if ($check_role == false) {
                        $check_user->roles()->attach($panelist_role->id);
                    }

                    //Variable to see if the panelist object with the same user id already exists
                    $check_panelist = $check_user->panelist()->exists();;

                    //Do if statement to see if the panelist with user id exists, else create
                    if ($check_panelist == true) {
                        $cur_panelist = $check_user->panelist;
                        $check_panelist_venture_interviews = $cur_panelist->venturePanelInterviews()->exists();

                        if ($check_panelist_venture_interviews == true) {
                            $cur_panelist_venture_interviews = $cur_panelist->venturePanelInterviews;

                            //boolean to see if panelist has applicant
                            $has_venture = false;

                            //loop through panelist applicants to see if the applicant id matches the current applicant id
                            foreach ($cur_panelist_venture_interviews as $venture_interviews) {
                                if ($venture_interviews->venture_id == $venture->id) {
                                    $has_venture = true;
                                }
                            }

                            //if panelist does not have current applicant, create applicantpanelist object
                            /*if ($has_venture == false) {*/
                                $create_panelist_venture_interview = VenturePanelInterview::create(['venture_id' => $venture->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                                    'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                            /*} else {
                                $get_object = DB::table('venture_panel_interviews')->where('venture_id', $venture->id)
                                    ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                        'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                                        'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                            }*/
                        } else {
                            $create_panelist_venture_interview = VenturePanelInterview::create(['venture_id' => $venture->id, 'panelist_id' => $cur_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                                'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                        }
                    } else {
                        $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                        $create_panelist_venture_interview = VenturePanelInterview::create(['venture_id' => $venture->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                            'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                    }
                } else {
                    $create_user = User::create(['email' => $selected_panelist, 'password' => Hash::make('secret')]);
                    $create_user->roles()->attach($panelist_role->id);
                    $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                    $create_panelist_venture_interview = VenturePanelInterview::create(['venture_id' => $venture->id, 'panelist_id' => $create_panelist->id,
                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                        'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                }
            }

            foreach ($guest_panelists as $guest_panelist) {
                $check_user = User::where('email', $guest_panelist->email)->first();

                //if user does exist
                if (isset($check_user)) {
                    //load relationships to be used
                    $check_user->load('roles');
                    //boolean to see if the user has panelist role
                    $check_role = false;

                    //check if user has role panelist
                    foreach ($check_user->roles as $user_role) {
                        if ($user_role->name == 'panelist') {
                            $check_role = true;
                        }
                    }

                    //if user has role panelist, do nothing, else attach role
                    if ($check_role == false) {
                        $check_user->roles()->attach($panelist_role->id);
                    }

                    //Variable to see if the panelist object with the same user id already exists
                    $check_panelist = $check_user->panelist()->exists();;

                    //Do if statement to see if the panelist with user id exists, else create
                    if ($check_panelist == true) {
                        $cur_panelist = $check_user->panelist;
                        $check_panelist_venture_interviews = $cur_panelist->venturePanelInterviews()->exists();

                        if ($check_panelist_venture_interviews == true) {
                            $cur_panelist_venture_interviews = $cur_panelist->venturePanelInterviews;
                            //boolean to see if panelist has applicant
                            $has_venture = false;

                            //loop through panelist applicants to see if the applicant id matches the current applicant id
                            foreach ($cur_panelist_venture_interviews as $panelist_venture) {
                                if ($panelist_venture->venture_id == $venture->id) {
                                    $has_venture = true;
                                }
                            }

                            //if panelist does not have current applicant, create applicantpanelist object
                            /*if ($has_venture == false) {*/
                                $create_panelist_venture_interview = VenturePanelInterview::create(['venture_id' => $venture->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                                    'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                           /* } else {
                                $get_object = DB::table('venture_panel_interviews')->where('venture_id', $venture->id)
                                    ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                        'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                                        'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                            }*/
                        } else {
                            $create_panelist_venture_interview = VenturePanelInterview::create(['venture_id' => $venture->id, 'panelist_id' => $cur_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                                'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                        }
                    } else {
                        $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                        $create_panelist_venture_interview = VenturePanelInterview::create(['venture_id' => $venture->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                            'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                    }
                } else {
                    $create_user = User::create(['name' => $guest_panelist->name, 'surname' => $guest_panelist->surname,
                        'contact_number' => $guest_panelist->contact_number, 'email' => $guest_panelist->email, 'password' => Hash::make('secret'),
                        'company_name' => $guest_panelist->company, 'position' => $guest_panelist->position, 'title' => $guest_panelist->title,
                        'initials' => $guest_panelist->initials]);
                    $create_user->roles()->attach($panelist_role->id);
                    $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                    $create_panelist_venture_interview = VenturePanelInterview::create(['venture_id' => $venture->id, 'panelist_id' => $create_panelist->id,
                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id,
                        'venture_panel_interview_category_id' => $venture_panel_interview_category_id]);
                }
            }
            DB::commit();
            return response()->json(['message' => 'Successfully created panelists.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

   /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR OCTOBER*/
    public function meVenturesOctober2021Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.oct-2021-spreadsheets-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.oct-2021-spreadsheets-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.oct-2021-spreadsheets-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getOctoberMEVenture2021()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'oct_spreadsheet_uploaded' => $ventures->oct_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'oct_date_submitted' => isset($ventures->oct_date_submitted) ? $ventures->oct_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR NOVEMBER*/
    public function meVenturesNovember2021Index()
    {

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.nov-2021-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.nov-2021-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.nov-2021-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getNovemberMEVenture2021()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'nov_spreadsheet_uploaded' => $ventures->nov_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'nov_date_submitted' => isset($ventures->nov_date_submitted) ? $ventures->nov_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-nov-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View spreadsheet"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR DEC*/
    public function meVenturesDec2021Index()
    {

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.dec-2021-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.dec-2021-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.dec-2021-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getDecMEVenture2021()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'dec_spreadsheet_uploaded' => $ventures->dec_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'dec_date_submitted' => isset($ventures->dec_date_submitted) ? $ventures->dec_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }


        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-dec-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View spreadsheet"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR JAN*/
    public function meVenturesJan2022Index()
    {

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.jan-2022-spreasheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.jan-2022-spreasheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.jan-2022-spreasheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getJanMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'jan_spreadsheet_uploaded' => $ventures->jan_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'jan_date_submitted' => isset($ventures->jan_date_submitted) ? $ventures->jan_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-jan-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View spreadsheet"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

   /* View spreadsheet*/
    public function viewSpreadsheet(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }
    /* View spreadsheet November*/
    public function viewSpreadsheetNov(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-nov',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-nov',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-nov',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }
    /* View spreadsheet December*/
    public function viewSpreadsheetDec(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-dec',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-dec',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-dec',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }
    /* View spreadsheet January*/
    public function viewSpreadsheetJan(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-jan',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-jan',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-jan',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet Feb 2022*/
    public function viewSpreadsheetFeb2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-feb-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-feb-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-feb-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet Feb 2022*/
    public function viewSpreadsheetMarch2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-march-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-march-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-march-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet April 2022*/
    public function viewSpreadsheetApril2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-april-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-april-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-april-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet May 2022*/
    public function viewSpreadsheetMay2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-may-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-may-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-may-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet June 2022*/
    public function viewSpreadsheetJune2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-june-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-june-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-june-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet July 2022*/
    public function viewSpreadsheetJuly2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-july-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-july-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-july-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet August 2022*/
    public function viewSpreadsheetAugust2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-august-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-august-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-august-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet Sep 2022*/
    public function viewSpreadsheetSeptember2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-september-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-september-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-september-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet Oct 2022*/
    public function viewSpreadsheetOctober2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-october-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-october-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-october-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet Nov 2022*/
    public function viewSpreadsheetNovember2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-november-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-november-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-november-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    /* View spreadsheet Dec 2022*/
    public function viewSpreadsheetDec2022(Venture $venture){
        $venture->load( 'ventureUploads')->get();
        $venture_upload = $venture->ventureUploads;
        $venture_upload->load('incubatee_venture_spreadsheet');

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.view-spreadsheet-december-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.view-spreadsheet-december-2022',compact('venture','venture_upload'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.view-spreadsheet-december-2022',compact('venture','venture_upload'));
        } else {
            return response("You are not authorized!");
        }
    }

    //Venture contact log
    public function ventureContactLog(Venture $venture){
        $venture->load('contactLog');
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.venture-contact-log',compact('venture'));
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.venture-contact-log',compact('venture'));
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.venture-contact-log',compact('venture'));
        } else {
            return response("You are not authorized!");
        }
    }
    public function storeApplicantContactLog(Request $request,User $user)
    {
        DB::beginTransaction();
        $input = $request->all();

        $user->load('applicant');
        $applicant = $user->applicant;
        $date = Carbon::today();

        try {
            if ($input['applicant_contact_results'] == 'Email') {
                $contact_log = ApplicantContactLog::create(['comment' => $input['comment'], 'date' => $date,
                    'applicant_contact_results' => $input['applicant_contact_results'],
                    'applicant_id' => $applicant->id]);


                /*$email =$user->email;
                $name = $user->name;
                $data = array(
                    'name' => $name,
                    'email' => $email,
                );
                $email_field = $email;
                Mail::send('emails.applicant-status-emailer', $data, function ($message) use ($email_field) {
                    $message->to($email_field)
                        ->subject('Incomplete Application to Propella Business Incubator');
                    $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                });*/

                DB::commit();
                return response()->json(['message' => 'Log added successfully']);

            }else{
                $contact_log = ApplicantContactLog::create(['comment' => $input['comment'], 'date' => $date,
                    'applicant_contact_results' => $input['applicant_contact_results'],
                    'applicant_id' => $applicant->id]);


                DB::commit();
                return response()->json(['message' => 'Log added successfully']);
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }
    //Venture Contact Log
    public function storeVentureContactLog(Request $request, Venture $venture)
    {
        DB::beginTransaction();
        $input = $request->all();

        $date = Carbon::today();

        try {
            if ($input['venture_contact_results'] == 'Email') {
                $contact_log = VentureContactLog::create(['comment' => $input['comment'], 'date' => $date,
                    'venture_contact_results' => $input['venture_contact_results'],'comment_email'=>$input['comment_email'],
                    'venture_id' => $venture->id]);


               /* $email =$venture->venture_email;
                $name = $venture->company_name;
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'comment_email'=>$input['comment_email']
                );
                $email_field = $email;
                Mail::send('emails.m-e-information-emailer', $data, function ($message) use ($email_field) {
                    $message->to($email_field)
                        ->subject('Incomplete Application to Propella Business Incubator');
                    $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                });*/

                DB::commit();

                return response()->json(['message' => 'Email sent']);

            }else{
                $contact_log = VentureContactLog::create(['comment' => $input['comment'], 'date' => $date,
                    'venture_contact_results' => $input['venture_contact_results'],
                    'venture_id' => $venture->id]);
                DB::commit();
                return response()->json(['message' => 'Log added successfully']);
            }


        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR FEB 2022*/
    public function meVenturesFeb2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.feb2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.feb2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.feb2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getFebMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'feb2022_spreadsheet_uploaded' => $ventures->feb2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'feb2022_date_submitted' => isset($ventures->feb2022_date_submitted) ? $ventures->feb2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-feb-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR MARCH 2022*/
    public function meVenturesMarch2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.march2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.march2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.march2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getMarchMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'march2022_spreadsheet_uploaded' => $ventures->march2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'march2022_date_submitted' => isset($ventures->march2022_date_submitted) ? $ventures->march2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-march-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR APRIL 2022*/
    public function meVenturesApril2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.april2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.april2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.april2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getAprilMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'april2022_spreadsheet_uploaded' => $ventures->april2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'april2022_date_submitted' => isset($ventures->april2022_date_submitted) ? $ventures->april2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-april-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR MAY 2022*/
    public function meVenturesMay2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.may2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.may2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.may2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getMayMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'may2022_spreadsheet_uploaded' => $ventures->may2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'may2022_date_submitted' => isset($ventures->may2022_date_submitted) ? $ventures->may2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-may-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR JUNE 2022*/
    public function meVenturesJune2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.june2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.june2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.june2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getJuneMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'june2022_spreadsheet_uploaded' => $ventures->june2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'june2022_date_submitted' => isset($ventures->june2022_date_submitted) ? $ventures->june2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-june-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR JUNE 2022*/
    public function meVenturesJuly2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.july2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.july2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.july2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getJulyMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'july2022_spreadsheet_uploaded' => $ventures->july2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'july2022_date_submitted' => isset($ventures->july2022_date_submitted) ? $ventures->july2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-july-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR JUNE 2022*/
    public function meVenturesAug2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.july2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.july2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.july2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getAugMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'july2022_spreadsheet_uploaded' => $ventures->july2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'july2022_date_submitted' => isset($ventures->july2022_date_submitted) ? $ventures->july2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-august-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR Sept 2022*/
    public function meVentureSept2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.sept2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.sept2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.sept2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getSeptMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'sept2022_spreadsheet_uploaded' => $ventures->sept2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'sept2022_date_submitted' => isset($ventures->sept2022_date_submitted) ? $ventures->sept2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-september-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR OCT 2022*/
    public function meVentureOct2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.oct2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.oct2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.oct2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getOctMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'oct2022_spreadsheet_uploaded' => $ventures->oct2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'oct2022_date_submitted' => isset($ventures->oct2022_date_submitted) ? $ventures->oct2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-october-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR NOV 2022*/
    public function meVentureNov2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.nov2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.nov2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.nov2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getNovMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'nov2022_spreadsheet_uploaded' => $ventures->nov2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'nov2022_date_submitted' => isset($ventures->nov2022_date_submitted) ? $ventures->nov2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-november-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    /* ACTIVE VENTURES THAT SUBMITTED M + E SPREADSHEET FOR DEC 2022*/
    public function meVentureDec2022Index()
    {
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.spreadsheets.dec2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.spreadsheets.dec2022-spreadsheet-index');
        }  else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.spreadsheets.dec2022-spreadsheet-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getDecMEVenture2022()
    {
        $venture = Venture::with('incubatee_venture_spreadsheet','incubatee')->where('status','Active')->get();
        $inc_ven_spreadsheet_array = [];
        foreach ($venture as $ventures){
            $object = (object)[
                'company_name' => $ventures->company_name,
                'cohort' => isset($ventures->cohort) ? $ventures->cohort : 'No cohort',
                'stage' => isset($ventures->stage) ? $ventures->stage : 'No stage',
                'email' => isset($ventures->venture_email) ? $ventures->venture_email : 'No email',
                'contact_number' => isset($ventures->contact_number) ? $ventures->contact_number : 'No number',
                'dec2022_spreadsheet_uploaded' => $ventures->dec2022_spreadsheet_uploaded == false ? 'No' : 'Yes',
                'id' => $ventures->id,
                'dec2022_date_submitted' => isset($ventures->dec2022_date_submitted) ? $ventures->dec2022_date_submitted : 'Not submitted',
            ];
            array_push($inc_ven_spreadsheet_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator' or $user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor') {
            return Datatables::of($inc_ven_spreadsheet_array)->addColumn('action', function ($venture) {
                $view = '/view-december-2022-spreadsheet/' . $venture->id;
                $log = '/venture-contact-log/' . $venture->id;
                return '<a href=' . $view . ' title="View spreadsheet"><i class="material-icons">remove_red_eye</i></a><a href=' . $log . ' title="View contact log"><i class="material-icons">assignment_returned</i></a>';
            })
                ->make(true);
        }

    }

    //Edit venture contract
    public function editVentureContract(VentureClientContract $venture){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.venture.edit-venture-contract', compact('venture'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.ventures.edit-venture-contract', compact('venture'));
        }else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.ventures.edit-venture-contract', compact('venture'));
        }
    }


    //Update venture contract
    public function updateVentureContract(Request $request, VentureClientContract $venture)
    {
        $input = $request->all();

        try{
            DB::beginTransaction();
            if ($request->hasFile('client_contract_url')) {
                $profile_picture_path = $request->file('client_contract_url')->store('incubatee_uploads');
                $venture->update(['client_contract_url' => $profile_picture_path]);
            }
            $venture->update([
                'date_of_signature' => $input['date_of_signature'],
                'contract_stage' => $input['contract_stage']]);

            DB::commit();
            return response()->json(['message' => 'Updated client contract.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }


    //Show venture incubatee
    public function showVentureIncubateeOnVenturesSide(User $user)
    {
        $user->load('incubatee');
        $incubatee = $user->incubatee;
        $incubatee->with('user', 'events', 'incubateeDeregisters', 'uploads', 'venture_category', 'panelists', 'incubateeContactLog', 'venture');
        $incubatee_uploads = $incubatee->uploads;
        $role = Auth::user()->roles()->first();


        $incubatee_user = $incubatee->user;
        $incubatee_user->load('applicant', 'userQuestionAnswers', 'bootcamper')->get();

        $incubatee_application_question_answers_array = [];


        //APPLICATION
        foreach ($incubatee_user->userQuestionAnswers as $userQuestionAnswer) {
            $question_id = $userQuestionAnswer->question_id;
            $question = Question::find($question_id);
            $answer_text = $userQuestionAnswer->answer_text;
            if (isset($question)) {
                if (isset($question->category->category_name)) {
                    if ($question->category->category_name == 'AA Application form - ICT Bootcamp' or
                        $question->category->category_name == 'AA Application form - Propella Satellite') {
                        $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                            'answer_text' => $answer_text];

                        array_push($incubatee_application_question_answers_array, $object);
                    }
                }
            }
        }

        //EVENTS
        $incubatee_bootcamper_events_array = [];
        //Check if the incubatee has been through the bootcamper journey,
        if(isset($incubatee_user->bootcamper)){
            $incubatee_bootcamper = $incubatee_user->bootcamper;
            //load all bootcamper relationships
            $incubatee_bootcamper->load('ventureCategory', 'events', 'panelists','bootcampercontactLog');

            if(count($incubatee_bootcamper->events) > 0){
                $incubatee_bootcamper_events = $incubatee_bootcamper->events;

                //Loop through bootcamper events and store them in an array,
                //then push that newly created array to a view
                foreach ($incubatee_bootcamper_events as $b_event){
                    $event_id = $b_event->event_id;
                    $event = Event::find($event_id);
                    $object = (object)[
                        'event_title' => isset($event) ? $event->title : 'No title',
                        'accepted' => isset($b_event->accepted) ? 'Yes' : 'No',
                        'date_registered' => $b_event->date_registered,
                        'attended' => isset($b_event->attended) ? 'Yes' : 'No',
                        'declined' => isset($b_event->declined) ? 'Yes' : 'No',
                        'event_date' => $b_event->start];
                    array_push($incubatee_bootcamper_events_array, $object);
                }
            }
        }
        $incubatee_events_array = [];
        //Check if the incubatee has any events
        if(isset($incubatee->events)){
            $incubatee_events = $incubatee->events;
            if(count($incubatee_events) > 0){
                foreach($incubatee_events as $i_event){
                    $event_id = $i_event->event_id;
                    $event = Event::find($event_id);

                    $object = (object)[
                        'event_title' => isset($event->title) ? $event->title : 'No title set',
                        'event_date' => isset($event->start) ? $event->start : 'No date set',
                        'attended'=> $i_event->attended == false ? 'No':'Yes',
                        'date_time_registered' => isset($i_event->date_time_registered) ? $i_event->date_time_registered : 'No date time set',
                        'found_out_via' => isset($i_event->found_out_via) ? $i_event->found_out_via : 'Not set'];
                    array_push($incubatee_events_array, $object);
                }
            }
        }
        $incubatee_deregistered_events_array = [];
        //Check if the incubatee has any de-registered events
        if(isset($incubatee->incubateeDeregisters)){
            $incubatee_deregistered_events = $incubatee->incubateeDeregisters;
            if(count($incubatee_deregistered_events) > 0){
                foreach($incubatee_deregistered_events as $i_d_event){
                    $event_id = $i_d_event->event_id;
                    $event = Event::find($event_id);

                    $object = (object)['event_title' => $i_d_event->event_name, 'event_date' => isset($event->start),
                        'de_register_reason' => $i_d_event->de_register_reason, 'date_time_de_register' => $i_d_event->date_time_de_register];
                    array_push($incubatee_deregistered_events_array, $object);
                }
            }
        }

        //WORKSHOP EVALUATION FORM
        $workshop_evaluation_form_array = [];
        if(isset($incubatee_user->bootcamper->userQuestionAnswers)){
            $incubatee_application_question_answers = $incubatee_user->userQuestionAnswers;
            if(count($incubatee_application_question_answers) > 0){
                foreach ($incubatee_application_question_answers as $application_question_answer){
                    $question_id = $application_question_answer->question_id;
                    $question = Question::find($question_id);
                    if ($question->category->category_name == 'Workshop Evaluation Form') {
                        if (isset($question)) {
                            $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                                'answer_text' => $application_question_answer->answer_text,'created_at'=>$application_question_answer->created_at];
                            array_push($workshop_evaluation_form_array, $object);
                        }
                    }
                }
            }
        }
        //Pre-bootcamp
        $pre_bootcamp_question_answers_array = [];
        if(isset($incubatee_user->bootcamper)) {
            $bootcamper = $incubatee_user->bootcamper;
            if (isset($bootcamper))
                $bootcamper->load('user');
            $bootcamper_user = $bootcamper->user;
            $bootcamper_user->load('userQuestionAnswers', 'applicant');
            if (isset($bootcamper_user))

                if(count($incubatee_user->userQuestionAnswers) > 0){
                    foreach ($incubatee_user->userQuestionAnswers as $item) {
                        $question_id = $item->question_id;
                        $question = Question::find($question_id);
                        if(isset($question)){
                            if(isset($question->category->category_name)) {
                                if ($question->category->category_name == 'Pre-Bootcamp Evaluation Form') {
                                    $answer_text = $item->answer_text;
                                    if ($answer_text == null) {

                                    } else {
                                        $object = (object)['question_number' => $question->question_number,
                                            'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                            'question_id' => $question_id];
                                        array_push($pre_bootcamp_question_answers_array, $object);
                                    }

                                }
                            }
                        }
                    }
                }

            //Post-bootcamp
            $post_bootcamp_question_answers_array = [];
            if(count($incubatee_user->userQuestionAnswers) > 0){
                foreach ($incubatee_user->userQuestionAnswers as $item) {
                    $question_id = $item->question_id;
                    $question = Question::find($question_id);
                    if(isset($question)){
                        if(isset($question->category->category_name)) {
                            if ($question->category->category_name == 'Post-Bootcamp Evaluation Form') {
                                $answer_text = $item->answer_text;
                                if ($answer_text == null) {

                                } else {
                                    $object = (object)['question_number' => $question->question_number,
                                        'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                        'question_id' => $question_id];
                                    array_push($post_bootcamp_question_answers_array, $object);
                                }

                            }
                        }
                    }
                }
            }
        }



        //PRE STAGE 2 EVALUATION
        $pre_stage2_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'Pre-Stage 2 Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($pre_stage2_question_answers_array, $object);
                            }

                        }
                    }
                }
            }
        }


        //POST STAGE 2 EVALUATION
        $post_stage2_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'Post-Stage 2 Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($post_stage2_question_answers_array, $object);
                            }

                        }
                    }
                }
            }
        }

        //PRE STAGE 3 EVALUATION
        $pre_stage3_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'Pre-Stage 3 Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($pre_stage3_question_answers_array, $object);
                            }

                        }
                    }
                }
            }
        }

        //POST STAGE 3 EVALUATION
        $post_stage3_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'Post-Stage 3 Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($post_stage3_question_answers_array, $object);
                            }

                        }
                    }
                }
            }
        }

        //PRE STAGE 4 EVALUATION
        $pre_stage4_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'Pre-Stage 4 Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($pre_stage4_question_answers_array, $object);
                            }

                        }
                    }
                }
            }
        }

        //POST STAGE 4 EVALUATION
        $post_stage4_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'Post-Stage 4 Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($post_stage4_question_answers_array, $object);
                            }

                        }
                    }
                }
            }
        }


        //PRE STAGE 5 EVALUATION
        $pre_stage5_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'Pre-Stage 5 Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($pre_stage5_question_answers_array, $object);
                            }

                        }
                    }
                }
            }
        }

        //POST STAGE 5 EVALUATION
        $post_stage5_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'Post-Stage 5 Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($post_stage5_question_answers_array, $object);
                            }

                        }
                    }
                }
            }
        }

        if ($role->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.show-venture-incubatee', compact('incubatee', 'incubatee_application_question_answers_array'
                , 'incubatee_uploads', 'incubatee_user', 'incubatee_bootcamper_events_array', 'incubatee_events_array', 'incubatee_deregistered_events_array',
                'workshop_evaluation_form_array', 'pre_stage2_question_answers_array', 'post_stage2_question_answers_array', 'pre_stage3_question_answers_array',
                'post_stage3_question_answers_array', 'pre_stage4_question_answers_array', 'post_stage4_question_answers_array', 'pre_stage5_question_answers_array',
                'post_stage5_question_answers_array', 'bootcamper', 'pre_bootcamp_question_answers_array', 'post_bootcamp_question_answers_array'));
        }elseif ($role->name == 'administrator'){
            return view('users/administrators/incubatees/show-venture-incubatee', compact('incubatee', 'incubatee_application_question_answers_array'
                ,'incubatee_uploads','incubatee_user','incubatee_bootcamper_events_array','incubatee_events_array','incubatee_deregistered_events_array',
                'workshop_evaluation_form_array','pre_stage2_question_answers_array','post_stage2_question_answers_array','pre_stage3_question_answers_array',
                'post_stage3_question_answers_array','pre_stage4_question_answers_array','post_stage4_question_answers_array','pre_stage5_question_answers_array',
                'post_stage5_question_answers_array','bootcamper','pre_bootcamp_question_answers_array','post_bootcamp_question_answers_array'));
        } elseif ($role->name == 'advisor'){
            return view('users/advisor-dashboard/incubatees/show-venture-incubatee', compact('incubatee', 'incubatee_application_question_answers_array'
                ,'incubatee_uploads','incubatee_user','incubatee_bootcamper_events_array','incubatee_events_array','incubatee_deregistered_events_array',
                'workshop_evaluation_form_array','pre_stage2_question_answers_array','post_stage2_question_answers_array','pre_stage3_question_answers_array',
                'post_stage3_question_answers_array','pre_stage4_question_answers_array','post_stage4_question_answers_array','pre_stage5_question_answers_array',
                'post_stage5_question_answers_array','bootcamper','pre_bootcamp_question_answers_array','post_bootcamp_question_answers_array'));
        }
    }

}
