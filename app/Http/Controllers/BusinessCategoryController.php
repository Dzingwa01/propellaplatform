<?php

namespace App\Http\Controllers;

use App\BusinessCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class BusinessCategoryController extends Controller
{
    public function businessCategoryIndex(){
        return view('users.business-category.business-category-index');
    }

    public function createBusinessCategory(){
        return view('users.business-category.create-business-category');
    }

    public function storeBusinessCategory(Request $request){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $business_category = BusinessCategory::create(['business_category_name' => $input['business_category_name']]);
            DB::commit();

            return response()->json(['message' => 'Business category added.']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }

    }

    public function getBusinessCategories()
    {
        $business_categories = BusinessCategory::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($business_categories)->addColumn('action', function ($business_category) {
                $re = '/business-category-edit/' . $business_category->id;
                $del = $business_category->id;
                return '<a href=' . $re . ' title="Edit Category" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_business_category(this)" title="Delete Business Category" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($business_categories)->addColumn('action', function ($business_category) {
                $re = '/business-category-edit/' . $business_category->id;
                return '<a href=' . $re . ' title="Edit Business Category" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateBusinessCategory(Request $request, BusinessCategory $businessCategory){
        $input = $request->all();

        try{
            DB::beginTransaction();
            $businessCategory->update(['business_category_name' => $input['business_category_name']]);
            $businessCategory->save();
            DB::commit();
            return response()->json(['message' => 'Business category updated.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function editBusinessCategory(BusinessCategory $businessCategory){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.business-category.edit-business-category', compact('businessCategory'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.business-category.edit-business-category', compact('businessCategory'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function deleteBusinessCategory(BusinessCategory $businessCategory){
        try{
            $businessCategory->forceDelete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }
}
