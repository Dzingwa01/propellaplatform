<?php

namespace App\Http\Controllers;

use App\ApplicantPanelist;
use App\Applicant;
use App\Appointment;
use App\DeclinedApplicant;
use App\Event;
use App\Category;
use App\EventVenue;
use App\Http\Requests\QuestionStoreRequest;
use App\Http\Requests\UserStoreRequest;
use App\Incubatee;
use App\Question;
use App\QuestionsCategory;
use App\QuestionType;
use App\TiaApplication;
use App\User;
use App\Role;
use App\Venture;
use App\VentureEmployee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function contact()
    {
        return view('/Home/contactUs');
    }

    public function visitors()
    {
        return view('/Home/visitors');
    }

    public function index()
    {
        $users = User::all();
        $user = Auth::user()->load('roles');
        $incubatees = User::whereHas('roles', function ($query) {
            $query->where('name', 'incubatee');
        })->get();
        $cur_incubatee = Incubatee::where('user_id', $user->id)->first();

        $pti_applicants_count = count(Applicant::with('user')->where('chosen_category','PTI Application')->get());

        $all_users = count(User::all());
        $all_applicants = count(Applicant::where('is_bootcamper', '=', false)
            ->where('is_incubatee', '=', false)
            ->get());
        $all_events = count(Event::all());
        $all_incubatees = count(Venture::all());
        $ict_count = count(Venture::where('hub', 'ICT')->get());
        $ind_count = count(Venture::where('hub', 'Industrial')->get());
        $tiaA  = count(TiaApplication::all());

        $ict_stage_1 = count(Venture::where('hub', 'ICT')->where('stage', 'Stage 1')->get());
        $ict_stage_2 = count(Venture::where('hub', 'ICT')->where('stage', 'Stage 2')->get());
        $ict_stage_3 = count(Venture::where('hub', 'ICT')->where('stage', 'Stage 3')->get());
        $ict_stage_4 = count(Venture::where('hub', 'ICT')->where('stage', 'Stage 4')->get());
        $ict_stage_5 = count(Venture::where('hub', 'ICT')->where('stage', 'Stage 5')->get());
        $ict_stage_6 = count(Venture::where('hub', 'ICT')->where('stage', 'Stage 6')->get());
        $ict_stage_Alumni = count(Venture::where('hub', 'ICT')->where('stage', 'Alumni')->get());
        $ict_stage_Exit = count(Venture::where('hub', 'ICT')->where('stage', 'Exit')->get());


        $ind_stage_1 = count(Venture::where('hub', 'Industrial')->where('stage', 'Stage 1')->get());
        $ind_stage_2 = count(Venture::where('hub', 'Industrial')->where('stage', 'Stage 2')->get());
        $ind_stage_3 = count(Venture::where('hub', 'Industrial')->where('stage', 'Stage 3')->get());
        $ind_stage_4 = count(Venture::where('hub', 'Industrial')->where('stage', 'Stage 4')->get());
        $ind_stage_5 = count(Venture::where('hub', 'Industrial')->where('stage', 'Stage 5')->get());
        $ind_stage_6 = count(Venture::where('hub', 'Industrial')->where('stage', 'Stage 6')->get());
        $ind_stage_Alumni = count(Venture::where('hub', 'Industrial')->where('stage', 'Alumni')->get());
        $ind_stage_Exit = count(Venture::where('hub', 'Industrial')->where('stage', 'Exit')->get());

        if ($user->roles[0]->name == 'app-admin') {
            //Load all user roles then do if statements to see if relationship exists
            $user->load('roles', 'events', 'userDeregisters', 'userQuestionAnswers', 'incubatee',
                'applicant', 'bootcamper', 'venueBooking', 'panelist', 'mentor', 'assigned_enquiry', 'pre_assigned_enquiries');

            //Place holder variables for the relationships if they are set
            $user_events = null;
            $user_deregisters = null;
            $user_question_asnwers = null;
            $user_incubatee = null;
            $user_applicant = null;
            $user_bootcamper = null;
            $user_venue_booking = null;
            $user_panelist = null;
            $user_mentor = null;
            $user_enquiries = null;
            $user_pre_assigned_enquiries = null;


            //If statements to see if the user relationships exists, and if they do, only then pass them to the view
            if(isset($user->events)){
                $user_events = $user->events;
            }

            if(isset($user->userDeregisters)){
                $user_deregisters = $user->userDeregisters;
            }

            if(isset($user->userQuestionAnswers)){
                $user_question_asnwers = $user->userQuestionAnswers;
            }

            if(isset($user->incubatee)){
                $user_incubatee = $user->incubatee;
            }

            if(isset($user->applicant)){
                $user_applicant = $user->applicant;
            }

            if(isset($user->bootcamper)){
                $user_bootcamper = $user->bootcamper;
            }

            if(isset($user->venueBooking)){
                $user_venue_booking = $user->venueBooking;
            }

            if(isset($user->panelist)){
                $user_panelist = $user->panelist;
            }

            if(isset($user->mentor)){
                $user_mentor = $user->mentor;
            }

            if(isset($user->assigned_enquiry)){
                $user_enquiries = $user->assigned_enquiry;
            }

            if(isset($user->pre_assigned_enquiries)){
                $user_pre_assigned_enquiries = $user->pre_assigned_enquiries;
            }

            return view('admin-home', compact('users', 'incubatees', 'ict_count', 'ind_count',
                'ict_stage_1', 'ict_stage_2', 'ict_stage_3', 'ind_stage_1', 'ind_stage_2', 'ind_stage_3',
                'user', 'all_incubatees', 'all_events', 'all_applicants', 'all_users', 'ict_stage_4', 'ict_stage_5',
                'ict_stage_6', 'ict_stage_Alumni', 'ict_stage_Exit', 'ind_stage_4', 'ind_stage_5', 'ind_stage_6',
                'ind_stage_Alumni', 'ind_stage_Exit', 'user_events', 'user_deregisters', 'user_question_asnwers',
                'user_incubatee', 'user_applicant', 'user_bootcamper', 'user_venue_booking', 'user_panelist', 'user_mentor',
                'user_enquiries', 'user_pre_assigned_enquiries','tiaA'));

            //End of app-admin role check//
        } else if ($user->roles[0]->name == 'clerk') {
            return view('/visitors/clerk/visitor-login', compact('incubatees', 'ict_count', 'ind_count',
                'ict_stage_1', 'ict_stage_2', 'ict_stage_3', 'ind_stage_1', 'ind_stage_2', 'ind_stage_3', 'user'));
        } else if ($user->roles[0]->name == 'applicant') {
            $user->load('applicant', 'userQuestionAnswers');

            return view('applicant-home', compact('user'));
        } elseif ($user->roles[0]->name == 'administrator') {
            //Load all user roles then do if statements to see if relationship exists
            $user->load('roles', 'events', 'userDeregisters', 'userQuestionAnswers', 'incubatee',
                'applicant', 'bootcamper', 'venueBooking', 'panelist', 'mentor', 'assigned_enquiry', 'pre_assigned_enquiries');

            //Place holder variables for the relationships if they are set
            $user_events = null;
            $user_deregisters = null;
            $user_question_asnwers = null;
            $user_incubatee = null;
            $user_applicant = null;
            $user_bootcamper = null;
            $user_venue_booking = null;
            $user_panelist = null;
            $user_mentor = null;
            $user_enquiries = null;
            $user_pre_assigned_enquiries = null;

            //If statements to see if the user relationships exists, and if they do, only then pass them to the view
            if(isset($user->events)){
                $user_events = $user->events;
            }

            if(isset($user->userDeregisters)){
                $user_deregisters = $user->userDeregisters;
            }

            if(isset($user->userQuestionAnswers)){
                $user_question_asnwers = $user->userQuestionAnswers;
            }

            if(isset($user->incubatee)){
                $user_incubatee = $user->incubatee;
            }

            if(isset($user->applicant)){
                $user_applicant = $user->applicant;
            }

            if(isset($user->bootcamper)){
                $user_bootcamper = $user->bootcamper;
            }

            if(isset($user->venueBooking)){
                $user_venue_booking = $user->venueBooking;
            }

            if(isset($user->panelist)){
                $user_panelist = $user->panelist;
            }

            if(isset($user->mentor)){
                $user_mentor = $user->mentor;
            }

            if(isset($user->assigned_enquiry)){
                $user_enquiries = $user->assigned_enquiry;
            }

            if(isset($user->pre_assigned_enquiries)){
                $user_pre_assigned_enquiries = $user->pre_assigned_enquiries;
            }

            return view('administrator-home', compact('users', 'incubatees', 'ict_count', 'ind_count',
                'ict_stage_1', 'ict_stage_2', 'ict_stage_3', 'ind_stage_1', 'ind_stage_2', 'ind_stage_3',
                'user', 'all_incubatees', 'all_events', 'all_applicants', 'all_users', 'ict_stage_4', 'ict_stage_5',
                'ict_stage_6', 'ict_stage_Alumni', 'ict_stage_Exit', 'ind_stage_4', 'ind_stage_5', 'ind_stage_6',
                'ind_stage_Alumni', 'ind_stage_Exit', 'user_events', 'user_deregisters', 'user_question_asnwers',
                'user_incubatee', 'user_applicant', 'user_bootcamper', 'user_venue_booking', 'user_panelist', 'user_mentor',
                'user_enquiries', 'user_pre_assigned_enquiries'));

            //End of administrator role check//
        } elseif ($user->roles[0]->name == 'incubatee') {
            $user->load('assigned_enquiry', 'pre_assigned_enquiries');
            $incubatee = Incubatee::where('user_id', $user->id)->first();
            $incubatee->load('venture');
            $user_enquiries = null;
            $user_pre_assigned_enquiries = null;

            if(isset($user->assigned_enquiry)){
                $user_enquiries = $user->assigned_enquiry;
            }

            if(isset($user->pre_assigned_enquiries)){
                $user_pre_assigned_enquiries = $user->pre_assigned_enquiries;
            }

            $category = QuestionsCategory::with('questions')->where('category_name','Workshop evaluation form')->get();
            $stage3_workshop = QuestionsCategory::with('questions')->where('category_name','Stage 3 workshop evaluation form')->get();
            $stage4_workshop = QuestionsCategory::with('questions')->where('category_name','Stage 4 workshop evaluation form')->get();
            $stage5_workshop = QuestionsCategory::with('questions')->where('category_name','Stage 5 workshop evaluation form')->get();
            $stage6_workshop = QuestionsCategory::with('questions')->where('category_name','Stage 6 workshop evaluation form')->get();
            $end_of_stage3_workshop = QuestionsCategory::with('questions')->where('category_name','End of Stage 3 Workshop Evaluation Form')->get();

            $pre_stage2_workshop = QuestionsCategory::with('questions')->where('category_name','CC 2022 3 Pre-Stage 2 Evaluation Form')->get();
            $post_stage2_workshop = QuestionsCategory::with('questions')->where('category_name','CC 2022 4 Post-Stage 2 Evaluation Form')->get();

            $pre_stage3_workshop = QuestionsCategory::with('questions')->where('category_name','CC 2022 5 Pre-Stage 3 Evaluation Form')->get();
            $post_stage3_workshop = QuestionsCategory::with('questions')->where('category_name','CC 2022 6 Post-Stage 3 Evaluation Form')->get();

            $pre_stage4_workshop = QuestionsCategory::with('questions')->where('category_name','CC 2022 7 Pre-Stage 4 Evaluation Form')->get();
            $post_stage4_workshop = QuestionsCategory::with('questions')->where('category_name','CC 2022 8 Post-Stage 4 Evaluation Form')->get();


            $pre_stage5_workshop = QuestionsCategory::with('questions')->where('category_name','CC 2022 9 Pre-Stage 5 Evaluation Form')->get();
            $post_stage5_workshop = QuestionsCategory::with('questions')->where('category_name','CC 2022 Post-Stage 5 Evaluation Form')->get();


            /*dd($venture_employee);*/
            return view('incubatee-home', compact('incubatee', 'user', 'user_enquiries', 'user_pre_assigned_enquiries','category',
            'stage3_workshop','stage4_workshop','stage5_workshop','stage6_workshop','end_of_stage3_workshop','pre_stage2_workshop','post_stage2_workshop','pre_stage3_workshop','post_stage3_workshop','pre_stage4_workshop','post_stage4_workshop','pre_stage5_workshop','post_stage5_workshop'));
        } elseif ($user->roles[0]->name == 'marketing') {
            $pending_applicants = Applicant::where('is_incubatee', false)->get();
            $declined_applicants = DeclinedApplicant::all();
            //Load all user roles then do if statements to see if relationship exists
            $user->load('roles', 'events', 'userDeregisters', 'userQuestionAnswers', 'incubatee',
                'applicant', 'bootcamper', 'venueBooking', 'panelist', 'mentor', 'assigned_enquiry', 'pre_assigned_enquiries');

            //Place holder variables for the relationships if they are set
            $user_events = null;
            $user_deregisters = null;
            $user_question_asnwers = null;
            $user_incubatee = null;
            $user_applicant = null;
            $user_bootcamper = null;
            $user_venue_booking = null;
            $user_panelist = null;
            $user_mentor = null;
            $user_enquiries = null;
            $user_pre_assigned_enquiries = null;


            //If statements to see if the user relationships exists, and if they do, only then pass them to the view
            if(isset($user->events)){
                $user_events = $user->events;
            }

            if(isset($user->userDeregisters)){
                $user_deregisters = $user->userDeregisters;
            }

            if(isset($user->userQuestionAnswers)){
                $user_question_asnwers = $user->userQuestionAnswers;
            }

            if(isset($user->incubatee)){
                $user_incubatee = $user->incubatee;
            }

            if(isset($user->applicant)){
                $user_applicant = $user->applicant;
            }

            if(isset($user->bootcamper)){
                $user_bootcamper = $user->bootcamper;
            }

            if(isset($user->venueBooking)){
                $user_venue_booking = $user->venueBooking;
            }

            if(isset($user->panelist)){
                $user_panelist = $user->panelist;
            }

            if(isset($user->mentor)){
                $user_mentor = $user->mentor;
            }

            if(isset($user->assigned_enquiry)){
                $user_enquiries = $user->assigned_enquiry;
            }

            if(isset($user->pre_assigned_enquiries)){
                $user_pre_assigned_enquiries = $user->pre_assigned_enquiries;
            }

            return view('marketing-home', compact('users', 'incubatees', 'ict_count', 'ind_count',
                'ict_stage_1', 'ict_stage_2', 'ict_stage_3', 'ind_stage_1', 'ind_stage_2', 'ind_stage_3',
                'user', 'all_incubatees', 'all_events', 'all_applicants', 'all_users', 'ict_stage_4', 'ict_stage_5',
                'ict_stage_6', 'ict_stage_Alumni', 'ict_stage_Exit', 'ind_stage_4', 'ind_stage_5', 'ind_stage_6',
                'ind_stage_Alumni', 'ind_stage_Exit', 'user_events', 'user_deregisters', 'user_question_asnwers',
                'user_incubatee', 'user_applicant', 'user_bootcamper', 'user_venue_booking', 'user_panelist', 'user_mentor', 'pending_applicants', 'declined_applicants',
                'user_enquiries', 'user_pre_assigned_enquiries'));

            //End of marketing role check//
        } elseif ($user->roles[0]->name == 'advisor') {
            //Load all user roles then do if statements to see if relationship exists
            $user->load('roles', 'events', 'userDeregisters', 'userQuestionAnswers', 'incubatee',
                'applicant', 'bootcamper', 'venueBooking', 'panelist', 'mentor', 'assigned_enquiry', 'pre_assigned_enquiries');

            //Place holder variables for the relationships if they are set
            $user_events = null;
            $user_deregisters = null;
            $user_question_asnwers = null;
            $user_incubatee = null;
            $user_applicant = null;
            $user_bootcamper = null;
            $user_venue_booking = null;
            $user_panelist = null;
            $user_mentor = null;
            $user_enquiries = null;
            $user_pre_assigned_enquiries = null;

            //If statements to see if the user relationships exists, and if they do, only then pass them to the view
            if(isset($user->events)){
                $user_events = $user->events;
            }

            if(isset($user->userDeregisters)){
                $user_deregisters = $user->userDeregisters;
            }

            if(isset($user->userQuestionAnswers)){
                $user_question_asnwers = $user->userQuestionAnswers;
            }

            if(isset($user->incubatee)){
                $user_incubatee = $user->incubatee;
            }

            if(isset($user->applicant)){
                $user_applicant = $user->applicant;
            }

            if(isset($user->bootcamper)){
                $user_bootcamper = $user->bootcamper;
            }

            if(isset($user->venueBooking)){
                $user_venue_booking = $user->venueBooking;
            }

            if(isset($user->panelist)){
                $user_panelist = $user->panelist;
            }

            if(isset($user->mentor)){
                $user_mentor = $user->mentor;
            }

            if(isset($user->assigned_enquiry)){
                $user_enquiries = $user->assigned_enquiry;
            }

            if(isset($user->pre_assigned_enquiries)){
                $user_pre_assigned_enquiries = $user->pre_assigned_enquiries;
            }
            $tiaA  = count(TiaApplication::all());
            return view('advisor-home', compact('users', 'incubatees', 'ict_count', 'ind_count',
                'ict_stage_1', 'ict_stage_2', 'ict_stage_3', 'ind_stage_1', 'ind_stage_2', 'ind_stage_3',
                'user', 'all_incubatees', 'all_events', 'all_applicants', 'all_users', 'ict_stage_4', 'ict_stage_5',
                'ict_stage_6', 'ict_stage_Alumni', 'ict_stage_Exit', 'ind_stage_4', 'ind_stage_5', 'ind_stage_6',
                'ind_stage_Alumni', 'ind_stage_Exit', 'user_events', 'user_deregisters', 'user_question_asnwers',
                'user_incubatee', 'user_applicant', 'user_bootcamper', 'user_venue_booking', 'user_panelist', 'user_mentor',
                'user_enquiries', 'user_pre_assigned_enquiries','tiaA'));

            //End of advisor role check//

        } elseif ($user->roles[0]->name == 'mentor') {
            $tiaA  = count(TiaApplication::all());
            $events = Event::with('EventVenue')->get();
            $venueName = EventVenue::orderBy('venue_name', 'asc')->get();

            $calendar = \Calendar::addEvents($events)->setCallbacks(['eventClick' => 'function(calEvent, jsEvent, view){

        }']);
            //Load all user roles then do if statements to see if relationship exists
            $user->load('roles', 'events', 'userDeregisters', 'userQuestionAnswers', 'incubatee',
                'applicant', 'bootcamper', 'venueBooking', 'panelist', 'mentor', 'assigned_enquiry', 'pre_assigned_enquiries');

            //Place holder variables for the relationships if they are set
            $user_events = null;
            $user_deregisters = null;
            $user_question_asnwers = null;
            $user_incubatee = null;
            $user_applicant = null;
            $user_bootcamper = null;
            $user_venue_booking = null;
            $user_panelist = null;
            $user_mentor = null;
            $user_enquiries = null;
            $user_pre_assigned_enquiries = null;

            //If statements to see if the user relationships exists, and if they do, only then pass them to the view
            if(isset($user->events)){
                $user_events = $user->events;
            }

            if(isset($user->userDeregisters)){
                $user_deregisters = $user->userDeregisters;
            }

            if(isset($user->userQuestionAnswers)){
                $user_question_asnwers = $user->userQuestionAnswers;
            }

            if(isset($user->incubatee)){
                $user_incubatee = $user->incubatee;
            }

            if(isset($user->applicant)){
                $user_applicant = $user->applicant;
            }

            if(isset($user->bootcamper)){
                $user_bootcamper = $user->bootcamper;
            }

            if(isset($user->venueBooking)){
                $user_venue_booking = $user->venueBooking;
            }

            if(isset($user->panelist)){
                $user_panelist = $user->panelist;
            }

            if(isset($user->mentor)){
                $user_mentor = $user->mentor;
            }

            if(isset($user->assigned_enquiry)){
                $user_enquiries = $user->assigned_enquiry;
            }

            if(isset($user->pre_assigned_enquiries)){
                $user_pre_assigned_enquiries = $user->pre_assigned_enquiries;
            }

            $incubatees = User::whereHas('roles', function ($query) {
                $query->where('name', 'incubatee');})->orderby('name','asc')->get();
            $appointment = Appointment::all();

            return view('mentor-home', compact('users', 'incubatees', 'ict_count', 'ind_count',
                'ict_stage_1', 'ict_stage_2', 'ict_stage_3', 'ind_stage_1', 'ind_stage_2', 'ind_stage_3',
                'user', 'all_incubatees', 'all_events', 'all_applicants', 'all_users', 'ict_stage_4', 'ict_stage_5',
                'ict_stage_6', 'ict_stage_Alumni', 'ict_stage_Exit', 'ind_stage_4', 'ind_stage_5', 'ind_stage_6',
                'ind_stage_Alumni', 'ind_stage_Exit', 'user_events', 'user_deregisters', 'user_question_asnwers',
                'user_incubatee', 'user_applicant', 'user_bootcamper', 'user_venue_booking', 'user_panelist', 'user_mentor',
                'user_enquiries', 'user_pre_assigned_enquiries','tiaA',
                'venueName','calendar','appointment'));

            //End of mentor role check//
        } elseif ($user->roles[0]->name == 'panelist') {
            $user->load('panelist');
            $user_panelist = $user->panelist;
            $user_panelist->load('applicants');
            $panelist_applicants = $user_panelist;
            $applicant_count = count($panelist_applicants->applicants);

            return view('panelist-home', compact('user', 'applicant_count'));
        } elseif ($user->roles[0]->name == 'tenant') {
            $user->load('assigned_enquiry', 'pre_assigned_enquiries');
            $user_enquiries = null;
            $user_pre_assigned_enquiries = null;

            if(isset($user->assigned_enquiry)){
                $user_enquiries = $user->assigned_enquiry;
            }

            if(isset($user->pre_assigned_enquiries)){
                $user_pre_assigned_enquiries = $user->pre_assigned_enquiries;
            }

            return view('tenant-home', compact('users', 'incubatees', 'ict_count', 'ind_count',
                'ict_stage_1', 'ict_stage_2', 'ict_stage_3', 'ind_stage_1', 'ind_stage_2', 'ind_stage_3', 'user', 'all_events',
                'user_enquiries', 'user_pre_assigned_enquiries'));
        }elseif  ($user->roles[0]->name == 'tablet') {
            return view('tablet-home', compact('users', 'incubatees', 'ict_count', 'ind_count',
                'ict_stage_1', 'ict_stage_2', 'ict_stage_3', 'ind_stage_1', 'ind_stage_2', 'ind_stage_3', 'user'));

        } else if($user->roles[0]->name == 'bootcamper'){
            $user->load('bootcamper', 'userQuestionAnswers', 'applicant');
            $bootcamper = $user->bootcamper;
            $bootcamper->load('events');
            $bootcamper_events = $bootcamper->events;
            $bootcamper_event_array = [];
            $bootcamper_question_array = [];
            $g_question_category = "";
            $g_questions = "";
            $category = QuestionsCategory::with('questions')->where('category_name','Bootcamp evaluation form')->get();
            $beginning_of_bootcamper = QuestionsCategory::with('questions')->where('category_name','CC 2022 1 Pre-Bootcamp Evaluation Form')->get();
            $post_evaluation = QuestionsCategory::with('questions')->where('category_name','CC 2022 2 Post-Bootcamp Evaluation Form')->get();


            foreach ($user->userQuestionAnswers as $userQuestionAnswer) {
                $question_id = $userQuestionAnswer->question_id;
                $question = Question::find($question_id);
                $answer_text = $userQuestionAnswer->answer_text;
                if(isset($question)) {
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'AA Application form - ICT Bootcamp' or $question->category->category_name == 'AA Application form - Propella Township Incubator') {
                            $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                                'answer_text' => $answer_text, 'question_id' => $question_id];

                            array_push($bootcamper_question_array, $object);
                        }
                    }
                }
            }

            //Bootcamp evaluation form
            $bootcamper_evaluation_question_array = [];
            foreach ($user->userQuestionAnswers as $userQuestionAnswer) {
                $question_id = $userQuestionAnswer->question_id;
                $question = Question::find($question_id);
                $answer_text = $userQuestionAnswer->answer_text;
                if(isset($question)) {
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'Bootcamp evaluation form') {
                            $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                                'answer_text' => $answer_text, 'question_id' => $question_id];

                            array_push($bootcamper_evaluation_question_array, $object);
                        }
                    }
                }
            }

            /*Beginning of bootcamp evaluation*/
            $beginning_of_evaluation_question_array = [];
            if(count($user->userQuestionAnswers) > 0) {
                foreach ($user->userQuestionAnswers as $userQuestionAnswer) {
                    $question_id = $userQuestionAnswer->question_id;
                    $question = Question::find($question_id);
                    $answer_text = $userQuestionAnswer->answer_text;
                    if (isset($question)) {
                        if (isset($question->category->category_name)) {
                            if ($question->category->category_name == 'CC 2022 1 Pre-Bootcamp Evaluation Form') {
                                $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                                    'answer_text' => $answer_text, 'question_id' => $question_id];
                                array_push($beginning_of_evaluation_question_array, $object);
                            }

                        }
                    }
                }
            }

            foreach ($bootcamper_events as $b_event){
                $event = Event::find($b_event->event_id);
                if(isset($event))
                $object = (object)['title' => $event->title, 'start' => $event->start->toDateString(),
                    'accepted' => $b_event->accepted, 'b_event_id' => $b_event->id, 'declined' => $b_event->declined,'id'=>$b_event->id,
                    'cell_number'=>$b_event->cell_number,'network'=>$b_event->network];
                if(isset($object))
                array_push($bootcamper_event_array, $object);
            }

            return view('bootcamper-home', compact('user', 'bootcamper_event_array', 'bootcamper',
                'bootcamper_question_array', 'g_question_category', 'g_questions','category','bootcamper_evaluation_question_array','beginning_of_bootcamper',
                'beginning_of_evaluation_question_array','post_evaluation'));
        } elseif ($user->roles[0]->name == 'PTI Admin'){
            return view('pti-home',compact('pti_applicants_count'));
        } elseif($user->roles[0]->name == 'rinp-administrator') {
            return view('rinp-administrator-home');
        } else {
            return response("You are not authorized!!");
        }
    }

    public function getIncubatees()
    {
        $incubatees = Incubatee::with('user')->where('hub', 'ICT')->get();
        return view('home.Incubatees', compact('incubatees'));
    }

    public function getIndustrial()
    {
        $incubatees = Incubatee::with('user')->where('hub', 'Industrial')->get();
        return view('home.Industrial', compact('incubatees'));
    }

    public function getFounder(Incubatee $incubatee)
    {

        return view('home.founders', compact('incubatee'));
    }

    //Upload Questions code.
    public function createQuestions()
    {
        $user = Auth::user()->load('roles');
        $questionCategories = QuestionsCategory::orderBy('category_name', 'asc')->get();
        $questionTypes = QuestionType::orderBy('datatype', 'asc')->get();

    }

    //Function to upload a question to the db
    public function storeQuestions(QuestionStoreRequest $request)
    {
        $input = $request->all();


        DB::beginTransaction();

        try {
            $question = Question::create(['question_text' => $input['question_text'],
                'percentage' => $input['percentage'],
                'question_category_id' => $input['question_category_id'],
                'question_type_id' => $input['question_type_id'],
                'question_number' => $input['question_number']]);

            DB::commit();

            return response()->json(['message' => 'Question added successfully', 'questions' => $question]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Function to view all questions
    public function questionsIndex()
    {
        $user = Auth::user();
        $user->load('roles');
        $roles = Role::where('name', '!=', 'app-admin')->get();
        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'clerk') {
            return view('users.questions.index', compact('roles'));
        } else {
            return view('users.profile', compact('roles'));
        }
    }

    //Function to get all questions


    //Function to edit a specific question
    public function editQuestions(Question $question)
    {
        $question->load('category');
        $questionCategories = QuestionsCategory::orderBy('category_name', 'asc')->get();
        $questionTypes = QuestionType::orderBy('datatype', 'asc')->get();

        return view('users.questions.question-edit', compact('question', 'questionCategories', 'questionTypes'));
    }

    //Function to update a specific question
    public function updateQuestions(QuestionStoreRequest $request, Question $question)
    {
        $input = $request->validated();

        DB::beginTransaction();

        try {
            $cur = $question;
            $question->update(['question_text' => $input['question_text'],
                'percentage' => $input['percentage'],
                'question_category_id' => $input['question_category_id'],
                'question_type_id' => $input['question_type_id'],
                'question_number' => $input['question_number']]);

            $question->save();

            DB::commit();


            $question = $cur->fresh();


            return response()->json(['message' => 'Question successfully updated', 'questions' => $question]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    //Function to delete a question
    public function destroyQuestions(Question $question)
    {
        $question->delete();

        return view('users.questions.index');
    }

    //Function for Thank you page
    public function endOfApplication()
    {
        return view('Application-Process.Thanks-page');
    }

    //Basic info page
    public function basicInfo()
    {
        return view('Application-Process.Basic-info');
    }

    //REPORTING
    public function reports()
    {
        $roles = Role::orderBy('name', 'asc')->get();
        $category = Category::orderBy('category_name', 'asc')->get();
        $incubatee = Incubatee::orderBy('startup_name', 'asc')->get();


        return view('users/app-admin/reports', compact('roles', 'category', 'incubatee'));
    }
}
