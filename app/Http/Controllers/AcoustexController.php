<?php
/**
 * Created by PhpStorm.
 * User: nkulu
 * Date: 3/13/2019
 * Time: 10:22 AM
 */

namespace App\Http\Controllers;


class AcoustexController extends Controller
{
    public function login()
    {
        return view('/Acoustex/login_page');
    }

    public function rawMaterial()
    {
        return view('/Acoustex/Raw_material');
    }
    public function loginNav()
    {
        return view('/Acoustex/loginNavigation');
    }

}

