<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\CompanyEmployee;
use App\CompanySector;
use App\Http\Requests\CompanyStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::orderBy('category_name', 'asc')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.index', compact('categories'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.campaign-manager.index', compact('categories'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.campaign-manager.index', compact('categories'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.campaign-manager.index', compact('categories'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.mentor-dashboard.campaign-manager.index', compact('categories'));
        } else {
            return view('campaign-manager.clerk.index', compact('categories'));
        }


    }

    public function getCompanies()
    {
        $companies = Company::with('category')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return Datatables::of($companies)
                ->addColumn('action', function ($company) {
                    $re = '/company/' . $company->id;
                    $sh = '/company/show/' . $company->id;
                    $del = $company->id;
                    return '<a href=' . $sh . ' title="View Company Employees"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . '  title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_company(this)" title="Delete Company" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
                })
                ->make(true);
        } elseif ($user->roles[0]->name == "administrator") {
            return Datatables::of($companies)
                ->addColumn('action', function ($company) {
                    $re = '/company/' . $company->id;
                    $sh = '/company/show/' . $company->id;
                    $del = $company->id;
                    return '<a href=' . $sh . ' title="View Company Employees"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . '  title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_company(this)" title="Delete Company" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
                })
                ->make(true);
        } elseif ($user->roles[0]->name == "marketing") {
            return Datatables::of($companies)
                ->addColumn('action', function ($company) {
                    $re = '/company/' . $company->id;
                    $sh = '/company/show/' . $company->id;
                    return '<a href=' . $sh . ' title="View Company Employees"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a>';
                })
                ->make(true);
        } elseif ($user->roles[0]->name == "advisor") {
            return Datatables::of($companies)
                ->addColumn('action', function ($company) {
                    $re = '/company/' . $company->id;
                    $sh = '/company/show/' . $company->id;
                    return '<a href=' . $sh . ' title="View Company Employees"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a>';
                })
                ->make(true);
        } elseif ($user->roles[0]->name == "mentor") {
            return Datatables::of($companies)
                ->addColumn('action', function ($company) {
                    $re = '/company/' . $company->id;
                    $sh = '/company/show/' . $company->id;
                    return '<a href=' . $sh . ' title="View Company Employees"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a>';
                })
                ->make(true);
        } else {
            return Datatables::of($companies)
                ->addColumn('action', function ($company) {
                    $re = '/company/' . $company->id;
                    $sh = '/company/show/' . $company->id;
                    return '<a href=' . $sh . ' title="View Company Employees"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a>';
                })
                ->make(true);
        }
    }


    public function create()
    {
        $categories = Category::orderBy('category_name', 'asc')->get();
        $companies = Company::orderBy('company_name', 'asc')->get();
        $company_sector = CompanySector::orderBy('sector_name', 'asc')->get();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.company-create', compact('companies', 'categories', 'company_sector'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.campaign-manager.company-create', compact('companies', 'categories', 'company_sector'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.campaign-manager.company-create', compact('companies', 'categories', 'company_sector'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.campaign-manager.company-create', compact('companies', 'categories', 'company_sector'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.campaign-manager.company-create', compact('companies', 'categories', 'company_sector'));
        } else {
            return view('campaign-manager.clerk.company-create', compact('companies', 'categories', 'company_sector'));
        }
    }


    public function store(CompanyStoreRequest $request)
    {
        $input = $request->validated();
        DB::beginTransaction();
        try {
            $company = Company::create(['company_name' => $input['company_name'],
                'contact_number' => $input['contact_number'],
                'address_one' => $input['address_one'],
                'address_two' => $input['address_two'],
                'address_three' => $input['address_three'],
                'city' => $input['city'],
                'postal_code' => $input['postal_code'],
                'website_url' => $input['website_url'],
                'category_id' => $input['category_id'],
                'company_sector_id' => $input['company_sector_id'],
                'turnover' => $input['turnover']]);

            DB::commit();

            $categories = explode(',', $input['secondary_category_id']);

            foreach ($categories as $category) {
                $company->secondary_interests()->attach($category);
            }

            return response()->json(['company' => $company, 'message' => 'Company created successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Company could not be saved at the moment ' . $e->getMessage()], 400);
        }
    }


    public function show(Company $company)
    {
        $company->load('category');
        $categories = Category::orderBy('category_name', 'asc')->get();
        $companies = Company::orderBy('company_name', 'asc')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.company-employees', compact('company', 'companies', 'categories'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.campaign-manager.company-employees', compact('company', 'companies', 'categories'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.campaign-manager.company-employees', compact('company', 'companies', 'categories'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.campaign-manager.company-employees', compact('company', 'companies', 'categories'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.advisor-dashboard.campaign-manager.company-employees', compact('company', 'companies', 'categories'));
        } else {
            return view('campaign-manager.clerk.company-employees', compact('company', 'companies', 'categories'));
        }
    }


    public function getSpecificCompanyEmployees(Company $company)
    {
        $companiesEmployees = CompanyEmployee::with('categories', 'company')->where('company_id', $company->id)->get();

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    $del =  $employee->id;
                    return '<a href=' . $sh . ' title="View Company Employee"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company Details" style="color:green!important;"><i  class="material-icons">create</i></a><a id=' . $del . '  onclick="confirm_delete_company_employee(this)" title="Delete Company" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';

                })->make(true);
        } elseif ($user->roles[0]->name == "administrator") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    return '<a href=' . $sh . ' title="View Company Employee"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company Details" style="color:green!important;"><i  class="material-icons">create</i></a>';

                })->make(true);
        } elseif ($user->roles[0]->name == "marketing") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    return '<a href=' . $sh . ' title="View Company Employee"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company Details" style="color:green!important;"><i  class="material-icons">create</i></a>';

                })->make(true);
        } elseif ($user->roles[0]->name == "advisor") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    $del =  $employee->id;
                    return '<a href=' . $sh . ' title="View Company Employee"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company Details" style="color:green!important;"><i  class="material-icons">create</i></a><a id=' . $del . '  onclick="confirm_delete_company_employee(this)" title="Delete Company" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';

                })->make(true);
        } elseif ($user->roles[0]->name == "mentor") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    $del =  $employee->id;
                    return '<a href=' . $sh . ' title="View Company Employee"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company Details" style="color:green!important;"><i  class="material-icons">create</i></a><a id=' . $del . '  onclick="confirm_delete_company_employee(this)" title="Delete Company" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';

                })->make(true);
        } else {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    return '<a href=' . $sh . ' title="View Company Employee"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company Details" style="color:green!important;"><i  class="material-icons">create</i></a>';

                })->make(true);
        }
    }


    public function edit(Company $company)
    {
        $categories = Category::orderBy('category_name', 'asc')->get();
        $company->load('category', 'secondary_interests');
        $company_sector = CompanySector::orderBy('sector_name', 'asc')->get();

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {

            return view('campaign-manager.company-edit', compact('company', 'categories', 'company_sector'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.campaign-manager.company-edit', compact('company', 'categories', 'company_sector'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.campaign-manager.company-edit', compact('company', 'categories', 'company_sector'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.campaign-manager.company-edit', compact('company', 'categories', 'company_sector'));
        } else {
            return view('campaign-manager.clerk.company-edit', compact('company', 'categories', 'company_sector'));
        }
    }

    public function update(CompanyStoreRequest $request, Company $company)
    {
        $input = $request->validated();

        DB::beginTransaction();
        try {
            $cur = $company;
            $company->update(['company_name' => $input['company_name'],
                'contact_number' => $input['contact_number'],
                'address_one' => $input['address_one'],
                'address_two' => $input['address_two'],
                'address_three' => $input['address_three'],
                'city' => $input['city'],
                'postal_code' => $input['postal_code'],
                'website_url' => $input['website_url'],
                'category_id' => $input['category_id'],
                'company_sector_id' => $input['company_sector_id'],
                'turnover' => $input['turnover']]);
            $company = $cur->fresh();

            $categories = explode(',', $input['secondary_category_id']);
            $company->secondary_interests()->sync($categories);

            DB::commit();
            return response()->json(['company' => $company, 'message' => 'Company updated successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Company could not be updated at the moment ' . $e->getMessage()], 400);
        }
    }


    public function destroy(Company $company)
    {
        $company->load('employees');

        foreach ($company->employees as $c_employee){
            $c_employee->forceDelete();
        }
        $company->forceDelete();
        return redirect('companies');
    }
}
