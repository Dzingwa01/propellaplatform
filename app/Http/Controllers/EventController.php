<?php

namespace App\Http\Controllers;

use App\Bootcamper;
use App\DeclinedBootcamper;
use App\EventBootcamper;
use App\EventMediaImage;
use App\Http\Requests\EventStoreRequest;
use App\Incubatee;
use App\PropellaVenue;
use App\Visitor;
use Carbon\Carbon;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Calendar;
use App\Event;
use App\User;
use App\Role;
use App\EventVenue;
use App\EventsMedia;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;


class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function createEventVenue()
    {
        $users = Auth::user()->load('roles');
        if ($users->roles[0]->name == 'app-admin') {
            return view('users.app-admin.full-calendar.create-event-venue');
        } elseif ($users->roles[0]->name == 'administrator') {
            return view('users.administrators.full-calendar.create-event-venue');
        } elseif ($users->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.full-calendar.create-event-venue');
        } elseif ($users->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.full-calendar.create-event-venue');
        } elseif ($users->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.full-calendar.create-event-venue');
        } elseif ($users->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.full-calendar.create-event-venue');
        } elseif ($users->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.full-calendar.create-event-venue');
        } elseif ($users->roles[0]->name == 'tablet') {
            return view('users.tablet.index');
        }
    }


    public function storeEventVenue(Request $request)
    {

        $input = $request->all();

        DB::beginTransaction();
        try {
            $venue_booking = EventVenue::create(['venue_name' => $input['venue_name']]);
            $venue_booking->save();
            DB::commit();
            return response()->json(['booking' => $venue_booking, 'message' => 'Venue Has Been created successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Venue could not be saved at the moment ' . $e->getMessage()], 400);
        }
    }


    public function indexVenue()
    {
        $users = Auth::user()->load('roles');
        if ($users->roles[0]->name == 'administrator') {
            return view('users.administrators.full-calendar.index-event-venue');
        }
    }

    public function getVenues()
    {
        $venues = EventVenue::all();

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'administrator') {
            return Datatables::of($venues)->addColumn('action', function ($venue) {
                $edit = '/edit-venue-event/' . $venue->id;
                $del = $venue->id;
                return '<a href=' . $edit . ' title="Edit Venues" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete(this)" title="Delete Event venue" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function deleteEventVenue(EventVenue $eventVenue){
        try{
            DB::beginTransaction();
            $eventVenue->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Event venue  deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Mentor cannot be deleted ' . $e->getMessage()], 500);
        }
    }

    public function editEventVenue($id)
    {

        $eventVenue = EventVenue::find($id);

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.full-calendar.edit-event-venue', compact('eventVenue'));
        }

    }

    public function updateVenueEvent(Request $request, $id)
    {
        $input = $request->all();

        $eventVenue = EventVenue::find($id);
        DB::beginTransaction();
        try {
            $eventVenue->update(['venue_name' => $input['venue_name']]);
            $eventVenue->save();
            DB::commit();
            return response()->json(['booking' => $eventVenue, 'message' => 'Venue Has Been Updated successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Venue could not be saved at the moment ' . $e->getMessage()], 400);
        }
    }





    public function index()
    {
        $events = Event::with('EventVenue')->get();
        $venueName = EventVenue::orderBy('venue_name', 'asc')->get();

        $calendar = \Calendar::addEvents($events)->setCallbacks(['eventClick' => 'function(calEvent, jsEvent, view){

        }']);

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('full_calendar.events', compact('calendar', 'venueName', 'events'));
        } else if ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.full-calendar.events', compact('calendar', 'venueName'));
        } else if ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.full-calendar.events', compact('calendar', 'venueName'));
        } else if ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.full-calendar.events', compact('calendar', 'venueName', 'events'));
        } else if ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.full-calendar.events',compact('calendar', 'venueName', 'events'));
        }else if ($user->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.full_calendar.events',compact('calendar', 'venueName', 'events'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function create(Request $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();

            $save_event = Event::create(['title' => $input['title'],'slug' => Str::slug($input['title'],'-'), 'start' => $input['start'],
                'end' => $input['end'], 'all_day' => $input['all_day'], 'color' => $input['color'], 'event_venue_id' => $input['venue_types'],
                'description' => $input['description'], 'type' => $input['type'], 'participants' => $input['participants'],
                'start_time' => $input['start_time'],
                'end_time' => $input['end_time']
            ]);

            if ($request->hasFile('event_image_url')) {
                $event_logo_url_path = $request->file('event_image_url')->store('events_media');
                if (isset($save_event)) {
                    $save_event->update(['event_image_url' => $event_logo_url_path]
                    );
                } else {
                    $save_event->create(['event_image_url' => $event_logo_url_path]
                    );
                }
            }
            if ($request->hasFile('invite_image_url')) {
                $invite_url_path = $request->file('invite_image_url')->store('events_media');
                if (isset($save_event)) {
                    $save_event->update(['invite_image_url' => $invite_url_path]
                    );
                } else {
                    $save_event->create(['invite_image_url' => $invite_url_path]
                    );
                }
            }

            DB::commit();
            return response()->json(['event' => $save_event, 'message' => 'Successfully added Event.'], 200);
        } catch
        (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    public function display()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('/full_calendar/view-events');
        }else if ($user->roles[0]->name == 'administrator'){
            return view('users/administrators/full-calendar/view-events');
        }else if ($user->roles[0]->name == 'advisor'){
            return view('users/advisor-dashboard/view-events');
        } else if ($user->roles[0]->name == 'tenant'){
            return view('users/tenant-dashboard/full_calendar/view-events');
        }else if ($user->roles[0]->name == 'marketing'){
            return view('users/marketing-dashboard/full-calendar/view-events');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function editEvents(Event $event)
    {
        $event->load('EventVenue', 'EventsMedia');
        $event_media = $event->EventsMedia;
        $venues = EventVenue::all();

        $event_media_images = [];

        if(isset($event_media)){
            $event_media->load('eventsMediaImage');

            if(isset($event_media->eventsMediaImage)){
                foreach($event_media->eventsMediaImage as $media_image){
                    array_push($event_media_images, $media_image);
                }
            }
        }

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('/full_calendar.event-edit', compact('event', 'event_media_images','venues'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.full-calendar.event-edit', compact('event', 'event_media_images','venues'));
        } elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.full-calendar.event-edit', compact('event', 'event_media_images','venues'));
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.full-calendar.event-edit', compact('event', 'event_media_images','venues'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.full-calendar.event-edit', compact('event', 'event_media_images','venues'));
        } elseif ($user->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.full_calendar.event-edit', compact('event', 'event_media_images','venues'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateEvents(Request $request, Event $event)
    {
        $input = $request->all();
        $images_count = $input['image_array_count'];

        try {
            DB::beginTransaction();
            $event->load('EventsMedia');

            $event->update(['title' => $input['title'],'slug' => Str::slug($input['title'],'-'), 'start' => $input['start'],
                'end' => $input['end'], 'all_day' => $input['all_day'], 'color' => $input['color'], 'event_venue_id' => $input['venue_types'],
                'description' => $input['description'], 'type' => $input['type'], 'participants' => $input['participants'],
                'start_time' => $input['start_time'],
                'end_time' => $input['end_time'],
                'eventStatus' => $input['eventStatus'],
                'eventShow' => $input['eventShow']
            ]);
            $event->save();

            if(isset($event->EventsMedia)){
                $event->EventsMedia->media_description = $input['media_description'];

                //Images will always be added to EventsMedia so therefor we will always run a create function.
                for ($i = 0; $i < $images_count; $i++) {
                    $cur_image_url = $request->file('image_' . $i)->store('events_media');

                    if ($cur_image_url) {
                        $create_event_media_images = EventMediaImage::create(['event_media_url' => $cur_image_url, 'event_media_id' => $event->EventsMedia->id]);
                    }
                }
            } else {
                $create_event_media = EventsMedia::create(['media_description' => $input['media_description'], 'events_id' => $event->id]);

                //Images will always be added to EventsMedia so therefor we will always run a create function.
                for ($i = 0; $i < $images_count; $i++) {
                    $cur_image_url = $request->file('image_' . $i)->store('events_media');

                    if ($cur_image_url) {
                        $create_event_media_images = EventMediaImage::create(['event_media_url' => $cur_image_url, 'event_media_id' => $create_event_media->id]);
                    }
                }
            }

            if ($request->hasFile('event_image_url')) {
                $event_logo_url_path = $request->file('event_image_url')->store('events_media');
                if (isset($event)) {
                    $event->update(['event_image_url' => $event_logo_url_path]
                    );
                }
            }
            if ($request->hasFile('invite_image_url')) {
                $invite_url_path = $request->file('invite_image_url')->store('events_media');
                if (isset($event)) {
                    $event->update(['invite_image_url' => $invite_url_path]
                    );
                }
            }

            DB::commit();
            return response()->json(['message' => 'Events successfully updated']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()], 400);
        }
    }

    public function destroyEvents($id)
    {
        $event = Event::find($id);
        try {
            DB::beginTransaction();

            if (isset($event)) {
                $event->delete();
            }
            DB::commit();
            return response()->json(['message' => 'Event deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Event can not be deleted ' . $e->getMessage()], 500);
        }
    }

    public function eventIndex()
    {
        $loggend_in_user = Auth::user()->load('roles');

        if ($loggend_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/event-index');
        } else if ($loggend_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/full-calendar/event-index');
        } else if ($loggend_in_user->roles[0]->name == 'marketing') {
            return view('/users/marketing-dashboard/full-calendar/event-index');
        }else if ($loggend_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/event-index');
        }else if ($loggend_in_user->roles[0]->name == 'tenant') {
            return view('/users/tenant-dashboard/event-index');
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }
    /*Delete events*/
    public function deleteEvents($id){
        DB::beginTransaction();
        $event = Event::find($id);
        $event->load('EventsMedia');

        try {
            if(isset($event->EventsMedia)){
                $event->EventsMedia()->forceDelete();
            }

            $event->forceDelete();

            DB::commit();
            return response()->json(['message' => 'Event deleted successfully'], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function getEvents()
    {
        $events = Event::with('EventVenue')->get();
        Event::chunk(200, function ($events) {
            $today = Carbon::now()->toDateString();
            foreach ($events as $event) {
                if ($event->end >= $today)
                    $event->update(['eventStatus' => 'Active']);
                else
                    $event->update(['eventStatus' => 'Inactive']);
            }
        });

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/event-show/' . $event->id;
                $del = $event->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a><a id=' . $del . ' onclick="confirm_delete_events(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/event-show/' . $event->id;
                $del = $event->id;
                return '<a href=' . $re . ' title="Edit Event" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a><a id=' . $del . ' onclick="confirm_delete_events(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/event-show/' . $event->id;
                $del = $event->id;
                return '<a href=' . $re . ' title="Edit Event" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a><a id=' . $del . ' onclick="confirm_delete_events(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/event-show/' . $event->id;
                $del = $event->id;
                return '<a href=' . $re . ' title="Edit Event" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a><a id=' . $del . ' onclick="confirm_delete_events(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'tenant') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/event-show/' . $event->id;
                $del = $event->id;
                return '<a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a>';
            })
                ->make(true);
        }
    }

    public function privateEventIndex(Event $event)
    {
        $event->load('incubatees');
        $loggend_in_user = Auth::user()->load('roles');

        if ($loggend_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/private-event-index', compact('event'));
        } else if ($loggend_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/full-calendar/private-event-index', compact('event'));
        } else if ($loggend_in_user->roles[0]->name == 'marketing') {
            return view('/users/marketing-dashboard/full-calendar/private-event-index', compact('event'));
        } else if ($loggend_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/full-calendar/private-event-index', compact('event'));
        }else if ($loggend_in_user->roles[0]->name == 'tenant') {
            return view('/users/tenant-dashboard/private-event-index', compact('event'));
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getPrivateEvents()
    {
        $events = Event::where('type', 'Private')->get();

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/private-event-show/' . $event->id;
                $del = '/event-delete-check/' . $event->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a><a href=' . $del . ' title="Delete Event" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/private-event-show/' . $event->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing' or $user->roles[0]->name == 'clerk') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/private-event-show/' . $event->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/private-event-show/' . $event->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'tenant') {
            return Datatables::of($events)->addColumn('action', function ($event) {
                $re = '/event-edit/' . $event->id;
                $sh = '/private-event-show/' . $event->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a>';
            })
                ->make(true);
        }
    }


    public function eventShow(Event $event)
    {
        $event->load('visitors', 'incubatees','internalUsers','bootcampers');

        $loggend_in_user = Auth::user()->load('roles');

        if ($loggend_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/event-visitor-index', compact('event'));
        } else if ($loggend_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/full-calendar/event-visitor-index', compact('event'));
        } else if ($loggend_in_user->roles[0]->name == 'marketing') {
            return view('/users/marketing-dashboard/full-calendar/event-visitor-index', compact('event'));
        }else if ($loggend_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/full-calendar/event-visitor-index', compact('event'));
        }else if ($loggend_in_user->roles[0]->name == 'tenant') {
            return view('/users/tenant-dashboard/event-visitor-index', compact('event'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function privateEventShow(Event $event)
    {
        $event->load('incubatees','bootcampers');

        $loggend_in_user = Auth::user()->load('roles');

        if ($loggend_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/private-event-visitor-index', compact('event'));
        } else if ($loggend_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/full-calendar/private-event-visitor-index', compact('event'));
        } else if ($loggend_in_user->roles[0]->name == 'marketing') {
            return view('/users/marketing-dashboard/full-calendar/private-event-visitor-index', compact('event'));
        } else if ($loggend_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/full-calendar/private-event-visitor-index', compact('event'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getEventVisitorsAndIncubatees(Event $event)
    {
        $event->load('visitors', 'incubatees', 'internalUsers','bootcampers','declinedBootcamperEvent');


        $all_event_visitors = [];

        if(count($event->visitors) > 0){
            foreach ($event->visitors as $visitor) {
                $cur_visitor_id = $visitor->visitor_id;
                $cur_visitor = Visitor::find($cur_visitor_id);

                if ($visitor->attended == null or $visitor->attended == false) {
                    $object = (object)['name' => $cur_visitor->first_name,
                        'surname' => $cur_visitor->last_name,
                        'email' => $cur_visitor->email,
                        'accepted'=> $visitor->accepted == false ? 'No':'Yes',
                        'date_registered' => $visitor->date_time_registered,
                        'data_cellnumber'=>isset($cur_visitor->data_cellnumber)? $cur_visitor->data_cellnumber:'Not provided',
                        'service_provider_network'=>isset($cur_visitor->service_provider_network)? $cur_visitor->service_provider_network:'Not provided',
                        'attended' => 'No', 'event_id' => $visitor->event_id];
                } else {
                    $object = (object)['name' => $cur_visitor->first_name,
                        'surname' => $cur_visitor->last_name,
                        'email' => $cur_visitor->email,
                        'date_registered' => $visitor->date_time_registered,
                        'accepted'=> $visitor->accepted == false ? 'No':'Yes',
                        'data_cellnumber'=>isset($cur_visitor->data_cellnumber)? $cur_visitor->data_cellnumber:'Not provided',
                        'service_provider_network'=>isset($cur_visitor->service_provider_network)? $cur_visitor->service_provider_network:'Not provided',
                        'attended' => 'Yes', 'event_id' => $visitor->event_id];
                }
                array_push($all_event_visitors, $object);
            }
        }


        if(count($event->incubatees) > 0){
            foreach ($event->incubatees as $incubatee) {
                $cur_incubatee_id = $incubatee->incubatee_id;
                $cur_incubatee = Incubatee::find($cur_incubatee_id);
                $cur_user_id = $cur_incubatee->user_id;
                $cur_user = User::find($cur_user_id);

                if ($incubatee->attended == null or $incubatee->attended == false) {
                    $object = (object)['name' => $cur_user->name,
                        'surname' => $cur_user->surname,
                        'email' => $cur_user->email,
                        'date_registered' => $incubatee->date_time_registered,
                        'accepted'=> $incubatee->accepted == false ? 'No':'Yes',
                        'data_cellnumber'=>isset($cur_user->data_cellnumber)? $cur_user->data_cellnumber:'Not provided',
                        'service_provider_network'=>isset($cur_user->service_provider_network)? $cur_user->service_provider_network:'Not provided',
                        'attended' => 'No','declined' => $incubatee->declined == false ? 'No':'Yes', 'event_id' => $incubatee->event_id];
                } else {
                    $object = (object)['name' => $cur_user->name,
                        'surname' => $cur_user->surname,
                        'email' => $cur_user->email,
                        'date_registered' => $incubatee->date_time_registered,
                        'attended' => $incubatee->attended == false ? 'No':'Yes',
                        'accepted'=> $incubatee->accepted == false ? 'No':'Yes',
                        'declined'=> $incubatee->declined == false ? 'No':'Yes',
                        'data_cellnumber'=>isset($cur_user->data_cellnumber)? $cur_user->data_cellnumber:'Not provided',
                        'service_provider_network'=>isset($cur_user->service_provider_network)? $cur_user->service_provider_network:'Not provided',
                        'event_id' => $incubatee->event_id];
                }
                array_push($all_event_visitors, $object);
            }
        }
        if(count($event->bootcampers) > 0){
            foreach($event->bootcampers as $bootcamper){
                $cur_bootcamper_id = $bootcamper->bootcamper_id;
                $cur_bootcamper = Bootcamper::find($cur_bootcamper_id);
                $cur_user_id = $cur_bootcamper->user_id;
                $cur_user = User::find($cur_user_id);
                $today = Carbon::now()->toDateString();

                $object = (object)[
                    'name' => $cur_user->name,
                    'surname' => $cur_user->surname,
                    'email' => $cur_user->email,
                    'date_registered' => $today,
                    'attended' => $bootcamper->attended == false ? 'No':'Yes',
                    'accepted'=> $bootcamper->accepted == false ? 'No':'Yes',
                    'declined'=> $bootcamper->declined == false ? 'No':'Yes',
                    'data_cellnumber'=>isset($cur_user->data_cellnumber)? $cur_user->data_cellnumber:'Not provided',
                    'service_provider_network'=>isset($cur_user->service_provider_network)? $cur_user->service_provider_network:'Not provided'
                ];

                array_push($all_event_visitors, $object);
            }
        }

        if(count($event->declinedBootcamperEvent) > 0){
            foreach($event->declinedBootcamperEvent as $bootcamper){
                $cur_bootcamper_id = $bootcamper->declined_bootcamper_id;
                $cur_bootcamper = DeclinedBootcamper::find($cur_bootcamper_id);
                $today = Carbon::now()->toDateString();

                $object = (object)[
                    'name' => $cur_bootcamper->name,
                    'surname' => $cur_bootcamper->surname,
                    'email' => $cur_bootcamper->email,
                    'date_registered' => $today,
                    'attended' => $bootcamper->attended == false ? 'No':'Yes',
                    'accepted'=> $bootcamper->accepted == false ? 'No':'Yes',
                    'declined'=> $bootcamper->declined == false ? 'No':'Yes',
                    'data_cellnumber'=>isset($cur_user->data_cellnumber)? $cur_user->data_cellnumber:'Not provided',
                    'service_provider_network'=>isset($cur_user->service_provider_network)? $cur_user->service_provider_network:'Not provided'
                ];

                array_push($all_event_visitors, $object);
            }
        }


        if(count($event->internalUsers) > 0){
            foreach($event->internalUsers as $internalUser){
                $user_id = $internalUser->user_id;
                $cur_user = User::find($user_id);

                if($internalUser->attended == null or $internalUser->attended == false){
                    $object = (object)['name' => $cur_user->name,
                        'surname' => $cur_user->surname,
                        'email' => $cur_user->email,
                        'accepted'=> $internalUser->accepted == false ? 'No':'Yes',
                        'data_cellnumber'=>isset($cur_user->data_cellnumber)? $cur_user->data_cellnumber:'Not provided',
                        'service_provider_network'=>isset($cur_user->service_provider_network)? $cur_user->service_provider_network:'Not provided',
                        'date_registered' => $internalUser->date_time_registered, 'attended' => 'No'];
                } else {
                    $object = (object)['name' => $cur_user->name,
                        'surname' => $cur_user->surname,
                        'email' => $cur_user->email,
                        'accepted'=> $internalUser->accepted == false ? 'No':'Yes',
                        'data_cellnumber'=>isset($cur_user->data_cellnumber)? $cur_user->data_cellnumber:'Not provided',
                        'service_provider_network'=>isset($cur_user->service_provider_network)? $cur_user->service_provider_network:'Not provided',
                        'date_registered' => $internalUser->date_time_registered, 'attended' => 'Yes'];
                }
                array_push($all_event_visitors, $object);
            }
        }

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($all_event_visitors)->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($all_event_visitors)->make(true);
        } elseif ($user->roles[0]->name == 'marketing' or $user->roles[0]->name == 'clerk') {
            return Datatables::of($all_event_visitors)->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($all_event_visitors)->make(true);
        } elseif ($user->roles[0]->name == 'tenant') {
            return Datatables::of($all_event_visitors)->make(true);
        }
    }

    public function getPrivateEventVisitors(Event $event)
    {
        $event->load('incubatees', 'internalUsers', 'bootcampers');
        $all_event_visitors = [];

        if(count($event->incubatees) > 0){
            foreach ($event->incubatees as $incubatee) {
                $cur_incubatee_id = $incubatee->incubatee_id;
                $cur_incubatee = Incubatee::find($cur_incubatee_id);
                $cur_user_id = $cur_incubatee->user_id;
                $cur_user = User::find($cur_user_id);

                if ($incubatee->attended == null or $incubatee->attended == false) {
                    $object = (object)['name' => $cur_user->name, 'surname' => $cur_user->surname, 'email' => $cur_user->email, 'contact_number' => $cur_user->contact_number,
                        'date_registered' => $incubatee->date_time_registered, 'attendance' => 'No'];
                } else {
                    $object = (object)['name' => $cur_user->name, 'surname' => $cur_user->surname, 'email' => $cur_user->email, 'contact_number' => $cur_user->contact_number,
                        'date_registered' => $incubatee->date_time_registered, 'attendance' => 'Yes'];
                }
                array_push($all_event_visitors, $object);
            }
        }

        if(count($event->bootcampers) > 0){
            foreach ($event->bootcampers as $bootcamper) {
                $cur_bootcamper_id = $bootcamper->bootcamper_id;
                $cur_bootcamper = Bootcamper::find($cur_bootcamper_id);
                $cur_user_id = $cur_bootcamper->user_id;
                $cur_user = User::find($cur_user_id);

                if ($bootcamper->attended == null or $bootcamper->attended == false) {
                    if($bootcamper->accepted == true){
                        $object = (object)['name' => $cur_user->name, 'surname' => $cur_user->surname, 'email' => $cur_user->email, 'contact_number' => $cur_user->contact_number,
                            'date_registered' => $bootcamper->date_registered, 'attendance' => 'No', 'accepted' => 'Yes'];
                    } else {
                        $object = (object)['name' => $cur_user->name, 'surname' => $cur_user->surname, 'email' => $cur_user->email, 'contact_number' => $cur_user->contact_number,
                            'date_registered' => $bootcamper->date_registered, 'attendance' => 'No', 'accepted' => 'No'];
                    }
                } else {
                    if($bootcamper->accepted != true){
                        $object = (object)['name' => $cur_user->name, 'surname' => $cur_user->surname, 'email' => $cur_user->email, 'contact_number' => $cur_user->contact_number,
                            'date_registered' => $bootcamper->date_registered, 'attendance' => 'Yes', 'accepted' => 'No'];
                    } else {
                        $object = (object)['name' => $cur_user->name, 'surname' => $cur_user->surname, 'email' => $cur_user->email, 'contact_number' => $cur_user->contact_number,
                            'date_registered' => $bootcamper->date_registered, 'attendance' => 'Yes', 'accepted' => 'Yes'];
                    }
                }
                array_push($all_event_visitors, $object);
            }
        }

        if(count($event->internalUsers) > 0){
            foreach($event->internalUsers as $internalUser){
                $user_id = $internalUser->user_id;
                $cur_user = User::find($user_id);

                if($internalUser->attended == null or $internalUser->attended == false){
                    $object = (object)['name' => $cur_user->name, 'surname' => $cur_user->surname, 'email' => $cur_user->email, 'contact_number' => $cur_user->contact_number,
                        'date_registered' => $incubatee->date_time_registered, 'attendance' => 'No'];
                } else {
                    $object = (object)['name' => $cur_user->name, 'surname' => $cur_user->surname, 'email' => $cur_user->email, 'contact_number' => $cur_user->contact_number,
                        'date_registered' => $incubatee->date_time_registered, 'attendance' => 'Yes'];
                }
                array_push($all_event_visitors, $object);
            }
        }

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($all_event_visitors)->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($all_event_visitors)->make(true);
        } elseif ($user->roles[0]->name == 'marketing' or $user->roles[0]->name == 'clerk') {
            return Datatables::of($all_event_visitors)->make(true);
        }elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($all_event_visitors)->make(true);
        }
    }

    public function deleteImage(EventMediaImage $eventMediaImage)
    {
        try {
            DB::beginTransaction();
            $eventMediaImage->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Successfully deleted the chosen image']);
        } catch (\Exception $e) {
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }



}
