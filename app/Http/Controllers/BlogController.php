<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogMedia;
use App\BlogParagraph;
use App\BlogSection;
use App\Event;
use App\Venture;
use App\VentureOwnership;
use App\VentureUpload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPUnit\Util\Json;
use Yajra\DataTables\DataTables;

class BlogController extends Controller
{
    //


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function indexBlog()
    {
        $user = Auth::user()->load('roles');


        if ($user->roles[0]->name == 'app-admin') {
            return view('Blogs.get-blogs');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.blogs.get-blog');
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.blogs.get-blog');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.blogs.get-blog');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.blogs.get-blog');
        }
    }

    public function getBlogs()
    {


        $blogs = Blog::with('BlogMedia')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-blog/' . $blog->id;
                $del = $blog->id;
                $sh = '/show-blog/' .$blog->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_blogs(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-blog/' . $blog->id;
                $del = $blog->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_blogs(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-blog/' . $blog->id;
                $del = $blog->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_blogs(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-blog/' . $blog->id;
                $del = $blog->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_blogs(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-blog/' . $blog->id;
                $del = $blog->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_blogs(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    //Create Blog
    public function create()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('Blogs.create-blog');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.blogs.create-blog');
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.blogs.create-blog');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.blogs.create-blog');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.blogs.create-blog');
        } else {
            return response('You are not authorized!');
        }
    }



    //Store paragraph and sections
    public function storeParagraphAndSection(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $paragraph_with_section_array = json_decode($input['paragraph_with_section_array']);

        $request->validate([
            'paragraph_text' => 'max:16777215',
        ]);

        try {
            foreach ($paragraph_with_section_array as $paragraph) {
                $blog_paragraph = BlogParagraph::create(['paragraph_title' => $paragraph->paragraph_title, 'paragraph_text' => $paragraph->paragraph_text,
                    'blog_section_id' => $paragraph->section_id, 'paragraph_number' => $paragraph->paragraph_number
                ]);
                $blog_paragraph->save();
            }
            DB::commit();
            return response()->json(['message' => 'Your Blog is now finalized.)!']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    //Store Section
    public function storeSection(Request $request)

    {
        DB::beginTransaction();
        $input = $request->all();
        $sections_array = json_decode($input['sections_array']);
        $new_section_array = [];

        try {
            foreach ($sections_array as $section) {
                $blog_section = BlogSection::create(['section_number' => $section->section_number, 'section_header' => $section->section_header,
                    'section_video_url' => $section->section_video_url, 'section_image_url' => $section->section_image_url, 'blog_id' => $input['blog_id']
                ]);

                array_push($new_section_array, $blog_section);
                $blog_section->save();
            }
            DB::commit();
            return response()->json(['message' => 'Your Blog section has been successfully saved.', 'sections' => $new_section_array]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    public function storeBlog(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::today();

        $request->validate([
            'description' => 'max:16777215',
        ]);

        try {
            $blog = Blog::create(['title' => $input['title'],
                'description' => $input['summernote'], 'blog_date' => $date
            ]);
            if ($request->hasFile('blog_image_url')) {
                $image_url_path = $request->file('blog_image_url')->store('incubatee_uploads');
                if (isset($blog)) {
                    $blog->update(['blog_image_url' => $image_url_path]
                    );
                } else {
                    $blog->create(['blog_image_url' => $image_url_path]
                    );
                }
            }
            $blog->save();
            DB::commit();
            return response()->json(['message' => 'Your Blog has been successfully saved.', 'blog' => $blog]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    public function index()
    {
        $blogs = Blog::all();

        return view('Blogs/Index', compact('blogs'));
    }


    public function editBlog(Blog $blog)
    {
        $blog->load('BlogMedia');

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('Blogs.edit-blog', compact('blog'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.blogs.edit-blog', compact('blog'));
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.blogs.edit-blog', compact('blog'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.blogs.edit-blog', compact('blog'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.blogs.edit-blog', compact('blog'));
        }
    }

    public function updateBlog(Request $request, Blog $blog)
    {
        $blog->load('blogMedia')->get();


        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::today();

        $request->validate([
            'description' => 'max:16777215',

        ]);

        try {
            $blog->update(['title' => $input['title'],
                'body' => $input['body'],
                'description' => $input['description'],
                'blog_date' => $date]);

            $blog->save();

            $blog->update(['video' => $input['video']]);

            if ($request->hasFile('blog_image_url')) {
                $image_url_path = $request->file('blog_image_url')->store('incubatee_uploads');
                if (isset($blog)) {
                    $blog->update(['blog_image_url' => $image_url_path]
                    );
                } else {
                    $blog->create(['blog_image_url' => $image_url_path]
                    );
                }
            }
            DB::commit();
            return response()->json(['message' => 'Successfully updated Blog']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function getSection()
    {
        $blogs = BlogSection::with('blog', 'blogParagraph')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-section/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-section/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-section/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        }elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-section/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        }elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-section/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        }
    }

    public function editSection(BlogSection $blog)
    {

        $blog->load('blogParagraph', 'blog')->get()->first();
        /*dd($blog);*/

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('Blogs.edit-section', compact('blog'));
        } elseif ($user->roles[0]->name == 'admninistrator') {
            return view('users.administrators.blogs.edit-section', compact('blog'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.blogs.edit-section', compact('blog'));
        }elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.blogs.edit-section', compact('blog'));
        }elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.blogs.edit-section', compact('blog'));
        }
    }

    public function updateSection(Request $request, BlogSection $blogSection)
    {
        DB::beginTransaction();
        $input = $request->all();

        $blogSection->load('blogParagraph', 'blog');
        /*return $blogSection;*/

        try {

            $blogSection->update(['section_number' => $input['section_number'],
                'section_header' => $input['section_header'],
                'section_video_url' => $input['section_video_url']]);


            if ($request->hasFile('section_image_url')) {
                $section_image_url_path = $request->file('section_image_url')->store('events_media');
                if (isset($blogSection)) {
                    $blogSection->update(['section_image_url' => $section_image_url_path]);
                } else {
                    $blogSection->create(['section_image_url' => $section_image_url_path]
                    );
                }
            }
            DB::commit();
            return response()->json(['message' => 'Successfully Updated Section']);
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }
    }

    public function S()
    {
        $blogs = BlogParagraph::with('blogSection')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-paragraph/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-paragraph/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-paragraph/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        }elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-paragraph/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        }elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-paragraph/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Section" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);

        }
    }

    public function editParagraph(BlogParagraph $blog)
    {
        $blog->load('blogParagraph', 'blog')->get();

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('Blogs.edit-paragraph', compact('blog'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.blogs.edit-paragraph', compact('blog'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.blogs.edit-paragraph', compact('blog'));
        }elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.blogs.edit-paragraph', compact('blog'));
        }elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.blogs.edit-paragraph', compact('blog'));
        }
    }

    public function updateParagraph(Request $request, BlogParagraph $blogParagraph)
    {

        DB::beginTransaction();
        $input = $request->all();

        $blogParagraph->load('blogSection');

        try {

            $blogParagraph->update(['paragraph_number' => $input['paragraph_number'],
                'paragraph_title' => $input['paragraph_title'],
                'paragraph_text' => $input['paragraph_text']]);

            $blogParagraph->save();
            DB::commit();
            return response()->json(['message' => 'Successfully Updated Paragraph']);
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }
    }

    public function destroyBlog($id)
    {
        try {
            DB::beginTransaction();
            $blog = Blog::find($id);

            $blog->load('blogSection');

            foreach ($blog->blogSection as $section) {
                $section->load('blogParagraph');
                foreach ($section->blogParagraph as $paragraph) {
                    $paragraph->forceDelete();
                }
                $section->forceDelete();
            }
            $blog->forceDelete();
            DB::commit();
            return view('Blogs.get-blogs');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }

    }
}
