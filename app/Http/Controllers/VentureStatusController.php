<?php

namespace App\Http\Controllers;

use App\VentureStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class VentureStatusController extends Controller
{
    public function ventureStatusIndex(){
//        $user = Auth::user()->load('roles');
//
//        if($user->roles[0]->name == 'app-admin'){
            return view('users.venture.venture-status-index');
//        } elseif ($user->roles[0]->name == 'administrator'){
//            return view('users.administrators.ventures.venture-status-index');
//        } else {
//            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
//        }
    }
    public function createVentureStatus(){
       return view('users.venture.create-venture-status');
    }

    public function storeVentureStatus(Request $request){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $venture_status = VentureStatus::create(['status_name' => $input['status_name']]);
            DB::commit();

            return response()->json(['message' => 'Venture status added.']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function getVentureStatuses()
    {
        $venture_statuses = VentureStatus::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($venture_statuses)->addColumn('action', function ($venture_status) {
                $re = '/venture-status-edit/' . $venture_status->id;
                $del = $venture_status->id;
                return '<a href=' . $re . ' title="Edit Venture Status" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture_status(this)" title="Delete Venture Status" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($venture_statuses)->addColumn('action', function ($venture_status) {
                $re = '/venture-status-edit/' . $venture_status->id;
                return '<a href=' . $re . ' title="Edit Venture Status" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateVentureStatus(Request $request, VentureStatus $ventureStatus){
        $input = $request->all();

        try{
            DB::beginTransaction();
            $ventureStatus->update(['status_name' => $input['status_name']]);
            $ventureStatus->save();
            DB::commit();
            return response()->json(['message' => 'Venture status updated.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function editVentureStatus(VentureStatus $ventureStatus){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.edit-venture-status', compact('ventureStatus'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.edit-venture-status', compact('ventureStatus'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function deleteVentureStatus(VentureStatus $ventureStatus){
        try{
            $ventureStatus->forceDelete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }
}
