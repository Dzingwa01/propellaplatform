<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionsCategory;
use App\User;
use App\Question;
use App\QuestionType;
use App\QuestionSubText;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     //CATEGORIES CODE
    public function createQuestionCategory(){

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users/app-admin/questions/create-question-category');
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users/administrators/questions/create-question-category');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function storeQuestionCategory(Request $request){
        DB::beginTransaction();
        $input = $request->all();
        $user = Auth::user()->load('roles');

        try{
            $create_category = QuestionsCategory::create(['category_name' => $input['category_name'],'category_description' => $input['category_description'],
                'user_id' => $user->id]);
            if ($request->hasFile('category_image_url')) {
                $image_url_path = $request->file('category_image_url')->store('incubatee_uploads');
                if (isset($create_category)) {
                    $create_category->update(['category_image_url' => $image_url_path]
                    );
                } else {
                    $create_category->create(['category_image_url' => $image_url_path]
                    );
                }
            }
            $create_category->save();


            DB::commit();
            return response()->json(['message'=>'Category added successfully']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }
    }

    public function categoryIndex()
    {
        $category = QuestionsCategory::all();

        return view('users/app-admin/questions/question-category-index', compact('category'));
    }

    public function editQuestionCategory(QuestionsCategory $QuestionsCategory)
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users/app-admin/questions/edit-question-category', compact('QuestionsCategory'));
        }elseif ($user->roles[0]->name=='administrator'){
            return view('users/administrators/questions/edit-question-category', compact('QuestionsCategory'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getCategory(){

        $category = QuestionsCategory::all();
        $user = Auth::user()->load('roles');

        if($user->roles[0]->name=='app-admin') {
            return Datatables::of($category)->addColumn('action', function ($category) {
                $del = '/category/delete/' . $category->id;
                $edit = 'edit-question-category/' . $category->id;
                return '<a href=' . $edit . ' title="Edit Category" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name=='administrator'){
            return Datatables::of($category)->addColumn('action', function ($category) {
                $del = '/category/delete/' . $category->id;
                $edit = 'edit-question-category/' . $category->id;
                return '<a href=' . $edit . ' title="Edit Category" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function updateCategory(Request $request, QuestionsCategory $QuestionsCategory)
    {
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'administrator') {
            try {
                $QuestionsCategory->update(['category_name' => $input['category_name'],'category_description' => $input['category_description']]);

                $QuestionsCategory->save();
                if ($request->hasFile('category_image_url')) {
                    $image_url_path = $request->file('category_image_url')->store('incubatee_uploads');
                    if (isset($QuestionsCategory)) {
                        $QuestionsCategory->update(['category_image_url' => $image_url_path]
                        );
                    }
                }
                DB::commit();
                return response()->json(['message' => 'Category updated successfully.', 'QuestionsCategory' => $QuestionsCategory], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Category could not be saved at the moment ' . $e->getMessage(),'QuestionsCategory'=>$QuestionsCategory], 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function destroyCategory($id)
    {

        $QuestionsCategory = QuestionsCategory::find($id);
        $QuestionsCategory->delete();

        return view('users/app-admin/questions/question-category-index');
    }

    //QUESTIONS CODE

    public function createQuestions()
    {
        $questionCategories = QuestionsCategory::orderBy('category_name', 'asc')->get();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users/app-admin/questions/upload-questions', compact('questionCategories'));

        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users/administrators/questions/upload-questions', compact('questionCategories'));
        }
    }

    public function storeQuestions(Request $request)
    {
        $input = $request->all();
        $sub_text_array = json_decode($input['sub_text_array']);

        DB::beginTransaction();

        try {
            $question = Question::create(['question_text' => $input['question_text'],
                'question_category_id' => $input['question_category_id'],
                'question_number' => $input['question_number'],
                'question_type' => $input['question_type']]);
            $question->save();

            foreach ($sub_text_array as $sub_text){

                $question->questionSubTexts()->create(['question_id', $question->id,'question_sub_text' => $sub_text->question_sub_text]);

            }


            DB::commit();

            return response()->json(['message' => 'Question added successfully', 'questions' => $question]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function getQuestions()
    {
        $questions = Question::all();
        $question_cat_array = [];

        foreach($questions as $question){
            $question->load('category');
            $question_category  = $question->category;

            $object = (object)[
                'question_number' => isset($question) ? $question->question_number: 'No number',
                'question_text' => isset($question) ? $question->question_text: 'No text',
                'category_name' => isset($question_category) ? $question_category->category_name: 'No category',
                'id' => $question->id
            ];
            array_push($question_cat_array, $object);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($question_cat_array)->addColumn('action', function ($question) {
                $re = '/editQuestions/' . $question->id;
                $del = '/question/delete/' . $question->id;
                return '<a href=' . $re . ' title="Edit Question" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete Question" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif($user->roles[0]->name == 'administrator'){
            return Datatables::of($question_cat_array)->addColumn('action', function ($question) {
                $re = '/editQuestions/' . $question->id;
                $del = '/question/delete/' . $question->id;
                return '<a href=' . $re . ' title="Edit Question" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete Question" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function questionIndex()
    {
        $question = Question::all()->load('category');

        return view('users/app-admin/questions/question-index', compact('question'));
    }

    public function editQuestions(Question $Question)
    {

        $user = Auth::user()->load('roles');
        $Question->load('questionSubTexts');
        $questionCategories = QuestionsCategory::orderBy('category_name', 'asc')->get();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users/app-admin/questions/edit-questions', compact('Question','questionCategories'));
        }elseif($user->roles[0]->name == 'administrator'){
            return view('users/administrators/questions/edit-questions', compact('Question','questionCategories','questionSub'));

        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateQuestions(Request $request, Question $Question)
    {
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();
        $Question->load('questionSubTexts');


        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'administrator') {
            try {
                $Question->update(['question_text' => $input['question_text'],
                    'question_number' => $input['question_number'],'question_category_id' => $input['question_category_id'],
                    'question_type' => $input['question_type']]);

                if(isset($Question->questionSubTexts)){
                    foreach($Question->questionSubTexts as $questionSubText){
                        $questionSubText->update(['question_sub_text' =>$input['question_sub_text']]);
                    }
                }

                $Question->save();

                DB::commit();
                return response()->json(['message' => 'Question updated successfully.', 'Question' => $Question], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Question could not be saved at the moment ' . $e->getMessage(),'Question'=>$Question], 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function destroyQuestion(Question $question)
    {
        $question->load('declined_applicant_question_answers','referred_applicant_question_answer',
            'userQuestionAnswers');
        try{
            DB::beginTransaction();
            foreach ($question->declined_applicant_question_answers as $declined){
                $declined->forceDelete();
            }
            foreach ($question->referred_applicant_question_answer as $referred){
                $referred->forceDelete();
            }
            foreach ($question->userQuestionAnswers as $user){
                $user->forceDelete();
            }
            $question->forceDelete();
            DB::commit();
            return redirect('questionIndex');
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
        /*try {
            DB::beginTransaction();
            $Question = Question::find($id);

            $Question->load('questionSubTexts');

            foreach ($Question->questionSubTexts as $subText) {
                $subText->forceDelete();
            }

            $Question->forceDelete();
            DB::commit();
            return view('users/app-admin/questions/question-index');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }*/

    }


}
