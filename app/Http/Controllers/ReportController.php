<?php

namespace App\Http\Controllers;

use App\Event;
use App\Exports\CustomSQLExport;
use App\Exports\DetailedBootcamperReportExport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    //Global logged in user var
    protected $loggedInUser;

    public function showReports(){
        //Access global logged in user var and load user roles
        $this->loggedInUser = Auth::user()->load('roles');

        if($this->loggedInUser->roles[0]->name == 'app-admin'
            or $this->loggedInUser->roles[0]->name == 'administrator'
            or $this->loggedInUser->roles[0]->name == 'marketing'){
            return view('/reports/reports');
        } else {
            return response('You are not authorized !!');
        }
    }

    public function showBootcamperReports(){
        //Access global logged in user var and load user roles
        $this->loggedInUser = Auth::user()->load('roles');

        if($this->loggedInUser->roles[0]->name == 'app-admin'
            or $this->loggedInUser->roles[0]->name == 'administrator'
            or $this->loggedInUser->roles[0]->name == 'marketing'){
            //Get all bootcamp events, order descending based on start date
            $bootcampEvents = Event::where('title', 'like', 'ICT 22%%')
                ->orderBy('start', 'desc')
                ->get();

            return view('/reports/bootcamper-reports', compact('bootcampEvents'));
        } else {
            return response('You are not authorized !!');
        }
    }

    public function downloadDetailedBootcamperReport(Event $event){
        return Excel::download(new DetailedBootcamperReportExport($event), $event->title .' - Report.xlsx');
    }

    public function sendPersonalizedScript($script){
        $results = DB::select($script);
        return Excel:: download(new CustomSQLExport($results), 'CustomReport.xlsx');
    }
}
