<?php

namespace App\Http\Controllers;

use App\Blog;
use App\CompanyEmployeeCovidForm;
use App\Enquiry;
use App\Event;
use App\PropellaVenue;
use App\Role;
use App\StaffCovidSymptom;
use App\User;
use App\UserVenueBooking;
use App\VisitorCovidSymptom;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;
use Calendar;

class TabletController extends Controller
{
    public function showVenueBookingsOnTablet()
    {
        $roles = Role::where('name','!=','tablet')->get();
        return view('users.tablet.show-venue-on-tablet', compact('roles'));
    }

    public  function geAllVenuesOnTablet(){

        $venue = UserVenueBooking::all();
        $user = Auth::user()->load('roles');


        if($user->roles[0]->name=='tablet'){
            return Datatables::of($venue)->addColumn('action', function ($enquiry) {
                $sh = '/home/enquiry-show/' . $enquiry->id;
                $del = '/enquiry/delete/' . $enquiry->id;
                return '<a href=' . $sh . ' title="Show enquiry" style="color:green!important;"><i class="material-icons">visibility</i></a><a href=' . $del . ' onclick="confirm_delete_enquiry(this)" title="Delete enquiry" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function indexTablet()
    {
        $venue = UserVenueBooking::all();

        return view('users.tablet.show-venue-on-tablet', compact('venue'));

    }

    public function viewDailyCovidForms(){
        $today = Carbon::now()->toDateString();
        $covidStff = StaffCovidSymptom::with('user')->where('date',$today)->get();

        $covidStff_array = [];

        foreach ($covidStff as $visitor_arrays) {
            $visitor_arrays->load('user')->get();

            $object = (object)[
                'staff_covid_symptoms_one' => isset($visitor_arrays) ? $visitor_arrays->staff_covid_symptoms_one: 'not selected',
                'staff_covid_symptoms_two' => isset($visitor_arrays) ? $visitor_arrays->staff_covid_symptoms_two: 'not selected',
                'staff_health_declaration' => isset($visitor_arrays) ? $visitor_arrays->staff_health_declaration: 'not selected',
                'staff_declaration' => isset($visitor_arrays) ? $visitor_arrays->staff_declaration: 'not selected',
                'staff_temperature' => isset($visitor_arrays) ? $visitor_arrays->staff_temperature: 'not temperature',
                'staff_digitally_signed_declaration' => isset($visitor_arrays) ? $visitor_arrays->staff_digitally_signed_declaration: 'not selected',
                'date' => $visitor_arrays->date,
                'name' => isset($visitor_arrays->user) ? $visitor_arrays->user->name: 'no name',
                'surname' => isset($visitor_arrays->user) ? $visitor_arrays->user->surname: 'no surname',
                'email' => isset($visitor_arrays->user) ? $visitor_arrays->user->email: 'no email',
                'id' => $visitor_arrays->id];
            array_push($covidStff_array, $object);
        }

        $covidVisitor = VisitorCovidSymptom::with('visitor')->where('date_visited',$today)->get();

        $visitor_array = [];

        foreach ($covidVisitor as $visitor_arrays) {
            $visitor_arrays->load('visitor')->get();

            $object = (object)[
                'covid_symptoms_one' => isset($visitor_arrays) ? $visitor_arrays->covid_symptoms_one: 'not selected',
                'covid_symptoms_two' => isset($visitor_arrays) ? $visitor_arrays->covid_symptoms_two: 'not selected',
                'health_declaration' => isset($visitor_arrays) ? $visitor_arrays->health_declaration: 'not selected',
                'declaration' => isset($visitor_arrays) ? $visitor_arrays->declaration: 'not selected',
                'temperature' => isset($visitor_arrays) ? $visitor_arrays->temperature: 'not temperature',
                'digitally_signed_declaration' => isset($visitor_arrays) ? $visitor_arrays->digitally_signed_declaration: 'not selected',
                'date_visited' => $visitor_arrays->date_visited,
                'first_name' => isset($visitor_arrays->visitor) ? $visitor_arrays->visitor->first_name: 'no name',
                'last_name' => isset($visitor_arrays->visitor) ? $visitor_arrays->visitor->last_name: 'no surname',
                'email' => isset($visitor_arrays->visitor) ? $visitor_arrays->visitor->email: 'no email',
                'id' => $visitor_arrays->id];
            array_push($visitor_array, $object);
        }

        $covidEmployee = CompanyEmployeeCovidForm::with('companyEmployees')->where('date',$today)->get();
        $users_array = [];
        foreach ($covidEmployee as $covidform){
            $covidform->load('companyEmployees');
            $object = (object)[
                'name' => $covidform->companyEmployees->name,
                'surname' => $covidform->companyEmployees->surname,
                'email' => $covidform->companyEmployees->email,
                'employee_temperature'=>$covidform->employee_temperature,
                'employee_covid_symptoms_one'=>$covidform->employee_covid_symptoms_one,
                'employee_covid_symptoms_two'=>$covidform->employee_covid_symptoms_two,
                'employee_health_declaration'=>$covidform->employee_health_declaration,
                'employee_declaration'=>$covidform->employee_declaration,
                'employee_digitally_signed_declaration'=>$covidform->employee_digitally_signed_declaration,
                'date'=>$covidform->date,
                'id' => $covidform->id,
            ];
            array_push($users_array, $object);
        }


        return view('users.tablet.view-signed-covid-users',compact('visitor_array','covidVisitor','covidStff','covidStff_array',
        'today','users_array','covidEmployee'));
    }

}
