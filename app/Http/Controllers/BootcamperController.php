<?php

namespace App\Http\Controllers;

use App\ApplicantContactLog;
use App\ApplicantPanelist;
use App\Bootcamper;
use App\BootcamperContactLog;
use App\BootcamperDataUpload;
use App\BootcamperPanelist;
use App\DeclinedApplicant;
use App\DeclinedApplicantQuestionAnswer;
use App\DeclinedBootcamper;
use App\DeclinedBootcamperContactLog;
use App\DeclinedBootcamperEvent;
use App\DeclinedBootcamperPanelist;
use App\DeclinedBootcamperPanelistQuestionAnswers;
use App\DeclinedBootcamperQuestionAnswer;
use App\Event;
use App\EventBootcamper;
use App\IctPanelistQuestionAnswer;
use App\Incubatee;
use App\IncubateeStage;
use App\IncubateeUpload;
use App\Panelist;
use App\Question;
use App\QuestionsCategory;
use App\Role;
use App\User;
use App\UserQuestionAnswer;
use App\VentureCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Postmark\PostmarkClient;
use Tightenco\Collect\Support\Arr;
use Yajra\DataTables\DataTables;

class BootcamperController extends Controller
{

    //Bootcampers Section
    public function bootcampersIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/bootcampers/bootcampers-index');
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/bootcampers/bootcampers-index');
        } elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/bootcampers/bootcampers-index');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getBootcampers(){
        $logged_in_user = Auth::user()->load('roles');
        $bootcampers = Bootcamper::with('user', 'ventureCategory','events')
            ->whereNull('is_incubatee')
            ->get();
        $bootcampers_array = [];

        foreach($bootcampers as $bootcamper){
            $object = (object)[
                'name' => isset($bootcamper->user) ? $bootcamper->user->name : 'No name',
                'surname' => isset($bootcamper->user) ? $bootcamper->user->surname : 'No surname',
                'pitch_video_link' => isset($bootcamper->pitch_video_link) ? 'Yes' : 'No',
                'email' => isset($bootcamper->user) ? $bootcamper->user->email : 'No email',
                'contact_number' => isset($bootcamper->user->contact_number) ? $bootcamper->user->contact_number : 'No cell',
                'id_document_url' => isset($bootcamper->id_document_url) ? 'Yes' : 'No',
                'category_name' => isset($bootcamper->ventureCategory) ? $bootcamper->ventureCategory->category_name : 'No category',
                'cipc' => isset($bootcamper->cipc) ? 'Yes' : 'No',
                'proof_of_address' => isset($bootcamper->proof_of_address) ? 'Yes' : 'No',
                'id' => $bootcamper->id,
                'date_joined' => $bootcamper->created_at->toDateString()
            ];
            array_push($bootcampers_array, $object);
        }


        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'advisor'
            or $logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($bootcampers_array)->addColumn('action', function ($bootcamper) {
                $sh = '/bootcamper-show/' . $bootcamper->id;
                $events = '/bootcamper-events/' . $bootcamper->id;
                $declined_applicant = '/bootcamper-to-declined-applicant-view/' . $bootcamper->id;
                $edit= '/bootcamper-edit-basic/' . $bootcamper->id;
                $delete = $bootcamper->id;
                return '<a href=' . $sh . ' title="View Bootcamper"><i class="material-icons">remove_red_eye</i></a>
                <a href=' . $events . ' title="Bootcamper Events"><i class="material-icons" style="color:forestgreen;">date_range</i></a>
                <a href=' . $declined_applicant . ' title="Bootcamper Declined Applicant"><i class="material-icons" style="color:orangered;">assignment_return</i></a>
                <a href=' . $edit . ' title="Edit Bootcamp Basic Info" style="color:green!important;"><i class="material-icons">create</i></a>
                <a id=' . $delete . ' onclick="confirm_delete_bootcamper(this)" title="Delete Bootcamper" style="color:red; cursor: pointer;"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response('You are not authorized!');
        }
    }

    //Declined Bootcampers Index
    public function declinedBootcampersIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/bootcampers/declined-bootcampers-index');
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/bootcampers/declined-bootcampers-index');
        } elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/bootcampers/declined-bootcampers-index');
        } else {
            return response("You are not authorized!!");
        }
    }

    //Get the declined bootcampers from the database and return them to a view in a datatable format
    public function getDeclinedBootcampers(){
        $logged_in_user = Auth::user()->load('roles');
        $declined_bootcampers = DeclinedBootcamper::all();
        $declined_bootcampers->load('declined_bootcamper_venture_category');
        $declined_bootcampers_array = [];

        foreach($declined_bootcampers as $declined_bootcamper){
            $venture_category = $declined_bootcamper->declined_bootcamper_venture_category->category_name;
            $object = (object)['name' => $declined_bootcamper->name, 'surname' => $declined_bootcamper->surname,
                'email' => $declined_bootcamper->email, 'contact_number' => $declined_bootcamper->contact_number,
                'category_name' => $venture_category, 'id' => $declined_bootcamper->id,
                'date_declined' => $declined_bootcamper->created_at->toDateString(),
                'declined_reason'=>$declined_bootcamper->declined_reason];

            array_push($declined_bootcampers_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'advisor'
            or $logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($declined_bootcampers_array)->addColumn('action', function ($declined_bootcamper) {
                $sh = '/declined-bootcamper-account-overview/' . $declined_bootcamper->id;
                $edit = '/edit-declined-bootcamper-info/' .$declined_bootcamper->id;
                $delete = $declined_bootcamper->id;
                return '<a href=' . $sh . ' title="Bootcamper Overview"><i class="material-icons" style="color: green;">remove_red_eye</i></a><a href=' . $edit . ' title="Edit declined bootcamper" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $delete . ' onclick="confirm_delete_declined_applicant(this)" title="Delete Declined Bootcamper"><i class="material-icons" style="color:red">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response('You are not authorized!');
        }
    }

    //Edit declined Bootcamper info
    public function editDeclinedBootcamperInfo(DeclinedBootcamper $declinedBootcamper){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/bootcampers/edit-declined-bootcamper-info',compact('declinedBootcamper'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/bootcampers/edit-declined-bootcamper-info',compact('declinedBootcamper'));
        }elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/administrators/bootcampers/edit-declined-bootcamper-info',compact('declinedBootcamper'));
        }
    }

    //Update Declined bootcamper
    public function declinedBootcamperUpdateBasicInfo(Request $request, DeclinedBootcamper $declinedBootcamper){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $declinedBootcamper->update(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'],'race'=> $input['race'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'],
                'gender' => $input['gender'],'declined_reason'=>$input['declined_reason'],
                'contacted_via'=>$input['contacted_via']]);
            DB::commit();
            return response()->json(['message' => 'Your details have successfully been updated ']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Delete Declined Bootcamper
    public function deleteDeclinedBootcamper(DeclinedBootcamper $declinedBootcamper){
        try{
            $declinedBootcamper->forceDelete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Bootcamper next step
    public function bootcamperNextStepsView(Bootcamper $bootcamper){
        $bootcamper->load('user');
        $logged_in_user = Auth::user()->load('roles');
        $today = Carbon::now()->toDateString();
        $venture_categories = VentureCategory::all();
        $incubatee_stages = IncubateeStage::all();

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.app-admin.bootcampers.bootcamper-next-step', compact('bootcamper', 'venture_categories', 'incubatee_stages'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.bootcampers.bootcamper-next-step', compact('bootcamper', 'venture_categories', 'incubatee_stages'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Admin decline bootcamper panel interview
    public function adminDeclinePanelInterview(Request $request, User $user)
    {
        $user->load('applicant', 'userQuestionAnswers','bootcamper');
        $input = $request->all();
        $user_applicant = $user->applicant;
        $user_question_answers = $user->userQuestionAnswers;

        try {
            DB::beginTransaction();

            if($request->has('referred_company')){
                $create_declined_applicant = DeclinedApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,
                    'code' => $user->code, 'declined' => true, 'referred' => true, 'chosen_category' => $user_applicant->chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'],
                    'venture_name' => $user_applicant->venture_name, 'referred_company' => $input['referred_company']]);
            } else {
                $create_declined_applicant = DeclinedApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,
                    'code' => $user->code, 'declined' => true, 'chosen_category' => $user_applicant->chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'],
                    'venture_name' => $user_applicant->venture_name]);
            }


            foreach ($user_question_answers as $question_answer) {
                $create_declined_applicant_question_answer = DeclinedApplicantQuestionAnswer::create(['d_a_id' => $create_declined_applicant->id,
                    'question_id' => $question_answer->question_id, 'answer_text' => $question_answer->answer_text]);
                $question_answer->forceDelete();
            }
            $user_applicant->forceDelete();
            $user->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Bootcamper was removed from the applicants list and set as declined applicant.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()], 200);
        }
    }

    //Set bootcamper to incubatee
    public function setBootcamperToIncubatee(Request $request, Bootcamper $bootcamper){
        $input = $request->all();
        $category_id = $input['category_id'];
        $stage_id = $input['stage_id'];
        $venture_category = VentureCategory::find($category_id);
        $incubatee_stage = IncubateeStage::find($stage_id);
        $bootcamper->load('user');
        $user = $bootcamper->user;
        $user->load('applicant');
        $bootcamper_applicant = $user->applicant;
        $incubatee_role = Role::where('name', 'incubatee')->first();
        $today = Carbon::today()->toDateString();

        try{
            DB::beginTransaction();
            $bootcamper->update(['is_incubatee' => true]);
            $bootcamper->save();

                $user->roles()->sync($incubatee_role->id);

                    $create_incubatee = Incubatee::create(['user_id' => $user->id, 'startup_name' => $bootcamper_applicant->venture_name, 'company_name' => $bootcamper_applicant->venture_name,
                        'contact_number' => $user->contact_number, 'physical_address_1' => $user->address_one, 'physical_address_2' => $user->address_two,
                        'physical_address_3' => $user->address_three, 'physical_city' => $user->city, 'physical_code' => $user->code, 'incubatee_start_date' => $today,
                        'venture_category_id' => $venture_category->id]);

                    $create_incubatee_upload = IncubateeUpload::create(['id_document' => $bootcamper->id_document_url,
                        'incubatee_id' => $create_incubatee->id]);

                    $create_incubatee->stages()->attach($incubatee_stage->id);

            DB::commit();
            return response()->json(['message' => 'Bootcamper now given an incubatee role.'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Update panel interview
    public function adminUpdatePanelInterviewDetails(Request $request, Bootcamper $bootcamper){
        $input = $request->all();
        $bootcamper->load('panelists');
        $bootcamper_panelists = $bootcamper->panelists;

        try{
            DB::beginTransaction();
            $bootcamper->update(['contacted_via' => $input['contact'],
                'result_of_contact' => $input['contact_result']]);

            if(count($bootcamper_panelists) > 0){
                foreach ($bootcamper_panelists as $applicant_panelist){
                    $applicant_panelist->panel_selection_date = $input['date'];
                    $applicant_panelist->panel_selection_time = $input['time'];
                    $applicant_panelist->question_category_id = $input['question_category_id'];
                    $applicant_panelist->save();
                }
            }

            DB::commit();
            return response()->json(['message' => 'Updated successfully!']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: '.$e->getMessage()], 400);
        }
    }

    //Append Panel
    public function adminAddPanelistFromPanelAccessWindow(Request $request){
        $input = $request->all();
        $bootcamper_id = $input['bootcamper_id'];
        $panelist_role = Role::where('name', 'panelist')->first();
        $bootcamper = Bootcamper::find($bootcamper_id);
        $bootcamper->load('panelists');

        try{
            DB::beginTransaction();
            //See if the actual user exists
            $check_user = User::where('email', $input['email'])->first();

            //if user does exist
            if (isset($check_user)) {
                //load relationships to be used
                $check_user->load('roles');
                //boolean to see if the user has panelist role
                $check_role = false;

                //check if user has role panelist
                foreach ($check_user->roles as $user_role) {
                    if ($user_role->name == 'panelist') {
                        $check_role = true;
                    }
                }

                //if user has role panelist, do nothing, else attach role
                if ($check_role == false) {
                    $check_user->roles()->attach($panelist_role->id);
                }

                //Variable to see if the panelist object with the same user id already exists
                $check_panelist = $check_user->panelist()->exists();;

                //Do if statement to see if the panelist with user id exists, else create
                if ($check_panelist == true) {
                    $cur_panelist = $check_user->panelist;
                    $check_panelist_applicants = $cur_panelist->bootcamper()->exists();

                    if ($check_panelist_applicants == true) {
                        $cur_panelist_applicants = $cur_panelist->bootcamper;

                        //boolean to see if panelist has applicant
                        $has_applicant = false;

                        //loop through panelist applicants to see if the applicant id matches the current applicant id
                        foreach ($cur_panelist_applicants as $panelist_applicant) {
                            if ($panelist_applicant->bootcamper_id == $bootcamper->id) {
                                $has_applicant = true;
                            }
                        }

                        //if panelist does not have current applicant, create applicantpanelist object
                        if ($has_applicant == false) {
                            $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $cur_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                                'question_category_id' => $bootcamper->panelists[0]->question_category_id]);
                        } else {
                            $get_object = DB::table('bootcamper_panelists')->where('bootcamper_id', $bootcamper->id)
                                ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                    'panel_selection_time' => $input['panel_selection_time'],
                                    'question_category_id' => $bootcamper->panelists[0]->question_category_id]);
                        }
                    } else {
                        $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $cur_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                            'question_category_id' => $bootcamper->panelists[0]->question_category_id]);
                    }
                } else {
                    $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                    $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $create_panelist->id,
                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                        'question_category_id' => $bootcamper->panelists[0]->question_category_id]);
                }
            } else {
                $create_user = User::create(['email' => $input['email'], 'password' => Hash::make('secret')]);
                $create_user->roles()->attach($panelist_role->id);
                $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $create_panelist->id,
                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                    'question_category_id' => $bootcamper->panelists[0]->question_category_id]);
            }
            DB::commit();
            return response()->json(['message' => 'Successfully added panelist'], 200);
        } catch (\Exception $e){
            return response()->json(['message' => 'Something went wrong: '. $e->getMessage()], 400);
        }
    }

    public function adminRemoveBootcamperPanelistFromPanel(BootcamperPanelist $bootcamperPanelist){
        try{
            DB::beginTransaction();
            $bootcamperPanelist->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Successfully removed panelist'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: '. $e->getMessage()], 400);
        }
    }

    //Bootcamper Panel Access Window
    public function showBootcamperPanelAccessWindow(Bootcamper $bootcamper)
    {
        $logged_in_user = Auth::user()->load('roles');
        $bootcamper->load('panelists','user');

        $bootcamper_panelists = [];
        $question_categories = QuestionsCategory::all();

        if(count($bootcamper->panelists) > 0){
            foreach($bootcamper->panelists as $bootcamper_panelist){
                $bootcamper_panelist->load('panelist');
                $panelist = $bootcamper_panelist->panelist;
                $panelist->load('user');
                $panelist_user = $panelist->user;

                $object = (object)['name' => $panelist_user->name, 'surname' => $panelist_user->surname,
                    'title' => $panelist_user->title, 'initials' => $panelist_user->initials,
                    'company_name' => $panelist_user->company_name, 'position' => $panelist_user->position,
                    'email' => $panelist_user->email, 'contact_number' => $panelist_user->contact_number,
                    'bootcamper_panelist_id' => $bootcamper_panelist->id];

                array_push($bootcamper_panelists, $object);
            }
        }
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.bootcampers.bootcamper-access-panel-window',compact('bootcamper','bootcamper_panelists','question_categories'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Admin approve bootcamper panel interview
    public function adminApproveBootcamperPanelInterview(Request $request, Bootcamper $bootcamper)
    {
        $input = $request->all();
        $selected_panelists = json_decode($input['selected_panelists']);
        $guest_panelists = json_decode($input['guest_panelists']);
        $panelist_role = Role::where('name', 'panelist')->first();
        $question_category_id = $input['question_category_id'];

        try {
            DB::beginTransaction();

            $bootcamper->update(['contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact']]);

            if(count($selected_panelists) > 0){
                foreach ($selected_panelists as $selected_panelist) {
                    //See if the actual user exists
                    $check_user = User::where('email', $selected_panelist)->first();

                    //if user does exist
                    if (isset($check_user)) {
                        //load relationships to be used
                        $check_user->load('roles');
                        //boolean to see if the user has panelist role
                        $check_role = false;

                        //check if user has role panelist
                        foreach ($check_user->roles as $user_role) {
                            if ($user_role->name == 'panelist') {
                                $check_role = true;
                            }
                        }

                        //if user has role panelist, do nothing, else attach role
                        if ($check_role == false) {
                            $check_user->roles()->attach($panelist_role->id);
                        }

                        //Variable to see if the panelist object with the same user id already exists
                        $check_panelist = $check_user->panelist()->exists();;

                        //Do if statement to see if the panelist with user id exists, else create
                        if ($check_panelist == true) {
                            $cur_panelist = $check_user->panelist;
                            $check_panelist_applicants = $cur_panelist->bootcamper()->exists();

                            if ($check_panelist_applicants == true) {
                                $cur_panelist_applicants = $cur_panelist->applicants;

                                //boolean to see if panelist has applicant
                                $has_applicant = false;

                                //loop through panelist applicants to see if the applicant id matches the current applicant id
                                foreach ($cur_panelist_applicants as $panelist_applicant) {
                                    if ($panelist_applicant->bootcamper_id == $bootcamper->id) {
                                        $has_applicant = true;
                                    }
                                }

                                //if panelist does not have current applicant, create applicantpanelist object
                                if ($has_applicant == false) {
                                    $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $cur_panelist->id,
                                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                                } else {
                                    $get_object = DB::table('bootcamper_panelists')->where('bootcamper_id', $bootcamper->id)
                                        ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                            'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                                }
                            } else {
                                $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            }
                        } else {
                            $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                            $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $create_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                        }
                    } else {
                        $create_user = User::create(['email' => $selected_panelist, 'password' => Hash::make('secret')]);
                        $create_user->roles()->attach($panelist_role->id);
                        $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                        $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                    }
                }
            }


            if(count($guest_panelists) > 0){
                foreach ($guest_panelists as $guest_panelist) {
                    $check_user = User::where('email', $guest_panelist->email)->first();

                    //if user does exist
                    if (isset($check_user)) {
                        //load relationships to be used
                        $check_user->load('roles');
                        //boolean to see if the user has panelist role
                        $check_role = false;

                        //check if user has role panelist
                        foreach ($check_user->roles as $user_role) {
                            if ($user_role->name == 'panelist') {
                                $check_role = true;
                            }
                        }

                        //if user has role panelist, do nothing, else attach role
                        if ($check_role == false) {
                            $check_user->roles()->attach($panelist_role->id);
                        }

                        //Variable to see if the panelist object with the same user id already exists
                        $check_panelist = $check_user->panelist()->exists();;

                        //Do if statement to see if the panelist with user id exists, else create
                        if ($check_panelist == true) {
                            $cur_panelist = $check_user->panelist;
                            $check_panelist_applicants = $cur_panelist->bootcamper()->exists();

                            if ($check_panelist_applicants == true) {
                                $cur_panelist_applicants = $cur_panelist->applicants;
                                //boolean to see if panelist has applicant
                                $has_applicant = false;

                                //loop through panelist applicants to see if the applicant id matches the current applicant id
                                foreach ($cur_panelist_applicants as $panelist_applicant) {
                                    if ($panelist_applicant->bootcamper_id == $bootcamper->id) {
                                        $has_applicant = true;
                                    }
                                }

                                //if panelist does not have current applicant, create applicantpanelist object
                                if ($has_applicant == false) {
                                    $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $cur_panelist->id,
                                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                                } else {
                                    $get_object = DB::table('bootcamper_panelists')->where('bootcamper_id', $bootcamper->id)
                                        ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                            'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                                }
                            } else {
                                $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            }
                        } else {
                            $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                            $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $create_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                        }
                    } else {
                        $create_user = User::create(['name' => $guest_panelist->name, 'surname' => $guest_panelist->surname,
                            'contact_number' => $guest_panelist->contact_number, 'email' => $guest_panelist->email, 'password' => Hash::make('secret'),
                            'company_name' => $guest_panelist->company, 'position' => $guest_panelist->position, 'title' => $guest_panelist->title,
                            'initials' => $guest_panelist->initials]);
                        $create_user->roles()->attach($panelist_role->id);
                        $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                        $create_panelist_applicant = BootcamperPanelist::create(['bootcamper_id' => $bootcamper->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully created panelists.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //Admin decline applicant panel interview
    public function adminDeclineBootcamperPanelInterview(Request $request, User $user)
    {
        $user->load('applicant', 'userQuestionAnswers');
        $input = $request->all();
        $user_applicant = $user->applicant;
        $user_question_answers = $user->userQuestionAnswers;

        try {
            DB::beginTransaction();

            if($request->has('referred_company')){
                $create_declined_applicant = DeclinedApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,
                    'code' => $user->code, 'declined' => true, 'referred' => true, 'chosen_category' => $user_applicant->chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'],
                    'venture_name' => $user_applicant->venture_name, 'referred_company' => $input['referred_company']]);
            } else {
                $create_declined_applicant = DeclinedApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,
                    'code' => $user->code, 'declined' => true, 'chosen_category' => $user_applicant->chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'],
                    'venture_name' => $user_applicant->venture_name]);
            }


            foreach ($user_question_answers as $question_answer) {
                $create_declined_applicant_question_answer = DeclinedApplicantQuestionAnswer::create(['d_a_id' => $create_declined_applicant->id,
                    'question_id' => $question_answer->question_id, 'answer_text' => $question_answer->answer_text]);
                $question_answer->forceDelete();
            }
            $user_applicant->forceDelete();
            $user->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Applicant was removed from the applicants list and set as declined applicant.'], 200);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()], 200);
        }
    }

    public function showBootcamperDetails(Bootcamper $bootcamper){
        $bootcamper->load('user', 'ventureCategory', 'events','bootcampercontactLog');
        $bootcamper_user = $bootcamper->user;
        $bootcamper_user->load('userQuestionAnswers','applicant');
        $applicant = $bootcamper_user->applicant;
        $applicant->load('contactLog');
        $applicant_contact_log = $applicant->contactLog;
        $logged_in_user = Auth::user()->load('roles');
        $user_contact_log = $bootcamper->bootcampercontactLog;

        $b_events_array = [];

        if(isset($bootcamper->events)){
            foreach($bootcamper->events as $b_event){
                $b_event->load('event');
                $event = $b_event->event;

                if ($b_event->attended == null or $b_event->attended == false) {
                    if($b_event->accepted == null or $b_event->accepted == false){
                        if(isset($event))
                            $object = (object)['event' => $event->title, 'accepted' => 'No',
                                'attended' => 'No', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                    } else {
                        if(isset($event))
                            $object = (object)['event' => $event->title, 'accepted' => 'Yes',
                                'attended' => 'No', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                    }
                } else {
                    if($b_event->accepted == true){
                        if ($b_event->attended == true){
                            if(isset($event))
                                $object = (object)['event' => $event->title, 'accepted' => 'Yes',
                                    'attended' => 'Yes', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                        } else {
                            if(isset($event))
                                $object = (object)['event' => $event->title, 'accepted' => 'Yes',
                                    'attended' => 'No', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                        }
                    } elseif ($b_event->attended == true){
                        if($b_event->accepted == true){
                            if(isset($event))
                                $object = (object)['event' => $event->title, 'accepted' => 'Yes',
                                    'attended' => 'Yes', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                        } else {
                            if(isset($event))
                                $object = (object)['event' => $event->title, 'accepted' => 'No',
                                    'attended' => 'Yes', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                        }
                    } elseif ($b_event->accepted == true and $b_event->attended == true){
                        if(isset($event))
                            $object = (object)['event' => $event->title, 'accepted' => 'Yes',
                                'attended' => 'Yes', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                    }
                }
                if(isset($object)) array_push($b_events_array, $object);

            }
        }

        //EVENTS
        $events = Event::where([['type','Private'], ['title', 'like', 'Bootcamp%']])->get();

        $bootcamperQuestionAnswers = [];

        foreach ($bootcamper_user->userQuestionAnswers as $userQuestionAnswer) {
            $question_id = $userQuestionAnswer->question_id;
            $question = Question::find($question_id);
            $answer_text = $userQuestionAnswer->answer_text;
            if(isset($question)) {
                if(isset($question->category->category_name)) {
                    if ($question->category->category_name == 'AA Application form - ICT Bootcamp' or
                        $question->category->category_name == 'AA Application form - Propella Township Incubator') {
                        $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                            'answer_text' => $answer_text];

                        array_push($bootcamperQuestionAnswers, $object);
                    }
                }
            }
        }

        $bootcamp_evaluation_question_array = [];
        if(count($bootcamper_user->userQuestionAnswers) > 0){
            foreach ($bootcamper_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'ZZ 2021 Bootcamp evaluation form') {
                            $answer_text = $item->answer_text;
                            $object = (object)['question_number' => $question->question_number,
                                'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                'question_id' => $question_id];
                            array_push($bootcamp_evaluation_question_array, $object);
                        }
                    }
                }
            }
        }
        /* Beginning of Bootcamp - Evaluation Form*/
        $beginning_of_bootcamp_evaluation_question_array = [];
        if(count($bootcamper_user->userQuestionAnswers) > 0){
            foreach ($bootcamper_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'CC 2022 1 Pre-Bootcamp Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($beginning_of_bootcamp_evaluation_question_array, $object);
                            }

                        }
                    }
                }
            }
        }

        /* Post of Bootcamp - Evaluation Form*/
        $post_of_bootcamp_evaluation_question_array = [];
        if(count($bootcamper_user->userQuestionAnswers) > 0){
            foreach ($bootcamper_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == 'CC 2022 2 Post-Bootcamp Evaluation Form') {
                            $answer_text = $item->answer_text;
                            if ($answer_text == null) {

                            } else {
                                $object = (object)['question_number' => $question->question_number,
                                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                    'question_id' => $question_id];
                                array_push($post_of_bootcamp_evaluation_question_array, $object);
                            }

                        }
                    }
                }
            }
        }

        $today = Carbon::now()->toDateString();
        $venture_categories = VentureCategory::all();
        $incubatee_stages = IncubateeStage::all();

        //BOOTCAMPER PANEL SCRORE
        $bootcamper->load('user', 'panelists');
        $bootcamper_user = $bootcamper->user;
        $bootcamper_user->load('applicant');
        $applicant = $bootcamper_user->applicant;
        $applicant_panelists = $bootcamper->panelists;
        $final_panelist_question_score_array = [];
        $panelists_question_score_collection = [];
        $final_question_numbers = [];
        $total_panelist_score = 0;
        $panelist_count = 0;

        foreach($applicant_panelists as $applicant_panelist){
            $applicant_panelist->load('panelist');
            $cur_panelist = $applicant_panelist->panelist;
            $cur_panelist->load('user');
            $cur_user = $cur_panelist->user;
            $cur_user_name = $cur_user->name;
            $cur_user_surname = $cur_user->surname;
            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;

            $panelist_question_answers = IctPanelistQuestionAnswer::where('bootcamper_panelist_id', $applicant_panelist->id)->get();

            foreach ($panelist_question_answers as $panelist_question_answer){
                $question_id = $panelist_question_answer->question_id;
                $cur_question = Question::find($question_id);
                $cur_question_number = $cur_question->question_number;
                $cur_score = $panelist_question_answer->score;
                $panelist_applicant_score += $cur_score;
                $object = (object)['question_number' => $cur_question_number,
                    'question_score' => $cur_score];
                array_push($panelist_questions_scores, $object);
                array_push($panelists_question_score_collection, $object);
                array_push($final_question_numbers, $cur_question_number);
            }

            $object = (object)['name' => $cur_user_name, 'surname' => $cur_user_surname,
                'user_questions_scores' => $panelist_questions_scores, 'panelist_applicant_score' => $panelist_applicant_score,
                'applicant_panelist_id' => $applicant_panelist->id];
            array_push($final_panelist_question_score_array, $object);

            $total_panelist_score += $panelist_applicant_score;

            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;
        }

        foreach ($final_panelist_question_score_array as $pqsa){
            if(count($pqsa->user_questions_scores)){
                $panelist_count += 1;
            }
        }

        if($panelist_count > 0){
            $average_panelist_score = $total_panelist_score / $panelist_count;
        } else {
            $average_panelist_score = 0;
        }

        //Store total panel score and average bootcamp score to the database for further use
        DB::beginTransaction();
        $bootcamper->total_panel_score = $total_panelist_score;
        $bootcamper->average_panel_score = $average_panelist_score;
        $bootcamper->save();
        DB::commit();

        $question_scores_array = [];
        $question_score = 0;

        foreach (array_unique($final_question_numbers) as $q_number){
            foreach ($applicant_panelists as $applicant_panelist) {
                $applicant_panelist->load('panelistQuestionAnswers');
                $applicant_panelist_questions = $applicant_panelist->panelistQuestionAnswers;
                foreach ($applicant_panelist_questions as $applicant_panelist_question){
                    $question_id = $applicant_panelist_question->question_id;
                    $cur_question = Question::find($question_id);
                    if($cur_question->question_number == $q_number){
                        $question_score += $applicant_panelist_question->score;
                    }
                }
            }
            $object = (object)['question_number' => $q_number, 'question_score' => $question_score];
            array_push($question_scores_array, $object);
            $question_score = 0;
        }

        //PANEL ACESS WINDOW, GO / NO GO PANEL
        $bootcamper_panel = [];
        $question_categories = QuestionsCategory::all();

        if(count($bootcamper->panelists) > 0){
            foreach($bootcamper->panelists as $bootcamper_panelist){
                $bootcamper_panelist->load('panelist');
                $panelist = $bootcamper_panelist->panelist;
                $panelist->load('user');
                $panelist_user = $panelist->user;

                $object = (object)['name' => $panelist_user->name, 'surname' => $panelist_user->surname,
                    'title' => $panelist_user->title, 'initials' => $panelist_user->initials,
                    'company_name' => $panelist_user->company_name, 'position' => $panelist_user->position,
                    'email' => $panelist_user->email, 'contact_number' => $panelist_user->contact_number,
                    'bootcamper_panelist_id' => $bootcamper_panelist->id];

                array_push($bootcamper_panel, $object);
            }
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/bootcampers/show-bootcamper', compact('bootcamper', 'bootcamperQuestionAnswers','events',
                'venture_categories','incubatee_stages','bootcamper_user','applicant','bootcamper_panel','question_categories',
                'final_panelist_question_score_array', 'question_scores_array', 'total_panelist_score', 'average_panelist_score','user_contact_log',
                'bootcamp_evaluation_question_array','applicant_contact_log','b_events_array','beginning_of_bootcamp_evaluation_question_array',
                'post_of_bootcamp_evaluation_question_array'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/bootcampers/show-bootcamper', compact('bootcamper', 'bootcamperQuestionAnswers','events',
                'venture_categories','incubatee_stages','bootcamper_user','applicant','bootcamper_panel','question_categories',
                'final_panelist_question_score_array', 'question_scores_array', 'total_panelist_score', 'average_panelist_score','user_contact_log',
                'bootcamp_evaluation_question_array','applicant_contact_log','b_events_array','beginning_of_bootcamp_evaluation_question_array',
                'post_of_bootcamp_evaluation_question_array'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/bootcampers/show-bootcamper', compact('bootcamper', 'bootcamperQuestionAnswers','events',
                'venture_categories','incubatee_stages','bootcamper_user','applicant','bootcamper_panel','question_categories',
                'final_panelist_question_score_array', 'question_scores_array', 'total_panelist_score', 'average_panelist_score','user_contact_log',
                'bootcamp_evaluation_question_array','applicant_contact_log','b_events_array','beginning_of_bootcamp_evaluation_question_array',
                'post_of_bootcamp_evaluation_question_array'));
        } else {
            return response("You are not authorized!!");
        }
    }

    public function showBootcamperEvents(Bootcamper $bootcamper){
        $logged_in_user = Auth::user()->load('roles');
        $today = Carbon::now()->toDateString();
        $bootcamper->load('user', 'ventureCategory', 'events');
        $events = Event::where([['type','Private'], ['title', 'like', 'ICT 22%']])->orderby('title','asc')->get();
        $events_psi = Event::where([['type','Private'], ['title', 'like', 'PSI%']])->orderby('title','asc')->get();
        $events_rap = Event::where([['type','Private'], ['title', 'like', 'RAP%']])->orderby('title','asc')->get();

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/bootcampers/bootcamper-events', compact('bootcamper', 'events','events_psi','events_rap'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/bootcampers/bootcamper-events', compact('bootcamper', 'events','events_psi','events_rap'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/bootcampers/bootcamper-events', compact('bootcamper', 'events','events_psi','events_rap'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getBootcamperEvents(Bootcamper $bootcamper){
        $logged_in_user = Auth::user()->load('roles');
        $bootcamper->load('events');
        $bootcamper_events_array = [];

        if(isset($bootcamper->events)){
            foreach($bootcamper->events as $b_event){
                $b_event->load('event');
                $event = $b_event->event;

                if (($b_event->attended == null or $b_event->attended == false) AND ($b_event->accepted == null or $b_event->accepted == false) AND ($b_event->declined == null or $b_event->declined == false)) {
                    if(isset($event))
                        $object = (object)['event' => $event->title, 'accepted' => 'No',
                            'attended' => 'No','declined' => 'No', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                }elseif($b_event->accepted == true AND $b_event->attended == false){
                    if(isset($event))
                        $object = (object)['event' => $event->title, 'accepted' => 'Yes',
                            'attended' => 'No','declined' => 'No', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                }elseif($b_event->accepted == true AND $b_event->attended == true){
                    if(isset($event))
                        $object = (object)['event' => $event->title, 'accepted' => 'Yes',
                            'attended' => 'Yes','declined' => 'No', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                }else{
                    if(isset($event))
                        $object = (object)['event' => $event->title, 'accepted' => 'No',
                            'attended' => 'No','declined' => 'Yes', 'date_registered' => $b_event->date_registered, 'id' => $b_event->id];
                }

                if(isset($object))
                    array_push($bootcamper_events_array, $object);
            }
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'advisor'
            or $logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($bootcamper_events_array)->addColumn('action', function ($bootcamper_event) {
                $del = $bootcamper_event->id;
                $edit = '/bootcamperEventEdit/' . $bootcamper_event->id;

                return '<a href=' . $edit . ' title="Edit Event" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_bootcamper_event(this)" title="Delete Registration"><i class="material-icons" style="color:red">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    ///=========================Edit Bootcamper event
    public function bootcamperEventEdit(EventBootcamper $eventBootcamper){
        $eventBootcamper->load('event','bootcamper');
        $event =$eventBootcamper->event;

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.bootcampers.bootcamper-edit-event',compact('eventBootcamper','event'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.bootcampers.bootcamper-edit-event',compact('eventBootcamper','event'));
        }

    }

    public function updateEventBootcamper(Request $request, EventBootcamper $eventBootcamper){
        $input = $request->all();
        $eventBootcamper->load('event');

        try {
            DB::beginTransaction();

            $eventBootcamper->update(['date_registered' => $input['date_registered'], 'attended' => $input['attended'], 'accepted' => $input['accepted']]);
            $eventBootcamper->save();
            DB::commit();
            return response()->json(['message' => 'Event bootcamper updated successfully.', 'eventBootcamper' => $eventBootcamper], 200);


        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Event bootcamper could not be saved ' . $e->getMessage()], 400);
        }
    }

    public function adminRegisterBootcamperEvent(Request $request){
        $input = $request->all();

        try{
            $bootcamper = Bootcamper::find($input['bootcamper_id']);
            $event = Event::find($input['event_id']);
            $bootcamper->load('events', 'user');
            $bootcamper_user = $bootcamper->user;
            $event->load('bootcampers');
            $today = Carbon::now()->toDateString();

            foreach($bootcamper->events as $b_event){
                if($b_event->event_id == $event->id){
                    return response()->json(['message' => 'Bootcamper already registered for this event.']);
                }
            }

            DB::beginTransaction();

            $create_bootcamper_event = EventBootcamper::create(['event_id' => $event->id, 'bootcamper_id' => $bootcamper->id,
                'accepted' => false, 'attended' => false, 'date_registered' => $today]);

            $email = $bootcamper_user->email;
            $password = 'Password_1234';
            $link = 'https://thepropella.co.za';
            $data = array(
                'name' => $bootcamper_user->name,
                'email' => $email,
                'password' => $password,
                'link' => $link,
                'event_start' => $event->start->toDateString(),
                'event_title' => $event->title
            );

            $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
            $fromEmail = "dev@propellaincubator.co.za";
            $toEmail = $email;
            $subject = "Your Bootcamp Times";
            $htmlBody = "<p>Dear <b>$bootcamper_user->name</b></p>
            <p> You have now been assigned to $event->title. This bootcamp will take place on $event->start->toDateString().  </p>
            <br>
            <p>Please make sure to log in to your Propella dashboard by visiting $link and then accepting / declining your allocated time slots.</p>
            <br>
            <p>You can use these credentials to log in.</p>
            <p>Email: $email</p>
            <p>Password: $password</p>
            <br>
            <p>We really look forward to start this journey with you.</p>
            <br>
            <p>Kind regards,</p>
            <p><b>Future Makers Team at Propella</b></p>";
            $textBody = "";
            $tag = "example-email-tag";
            $trackOpens = true;
            $trackLinks = "None";
            $messageStream = "broadcast";

            // Send an email:
            $sendResult = $client->sendEmail(
                $fromEmail,
                $toEmail,
                $subject,
                $htmlBody,
                $textBody,
                $tag,
                $trackOpens,
                NULL, // Reply To
                NULL, // CC
                NULL, // BCC
                NULL, // Header array
                NULL, // Attachment array
                $trackLinks,
                NULL, // Metadata array
                $messageStream
            );

            /* $email_field = $email;
             Mail::send('emails.assign-bootcamper-to-event', $data, function ($message) use ($email_field) {
                 $message->to($email_field)
                     ->subject('Your Bootcamp Times');
                 $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

             });*/


            DB::commit();
            return response()->json(['message' => 'Bootcamper now registered.']);
        } catch (\Exception $e){
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    public function bootcamperAcceptEvent(Request $request){
        $input = $request->all();
        $b_event_id = $input['b_event_id'];

        try{
            DB::beginTransaction();
            $event_bootcamper = EventBootcamper::find($b_event_id);
            $event_bootcamper->update(['accepted' => true, 'declined' => false]);
            $event_bootcamper->save();
            DB::commit();

            return response()->json(['message' => 'Thank you for accepting your invite']);
        } catch (\Exception $e){
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    public function bootcamperDeclineEvent(Request $request){
        $input = $request->all();
        $b_event_id = $input['b_event_id'];

        try{
            DB::beginTransaction();
            $event_bootcamper = EventBootcamper::find($b_event_id);
            $event_bootcamper->load('bootcamper', 'event');
            $event = $event_bootcamper->event;
            $bootcamper = $event_bootcamper->bootcamper;
            $bootcamper->load('user');
            $bootcamper_user = $bootcamper->user;
            $event_bootcamper->update(['declined' => true]);
            $event_bootcamper->save();
            DB::commit();

            $email = 'reception@propellaincubator.co.za';
            $data = array(
                'email' => $email,
                'user_name' => $bootcamper_user->name,
                'user_surname' => $bootcamper_user->surname,
                'user_phone' => $bootcamper_user->contact_number,
                'user_email' => $bootcamper_user->email,
                'event' => $event->title
            );

            $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
            $fromEmail = "dev@propellaincubator.co.za";
            $toEmail = $email;
            $subject = "Bootcamper Declined Event";
            $htmlBody = "<p>Dear <b>$bootcamper_user->name</b></p> <p>$bootcamper_user->name $bootcamper_user->surname has declined $event->title  </p>
            <br>
            <p>Could you please contact them on $bootcamper_user->contact_number or $bootcamper_user->email.</p>
            <br>
            <p>Once you have arranged for a new time slot, please don't forget to delete the current registration and add a new once. This will
                allow the platform to keep track of the data.</p>
            <br>
            <p>Kind regards,</p>
            <br>
            <p><b>Propella Platform</b></p>";
            $textBody = "";
            $tag = "example-email-tag";
            $trackOpens = true;
            $trackLinks = "None";
            $messageStream = "broadcast";

            // Send an email:
            $sendResult = $client->sendEmail(
                $fromEmail,
                $toEmail,
                $subject,
                $htmlBody,
                $textBody,
                $tag,
                $trackOpens,
                NULL, // Reply To
                NULL, // CC
                NULL, // BCC
                NULL, // Header array
                NULL, // Attachment array
                $trackLinks,
                NULL, // Metadata array
                $messageStream
            );


            /*$email_field = $email;
            Mail::send('emails.bootcamper-decline-event', $data, function ($message) use ($email_field) {
                $message->to($email_field)
                    ->subject('Bootcamper Declined Event');
                $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

            });*/

            return response()->json(['message' => 'Thank you for your response. We will contact you to set another time slot.']);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    public function deleteBootcamper(Bootcamper $bootcamper){
        $bootcamper->load('user', 'events');

        try{
            DB::beginTransaction();
            foreach ($bootcamper->events as $b_event){
                $b_event->forceDelete();
            }
            $bootcamper->forceDelete();
            $bootcamper_user = $bootcamper->user;
            $bootcamper_user->load('userQuestionAnswers', 'applicant');
            $bootcamper_user->applicant()->forceDelete();
            foreach ($bootcamper_user->userQuestionAnswers as $b_u_q_a){
                $b_u_q_a->forceDelete();
            }
            $bootcamper_user->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Bootcamper deleted.']);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    public function deleteBootcamperEventRegistration(EventBootcamper $eventBootcamper){
        try{
            DB::beginTransaction();
            $eventBootcamper->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Bootcamper registration deleted.']);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    public function bootcamperUploadPitchVideo(Request $request, Bootcamper $bootcamper){
        $input = $request->all();
        $today = Carbon::now();

        try{
            DB::beginTransaction();

            if($bootcamper->pitch_video_link != null or $bootcamper->pitch_video_link != false){
                $bootcamper->update(['pitch_video_link' => $input['pitch_video'], 'pitch_video_date_time' => $today->toDateTimeLocalString()]);
                $bootcamper->pitch_video_uploaded = true;
                $bootcamper->save();
                DB::commit();
                return response()->json(['message' => 'Pitch video updated.']);
            } else {
                $bootcamper->pitch_video_link = $input['pitch_video'];
                $bootcamper->pitch_video_date_time = $today->toDateTimeLocalString();
                $bootcamper->pitch_video_uploaded = true;
                $bootcamper->save();
                DB::commit();
                return response()->json(['message' => 'Pitch video added.']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    public function bootcamperSecondUploadPitchVideo(Request $request, Bootcamper $bootcamper){
        $input = $request->all();
        $today = Carbon::now();

        try{
            DB::beginTransaction();

            if($bootcamper->pitch_video_link_two != null or $bootcamper->pitch_video_link_two != false){
                $bootcamper->update(['pitch_video_link_two' => $input['second_pitch_video'], 'pitch_video_date_time' => $today->toDateTimeLocalString()]);
                $bootcamper->pitch_video_uploaded = true;
                $bootcamper->save();
                DB::commit();
                return response()->json(['message' => 'Second Pitch video updated.']);
            } else {
                $bootcamper->pitch_video_link_two = $input['second_pitch_video'];
                $bootcamper->pitch_video_date_time = $today->toDateTimeLocalString();
                $bootcamper->pitch_video_uploaded = true;
                $bootcamper->save();
                DB::commit();
                return response()->json(['message' => 'Second Pitch video added.']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    public function bootcamperUploadIDDocument(Request $request, Bootcamper $bootcamper){
        $input = $request->all();
        $request->validate([
            'id_document' => 'required|mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048'
        ]);
        try {
            if ($request->hasFile('id_document_url')) {
                $id_document_path = $request->file('id_document_url')->store('bootcamper_uploads');
                $bootcamper->update(['id_document_url' => $id_document_path]);
                $bootcamper->id_copy_uploaded = true;
                $bootcamper->save();
            } else {
                if ($request->hasFile('id_document')) {
                    $id_document_path = $request->file('id_document')->store('bootcamper_uploads');
                    $bootcamper->id_document_url = $id_document_path;
                    $bootcamper->id_copy_uploaded = true;
                }
                $bootcamper->save();
                DB::commit();
                return response()->json(['message' => 'ID document updated.']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    public function bootcamperToDeclinedApplicantView(Bootcamper $bootcamper){
        $logged_in_user = Auth::user()->load('roles');
        $bootcamper->load('user');
        $user = $bootcamper->user;

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/bootcampers/bootcamper-declined-applicant', compact('bootcamper', 'user'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/bootcampers/bootcamper-declined-applicant', compact('bootcamper', 'user'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/bootcampers/bootcamper-declined-applicant', compact('bootcamper', 'user'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function setBootcamperToDeclinedApplicant(Request $request, Bootcamper $bootcamper){
        $input = $request->all();
        $bootcamper->load('user', 'events');
        $user = $bootcamper->user;
        $user->load('applicant', 'userQuestionAnswers');
        $user_applicant = $user->applicant;
        $user_question_answers = $user->userQuestionAnswers;
        try {
            DB::beginTransaction();

            if($request->has('referred_company')){
                $create_declined_applicant = DeclinedApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,
                    'code' => $user->code, 'declined' => true, 'referred' => true, 'chosen_category' => $user_applicant->chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'],
                    'venture_name' => $user_applicant->venture_name, 'referred_company' => $input['referred_company']]);
            } else {
                $create_declined_applicant = DeclinedApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,
                    'code' => $user->code, 'declined' => true, 'chosen_category' => $user_applicant->chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'],
                    'venture_name' => $user_applicant->venture_name]);
            }

            foreach ($bootcamper->events as $b_event){
                $b_event->forceDelete();
            }

            $bootcamper->forceDelete();

            foreach ($user_question_answers as $question_answer) {
                $create_declined_applicant_question_answer = DeclinedApplicantQuestionAnswer::create(['d_a_id' => $create_declined_applicant->id,
                    'question_id' => $question_answer->question_id, 'answer_text' => $question_answer->answer_text]);
                $question_answer->forceDelete();
            }
            $user_applicant->forceDelete();
            $user->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Bootcamper now set to declined applicant.'], 200);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()], 200);
        }
    }

    public function updateBootcamperEvent(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();
        $bootcamper_id = $input['bootcamper_id'];
        $cur_bootcamper = Bootcamper::find($bootcamper_id);
        $cur_bootcamper->load('events');

                DB::beginTransaction();
                try {
                    if ($request->has('event_id')) {
                        $event_id = $input['event_id'];
                        $check_event = Event::find($event_id);
                        if(isset($check_event)){
                            $findEventBootcamper = EventBootcamper::where('bootcamper_id', '=', $cur_bootcamper->id)
                                ->where('event_id', '=', $check_event->id)->firstOrFail();

                            if(isset($findEventBootcamper)){
                                if($cur_bootcamper->bootcamp_nda_agreement == false){
                                    return response()->json(['message'=> 'Please view and accept the agreement.']);
                                } else {
                                    $findEventBootcamper->update(['attended'=>true]);
                                    $findEventBootcamper->save();
                                    DB::commit();
                                    return response()->json(['message'=> 'Thanks for attending.'],200);
                                }
                            }
                            return response()->json(['message'=> 'Bootcamper not found in system.']);
                        }

                    } else {
                        return response()->json(['message'=> 'Are you sure you chose an event?']);
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 500);
                }

    }

    //STORE AND UPDATE PROOF OF ADDRESS
    public function bootcamperUploadProofOfAddress(Request $request, Bootcamper $bootcamper){
        $request->validate([
            'proof_of_address' => 'required|mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048'
        ]);
        try{
            if ($request->hasFile('proof_of_address')) {
                $proof_of_address_path = $request->file('proof_of_address')->store('bootcamper_uploads');
                $bootcamper->update(['proof_of_address' => $proof_of_address_path]);
                $bootcamper->proof_of_address_uploaded = true;
                $bootcamper->save();
            } else {
                if ($request->hasFile('proof_of_address')) {
                    $proof_of_address_path = $request->file('proof_of_address')->store('bootcamper_uploads');
                    $bootcamper->proof_of_address = $proof_of_address_path;
                    $bootcamper->proof_of_address_uploaded = true;
                }
                $bootcamper->save();
            }
            DB::commit();
            return response()->json(['message' => 'Proof of address updated.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    //STORE AND UPDATE CIPC
    public function bootcamperUploadCIPC(Request $request, Bootcamper $bootcamper){
        $request->validate([
            'cipc' => 'required|mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048'
        ]);
        try{
            if ($request->hasFile('cipc')) {
                $cipc_path = $request->file('cipc')->store('bootcamper_uploads');
                $bootcamper->update(['cipc' => $cipc_path]);
                $bootcamper->cipc_uploaded = true;
                $bootcamper->save();
            } else {
                if ($request->hasFile('cipc')) {
                    $cipc_path = $request->file('cipc')->store('bootcamper_uploads');
                    $bootcamper->cipc = $cipc_path;
                    $bootcamper->cipc_uploaded = true;
                }
                $bootcamper->save();
            }
            DB::commit();
            return response()->json(['message' => 'CIPCs uploaded.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    //==========================Declined Bootcamper Section
    public function declinedBootcamperAccountOverview(DeclinedBootcamper $declinedBootcamper){
        $logged_in_user = Auth::user()->load('roles');
        $declinedBootcamper->load('declined_bootcamper_events', 'declined_bootcamper_application_question_answers',
        'declined_bootcamper_panelists', 'declined_bootcamper_venture_category','contactLog');
        $declinedBootcamperEventArray = [];
        $declinedBootcamperApplicationQuestionAnswersArray = [];
        $declinedBootcamperPanelInterviewArray = [];
        $bootcamperPanelSelectionDate = null;
        $bootcamperPanelSelectionTime = null;


        //If bootcamper has application events, loop through each one, find the event and store the event,
        //with the event details to the declinedBootcamperEventArray
        if(count($declinedBootcamper->declined_bootcamper_events) > 0){
            foreach ($declinedBootcamper->declined_bootcamper_events as $declined_bootcamper_event){
                $event_id = $declined_bootcamper_event->event_id;
                $event = Event::find($event_id);
                if(isset($event)){
                    $object = (object)['event_title' => $event->title, 'event_date' => $event->start,
                        'accepted' => $declined_bootcamper_event->accepted, 'date_registered' => $declined_bootcamper_event->date_registered,
                        'attended' => $declined_bootcamper_event->attended, 'declined' => $declined_bootcamper_event->declined];
                    array_push($declinedBootcamperEventArray, $object);
                }
            }
        }

        //If bootcamper has application question answers, loop through each one, find the question and store the question,
        //with an answer to the declinedBootcamperApplicantQuestionAnswerArray
        if(count($declinedBootcamper->declined_bootcamper_application_question_answers) > 0){
            foreach ($declinedBootcamper->declined_bootcamper_application_question_answers as $declined_bootcamper_application_question_answer){
                $question_id = $declined_bootcamper_application_question_answer->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    $answer_text = $declined_bootcamper_application_question_answer->answer_text;
                    $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                        'answer_text' => $answer_text];
                    array_push($declinedBootcamperApplicationQuestionAnswersArray, $object);
                }
            }
        }

        //Return a sorted version of the ApplicationQuestionAnswer Array -> using Arr::sort() as per laravel requests
        $sortedDeclinedBootcamperApplicationQuestionAnswersArray = array_values(Arr::sort($declinedBootcamperApplicationQuestionAnswersArray, function ($value) {
            return $value->question_number;
        }));

        //If bootcamper has panelist question answers, loop through each one, find the question and store the question,
        //with an answer and the panelist details to the declinedBootcamperPanelInterviewArray
        if(count($declinedBootcamper->declined_bootcamper_panelists) > 0){
            $panel_selection_time = $declinedBootcamper->declined_bootcamper_panelists[0]->panel_selection_time;
            $bootcamperPanelSelectionTime = $panel_selection_time;
            $panel_selection_date = $declinedBootcamper->declined_bootcamper_panelists[0]->panel_selection_date;
            $bootcamperPanelSelectionDate = $panel_selection_date;

            foreach ($declinedBootcamper->declined_bootcamper_panelists as $declined_bootcamper_panelist){
                $declined_bootcamper_panelist->load('declined_bootcamper_panelist_question_answers');
                $panelist_id = $declined_bootcamper_panelist->panelist_id;
                $panelist = Panelist::find($panelist_id);
                $panelist->load('user');
                $panelist_user = $panelist->user;
                $declined_panelist_question_answer_array = [];
                $totalScore = 0;

                if(count($declined_bootcamper_panelist->declined_bootcamper_panelist_question_answers) > 0){
                    foreach ($declined_bootcamper_panelist->declined_bootcamper_panelist_question_answers as $item){
                        $question_id = $item->question_id;
                        $question = Question::find($question_id);
                        $totalScore += $item->score;
                        if(isset($question)){
                            $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                                'panelist_score' => $item->score, 'panelist_comment' => $item->comment];
                            array_push($declined_panelist_question_answer_array, $object);
                        }
                    }
                }

                //Return a sorted version of the declined_panelist_question_answer_array Array -> using Arr::sort() as per laravel requests
                $sortedDeclinedPanelistQuestionAnswerArray = array_values(Arr::sort($declined_panelist_question_answer_array, function ($value) {
                    return $value->question_number;
                }));

                $object = (object)['panelist_details' => $panelist_user->name . ' ' . $panelist_user->surname, 'panelist_question_answers' => $sortedDeclinedPanelistQuestionAnswerArray, 'total_score' => $totalScore];
                array_push($declinedBootcamperPanelInterviewArray, $object);
            }
        }



        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/bootcampers/declined-bootcamper-account-overview', compact('sortedDeclinedBootcamperApplicationQuestionAnswersArray',
                'declinedBootcamperEventArray', 'declinedBootcamperPanelInterviewArray', 'bootcamperPanelSelectionDate', 'bootcamperPanelSelectionTime', 'declinedBootcamper'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/bootcampers/declined-bootcamper-account-overview', compact('sortedDeclinedBootcamperApplicationQuestionAnswersArray',
                'declinedBootcamperEventArray', 'declinedBootcamperPanelInterviewArray', 'bootcamperPanelSelectionDate', 'bootcamperPanelSelectionTime', 'declinedBootcamper'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/bootcampers/declined-bootcamper-account-overview', compact('sortedDeclinedBootcamperApplicationQuestionAnswersArray',
                'declinedBootcamperEventArray', 'declinedBootcamperPanelInterviewArray', 'bootcamperPanelSelectionDate', 'bootcamperPanelSelectionTime', 'declinedBootcamper'));
        } else {
            return response("You are not authorized!!");
        }
    }

    //==================================================================================================================

    //==========================Bootcamper Go /  No Go section
    //This is where the admin declines a bootcamper and sets them to the declined bootcamper table
    // ---- This is quite a big function as there are many relationships that need to be deleted / created
    public function setBootcamperToDeclinedBootcamper(Request $request, Bootcamper $bootcamper){
        $input = $request->all();
        $bootcamper->load('user', 'events', 'panelists','bootcampercontactLog');
        $bootcamper_contact_log = $bootcamper->bootcampercontactLog;
        try {
            $bootcamper_user = $bootcamper->user;
            $bootcamper_user->load('applicant', 'userQuestionAnswers');
            $bootcamper_applicant = $bootcamper_user->applicant;

            DB::beginTransaction();

            //Create the declined bootcamper object
            $create_declined_bootcamper = DeclinedBootcamper::create(['name' => $bootcamper_user->name, 'surname' => $bootcamper_user->surname,
                'email' => $bootcamper_user->email, 'contact_number' => $bootcamper_user->contact_number, 'contact_number_two' => $bootcamper_user->contact_number_two,
                'id_number' => $bootcamper_user->id_number, 'address_one' => $bootcamper_user->address_one, 'address_two' => $bootcamper_user->address_two,
                'address_three' => $bootcamper_user->address_three, 'title' => $bootcamper_user->title, 'initials' => $bootcamper_user->initials, 'date_of_birth' => $bootcamper_user->dob,
                'age' => $bootcamper_user->age, 'gender' => $bootcamper_user->gender, 'city' => $bootcamper_user->city, 'code' => $bootcamper_user->city,
                'race' => $bootcamper_user->race, 'referred' => false, 'declined' => true, 'venture_name' => $bootcamper_applicant->venture_name, 'chosen_category' => $bootcamper_applicant->chosen_category,
                'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'], 'referred_company' => $input['referred_company'],
                'pitch_video_link' => $bootcamper->pitch_video_link, 'pitch_video_date_time' => $bootcamper->pitch_video_date_time, 'pitch_video_link_two' => $bootcamper->pitch_video_link_two,
                'total_panel_score' => $bootcamper->total_panel_score, 'average_panel_score' => $bootcamper->average_panel_score, 'venture_category_id' => $bootcamper->venture_category_id,
                'id_document_url'=>$bootcamper->id_document_url,'proof_of_address'=>$bootcamper->proof_of_address,'cipc'=>$bootcamper->cipc,
                'bootcamper_contract_upload'=>$bootcamper->bootcamper_contract_upload
                ]);

            //Check if bootcamper user has question and answers, if yes, store it in the newly created bootcamper question answers
            if(count($bootcamper_user->userQuestionAnswers) > 0){
                foreach ($bootcamper_user->userQuestionAnswers as $b_q_a){
                    $create_declined_bootcamper_question_answers = DeclinedBootcamperQuestionAnswer::create(['question_id' => $b_q_a->question_id,
                        'd_b_id' => $create_declined_bootcamper->id, 'answer_text' => $b_q_a->answer_text]);

                    $b_q_a->forceDelete();
                }
            }

            //Check if bootcamper has events, if yes, create declined bootcamper event objects
            if (count($bootcamper->events) > 0) {
                $bootcamper_events = $bootcamper->events;

                foreach ($bootcamper_events as $b_event) {
                    //Assign event to declined bootcamper, this will create the relationship too
                    $create_declined_bootcamper_event = DeclinedBootcamperEvent::create(['event_id' => $b_event->event_id,
                        'declined_bootcamper_id' => $create_declined_bootcamper->id, 'accepted' => $b_event->accepted, 'date_registered' => $b_event->date_registered,
                        'attended' => $b_event->attended, 'declined' => $b_event->declined]);

                    //Delete bootcamper event
                    $b_event->forceDelete();
                }
            }

            //Check if bootcamper has events, if yes, create declined bootcamper panelist objects
            if (count($bootcamper->panelists) > 0) {
                $bootcamper_panelists = $bootcamper->panelists;

                //Loop through each panelist
                foreach($bootcamper_panelists as $b_panelist){
                    $b_panelist->load('panelistQuestionAnswers');
                    $create_declined_bootcamper_panelist = DeclinedBootcamperPanelist::create(['declined_bootcamper_id' => $create_declined_bootcamper->id,
                        'panelist_id' => $b_panelist->panelist_id, 'panel_selection_date' => $b_panelist->panel_selection_date, 'panel_selection_time' => $b_panelist->panel_selection_time,
                        'question_category_id' => $b_panelist->question_category_id]);

                    //See if panelist has question and answers, if yes, loop through each q_and_a and create a declined q_and_a
                    if(count($b_panelist->panelistQuestionAnswers) > 0){
                        foreach($b_panelist->panelistQuestionAnswers as $b_panelist_q_a){
                            $create_declined_bootcamper_panelist_question_answers = DeclinedBootcamperPanelistQuestionAnswers::create(['question_id' => $b_panelist_q_a->question_id,
                                'score' => $b_panelist_q_a->score, 'comment' => $b_panelist_q_a->comment, 'declined_bootcamper_panelist_id' => $create_declined_bootcamper_panelist->id]);

                            //Delete bootcamper q_and_a
                            $b_panelist_q_a->forceDelete();
                        }
                    }
                    //Delete bootcamper panelist
                    $b_panelist->forceDelete();
                }
            }

            //Delete the relationship between bootcamper user and applicant
            $bootcamper_applicant->forceDelete();

            //Delete the actual bootcamper object
            $bootcamper->forceDelete();

            //Finally, delete the user object that will deny the user to log back in
            $bootcamper_user->forceDelete();

            if(count($bootcamper_contact_log) > 0){
                foreach ($bootcamper_contact_log as $bootcamper_contact_logS) {
                    $contact_log = DeclinedBootcamperContactLog::create(['declined_bootcamper_id' => $create_declined_bootcamper->id,
                        'comment' => $bootcamper_contact_logS->comment,
                        'date' => $bootcamper_contact_logS->date,
                        'bootcamper_contact_results' => $bootcamper_contact_logS->bootcamper_contact_results]);
                }
            }

            $email = $bootcamper_user->email;
            $name = $bootcamper_user->name;

            $data = array(
                'name' => $name,
                'email' => $email,
            );

            $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
            $fromEmail = "dev@propellaincubator.co.za";
            $toEmail = $email;
            $subject = "Bootcamper declined";
            $htmlBody = "<p>Dear <b>$name</b></p><p>We thank you for your interest in
            the programme and coming in to present your business idea to our selection panel.
            was lovely to meet you and have you on the bootcamp. </p>
            <br/>
            <p>Unfortunately, based on the funders criteria, your application did not make the top 50 spots prescribed by our funder.</p>
            <br/>
            <p>Please do not let this demotivate you from pursuing your vision and ultimately reaching the goals you have set out for yourself and your team.</p>
            <br/>
            <p>We wish you the best of luck in the further development of your idea.</p>
            <br/>
            <p>We would love you to stay connected with Propella, so please follow our social media channels and join our
                future social workshops and events.</p>
            <br>
            Kind regards<br>
            The Propella Team";
            $textBody = " ";
            $tag = "example-email-tag";
            $trackOpens = true;
            $trackLinks = "None";
            $messageStream = "broadcast";

            // Send an email:
            $sendResult = $client->sendEmail(
                $fromEmail,
                $toEmail,
                $subject,
                $htmlBody,
                $textBody,
                $tag,
                $trackOpens,
                NULL, // Reply To
                NULL, // CC
                NULL, // BCC
                NULL, // Header array
                NULL, // Attachment array
                $trackLinks,
                NULL, // Metadata array
                $messageStream
            );

            /* $email_field = $email;
             Mail::send('emails.bootcamper-declined-email', $data, function ($message) use ($email_field) {
                 $message->to($email_field)
                     ->subject('Bootcamper declined');
                 $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

             });*/

            DB::commit();
            return response()->json(['message' => 'Completed!'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 500);
        }
    }



    //Referred Bootcampers Index
    public function referredBootcampersIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/bootcampers/referred-bootcamper-index');
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/bootcampers/referred-bootcamper-index');
        } elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/bootcampers/referred-bootcamper-index');
        }
        else {
            return response("You are not authorized!!");
        }
    }

    //Get the referred bootcampers from the database and return them to a view in a datatable format
    public function getReferredBootcampers(){
        $logged_in_user = Auth::user()->load('roles');
        $declined_bootcampers = DeclinedBootcamper::with('declined_bootcamper_venture_category')->where('result_of_contact','Stage 2 - Referred')->get();
        $declined_bootcampers_array = [];

        foreach($declined_bootcampers as $declined_bootcamper){
            $venture_category = $declined_bootcamper->declined_bootcamper_venture_category->category_name;
            $object = (object)['name' => $declined_bootcamper->name, 'surname' => $declined_bootcamper->surname,
                'email' => $declined_bootcamper->email, 'contact_number' => $declined_bootcamper->contact_number,
                'category_name' => $venture_category, 'id' => $declined_bootcamper->id,
                'date_declined' => $declined_bootcamper->created_at->toDateString(),
                'declined_reason'=>$declined_bootcamper->declined_reason];

            array_push($declined_bootcampers_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'advisor'
            or $logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($declined_bootcampers_array)->addColumn('action', function ($declined_bootcamper) {
                $sh = '/declined-bootcamper-account-overview/' . $declined_bootcamper->id;
                $edit = '/edit-declined-bootcamper-info/' .$declined_bootcamper->id;
                $delete = $declined_bootcamper->id;
                return '<a href=' . $sh . ' title="Bootcamper Overview"><i class="material-icons" style="color: darkslategray;">person</i></a><a href=' . $edit . ' title="Edit declined bootcamper" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $delete . ' onclick="confirm_delete_declined_applicant(this)" title="Delete Declined Bootcamper"><i class="material-icons" style="color:red">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response('You are not authorized!');
        }
    }

    //Bootcamper Contact Log
    public function storeBootcamperContactLog(Request $request,Bootcamper $bootcamper)
    {
        DB::beginTransaction();
        $input = $request->all();

        try {
            $contact_log = BootcamperContactLog::create(['comment' => $input['comment'], 'date' => $input['date'],
                'bootcamper_contact_results' => $input['bootcamper_contact_results'],
                'bootcamper_id' => $bootcamper->id]);

            $contact_log->save();


            DB::commit();
            return response()->json(['message' => 'Log added successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    //Update status
    public function updateBootcamperStatus(Request $request,Bootcamper $bootcamper)
    {
        DB::beginTransaction();
        $input = $request->all();

        try {

            $bootcamper->update(['status' => $input['status']]);

            $bootcamper->save();


            DB::commit();
            return response()->json(['message' => 'Status updated successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }


    //Bootcamper data upload
    public function storeBootcamperDataUpload(Request $request,User $user)
    {
        DB::beginTransaction();
        $input = $request->all();

        try {
            $user->data_cellnumber = $input['data_cellnumber'];
            $user->service_provider_network = $input['service_provider_network'];

            $user->save();

            DB::commit();
            return response()->json(['message' => 'Details added successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    //Bootcamp agreement form
    public function updateBootcampAgreement(Request $request,Bootcamper $bootcamper)
    {
        DB::beginTransaction();

        try {
            $bootcamper->bootcamp_nda_agreement = true;
            $bootcamper->save();

            DB::commit();
            return response()->json(['message' => 'Thank you']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    //STORE BOOTCAMP EVALUATION
    public function storeBootcampEvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'applicant','bootcamper');


            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->bootcamper->update(['evaluation_form_completed' => true]);
                $applicant_user->save();
            }


            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    /*STORE BEGINNING OF BOOTCAMP EVALUATION FORM*/
    public function storeBeginningBootcampEvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'applicant','bootcamper');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->bootcamper->update(['beginning_evaluation_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    /*STORE POST OF BOOTCAMP EVALUATION FORM*/
    public function storePostBootcampEvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'applicant','bootcamper');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->bootcamper->update(['post_evaluation_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }


    //Bootcamper evaluation index
    public function bootcamperEvaluationIndex(Bootcamper $bootcamper){
        $user = Auth::user()->load('roles');
        $bootcamper_question_array = [];
        $bootcamper->load( 'user');
        $b_user = $bootcamper->user;
        $b_user->load('userQuestionAnswers');
        if(count($b_user->userQuestionAnswers) > 0){
            foreach ($b_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $question = Question::find($question_id);
                if(isset($question)){
                    if($question->category->category_name == 'Bootcamp evaluation form'){
                        $answer_text = $item->answer_text;
                        $object = (object)['question_number' => $question->question_number,
                            'question_text' => $question->question_text, 'answer_text' => $answer_text,
                            'question_id' => $question_id];
                        array_push($bootcamper_question_array, $object);
                    }
                }
            }
        }

        if($user->roles[0]->name == 'app-admin'){
            return view('users.app-admin.bootcampers.show-bootcamper-evaluation-form-index',compact('bootcamper','bootcamper_question_array'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.bootcampers.show-bootcamper-evaluation-form-index',compact('bootcamper','bootcamper_question_array'));
        } elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.bootcampers.show-bootcamper-evaluation-form-index',compact('bootcamper','bootcamper_question_array'));
        }
        else {
            return response("You are not authorized!!");
        }
    }

    //STORE BOOTCAMPER CONTRACT
    public function bootcamperUploadContract(Request $request, Bootcamper $bootcamper){
        $request->validate([
            'bootcamper_contract_upload' => 'required|mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf|max:2048'
        ]);
        try{
            if ($request->hasFile('bootcamper_contract_upload')) {
                $contract_path = $request->file('bootcamper_contract_upload')->store('bootcamper_uploads');
                $bootcamper->update(['bootcamper_contract_upload' => $contract_path]);
                $bootcamper->contract_uploaded = true;
                $bootcamper->save();
            } else {
                if ($request->hasFile('bootcamper_contract_upload')) {
                    $contract_path = $request->file('bootcamper_contract_upload')->store('bootcamper_uploads');
                    $bootcamper->bootcamper_contract_upload = $contract_path;
                    $bootcamper->contract_uploaded = true;
                }
                $bootcamper->save();
            }
            DB::commit();
            return response()->json(['message' => 'Contract updated.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }

    }

    //Declined Bootcamper Contact Log
    public function storeDeclinedBootcamperContactLog(Request $request,DeclinedBootcamper $declinedBootcamper)
    {
        DB::beginTransaction();
        $input = $request->all();

        $date = Carbon::today();

        try {
            if ($input['bootcamper_contact_results'] == 'Email') {
                $contact_log = DeclinedBootcamperContactLog::create(['comment' => $input['comment'], 'date' => $date,
                    'bootcamper_contact_results' => $input['bootcamper_contact_results'],
                    'declined_bootcamper_id' => $declinedBootcamper->id]);


                /*$email =$declinedBootcamper->email;
                $name = $declinedBootcamper->name;
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'comment' => $input['comment']
                );


                $email_field = $email;
                Mail::send('emails.declined-bootcamper-contact-log-emailer', $data, function ($message) use ($email_field) {
                    $message->to($email_field)
                        ->subject('Application to Propella Business Incubator');
                    $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                });*/

                DB::commit();
                return response()->json(['message' => 'Log added successfully']);

            }else{
                $contact_log = DeclinedBootcamperContactLog::create(['comment' => $input['comment'], 'date' => $date,
                    'bootcamper_contact_results' => $input['bootcamper_contact_results'],
                    'declined_bootcamper_id' => $declinedBootcamper->id]);


                DB::commit();
                return response()->json(['message' => 'Log added successfully']);
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }


    //==================================================================================================================
}
