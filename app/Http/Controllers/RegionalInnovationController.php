<?php

namespace App\Http\Controllers;

use App\Panelist;
use App\QuestionsCategory;
use App\RinpApplicant;
use App\RinpApplicantCategory;
use App\RinpApplicantPanelist;
use App\RinpApplicantQuestionAnswer;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class RegionalInnovationController extends Controller
{
    public function storeApplicantApplication(Request $request){
        $input = $request->all();

        try{
            DB::beginTransaction();

            //Create applicant object in database
            $create_applicant = RinpApplicant::create(['name' => $input['name'], 'surname' => $input['surname'], 'email' => $input['email'],
                'contact_number' => $input['contact_number'], 'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'], 'title' => $input['title'],
                'initials' => $input['initials'], 'date_of_birth' => $input['date_of_birth'], 'age' => $input['age'], 'gender' => $input['gender'],
                'city' => $input['city'], 'postal_code' => $input['postal_code'], 'race' => $input['race']]);


           //Get applicant question and answers
            $question_answers = $input['q_a'];

            if(is_array($question_answers)){
                //loop through applicant question and answers
                foreach ($question_answers as $question_answer) {
                    $create_applicant_question_answer = RinpApplicantQuestionAnswer::create(['question_text' => $question_answer['question'],
                        'answer_text' => $question_answer['answer'], 'rinp_applicant_id' => $create_applicant->id]);
                }
            }


            DB::commit();
            return response('All went well. You can remove data.', 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response('Something went wrong ' . $e->getMessage(), 400);
        }
    }

    public function rinpApplicantCategoriesIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/rinp-applicants/rinp-applicant-categories-index');
        } else if ($logged_in_user->roles[0]->name == 'rinp-administrator'){
            return view('/users/rinp-administrators/rinp-applicants/rinp-applicant-categories-index');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getRinpApplicantCategories(){
        $rinp_applicant_categories = RinpApplicantCategory::all();
        $user = Auth::user()->load('roles');
        $rinp_applicant_categories_array = [];

        foreach ($rinp_applicant_categories as $rinp_applicant_category){
            $rinp_applicant_categories->load('rinpApplicants');
            $applicant_count = 0;
            if(count($rinp_applicant_category->rinpApplicants) > 0){
                $applicant_count = count($rinp_applicant_category->rinpApplicants);
            }
            $object = (object)['category_name' => $rinp_applicant_category->category_name,
                'applicant_count' => $applicant_count, 'id' => $rinp_applicant_category->id];
            array_push($rinp_applicant_categories_array, $object);
        }

        if ($user->roles[0]->name == 'app-admin' || $user->roles[0]->name == 'rinp-administrator') {
            return Datatables::of($rinp_applicant_categories_array)->addColumn('action', function ($rinp_applicant_category) {
                $edit= '/edit-rinp-applicant-category/' . $rinp_applicant_category->id;
                return '<a href=' . $edit . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function storeRinpApplicantCategory(Request $request){
        $input = $request->all();
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin' || $logged_in_user->roles[0]->name == 'rinp-administrator'){
            try{
                DB::beginTransaction();
                    $create_rinp_applicant_category = RinpApplicantCategory::create(['category_name' => $input['category_name']]);
                DB::commit();
                return response('Success', 200);
            } catch (\Exception $e){
                DB::rollBack();
                return response()->json(['message' => 'Something went wrong ' . $e->getMessage()], 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function rinpApplicantIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/rinp-applicants/rinp-applicants-index');
        } else if ($logged_in_user->roles[0]->name == 'rinp-administrator'){
            return view('/users/rinp-administrators/rinp-applicants/rinp-applicants-index');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getRinpApplicants(){
        $rinp_applicants = RinpApplicant::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin' || $user->roles[0]->name == 'rinp-administrator') {
            return Datatables::of($rinp_applicants)->addColumn('action', function ($rinp_applicant) {
                $view = '/view-rinp-applicant/' . $rinp_applicant->id;
                $delete = '/delete-rinp-applicant/' . $rinp_applicant->id;
                $paw = '/view-rinp-applicant-panel-access-window/' . $rinp_applicant->id;
                return '<a href=' . $view . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a>
                <a href=' . $paw . ' title="Panel Access Window" style="color:orange;"><i class="material-icons">account_balance</i></a>
                <a href=' . $delete . ' title="Delete Applicant" style="color:red; cursor: pointer;"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function adminViewRinpApplicant(RinpApplicant $rinpApplicant){
        $user = Auth::user()->load('roles');
        $rinpApplicant->load('applicantQuestionAnswers');

        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/rinp-applicants/view-rinp-applicant', compact('rinpApplicant'));
        } elseif ($user->roles[0]->name == 'rinp-administrator') {
            return view('/users/rinp-administrators/rinp-applicants/view-rinp-applicant', compact('rinpApplicant'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function adminDeleteRinpApplicant(RinpApplicant $rinpApplicant){
        $user = Auth::user()->load('roles');

        if($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'rinp-administrator'){
            try{
                DB::beginTransaction();
                $rinpApplicant->load('applicantQuestionAnswers', 'panelists');
                $rinpApplicationQuestionAnswers = $rinpApplicant->applicantQuestionAnswers;
                $rinpApplicantPanelists = $rinpApplicant->panelists;
                if(count($rinpApplicationQuestionAnswers) > 0){
                    foreach ($rinpApplicationQuestionAnswers as $questionAnswer){
                        $questionAnswer->forceDelete();
                    }
                }

                if(count($rinpApplicantPanelists) > 0){
                    $rinpApplicant->panelists()->forceDelete();
                }

                $rinpApplicant->forceDelete();

                DB::commit();
                return response("Deleted!", 200);
            } catch (\Exception $e){
                DB::rollBack();
                return response("Something went wrong " . $e->getMessage(), 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function adminViewRinpApplicantPanelAccessWindow(RinpApplicant $rinpApplicant){
        $user = Auth::user()->load('roles');
        $rinpApplicant->load('panelists');
        $applicant_panelists = [];
        $question_categories = QuestionsCategory::all();
        $rinp_applicant_categories = RinpApplicantCategory::all();

        if(count($rinpApplicant->panelists) > 0){
            foreach($rinpApplicant->panelists as $applicant_panelist){
                $applicant_panelist->load('panelist');
                $panelist = $applicant_panelist->panelist;
                $panelist->load('user');
                $panelist_user = $panelist->user;

                $object = (object)['name' => $panelist_user->name, 'surname' => $panelist_user->surname,
                    'title' => $panelist_user->title, 'initials' => $panelist_user->initials,
                    'company_name' => $panelist_user->company_name, 'position' => $panelist_user->position,
                    'email' => $panelist_user->email, 'contact_number' => $panelist_user->contact_number,
                    'applicant_panelist_id' => $applicant_panelist->id, 'question_category_id' => $applicant_panelist->question_category_id];

                array_push($applicant_panelists, $object);
            }
        }

        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/rinp-applicants/view-rinp-applicant-panel-access-window', compact('rinpApplicant', 'question_categories',
                'applicant_panelists', 'rinp_applicant_categories'));
        }  else if ($user->roles[0]->name == 'rinp-administrator'){
            return view('/users/rinp-administrators/rinp-applicants/view-rinp-applicant-panel-access-window', compact('rinpApplicant', 'question_categories',
                'applicant_panelists', 'rinp_applicant_categories'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function adminSetRinpApplicantPanelInterview(Request $request, RinpApplicant $rinpApplicant){
        $input = $request->all();

        try {
            $selected_panelists = json_decode($input['selected_panelists']);
            $guest_panelists = json_decode($input['guest_panelists']);
            $panelist_role = Role::where('name', 'panelist')->first();
            $question_category_id = $input['question_category_id'];
            $rinp_applicant_category_id = $input['rinp_applicant_category_id'];
            DB::beginTransaction();

            foreach ($selected_panelists as $selected_panelist) {
                //See if the actual user exists
                $check_user = User::where('email', $selected_panelist)->first();

                //if user does exist
                if (isset($check_user)) {
                    //load relationships to be used
                    $check_user->load('roles');
                    //boolean to see if the user has panelist role
                    $check_role = false;

                    //check if user has role panelist
                    foreach ($check_user->roles as $user_role) {
                        if ($user_role->name == 'panelist') {
                            $check_role = true;
                        }
                    }

                    //if user has role panelist, do nothing, else attach role
                    if ($check_role == false) {
                        $check_user->roles()->attach($panelist_role->id);
                    }

                    //Variable to see if the panelist object with the same user id already exists
                    $check_panelist = $check_user->panelist()->exists();;

                    //Do if statement to see if the panelist with user id exists, else create
                    if ($check_panelist == true) {
                        $cur_panelist = $check_user->panelist;
                        $check_panelist_applicants = $cur_panelist->rinpApplicants()->exists();

                        if ($check_panelist_applicants == true) {
                            $cur_panelist_applicants = $cur_panelist->rinpApplicants;

                            //boolean to see if panelist has applicant
                            $has_applicant = false;

                            //loop through panelist applicants to see if the applicant id matches the current applicant id
                            foreach ($cur_panelist_applicants as $panelist_applicant) {
                                if ($panelist_applicant->rinp_applicant_id == $rinpApplicant->id) {
                                    $has_applicant = true;
                                }
                            }

                            //if panelist does not have current applicant, create applicantpanelist object
                            if ($has_applicant == false) {
                                $create_panelist_applicant = RinpApplicantPanelist::create(['rinp_applicant_id' => $rinpApplicant->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            } else {
                                $get_object = DB::table('rinp_applicant_panelists')->where('rinp_applicant_id', $rinpApplicant->id)
                                    ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                        'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            }
                        } else {
                            $create_panelist_applicant = RinpApplicantPanelist::create(['rinp_applicant_id' => $rinpApplicant->id, 'panelist_id' => $cur_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                        }
                    } else {
                        $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                        $create_panelist_applicant = RinpApplicantPanelist::create(['rinp_applicant_id' => $rinpApplicant->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                    }
                } else {
                    $create_user = User::create(['email' => $selected_panelist, 'password' => Hash::make('secret')]);
                    $create_user->roles()->attach($panelist_role->id);
                    $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                    $create_panelist_applicant = RinpApplicantPanelist::create(['rinp_applicant_id' => $rinpApplicant->id, 'panelist_id' => $create_panelist->id,
                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                }
            }

            foreach ($guest_panelists as $guest_panelist) {
                $check_user = User::where('email', $guest_panelist->email)->first();

                //if user does exist
                if (isset($check_user)) {
                    //load relationships to be used
                    $check_user->load('roles');
                    //boolean to see if the user has panelist role
                    $check_role = false;

                    //check if user has role panelist
                    foreach ($check_user->roles as $user_role) {
                        if ($user_role->name == 'panelist') {
                            $check_role = true;
                        }
                    }

                    //if user has role panelist, do nothing, else attach role
                    if ($check_role == false) {
                        $check_user->roles()->attach($panelist_role->id);
                    }

                    //Variable to see if the panelist object with the same user id already exists
                    $check_panelist = $check_user->panelist()->exists();;

                    //Do if statement to see if the panelist with user id exists, else create
                    if ($check_panelist == true) {
                        $cur_panelist = $check_user->panelist;
                        $check_panelist_applicants = $cur_panelist->rinpApplicants()->exists();

                        if ($check_panelist_applicants == true) {
                            $cur_panelist_applicants = $cur_panelist->rinpApplicants;
                            //boolean to see if panelist has applicant
                            $has_applicant = false;

                            //loop through panelist applicants to see if the applicant id matches the current applicant id
                            foreach ($cur_panelist_applicants as $panelist_applicant) {
                                if ($panelist_applicant->rinp_applicant_id == $rinpApplicant->id) {
                                    $has_applicant = true;
                                }
                            }

                            //if panelist does not have current applicant, create applicantpanelist object
                            if ($has_applicant == false) {
                                $create_panelist_applicant = RinpApplicantPanelist::create(['rinp_applicant_id' => $rinpApplicant->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            } else {
                                $get_object = DB::table('rinp_applicant_panelists')->where('rinp_applicant_id', $rinpApplicant->id)
                                    ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                        'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            }
                        } else {
                            $create_panelist_applicant = RinpApplicantPanelist::create(['rinp_applicant_id' => $rinpApplicant->id, 'panelist_id' => $cur_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                        }
                    } else {
                        $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                        $create_panelist_applicant = RinpApplicantPanelist::create(['rinp_applicant_id' => $rinpApplicant->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                    }
                } else {
                    $create_user = User::create(['name' => $guest_panelist->name, 'surname' => $guest_panelist->surname,
                        'contact_number' => $guest_panelist->contact_number, 'email' => $guest_panelist->email, 'password' => Hash::make('secret'),
                        'company_name' => $guest_panelist->company, 'position' => $guest_panelist->position, 'title' => $guest_panelist->title,
                        'initials' => $guest_panelist->initials]);
                    $create_user->roles()->attach($panelist_role->id);
                    $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                    $create_panelist_applicant = RinpApplicantPanelist::create(['rinp_applicant_id' => $rinpApplicant->id, 'panelist_id' => $create_panelist->id,
                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                }
            }

            $rinpApplicant->update(['rinp_applicant_category_id' => $rinp_applicant_category_id]);
            $rinpApplicant->save();

            DB::commit();
            return response()->json(['message' => 'Successfully created panelists.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function adminUpdateRinpApplicantPanelInterview(Request $request, RinpApplicant $rinpApplicant){
        $input = $request->all();
        $rinpApplicant->load('panelists');
        $applicant_panelists = $rinpApplicant->panelists;

        try{
            DB::beginTransaction();
            if(count($applicant_panelists) > 0){
                foreach ($applicant_panelists as $applicant_panelist){
                    $applicant_panelist->panel_selection_date = $input['date'];
                    $applicant_panelist->panel_selection_time = $input['time'];
                    $applicant_panelist->save();
                }
            }

            DB::commit();
            return response()->json(['message' => 'Updated successfully!']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: '.$e->getMessage()], 400);
        }
    }
}
