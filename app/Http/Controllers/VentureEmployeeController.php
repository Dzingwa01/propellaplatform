<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventMediaImage;
use App\EventsMedia;
use App\PropellaVenue;
use App\UserVenueBooking;
use App\Venture;
use App\VentureEmployee;
use App\VentureUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class VentureEmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Venture $venture)
    {

        $venture->load('ventureEmployee')->get()->first();
        /*dd($venture);*/
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture-employee.index', compact('venture'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    /*public function getVentureEmployee(Venture $venture)
    {
        $venture->load('ventureEmployee');

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($venture->ventureEmployee)->addColumn('action', function ($employee) {
                $re = '/event-edit/' . $employee->id;
                $sh = '/event-show/' . $employee->id;
                $del = $employee->id;
                return '<a href=' . $re . ' title="Edit Events" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">group</i></a><a id=' . $del . ' onclick="confirm_delete_events(this)" title="Delete Event" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }*/

    public function createEmployee(Venture $venture)
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture-employee.create-venture-employee', compact('venture'));
        } elseif ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.venture-employee.create-venture-employee', compact('venture'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.ventures.venture-employee.create-venture-employee', compact('venture'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ventures.venture-employee.create-venture-employee', compact('venture'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ventures.venture-employee.create-venture-employee', compact('venture'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function storeEmployee(Request $request, Venture $venture)
    {

        try {
            $input = $request->all();
            DB::beginTransaction();

            $venture_employee = VentureEmployee::create(['employee_name' => $input['employee_name'], 'employee_surname' => $input['employee_surname'],
                'position' => $input['position'], 'start_date' => $input['start_date'], 'end_date' => $input['end_date'], 'title' => $input['title'],
                'initials' => $input['initials'], 'id_number' => $input['id_number'], 'gender' => $input['gender'], 'contact_number' => $input['contact_number'],
                'email' => $input['email'], 'status' => $input['status'], 'resigned_date' => $input['resigned_date'], 'venture_id' => $venture->id]);

            $venture_employee->save();

            if ($request->hasFile('contract_url')) {
                $contract_url_path = $request->file('contract_url')->store('incubatee_uploads');
                if (isset($venture_employee)) {
                    $venture_employee->update(['contract_url' => $contract_url_path, 'venture_id' => $venture->id]
                    );
                } else {
                    $venture_employee->create(['contract_url' => $contract_url_path, 'venture_id' => $venture->id]
                    );
                }
            }

            DB::commit();
            return response()->json(['employee' => $venture_employee, 'message' => 'Employee Has Been created successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Employee could not be saved at the moment ' . $e->getMessage()], 400);
        }
    }

    public function editEmployee(VentureEmployee $venture_employee)
    {

        $venture= $venture_employee->load('Venture')->get();

        // = $venture_employee-> load('venture');
        //dd($venture_employee);
        /*        $venture_employee = VentureEmployee::with('venture')->where('venture_id', $venture->id)->get()->first();*/


        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture-employee.edit-venture-employee', compact('venture','venture_employee'));
        } elseif ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.venture-employee.edit-venture-employee', compact('venture_employee'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.incubatee-dashboard.venture-employee.edit-venture-employee', compact('venture_employee'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.incubatee-dashboard.venture-employee.edit-venture-employee', compact('venture_employee'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.incubatee-dashboard.venture-employee.edit-venture-employee', compact('venture_employee'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateEmployee(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $employee_id = $input['employee_id'];
        $venture_employee = VentureEmployee::find($employee_id);


        try {
            $venture_employee->load('Venture');
            $venture_employee->update(['employee_name' => $input['employee_name'], 'employee_surname' => $input['employee_surname'],
                'position' => $input['position'], 'start_date' => $input['start_date'], 'end_date' => $input['end_date'], 'title' => $input['title'],
                'initials' => $input['initials'], 'id_number' => $input['id_number'], 'gender' => $input['gender'], 'contact_number' => $input['contact_number'],
                'email' => $input['email'], 'status' => $input['status'], 'resigned_date' => $input['resigned_date']]);

            $venture_employee->save();

            if ($request->hasFile('contract_url')) {
                $contract_url_path = $request->file('contract_url')->store('incubatee_uploads');
                if (isset($venture_employee)) {
                    $venture_employee->update(['contract_url' => $contract_url_path]
                    );
                }
            }

            DB::commit();
            return response()->json(['employee' => $venture_employee, 'message' => 'Employee Has Been Updated successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Employee could not be saved at the moment ' . $e->getMessage()], 400);
        }
    }

    public function destroyEmployee(VentureEmployee $employee)
    {
        try {
            DB::beginTransaction();
            $employee->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Successfully deleted this employee']);

        } catch (\Exception $e) {
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }

    }
}
