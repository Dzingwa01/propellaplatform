<?php

namespace App\Http\Controllers;

use App\CompanyEmployee;
use App\Incubatee;
use App\User;
use App\Venture;
use App\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Postmark\PostmarkClient;
use Yajra\DataTables\DataTables;

class PropellaUsersEmailerController extends Controller
{
    //PROPELLA EMAIL
    public function propellaUsersIndex(){
        $logged_in_user = Auth::user()->load('roles');
        $visitors = Visitor::with('visitor_backup')->get();
        $users = User::all();
        $incubatees = Incubatee::with( 'user','venture')->get();
        $companiesEmployees = CompanyEmployee::with('categories', 'company')->get();
        if($logged_in_user->roles[0]->name == 'app-admin') {
            return view('Platform-emailer.all-propella-users', compact('users', 'visitors', 'incubatees', 'companiesEmployees'));
        }elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.Platform-emailer.all-propella-users', compact('users', 'visitors', 'incubatees', 'companiesEmployees'));
        }elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Platform-emailer.all-propella-users', compact('users', 'visitors', 'incubatees', 'companiesEmployees'));

        }
    }

    public function sendMailToUsers(Request $request)
    {
        $input = $request->all();
        $selected_users = json_decode($input['selected_users']);

        try {
            DB::beginTransaction();

            foreach ($selected_users as $selected_users_in_db) {
                if ($selected_users_in_db) {
                    $users = User::find($selected_users_in_db);

                    $link = 'https://thepropella.co.za/show-events';

                    $data = array(
                        'name' => $users->name,
                        'email' => $users->email,
                        'link' => $link
                    );
                    $email_field = $users->email;
                    Mail::send('emails.send-email-to-all-users', $data, function ($message) use ($email_field) {
                        $message->to($email_field)
                            ->subject('Event invite');
                        $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                    });

                    return response()->json(['message' => 'Email sent']);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully sent email.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function sendMailToVisitors(Request $request)
    {
        $input = $request->all();
        $selected_visitors = json_decode($input['selected_visitors']);

        try {
            DB::beginTransaction();

            foreach ($selected_visitors as $selected_users_in_db) {
                if ($selected_users_in_db) {
                    $visitors = Visitor::find($selected_users_in_db);

                    $link = 'https://thepropella.co.za/show-events';

                    $data = array(
                        'first_name' => $visitors->first_name,
                        'email' => $visitors->email,
                        'link' => $link
                    );

                    $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                    $fromEmail = "dev@propellaincubator.co.za";
                    $toEmail = $visitors->email;
                    $subject = "Event invite";
                    $htmlBody = "<p>Dear <b>$visitors->first_name</b></p><p>We are running a workshop on (name of the workshop) via Zoom which we thought may be of interest to you.</p>
                    <br/>
                    <p>To view/register this workshop click here: $link</p>
                    <br>
                    <p>Once registered you will receive the zoom link to the workshop</p>

                    <br>
                    <p>With many thanks.</p>
                    <p>The Propella Team</p>";
                    $textBody = "";
                    $tag = "example-email-tag";
                    $trackOpens = true;
                    $trackLinks = "None";
                    $messageStream = "broadcast";

                    // Send an email:
                    $sendResult = $client->sendEmail(
                        $fromEmail,
                        $toEmail,
                        $subject,
                        $htmlBody,
                        $textBody,
                        $tag,
                        $trackOpens,
                        NULL, // Reply To
                        NULL, // CC
                        NULL, // BCC
                        NULL, // Header array
                        NULL, // Attachment array
                        $trackLinks,
                        NULL, // Metadata array
                        $messageStream
                    );


                    /*$email_field = $visitors->email;
                    Mail::send('emails.send-email-to-all-users', $data, function ($message) use ($email_field) {
                        $message->to($email_field)
                            ->subject('Event invite');
                        $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                    });*/


                    return response()->json(['message' => 'Email sent']);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully sent email.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function sendMailToVentures(Request $request)
    {
        $input = $request->all();
        $selected_ventures = json_decode($input['selected_ventures']);

        try {
            DB::beginTransaction();

            foreach ($selected_ventures as $selected_ventures_in_db) {
                if ($selected_ventures_in_db) {
                    $incubatee = Incubatee::find($selected_ventures_in_db);
                    $incubatee->load('user');
                    $user = $incubatee->user;

                    $link = 'https://thepropella.co.za/show-events';
                    $data = array(
                        'name' => $user->name,
                        'email' => $user->email,
                        'link' => $link
                    );

                    $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                    $fromEmail = "dev@propellaincubator.co.za";
                    $toEmail = $user->email;
                    $subject = "Event invite";
                    $htmlBody = "<p>Dear <b>$user->name</b></p><p>We are running a workshop on (name of the workshop) via Zoom which we thought may be of interest to you.</p>
                    <br/>
                    <p>To view/register this workshop click here: $link</p>
                    <br>
                    <p>Once registered you will receive the zoom link to the workshop</p>

                    <br>
                    <p>With many thanks.</p>
                    <p>The Propella Team</p>";
                    $textBody = "";
                    $tag = "example-email-tag";
                    $trackOpens = true;
                    $trackLinks = "None";
                    $messageStream = "broadcast";

                    // Send an email:
                    $sendResult = $client->sendEmail(
                        $fromEmail,
                        $toEmail,
                        $subject,
                        $htmlBody,
                        $textBody,
                        $tag,
                        $trackOpens,
                        NULL, // Reply To
                        NULL, // CC
                        NULL, // BCC
                        NULL, // Header array
                        NULL, // Attachment array
                        $trackLinks,
                        NULL, // Metadata array
                        $messageStream
                    );

                   /* $email_field = $user->email;
                    Mail::send('emails.send-email-to-all-users', $data, function ($message) use ($email_field) {
                        $message->to($email_field)
                            ->subject('Event invite');
                        $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                    });*/



                    return response()->json(['message' => 'Email sent']);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully sent email.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function sendMailToCompany(Request $request)
    {
        $input = $request->all();
        $selected_company = json_decode($input['selected_company']);

        try {
            DB::beginTransaction();

            foreach ($selected_company as $selected_ventures_in_db) {
                if ($selected_ventures_in_db) {
                    $company = CompanyEmployee::find($selected_ventures_in_db);

                    $link = 'https://thepropella.co.za/show-events';
                    $data = array(
                        'name' => $company->name,
                        'email' => $company->email,
                        'link' => $link
                    );
                    $email_field = $company->email;
                    Mail::send('emails.send-email-to-all-users', $data, function ($message) use ($email_field) {
                        $message->to($email_field)
                            ->subject('Event invite');
                        $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                    });


                    return response()->json(['message' => 'Email sent']);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully sent email.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }
}
