<?php

namespace App\Http\Controllers;

use App\VentureType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class VentureTypeController extends Controller
{
    public function ventureTypeIndex(){
//        $user = Auth::user()->load('roles');
//
//        if($user->roles[0]->name == 'app-admin'){
            return view('users.venture.venture-type-index');
//        } elseif ($user->roles[0]->name == 'administrator'){
//            return view('users.administrators.ventures.venture-type-index');
//        } else {
//            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
//        }
    }
    public function createVentureType(){
        return view('users.administrators.ventures.create-venture-type');
    }

    public function storeVentureType(Request $request){
        DB::beginTransaction();
        $input = $request->all();

        try{
            $create_type = VentureType::create(['type_name' => $input['type_name']]);

            $create_type->save();


            DB::commit();
            return response()->json(['message'=>'Venture type added successfully']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }
    }

    public function getVentureTypes()
    {
        $venture_types = VentureType::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($venture_types)->addColumn('action', function ($venture_type) {
                $re = '/venture-type-edit/' . $venture_type->id;
                $del = $venture_type->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_venture_type(this)" title="Delete Venture Type" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($venture_types)->addColumn('action', function ($venture_type) {
                $re = '/venture-type-edit/' . $venture_type->id;
                return '<a href=' . $re . ' title="Edit Venture Type" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }
}
