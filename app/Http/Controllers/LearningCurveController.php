<?php

namespace App\Http\Controllers;

use App\PropellaLearningCurve;
use App\PropellaLearningCurveSummernote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Cviebrock\EloquentSluggable\Services\SlugService;

class LearningCurveController extends Controller
{
    //Create Learning curve
    public function createLearningCurve(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.learning-curves.create-learning-curves');
        }elseif ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.learning-curves.create-learning-curves');
        }elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.learning-curves.create-learning-curves');
        }elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.learning-curves.create-learning-curves');
        }
    }

    //Store blog
    public function storeLearningCurve(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::today();

        try {
            $learningCurve = PropellaLearningCurve::create(['description' => $input['description'],
                'added_date' => $date,'title'=>$input['title']
            ]);
            if ($request->hasFile('learning_image_url')) {
                $image_url_path = $request->file('learning_image_url')->store('incubatees_uploads');
                if (isset($learningCurve)) {
                    $learningCurve->update(['learning_image_url' => $image_url_path]
                    );
                } else {
                    $learningCurve->create(['learning_image_url' => $image_url_path]
                    );
                }
            }
            $learningCurve->save();
            DB::commit();
            return response()->json(['message' => 'Your Learning curve has been successfully saved.', 'learning_curve_id' => $learningCurve->id]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Store Learning Curve summernote
    public function storeLearningCurveSummernote(Request $request)
    {
        $detail = $request->summernoteInput;
        $learning_curve = $request->learning_curve_id;

        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $detail = $dom->savehtml();

        $learningCurveSummernote = new PropellaLearningCurveSummernote();

        $learningCurveSummernote->content = $detail;
        $learningCurveSummernote->learning_curve_id = $learning_curve;

        $learningCurveSummernote->save();
        return view('users.marketing-dashboard.learning-curves.create-learning-curves',compact('learningCurveSummernote'));
    }

    //Show Learning curve
    public function showLearningCurve()
    {
        $learning_curves = PropellaLearningCurve::orderBy('added_date', 'desc')->get();

        return view('users.marketing-dashboard.learning-curves.show-learning-curves',compact('learning_curves'));

    }

    //Shoe Detailes Learning curve
    public function showDetailedLearningCurve($id)
    {
        $propellaLearningCurves = PropellaLearningCurve::all();
        $propellaLearningCurve = PropellaLearningCurve::where('slug',$id)->first();
        $propellaLearningCurve->load('learningCurveSummernote');
        return view('users.marketing-dashboard.learning-curves.detailed-learning',compact('propellaLearningCurve','propellaLearningCurves'));
    }

    //Index
    public function indexLearning(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.learning-curves.learning-index');
        }elseif ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.learning-curves.learning-index');
        }elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.learning-curves.learning-index');
        }elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.learning-curves.learning-index');
        }
    }

    public function getLearningCurves()
    {
        $learning_curves = PropellaLearningCurve::with('learningCurveSummernote')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'marketing' or
            $user->roles[0]->name == 'app-admin' or
            $user->roles[0]->name == 'administrator' or
            $user->roles[0]->name == 'advisor') {
            return Datatables::of($learning_curves)->addColumn('action', function ($learning) {
                $re = '/edit-learning-curves/' . $learning->id;
                $del = '/destroy-learning-curves/' . $learning->id;
                return '<a href=' . $re . ' title="Edit Blog" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Blog" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    //Edit learning curves
    public function editLearningCurves(PropellaLearningCurve $propellaLearningCurve){
        $propellaLearningCurve->load('learningCurveSummernote');
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.learning-curves.edit-learning-curves',compact('propellaLearningCurve'));
        }elseif ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.learning-curves.edit-learning-curves',compact('propellaLearningCurve'));
        }elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.learning-curves.edit-learning-curves',compact('propellaLearningCurve'));
        }elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.learning-curves.edit-learning-curves',compact('propellaLearningCurve'));
        }
    }

    //Update learning curves
    public function updateLearningCurve(Request $request, PropellaLearningCurve $propellaLearningCurve)
    {
        $propellaLearningCurve->load('learningCurveSummernote')->get();

        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::today();


        try {
            $propellaLearningCurve->update(['title' => $input['title'],
                'description' => $input['description'],
                'added_date' => $date]);
            $propellaLearningCurve->save();

            if ($request->hasFile('learning_image_url')) {
                $image_url_path = $request->file('learning_image_url')->store('incubatee_uploads');
                if (isset($propellaLearningCurve)) {
                    $propellaLearningCurve->update(['learning_image_url' => $image_url_path]
                    );
                } else {
                    $propellaLearningCurve->create(['learning_image_url' => $image_url_path]
                    );
                }
            }
            DB::commit();
            return response()->json(['message' => 'Successfully updated Learning curve']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Update Learning curve summernote
    public function updateLearningSummernote(Request $request, PropellaLearningCurve $propellaLearningCurve)
    {
        $propellaLearningCurve->load('learningCurveSummernote')->get();

        DB::beginTransaction();
        $input = $request->all();

        try {
            $propellaLearningCurve->save();
            if(isset($propellaLearningCurve->learningCurveSummernote)) {
                $learningCurveSummernote_content = $propellaLearningCurve->learningCurveSummernote;
                if ($request->has('content')) {
                    $learningCurveSummernote_content->update(['content' => $input['content']]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully updated Learning curve summernote']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Delete learning curve with its relationships
    public function destroyLearningCurve($id)
    {
        try {
            DB::beginTransaction();
            $learningCurve = PropellaLearningCurve::find($id);
            $learningCurve->load('learningCurveSummernote');
            if (isset($learningCurve->learningCurveSummernote)){
                $learningCurve->learningCurveSummernote->forceDelete();
            }else {
                $learningCurve->forceDelete();
            }

            DB::commit();
            return view('users.marketing-dashboard.learning-curves.learning-index');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }

    }
}
