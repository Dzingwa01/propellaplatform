<?php

namespace App\Http\Controllers;

use App\CompanyEmployee;
use App\ContactLog;
use App\ContactType;
use App\Http\Requests\LogStoreRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class ContactLogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.contact-logs.index', compact('user', 'callers', 'contact_types', 'receivers'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.contact-log.index', compact('user', 'callers', 'contact_types', 'receivers'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.contact-log.index',  compact('user', 'callers', 'contact_types', 'receivers'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.contact-logs.index', compact('user'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.contact-logs.index', compact('user'));
        } else {
            return view('campaign-manager.contact-logs.clerk.index', compact('user', 'callers', 'contact_types', 'receivers'));
        }

    }

    public function addActivityLog()
    {
        $log_latest = ContactLog::latest()->get();
//        if(is_null($log_latest)){
//            $reference_number = "Prop_"+
//        }
        $user = Auth::user();
        $callers = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin')->orWhere('name', 'clerk')->orWhere('name', 'app-admin');
            return $query;
        })->whereNull('users.deleted_at')->get();
        $receivers = CompanyEmployee::all();

        $contact_types = ContactType::orderBy('type_name', 'asc')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.contact-logs.add-contact-log', compact('user', 'callers', 'contact_types', 'receivers'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.contact-log.add-contact-log', compact('user', 'callers', 'contact_types', 'receivers'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.contact-log.add-contact-log', compact('user', 'callers', 'contact_types', 'receivers'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.contact-logs.add-contact-log', compact('user', 'callers', 'contact_types', 'receivers'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.contact-logs.add-contact-log', compact('user', 'callers', 'contact_types', 'receivers'));
        }else {
            return view('campaign-manager.contact-logs.clerk.add-contact-log', compact('user', 'callers', 'contact_types', 'receivers'));
        }

    }

    public function getContactLogs()
    {
        $contact_logs = ContactLog::with('caller', 'receiver', 'contact_type')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return Datatables::of($contact_logs)
                ->addColumn('action', function ($contact_log) {
                    $re = '/contact-logs/' . $contact_log->id;
                    $sh = '/contact-logs/show/' . $contact_log->id;
                    $del = '/contact-logs/delete/' . $contact_log->id;
                    return '<a href=' . $sh . ' title="View Contact Log"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Contact Log" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Contact Log" style="color:red"><i class="material-icons">delete_forever</i></a>';
                })
                ->make(true);
        } elseif ($user->roles[0]->name == "administrator") {
            return Datatables::of($contact_logs)
                ->addColumn('action', function ($contact_log) {
                    $re = '/contact-logs/' . $contact_log->id;
                    $sh = '/contact-logs/show/' . $contact_log->id;
                    return '<a href=' . $sh . ' title="View Contact Log"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Contact Log" style="color:green!important;"><i class="material-icons">create</i></a>';
                })
                ->make(true);
        } elseif ($user->roles[0]->name == "marketing") {
            return Datatables::of($contact_logs)
                ->addColumn('action', function ($contact_log) {
                    $re = '/contact-logs/' . $contact_log->id;
                    $sh = '/contact-logs/show/' . $contact_log->id;
                    return '<a href=' . $sh . ' title="View Contact Log"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Contact Log" style="color:green!important;"><i class="material-icons">create</i></a>';
                })
                ->make(true);
        } elseif ($user->roles[0]->name == "advisor") {
            return Datatables::of($contact_logs)
                ->addColumn('action', function ($contact_log) {
                    $re = '/contact-logs/' . $contact_log->id;
                    $sh = '/contact-logs/show/' . $contact_log->id;
                    $del = '/contact-logs/delete/' . $contact_log->id;
                    return '<a href=' . $sh . ' title="View Contact Log"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Contact Log" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Contact Log" style="color:red"><i class="material-icons">delete_forever</i></a>';
                })
                ->make(true);
        } elseif ($user->roles[0]->name == "mentor") {
            return Datatables::of($contact_logs)
                ->addColumn('action', function ($contact_log) {
                    $re = '/contact-logs/' . $contact_log->id;
                    $sh = '/contact-logs/show/' . $contact_log->id;
                    $del = '/contact-logs/delete/' . $contact_log->id;
                    return '<a href=' . $sh . ' title="View Contact Log"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Contact Log" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Contact Log" style="color:red"><i class="material-icons">delete_forever</i></a>';
                })
                ->make(true);
        } else {
            return Datatables::of($contact_logs)
                ->addColumn('action', function ($contact_log) {
                    $re = '/contact-logs/' . $contact_log->id;
                    $sh = '/contact-logs/show/' . $contact_log->id;
                    return '<a href=' . $sh . ' title="View Contact Log"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Contact Log" style="color:green!important;"><i class="material-icons">create</i></a>';
                })
                ->make(true);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LogStoreRequest $request)
    {
        //
        $input = $request->validated();
        DB::beginTransaction();
        try {
            $log_latest = DB::table('contact_logs')->latest()->first();
            if (is_null($log_latest)) {
                $reference_number = "Prop_001";
                $log = ContactLog::create(['contact_date' => $input['contact_date'], 'description' => $input['description'],
                    'reference_number' => $reference_number, 'contact_type_id' => $input['contact_type_id'], 'caller_id' => $input['caller_id'], 'receiver_id' => $input['receiver_id']]);

            } else {
                $temp_ref = explode('_', $log_latest->reference_number);
                $new_ref_value = (int)$temp_ref[1] + 1;
                $reference_number = "Prop_" . $new_ref_value;
                $log = ContactLog::create(['contact_date' => $input['contact_date'], 'description' => $input['description'],
                    'reference_number' => $reference_number, 'contact_type_id' => $input['contact_type_id'], 'caller_id' => $input['caller_id'], 'receiver_id' => $input['receiver_id']]);
            }
            $companyEmployee = CompanyEmployee::where('id', $input['receiver_id'])->first();

            DB::commit();
            return response()->json(['message' => 'Activity log saved successfully', 'log' => $log, 'company_employee' => $companyEmployee], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured while saving log, please contact your IT adminstrator ' . $e->getMessage()], 400);
        }
    }

    public function addActivityLogCompanyEmployee(CompanyEmployee $companyEmployee)
    {
        $contact_types = ContactType::orderBy('type_name', 'asc')->get();
        $user = Auth::user();
        $callers = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin')->orWhere('name', 'clerk')->orWhere('name', 'app-admin')->orWhere('name', 'administrator')->orWhere('name', 'marketing');
            return $query;
        })->whereNull('users.deleted_at')->get();
        $receivers = CompanyEmployee::all();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.contact-logs.add-contact-log-for-contact', compact('companyEmployee', 'contact_types', 'user', 'callers'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.contact-log.add-contact-log-for-contact', compact('companyEmployee', 'contact_types', 'user', 'callers'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.contact-log.add-contact-log-for-contact', compact('companyEmployee', 'contact_types', 'user', 'callers'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.contact-logs.add-contact-log-for-contact', compact('companyEmployee', 'contact_types', 'user', 'callers'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.contact-logs.add-contact-log-for-contact', compact('companyEmployee', 'contact_types', 'user', 'callers'));
        } else {
            return view('campaign-manager.contact-logs.clerk.add-contact-log-for-contact', compact('companyEmployee', 'contact_types', 'user', 'callers'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\ContactLog $contactLog
     * @return \Illuminate\Http\Response
     */
    public function show(ContactLog $contactLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\ContactLog $contactLog
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactLog $contactLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\ContactLog $contactLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactLog $contactLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ContactLog $contactLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactLog $contactLog)
    {
        //
        $contactLog->delete();
        return redirect('contact-logs');
    }
}
