<?php

namespace App\Http\Controllers;

use App\Bootcamper;
use App\CompanyEmployee;
use App\CompanyEmployeeCovidForm;
use App\Event;
use App\EventInternalUser;
use App\EventVisitor;
use App\Incubatee;
use App\Mail\ContactMail;
use App\StaffCovidSymptom;
use App\VisitorBackUp;
use App\VisitorCovidSymptom;
use App\VisitorDeregister;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Visitor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rules\In;
use Postmark\PostmarkClient;
use Yajra\Datatables\Datatables;
use App\User;


class VisitorController extends Controller
{
    //This is the view for the datatable for all the visitors
    public function visitorsIndex()
    {
        $user = Auth::user();
        $visitors = Visitor::with('visitor_backup')->get();

        if ($user != null) {
            $user->load('roles');

            if ($user->roles[0]->name == 'app-admin') {
                return view('/users/app-admin/visitors/visitors-index', compact('visitors'));
            } else if ($user->roles[0]->name == 'administrator') {
                return view('/users/administrators/visitors/visitors-index', compact('visitors'));
            } else {
                return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
            }
        }
    }

    //This returns the datatable for the visitorsIndex view
    public function getVisitors()
    {
        $visitors = Visitor::all();
        $user = Auth::user()->load('roles');
        $visitor_array = [];

        foreach ($visitors as $visitor) {
            $visitor_backups = $visitor->visitor_backup()->get()->toArray();
            $last_visitor_backup = end($visitor_backups);

            $object = (object)['first_name' => $visitor->first_name, 'last_name' => $visitor->last_name,
                'cell_number' => $visitor->cell_number, 'land_line' => $visitor->land_line,
                'email' => $visitor->email, 'date_in' => $last_visitor_backup['date_in'], 'id' => $visitor->id];

            array_push($visitor_array, $object);
        }


        if ($user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'administrator') {
            return Datatables::of($visitor_array)->addColumn('action', function ($visitor) {
                $re = '/visitor-edit/' . $visitor->id;
                $del = $visitor->id;
                return '<a href=' . $re . ' title="Edit visitor" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' title="Delete visitor" onclick="deleteConfirm(this)" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //This returns the login view based on who is logged in
    //if visitor is trying to log in, it will take them to their normal visitor login
    //if clerk role is logged in, it will go to 'back end' clerk login, to track visits @ Propella.
    public function visitorLogin()
    {
        $user = Auth::user();

        if ($user == null) {
            return view('/visitors/visitor-login');
        } else {
            $user->load('roles');

            if ($user->roles[0]->name == 'clerk') {
                return view('/visitors/clerk/visitor-login');
            }
        }
    }

    //This returns the view to create a visitor (no login required)
    public function createVisitor()
    {
        return view('/visitors/create-visitor');
    }

    //This stores the visitor via the visitor login and not the clerk login
    public function visitorStore(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();
        $email = $input['email'];

        try {
            DB::beginTransaction();
            $existing_visitor = Visitor::where('email', $email)->first();

            if (isset($existing_visitor)) {
                return response()->json(['message' => 'This email is already in use.']);
            } else {


                $save_visitor = Visitor::create(['first_name' => $input['first_name'], 'last_name' => $input['last_name'],
                    'email' => $input['email'], 'cell_number' => $input['cell_number'], 'land_line' => $input['land_line'], 'company' => $input['company'],
                    'dob' => $input['dob'], 'title' => $input['title'], 'designation' => $input['designation'], 'date_in' => $date]);

                $save_visitor->save();


                DB::commit();


                return response()->json(['message' => 'Welcome ' . $input['first_name'] . ' ' . $input['last_name']], 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //This returns the create visitor view via clerk (to track visit)
    public function createVisitorViaClerk()
    {
        $events = Event::with('EventVenue')->orderBy('start', 'desc')->get();
        $today = Carbon::now()->toDateString();

        return view('/visitors/clerk/create-visitor', compact('events', 'today'));
    }

    //This stores the visitor and their visit on that day (appointment, event, etc)
    public function visitorStoreViaClerk(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();
        $email = $input['email'];


        DB::beginTransaction();
        try {
            $existing_visitor = Visitor::where('email', $email)->first();

            if (isset($existing_visitor)) {
                return response()->json(['message' => 'This email is already in use.']);
            } else {
                $save_visitor = Visitor::create(['first_name' => $input['first_name'], 'last_name' => $input['last_name'],
                    'email' => $input['email'], 'cell_number' => $input['cell_number'], 'land_line' => $input['land_line'], 'company' => $input['company'],
                    'dob' => $input['dob'], 'title' => $input['title'], 'designation' => $input['designation'], 'date_in' => $date
                ]);

                if ($request->has('found_us_via')) {
                    $save_visitor->visitor_backup()->create(['visitor_id' => $save_visitor->id, 'visit_reason' => $input['visit_reason'],
                            'date_in' => $date, 'visit_category' => $input['visit_category'], 'found_us_via' => $input['found_us_via']]
                    );

                } else if ($request->has('event_id')) {
                    $event_id = $input['event_id'];
                    $event = Event::find($event_id);

                    $save_visitor->visitor_backup()->create(['visitor_id' => $save_visitor->id, 'visit_reason' => $event->title,
                            'date_in' => $date, 'visit_category' => $input['visit_category']]
                    );


                    $event_visitor = EventVisitor::create(['visitor_id' => $save_visitor->id, 'event_id' => $event->id,
                        'registered' => true, 'date_time_registered' => $date, 'attended' => true]);

                    $event_visitor->save();

                } else {
                    $save_visitor->visitor_backup()->create(['visitor_id' => $save_visitor->id, 'visit_reason' => $input['visit_reason'],
                            'date_in' => $date, 'visit_category' => $input['visit_category']]
                    );
                }
                $vist_cat = $input['visit_category'];
                if ($vist_cat == 'Appointment') {
                    $data = array(
                        'name' => $input['first_name'],
                        'email' => $email
                    );

                    $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                    $fromEmail = "dev@propellaincubator.co.za";
                    $toEmail = $email;
                    $subject = "Propella Business Incubator Appreciation Mail";
                    $htmlBody = "<p>Dear <b> visitor</b></p><p>Thank you for visiting the Propella precinct today, we trust that your visit was productive.</p>
                    <br/>
                    <br/>
                    We look forward to seeing you again. Wishing you a great day further.
                    you will use your email $email to sign again when visiting.
                    <br/>
                    <br/>
                    Warm regards<br>
                    The Propella Team";
                    $textBody = "";
                    $tag = "example-email-tag";
                    $trackOpens = true;
                    $trackLinks = "None";
                    $messageStream = "broadcast";

                    // Send an email:
                    $sendResult = $client->sendEmail(
                        $fromEmail,
                        $toEmail,
                        $subject,
                        $htmlBody,
                        $textBody,
                        $tag,
                        $trackOpens,
                        NULL, // Reply To
                        NULL, // CC
                        NULL, // BCC
                        NULL, // Header array
                        NULL, // Attachment array
                        $trackLinks,
                        NULL, // Metadata array
                        $messageStream
                    );


                    /*$email_field = $email;
                    Mail::send('emails.appointment', $data, function ($message) use ($email_field) {
                        $message->to($email_field)
                            ->subject('Propella Business Incubator Appreciation Mail');
                        $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

                    });*/
                }elseif ($vist_cat == 'Event'){

                    $event_name = $event->title;
                    $event_date = $event->start;
                    $data = array(
                        'name' => $input['first_name'],
                        'email' => $email,
                        'event_name' => $event_name,
                        'event_date' => $event_date
                    );

                    $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                    $fromEmail = "dev@propellaincubator.co.za";
                    $toEmail = $email;
                    $subject = "THANK YOU FOR REGISTERING";
                    $htmlBody = "<p>Dear <b> visitor</b></p>";
                    $textBody = " Thank you for registering for the $event_name on $event_date.
                    <br/>
                    <br/>
                    We look forward to seeing you.
                    <br/>
                    <br/>
                    Warm regards<br>
                    The Propella Team
                    ";
                    $tag = "example-email-tag";
                    $trackOpens = true;
                    $trackLinks = "None";
                    $messageStream = "broadcast";

                    // Send an email:
                    $sendResult = $client->sendEmail(
                        $fromEmail,
                        $toEmail,
                        $subject,
                        $htmlBody,
                        $textBody,
                        $tag,
                        $trackOpens,
                        NULL, // Reply To
                        NULL, // CC
                        NULL, // BCC
                        NULL, // Header array
                        NULL, // Attachment array
                        $trackLinks,
                        NULL, // Metadata array
                        $messageStream
                    );


                    /* $email_field = $email;
                     Mail::send('emails.event', $data, function ($message) use ($email_field) {
                         $message->to($email_field)
                             ->subject('THANK YOU FOR REGISTERING');
                         $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

                     });*/
                }
                DB::commit();

                return response()->json(['message' => 'Welcome ' . $input['first_name'] . ' ' . $input['last_name']], 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //if the visitor chooses to create a new account and register for event via the clerk login screen
    //This function stores both the visitor and the event for the relationship
    public function storeVisitorViaEventRegister(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();
        $email = $input['email'];

        $existing_visitor = Visitor::where('email', $email)->first();

        if (isset($existing_visitor)) {
            return response()->json(['message' => 'This email already exists.']);
        } else {
            DB::beginTransaction();
            try {
                $save_visitor = Visitor::create(['first_name' => $input['first_name'], 'last_name' => $input['last_name'],
                    'email' => $input['email'], 'cell_number' => $input['cell_number'], 'land_line' => $input['land_line'], 'company' => $input['company'],
                    'dob' => $input['dob'], 'title' => $input['title'], 'designation' => $input['designation'], 'date_in' => $date
                ]);

                if ($request->has('event_id')) {
                    $event_id = $input['event_id'];
                    $found_out_via = $input['found_out_via'];

                    $event_visitor = EventVisitor::create(['visitor_id' => $save_visitor->id, 'event_id' => $event_id,
                        'registered' => true, 'date_time_registered' => $date, 'found_out_via' => $found_out_via]);

                    $event_visitor->save();
                }

                DB::commit();

                return response()->json(['message' => 'Welcome ' . $input['first_name'] . ' ' . $input['last_name']], 200);

            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
            }
        }
    }

    //When a visitor logs in via clerk login screen, it takes them to the clerk/visitor-edit page
    //We use this view to track visits
    public function editVisitorBackUpView(Visitor $visitor)
    {
        $visitor->load('visitor_backup');
        $events = Event::with('EventVenue')->orderBy('start', 'desc')->get();
        $today = Carbon::now()->toDateString();


        return view('/visitors/clerk/visitor-edit', compact('visitor', 'events', 'today'));
    }

    //When a incubatee logs in via clerk login screen, it takes them to the clerk/incubatee-visit-edit page
    //We use this view to track event attendance
    public function editIncubateeEventView(Incubatee $incubatee)
    {
        $events = Event::with('EventVenue')->orderBy('start','desc')->get();
        $today = Carbon::now()->toDateString();

        return view('/visitors/clerk/incubatee-visit-edit', compact('events', 'incubatee','today'));
    }

    //When a bootcamper logs in via clerk login screen, it takes them to the clerk/bootcamper-visit-edit page
    //We use this view to track event attendance
    public function editBootcamperEventView(Bootcamper $bootcamper){
        $bootcamper->load('events');
        $event = null;
        $today = Carbon::now()->toDateString();
        foreach ($bootcamper->events as $bootcamper_event){

            if (isset($bootcamper_event->event)){
                if($bootcamper_event->event->start->toDateString() == $today){
                    $event = $bootcamper_event;
                }
            }

        }

        return view('/visitors/clerk/bootcamper-visit-edit', compact('bootcamper','event'));
    }

    //When a user (internal team) logs in via clerk login screen, it takes them to the clerk/user-event-edit page
    //We use this view to track event attendance
    public function editUserEventView(User $user)
    {
        $events = Event::with('EventVenue')->orderBy('start','desc')->get();

        $today = Carbon::now()->toDateString();
        return view('/visitors/clerk/user-event-edit', compact('events', 'user','today'));
    }

    //If the visitor chooses to update his / her basic details via the clerk login screen
    //This function updates all the Visitor's basic details
    public function updateVisitorBasicDetails(Request $request)
    {
        $input = $request->all();
        $visitor_id = $input['visitor_id'];

        $cur_visitor = Visitor::find($visitor_id);

        try {
            $cur_visitor->update(['first_name' => $input['first_name'], 'last_name' => $input['last_name'],
                'email' => $input['email'], 'cell_number' => $input['cell_number'], 'land_line' => $input['land_line'],
                'company' => $input['company'],
                'dob' => $input['dob'], 'title' => $input['title'], 'designation' => $input['designation']
            ]);
            $cur_visitor->save();

            DB::commit();

            return response()->json(['message' => 'Your details were successfully updated.']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //The visitor must choose a reason for visit if they log in via clerk login
    //This function updates the reason for the visitor's visit
    //We have a lot of conditional statements here for the different type of visitor categories (appointment, event, supplier, other)
    public function updateVisitorBackUp(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();
        $visitor_id = $input['visitor_id'];
        $cur_visitor = Visitor::find($visitor_id);
        $cur_visitor->load('visitor_backup');



                DB::beginTransaction();
                try {
                    if ($request->has('event_id')) {
                        $event_id = $input['event_id'];
                        $check_event = Event::find($event_id);
                        $visitorBackup = VisitorBackUp::create(['visitor_id' => $cur_visitor->id, 'visit_reason' => $check_event->title,
                            'date_in' => $date, 'visit_category' => $input['visit_category']]);

                        $visitor_events = EventVisitor::where('visitor_id', $visitor_id)->get();

                        $bool_found = false;
                        foreach ($visitor_events as $visitor_event) {
                            if ($visitor_event->event_id == $event_id) {
                                $bool_found = true;
                                $visitor_event->attended = true;
                                $visitor_event->save();
                                DB::commit();
                                return response()->json(['visitor' => $cur_visitor, 'message' => 'Thank you for visiting again ' . $cur_visitor->first_name . ' ' . $cur_visitor->last_name], 200);
                            }
                        }

                        if (!$bool_found) {
                            $create_visitor_event = EventVisitor::create(['event_id' => $event_id, 'visitor_id' => $visitor_id,
                                'attended' => true, 'registered' => true, 'date_time_registered' => $date]);
                            DB::commit();
                            response()->json(["hit" => $create_visitor_event]);
                            return response()->json(['visitor' => $cur_visitor, 'message' => 'Thank you for visiting again ' . $cur_visitor->first_name . ' ' . $cur_visitor->last_name], 200);
                        }

                    } else {
                        if ($request->has('found_us_via')) {
                            $visitor_backup = VisitorBackUp::create(['visitor_id' => $cur_visitor->id, 'visit_reason' => $input['visit_reason'],
                                    'date_in' => $date, 'visit_category' => $input['visit_category'], 'found_us_via' => $input['found_us_via']]
                            );
                            DB::commit();
                            return response()->json(['visitor' => $cur_visitor, 'message' => 'Thank you for visiting again ' . $cur_visitor->first_name . ' ' . $cur_visitor->last_name], 200);
                        } else {
                            $visitor_backup = VisitorBackUp::create(['visitor_id' => $cur_visitor->id, 'visit_reason' => $input['visit_reason'],
                                    'date_in' => $date, 'visit_category' => $input['visit_category']]
                            );
                            DB::commit();
                            return response()->json(['visitor' => $cur_visitor, 'message' => 'Thank you for visiting again ' . $cur_visitor->first_name . ' ' . $cur_visitor->last_name], 200);
                        }
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 500);
                }


    }

    //When we get to the visitor-login via clerk or via public site
    //This function searches all the visitors and then the next screen displays according to the user access.
    public function searchVisitor(Request $request)
    {
        $input = $request->all();
        $visitor_email = $input['visitor_email'];

        try {
            $users = User::all();
            $cur_incubatee = null;
            $cur_bootcamper = null;
            $cur_user = null;
            $cur_visitor = Visitor::where('email', $visitor_email)->first();
            foreach ($users as $user) {
                if ($user->email == $visitor_email) {
                    $temp_incubatee = Incubatee::where('user_id', $user->id)->first();
                    $cur_incubatee = $temp_incubatee;
                    $temp_bootcamper = Bootcamper::where('user_id', $user->id)->first();
                    $cur_bootcamper = $temp_bootcamper;
                    $temp_user = User::find($user->id);
                    $cur_user = $temp_user;
                }
            }

             if (isset($cur_incubatee)) {
                return response()->json(['message' => 'Incubatee', 'incubatee' => $cur_incubatee]);
            } else if (isset($cur_bootcamper)) {
                return response()->json(['message' => 'Bootcamper', 'bootcamper' => $cur_bootcamper]);
            } else if (isset($cur_user)) {
                return response()->json(['message' => 'User', 'user' => $cur_user]);
            } else if (isset($cur_visitor)) {
                 return response()->json(['message' => 'Success', 'visitor_id' => $cur_visitor->id]);
             } else {
                return response()->json(['message' => 'You are not found in the database. Please register.']);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'You are not found in the database. Please register.' . $e->getMessage()]);
        }
    }

    //Returns the visitor dashboard view with the visitor as a parameter
    public function visitorDashboard(Visitor $visitor)
    {
        return view('/visitors/visitor-dashboard', compact('visitor'));
    }

    //Returns the visitor summary view (with all their visits to propella), with the visitor as a parameter
    public function visitorSummary(Visitor $visitor)
    {
        return view('/visitors/visitor-summary', compact('visitor'));
    }

    //Returns the datatable for the visitor summary page
    public function getVisitorSummary(Visitor $visitor)
    {
        $visitor->load('visitor_backup');

        if (isset($visitor)) {
            return Datatables::of($visitor->visitor_backup)->make(true);
        } else {
            return response('The system cannot pick up a visitor.');
        }
    }

    //Returns the visitor events view (with all events in the system, and all the events that he/she has registered for)
    public function visitorEvents(Visitor $visitor)
    {
        $visitor->load('events');
        $events = Event::with('EventVenue')->orderBy('start', 'desc')->get();
        $visitor_events = [];
        foreach ($visitor->events as $visitor_event) {
            $event_id = $visitor_event->event_id;
            $cur_event = Event::find($event_id);

            if (isset($cur_event)) {
                $object = (object)['visitor_event_registered' => $visitor_event, 'visitor_events_by_event_id' => $cur_event];
                array_push($visitor_events, $object);
            }
        }
        return view('/visitors/visitor-events', compact('visitor', 'events', 'visitor_events'));
    }


    //If the visitor has logged in with their 'account' on their own device and they chose to register for an event via that page
    //This function will do the validation check and store it accordingly
    public function storeVisitorEventViaVisitorEventPage(Request $request)
    {
        $input = $request->all();
        $event_id = $input['event_id'];
        $visitor_id = $input['visitor_id'];
        $cur_visitor = Visitor::find($visitor_id);
        $cur_event = Event::find($event_id);
        $date = Carbon::now();

        if (isset($cur_visitor) and isset($cur_event)) {
            $cur_visitor->load('events');

            foreach ($cur_visitor->events as $visitor_event) {
                if ($visitor_event->event_id == $event_id) {
                    return response()->json(['message' => 'You are already registered for this event.']);
                }
            }

            try {
                DB::beginTransaction();

                $event_visitor = EventVisitor::create(['visitor_id' => $visitor_id, 'event_id' => $event_id,
                    'registered' => true, 'date_time_registered' => $date, 'found_out_via' => $input['found_out_via']]);

                $event_visitor->save();

                DB::commit();

                return response()->json(['message' => 'Event now added to your registered list.']);

            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
            }
        } else {
            return response()->json(['message' => 'The system cannot pick up this event.']);
        }
    }

    //If the visitor chooses to deregister for an event via their own device
    //This functionality will do the validation accordingly and deregister that visitor for that event
    public function deregisterVisitor(Request $request)
    {
        $input = $request->all();
        $visitor_id = $input['visitor_id'];
        $event_id = $input['event_id'];
        $cur_visitor = Visitor::find($visitor_id);
        $cur_visitor->load('events');
        $date = Carbon::now();

        try {
            if (isset($cur_visitor)) {

                DB::beginTransaction();

                foreach ($cur_visitor->events as $visitor_event) {
                    if ($visitor_event->event_id == $event_id) {
                        $cur_event = Event::find($event_id);
                        $create_visitor_deregister = VisitorDeregister::create(['event_id' => $event_id, 'visitor_id' => $cur_visitor->id,
                            'de_registered' => true, 'de_register_reason' => $input['deregister_reason'],
                            'date_time_de_register' => $date, 'event_name' => $cur_event->title]);

                        $visitor_event->forceDelete();

                        DB::commit();
                        return response()->json(['message' => 'You are now de-registered for that event.']);
                    }
                }
            } else {
                return response()->json(['message' => 'The system could not pick up the visitor.']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //If the visitor chose an event on the website but have not registered an account before
    //This function will take the visitor to a create-visitor page but it stores the event_id to create the relationship
    public function createVisitorViaEvent()
    {
        return view('/visitors/create-visitor-via-event');
    }

    //This function is for admin that wants to edit a visitor
    //Returns the edit-visitor view with the visitor as a parameter
    public function adminEditVisitor(Visitor $visitor)
    {
        $visitor->load('events');
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/visitors/edit-visitor', compact('visitor'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/visitors/edit-visitor', compact('visitor'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //This function is for admin that wants to view all the registered events for a specific visitor
    //Returns the visitor-registered-event-index view with the visitor as a parameter
    public function adminGetVisitorRegisteredEventsIndex(Visitor $visitor)
    {
        $visitor->load('events');
        $events = Event::with('EventVenue', 'EventsMedia')->orderBy('start', 'DESC')->get();
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/visitors/visitor-registered-event-index', compact('visitor', 'events'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/visitors/visitor-registered-event-index', compact('visitor', 'events'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //This function returns a datatable of all the registered events for a specific visitor passed as a parameter
    //The datatable also allows the ability to edit a specific visitor-event and update the details of that visitor's event that he/she has registered for
    public function getVisitorEventsViaEditView(Visitor $visitor)
    {
        $visitor->load('events');

        if (isset($visitor->events)) {
            $visitor_events = [];

            foreach ($visitor->events as $visitor_event) {
                $event_id = $visitor_event->event_id;
                $event = Event::find($event_id);

                if ($visitor_event->attended == null or $visitor_event->attended == false) {
                    $object = (object)['event' => $event->title, 'date_registered' => $visitor_event->date_time_registered,
                        'attended' => 'No', 'event_id' => $visitor_event->id];
                    array_push($visitor_events, $object);
                } else {
                    $object = (object)['event' => $event->title, 'date_registered' => $visitor_event->date_time_registered,
                        'attended' => 'Yes', 'event_id' => $visitor_event->id];
                    array_push($visitor_events, $object);
                }
            }

            return Datatables::of($visitor_events)->addColumn('action', function ($event) {
                $re = '/visitor-event-edit/' . $event->event_id;
                return '<a href=' . $re . ' title="Edit visitor event" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response()->json(['message' => 'No events found for this visitor']);
        }
    }

    //This function is for admin that wants to view all the de-registered events for a specific visitor
    //Returns the visitor-registered-event-index view with the visitor as a parameter
    public function adminGetVisitorDeRegisteredEventsIndex(Visitor $visitor)
    {
        $visitor->load('visitorDeregisters');
        $events = Event::with('EventVenue', 'EventsMedia')->orderBy('start', 'DESC')->get();
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/visitors/visitor-de-registered-event-index', compact('visitor', 'events'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/visitors/visitor-de-registered-event-index', compact('visitor', 'events'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //This function returns a datatable of all the de-registered events for a specific visitor passed as a parameter
    public function getVisitorDeRegisteredEventsViaEditView(Visitor $visitor)
    {
        $visitor->load('visitorDeregisters');

        if (isset($visitor->visitorDeregisters)) {
            return Datatables::of($visitor->visitorDeregisters)->addColumn('action', function ($event) {
                $re = '/visitor-de-registered-event-edit/' . $event->id;
                $del = $event->id;
                return '<a href=' . $re . ' title="Edit de-registration" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="deleteConfirm(this)" title="Delete de-registration" style="color:red!important;"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response()->json(['message' => 'No events found for this visitor']);
        }
    }

    //This function allows admin to delete a visitor-deregistration record
    public function adminDeleteVisitorDeregisteredEvent(VisitorDeregister $visitorDeregister)
    {
        try {
            DB::beginTransaction();
            $visitorDeregister->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Visitor de-registration record deleted']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //This function returns the view that allows admin to edit visitor-de-registered-event
    public function adminEditVisitorDeregisteredEvent(VisitorDeregister $visitorDeregister)
    {
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/visitors/visitor-de-registered-event-edit', compact('visitorDeregister'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/visitors/visitor-de-registered-event-edit', compact('visitorDeregister'));
        } else {
            return response('You are not authorized!');
        }
    }

    //This function allows admin to update visitor de-registration reason only
    public function adminUpdateVisitorDeregisteredEvent(Request $request, VisitorDeregister $visitorDeregister)
    {
        $input = $request->all();
        try {
            DB::beginTransaction();
            $visitorDeregister->update(['de_register_reason' => $input['de_register_reason']]);
            DB::commit();
            return response()->json(['message' => 'Visitor de-registration successfully updated']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //This function allows admin to register an event for a visitor if the visitor could not register themselves perhaps
    public function adminRegisterVisitorEvent(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();

        try {
            DB::beginTransaction();
            if ($input['attended'] == 'Yes') {
                $create_visitor_event = EventVisitor::create(['event_id' => $input['event_id'], 'visitor_id' => $input['visitor_id'],
                    'attended' => true, 'date_time_registered' => $date, 'registered' => true]);
                DB::commit();
                return response()->json(['message' => 'Visitor now successfully registered for this event.']);
            } else {
                $create_visitor_event = EventVisitor::create(['event_id' => $input['event_id'], 'visitor_id' => $input['visitor_id'],
                    'attended' => false, 'date_time_registered' => $date, 'registered' => true]);
                DB::commit();
                return response()->json(['message' => 'Visitor now successfully registered for this event.']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //This function allows admin to update the visitor's basic details
    public function adminUpdateVisitorDetails(Request $request, Visitor $visitor)
    {
        $input = $request->all();

        try {
            DB::beginTransaction();
            $visitor->update(['first_name' => $input['first_name'], 'last_name' => $input['last_name'],
                'email' => $input['email'], 'cell_number' => $input['cell_number'], 'company' => $input['company'],
                'dob' => $input['dob'], 'title' => $input['title'], 'designation' => $input['designation']
            ]);

            DB::commit();

            return response()->json(['message' => 'Visitor successfully updated.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //This function allows for admin to edit a visitor's registered event
    //Returns the visitor-event-edit view with the related event + visitor model
    public function adminEditVisitorEvent(EventVisitor $eventVisitor)
    {
        $logged_in_user = Auth::user()->load('roles');
        $eventVisitor->load('visitor', 'event');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/visitors/visitor-event-edit', compact('eventVisitor'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/visitors/visitor-event-edit', compact('eventVisitor'));
        } else {
            return response('You are not authorized!');
        }
    }

    //This function saves the visitor event that the admin has updated
    //Based on the 'registered' input, the event is either updated or de-registered
    public function adminUpdateVisitorRegisteredEvent(Request $request, EventVisitor $eventVisitor)
    {
        $eventVisitor->load('event', 'visitor');
        $input = $request->all();

        try {
            if ($input['registered'] == 'Yes') {
                if ($input['attended'] == 'Yes') {
                    $eventVisitor->update(['registered' => true, 'attended' => true]);
                    $eventVisitor->save();
                    DB::commit();
                    return response()->json(['message' => 'Visitor event updated.']);
                } else {
                    $eventVisitor->update(['registered' => true, 'attended' => false]);
                    $eventVisitor->save();
                    DB::commit();
                    return response()->json(['message' => 'Visitor event updated.']);
                }
            } else if ($input['registered'] == 'No') {
                $event_id = $input['event_id'];
                $cur_event = Event::find($event_id);
                $create_visitor_de_register = VisitorDeregister::create(['event_id' => $input['event_id'],
                    'visitor_id' => $input['visitor_id'], 'de_registered' => true, 'de_register_reason' => $input['de_registered-reason'],
                    'date_time_de_register' => $input['de-registered_date_time'], 'event_name' => $cur_event->title]);
                $eventVisitor->forceDelete();
                DB::commit();
                return response()->json(['message' => 'Visitor event now added to de-registered list and removed off registered list.']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
        return $input;
    }

    public function adminGetVisitorSummaryIndex(Visitor $visitor)
    {
        $visitor->load('visitor_backup');
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/visitors/visitor-summary-index', compact('visitor'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/visitors/visitor-summary-index', compact('visitor'));
        } else {
            return response('You are not authorized!');
        }
    }

    //Returns the datatable for the visitor summary page for the admin
    public function adminGetVisitorSummary(Visitor $visitor)
    {
        $visitor->load('visitor_backup');
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return Datatables::of($visitor->visitor_backup)->addColumn('action', function ($visitor_backup) {
                $del = $visitor_backup->id;
                return '<a id=' . $del . ' onclick="deleteConfirm(this)" title="Delete visit" style="color:red!important;"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($visitor->visitor_backup)->make(true);
        } else {
            return response('The system cannot pick up a visitor.');
        }
    }

    //This function allows for admin to delete a visitor's visit
    public function adminDeleteVisitorSummary(VisitorBackUp $visitorBackup)
    {
        try {
            DB::beginTransaction();
            $visitorBackup->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Visitor record deleted']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //This function is to delete a visitor and all the related database relationships
    public function deleteVisitor(Visitor $visitor)
    {
        try {
            DB::beginTransaction();
            $visitor->load('events', 'visitor_backup', 'visitorDeregisters');

            if (isset($visitor)) {
                if (isset($visitor->events)) {
                    foreach ($visitor->events as $visitor_event) {
                        $visitor_event->forceDelete();
                    }
                }

                if (isset($visitor->visitor_backup)) {
                    foreach ($visitor->visitor_backup as $visitor_backup) {
                        $visitor_backup->forceDelete();
                    }
                }

                if (isset($visitor->visitorDeregisters)) {
                    foreach ($visitor->visitorDeregisters as $visitor_deregister) {
                        $visitor_deregister->forceDelete();
                    }
                }

                $visitor->forceDelete();
                DB::commit();
                return response()->json(['message', 'Visitor and relationships has been successfully deleted'], 200);
            } else {
                return response()->json(['message', 'Visitor could not be found!'], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message', 'Could not delete this visitor. Reason: ' . $e->getMessage()]);
        }

    }

    public function updateUserEvent(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();
        $user_id = $input['user_id'];
        $cur_user = User::find($user_id);
        $cur_user->load('events');

        DB::beginTransaction();
        try {
            if(isset($cur_user)){
                if ($request->has('event_id')) {
                    $event_id = $input['event_id'];
                    $check_event = Event::find($event_id);

                    $user_events = EventInternalUser::where('user_id', $user_id)->get();


                    foreach ($user_events as $user_event) {
                        if ($user_event->event_id == $event_id) {
                            $bool_found = true;
                            $user_event->attended = true;
                            $user_event->save();
                            DB::commit();
                            return response()->json(['visitor' => $cur_user, 'message' => 'Enjoy your event / workshop!'], 200);
                        }
                    }


                    $create_user_event = EventInternalUser::create(['event_id' => $event_id, 'user_id' => $user_id,
                        'attended' => true, 'registered' => true, 'de_registered' => false, 'date_time_registered' => $date]);
                    DB::commit();
                    return response()->json(['user' => $cur_user, 'message' => 'Enjoy your event / workshop!'], 200);


                } else {
                    return response()->json(['message', 'Are you sure you chose and event?']);
                } }else{
                return response()->json(['message', 'Not part of users?']);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 500);
        }


    }

    //COVID LOGIN
    public function visitorCovidLogin()
    {
        $user = Auth::user();

        if ($user == null) {
            return view('/visitors/covid-login');
        } else {
            $user = Auth::user()->load('roles');

            if ($user->roles[0]->name == 'app-admin' and $user->roles[0]->name == 'administrator'
                and $user->roles[0]->name == 'incubatee' and $user->roles[0]->name == 'partner'
                and $user->roles[0]->name == 'applicant' and $user->roles[0]->name == 'marketing'
                and $user->roles[0]->name == 'advisor'  and $user->roles[0]->name == 'mentor'
                and $user->roles[0]->name == 'panelist' and $user->roles[0]->name == 'bootcamper') {
                return view('/visitors/covid-login');
            }
        }
    }


    //COVID LOGIN
    public function employeesCovidLogin()
    {
        $user = Auth::user();

        if ($user == null) {
            return view('/visitors/company-employees-covid-login');
        } else {
            $user = Auth::user()->load('roles');

            if ($user->roles[0]->name == 'app-admin' and $user->roles[0]->name == 'administrator'
                and $user->roles[0]->name == 'incubatee' and $user->roles[0]->name == 'partner'
                and $user->roles[0]->name == 'applicant' and $user->roles[0]->name == 'marketing'
                and $user->roles[0]->name == 'advisor'  and $user->roles[0]->name == 'mentor'
                and $user->roles[0]->name == 'panelist' and $user->roles[0]->name == 'bootcamper') {
                return view('/visitors/company-employees-covid-login');
            }
        }
    }

    //Returns the visitor dashboard view with the visitor as a parameter
    public function covidVisitorDashboard(Visitor $visitor)
    {
        return view('visitors.covid-visitor-dashboard', compact('visitor'));
    }

    //VISITOR STORE COVID FORMS
    public function visitorStoreCovidForms(Request $request)
    {
        $input = $request->all();
        $visitor_id = $input['visitor_id'];

        $cur_visitor = Visitor::find($visitor_id);
        $today = Carbon::now()->toDateString();
        try {

            $cur_visitor->save();
            $vistorCovidForm = VisitorCovidSymptom::create(['covid_symptoms_one'=>$input['covid_symptoms_one'],
                'covid_symptoms_two'=>$input['covid_symptoms_two'],'declaration'=>$input['declaration'],
                'digitally_signed_declaration'=>$input['digitally_signed_declaration'],
                'health_declaration'=>$input['health_declaration'],
                'visitor_id'=>$cur_visitor->id,'date_visited'=>$today,'temperature'=>$input['temperature'],
                'is_vaccinated'=>$input['is_vaccinated']]);
            $vistorCovidForm->save();
            $cur_visitor->update(['is_vaccinated'=>$vistorCovidForm->is_vaccinated]);
            DB::commit();

            return response()->json(['message' => 'Thank you.' .$cur_visitor->first_name]);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //GET VISITOR COVID FORMS
    public function visitorCovidFormsIndex(){

        return view('visitors.covid-visitor-index');
    }

    public function getVisitorCovidForms()
    {
        $covid = VisitorCovidSymptom::with('visitor')->get();

        $visitor_array = [];

        foreach ($covid as $visitor_arrays) {
            $visitor_arrays->load('visitor')->get();

            $object = (object)[
                'covid_symptoms_one' => isset($visitor_arrays) ? $visitor_arrays->covid_symptoms_one: 'not selected',
                'covid_symptoms_two' => isset($visitor_arrays) ? $visitor_arrays->covid_symptoms_two: 'not selected',
                'health_declaration' => isset($visitor_arrays) ? $visitor_arrays->health_declaration: 'not selected',
                'declaration' => isset($visitor_arrays) ? $visitor_arrays->declaration: 'not selected',
                'temperature' => isset($visitor_arrays) ? $visitor_arrays->temperature: 'not temperature',
                'digitally_signed_declaration' => isset($visitor_arrays) ? $visitor_arrays->digitally_signed_declaration: 'not selected',
                'date_visited' => $visitor_arrays->date_visited,
                'first_name' => isset($visitor_arrays->visitor) ? $visitor_arrays->visitor->first_name: 'no name',
                'last_name' => isset($visitor_arrays->visitor) ? $visitor_arrays->visitor->last_name: 'no surname',
                'email' => isset($visitor_arrays->visitor) ? $visitor_arrays->visitor->email: 'no email',
                'is_vaccinated'=>$visitor_arrays->is_vaccinated ? 'Yes' : 'No',
                'date_of_vaccine'=>$visitor_arrays->date_of_vaccine,
                'id' => $visitor_arrays->id];
            array_push($visitor_array, $object);
        }



        return Datatables::of($visitor_array)->addColumn('action', function ($user) {
            $del = '/delete-visitor-covid-form/' . $user->id;
            return '<a href=' . $del . ' onclick="confirm_delete_users(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
        })
            ->ToJson();
    }

    //Returns the visitor dashboard view with the visitor as a parameter
    public function covidAllUsersDashboard(User $user)
    {
        return view('visitors.covid-all-users-dashboard', compact('user'));
    }

    //Returns the employees dashboard view with the visitor as a parameter
    public function covidEmployeesDashboard(CompanyEmployee $companyEmployee)
    {
        return view('visitors.covid-employees-dashboard', compact('companyEmployee'));
    }

    //COVID LOGIN
    public function staffCovidLogin()
    {
        return view('visitors.staff-covid-login');
    }

    //When we get to the stagg-login via clerk or via public site
    //This function searches all the staff and then the next screen displays according to the user access.
    public function searchStaff(Request $request)
    {
        $input = $request->all();
        $staff_email = $input['email'];

        try {

            $cur_staff = User::where('email', $staff_email)->first();

            if (isset($cur_incubatee)) {
                return response()->json(['message' => 'Incubatee', 'incubatee' => $cur_incubatee]);
            } else if (isset($cur_bootcamper)) {
                return response()->json(['message' => 'Bootcamper', 'bootcamper' => $cur_bootcamper]);
            } else if (isset($cur_staff)) {
                return response()->json(['message' => 'Success', 'user_id' => $cur_staff->id]);
            } else if (isset($cur_visitor)) {
                return response()->json(['message' => 'Success', 'visitor_id' => $cur_visitor->id]);
            } else {
                return response()->json(['message' => 'You are not found in the database. Please register.']);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'You are not found in the database. Please register.' . $e->getMessage()]);
        }
    }

    //When we get to the company-login via clerk or via public site
    //This function searches all the employees and then the next screen displays according to the user access.
    public function searchEmployees(Request $request)
    {
        $input = $request->all();
        $staff_email = $input['email'];

        try {

            $cur_staff = CompanyEmployee::where('email', $staff_email)->first();

            if (isset($cur_staff)) {
                return response()->json(['message' => 'Success', 'company_employee_id' => $cur_staff->id]);
            } else {
                return response()->json(['message' => 'You are not found in the database. Please register.']);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'You are not found in the database. Please register.' . $e->getMessage()]);
        }
    }

    //STAFF STORE COVID FORMS
    public function staffStoreCovidForms(Request $request)
    {
        $input = $request->all();
        $user_id = $input['user_id'];

        $cur_user = User::find($user_id);
        $today = Carbon::now()->toDateString();
        try {

            $staffCovidForm = StaffCovidSymptom::create(['staff_covid_symptoms_one'=>$input['staff_covid_symptoms_one'],
                'staff_covid_symptoms_two'=>$input['staff_covid_symptoms_two'],'staff_declaration'=>$input['staff_declaration'],
                'staff_digitally_signed_declaration'=>$input['staff_digitally_signed_declaration'],
                'staff_health_declaration'=>$input['staff_health_declaration'],
                'user_id'=>$cur_user->id,'date'=>$today,'staff_temperature'=>$input['staff_temperature'],
                'is_vaccinated'=>$input['is_vaccinated']]);
            $staffCovidForm->save();
            $cur_user->update(['is_vaccinated' => $staffCovidForm->is_vaccinated]);
            $cur_user->save();
            DB::commit();

            return response()->json(['message' => 'Thank you.' .$cur_user->name]);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Delete visitor covid form
    public function adminDeleteVisitorForm(VisitorCovidSymptom $visitorCovidSymptom){

        try {
            DB::beginTransaction();
            $visitorCovidSymptom->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Successfully deleted form.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 400);
        }

    }

    //Delete staff covid form
    public function adminDeleteStaffForm(StaffCovidSymptom $staffCovidSymptom){

        try {
            DB::beginTransaction();
            $staffCovidSymptom->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Successfully deleted form.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 400);
        }

    }

    //Delete employees covid form
    public function adminDeleteCompanyEmployeesForm(CompanyEmployeeCovidForm $companyEmployeeCovidForm){

        try {
            DB::beginTransaction();
            $companyEmployeeCovidForm->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Successfully deleted form.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 400);
        }

    }


    //STAFF STORE COVID FORMS
    public function employeesStoreCovidForms(Request $request)
    {
        $input = $request->all();
        $company_employee_id = $input['company_employee_id'];

        $cur_employee = CompanyEmployee::find($company_employee_id);
        $today = Carbon::now()->toDateString();
        try {

            $employeeCovidForm = CompanyEmployeeCovidForm::create(['employee_covid_symptoms_one'=>$input['employee_covid_symptoms_one'],
                'employee_covid_symptoms_two'=>$input['employee_covid_symptoms_two'],'employee_declaration'=>$input['employee_declaration'],
                'employee_digitally_signed_declaration'=>$input['employee_digitally_signed_declaration'],
                'employee_health_declaration'=>$input['employee_health_declaration'],
                'company_employee_id'=>$cur_employee->id,'date'=>$today,'employee_temperature'=>$input['employee_temperature'],
                'is_vaccinated'=>$input['is_vaccinated']]);
            $employeeCovidForm->save();
            $cur_employee->update(['is_vaccinated' =>$employeeCovidForm->is_vaccinated]);
            $cur_employee->save();
            DB::commit();

            return response()->json(['message' => 'Thank you.' .$cur_employee->name]);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //GET STAFF COVID FORMS
    public function staffCovidFormsIndex(){

        return view('visitors.covid-staff-index');
    }

    public function getStaffCovidForms()
    {
        $covidStff = StaffCovidSymptom::with('user')->get();

        $covidStff_array = [];

        foreach ($covidStff as $visitor_arrays) {
            $visitor_arrays->load('user')->get();

            $object = (object)[
                'staff_covid_symptoms_one' => isset($visitor_arrays) ? $visitor_arrays->staff_covid_symptoms_one: 'not selected',
                'staff_covid_symptoms_two' => isset($visitor_arrays) ? $visitor_arrays->staff_covid_symptoms_two: 'not selected',
                'staff_health_declaration' => isset($visitor_arrays) ? $visitor_arrays->staff_health_declaration: 'not selected',
                'staff_declaration' => isset($visitor_arrays) ? $visitor_arrays->staff_declaration: 'not selected',
                'staff_temperature' => isset($visitor_arrays) ? $visitor_arrays->staff_temperature: 'not temperature',
                'staff_digitally_signed_declaration' => isset($visitor_arrays) ? $visitor_arrays->staff_digitally_signed_declaration: 'not selected',
                'date' => $visitor_arrays->date,
                'name' => isset($visitor_arrays->user) ? $visitor_arrays->user->name: 'no name',
                'surname' => isset($visitor_arrays->user) ? $visitor_arrays->user->surname: 'no surname',
                'email' => isset($visitor_arrays->user) ? $visitor_arrays->user->email: 'no email',
                'is_vaccinated'=>isset($visitor_arrays) ? $visitor_arrays->is_vaccinated : 'none',
                'date_of_vaccine'=>$visitor_arrays->date_of_vaccine,
                'id' => $visitor_arrays->id];
            array_push($covidStff_array, $object);
        }

        return Datatables::of($covidStff_array)->addColumn('action', function ($user) {
            $del = '/delete-staff-covid-form/' . $user->id;
            return '<a href=' . $del . ' onclick="confirm_delete_users(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
        })
            ->ToJson();
    }

    //GET EMPLOYEES COVID FORMS
    public function employeesCovidFormsIndex(){

        return view('visitors.covid-employees-index');
    }

    public function getEmployeesCovidForms()
    {
        $covid = CompanyEmployeeCovidForm::all()/*with('companyEmployees')->get()*/;
        $users_array = [];
        foreach ($covid as $covidform){
            $covidform->load('companyEmployees');
            $object = (object)[
                'name' => $covidform->companyEmployees->name,
                'surname' => $covidform->companyEmployees->surname,
                'email' => $covidform->companyEmployees->email,
                'employee_temperature'=>$covidform->employee_temperature,
                'employee_covid_symptoms_one'=>$covidform->employee_covid_symptoms_one,
                'employee_covid_symptoms_two'=>$covidform->employee_covid_symptoms_two,
                'employee_health_declaration'=>$covidform->employee_health_declaration,
                'employee_declaration'=>$covidform->employee_declaration,
                'employee_digitally_signed_declaration'=>$covidform->employee_digitally_signed_declaration,
                'is_vaccinated'=>$covidform->is_vaccinated ? 'Yes' : 'No',
                'date'=>$covidform->date,
                'date_of_vaccine'=>$covidform->date_of_vaccine,
                'id' => $covidform->id,
            ];
            array_push($users_array, $object);
        }

        return Datatables::of($users_array)->addColumn('action', function ($user) {
            $del = '/delete-employee-covid-form/' . $user->id;
            return '<a href=' . $del . ' onclick="confirm_delete_users(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
        })
            ->ToJson();
    }
}
