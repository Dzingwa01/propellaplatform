<?php

namespace App\Http\Controllers;

use App\CompanyEmployee;
use App\Incubatee;
use App\Mail\SendBulkMail;
use App\User;
use App\Venture;
use App\Role;
use App\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;

class BulkMailerController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Ventures
    public function allVentureIndex()
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            $incubatees = Incubatee::with( 'user','venture')->get();
            return view('bulk-mailer.all-ventures-index',compact('incubatees'));
        }elseif ($user->roles[0]->name == 'advisor') {
            $incubatees = Incubatee::with( 'user','venture')->get();
            return view('users.advisor-dashboard.bulk-mailer.all-ventures-index',compact('incubatees'));
        }elseif ($user->roles[0]->name == 'marketing') {
            $incubatees = Incubatee::with( 'user','venture')->get();
            return view('users.marketing-dashboard.bulk-mailer.all-ventures-index',compact('incubatees'));
        }
    }

    public function getAllVentures(){
        $incubatees = Incubatee::with( 'user','venture')->get();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';
            })
                ->make(true);
        }elseif  ($user->roles[0]->name == 'advisor') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';
            })
                ->make(true);
        }elseif  ($user->roles[0]->name == 'marketing') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';
            })
                ->make(true);
        }
    }

    public function sendBulkEmailToVenture(Request $request){
        $input = $request->input();
        $message = $input['message'];
        $subject = $input['subject'];
        $valid_emails = [];

        try{
            if($input['selection_type']=='multi'){
                foreach (json_decode($input['users']) as $user_id){
                    $user = Incubatee::find($user_id);
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }else{
                $users = Incubatee::whereHas('roles',function($query){
                    $query->where('name','!=','app-admin')->where('name','!=','clerk');
                    return $query;
                });

                foreach ($users as $user){
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }
//            return response()->json($valid_emails);
            /**
             * Chucking emails into sets of 50
             * Its recommended to send at most 50 at a time
             */
            $mail_chunks = array_chunk($valid_emails,50,true);

            $count = 0;
            if($request->has('attachment')) {
                $path = $request->file('attachment')->store('attachments');
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,url('/').'/storage/'.$path));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }

            }else{
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,null));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }
            }

            return response()->json(['message'=>'Emails sent successfully','valid_email'=>$valid_emails,'mail chuncks'=>$mail_chunks,200]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Emails could not be sent, please try again. If the problem persists contact the System Admin '.$e->getMessage(),'valid'=>$valid_emails,400]);
        }

    }

    public function uploadEmbedsForVenture(Request $request){
        if($request->has('image')) {
            $path = $request->file('image')->store('attachments');
            return $path;
        }
        return null;
    }

    //Companies
    public function index(){
        $user = Auth::user()->load('roles');
        if($user->roles[0]->name=='app-admin'){
            $companiesEmployees = CompanyEmployee::with('categories', 'company')->get();
            return view('bulk-mailer.index',compact('companiesEmployees'));
        }elseif($user->roles[0]->name=='advisor'){
            $companiesEmployees = CompanyEmployee::with('categories', 'company')->get();
            return view('users.advisor-dashboard.bulk-mailer.index',compact('companiesEmployees'));
        }elseif($user->roles[0]->name=='marketing'){
            $companiesEmployees = CompanyEmployee::with('categories', 'company')->get();
            return view('users.marketing-dashboard.bulk-mailer.index',compact('companiesEmployees'));
        } else{
            $companiesEmployees = CompanyEmployee::with('categories', 'company')->get();
            return view('bulk-mailer.agent-index',compact('companiesEmployees'));
        }
    }

    public function getContacts(){
        $companiesEmployees = CompanyEmployee::with('categories', 'company')->get();

        return Datatables::of($companiesEmployees)->addColumn('action', function ($employee) {
            return '<input type="checkbox" class="check" id='.$employee->id.' value='.$employee->id.' name='.$employee->email.' />';
//            return '<a href=' . $re . ' title="Assign Agent Stock" style="color:green;margin-right: 2em;"><i class="material-icons">assignment_turned_in</i></a><a href=' . $del . ' title="View Assigned Stock" style="color:blue"><i class="material-icons">remove_red_eye</i></a>';
        })
            ->make(true);

    }

    public function sendBulkEmailToVisitor(Request $request){
        $input = $request->input();
//            dd($input);
        $message = $input['message'];
        $subject = $input['subject'];
        $valid_emails = [];

        try{
            if($input['selection_type']=='multi'){
                foreach (json_decode($input['users']) as $user_id){
                    $user = Visitor::find($user_id);
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }else{
                $users = Visitor::whereHas('roles',function($query){
                    $query->where('name','!=','app-admin')->where('name','!=','clerk');
                    return $query;
                });

                foreach ($users as $user){
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }
//            return response()->json($valid_emails);
            /**
             * Chucking emails into sets of 50
             * Its recommended to send at most 50 at a time
             */
            $mail_chunks = array_chunk($valid_emails,50,true);

            $count = 0;
            if($request->has('attachment')) {
                $path = $request->file('attachment')->store('attachments');
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,url('/').'/storage/'.$path));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }

            }else{
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,null));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }
            }

            return response()->json(['message'=>'Emails sent successfully','valid_email'=>$valid_emails,'mail chuncks'=>$mail_chunks,200]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Emails could not be sent, please try again. If the problem persists contact the System Admin '.$e->getMessage(),'valid'=>$valid_emails,400]);
        }

    }


    public function sendBulkEmail(Request $request){
        $input = $request->input();
//            dd($input);
        $message = $input['message'];
        $subject = $input['subject'];
        $valid_emails = [];

        try{
            if($input['selection_type']=='multi'){
                foreach (json_decode($input['users']) as $user_id){
                    $user = CompanyEmployee::find($user_id);
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }else{
                $users = CompanyEmployee::whereHas('roles',function($query){
                    $query->where('name','!=','app-admin')->where('name','!=','clerk');
                    return $query;
                });

                foreach ($users as $user){
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }
//            return response()->json($valid_emails);
            /**
             * Chucking emails into sets of 50
             * Its recommended to send at most 50 at a time
             */
            $mail_chunks = array_chunk($valid_emails,50,true);

            $count = 0;
            if($request->has('attachment')) {
                $path = $request->file('attachment')->store('attachments');
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,url('/').'/storage/'.$path));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }

            }else{
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,null));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }
            }

            return response()->json(['message'=>'Emails sent successfully','valid_email'=>$valid_emails,'mail chuncks'=>$mail_chunks,200]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Emails could not be sent, please try again. If the problem persists contact the System Admin '.$e->getMessage(),'valid'=>$valid_emails,400]);
        }

    }

    public function uploadEmbeds(Request $request){
        if($request->has('image')) {
            $path = $request->file('image')->store('attachments');
            return $path;
        }
        return null;
    }

    //All Users
    public function allUsersIndex(){
        $users = Auth::user()->load('roles');
        if($users->roles[0]->name=='app-admin'){
            $users = User::all();
            return view('bulk-mailer.all-users-emailer', compact('users'));
        }elseif ($users->roles[0]->name=='marketing'){
            $users = User::all();
            return view('users.marketing-dashboard.bulk-mailer.all-users-emailer', compact('users'));
        }
    }

    public function sendEmailToVentures(){
        $users = Auth::user()->load('roles');
        $incubatees = Incubatee::with( 'user','venture')->get();

        if($users->roles[0]->name=='app-admin'){
            return view('bulk-mailer.send-email-to-ventures',compact('incubatees'));
        }
    }

    public function getAllUsersMailer()
    {
        $users = User::all();

        return Datatables::of($users)->addColumn('action', function ($user) {
            return '<input type="checkbox" class="check" id='.$user->id.' value='.$user->id.' name='.$user->email.' />';
//            return '<a href=' . $re . ' title="Assign Agent Stock" style="color:green;margin-right: 2em;"><i class="material-icons">assignment_turned_in</i></a><a href=' . $del . ' title="View Assigned Stock" style="color:blue"><i class="material-icons">remove_red_eye</i></a>';
        })
            ->make(true);

//        $logged_in_user = Auth::user()->load('roles');
//        $users = User::all();
//        $users_array = [];
//
//        foreach ($users as $user){
//            $user->load('roles');
//            $object = (object)['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
//                'contact_number' => $user->contact_number, 'land_line' => $user->land_line, 'role' => $user->roles[0]->name, 'id' => $user->id];
//            array_push($users_array, $object);
//        }
//
//        if ($logged_in_user->roles[0]->name == 'app-admin') {
//            return Datatables::of($users_array)->addColumn('action', function ($user) {
//                return '<input type="checkbox" class="check" id='.$user->id.' value='.$user->id.' name='.$user->email.' />';
//            })
//                ->make(true);
//        }
    }

    public function sendUsersBulkEmail(Request $request){
        $input = $request->input();
//            dd($input);
        $message = $input['message'];
        $subject = $input['subject'];
        $valid_emails = [];

        try{
            if($input['selection_type']=='multi'){
                foreach (json_decode($input['users']) as $user_id){
                    $user = User::find($user_id);
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }else{
                $users = User::whereHas('roles',function($query){
                    $query->where('name','!=','app-admin')->where('name','!=','clerk');
                    return $query;
                });

                foreach ($users as $user){
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }
//            return response()->json($valid_emails);
            /**
             * Chucking emails into sets of 50
             * Its recommended to send at most 50 at a time
             */
            $mail_chunks = array_chunk($valid_emails,50,true);

            $count = 0;
            if($request->has('attachment')) {
                $path = $request->file('attachment')->store('attachments');
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,url('/').'/storage/'.$path));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }

            }else{
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,null));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }
            }

            return response()->json(['message'=>'Emails sent successfully','valid_email'=>$valid_emails,'mail chuncks'=>$mail_chunks,200]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Emails could not be sent, please try again. If the problem persists contact the System Admin '.$e->getMessage(),'valid'=>$valid_emails,400]);
        }

    }

    public function uploadUsersEmbeds(Request $request){
        if($request->has('image')) {
            $path = $request->file('image')->store('attachments');
            return $path;
        }
        return null;
    }


    //Visitors
    public function visitorIndex(){
        $user = Auth::user()->load('roles');
        if($user->roles[0]->name=='app-admin'){
            $visitors = Visitor::all();
            return view('bulk-mailer.visitor-emailer',compact('visitors'));
        }elseif($user->roles[0]->name=='marketing'){
            $visitors = Visitor::with('visitor_backup')->get();
            return view('users.marketing-dashboard.bulk-mailer.visitor-emailer',compact('visitors'));
        } else{
            $visitors = Visitor::with('visitor_backup')->get();
            return view('bulk-mailer.agent-visitor-emailer',compact('visitors'));
        }
    }

    public function getVisitorsContacts(){
        $visitors = Visitor::all();


        return Datatables::of($visitors)->addColumn('action', function ($visitor) {
            return '<input type="checkbox" class="check" id='.$visitor->id.' value='.$visitor->id.' name='.$visitor->email.' />';
//            return '<a href=' . $re . ' title="Assign Agent Stock" style="color:green;margin-right: 2em;"><i class="material-icons">assignment_turned_in</i></a><a href=' . $del . ' title="View Assigned Stock" style="color:blue"><i class="material-icons">remove_red_eye</i></a>';
        })
            ->make(true);

    }

    public function sendVisitorBulkEmail(Request $request){
        $input = $request->input();
//            dd($input);
        $message = $input['message'];
        $subject = $input['subject'];
        $valid_emails = [];

        try{
            if($input['selection_type']=='multi'){
                foreach (json_decode($input['users']) as $user_id){
                    $user = Visitor::find($user_id);
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }else{
                $users = Visitor::whereHas('roles',function($query){
                    $query->where('name','!=','app-admin')->where('name','!=','clerk');
                    return $query;
                });

                foreach ($users as $user){
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }
//            return response()->json($valid_emails);
            /**
             * Chucking emails into sets of 50
             * Its recommended to send at most 50 at a time
             */
            $mail_chunks = array_chunk($valid_emails,50,true);

            $count = 0;
            if($request->has('attachment')) {
                $path = $request->file('attachment')->store('attachments');
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,url('/').'/storage/'.$path));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }

            }else{
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,null));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }
            }

            return response()->json(['message'=>'Emails sent successfully','valid_email'=>$valid_emails,'mail chuncks'=>$mail_chunks,200]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Emails could not be sent, please try again. If the problem persists contact the System Admin '.$e->getMessage(),'valid'=>$valid_emails,400]);
        }

    }

    public function uploadVisitorEmbeds(Request $request){
        if($request->has('image')) {
            $path = $request->file('image')->store('attachments');
            return $path;
        }
        return null;
    }


}
