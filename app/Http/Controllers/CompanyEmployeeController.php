<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\CompanyEmployee;
use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class CompanyEmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $categories = Category::orderBy('category_name', 'asc')->get();
        $companies = Company::orderBy('company_name', 'asc')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.employees-index', compact('categories', 'companies'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.campaign-manager.employees-index', compact('categories', 'companies'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.campaign-manager.employees-index', compact('categories', 'companies'));
        }elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.campaign-manager.employees-index', compact('company', 'companies', 'categories'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.campaign-manager.employees-index',compact('categories', 'companies'));
        }else {
            return view('campaign-manager.clerk.employees-index', compact('categories', 'companies'));
        }

    }

    public function getCompanyEmployee()
    {
        $companiesEmployees = CompanyEmployee::with('categories', 'company')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    $del = '/company-employee/delete/' . $employee->id;
                    return '<a href=' . $sh . ' title="View Person Details"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Company" style="color:red"><i class="material-icons">delete_forever</i></a>';

                })->make(true);
        } elseif ($user->roles[0]->name == "administrator") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    return '<a href=' . $sh . ' title="View Person Details"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a>';

                })->make(true);
        } elseif ($user->roles[0]->name == "marketing") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    return '<a href=' . $sh . ' title="View Person Details"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a>';

                })->make(true);
        } elseif ($user->roles[0]->name == "advisor") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    return '<a href=' . $sh . ' title="View Person Details"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a>';

                })->make(true);
        } elseif ($user->roles[0]->name == "mentor") {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    return '<a href=' . $sh . ' title="View Person Details"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a>';

                })->make(true);
        } else {
            return Datatables::of($companiesEmployees)
                ->addColumn('action', function ($employee) {
                    $re = '/company-employee/' . $employee->id;
                    $sh = '/company-employee/show/' . $employee->id;
                    return '<a href=' . $sh . ' title="View Person Details"><i class="material-icons">remove_red_eye</i></a><a href=' . $re . ' title="Edit Company" style="color:green!important;"><i class="material-icons">create</i></a>';

                })->make(true);
        }

    }

    public function create(Company $company)
    {
        //
        $categories = Category::orderBy('category_name', 'asc')->get();
        $companies = Company::orderBy('company_name', 'asc')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.company-employees-create', compact('categories', 'companies', 'company'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.campaign-manager.company-employees-create', compact('categories', 'companies', 'company'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.campaign-manager.company-employees-create', compact('categories', 'companies', 'company'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.campaign-manager.company-employees-create', compact('categories', 'companies', 'company'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.campaign-manager.company-employees-create', compact('categories', 'companies', 'company'));
        } else {
            return view('campaign-manager.clerk.company-employees-create', compact('categories', 'companies', 'company'));
        }
    }

    public function store(EmployeeStoreRequest $request)
    {
        $input = $request->validated();

        DB::beginTransaction();
        try {
            $companyEmployee = CompanyEmployee::create(['email' => $input['email'], 'name' => $input['name'], 'surname' => $input['surname'], 'contact_number' => $input['contact_number'], 'contact_number_two' => $input['contact_number_two'], 'address' => $input['address'], 'address_two' => $input['address_two'], 'address_three' => $input['address_three'], 'company_id' => $input['company_id']]);
            $categories = explode(',', $input['category_id']);
            foreach ($categories as $category) {
                $companyEmployee->categories()->attach($category);
            }
            DB::commit();
            $companyEmployee->load('categories', 'company');
            return response()->json(['company' => $companyEmployee, 'message' => 'Company Employee created successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Company Employee could not be saved at the moment ' . $e->getMessage()], 400);
        }
    }


    public function show(CompanyEmployee $companyEmployee)
    {
        $companyEmployee = $companyEmployee->load('categories', 'company', 'recieved_logs');
        $user = Auth::user()->load('roles');
        $company = $companyEmployee -> load('company');
       //dd($companyEmployee);
        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.employee-view-info', compact('company','companyEmployee'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.campaign-manager.employee-view-info', compact('company','companyEmployee'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.campaign-manager.employee-view-info', compact('company','companyEmployee'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.campaign-manager.employee-view-info', compact('company','companyEmployee'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.campaign-manager.employee-view-info', compact('company','companyEmployee'));
        } else {
            return view('campaign-manager.index.employee-view-info', compact('company','companyEmployee'));
        }
    }

    public function edit(CompanyEmployee $companyEmployee)
    {
        $companies = Company::orderBy('company_name', 'asc')->get();
        $categories = Category::orderBy('category_name', 'asc')->get();
        $user = Auth::user()->load('roles');

        $company = $companyEmployee -> load('company');
        //dd($companyEmployee);
        if ($user->roles[0]->name == "app-admin") {
            return view('campaign-manager.employee-edit', compact('company','companyEmployee', 'companies', 'categories'));
        } elseif ($user->roles[0]->name == "administrator") {
            return view('users.administrators.campaign-manager.employee-edit', compact('company','companyEmployee', 'companies', 'categories'));
        } elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.campaign-manager.employee-edit', compact('company','companyEmployee', 'companies', 'categories'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.campaign-manager.employee-edit', compact('company','companyEmployee', 'companies', 'categories'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.campaign-manager.employee-edit', compact('company','companyEmployee', 'companies', 'categories'));
        } else {
            return view('campaign-manager.clerk.employee-edit', compact('company','companyEmployee', 'companies', 'categories'));
        }
    }

    public function update(EmployeeUpdateRequest $request, CompanyEmployee $companyEmployee)
    {
        $input = $request->validated();

        DB::beginTransaction();
        try {
            $cur = $companyEmployee;
            $companyEmployee->update(['name' => $input['name'], 'surname' => $input['surname'], 'contact_number_two' => $input['contact_number_two'], 'contact_number' => $input['contact_number'], 'address' => $input['address'], 'address_two' => $input['address_two'], 'address_three' => $input['address_three'], 'company_id' => $input['company_id']]);
            $categories = explode(',', $input['category_id']);
            $companyEmployee = $cur->fresh();
            $companyEmployee->categories()->sync($categories);

            DB::commit();
            $companyEmployee->load('categories', 'company');
            return response()->json(['company' => $companyEmployee, 'message' => 'Company Employee updated successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Company Employee could not be updated at the moment ' . $e->getMessage()], 400);
        }
    }

    public function destroy(CompanyEmployee $companyEmployee)
    {
        DB::beginTransaction();
        try {
            $companyEmployee->delete();
            DB::commit();

            $categories = Category::orderBy('category_name', 'asc')->get();
            $companies = Company::orderBy('company_name', 'asc')->get();
            return view('campaign-manager.employees-index', compact('categories', 'companies'));

        } catch (\Exception $e) {
            DB::rollback();

            $categories = Category::orderBy('category_name', 'asc')->get();
            $companies = Company::orderBy('company_name', 'asc')->get();
            return view('campaign-manager.employees-index', compact('categories', 'companies'));

        }
    }
}
