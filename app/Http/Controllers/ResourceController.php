<?php

namespace App\Http\Controllers;


use App\FundingResource;
use App\Incubatee;
use App\Mail\SendBulkMail;
use App\ResourceCategory;
use App\ResourceSummernote;
use App\ResourceDocument;
use App\User;
use App\Venture;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Postmark\PostmarkClient;
use Yajra\DataTables\DataTables;


class ResourceController extends Controller
{
    //Create Category
    public function createResourceCategory(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.create-category');
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.create-category');
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.create-category');
        }
    }
    //Store Category
    public function storeCategory(Request $request)
    {
        $input = $request->all();
        DB::beginTransaction();

        try {
            $create_category = ResourceCategory::create(['category_name' => $input['category_name']]);

            DB::commit();
            return response()->json(['message'=>'Category added successfully','create_category'=>$create_category]);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Get Category
    public function indexCategory()
    {
        $category = ResourceCategory::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.categories', compact('category'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.categories', compact('category'));

        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.categories', compact('category'));

        }
    }

    public function editCategory(ResourceCategory $resourceCategory)
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.edit-category', compact('resourceCategory'));
        }elseif ($user->roles[0]->name=='advisor'){
            return view('users.advisor-dashboard.Resources.edit-category', compact('resourceCategory'));
        }elseif ($user->roles[0]->name=='administrator'){
            return view('users.administrators.Resources.edit-category', compact('resourceCategory'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateCategory(Request $request, ResourceCategory $resourceCategory)
    {
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'advisor') {
            try {
                $resourceCategory->update(['category_name' => $input['category_name']]);

                $resourceCategory->save();

                DB::commit();
                return response()->json(['message' => 'Category updated successfully.', 'ResourceCategory' => $resourceCategory], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Category could not be saved at the moment ' . $e->getMessage(),'Role'=>$role], 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getCategory(){

        $category = ResourceCategory::all();
        $user = Auth::user()->load('roles');

        if($user->roles[0]->name=='app-admin') {
            return Datatables::of($category)->addColumn('action', function ($category) {
                $del = '/destroyRC/' . $category->id;
                $edit = '/editCategory/' . $category->id;
                return '<a href=' . $edit . ' title="Edit role" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name=='advisor'){
            return Datatables::of($category)->addColumn('action', function ($category) {
                $del = '/destroyRC/' . $category->id;
                $edit = '/editCategory/' . $category->id;
                return '<a href=' . $edit . ' title="Edit Category" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . '  title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name=='administrator'){
            return Datatables::of($category)->addColumn('action', function ($category) {
                $del = '/destroyRC/' . $category->id;
                $edit = '/editCategory/' . $category->id;
                return '<a href=' . $edit . ' title="Edit Category" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . '  title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    //Create Resource Funding
    public function createResource()
    {
        $user = Auth::user()->load('roles');
        $categories = ResourceCategory::orderBy('category_name', 'asc')->get();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.create_resources', compact('categories'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.create_resources', compact('categories'));
        }elseif ($user->roles[0]->name == 'mentor'){
            return view('users.mentor-dashboard.Resources.create_resources', compact('categories'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.create_resources', compact('categories'));
        }

    }

    //Store Resource Funding
    public function storeResourceFunding(Request $request)
    {
        $input = $request->all();
        DB::beginTransaction();

        try {
            $resource_funding = FundingResource::create(['title' => $input['title'], 'resource_category_id' => $input['resource_category_id'], 'closing_date' => $input['closing_date']]);
            if ($request->hasFile('image_url')) {
                $image_url_path = $request->file('image_url')->store('resources_documents');
                if (isset($resource_funding)) {
                    $resource_funding->update(['image_url' => $image_url_path]
                    );
                } else {
                    $resource_funding->create(['image_url' => $image_url_path]
                    );
                }
            }
            //$resource_funding->save();
            DB::commit();
            return response()->json(['message' => 'Resource funding has been successfully saved.', 'fundingResource_id' => $resource_funding->id]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }

    }

    //Store summernote
    public function storeDoc(Request $request)
    {
        $detail = $request->summernoteInput;
        $resource_funding = $request->fundingResource_id;

        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);


        $detail = $dom->savehtml();


        $ResourceSummernote = new ResourceSummernote;


        $ResourceSummernote->content = $detail;
        $ResourceSummernote->fundingResource_id = $resource_funding;


        $ResourceSummernote->save();
         return view('users.app-admin.Resources.index',compact('ResourceSummernote'));


    }

    //Index
    public function index()
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.index');
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.index');
        }elseif ($user->roles[0]->name == 'mentor'){
            return view('users.mentor-dashboard.Resources.index');
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.index');
        }

    }

    //Get Index
    public function getIndex()
    {
        $user = Auth::user()->load('roles');

        $fundingResources = FundingResource::with('fundingResourceCategory')->get();
        $fundings = [];
        foreach ($fundingResources as $funding) {
            $fundingResource_id = $funding->id;
            $category_id = $funding->resource_category_id;
            $category = ResourceCategory::find($category_id);

            if ($category_id == null) {
                $object = (object)['category_name' => 'No category selected', 'closing_date' => $funding->closing_date,
                    'title' => $funding->title, 'fundingResource_id' => $fundingResource_id];
                array_push($fundings, $object);
            } else {
                $object = (object)['category_name' => $category->category_name, 'closing_date' => $funding->closing_date,
                    'title' => $funding->title, 'fundingResource_id' => $fundingResource_id];
                array_push($fundings, $object);
            }
        }
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($fundings)->addColumn('action', function ($funding) {
                $re = '/edit-resource/' . $funding->fundingResource_id;
                $del = '/funding-del/' . $funding->fundingResource_id;
                return '<a href=' . $re . ' title="Edit Funding Resource" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Funding Resource" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'advisor'){
            return Datatables::of($fundings)->addColumn('action', function ($funding) {
                $re = '/edit-resource/' . $funding->fundingResource_id;
                $del = '/funding-del/' . $funding->fundingResource_id;
                return '<a href=' . $re . ' title="Edit Funding Resource" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Funding Resource" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'mentor'){
            return Datatables::of($fundings)->addColumn('action', function ($funding) {
                $re = '/edit-resource/' . $funding->fundingResource_id;
                $del = '/funding-del/' . $funding->fundingResource_id;
                return '<a href=' . $re . ' title="Edit Funding Resource" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Funding Resource" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'administrator'){
            return Datatables::of($fundings)->addColumn('action', function ($funding) {
                $re = '/edit-resource/' . $funding->fundingResource_id;
                $del = '/funding-del/' . $funding->fundingResource_id;
                return '<a href=' . $re . ' title="Edit Funding Resource" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Funding Resource" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }


    }

    //Edit Funding Resource
    public function edit(FundingResource $fundingResource)
    {
        $user = Auth::user()->load('roles');
        $categories = ResourceCategory::orderBy('category_name', 'asc')->get();
        $fundingResource->load('fundingResourceCategory', 'fundingResourceDocuments');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.edit_resources', compact('fundingResource', 'categories'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.edit_resources', compact('fundingResource', 'categories'));
        }elseif ($user->roles[0]->name == 'mentor'){
            return view('users.mentor-dashboard.Resources.edit_resources', compact('fundingResource', 'categories'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.edit_resources', compact('fundingResource', 'categories'));
        }


    }

    //Update Funding Resources Uploads
    public function updateFunding(Request $request, FundingResource $fundingResource)
    {
        $fundingResource->load('fundingResourceCategory', 'fundingResourceDocuments')->get();

        DB::beginTransaction();
        $input = $request->all();
        $file_count = $input['file_array_count'];

        try {

            for ($i = 0; $i < $file_count; $i++) {
                $file_url = $request->file('file_' . $i)->store('resources_documents');

                if (isset($file_url)) {
                    $create_doc = ResourceDocument::create(['documents' => $file_url, 'fundingResource_id' => $input['fundingResource_id']]);
                    $create_doc->save();
                }
            }

            DB::commit();
            return response()->json(['message' => 'PDF file uploaded successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Update Funding Resources
    public function updateResourceFunding(Request $request, FundingResource $fundingResource)
    {

        $fundingResource->load('fundingResourceCategory', 'fundingResourceDocuments')->get();
        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::today();

        try {

            $fundingResource->update(['title' => $input['title'],
                'closing_date' => $date, 'resource_category_id' => $input['resource_category_id']]);
            if ($request->hasFile('image_url')) {
                $image_url_path = $request->file('image_url')->store('resources_documents');
                if (isset($fundingResource)) {
                    $fundingResource->update(['image_url' => $image_url_path]
                    );
                } else {
                    $fundingResource->create(['image_url' => $image_url_path]
                    );
                }
            }
            $fundingResource->save();

            DB::commit();
            return response()->json(['message' => 'Updated!']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Update Summernote
    public function updateFundingSummernote(Request $request, FundingResource $fundingResource)
    {
        $fundingResource->load('resourceSummernote')->get();

        DB::beginTransaction();
        $input = $request->all();

        try {
            $fundingResource->save();

            if (isset($fundingResource->resourceSummernote)) {
                $summernote_content = $fundingResource->resourceSummernote;
                if ($request->has('content')) {
                    $summernote_content->update(['content' => $input['content']]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully updated ']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Delete Funding Resource
    public function fundingDestroy($id)
    {

        try {
            DB::beginTransaction();
            $funding = FundingResource::find($id);
            $funding->load('resourceSummernote', 'fundingResourceCategory', 'fundingResourceDocuments');

            $funding->forceDelete();

            DB::commit();
            return view('users.app-admin.Resources.index');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }
    }

    //Get documents for a specific Resource funding
    public function documentsIndex(FundingResource $fundingResource)
    {
        $fundingResource->load('fundingResourceDocuments');

        $doc_uploads = $fundingResource->fundingResourceDocuments;

        $documents_array = [];

        for ($i = 0; $i < count($doc_uploads); $i++) {
            $current_doc = $doc_uploads[$i];
            array_push($documents_array, $current_doc);
        }

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($documents_array)->addColumn('action', function ($documents) {
                $re = '/document-del/' . $documents->id;

                $del = '/document-del/' . $documents->id;
                return '<a href=' . $del . ' title="Delete Document" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';

            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($documents_array)->addColumn('action', function ($documents) {
                $re = '/document-del/' . $documents->id;

                $del = '/document-del/' . $documents->id;
                return '<a href=' . $del . ' title="Delete Document" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';

            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($documents_array)->addColumn('action', function ($documents) {
                $re = '/document-del/' . $documents->id;

                $del = '/document-del/' . $documents->id;
                return '<a href=' . $del . ' title="Delete Document" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';

            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($documents_array)->addColumn('action', function ($documents) {
                $re = '/document-del/' . $documents->id;

                $del = '/document-del/' . $documents->id;
                return '<a href=' . $del . ' title="Delete Document" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';

            })
                ->make(true);
        }
    }

    //Delete Funding Resource
    public function documentDestroy($id)
    {
        $funding = ResourceDocument::find($id);
        try {
            DB::beginTransaction();

            if (isset($funding)) {
                $funding->forceDelete();
            }
            DB::commit();
            return response()->json(['message' => 'Successfully deleted document']);
        } catch (\Exception $e) {
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    //Task Show
    public function tradeShowIndex()
    {
        $user = Auth::user()->load('roles');
        $date = Carbon::now();
        $resourceCategory = ResourceCategory::with('fundingResource')->where('category_name', 'Trade Show')->first();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.trade-show', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.trade-show', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'mentor'){
            return view('users.mentor-dashboard.Resources.trade-show', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.trade-show', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'incubatee'){
            return view('users.incubatee-dashboard.Resources.trade-show', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.Resources.trade-show', compact('resourceCategory','date'));
        }


    }
    //Funding only
    public function fundingsIndex()
    {
        $user = Auth::user()->load('roles');
        $date = Carbon::now();
        $resourceCategory = ResourceCategory::with('fundingResource')->where('category_name', 'Funding')->first();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.fundings', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.fundings', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'mentor'){
            return view('users.mentor-dashboard.Resources.fundings', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.fundings', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'incubatee'){
            return view('users.incubatee-dashboard.Resources.fundings', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.Resources.fundings', compact('resourceCategory','date'));
        }


    }
    //Grant
    public function grantIndex()
    {
        $user = Auth::user()->load('roles');
        $date = Carbon::now();
        $resourceCategory = ResourceCategory::with('fundingResource')->where('category_name', 'Grant')->first();
        $fund = FundingResource::with('fundingResourceDocuments')->first();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.grant', compact('resourceCategory','fund','date'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.grant', compact('resourceCategory','fund','date'));
        }elseif ($user->roles[0]->name == 'mentor'){
            return view('users.mentor-dashboard.Resources.grant', compact('resourceCategory','fund','date'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.grant', compact('resourceCategory','fund','date'));
        }elseif ($user->roles[0]->name == 'incubatee'){
            return view('users.incubatee-dashboard.Resources.grant', compact('resourceCategory','fund','date'));
        }elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.Resources.grant', compact('resourceCategory','fund','date'));
        }


    }
    //Competition
    public function competitionIndex()
    {
        $user = Auth::user()->load('roles');
        $date = Carbon::now();
        $resourceCategory = ResourceCategory::with('fundingResource')->where('category_name', 'Competition')->first();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.competition', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.competition', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'mentor'){
            return view('users.mentor-dashboard.Resources.competition', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.competition', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'incubatee'){
            return view('users.incubatee-dashboard.Resources.competition', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.Resources.competition', compact('resourceCategory','date'));
        }

    }
    //Crowd
    public function crowdIndex()
    {
        $user = Auth::user()->load('roles');
        $date = Carbon::now();
        $resourceCategory = ResourceCategory::with('fundingResource')->where('category_name', 'Crowd Funding')->first();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.crowd', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.crowd', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'mentor'){
            return view('users.mentor-dashboard.Resources.crowd', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.crowd', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'incubatee'){
            return view('users.incubatee-dashboard.Resources.crowd', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.Resources.crowd', compact('resourceCategory','date'));
        }


    }
    //Events
    public function eventsIndex()
    {
        $user = Auth::user()->load('roles');
        $date = Carbon::now();
        $resourceCategory = ResourceCategory::with('fundingResource')->where('category_name', 'Events')->first();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.events', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.events', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'mentor'){
            return view('users.mentor-dashboard.Resources.events', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.events', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'incubatee'){
            return view('users.incubatee-dashboard.Resources.events', compact('resourceCategory','date'));
        }elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.Resources.events', compact('resourceCategory','date'));
        }

    }

    //Download pdf
    public function downloadFile($id)
    {
        $document = ResourceDocument::find($id);

        return Storage::download($document->documents);
    }

    //Show all info
    public function showAll(FundingResource $fundingResource)
    {
        $fundingResource->load('resourceSummernote','fundingResourceDocuments');
        return view('users.app-admin.Resources.show', compact('fundingResource'));
    }

    //Emailer
    public function allIncubatees()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            $incubatees = Incubatee::with( 'user','venture')->get();
            return view('users.app-admin.Resources.emailer',compact('incubatees'));
        }elseif ($user->roles[0]->name == 'advisor') {
            $incubatees = Incubatee::with( 'user','venture')->get();
            return view('users.advisor-dashboard.Resources.emailer',compact('incubatees'));
        }elseif ($user->roles[0]->name == 'mentor') {
            $incubatees = Incubatee::with( 'user','venture')->get();
            return view('users.mentor-dashboard.Resources.emailer',compact('incubatees'));
        }elseif ($user->roles[0]->name == 'administrator') {
            $incubatees = Incubatee::with( 'user','venture')->get();
            return view('users.administrators.Resources.emailer',compact('incubatees'));
        }

    }

    //Send email to selected emails
    public function sendEmailToSelectedIncubatees(){
        $user = Auth::user()->load('roles');
        $incubatees = Incubatee::with('uploads', 'user','venture')->get();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Resources.send-email',compact('incubatees'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.Resources.send-email',compact('incubatees'));
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users.administrators.Resources.send-email',compact('incubatees'));
        }
    }

    public function sendMailToVentures(Request $request)
    {
        $input = $request->all();
        $selected_ventures = json_decode($input['selected_ventures']);

        try {
            DB::beginTransaction();

            foreach ($selected_ventures as $selected_ventures_in_db) {
                if ($selected_ventures_in_db) {
                    $incubatee = Incubatee::find($selected_ventures_in_db);
                    $incubatee->load('user');
                    $user =$incubatee->user;

                    $link = 'https://thepropella.co.za/login';
                    $data = array(
                        'name' => $user->name,
                        'email' =>$user->email,
                        'link' => $link
                    );

                    $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                    $fromEmail = "dev@propellaincubator.co.za";
                    $toEmail = $user->email;
                    $subject = "Funding Resource";
                    $htmlBody = "<p>Dear <b>$user->name</b></p>A new grant/competition/funding has been loaded onto the platform and you may be interested in it for your venture.
                    Have a look by clicking here: $link
                    <br/>

                    <br/>
                    <br/>
                    Kind regards<br>
                    Incubator Journey Facilitator";
                    $textBody = "";
                    $tag = "example-email-tag";
                    $trackOpens = true;
                    $trackLinks = "None";
                    $messageStream = "broadcast";

                    // Send an email:
                    $sendResult = $client->sendEmail(
                        $fromEmail,
                        $toEmail,
                        $subject,
                        $htmlBody,
                        $textBody,
                        $tag,
                        $trackOpens,
                        NULL, // Reply To
                        NULL, // CC
                        NULL, // BCC
                        NULL, // Header array
                        NULL, // Attachment array
                        $trackLinks,
                        NULL, // Metadata array
                        $messageStream
                    );


                    /*$email_field = $user->email;
                    Mail::send('emails.resources', $data, function ($message) use ($email_field) {
                        $message->to($email_field)
                            ->subject('Funding Resource .');
                        $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

                    });*/


                    return response()->json(['message' => 'Email sent']);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully sent email.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }


    public function getMailIncubatees()
    {
        $incubatees = Incubatee::with('uploads', 'user','venture')->get();
        dd($incubatees);
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';
            })
                ->make(true);

        } elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';

            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';

            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getAllIncubatees(){
        $incubatees = Incubatee::with( 'user','venture')->get();

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';
            })
                ->make(true);
        }elseif  ($user->roles[0]->name == 'advisor') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';
            })
                ->make(true);
        }elseif  ($user->roles[0]->name == 'marketing') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';
            })
                ->make(true);
        }elseif  ($user->roles[0]->name == 'administrator') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';
            })
                ->make(true);
        }
    }

    public function sendBulkEmailToVenture(Request $request){
        $input = $request->input();
        $message = $input['message'];
        $subject = $input['subject'];
        $valid_emails = [];

        try{
            if($input['selection_type']=='multi'){
                foreach (json_decode($input['users']) as $user_id){
                    $user = Incubatee::find($user_id);
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }else{
                $users = Incubatee::whereHas('roles',function($query){
                    $query->where('name','!=','app-admin')->where('name','!=','clerk');
                    return $query;
                });

                foreach ($users as $user){
                    if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                        array_push($valid_emails,$user->email);
                    }
                }
            }
//            return response()->json($valid_emails);
            /**
             * Chucking emails into sets of 50
             * Its recommended to send at most 50 at a time
             */
            $mail_chunks = array_chunk($valid_emails,50,true);

            $count = 0;
            if($request->has('attachment')) {
                $path = $request->file('attachment')->store('attachments');
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,url('/').'/storage/'.$path));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }

            }else{
                foreach ($mail_chunks as $mail_chunk) {
                    $count++;
                    Mail::bcc($mail_chunk)->send(new SendBulkMail($message, $subject,null));
                    if($count % 13 === 0)
                    {
                        sleep(1);
                    }
                }
            }

            return response()->json(['message'=>'Emails sent successfully','valid_email'=>$valid_emails,'mail chuncks'=>$mail_chunks,200]);
        }catch(\Exception $e){
            return response()->json(['message'=>'Emails could not be sent, please try again. If the problem persists contact the System Admin '.$e->getMessage(),'valid'=>$valid_emails,400]);
        }

    }

    public function uploadEmbedsForVenture(Request $request){
        if($request->has('image')) {
            $path = $request->file('image')->store('attachments');
            return $path;
        }
        return null;
    }

    public function destroyResourceCategory(ResourceCategory $resourceCategory)
    {
        $resourceCategory->load('fundingResource');
        $resourceCategory->forceDelete();
        return view('users.app-admin.Resources.categories');
    }

}
