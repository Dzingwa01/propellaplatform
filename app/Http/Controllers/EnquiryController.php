<?php

namespace App\Http\Controllers;

use App\GeneralAssignedEnquiry;
use App\GeneralEnquiry;
use App\GeneralEnquiryCategory;
use App\GeneralPreAssignedEnquiry;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;

class EnquiryController extends Controller
{
    //Returns a view with a datatable of all enquiry categories
    public function enquiryCategoryIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/enquiries/enquiry-category-index');
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/enquiries/enquiry-category-index');
        } else {
            return response("You are not authorized!!");
        }
    }

    //Gets all the enquiry categories from the database and sends them back to the view in a datatable format
    public function getEnquiryCategories(){
        $enquiry_categories_array = [];
        $enquiry_categories = GeneralEnquiryCategory::all();

        foreach($enquiry_categories as $enquiry_category){
            $enquiry_category->load('general_enquiries');
            $category_count = 0;

            if(count($enquiry_category->general_enquiries) > 0){
                $category_count = count($enquiry_category->general_enquiries);
            }

            $object = (object)['category_name' => $enquiry_category->category_name, 'category_count' => $category_count, 'id' => $enquiry_category->id];
            array_push($enquiry_categories_array, $object);
        }

        return Datatables::of($enquiry_categories_array)->addColumn('action', function ($category) {
                $show = '/show-enquiry-category-enquiries/' . $category->id;
                $edit = '/edit-enquiry-category/' . $category->id;
                return '<a href=' . $show . ' title="Show Enquiry Category"><i class="material-icons" style="color: blue;">remove_red_eye</i></a>
                <a href=' . $edit . ' title="Edit Enquiry Category"><i class="material-icons" style="color: green;">edit</i></a>';
            })
            ->make(true);
    }

    //Returns a view with a datatable of all the enquiries received
    public function unassignedEnquiryIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/enquiries/unassigned-enquiry-index');
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/enquiries/unassigned-enquiry-index');
        } else {
            return response("You are not authorized!!");
        }
    }

    //Gets all the enquiries from the database and sends them back to the view is a datatable format
    public function getUnassignedEnquiries(){
        $enquiries = GeneralEnquiry::all();
        $enquiries_array = [];

        foreach($enquiries as $enquiry){
            $enquiry->load('enquiry_category');
            $enquiry_category = "None";

            if($enquiry->enquiry_category != null){
                $enquiry_category = $enquiry->enquiry_category->category_name;
            }

            if($enquiry->is_pre_assigned == false){
                if($enquiry->is_assigned == false ){
                    $object = (object)['id' => $enquiry->id, 'title' => $enquiry->title, 'name' => $enquiry->name, 'surname' => $enquiry->surname,
                        'email' => $enquiry->email, 'contact_number' => $enquiry->contact_number, 'company' => $enquiry->company, 'city' => $enquiry->city,
                        'heard_about_us' => $enquiry->heard_about_us, 'enquiry_message' => $enquiry->enquiry_message, 'date_enquired' => $enquiry->date_enquired,
                        'category' => $enquiry_category];
                    array_push($enquiries_array, $object);
                }
            }
        }

        return Datatables::of($enquiries_array)->addColumn('action', function ($enquiry) {
                $show = '/show-enquiry/' . $enquiry->id;
                $pre_assign_enquiry = '/pre-assign-enquiry/' . $enquiry->id;
                return '<a href=' . $show . ' title="Show Enquiry"><i class="material-icons" style="color: dodgerblue;">remove_red_eye</i></a>
                <a href=' . $pre_assign_enquiry . ' title="Pre-Assign Enquiry"><i class="material-icons" style="color: orange;">account_balance</i></a>';
            })
            ->make(true);
    }

    //Returns a view with the pre-assigned enquiries to a datatable
    public function preAssignedEnquiryIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/enquiries/pre-assigned-enquiry-index');
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/enquiries/pre-assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'incubatee'){
            return view('/users/incubatee-dashboard/enquiries/pre-assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/enquiries/pre-assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/enquiries/pre-assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'mentor'){
            return view('/users/mentor-dashboard/enquiries/pre-assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'tenant'){
            return view('/users/tenant-dashboard/enquiries/pre-assigned-enquiry-index');
        } else {
            return response("You are not authorized!!");
        }
    }

    //Fetch all the assigned enquiries and return it to the view i a datatable format
    public function getPreAssignedEnquiries(){
        $logged_in_user = Auth::user()->load('roles');
        $pre_assigned_enquiries = GeneralPreAssignedEnquiry::all();
        $pre_assigned_enquiries_array = [];

        foreach($pre_assigned_enquiries as $pre_assigned_enquiry){
            $pre_assigned_enquiry->load('general_enquiry', 'user');
            $pre_assigned_user = $pre_assigned_enquiry->user;
            $general_enquiry = $pre_assigned_enquiry->general_enquiry;
            $general_enquiry->load('enquiry_category');
            $enquiry_category = "None";

            if($general_enquiry->enquiry_category != null){
                $enquiry_category = $general_enquiry->enquiry_category->category_name;
            }

            if($general_enquiry->is_pre_assigned == true and $general_enquiry->is_assigned != true){
                $object = (object)['id' => $general_enquiry->id, 'title' => $general_enquiry->title, 'name' => $general_enquiry->name, 'surname' => $general_enquiry->surname,
                    'email' => $general_enquiry->email, 'contact_number' => $general_enquiry->contact_number, 'company' => $general_enquiry->company, 'city' => $general_enquiry->city,
                    'heard_about_us' => $general_enquiry->heard_about_us, 'enquiry_message' => $general_enquiry->enquiry_message, 'date_enquired' => $general_enquiry->date_enquired,
                    'category' => $enquiry_category, 'date_pre_assigned' => $pre_assigned_enquiry->date_pre_assigned, 'pre_assigned_user' => $pre_assigned_enquiry->name . ' ' . $pre_assigned_enquiry->surname];
                array_push($pre_assigned_enquiries_array, $object);
            }
        }

        if($logged_in_user->roles[0]->name == 'app-admin' or $logged_in_user->roles[0]->name == 'administrator'){
            return Datatables::of(array_unique($pre_assigned_enquiries_array))
                ->addColumn('action', function ($enquiry) {
                    $show = '/show-enquiry/' . $enquiry->id;
                    $assign_enquiry = '/assign-enquiry-view/' . $enquiry->id;
                    return '<a href=' . $show . ' title="Show Enquiry"><i class="material-icons" style="color: dodgerblue;">remove_red_eye</i></a>
                <a href=' . $assign_enquiry . ' title="Assign Enquiry"><i class="material-icons" style="color: darkred;">add_alert</i></a>';
                })
                ->make(true);
        } else if ($logged_in_user->roles[0]->name == 'incubatee' or
            $logged_in_user->roles[0]->name == 'marketing' or
            $logged_in_user->roles[0]->name == 'advisor' or
            $logged_in_user->roles[0]->name == 'mentor' or
            $logged_in_user->roles[0]->name == 'tenant'){
            return Datatables::of($pre_assigned_enquiries_array)
                ->addColumn('action', function ($enquiry) {
                    $show = '/show-enquiry/' . $enquiry->id;
                    $assign_enquiry = '/assign-enquiry-view/' . $enquiry->id;
                    return '<a href=' . $show . ' title="Show Enquiry"><i class="material-icons" style="color: dodgerblue;">remove_red_eye</i></a>
                <a href=' . $assign_enquiry . ' title="Assign Enquiry"><i class="material-icons" style="color: darkred;">add_alert</i></a>';
                })
                ->make(true);
        } else {
            return response("You are not authorized!!");
        }
    }

    //Returns a view with the assigned enquiries to a datatable
    public function assignedEnquiryIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/enquiries/assigned-enquiry-index');
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/enquiries/assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'incubatee'){
            return view('/users/incubatee-dashboard/enquiries/assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/enquiries/assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/enquiries/assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'mentor'){
            return view('/users/mentor-dashboard/enquiries/assigned-enquiry-index');
        } else if($logged_in_user->roles[0]->name == 'tenant'){
            return view('/users/tenant-dashboard/enquiries/assigned-enquiry-index');
        } else {
            return response("You are not authorized!!");
        }
    }

    //Fetch all the assigned enquiries and return it to the view in a datatable format
    public function getAssignedEnquiries(){
        $logged_in_user = Auth::user()->load('roles');
        $enquiries = GeneralEnquiry::all();
        $enquiries_array = [];

        foreach($enquiries as $enquiry){
            $enquiry->load('enquiry_category');
            $enquiry_category = "None";

            if($enquiry->enquiry_category != null){
                $enquiry_category = $enquiry->enquiry_category->category_name;
            }

            if($enquiry->is_assigned == true){
                $object = (object)['id' => $enquiry->id, 'title' => $enquiry->title, 'name' => $enquiry->name, 'surname' => $enquiry->surname,
                    'email' => $enquiry->email, 'contact_number' => $enquiry->contact_number, 'company' => $enquiry->company, 'city' => $enquiry->city,
                    'heard_about_us' => $enquiry->heard_about_us, 'enquiry_message' => $enquiry->enquiry_message, 'date_enquired' => $enquiry->date_enquired,
                    'category' => $enquiry_category, 'date_assigned' => $enquiry->date_assigned, 'enquiry_assigned_person' => $enquiry->enquiry_assigned_person,
                    'enquiry_next_step' => $enquiry->enquiry_next_step];
                array_push($enquiries_array, $object);
            }
        }

        if($logged_in_user->roles[0]->name == 'app-admin' or $logged_in_user->roles[0]->name == 'administrator'){
            return Datatables::of($enquiries_array)->addColumn('action', function ($enquiry) {
                $show = '/show-enquiry/' . $enquiry->id;
                return '<a href=' . $show . ' title="Show Enquiry"><i class="material-icons" style="color: dodgerblue;">remove_red_eye</i></a>';
            })
                ->make(true);
        } else if ($logged_in_user->roles[0]->name == 'incubatee' or
            $logged_in_user->roles[0]->name == 'marketing' or
            $logged_in_user->roles[0]->name == 'advisor' or
            $logged_in_user->roles[0]->name == 'mentor' or
            $logged_in_user->roles[0]->name == 'tenant'){
            return Datatables::of($enquiries_array)->addColumn('action', function ($enquiry) {
                $show = '/show-enquiry/' . $enquiry->id;
                return '<a href=' . $show . ' title="Show Enquiry"><i class="material-icons" style="color: dodgerblue;">remove_red_eye</i></a>';
            })
                ->make(true);
        } else {
            return response("You are not authorized!!");
        }
    }

    //Store GeneralEnquiryCategory
    public function storeEnquiryCategory(Request $request){
        $input = $request->all();

        try{
            $category_name = $input['category_name'];
            DB::beginTransaction();
            $create_enquiry_category = GeneralEnquiryCategory::create(['category_name' => $category_name]);
            DB::commit();
            return response()->json(['message' => 'Category created.'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //This is when a viewers sends enquiry to the system, we store it in the database for later use
    public function sendEnquiry(Request $request){
        $input = $request->all();
        $today = Carbon::now()->toDateString();

        try{
            $title = $input['title'];
            $name = $input['name'];
            $surname = $input['surname'];
            $email = $input['email'];
            $company = $input['company'];
            $contact_number = $input['contact_number'];
            $city = $input['city'];
            $enquiry_category_id = $input['enquiry_category_id'];
            $heard_about_us = $input['heard_about_us'];
            $enquiry_message = $input['enquiry_message'];

            DB::beginTransaction();
            $create_enquiry = GeneralEnquiry::create(['general_enquiry_category_id' => $enquiry_category_id, 'title' => $title, 'name' => $name,
            'surname' => $surname, 'email' => $email, 'company' => $company, 'contact_number' => $contact_number, 'city' => $city, 'heard_about_us' => $heard_about_us,
            'enquiry_message' => $enquiry_message, 'date_enquired' => $today]);
            DB::commit();

            $data = array(
                'name' => $input['name'],
                'email' => $email
            );
            $email_field = $email;
            Mail::send('emails.hedge-enquiry-response', $data, function ($message) use ($email_field) {
                $message->to($email_field)
                    ->subject('Thank you for your enquiry.');
                $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
            });

            return response()->json(['message' => 'Message sent.'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Return a view that will display all the enquiries that are listed under a specific category
    public function adminViewEnquiryCategoryEnquiries(GeneralEnquiryCategory $enquiryCategory){
        $enquiryCategory->load('general_enquiries');
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/enquiries/show-enquiry-category-enquiries', compact('enquiryCategory'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/enquiries/show-enquiry-category-enquiries', compact('enquiryCategory'));
        } else {
            return response("You are not required!!");
        }
    }

    //Returns a view where admin can update a category name
    public function adminEditEnquiryCategory(GeneralEnquiryCategory $enquiryCategory){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/enquiries/edit-enquiry-category', compact('enquiryCategory'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/enquiries/edit-enquiry-category', compact('enquiryCategory'));
        } else {
            return response("You are not required!!");
        }
    }

    //Admin updating the enquiry category, category name
    public function adminUpdateEnquiryCategory(Request $request, GeneralEnquiryCategory $enquiryCategory){
        $input = $request->all();

        try{
            DB::beginTransaction();
            $enquiryCategory->category_name = $input['category_name'];
            $enquiryCategory->save();
            DB::commit();
            return response()->json(['message' => 'Category updated.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Returns a view that will display the full overview of the enquiry before it has been assigned
    public function adminViewEnquiry(GeneralEnquiry $enquiry){
        $enquiry->load('enquiry_category');
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/enquiries/show-enquiry-overview', compact('enquiry'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/enquiries/show-enquiry-overview', compact('enquiry'));
        } else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/enquiries/show-enquiry-overview', compact('enquiry'));
        } else if ($logged_in_user->roles[0]->name == 'mentor'){
            return view('/users/mentor-dashboard/enquiries/show-enquiry-overview', compact('enquiry'));
        } else if ($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/enquiries/show-enquiry-overview', compact('enquiry'));
        }  else if ($logged_in_user->roles[0]->name == 'incubatee'){
            return view('/users/incubatee-dashboard/enquiries/show-enquiry-overview', compact('enquiry'));
        }   else if ($logged_in_user->roles[0]->name == 'tenant'){
            return view('/users/tenant-dashboard/enquiries/show-enquiry-overview', compact('enquiry'));
        } else {
            return response("You are not authorized!!");
        }
    }

    //Returns a view that will allow the admin to follow up with the enquiry and assign someone to the enquiry
    public function adminPreAssignEnquiryView(GeneralEnquiry $enquiry){
        $enquiry->load('enquiry_category');
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/enquiries/pre-assign-enquiry-view', compact('enquiry'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/enquiries/pre-assign-enquiry-view', compact('enquiry'));
        } else {
            return response("You are not required!!");
        }
    }

    //Admin assign enquiry to a list of people and leave it up to the assigned people to follow up
    public function adminStorePreAssignedEnquiry(Request $request, GeneralEnquiry $enquiry){
        $input = $request->all();
        $enquiry->load('enquiry_category');
        $today = Carbon::now()->toDateString();
        $assigned_list = $input['assigned_list'];
        $split_list = explode(",", $assigned_list);

        try{
            DB::beginTransaction();

            //Loop through list coming from view
            for ($i = 0; $i < count($split_list); $i++){
                $user = User::where('email', $split_list[$i])->first();

                if(isset($user)){
                    $create_general_pre_assigned_enquiry = GeneralPreAssignedEnquiry::create(['user_id' => $user->id, 'general_enquiry_id' => $enquiry->id,
                        'date_pre_assigned' => $today]);

                    $enquiry->is_pre_assigned = true;
                    $enquiry->save();

                    $data = array(
                        'title' => $enquiry->title,
                        'name' => $enquiry->name,
                        'surname' => $enquiry->surname,
                        'email' => $enquiry->email,
                        'contact_number' => $enquiry->contact_number,
                        'enquiry_message' => $enquiry->enquiry_message,
                        'company' => $enquiry->company,
                        'category' => $enquiry->enquiry_category->category_name,
                        'assigned_person' => $user->name
                    );
                    $email_field = $user->email;
                    Mail::send('emails.pre-assigned-enquiry-email', $data, function ($message) use ($email_field) {
                        $message->to($email_field)
                            ->subject('You have been assigned to an enquiry.');
                        $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                    });
                }
            }

            DB::commit();
            return response()->json(['message' => 'They have been assigned.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Returns a view that will allow the user to assign the enquiry for a final time
    public function adminAssignEnquiryView(GeneralEnquiry $enquiry){
        $logged_in_user = Auth::user()->load('roles');
        $enquiry->load('pre_assigned_enquiries');
        $enquiry_users_array = [];

        foreach ($enquiry->pre_assigned_enquiries as $pre_assigned_enquiry){
            $user_id = $pre_assigned_enquiry->user_id;
            $user = User::find($user_id);
            $object = (object)['user_name' => $user->name . ' ' . $user->surname, 'user_email' => $user->email, 'user_id' => $user->id];
            array_push($enquiry_users_array, $object);
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/enquiries/assign-enquiry-view', compact('enquiry', 'enquiry_users_array'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/enquiries/assign-enquiry-view', compact('enquiry', 'enquiry_users_array'));
        } else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/enquiries/assign-enquiry-view', compact('enquiry', 'enquiry_users_array'));
        } else if ($logged_in_user->roles[0]->name == 'mentor'){
            return view('/users/mentor-dashboard/enquiries/assign-enquiry-view', compact('enquiry', 'enquiry_users_array'));
        } else if ($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/enquiries/assign-enquiry-view', compact('enquiry', 'enquiry_users_array'));
        }  else if ($logged_in_user->roles[0]->name == 'incubatee'){
            return view('/users/incubatee-dashboard/enquiries/assign-enquiry-view', compact('enquiry', 'enquiry_users_array'));
        }   else if ($logged_in_user->roles[0]->name == 'tenant'){
            return view('/users/tenant-dashboard/enquiries/assign-enquiry-view', compact('enquiry', 'enquiry_users_array'));
        } else {
            return response("You are not authorized!!");
        }
    }

    //Admin store enquiry follow up. This will update fields right throughout other pages too
    public function adminAssignGeneralEnquiry(Request $request, GeneralEnquiry $enquiry){
        $enquiry->load('enquiry_category', 'pre_assigned_enquiries');
        $input = $request->all();
        $today = Carbon::now()->toDateString();

        try{
            $enquiry_assigned_person = $input['enquiry_assigned_person_email'];

            DB::beginTransaction();

            //Update the whole enquiry
            $enquiry->update(['initial_contact_person' => $input['initial_contact_person'], 'initial_contact_method' => $input['initial_contact_method'],
                'initial_report' => $input['initial_report'], 'enquiry_next_step' => $input['enquiry_next_step'], 'enquiry_assigned_person' => $input['enquiry_assigned_person'],
                'date_assigned' => $today, 'is_assigned' => true]);
            $enquiry->save();

            //Create assigned enquiry relationship (ease of use)
            $user = User::where('email', $enquiry_assigned_person)->first();

            if(isset($user)){
                $create_assigned_enquiry = GeneralAssignedEnquiry::create(['user_id' => $user->id, 'general_enquiry_id' => $enquiry->id,
                    'initial_contact_person' => $input['initial_contact_person'], 'initial_contact_method' => $input['initial_contact_method'],
                    'initial_report' => $input['initial_report'], 'enquiry_next_step' => $input['enquiry_next_step'], 'enquiry_assigned_person' => $input['enquiry_assigned_person'],
                    'date_assigned' => $today]);
            }

            foreach ($enquiry->pre_assigned_enquiries as $pre_assigned_enquiry){
                $pre_assigned_enquiry->forceDelete();
            }

            DB::commit();

            $data = array(
                'title' => $enquiry->title,
                'name' => $enquiry->name,
                'surname' => $enquiry->surname,
                'email' => $enquiry->email,
                'contact_number' => $enquiry->contact_number,
                'company' => $enquiry->company,
                'city' => $enquiry->city,
                'heard_about_us' => $enquiry->heard_about_us,
                'category' => $enquiry->enquiry_category->category_name,
                'enquiry_message' => $enquiry->enquiry_message,
                'contacted_by' => $input['initial_contact_person'],
                'report' => $input['initial_contact_method'],
                'next_step' => $input['enquiry_next_step'],
                'date_assigned' => $today,
                'assigned_person' => $input['enquiry_assigned_person']
            );
            $email_field = $enquiry_assigned_person;
            Mail::send('emails.hedge-assigned-person', $data, function ($message) use ($email_field) {
                $message->to($email_field)
                    ->subject('You have been assigned to an enquiry.');
                $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
            });


            return response()->json(['message' => 'Enquiry updated.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }
}
