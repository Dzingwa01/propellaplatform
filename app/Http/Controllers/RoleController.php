<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use PhpParser\Node\Expr\Cast\Object_;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexRole()
    {
        $roles = Role::all();

        return view('users/app-admin/roles/role-index', compact('roles'));
    }


    public function createRole()
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.roles.create-role');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }


    public function storeRole(Request $request)
    {
        $input = $request->all();


        DB::beginTransaction();

        try {
            $create_role = Role::create(['name' => $input['name'],'display_name' => $input['name'],'guard_name'=>'web',
                'permissions' =>[
                    'name-delete' => true,
                    'name-add' => true,
                    'name-update' => true,
                    'name-view' => true,
                ]]);
            $create_role->save();
            DB::commit();
            return response()->json(['message' => 'Role added successfully', 'create_role' => $create_role]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    public function getRoles()
    {

        $role = Role::all();
        $user = Auth::user()->load('roles');

        if($user->roles[0]->name=='app-admin') {
            return Datatables::of($role)->addColumn('action', function ($role) {
                $del = '/role/delete/' . $role->id;
                $edit = '/editRole/' . $role->id;
                return '<a href=' . $edit . ' title="Edit role" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete_roles(this)" title="Delete Role" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name=='administrator'){
            return Datatables::of($role)->addColumn('action', function ($role) {
                $del = '/role/delete/' . $role->id;
                $edit = '/editRole/' . $role->id;
                return '<a href=' . $edit . ' title="Edit Category" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . '  title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }



    public function editRole(Role $role)
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users/app-admin/roles/edit-role', compact('role'));
        }elseif ($user->roles[0]->name=='administrator'){
            return view('users/app-admin/roles/edit-role', compact('role'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }


    public function updateRole(Request $request, Role $role)
    {
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'administrator') {
            try {
                $role->update(['name' => $input['name'],'display_name' => $input['display_name'],
                    'permissions' => $input['permissions'],'guard_name' => $input['guard_name']]);

                $role->save();

                DB::commit();
                return response()->json(['message' => 'Role updated successfully.', 'Role' => $role], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Role could not be saved at the moment ' . $e->getMessage(),'Role'=>$role], 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function destroyRole(Role $role)
    {
        $role->delete();

        return view('users.app-admin.roles.role-index');
    }
}
