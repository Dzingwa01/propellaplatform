<?php

namespace App\Http\Controllers;

use App\Event;
use App\PropellaVenue;
use App\User;
use App\UserVenueBooking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;
use Calendar;

class BookingVenueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = Auth::user()->load('roles');
        if ($users->roles[0]->name == 'app-admin') {
            return view('users.app-admin.BookingVenue.index');
        } elseif ($users->roles[0]->name == 'administrator') {
            return view('users.administrators.BookingVenue.index');
        } elseif ($users->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.BookingVenue.index');
        } elseif ($users->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.BookingVenue.index');
        } elseif ($users->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.BookingVenue.index');
        } elseif ($users->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.BookingVenue.index');
        } elseif ($users->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.BookingVenue.index');
        } elseif ($users->roles[0]->name == 'tablet') {
            return view('users.tablet.BookingVenue.index');
        }

    }

    public function getVenues()
    {
        $venues = PropellaVenue::all();

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'administrator') {
            return Datatables::of($venues)->addColumn('action', function ($venue) {
                $re = '/show-venue-bookings/' . $venue->id;
                $edit = '/edit-venue/' . $venue->id;
                $del =  $venue->id;
                return '<a href=' . $re . ' title="View Booked Venues" style="color:blue!important;"><i class="material-icons">remove_red_eye</i></a><a href=' . $edit . ' title="Edit Venues" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_booking(this)" title="Delete Venues" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($venues)->addColumn('action', function ($venue) {
                $re = '/show-venue-bookings/' . $venue->id;
                $edit = '/edit-venue/' . $venue->id;
                $del = '/venue-delete-check/' . $venue->id;
                return '<a href=' . $re . ' title="View Booked Venues" style="color:blue!important;"><i class="material-icons">remove_red_eye</i></a><a href=' . $edit . ' title="Edit Venues" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_booking(this)" title="Delete Venues" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($venues)->addColumn('action', function ($venue) {
                $re = '/show-venue-bookings/' . $venue->id;
                $edit = '/edit-venue/' . $venue->id;
                $del = $venue->id;
                return '<a href=' . $re . ' title="View Booked Venues" style="color:blue!important;"><i class="material-icons">remove_red_eye</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'tablet') {
            return Datatables::of($venues)->addColumn('action', function ($venue) {
                $re = '/show-venue-bookings/' . $venue->id;
                $edit = '/edit-venue/' . $venue->id;
                $del = $venue->id;
                return '<a href=' . $re . ' title="View Booked Venues" style="color:blue!important;"><i class="material-icons">remove_red_eye</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'tenant') {
            return Datatables::of($venues)->addColumn('action', function ($venue) {
                $re = '/show-venue-bookings/' . $venue->id;
                $edit = '/edit-venue/' . $venue->id;
                $del = $venue->id;
                return '<a href=' . $re . ' title="View Booked Venues" style="color:blue!important;"><i class="material-icons">remove_red_eye</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($venues)->addColumn('action', function ($venue) {
                $re = '/show-venue-bookings/' . $venue->id;
                $edit = '/edit-venue/' . $venue->id;
                $del = $venue->id;
                return '<a href=' . $re . ' title="View Booked Venues" style="color:blue!important;"><i class="material-icons">remove_red_eye</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function createVenue()
    {

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.BookingVenue.create-venue');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.BookingVenue.create-venue');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.BookingVenue.create-venue');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.BookingVenue.create-venue');
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.BookingVenue.create-venue');
        } elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.BookingVenue.create-venue');
        } elseif ($user->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.BookingVenue.create-venue');
        } elseif ($user->roles[0]->name == 'tablet') {
            return view('users.tablet.BookingVenue.index');
        }

    }

    public function storeVenue(Request $request)
    {

        $input = $request->all();

        DB::beginTransaction();
        try {
            $venue_booking = PropellaVenue::create(['venue_name' => $input['venue_name'], 'number_of_seats' => $input['number_of_seats']]);
            $venue_booking->save();
            DB::commit();
            return response()->json(['booking' => $venue_booking, 'message' => 'Venue Has Been created successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Venue could not be saved at the moment ' . $e->getMessage()], 400);
        }
    }

    public function editVenue($id)
    {

        $propellaVenue = PropellaVenue::find($id);

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.BookingVenue.edit-venue', compact('propellaVenue'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.BookingVenue.edit-venue', compact('propellaVenue'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.BookingVenue.edit-venue', compact('propellaVenue'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.BookingVenue.edit-venue', compact('propellaVenue'));
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.BookingVenue.edit-venue', compact('propellaVenue'));
        } elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.BookingVenue.edit-venue', compact('propellaVenue'));
        } elseif ($user->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.BookingVenue.edit-venue', compact('propellaVenue'));
        } elseif ($user->roles[0]->name == 'tablet') {
            return view('users.tablet.BookingVenue.index', compact('propellaVenue'));
        }

    }

    public function updateVenue(Request $request, $id)
    {
        $input = $request->all();

        $propellaVenue = PropellaVenue::find($id);
        DB::beginTransaction();
        try {
            $propellaVenue->update(['venue_name' => $input['venue_name'], 'number_of_seats' => $input['number_of_seats']]);
            $propellaVenue->save();
            DB::commit();
            return response()->json(['booking' => $propellaVenue, 'message' => 'Venue Has Been Updated successfully'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Venue could not be saved at the moment ' . $e->getMessage()], 400);
        }
    }

    public function showVenues(User $user)
    {
        $user->load('venueBooking');
        $logged_in_user = Auth::user()->load('roles');
        $user_bookings = [];
        $date = Carbon::today()->toDateString();
        $venues = PropellaVenue::all();

        foreach ($user->venueBooking as $user_booking) {
            $venue_id = $user_booking->propella_venue_id;
            $venue = PropellaVenue::find($venue_id);

            $object = (object)['venue_name' => $venue->venue_name, 'venue_date' => $user_booking->venue_date,
                'venue_start_time' => $user_booking->venue_start_time, 'venue_end_time' => $user_booking->venue_end_time,
                'booking_reason' => $user_booking->booking_reason, 'id' => $user_booking->id];


            array_push($user_bookings, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.BookingVenue.show-venues', compact('venues', 'user_bookings', 'date','user'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.BookingVenue.show-venues', compact('venues', 'user_bookings', 'date','user'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.BookingVenue.show-venues', compact('venues', 'user_bookings', 'date','user'));
        } elseif ($logged_in_user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.BookingVenue.show-venues', compact('venues', 'user_bookings', 'date','user'));
        } elseif ($logged_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.BookingVenue.show-venues', compact('venues', 'user_bookings', 'date','user'));
        } elseif ($logged_in_user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.BookingVenue.show-venues', compact('venues', 'user_bookings', 'date','user'));
        } elseif ($logged_in_user->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.BookingVenue.show-venues', compact('venues', 'user_bookings', 'date','user'));
        } elseif ($logged_in_user->roles[0]->name == 'tablet') {
            return view('users.tablet.BookingVenue.show-venues', compact('venues', 'user_bookings', 'date','user'));
        }
    }


    public function venueBooking(PropellaVenue $venue)
    {
        $venue->load('venueBooking');
        $date = Carbon::today()->toDateString();
        $users = Auth::user()->load('roles');

        if ($users->roles[0]->name == 'app-admin') {
            return view('users.app-admin.BookingVenue.get-venue-booking', compact('venue','users'));
        } elseif ($users->roles[0]->name == 'administrator') {
            return view('users.administrators.BookingVenue.get-venue-booking', compact('venue', 'date','users'));
        } elseif ($users->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.BookingVenue.get-venue-booking', compact('venue','users'));
        } elseif ($users->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.BookingVenue.get-venue-booking', compact('venue','users'));
        } elseif ($users->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.BookingVenue.get-venue-booking', compact('venue','users'));
        } elseif ($users->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.BookingVenue.get-venue-booking', compact('venue','users'));
        } elseif ($users->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.BookingVenue.get-venue-booking', compact('venue','users'));
        } elseif ($users->roles[0]->name == 'tablet') {

            return view('users.tablet.BookingVenue.get-venue-booking', compact('venue','users'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function checkVenueBookingTablet(PropellaVenue $venue)
    {
        $venue->load('venueBooking');
        $time = Carbon::now()->toTimeString();
        $date = Carbon::today()->toDateString();
        $datatable = false;
        $active_booking = null;
        $indexed_time = substr($time, 0, 5);
        $first_two_indexes_time = "$indexed_time[0]" . "$indexed_time[1]";
        $first_two_indexes_int_plus_2 = (int)$first_two_indexes_time + 2;
        $fixed_current_time = "";
        foreach ($venue->venueBooking as $booking) {
            //Convert the time to +2GMT
            if ($first_two_indexes_int_plus_2 < 10) {
                $fixed_current_time = "0" . (string)$first_two_indexes_int_plus_2 . ":" . substr($indexed_time, 3, 5);
            } else {
                $fixed_current_time = (string)$first_two_indexes_int_plus_2 . ":" . substr($indexed_time, 3, 5);
            }

            if ($booking->venue_date == $date) {
                if ($fixed_current_time >= $booking->venue_start_time and $fixed_current_time <= $booking->venue_end_time) {
                    $active_booking = $booking;
                    $datatable = false;
                    break;
                }
            } else {
                $datatable = true;
            }
        }
        return response()->json(['datatable' => $datatable, 'active_booking' => $active_booking]);
    }

    public function getVenueBooking(PropellaVenue $venue)
    {
        $venue->load('venueBooking');
        $user = Auth::user()->load('roles');
        $venue_bookings = [];
        $date = Carbon::today()->toDateString();


        foreach ($venue->venueBooking as $venue_booking) {
            $user_id = $venue_booking->user_id;
            $venue_user = User::find($user_id);

            if ($venue_booking->venue_date >= $date) {
                $object = (object)['venue_name' => $venue->venue_name, 'booking_reason' => $venue_booking->booking_reason, 'venue_date' => $venue_booking->venue_date,
                    'venue_start_time' => $venue_booking->venue_start_time, 'venue_end_time' => $venue_booking->venue_end_time, 'booking_person' => $venue_user->name,
                    'duration_time' => $venue_booking->duration_time, 'user_venue_booking_id' => $venue_booking->id];
                array_push($venue_bookings, $object);
            }
        }

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($venue_bookings)->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($venue_bookings)->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($venue_bookings)->make(true);
        } elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($venue_bookings)->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($venue_bookings)->make(true);
        } elseif ($user->roles[0]->name == 'incubatee') {
            return Datatables::of($venue_bookings)->make(true);
        } elseif ($user->roles[0]->name == 'tenant') {
            return Datatables::of($venue_bookings)->make(true);
        } elseif ($user->roles[0]->name == 'tablet') {
            return Datatables::of($venue_bookings)->make(true);
        }
    }

    public function createVenueBooking(PropellaVenue $venue)
    {
        $users = Auth::user()->load('roles');
        if ($users->roles[0]->name == 'app-admin') {
            return view('users.app-admin.BookingVenue.create-venue-booking', compact('users', 'venue'));
        } elseif ($users->roles[0]->name == 'administrator') {
            return view('users.administrators.BookingVenue.create-venue-booking', compact('users', 'venue'));
        } elseif ($users->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.BookingVenue.create-venue-booking', compact('users', 'venue'));
        } elseif ($users->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.BookingVenue.create-venue-booking', compact('users', 'venue'));
        } elseif ($users->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.BookingVenue.create-venue-booking', compact('users', 'venue'));
        } elseif ($users->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.BookingVenue.create-venue-booking', compact('users', 'venue'));
        } elseif ($users->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.BookingVenue.create-venue-booking', compact('users', 'venue'));
        } elseif ($users->roles[0]->name == 'tablet') {
            return view('users.tablet.BookingVenue.create-venue-booking', compact('users', 'venue'));
        }
    }

    public function storeVenueBooking(Request $request)
    {
        $input = $request->all();
        $user_id = $input['user_id'];
        $propella_venue_id = $input['venue_id'];
        $current_venue = PropellaVenue::find($propella_venue_id);
        $current_venue->load('venueBooking');
        $user = Auth::user()->load('roles');
        //Venue booking duplicate
         $is_already_booked = False;
        try {
            if ($user->roles[0]->name == 'app-admin') {
                foreach ($current_venue->venueBooking as $venue_booking) {
                    if ($venue_booking->venue_date == $input['venue_date']) {
                        if ($input['venue_start_time'] != $venue_booking->venue_start_time) {
                            if ($input['venue_start_time'] < $venue_booking->venue_start_time) {
                                if ($input['venue_end_time'] < $venue_booking->venue_start_time) {

                                }else{
                                    $is_already_booked = true;
                                }
                            }elseif($input['venue_start_time'] > $venue_booking->venue_start_time){
                                if($input['venue_start_time'] > $venue_booking->venue_end_time){
                                }else{
                                    $is_already_booked = true;
                                }
                            }
                        }else{
                            $is_already_booked = true;
                        }
                    }
                }
                if($is_already_booked ){
                    return response()->json(['message' => 'There is already a booking for this date and time']);
                }

            } elseif ($user->roles[0]->name == 'incubatee') {
                foreach ($current_venue->venueBooking as $venue_booking) {
                    if ($venue_booking->venue_date == $input['venue_date']) {
                        if ($input['venue_start_time'] != $venue_booking->venue_start_time) {
                            if ($input['venue_start_time'] < $venue_booking->venue_start_time) {
                                if ($input['venue_end_time'] < $venue_booking->venue_start_time) {

                                }else{
                                    $is_already_booked = true;
                                }
                            }elseif($input['venue_start_time'] > $venue_booking->venue_start_time){
                                if($input['venue_start_time'] > $venue_booking->venue_end_time){
                                }else{
                                    $is_already_booked = true;
                                }
                            }
                        }else{
                            $is_already_booked = true;
                        }
                    }
                }
                if($is_already_booked ){
                    return response()->json(['message' => 'There is already a booking for this date and time']);
                }
            } elseif ($user->roles[0]->name == 'clerk') {
                foreach ($current_venue->venueBooking as $venue_booking) {
                    if ($venue_booking->venue_date == $input['venue_date']) {
                        if ($input['venue_start_time'] != $venue_booking->venue_start_time) {
                            if ($input['venue_start_time'] < $venue_booking->venue_start_time) {
                                if ($input['venue_end_time'] < $venue_booking->venue_start_time) {

                                }else{
                                    $is_already_booked = true;
                                }
                            }elseif($input['venue_start_time'] > $venue_booking->venue_start_time){
                                if($input['venue_start_time'] > $venue_booking->venue_end_time){
                                }else{
                                    $is_already_booked = true;
                                }
                            }
                        }else{
                            $is_already_booked = true;
                        }
                    }
                }
                if($is_already_booked ){
                    return response()->json(['message' => 'There is already a booking for this date and time']);
                }
            } elseif ($user->roles[0]->name == 'administrator') {
                foreach ($current_venue->venueBooking as $venue_booking) {
                    if ($venue_booking->venue_date == $input['venue_date']) {
                        if ($input['venue_start_time'] != $venue_booking->venue_start_time) {
                            if ($input['venue_start_time'] < $venue_booking->venue_start_time) {
                                if ($input['venue_end_time'] < $venue_booking->venue_start_time) {

                                }else{
                                    $is_already_booked = true;
                                }
                            }elseif($input['venue_start_time'] > $venue_booking->venue_start_time){
                                if($input['venue_start_time'] > $venue_booking->venue_end_time){
                                }else{
                                    $is_already_booked = true;
                                }
                            }
                        }else{
                            $is_already_booked = true;
                        }
                    }
                }
                if($is_already_booked ){
                    return response()->json(['message' => 'There is already a booking for this date and time']);
                }
            } elseif ($user->roles[0]->name == 'marketing') {
                foreach ($current_venue->venueBooking as $venue_booking) {
                    if ($venue_booking->venue_date == $input['venue_date']) {
                        if ($input['venue_start_time'] != $venue_booking->venue_start_time) {
                            if ($input['venue_start_time'] < $venue_booking->venue_start_time) {
                                if ($input['venue_end_time'] < $venue_booking->venue_start_time) {

                                }else{
                                    $is_already_booked = true;
                                }
                            }elseif($input['venue_start_time'] > $venue_booking->venue_start_time){
                                if($input['venue_start_time'] > $venue_booking->venue_end_time){
                                }else{
                                    $is_already_booked = true;
                                }
                            }
                        }else{
                            $is_already_booked = true;
                        }
                    }
                }
                if($is_already_booked ){
                    return response()->json(['message' => 'There is already a booking for this date and time']);
                }
            } elseif ($user->roles[0]->name == 'advisor') {
                foreach ($current_venue->venueBooking as $venue_booking) {
                    if ($venue_booking->venue_date == $input['venue_date']) {
                        if ($input['venue_start_time'] != $venue_booking->venue_start_time) {
                            if ($input['venue_start_time'] < $venue_booking->venue_start_time) {
                                if ($input['venue_end_time'] < $venue_booking->venue_start_time) {

                                }else{
                                    $is_already_booked = true;
                                }
                            }elseif($input['venue_start_time'] > $venue_booking->venue_start_time){
                                if($input['venue_start_time'] > $venue_booking->venue_end_time){
                                }else{
                                    $is_already_booked = true;
                                }
                            }
                        }else{
                            $is_already_booked = true;
                        }
                    }
                }
                if($is_already_booked ){
                    return response()->json(['message' => 'There is already a booking for this date and time']);
                }
            } elseif ($user->roles[0]->name == 'mentor') {
                foreach ($current_venue->venueBooking as $venue_booking) {
                    if ($venue_booking->venue_date == $input['venue_date']) {
                        if ($input['venue_start_time'] != $venue_booking->venue_start_time) {
                            if ($input['venue_start_time'] < $venue_booking->venue_start_time) {
                                if ($input['venue_end_time'] < $venue_booking->venue_start_time) {

                                }else{
                                    $is_already_booked = true;
                                }
                            }elseif($input['venue_start_time'] > $venue_booking->venue_start_time){
                                if($input['venue_start_time'] > $venue_booking->venue_end_time){
                                }else{
                                    $is_already_booked = true;
                                }
                            }
                        }else{
                            $is_already_booked = true;
                        }
                    }
                }
                if($is_already_booked ){
                    return response()->json(['message' => 'There is already a booking for this date and time']);
                }
            } elseif ($user->roles[0]->name == 'tenant') {
                foreach ($current_venue->venueBooking as $venue_booking) {
                    if ($venue_booking->venue_date == $input['venue_date']) {
                        if ($input['venue_start_time'] != $venue_booking->venue_start_time) {
                            if ($input['venue_start_time'] < $venue_booking->venue_start_time) {
                                if ($input['venue_end_time'] < $venue_booking->venue_start_time) {

                                }else{
                                    $is_already_booked = true;
                                }
                            }elseif($input['venue_start_time'] > $venue_booking->venue_start_time){
                                if($input['venue_start_time'] > $venue_booking->venue_end_time){
                                }else{
                                    $is_already_booked = true;
                                }
                            }
                        }else{
                            $is_already_booked = true;
                        }
                    }
                }
                if($is_already_booked ){
                    return response()->json(['message' => 'There is already a booking for this date and time']);
                }
            } elseif ($user->roles[0]->name == 'tablet') {
                foreach ($current_venue->venueBooking as $venue_booking) {
                    if ($venue_booking->venue_date == $input['venue_date']) {
                        if ($input['venue_start_time'] != $venue_booking->venue_start_time) {
                            if ($input['venue_start_time'] < $venue_booking->venue_start_time) {
                                if ($input['venue_end_time'] < $venue_booking->venue_start_time) {

                                }else{
                                    $is_already_booked = true;
                                }
                            }elseif($input['venue_start_time'] > $venue_booking->venue_start_time){
                                if($input['venue_start_time'] > $venue_booking->venue_end_time){
                                }else{
                                    $is_already_booked = true;
                                }
                            }
                        }else{
                            $is_already_booked = true;
                        }
                    }
                }
                if($is_already_booked ){
                    return response()->json(['message' => 'There is already a booking for this date and time']);
                }
            }

            DB::beginTransaction();
            $venue_booking = UserVenueBooking::create(['propella_venue_id' => $input['venue_id'], 'venue_date' => $input['venue_date'], 'venue_start_time' => $input['venue_start_time'],
                'venue_end_time' => $input['venue_end_time'], 'booking_person' => $input['booking_person'], 'booking_reason' => $input['booking_reason'], 'user_id' => $user_id]);
            $venue_booking->save();
            DB::commit();
            return response()->json(['booking' => $venue_booking, 'message' => 'You Have successfully Booked This Venue'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Venue could not be booked at the moment ' . $e->getMessage()], 400);
        }
    }

    public function editVenueBooking(UserVenueBooking $userBooking)
    {
        $propella_venue_id = $userBooking->propella_venue_id;
        $venue = PropellaVenue::find($propella_venue_id);
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.BookingVenue.edit-venue-booking', compact('userBooking', 'user', 'venue'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.BookingVenue.edit-venue-booking', compact('userBooking', 'user', 'venue'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.BookingVenue.edit-venue-booking', compact('userBooking', 'user', 'venue'));
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.BookingVenue.edit-venue-booking', compact('userBooking', 'user', 'venue'));
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.BookingVenue.edit-venue-booking', compact('userBooking', 'user', 'venue'));
        } elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.BookingVenue.edit-venue-booking', compact('userBooking', 'user', 'venue'));
        } elseif ($user->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.BookingVenue.edit-venue-booking', compact('userBooking', 'user', 'venue'));
        } elseif ($user->roles[0]->name == 'tablet') {
            return view('users.tablet.BookingVenue.edit-venue-booking', compact('userBooking', 'user', 'venue'));
        }
    }

    public function updateVenueBooking(Request $request, UserVenueBooking $userBooking)
    {
        $input = $request->all();
        $propella_venue_id = $input['venues_id'];
        $current_venue = PropellaVenue::find($propella_venue_id);

        try {
            foreach ($current_venue->venueBooking as $venue_booking) {
                if ($venue_booking->venue_date == $input['venue_date']) {
                    if ($input['venue_start_time'] >= $venue_booking->venue_start_time) {
                        if ($input['venue_end_time'] <= $venue_booking->venue_end_time) {
                            return response()->json(['message' => 'There is already a booking for this date and time']);
                        }
                    }
                }
            }


            DB::beginTransaction();
            $userBooking->update(['venue_date' => $input['venue_date'], 'venue_start_time' => $input['venue_start_time'],
                'venue_end_time' => $input['venue_end_time'], 'booking_person' => $input['booking_person'], 'booking_reason' => $input['booking_reason']]);

            DB::commit();
            return response()->json(['booking' => $userBooking, 'message' => 'You have successfully updated this booking'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Venue could not be booked at the moment ' . $e->getMessage()], 400);
        }
    }

    public function showVenueBookings(PropellaVenue $venue)
    {
        $venue->load('venueBooking');
        $venue_bookings = $venue->venueBooking;
        $user = $venue->user;
      /*  dd($venue_bookings);*/

        $users = Auth::user()->load('roles');
        if ($users->roles[0]->name == 'app-admin') {
            return view('users.app-admin.BookingVenue.get-all-venue-bookings', compact('venue'));
        } elseif ($users->roles[0]->name == 'administrator') {
            return view('users.administrators.BookingVenue.get-all-venue-bookings', compact('venue','user'));
        } elseif ($users->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.BookingVenue.get-all-venue-bookings', compact('venue'));
        } elseif ($users->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.BookingVenue.get-all-venue-bookings', compact('venue'));
        } elseif ($users->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.BookingVenue.get-all-venue-bookings', compact('venue','user'));
        } elseif ($users->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.BookingVenue.get-all-venue-bookings', compact('venue'));
        } elseif ($users->roles[0]->name == 'tablet') {
            return view('users.tablet.BookingVenue.get-all-venue-bookings', compact('venue'));
        } elseif ($users->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.BookingVenue.get-all-venue-bookings', compact('venue'));
        }
    }

    public
    function getAllVenueBooking(PropellaVenue $venue)
    {
        $venue->load('venueBooking');
        $venue_bookings = $venue->venueBooking;
        /*dd($venue_bookings);*/

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($venue_bookings)->addColumn('action', function ($booking) {
                $del = $booking->id;
                return '<a id=' . $del . ' onclick="confirm_delete_booking(this)" title="Delete Venues" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($venue_bookings)->addColumn('action', function ($booking) {
                $del = $booking->id;
                return '<a id=' . $del . ' onclick="confirm_delete_booking(this)" title="Delete Venues" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($venue_bookings)
                ->make(true);
        } elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($venue_bookings)
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($venue_bookings)
                ->make(true);
        } elseif ($user->roles[0]->name == 'incubatee') {
            return Datatables::of($venue_bookings)
                ->make(true);
        } elseif ($user->roles[0]->name == 'tablet') {
            return Datatables::of($venue_bookings)
                ->make(true);
        } elseif ($user->roles[0]->name == 'tenant') {
            return Datatables::of($venue_bookings)
                ->make(true);
        }
    }

    public
    function destroyVenue(PropellaVenue $propellaVenue)
    {
        try{
            DB::beginTransaction();
            foreach ($propellaVenue->venueBooking as $p_venue){
                $p_venue->forceDelete();
            }
            $propellaVenue->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Successfully Deleted This Venue!'], 200);
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }
    }

    public
    function cancelBooking($id)
    {
        try {
            DB::beginTransaction();
            $userBooking = UserVenueBooking::find($id);
            $userBooking->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Successfully canceled your booking']);

        } catch (\Exception $e) {
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }

    }
}
