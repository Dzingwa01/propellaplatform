<?php

namespace App\Http\Controllers;

use App\ApplicantPanelist;
use App\Applicants;
use App\Appointment;
use App\Appontment;
use App\Blog;
use App\Bootcamper;
use App\BootcamperPanelist;
use App\CompanyEmployee;
use App\DeclinedApplicant;
use App\DeclinedApplicantCategory;
use App\DeclinedApplicantContactLog;
use App\DeclinedApplicantQuestionAnswer;
use App\DeclinedBootcamper;
use App\DeclinedBootcamperPanelist;
use App\Donation;
use App\Event;
use App\EventBootcamper;
use App\EventIncubatee;
use App\EventInternalUser;
use App\HandoutUpload;
use App\Http\Requests\UserStoreRequest;
use App\IctPanelistQuestionAnswer;
use App\Incubatee;
use App\IncubateePresentation;
use App\IncubateeStage;
use App\Mail\InviteUser;
use App\Mentor;
use App\MentorVentureShadowboard;
use App\Newsletter;
use App\PanelInterviewDeclined;
use App\PanelInterviewDeclinedApplicant;
use App\Panelist;
use App\PanelistQuestionAnswer;
use App\Presentation;
use App\PresentationUpload;
use App\PropellaReferredApplicant;
use App\Question;
use App\Answer;
use App\QuestionsCategory;
use App\ReferredApplicant;
use App\ReferredApplicantCategory;
use App\ReferredApplicantQuestionAnswer;
use App\Role;
use App\SafetyForm;
use App\ShadowBoardComment;
use App\ShadowBoardQuestion;
use App\ShadowBoardQuestionAnswer;
use App\ShadowBoardQuestionCategory;
use App\ShadowBoardSubQuestionText;
use App\TiaApplication;
use App\User;
use App\UserEventDeregister;
use App\Venture;
use App\Applicant;
use App\VentureCategory;
use App\VenturePanelInterview;
use App\VenturePanelInterviewCategory;
use App\VenturePanelistQuestionAnswer;
use App\VentureQuestionAnswer;
use App\Video;
use App\Visitor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use mysql_xdevapi\CrudOperationBindable;
use PharIo\Manifest\ApplicationName;
use PhpParser\Node\Expr\Cast\Object_;
use Postmark\PostmarkClient;
use Tightenco\Collect\Support\Arr;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //user index view
    public function indexUser()
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            $roles = Role::where('name', '!=', 'app-admin')->get();
            return view('users.user-crud.index', compact('roles'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    /*Create Users*/
    public function createUser()
    {
        $roles = Role::orderBy('name', 'asc')->get();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.user-crud.create', compact('roles'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Store user
    public function store(Request $request){

        $input = $request->all();
        DB::beginTransaction();

        try {

            $user = User::create(['name' => $input['name'], 'address_one' => $input['address_one'],
                'surname' => $input['surname'], 'contact_number' => $input['contact_number'],
                'email' => $input['email'], 'password' => Hash::make('secret')]);

            $user->roles()->attach($input['role_id']);
            $user = $user->load('roles');


            $role_id = $input['role_id'];
            $role = Role::find($role_id);

            if($role->name == 'mentor'){
                $create_mentor = Mentor::create(['user_id'=>$user->id]);
            }

//
            DB::commit();
            return response()->json(['user' => $user, 'message' => 'User added successfully.','role'=>$role], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'User could not be saved  ' . $e->getMessage()], 400);
        }

    }
    //edit user view

    public function editUser(User $user)
    {
        $user->load('roles');
        $roles = Role::orderBy('name', 'asc')->get();
        return view('/users/user-crud/edit-user', compact( 'user', 'roles' ));

        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/user-crud/edit-user', compact('user', 'roles'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }

    }

    //update user details
    public function updateUser(Request $request, User $user)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            if ($request->has('role_id')) {

                $user->update(['name' => $input['name'],
                    'address_one' => $input['address_one'],
                    'surname' => $input['surname'],
                    'land_line' => $input['land_line'],
                    'contact_number' => $input['contact_number'],
                    'email' => $input['email'],
                    'password' => Hash::make('secret')]);

                $user->roles()->sync($input['role_id']);
                $user->save();
                DB::commit();
                return response()->json(['message' => 'User updated successfully.', 'user' => $user], 200);

            } else {
                return ('Not working');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'User could not be saved ' . $e->getMessage()], 400);
        }
    }

    public function index()
    {
        $roles = Role::where('name', '!=', 'app-admin')->get();
        return view('users.index', compact('roles'));
    }

    public function incubateeStagesIndex(){



        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.incubatees.admin-incubatees.incubatee-stages-index');
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.incubatees.incubatee-stages-index');
        } else {
            return response("You are not authorized!!");
        }
    }

    public function getIncubateeStages(){
        $incubatee_stages = IncubateeStage::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($incubatee_stages)->addColumn('action', function ($incubatee_stage) {
                $re = '/incubatee-stage-edit/' . $incubatee_stage->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($incubatee_stages)->addColumn('action', function ($incubatee_stage) {
                $re = '/incubatee-stage-edit/' . $incubatee_stage->id;
                return '<a href=' . $re . ' title="Edit Venture" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function createIncubateeStage(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.create-incubatee-stage');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.incubatees.create-incubatee-stage');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function storeIncubateeStage(Request $request){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $incubatee_stage = IncubateeStage::create(['stage_name' => $input['stage_name']]);
            DB::commit();

            return response()->json(['message' => 'Incubatee Stage added.']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function editIncubateeStage(IncubateeStage $incubateeStage){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.edit-incubatee-stage', compact('incubateeStage'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.incubatees.edit-incubatee-stage', compact('incubateeStage'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateIncubateeStage(Request $request, IncubateeStage $incubateeStage){
        $input = $request->all();

        try{
            DB::beginTransaction();
            $incubateeStage->update(['stage_name' => $input['stage_name']]);
            $incubateeStage->save();
            DB::commit();
            return response()->json(['message' => 'Incubatee stage updated.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function incubateesIndex()
    {
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.index');
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.incubatees.index');
        } elseif ($logged_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.incubatees.index');
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.incubatees.index');
        } elseif ($logged_in_user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.incubatees.index');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Alumni Incubatee
    public function alumniIncubatees(){
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.alumni.alumni-incubatees');
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.alumni.alumni-incubatees');
        } elseif ($logged_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.incubatees.index');
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.alumni.alumni-incubatees');
        } elseif ($logged_in_user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.incubatees.index');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Exited Incubatee
    public function exitedIncubatees(){

        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.exited-ventures.exited-incubatees');
        }elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.exited-ventures.exited-incubatees');
        }elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.exited-ventures.exited-incubatees');
        }elseif ($logged_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.exited-ventures.exited-incubatees');
        }
    }
    public function getExitedIncubatees()
    {
        $incubatees = Incubatee::with('uploads', 'user','venture')->where('status','Exit');

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin'
            or $user->roles[0]->name == 'advisor'
            or $user->roles[0]->name == 'administrator'
            or $user->roles[0]->name == 'marketing') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                })
                ->make(true);
        }
    }


    public function getAlumniIncubatees()
    {
        $incubatees = Incubatee::with('uploads', 'user','venture')->where('status','Alumni');

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                 })
                ->make(true);
        }elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                $re = '/incubatee-edit/' . $incubatee->id;
                $sh = '/incubatee/show/' . $incubatee->id;
                $event = '/incubatee-events/' . $incubatee->id;
                $del = $incubatee->id;
                return '<a href=' . $re . ' title="Edit Incubatee" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_incubatee(this)" title="Delete Incubatee" style="color:red; cursor: pointer;"><i class="material-icons">delete_forever</i></a><a href=' . $event . ' title="Incubatee Events" style="color:orange!important;"><i class="material-icons">date_range</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                $re = '/incubatee-edit/' . $incubatee->id;
                $sh = '/incubatee/show/' . $incubatee->id;
                $event = '/incubatee-events/' . $incubatee->id;
                $del = $incubatee->id;
                return '<a href=' . $re . ' title="Edit Incubatee" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_incubatee(this)" title="Delete Incubatee" style="color:red; cursor: pointer;"><i class="material-icons">delete_forever</i></a><a href=' . $event . ' title="Incubatee Events" style="color:orange!important;"><i class="material-icons">date_range</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'makerting') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                $re = '/incubatee-edit/' . $incubatee->id;
                $sh = '/incubatee/show/' . $incubatee->id;
                $event = '/incubatee-events/' . $incubatee->id;
                $del = $incubatee->id;
                return '<a href=' . $re . ' title="Edit Incubatee" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_incubatee(this)" title="Delete Incubatee" style="color:red; cursor: pointer;"><i class="material-icons">delete_forever</i></a><a href=' . $event . ' title="Incubatee Events" style="color:orange!important;"><i class="material-icons">date_range</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($incubatees)->addColumn('action', function ($incubatee) {
                $re = '/incubatee-edit/' . $incubatee->id;
                $sh = '/incubatee/show/' . $incubatee->id;
                $event = '/incubatee-events/' . $incubatee->id;
                $del = $incubatee->id;
                return '<a href=' . $re . ' title="Edit Incubatee" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_delete_incubatee(this)" title="Delete Incubatee" style="color:red; cursor: pointer;"><i class="material-icons">delete_forever</i></a><a href=' . $event . ' title="Incubatee Events" style="color:orange!important;"><i class="material-icons">date_range</i></a>';
            })
                ->make(true);
        }
    }



    public function mentorsIndex()
    {
        $user = Auth::user()->load('roles');
        $roles = Role::where('name', '!=', 'app-admin')->get();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.mentors.admin-mentors.index', compact('roles'));
        } elseif($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.mentors.index', compact('roles'));
        } elseif($user->roles[0]->name == 'administrator') {
            return view('users.administrators.mentors.index', compact('roles'));
        }elseif($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.mentors.index', compact('roles'));
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getUserProfile()
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.profile', compact('user'));
        } else if ($user->roles[0]->name == 'clerk') {
            return view('users.admin-profile', compact('user'));
        } else if ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.applicants.profiles', compact('user'));
        } else if ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.applicants.profiles', compact('user'));
        } else if ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.applicants.profiles', compact('user'));
        } else if ($user->roles[0]->name == 'tenant') {
            return view('users.tenant-dashboard.profiles', compact('user'));
        }
    }

    public function getUsers()
    {
        $logged_in_user = Auth::user()->load('roles');
        $users = User::with('roles')->get();

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return Datatables::of($users)->addColumn('action', function ($user) {
                $re = '/edit-user/' . $user->id;
                $sh = '/user/show/' . $user->id;
                $del = '/user-del/' .$user->id;
                return '<a href=' . $re . ' title="Edit user" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete_users(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->ToJson();
        }
    }
    public function userDestroy($id){
        try {
            DB::beginTransaction();
            $user = User::find($id);
            $user->load('applicant','bootcamper','userQuestionAnswers');
            if(isset($user->applicant)){
                $user->applicant()->forceDelete();
            }
            if(isset($user->bootcamper)){
                $user->bootcamper()->forceDelete();
            }
            if(isset($user->userQuestionAnswers)){
                $user->userQuestionAnswers()->forceDelete();
            }
            $user->forceDelete();
            DB::commit();
            return view('users/user-crud/index');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }
    }

    public function getIncubatees()
    {
        $incubatees = Incubatee::with('uploads', 'user','venture','venture_category')->get();

        $user = Auth::user()->load('roles');

        $incubatee_array = [];

        foreach($incubatees as $incubatee){
                $object = (object)[
                    'id' => $incubatee->id,
                    'name' => isset($incubatee->user->name) ? $incubatee->user->name : 'No name set',
                    'surname' => isset($incubatee->user->surname) ? $incubatee->user->surname : 'No surname set',
                    'email' => isset($incubatee->user->email) ? $incubatee->user->email : 'No email set',
                    'contact_number' => isset($incubatee->user->contact_number) ? $incubatee->user->contact_number : 'No numbers set',
                    'category_name' => isset($incubatee->venture_category) ? $incubatee->venture_category->category_name : 'No category',
                    'stage' => isset($incubatee->venture) ? $incubatee->venture->stage : 'No stage',
                    'status' => isset($incubatee->venture) ? $incubatee->venture->status : 'No status',
                    'date_joined' => $incubatee->created_at->toDateString(),
                    'incubatee_popi_act_agreement'=> isset($incubatee->incubatee_popi_act_agreement) ? $incubatee->incubatee_popi_act_agreement : 'No'
                ];
                array_push($incubatee_array, $object);
        }

        if ($user->roles[0]->name == 'app-admin'
                or $user->roles[0]->name == 'administrator'
                or $user->roles[0]->name == 'advisor'
                or $user->roles[0]->name == 'marketing'
                or $user->roles[0]->name == 'mentor') {
                return Datatables::of($incubatee_array)->addColumn('action', function ($incubatee) {
                    $show_overview = '/show-incubatee-details/' . $incubatee->id;
                    $re = '/incubatee-edit/' . $incubatee->id;
                    $event = '/incubatee-events/' . $incubatee->id;

                    $del = '/delete-incubatees/' . $incubatee->id;
                    if ($incubatee->category_name == "Industrial")
                    {
                        return '</a><a href=' . $show_overview . ' title="View Incu"><i class="material-icons">remove_red_eye</i></a>
                    </a><a href=' . $re . ' title="Edit Incubatee" style="color:green!important;"><i class="material-icons">create</i></a>
                    <a href=' . $event . ' title="Incubatee Events" style="color:orange!important;"><i class="material-icons">date_range</i></a>
                    <a href=' . $del . ' title="Delete incubatee" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
                    }
                    return '</a><a href=' . $show_overview . ' title="View Incu"><i class="material-icons">remove_red_eye</i></a>
                    </a><a href=' . $re . ' title="Edit Incubatee" style="color:green!important;"><i class="material-icons">create</i></a>
                    <a href=' . $event . ' title="Incubatee Events" style="color:orange!important;"><i class="material-icons">date_range</i></a>
                    <a href=' . $del . ' title="Delete incubatee" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
                })
                    ->make(true);
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Delete incubatee
    public function deleteIncubatees($id){

        try {
            DB::beginTransaction();
            $incubatee = Incubatee::find($id);
            $incubatee->load('events','uploads');

            if (isset($incubatee->events)) {
                foreach($incubatee->events as $event){
                    $event->forceDelete();
                }
            }
            if (isset($incubatee->uploads)) {
                $incubatee->uploads()->forceDelete();
            }
            if (isset($incubatee)) {
                $incubatee->forceDelete();
            }

            DB::commit();
            return view('users.incubatees.admin-incubatees.index');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }

    }

    public function adminViewIncubateeEvaluations(Incubatee $incubatee)
    {
        $logged_in_user = Auth::user()->load('roles');
        $today = Carbon::now()->toDateString();
        $incubatee->load('user');

        $incubatee_user = $incubatee->user;
        $incubatee_user->load('applicant', 'userQuestionAnswers', 'bootcamper')->get();


        //WORKSHOP EVALUATION FORM
        $workshop_evaluation_form_array = [];
        if(isset($incubatee_user->bootcamper->userQuestionAnswers)){
            $incubatee_application_question_answers = $incubatee_user->userQuestionAnswers;
            if(count($incubatee_application_question_answers) > 0){
                foreach ($incubatee_application_question_answers as $application_question_answer){
                    $question_id = $application_question_answer->question_id;
                    $question = Question::find($question_id);
                    if ($question->category->category_name == 'Workshop Evaluation Form') {
                        if (isset($question)) {
                            $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                                'answer_text' => $application_question_answer->answer_text,'created_at'=>$application_question_answer->created_at];
                            array_push($workshop_evaluation_form_array, $object);
                        }
                    }
                }
            }
        }
        //Pre-bootcamp
        $pre_bootcamp_question_answers_array = [];
        if(isset($incubatee_user->bootcamper)) {
            $bootcamper = $incubatee_user->bootcamper;
            if (isset($bootcamper))
                $bootcamper->load('user');
            $bootcamper_user = $bootcamper->user;
            $bootcamper_user->load('userQuestionAnswers', 'applicant');
            if (isset($bootcamper_user))

                if(count($incubatee_user->userQuestionAnswers) > 0){
                    foreach ($incubatee_user->userQuestionAnswers as $item) {
                        $question_id = $item->question_id;
                        $question = Question::find($question_id);
                        if(isset($question)){
                            if(isset($question->category->category_name)) {
                                if ($question->category->category_name == 'CC 2022 1 Pre-Bootcamp Evaluation Form') {
                                    $answer_text = $item->answer_text;
                                    if ($answer_text == null) {

                                    } else {
                                        $object = (object)['question_number' => $question->question_number,
                                            'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                            'question_id' => $question_id];
                                        array_push($pre_bootcamp_question_answers_array, $object);
                                    }

                                }
                            }
                        }
                    }
                }

            //Post-bootcamp
            $post_bootcamp_question_answers_array = [];
            if(count($incubatee_user->userQuestionAnswers) > 0){
                foreach ($incubatee_user->userQuestionAnswers as $item) {
                    $question_id = $item->question_id;
                    $question = Question::find($question_id);
                    if(isset($question)){
                        if(isset($question->category->category_name)) {
                            if ($question->category->category_name == 'CC 2022 2 Post-Bootcamp Evaluation Form') {
                                $answer_text = $item->answer_text;
                                if ($answer_text == null) {

                                } else {
                                    $object = (object)['question_number' => $question->question_number,
                                        'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                        'question_id' => $question_id];
                                    array_push($post_bootcamp_question_answers_array, $object);
                                }

                            }
                        }
                    }
                }
            }
        }



        //PRE STAGE 2 EVALUATION
        $pre_stage2_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $questions = Question::all();
                $question = $questions->find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == "CC 2022 3 Pre-Stage 2 Evaluation Form") {
                            $answer_text = $item->answer_text;
                            $object = (object)['question_number' => $question->question_number,
                                'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                'question_id' => $question_id];
                            array_push($pre_stage2_question_answers_array, $object);

                        }
                    }
                }
            }
        }


        //POST STAGE 2 EVALUATION
        $post_stage2_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $questions = Question::all();
                $question = $questions->find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == "CC 2022 4 Post-Stage 2 Evaluation Form") {
                            $answer_text = $item->answer_text;
                            $object = (object)['question_number' => $question->question_number,
                                'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                'question_id' => $question_id];
                            array_push($post_stage2_question_answers_array, $object);

                        }
                    }
                }
            }
        }

        //PRE STAGE 3 EVALUATION
        $pre_stage3_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $questions = Question::all();
                $question = $questions->find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == "CC 2022 5 Pre-Stage 3 Evaluation Form") {
                            $answer_text = $item->answer_text;
                            $object = (object)['question_number' => $question->question_number,
                                'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                'question_id' => $question_id];
                            array_push($pre_stage3_question_answers_array, $object);

                        }
                    }
                }
            }
        }

        //POST STAGE 3 EVALUATION
        $post_stage3_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $questions = Question::all();
                $question = $questions->find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == "CC 2022 6 Post-Stage 3 Evaluation Form") {
                            $answer_text = $item->answer_text;
                            $object = (object)['question_number' => $question->question_number,
                                'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                'question_id' => $question_id];
                            array_push($post_stage3_question_answers_array, $object);

                        }
                    }
                }
            }
        }

        //PRE STAGE 4 EVALUATION
        $pre_stage4_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $questions = Question::all();
                $question = $questions->find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == "CC 2022 7 Pre-Stage 4 Evaluation Form") {
                            $answer_text = $item->answer_text;
                            $object = (object)['question_number' => $question->question_number,
                                'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                'question_id' => $question_id];
                            array_push($pre_stage4_question_answers_array, $object);

                        }
                    }
                }
            }
        }

        //POST STAGE 4 EVALUATION
        $post_stage4_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $questions = Question::all();
                $question = $questions->find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == "CC 2022 8 Post-Stage 4 Evaluation Form") {
                            $answer_text = $item->answer_text;
                            $object = (object)['question_number' => $question->question_number,
                                'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                'question_id' => $question_id];
                            array_push($post_stage4_question_answers_array, $object);

                        }
                    }
                }
            }
        }


        //PRE STAGE 5 EVALUATION
        $pre_stage5_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $questions = Question::all();
                $question = $questions->find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == "CC 2022 9 Pre-Stage 5 Evaluation Form") {
                            $answer_text = $item->answer_text;
                            $object = (object)['question_number' => $question->question_number,
                                'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                'question_id' => $question_id];
                            array_push($pre_stage5_question_answers_array, $object);

                        }
                    }
                }
            }
        }

        //POST STAGE 5 EVALUATION
        $post_stage5_question_answers_array = [];
        if(count($incubatee_user->userQuestionAnswers) > 0){
            foreach ($incubatee_user->userQuestionAnswers as $item) {
                $question_id = $item->question_id;
                $questions = Question::all();
                $question = $questions->find($question_id);
                if(isset($question)){
                    if(isset($question->category->category_name)) {
                        if ($question->category->category_name == "CC 2022 Post-Stage 5 Evaluation Form") {
                            $answer_text = $item->answer_text;
                            $object = (object)['question_number' => $question->question_number,
                                'question_text' => $question->question_text, 'answer_text' => $answer_text,
                                'question_id' => $question_id];
                            array_push($post_stage5_question_answers_array, $object);

                        }
                    }
                }
            }
        }




        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/incubatees/admin-incubatees/incubatee-evaluations', compact('incubatee','workshop_evaluation_form_array','pre_stage2_question_answers_array','post_stage2_question_answers_array','pre_stage3_question_answers_array',
                'post_stage3_question_answers_array','pre_stage4_question_answers_array','post_stage4_question_answers_array','pre_stage5_question_answers_array',
                'post_stage5_question_answers_array','bootcamper','pre_bootcamp_question_answers_array','post_bootcamp_question_answers_array'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/incubatees/incubatee-evaluations', compact('incubatee','workshop_evaluation_form_array','pre_stage2_question_answers_array','post_stage2_question_answers_array','pre_stage3_question_answers_array',
                'post_stage3_question_answers_array','pre_stage4_question_answers_array','post_stage4_question_answers_array','pre_stage5_question_answers_array',
                'post_stage5_question_answers_array','bootcamper','pre_bootcamp_question_answers_array','post_bootcamp_question_answers_array'));
        }
    }

    //========================================================================================//
    //Admin incubatee Events
    public function adminViewIncubateeEvents(Incubatee $incubatee)
    {
        $logged_in_user = Auth::user()->load('roles');
        $today = Carbon::now()->toDateString();
        $incubatee->load('user', 'events');
        $events = Event::orderBy('start', 'desc')->get();
        $deregistered_events = [];

        if (isset($incubatee->events)) {
            foreach ($incubatee->events as $incubatee_event) {
                $event = $incubatee_event->events;
                $event_id = $incubatee_event->event_id;
                $event = Event::find($event_id);

                if ($incubatee_event->de_register_reason != null) {
                    $object = (object)['event' => $event->title,
                        'date_registered' => $incubatee_event->date_time_registered,
                        'attended' => 'No', 'event_id' => $incubatee_event->id,
                        'de_register_reason' => $incubatee_event->de_register_reason,
                        'date_time_de_register' => $incubatee_event->date_time_de_register];
                    array_push($deregistered_events, $object);
                }

            }
        }

        //EVENTS
        $incubatee_bootcamper_events_array = [];
        //Check if the incubatee has been through the bootcamper journey,
        if(isset($incubatee_user->bootcamper)){
            $incubatee_bootcamper = $incubatee_user->bootcamper;
            //load all bootcamper relationships
            $incubatee_bootcamper->load('ventureCategory', 'events', 'panelists','bootcampercontactLog');

            if(count($incubatee_bootcamper->events) > 0){
                $incubatee_bootcamper_events = $incubatee_bootcamper->events;

                //Loop through bootcamper events and store them in an array,
                //then push that newly created array to a view
                foreach ($incubatee_bootcamper_events as $b_event){
                    $event_id = $b_event->event_id;
                    $event = Event::find($event_id);
                    $object = (object)[
                        'event_title' => isset($event) ? $event->title : 'No title',
                        'accepted' => isset($b_event->accepted) ? 'Yes' : 'No',
                        'date_registered' => $b_event->date_registered,
                        'attended' => isset($b_event->attended) ? 'Yes' : 'No',
                        'declined' => isset($b_event->declined) ? 'Yes' : 'No',
                        'event_date' => $b_event->start];
                    array_push($incubatee_bootcamper_events_array, $object);
                }
            }
        }
        $incubatee_events_array = [];
        //Check if the incubatee has any events
        if(isset($incubatee->events)){
            $incubatee_events = $incubatee->events;
            if(count($incubatee_events) > 0){
                foreach($incubatee_events as $i_event){
                    $event_id = $i_event->event_id;
                    $event = Event::find($event_id);

                    $object = (object)[
                        'event_title' => isset($event->title) ? $event->title : 'No title set',
                        'event_date' => isset($event->start) ? $event->start : 'No date set',
                        'attended'=> $i_event->attended == false ? 'No':'Yes',
                        'date_time_registered' => isset($i_event->date_time_registered) ? $i_event->date_time_registered : 'No date time set',
                        'found_out_via' => isset($i_event->found_out_via) ? $i_event->found_out_via : 'Not set'];
                    array_push($incubatee_events_array, $object);
                }
            }
        }
        $incubatee_deregistered_events_array = [];
        //Check if the incubatee has any de-registered events
        if(isset($incubatee->incubateeDeregisters)){
            $incubatee_deregistered_events = $incubatee->incubateeDeregisters;
            if(count($incubatee_deregistered_events) > 0){
                foreach($incubatee_deregistered_events as $i_d_event){
                    $event_id = $i_d_event->event_id;
                    $event = Event::find($event_id);

                    $object = (object)['event_title' => $i_d_event->event_name, 'event_date' => isset($event->start),
                        'de_register_reason' => $i_d_event->de_register_reason, 'date_time_de_register' => $i_d_event->date_time_de_register];
                    array_push($incubatee_deregistered_events_array, $object);
                }
            }
        }


        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/incubatees/admin-incubatees/incubatee-events', compact('incubatee','events','deregistered_events','incubatee_bootcamper_events_array','incubatee_events_array','incubatee_deregistered_events_array'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/incubatees/incubatee-events', compact('incubatee','events','deregistered_events'));
        }
    }

    //Delete venture spreadsheet
    public function destroyUserEvents($id){
        $eventIncubatee = EventIncubatee::find($id);

        try{
            if(isset($eventIncubatee)){
                $eventIncubatee->forceDelete();
                return response()->json(['message' => 'Event incubatee Deleted Successfully'], 200);
            } else {
                return response("No file!");
            }
        } catch (\Exception $e){
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    //Admin regiter incubatee to an Event
    public function adminRegisterIncubateeEvent(Request $request){
        $input = $request->all();

        try{
            $incubatee = Incubatee::find($input['incubatee_id']);
            $event = Event::find($input['event_id']);
            $incubatee->load('events', 'user');
            $incubatee_user = $incubatee->user;
            $event->load('incubatees');
            $today = Carbon::now()->toDateString();

            foreach($incubatee->events as $b_event){
                if($b_event->event_id == $event->id){
                    return response()->json(['message' => 'Incubatee already registered for this event.']);
                }
            }

            DB::beginTransaction();

            $create_incubatee_event = EventIncubatee::create(['event_id' => $event->id, 'incubatee_id' => $incubatee->id,
                'accepted' => false, 'attended' => false, 'date_registered' => $today]);

            $email = $incubatee_user->email;
            $password = 'secret';
            $link = 'https://thepropella.co.za';
            $data = array(
                'name' => $incubatee_user->name,
                'email' => $email,
                'password' => $password,
                'link' => $link,
                'event_start' => $event->start->toDateString(),
                'event_title' => $event->title
            );

            $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
            $fromEmail = "dev@propellaincubator.co.za";
            $toEmail = $email;
            $subject = "Your Event Timese";
            $htmlBody = "<p>Dear <b>$incubatee_user->name</b></p>
            <p> You have now been assigned to $event->title. This event will take place on $event->start->toDateString().  </p>
            <br>
            <p>Please make sure to log in to your Propella dashboard by visiting $link and then accepting / declining your allocated time slots.</p>
            <br>
            <p>You can use these credentials to log in.</p>
            <p>Email: $email</p>
            <p>Password: $password</p>
            <br>
            <br>
            <p>Kind regards,</p>
            <p><b>Future Makers Team at Propella</b></p>";
            $textBody = "";
            $tag = "example-email-tag";
            $trackOpens = true;
            $trackLinks = "None";
            $messageStream = "broadcast";

            // Send an email:
            $sendResult = $client->sendEmail(
                $fromEmail,
                $toEmail,
                $subject,
                $htmlBody,
                $textBody,
                $tag,
                $trackOpens,
                NULL, // Reply To
                NULL, // CC
                NULL, // BCC
                NULL, // Header array
                NULL, // Attachment array
                $trackLinks,
                NULL, // Metadata array
                $messageStream
            );



            /*$email_field = $email;
            Mail::send('emails.assign-incubatee-to-event', $data, function ($message) use ($email_field) {
                $message->to($email_field)
                    ->subject('Your Event Times');
                $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

            });*/


            DB::commit();
            return response()->json(['message' => 'Incubatee now registered.']);
        } catch (\Exception $e){
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    public function adminGetIncubateeRegisteredEvents(Incubatee $incubatee)
    {
        $incubatee->load('events');
        $logged_in_user = Auth::user()->load('roles');
        $incubatee_events = [];
        $today = Carbon::now()->toDateString();

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            if (isset($incubatee->events)) {
                foreach ($incubatee->events as $incubatee_event) {
                    $events = $incubatee_event->events;
                    $event_id = $incubatee_event->event_id;
                    $event = Event::find($event_id);

                    if ($incubatee_event->attended == null or $incubatee_event->attended == false) {
                        $object = (object)['event' => isset($event->title) ? $event->title : 'No title',
                            'date_registered' => $incubatee_event->created_at->toDateString(),
                            'attended' => 'No', 'event_id' => $incubatee_event->id,
                            'de_register_reason'=> isset($incubatee_event->de_register_reason) ? $incubatee_event->de_register_reason : 'No reason',
                            'date_time_de_register'=> isset($incubatee_event->date_time_de_register) ? $incubatee_event->date_time_de_register : 'No date'
                        ];
                        array_push($incubatee_events, $object);
                    } else {
                        $object = (object)['event' => isset($event->title) ? $event->title : 'No title',
                            'date_registered' => $incubatee_event->created_at->toDateString(),
                            'attended' => 'Yes',
                            'event_id' => $incubatee_event->id,
                            'de_register_reason'=> isset($incubatee_event->de_register_reason) ? $incubatee_event->de_register_reason : 'No reason',
                            'date_time_de_register'=> isset($incubatee_event->date_time_de_register) ? $incubatee_event->date_time_de_register : 'No date'
                        ];
                        array_push($incubatee_events, $object);
                    }
                }

                return Datatables::of($incubatee_events)->addColumn('action', function ($incubatee_event) {
                    $re = '/admin/incubatee-registered-event-edit/' . $incubatee_event->event_id;
                    $del = '/delete-user-event/' . $incubatee_event->event_id;
                    return '<a href=' . $re . ' title="Edit Incubatee Event" style="color:green!important; cursor: pointer;"><i class="material-icons">create</i></a>
                     <a href=' . $del . ' title="Delete Applicant" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
                })
                    ->make(true);
            } else {
                return response()->json(['message' => 'No events found for this visitor']);
            }
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            if (isset($incubatee->events)) {
                $incubatee_events = [];

                foreach ($incubatee->events as $incubatee_event) {
                    $event_id = $incubatee_event->event_id;
                    $event = Event::find($event_id);

                    if ($incubatee_event->attended == null or $incubatee_event->attended == false) {
                        $object = (object)['event' => $event->title, 'date_registered' => $today,
                            'attended' => 'No', 'event_id' => $incubatee_event->id,
                            'de_register_reason'=>$incubatee_event->de_register_reason,
                            'date_time_de_register'=>$incubatee_event->date_time_de_register];
                        array_push($incubatee_events, $object);
                    } else {
                        $object = (object)['event' => $event->title, 'date_registered' => $today,
                            'attended' => 'Yes', 'event_id' => $incubatee_event->id,
                            'de_register_reason'=>$incubatee_event->de_register_reason,
                            'date_time_de_register'=>$incubatee_event->date_time_de_register];
                        array_push($incubatee_events, $object);
                    }
                }

                return Datatables::of($incubatee_events)->addColumn('action', function ($incubatee_event) {
                    $re = '/admin/incubatee-registered-event-edit/' . $incubatee_event->event_id;
                    return '<a href=' . $re . ' title="Edit Incubatee Event" style="color:green!important; cursor: pointer;"><i class="material-icons">create</i></a>';
                })
                    ->make(true);
            } else {
                return response()->json(['message' => 'No events found for this visitor']);
            }
        } else if ($logged_in_user->roles[0]->name == 'incubatee') {
            if (isset($incubatee->events)) {
                $incubatee_events = [];

                foreach ($incubatee->events as $incubatee_event) {
                    $event_id = $incubatee_event->event_id;
                    $event = Event::find($event_id);

                    if ($incubatee_event->attended == null or $incubatee_event->attended == false) {
                        $object = (object)['event' => $event->title, 'date_registered' => $today,
                            'attended' => 'No', 'event_id' => $incubatee_event->id,
                            'de_register_reason'=>$incubatee_event->de_register_reason,
                            'date_time_de_register'=>$incubatee_event->date_time_de_register];
                        array_push($incubatee_events, $object);
                    } else {
                        $object = (object)['event' => $event->title, 'date_registered' => $today,
                            'attended' => 'Yes', 'event_id' => $incubatee_event->id,
                            'de_register_reason'=>$incubatee_event->de_register_reason,
                            'date_time_de_register'=>$incubatee_event->date_time_de_register];
                        array_push($incubatee_events, $object);
                    }
                }

                return Datatables::of($incubatee_events)->addColumn('action', function ($incubatee_event) {
                    $re = '/admin/incubatee-registered-event-edit/' . $incubatee_event->event_id;
                    return '<a href=' . $re . ' title="Edit Incubatee Event" style="color:green!important; cursor: pointer;"><i class="material-icons">create</i></a>';
                })
                    ->make(true);
            } else {
                return response()->json(['message' => 'No events found for this visitor']);
            }
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function adminGetIncubateeDeRegisteredEvents(Incubatee $incubatee)
    {
        $incubatee->load('incubateeDeregisters');
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return Datatables::of($incubatee->incubateeDeregisters)->addColumn('action', function ($incubatee_de_registered_event) {
                $re = '/admin/incubatee-event-edit/' . $incubatee_de_registered_event->id;
                return '<a href=' . $re . ' title="Edit Incubatee Event" style="color:green!important; cursor: pointer;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($incubatee->incubateeDeregisters)->addColumn('action', function ($incubatee_de_registered_event) {
                $re = '/admin/incubatee-event-edit/' . $incubatee_de_registered_event->id;
                return '<a href=' . $re . ' title="Edit Incubatee Event" style="color:green!important; cursor: pointer;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function adminEditIncubateeRegisteredEvent(EventIncubatee $eventIncubatee)
    {
        $logged_in_user = Auth::user()->load('roles');
        $eventIncubatee->load('incubatees', 'events');
        $incubatee = $eventIncubatee-> load('incubatees');
//dd($eventIncubatee);
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/incubatees/admin-incubatees/incubatee-registered-event-edit', compact('eventIncubatee'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/incubatees/incubatee-registered-event-edit', compact('incubatee','eventIncubatee'));
        }else if ($logged_in_user->roles[0]->name == 'incubatee') {
            return view('/users/incubatee-dashboard/incubatee/incubatee-registered-event-edit', compact('incubatee','eventIncubatee'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateProfile(Request $request, User $user)
    {
        $input = $request->all();
        $temp_user = $user;
        DB::beginTransaction();
        try {
            if ($request->has('profile_picture_url')) {
                $path = $request->file('profile_picture_url')->store('users');
                $user->update(['name' => $input['name'], 'surname' => $input['surname'], 'contact_number' => $input['contact_number'],
                    'address' => $input['address'], 'profile_picture_url' => $path, 'email' => $input['email']]);
            } elseif ($request->has('profile_picture_url')) {
                $path = $request->file('profile_picture_url')->store('users');
                $user->update(['name' => $input['name'], 'surname' => $input['surname'], 'contact_number' => $input['contact_number'],
                    'address' => $input['address'], 'profile_picture_url' => $path, 'email' => $input['email']]);
            } else {
                $user->update(['name' => $input['name'], 'surname' => $input['surname'], 'contact_number' => $input['contact_number'],
                    'address' => $input['address'], 'email' => $input['email']]);

            }
            DB::commit();
            $user = $temp_user->fresh();
            return response()->json(['message' => 'Your profile has been updated successfully', 'user' => $user], 200);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'An error occured while trying to update your profile ' . $e->getMessage()], 500);
        }
    }


    //========================================================================================//
    public function getMentors()
    {
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'mentor');
        })->get();

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($users->load('mentor'))->addColumn('action', function ($user) {
                $re = '/edit-user/' . $user->id;
                $sh = '/mentor-show/' . $user->id;
                $del = '/delete-mentor/' .$user->id;
                return '<a href=' . $re . ' title="Edit Mentor" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">visibility</i></a><a href=' . $del . ' title="Delete Mentor" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);

        } elseif($user->roles[0]->name == 'mentor') {
            return Datatables::of($users->load('mentor'))->addColumn('action', function ($user) {
                $re = '/edit-user/' . $user->id;
                $sh = '/mentor-show/' . $user->id;
                $del = '/delete-mentor/' .$user->id;
                return '<a href=' . $re . ' title="Edit Mentor" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">visibility</i></a><a href=' . $del . ' title="Delete Mentor" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
        elseif($user->roles[0]->name == 'advisor') {
            return Datatables::of($users->load('mentor'))->addColumn('action', function ($user) {
                $re = '/edit-user/' . $user->id;
                $sh = '/mentor-show/' . $user->id;
                $del = '/delete-mentor/' .$user->id;
                return '<a href=' . $re . ' title="Edit Mentor" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">visibility</i></a><a href=' . $del . ' title="Delete Mentor" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif($user->roles[0]->name == 'administrator') {
            return Datatables::of($users->load('mentor'))->addColumn('action', function ($user) {
                $re = '/edit-user/' . $user->id;
                $sh = '/mentor-show/' . $user->id;
                $del = '/delete-mentor/' .$user->id;
                return '<a href=' . $re . ' title="Edit Mentor" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">visibility</i></a><a href=' . $del . ' title="Delete Mentor" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif($user->roles[0]->name == 'marketing') {
            return Datatables::of($users->load('mentor'))->addColumn('action', function ($user) {
                $re = '/edit-user/' . $user->id;
                $sh = '/mentor-show/' . $user->id;
                $del = '/delete-mentor/' .$user->id;
                return '<a href=' . $re . ' title="Edit Mentor" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $sh . ' title="Show Event" style="color:orange!important;"><i class="material-icons">visibility</i></a><a href=' . $del . ' title="Delete Mentor" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    //MENTORS AND VENTURES
    //Details of a single mentor feedback
    public function showMentorFeedback(Mentor $mentor)
    {
        $logged_in_user = Auth::user()->load('roles');
        $mentor->load('questionAnswers','venture','user');
        $venture =$mentor->venture;

        $userQuestionAnswers = [];

        foreach ($mentor->questionAnswers as $userQuestionAnswer) {
            $shadow_board_question_id = $userQuestionAnswer->shadow_board_question_id;
            $question = ShadowBoardQuestion::find($shadow_board_question_id);
            $answer_text = $userQuestionAnswer->answer_text;
            $object = (object)[
                'question_number'=>isset($question->question_number)? $question->question_number:'No question number',
                'question_text'=>isset($question->question_text) ? $question->question_text:'No question text',
                'answer_text' => $answer_text];

            array_push($userQuestionAnswers, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.mentors.show-mentor-answers',compact('mentor','question','userQuestionAnswers','venture'));
        }
    }

    public function showVentureFeedback(Venture $venture)
    {
        $logged_in_user = Auth::user()->load('roles');
        $venture->load('mentor','ventureShadow','questionAnswers');
        $v_q_a = $venture->questionAnswers;

        $v_shadow = $venture->ventureShadow;

        $venture_shadow = [];
        $ventureQuestionAnswers = [];

        foreach ($v_shadow as $ven){
            foreach ($v_q_a as $userQuestionAnswer) {
                $shadow_board_question_id = $userQuestionAnswer->shadow_board_question_id;
                $question = ShadowBoardQuestion::find($shadow_board_question_id);
                $answer_text = $userQuestionAnswer->answer_text;
                $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                    'answer_text' => $answer_text];
                array_push($ventureQuestionAnswers, $object);
            }
            $object = (object)['shadow_board_date'=>$ven->shadow_board_date,'shadow_board_time'=>$ven->shadow_board_time,
                'ventureQuestionAnswers' => $ventureQuestionAnswers];
            array_push($venture_shadow, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.mentors.show-venture-answers',compact('venture','v_shadow','venture_shadow','ventureQuestionAnswers'));
        }
    }

    public function showMentorOnApp(Venture $venture){
        $user = Auth::user()->load('roles');
        $venture->load('mentorQuestionAnswers','ventureShadow','mentor');

        $v_shadow = $venture->ventureShadow;

        $venture_shadow = [];
        $ventureQuestionAnswers = [];

        foreach ($v_shadow as $ven){
            foreach ($venture->mentorQuestionAnswers as $userQuestionAnswer) {
                $shadow_board_question_id = $userQuestionAnswer->shadow_board_question_id;
                $question = ShadowBoardQuestion::find($shadow_board_question_id);
                $answer_text = $userQuestionAnswer->answer_text;
                $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                    'answer_text' => $answer_text];
                array_push($ventureQuestionAnswers, $object);
            }
            $object = (object)['shadow_board_date'=>$ven->shadow_board_date,'shadow_board_time'=>$ven->shadow_board_time,
                'ventureQuestionAnswers' => $ventureQuestionAnswers];
            array_push($venture_shadow, $object);
        }

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.mentors.show-feedback-on-admin',compact('venture','v_shadow','venture_shadow','ventureQuestionAnswers'));
        }
    }

    //Details of a single applicant
    public function showApplicant(User $user){
        $logged_in_user = Auth::user()->load('roles');
        $user->load('applicant', 'userQuestionAnswers');
        $user_applicant = $user->applicant;
        $user_applicant->load('contactLog');


        $applicant = $user->applicant;
        $applicant->load('panelists');
        $applicantion_panelists = [];
        $question_categories = QuestionsCategory::all();

        if(count($applicant->panelists) > 0){
            foreach($applicant->panelists as $applicant_panelist){
                $applicant_panelist->load('panelist');
                $applicant_panelist_question_category_id = $applicant_panelist->question_category_id;
                $applicant_panelist_question_category = QuestionsCategory::find($applicant_panelist_question_category_id);
                $panelist = $applicant_panelist->panelist;
                $panelist->load('user');
                $panelist_user = $panelist->user;



                $object = (object)['name' => $panelist_user->name, 'surname' => $panelist_user->surname,
                    'title' => $panelist_user->title, 'initials' => $panelist_user->initials,
                    'company_name' => $panelist_user->company_name, 'position' => $panelist_user->position,
                    'email' => $panelist_user->email, 'contact_number' => $panelist_user->contact_number,
                    'applicant_panelist_id' => $applicant_panelist->id, 'question_category_id' => $applicant_panelist_question_category->id,
                    'question_category' => $applicant_panelist_question_category->category_name];


                array_push($applicantion_panelists, $object);

            }
        }

        $userQuestionAnswers = [];
        $applicant = $user->applicant;

        foreach ($user->userQuestionAnswers as $userQuestionAnswer) {
            $question_id = $userQuestionAnswer->question_id;
            $question = Question::find($question_id);
            $answer_text = $userQuestionAnswer->answer_text;
            $object = (object)[
                'question_number' => isset($question->question_number)? $question->question_number:'No question number',
                'question_text' => isset($question->question_text)? $question->question_text:'No question text',
                'answer_text' => $answer_text];

            array_push($userQuestionAnswers, $object);
        }


        $applicant_panelists = $applicant->panelists;
        $final_panelist_question_score_array = [];
        $panelists_question_score_collection = [];
        $final_question_numbers = [];
        $total_panelist_score = 0;
        $panelist_count = 0;

        foreach($applicant_panelists as $applicant_panelist){
            $applicant_panelist->load('panelist');
            $cur_panelist = $applicant_panelist->panelist;
            $cur_panelist->load('user');
            $cur_user = $cur_panelist->user;
            $cur_user_name = $cur_user->name;
            $cur_user_surname = $cur_user->surname;
            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;

            $panelist_question_answers = PanelistQuestionAnswer::where('applicant_panelist_id', $applicant_panelist->id)->get();

            foreach ($panelist_question_answers as $panelist_question_answer){
                $question_id = $panelist_question_answer->question_id;
                $cur_question = Question::find($question_id);
                $cur_question_number = $cur_question->question_number;
                $cur_score = $panelist_question_answer->score;
                $panelist_applicant_score += $cur_score;
                $object = (object)['question_number' => $cur_question_number,
                    'question_score' => $cur_score];
                array_push($panelist_questions_scores, $object);
                array_push($panelists_question_score_collection, $object);
                array_push($final_question_numbers, $cur_question_number);
            }

            $object = (object)['name' => $cur_user_name, 'surname' => $cur_user_surname,
                'user_questions_scores' => $panelist_questions_scores, 'panelist_applicant_score' => $panelist_applicant_score,
                'applicant_panelist_id' => $applicant_panelist->id];
            array_push($final_panelist_question_score_array, $object);

            $total_panelist_score += $panelist_applicant_score;

            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;
        }

        foreach ($final_panelist_question_score_array as $pqsa){
            if(count($pqsa->user_questions_scores)){
                $panelist_count += 1;
            }
        }

        if($panelist_count > 0){
            $average_panelist_score = $total_panelist_score / $panelist_count;
        } else {
            $average_panelist_score = 0;
        }

        $today = Carbon::now()->toDateString();
        $venture_categories = VentureCategory::all();
        $incubatee_stages = IncubateeStage::all();

        $events_array = Event::orderBy('start', 'asc')->get();
        $events = [];
        $declined_applicant_categories = DeclinedApplicantCategory::all()->where('category_name','!=','Panel decision declined');
        $referred_applicant_categories = ReferredApplicantCategory::all();
        $user_applicant->load('contactLog','panelists');

        //Store total panel score and average applicant score to the database for further use
        DB::beginTransaction();

        $applicant->save();
        DB::commit();

        $question_scores_array = [];
        $question_score = 0;

        foreach (array_unique($final_question_numbers) as $q_number){
            foreach ($applicant_panelists as $applicant_panelist) {
                $applicant_panelist->load('panelistQuestionAnswers');
                $applicant_panelist_questions = $applicant_panelist->panelistQuestionAnswers;
                foreach ($applicant_panelist_questions as $applicant_panelist_question){
                    $question_id = $applicant_panelist_question->question_id;
                    $cur_question = Question::find($question_id);
                    if($cur_question->question_number == $q_number){
                        $question_score += $applicant_panelist_question->score;
                    }
                }
            }
            $object = (object)['question_number' => $q_number, 'question_score' => $question_score];
            array_push($question_scores_array, $object);
            $question_score = 0;
        }

        foreach ($events_array as $event){
            $start_time = $event->start->toDateString();
            if($start_time >= $today){
                array_push($events,$event);
            }
        }


        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/show-applicant', compact('user', 'applicant', 'userQuestionAnswers','user_applicant',
                'question_categories','declined_applicant_categories','applicantion_panelists','final_panelist_question_score_array', 'question_scores_array', 'applicant',
                'total_panelist_score', 'average_panelist_score','venture_categories','incubatee_stages','events','referred_applicant_categories'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/show-applicant', compact('user', 'applicant', 'userQuestionAnswers','user_applicant',
                'question_categories','declined_applicant_categories','applicantion_panelists','final_panelist_question_score_array', 'question_scores_array', 'applicant',
                'total_panelist_score', 'average_panelist_score','venture_categories','incubatee_stages','events','referred_applicant_categories'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/show-applicant', compact('user', 'applicant', 'userQuestionAnswers','user_applicant',
                'question_categories','declined_applicant_categories','applicantion_panelists','final_panelist_question_score_array', 'question_scores_array', 'applicant',
                'total_panelist_score', 'average_panelist_score','venture_categories','incubatee_stages','events','referred_applicant_categories'));
        } else if ($logged_in_user->roles[0]->name == 'panelist') {
            return view('/users/panelist/applicants/show-applicant-questions', compact('user', 'applicant', 'userQuestionAnswers'));
        }else if ($logged_in_user->roles[0]->name == 'PTI Admin') {
            return view('/users/marketing-dashboard/applicants/show-applicant', compact('user', 'applicant', 'userQuestionAnswers'));
        }else if ($logged_in_user->roles[0]->name == 'marketing') {
            return view('/users/pti-dashboard/show-applicant', compact('user', 'applicant', 'userQuestionAnswers','user_applicant',
                'question_categories','declined_applicant_categories','applicantion_panelists','final_panelist_question_score_array', 'question_scores_array', 'applicant',
                'total_panelist_score', 'average_panelist_score','venture_categories','incubatee_stages','events','referred_applicant_categories'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function panelistShowIctApplicant(User $user){
        $logged_in_user = Auth::user()->load('roles');
        $user->load('applicant', 'userQuestionAnswers');
        $userQuestionAnswers = [];
        $applicant = $user->applicant;

        foreach ($user->userQuestionAnswers as $userQuestionAnswer) {
            $question_id = $userQuestionAnswer->question_id;
            $question = Question::find($question_id);
            $answer_text = $userQuestionAnswer->answer_text;
            if(isset($question)) {
                if (isset($question->category->category_name)) {
                    if ($question->category->category_name == 'AA Application form - ICT Bootcamp'
                        or $question->category->category_name == 'AA Application form - Propella Satellite'
                        or $question->category->category_name == 'AA Application form - RAP Programme') {
                        $object = (object)[
                            'question_number' => isset($question->question_number) ? $question->question_number : 'No question number',
                            'question_text' => isset($question->question_text) ? $question->question_text : 'No question text',
                            'answer_text' => $answer_text];

                        array_push($userQuestionAnswers, $object);
                    }
                }
            }
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/bootcampers/show-ict-applicant', compact('user', 'applicant', 'userQuestionAnswers'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/bootcampers/show-ict-applicant', compact('user', 'applicant', 'userQuestionAnswers'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/bootcampers/show-ict-applicant', compact('user', 'applicant', 'userQuestionAnswers'));
        } else if ($logged_in_user->roles[0]->name == 'panelist') {
            return view('/users/panelist/bootcampers/show-ict-applicant', compact('user', 'applicant', 'userQuestionAnswers'));
        } else if ($logged_in_user->roles[0]->name == 'marketing') {
            return view('/users/marketing-dashboard/bootcampers/show-ict-applicant', compact('user', 'applicant', 'userQuestionAnswers'));
        } else {
            return response('You are not authorized!');
        }
    }

    //SHOW INCUBATEES ASSIGNED TO A MENTOR
    public function showMentorInfo(User $user){
        $user->load('mentor','incubatee','roles');
        $user_mentor = $user->mentor;
        $user_venture = $user->incubatee;

        $role = Role::with('users')->where('name', 'mentor')->get();
        $venture =Venture::orderBy('company_name', 'asc')->get();


        $loggend_in_user = Auth::user()->load('roles');

        if ($loggend_in_user->roles[0]->name == 'app-admin') {
            return view('users.mentors.admin-mentors.show-mentor-info',compact('user','role','venture', 'user_mentor','user_venture','user_venture'));
        } else if($loggend_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.mentors.show-mentor-info',compact('user','role','venture', 'user_mentor','user_venture','user_venture'));
        } else if($loggend_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.mentors.show-mentor-info',compact('user','role','venture', 'user_mentor','user_venture','user_venture'));
        } elseif ($loggend_in_user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.mentors.admin-mentors.show-mentor-info',compact('user','role','venture', 'user_mentor','user_venture','user_venture'));
        } elseif ($loggend_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.mentors.show-mentor-info',compact('user','role','venture', 'user_mentor','user_venture','user_venture'));
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function assignVenturesToMentor(Request $request)
    {
        $input = $request->all();
        $selected_ventures = json_decode($input['selected_ventures']);
        $mentor_id = $input['mentor_id'];
        $mentor = Mentor::find($mentor_id);

        try {
            DB::beginTransaction();

            foreach ($selected_ventures as $selected_venture) {
                $venture_id = $selected_venture;
                $venture = Venture::find($venture_id);

                $venture->update(['mentor_id' => $mentor->id]);
                $venture->save();
            }

            DB::commit();
            return response()->json(['message' => 'Successfully assigned venture(s).'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function adminAssignShadowBoard(Request $request){
        $input = $request->all();
        $selected_ventures = json_decode($input['selected_ventures']);
        $mentor_id = $input['mentor_id'];
        $mentor = Mentor::find($mentor_id);
        $mentor->load('user');
        $mentor_user = $mentor->user;

        try {
            DB::beginTransaction();

            foreach ($selected_ventures as $selected_venture) {
                $venture_id = $selected_venture;
                $venture = Venture::find($venture_id);
                $venture->load('incubatee');

                if (count($venture->incubatee) > 0) {
                    foreach ($venture->incubatee as $ven_inc) {
                        $ven_inc->load('user');
                        $inc_user = $ven_inc->user;
                        $user_email = $inc_user->email;
                        $name = $inc_user->name;
                        $data = array(
                            'email' => $user_email,
                            'name' => $name,
                        );

                        $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                        $fromEmail = "dev@propellaincubator.co.za";
                        $toEmail = $user_email;
                        $subject = "Your Shadow board Times";
                        $htmlBody = "<p>Dear <b>$name</b></p>
                        <p>Your Shadow Board is due to occur tomorrow.</p>
                        <p>Please ensure you have competed your<span><b>3 Monthly Evaluation Form on your Mentor prior to your Shadow Board</b></span> by logging in to : www.thepropella.co.za .</p>
                        <p>If you have not completed it then please ensure you arrive 10 minutes early for your Shadow Board and complete on our Propella tablet before your Shadow Board starts.</p>
                        <p>This is important to ensure the success of the Mentoring Relationship and to enable the Mentor to keep you moving forward.</p>
                        <p>You are required to provide feedback on your Action Items from previous Shadow Board.</p>
                        <p>The agenda for the Shadow Board is below in case you require it:
                        </p>
                        <p><b> SHADOW BOARD MEETING</b></p>
                        <p>1)	Present: Incubatee / Mentor / Advisor / Incubation Manager / AN Other</p>
                        <p>2)	Meets quarterly at Propella</p>
                        <p>3)	Incubatee reviews roadmap and discusses goals set for next quarter </p>
                        <p><b>Agenda</b></p>
                        <p> 1)	Strategy & Stakeholders</p>
                        <p> 2)	Marketing</p>
                        <p> 3)	Sales (achieved and pipeline)</p>
                        <p> 4)	Finance (performance against budget)</p>
                        <p> 5)	Legal & Compliance (including HR)</p>
                        <p> 6)	Operations (Process / Admin)</p>
                        <p> 7)	Product / Technology Development</p>
                        <p> 8)	Entrepreneur / Personal Development</p>
                        <p> 9)	General</p>
                        <p> 10)	Dates for the next 3 months</p>
                        <p> -	Incubatee / Mentor</p>
                        <p> -	Propella Advisor</p>
                        <p> -	Next Shadow Board</p>
                        <br>
                        <p>With many thanks.</p>
                        The Propella Team";
                        $textBody = "";
                        $tag = "example-email-tag";
                        $trackOpens = true;
                        $trackLinks = "None";
                        $messageStream = "broadcast";

                        // Send an email:
                        $sendResult = $client->sendEmail(
                            $fromEmail,
                            $toEmail,
                            $subject,
                            $htmlBody,
                            $textBody,
                            $tag,
                            $trackOpens,
                            NULL, // Reply To
                            NULL, // CC
                            NULL, // BCC
                            NULL, // Header array
                            NULL, // Attachment array
                            $trackLinks,
                            NULL, // Metadata array
                            $messageStream
                        );
                    }
                }

                $mentor_venture = MentorVentureShadowboard::create(['mentor_id' => $mentor->id,'venture_id' => $venture->id,'shadow_board_time' => $input['shadow_board_time'],'shadow_board_date'=>$input['shadow_board_date']]);
                $mentor_venture->save();


                $mentor_email = $mentor_user->email;
                $name = $mentor_user->name;

                $data = array(
                    'email' => $mentor_email,
                    'name' => $name,
                );
                $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                $fromEmail = "dev@propellaincubator.co.za";
                $toEmail = $mentor_email;
                $subject = "Your Shadow board Times for " .$inc_user->name ;
                $htmlBody = "<p>Dear <b>$name</b></p>
           <p>Your Shadow Board is due to occur tomorrow.</p>
            <p>Please ensure you have competed your<span><b>3 Monthly Evaluation Form on your Mentee prior to your Shadow Board</b></span> by logging in to : www.thepropella.co.za .</p>
            <p>If you have not completed it then please ensure you arrive 10 minutes early for your Shadow Board and complete on our Propella tablet before your Shadow Board starts.</p>
            <p>This is important to ensure the success of the Mentoring Relationship and to enable the Mentee to keep moving forward.</p>
            <p>Your Mentee is required to provide feedback on <span><b>Action Items</b></span> from previous Shadow Board.
            </p>
            <p>The agenda for the Shadow Board is below in case you require it:
            </p>
            <p><b> SHADOW BOARD MEETING</b></p>
            <p>1)	Present: Incubatee / Mentor / Advisor / Incubation Manager / AN Other</p>
            <p>2)	Meets quarterly at Propella</p>
            <p>3)	Incubatee reviews roadmap and discusses goals set for next quarter </p>
            <p><b>Agenda</b></p>
            <p> 1)	Strategy & Stakeholders</p>
            <p> 2)	Marketing</p>
            <p> 3)	Sales (achieved and pipeline)</p>
            <p> 4)	Finance (performance against budget)</p>
            <p> 5)	Legal & Compliance (including HR)</p>
            <p> 6)	Operations (Process / Admin)</p>
            <p> 7)	Product / Technology Development</p>
            <p> 8)	Entrepreneur / Personal Development</p>
            <p> 9)	General</p>
            <p> 10)	Dates for the next 3 months</p>
            <p> -	Incubatee / Mentor</p>
            <p> -	Propella Advisor</p>
            <p> -	Next Shadow Board</p>
            <br>
            <p>With many thanks.</p>
            The Propella Team";
                $textBody = "";
                $tag = "example-email-tag";
                $trackOpens = true;
                $trackLinks = "None";
                $messageStream = "broadcast";

                // Send an email:
                $sendResult = $client->sendEmail(
                    $fromEmail,
                    $toEmail,
                    $subject,
                    $htmlBody,
                    $textBody,
                    $tag,
                    $trackOpens,
                    NULL, // Reply To
                    NULL, // CC
                    NULL, // BCC
                    NULL, // Header array
                    NULL, // Attachment array
                    $trackLinks,
                    NULL, // Metadata array
                    $messageStream
                );
            }

           /* $email_field = $mentor_email;
            Mail::send('emails.mentor-shadowboard-emailer', $data, function ($message) use ($email_field) {
                $message->to($email_field)
                    ->subject('Your Shadow board Times');
                $message->from('propellabusinessincubator@gmail.com', 'Propella Business Incubator');

            });*/


            DB::commit();
            return response()->json(['message' => 'Completed.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function adminAssignShadowBoardMentee(Request $request){
        $input = $request->all();
        $selected_ventures = json_decode($input['selected_ventures']);
        $mentor_id = $input['mentor_id'];
        $mentor = Mentor::find($mentor_id);
        $mentor->load('user');
        $mentor_user = $mentor->user;

        try {
            DB::beginTransaction();

            foreach ($selected_ventures as $selected_venture) {
                $venture_id = $selected_venture;
                $venture = Venture::find($venture_id);
                $venture->load('incubatee');

                if (count($venture->incubatee) > 0) {
                    foreach ($venture->incubatee as $ven_inc) {
                        $ven_inc->load('user');
                        $inc_user = $ven_inc->user;
                        $user_email = $inc_user->email;
                        $name = $inc_user->name;
                    }
                }

            }

            DB::commit();
            return response()->json(['message' => 'Completed.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function deleteMentor($id){
        try {
            DB::beginTransaction();
            $user = User::find($id);
            $user->load('mentor');
            $user_mentor = $user->mentor;

            if(isset($user_mentor)){
                $user_mentor->load('questionAnswers');
                if(isset($user_mentor->questionAnswers)){
                    foreach ($user_mentor->questionAnswers as $item) {
                        $item->forceDelete();
                    }
                }
            }
            $user_mentor->forceDelete();

            $user->load('events', 'userDeregisters');
            if (isset($user->events)) {
                foreach ($user->events as $m_events) {
                    $m_events->forceDelete();
                }
            }

            if(isset($user->userDeregisters)){
                foreach ($user->userDeregisters as $m_dr) {
                    $m_dr->forceDelete();
                }
            }
            $user->forceDelete();

            DB::commit();
            return response()->json(['message' => 'Mentor deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Mentor cannot be deleted ' . $e->getMessage()], 500);
        }
    }

    //Mentor venture shadow board
    public function mentorVentureShadowIndex(User $user){
        $user->load('mentor','incubatee','roles');
        $user_mentor = $user->mentor;
        $user_logged_in = Auth::user()->load('roles');
        if ($user_logged_in->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.mentors.mentor-venture-shadow-index',compact('user','user_mentor'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Mentor venture shadow board
    public function ventureShadowboardIndex(Venture $venture){
        $venture->load('mentor','ventureShadow');

        $user_logged_in = Auth::user()->load('roles');
        if ($user_logged_in->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture.venture-shadow-index',compact('venture'));
        }
    }

    public function getVentureShadowboards(Venture $venture){
        $venture->load('ventureShadow','mentor');
        $user = Auth::user()->load('roles');
        $venture_shadowboards = [];

        foreach($venture->ventureShadow as $v_shadow){
            $v_shadow->load('mentor');
            $mentor = $v_shadow->mentor;
            $mentor->load('user');
            $mentor_user = $mentor->user;

            $object = (object)['shadow_board_date' => $v_shadow->shadow_board_date, 'shadow_board_time' => $v_shadow->shadow_board_time,
                'mentor' => $mentor_user->name . " " . $mentor_user->surname, 'mentor_venture_id' => $v_shadow->id];
            array_push($venture_shadowboards, $object);
        }

        if ($user->roles[0]->name == 'incubatee') {
            return Datatables::of($venture_shadowboards)->addColumn('action', function ($v_shadow) {
                $show = '/venture-shadowboard-form/' .$v_shadow->mentor_venture_id;
                return '<a href=' . $show . ' title="Show shadow board form" style="color:green!important;"><i class="material-icons">visibility</i></a>';
            })
                ->make(true);
        }

    }

    public function getMentorVentureShadow(Mentor $mentor)
    {
        $mentor->load('mentorVentureShadow', 'venture');
        $v_m_array = [];

        $user = Auth::user()->load('roles');
        foreach ($mentor->mentorVentureShadow as $m_v_shadow) {
            $venture_id = $m_v_shadow->venture_id;
            $venture = Venture::find($venture_id);
            if(isset($venture->company_name)) {
                $object = (object)['company_name' => $venture->company_name, 'shadow_board_date' => $m_v_shadow->shadow_board_date,
                    'shadow_board_time' => $m_v_shadow->shadow_board_time, 'id' => $m_v_shadow->id];
                array_push($v_m_array, $object);
            }else{
                $object = (object)['company_name'=>'No venture name', 'shadow_board_date' => $m_v_shadow->shadow_board_date,
                    'shadow_board_time' => $m_v_shadow->shadow_board_time, 'id' => $m_v_shadow->id];
                array_push($v_m_array, $object);
            }
        }

        if ($user->roles[0]->name == 'mentor') {
            return Datatables::of($v_m_array)->addColumn('action', function ($m_v_sb) {
                $show = '/mentor-shadow-board-form/' . $m_v_sb->id;
                $del = '/delete-shadow-board/' . $m_v_sb->id;
                return '<a href=' . $show . ' title="Show shadow board form" style="color:green!important;"><i class="material-icons">visibility</i></a><a href=' . $del . ' title="Delete Shadow board" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function destroyShadowBoard(MentorVentureShadowboard $mentorVentureShadowboard){
        try {
            DB::beginTransaction();

            $mentorVentureShadowboard->forceDelete();

            DB::commit();
            return response('Shadow board deleted successfully');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }
    }

    public function ventureShadowboardForm(MentorVentureShadowboard $mentorVentureShadowboard){
        $user = Auth::user()->load('roles');
        $mentorVentureShadowboard->load('venture','mentor', 'ventureAnswers');
        $ventureQuestionsAnswers = [];
        $venture = $mentorVentureShadowboard->venture;
        $mentor = $mentorVentureShadowboard->mentor;
        $mentor->load('user');
        $question_category = ShadowBoardQuestionCategory::where('category_name', 'Venture Questions')->firstOrFail();
        $question_category->load('shadowBoardQuestion');
        $questions = $question_category->shadowBoardQuestion->sortBy('question_number');

        if(count($mentorVentureShadowboard->ventureAnswers) > 0){
            foreach($mentorVentureShadowboard->ventureAnswers as $ventureAnswer){
                $shadowboardQuestionId = $ventureAnswer->shadow_board_question_id;
                $shadowBoardQuestion = ShadowBoardQuestion::find($shadowboardQuestionId);
                $shadowBoardQuestion->load('shadowBoardQuestionSubQuestionText');
                $shadowboardQuestionSubTexts = [];
                if(count($shadowBoardQuestion->shadowBoardQuestionSubQuestionText) > 0){
                    foreach($shadowBoardQuestion->shadowBoardQuestionSubQuestionText as $item)
                        $object = (object)['question_sub_text' => $item->question_sub_text];
                    array_push($shadowboardQuestionSubTexts, $object);
                }
                $answerText = $ventureAnswer->answer_text;

                $object = (object)['id' => $shadowBoardQuestion->id, 'question_number' => $shadowBoardQuestion->question_number, 'question_text' => $shadowBoardQuestion->question_text,
                    'question_sub_texts' => $shadowboardQuestionSubTexts, 'question_type' => $shadowBoardQuestion->question_type, 'answer_text' => $answerText];
                array_push($ventureQuestionsAnswers, $object);
            }
        }

        if ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture.shadow-venture-form',compact('mentorVentureShadowboard','question_category','questions','venture','mentor', 'ventureQuestionsAnswers'));
        }
    }

    public function mentorShadowboardForm(MentorVentureShadowboard $mentorVentureShadowboard){
        $mentorVentureShadowboard->load('mentor', 'venture', 'mentorAnswers');
        $mentorQuestionsAnswers = [];

        if(count($mentorVentureShadowboard->mentorAnswers) > 0){
            foreach($mentorVentureShadowboard->mentorAnswers as $mentorAnswer){
                $shadowboardQuestionId = $mentorAnswer->shadow_board_question_id;
                $shadowBoardQuestion = ShadowBoardQuestion::find($shadowboardQuestionId);
                $shadowBoardQuestion->load('shadowBoardQuestionSubQuestionText');
                $shadowboardQuestionSubTexts = [];
                if(count($shadowBoardQuestion->shadowBoardQuestionSubQuestionText) > 0){
                    foreach($shadowBoardQuestion->shadowBoardQuestionSubQuestionText as $item)
                    $object = (object)['question_sub_text' => $item->question_sub_text];
                    array_push($shadowboardQuestionSubTexts, $object);
                }
                $answerText = $mentorAnswer->answer_text;

                $object = (object)['id' => $shadowBoardQuestion->id, 'question_number' => $shadowBoardQuestion->question_number, 'question_text' => $shadowBoardQuestion->question_text,
                    'question_sub_texts' => $shadowboardQuestionSubTexts, 'question_type' => $shadowBoardQuestion->question_type, 'answer_text' => $answerText];
                array_push($mentorQuestionsAnswers, $object);
            }
        }

        $venture = $mentorVentureShadowboard->venture;
        $user = Auth::user()->load('roles');
        $user->load('mentor');
        $user_mentor = $user->mentor;
        $user_mentor->load('venture','user', 'mentorVentureShadow');

        $question_category = ShadowBoardQuestionCategory::where('category_name', 'Mentor Questions')->firstOrFail();
        $question_category->load('shadowBoardQuestion');
        $questions = $question_category->shadowBoardQuestion->sortBy('question_number');

        if ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.mentors.shadow-mentor-form',compact('venture','questions','user_mentor', 'mentorVentureShadowboard', 'mentorQuestionsAnswers'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getVentureAssigned(Mentor $mentor)
    {
        $mentor->load('venture','user', 'mentorVentureShadow');
        $assigned_venture_array = [];
        $user = Auth::user()->load('roles');

        foreach ($mentor->venture as $mentor_venture){
            $object = (object)['company_name'=>$mentor_venture->company_name,'contact_number'=>$mentor_venture->contact_number,'id'=>$mentor_venture->id];
            array_push($assigned_venture_array, $object);
        }

        if ($user->roles[0]->name == 'app-admin' or
            $user->roles[0]->name == 'administrator' or
            $user->roles[0]->name == 'advisor' or
            $user->roles[0]->name == 'marketing') {
            return Datatables::of($assigned_venture_array)->addColumn('action', function ($venture) {
                $show = '/show-mentor-venture-content/' . $venture->id;
                return '<a href=' . $show . ' title="View Venture and Mentor Content" style="color:blue!important;"><i class="material-icons">visibility</i></a>';
            })
                ->make(true);
        }elseif  ($user->roles[0]->name == 'mentor') {
            return Datatables::of($assigned_venture_array)->addColumn('action', function ($venture) {
                    $form = '/mentorForm/' .$venture->id;
                return '<a href=' . $form . ' title="Show Mentor form" style="color:yellow!important;"><i class="material-icons">visibility</i></a>';
            })
                ->make(true);
        } else {
            return response("You are not authorized!!");
        }
    }

    public function showMentorVentureContent(Venture $venture){
        $venture->load('mentor', 'questionAnswers', 'ventureShadow');
        $venture_mentor = $venture->mentor()->first();
        $venture_mentor->load('user');
        $mentor_user = $venture_mentor->user;
        $venture_shadowboards = [];
        $logged_in_user = Auth::user()->load('roles');

        foreach($venture->ventureShadow->sortByDesc('shadow_board_date') as $v_shadow){
            $mentor_id = $v_shadow->mentor_id;
            $mentor = Mentor::find($mentor_id);
            $mentor->load('user');

            $object = (object)['mentor_name' => $mentor->user->name, 'mentor_surname' => $mentor->user->surname,
                'shadow_board_date' => $v_shadow->shadow_board_date, 'shadow_board_time' => $v_shadow->shadow_board_time, 'mentor_venture_shadowboard_id' => $v_shadow->id];
            array_push($venture_shadowboards, $object);
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/mentors/admin-mentors/show-mentor-venture-content', compact('venture', 'mentor_user', 'venture_shadowboards'));
        } else if($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/mentors/show-mentor-venture-content',compact('venture','mentor_user','venture_shadowboards'));
        } else if($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/mentors/show-mentor-venture-content', compact('venture_shadowboards', 'mentor_user', 'venture'));
        } else if($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/mentors/show-mentor-venture-content', compact('venture_shadowboards', 'mentor_user', 'venture'));
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function adminViewMentorVentureShadowBoardResults(MentorVentureShadowboard $mentorVentureShadowboard){
        $mentorVentureShadowboard->load('ventureAnswers', 'mentorAnswers', 'venture', 'mentor','mentorComments');
        $mentorComment = $mentorVentureShadowboard->mentorComments;
        $ventureShadowboardAnswers = [];
        $mentorShadowboardAnswers = [];
        $venture = $mentorVentureShadowboard->venture;
        $mentor = $mentorVentureShadowboard->mentor;
        $mentor->load('user');
        $mentor_user = $mentor->user;
        $logged_in_user = Auth::user()->load('roles');

        if(count($mentorVentureShadowboard->mentorAnswers) > 0){
            foreach($mentorVentureShadowboard->mentorAnswers as $m_qa){
                $shadowboardQuestionId = $m_qa->shadow_board_question_id;
                $question = ShadowBoardQuestion::find($shadowboardQuestionId);
                $question->load('shadowBoardQuestionSubQuestionText');

                $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                    'answer_text' => $m_qa->answer_text];
                array_push($mentorShadowboardAnswers, $object);
            }
        }

        if(count($mentorVentureShadowboard->ventureAnswers) > 0){
            foreach($mentorVentureShadowboard->ventureAnswers as $v_qa){
                $shadowboardQuestionId = $v_qa->shadow_board_question_id;
                $question = ShadowBoardQuestion::find($shadowboardQuestionId);
                $question->load('shadowBoardQuestionSubQuestionText');

                $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                    'answer_text' => $v_qa->answer_text];
                array_push($ventureShadowboardAnswers, $object);
            }
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/mentors/admin-mentors/show-mentor-venture-shadowboard-results', compact('mentorVentureShadowboard',
                'mentorShadowboardAnswers', 'ventureShadowboardAnswers', 'mentor_user', 'venture','mentorComment'));
        } else if($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/mentors/show-mentor-venture-shadowboard-results', compact('mentorVentureShadowboard',
                'mentorShadowboardAnswers', 'ventureShadowboardAnswers', 'mentor_user', 'venture','mentorComment'));
        } else if($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/mentors/show-mentor-venture-shadowboard-results', compact('mentorVentureShadowboard',
                'mentorShadowboardAnswers', 'ventureShadowboardAnswers', 'mentor_user', 'venture','mentorComment'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    /////////////////////////////////////////////////***VENTURE AND MENTOR*****///////////////////////////////////////////



    //Show mentor form the correct one
    public function showFormMentor(Venture $venture){
        $user = Auth::user()->load('roles');
        $user->load('mentor');
        $user_mentor = $user->mentor;
        $user_mentor->load('venture');

        $answered_questions = ShadowBoardQuestionAnswer::where('mentor_id', $user_mentor->id)
            ->where('venture_id', $venture->id)->get();
        $question_array = [];

        if (count($answered_questions) > 0) {
            $g_question_category = "";
            $g_questions = "";

            foreach ($answered_questions as $item) {
                $question_id = $item->shadow_board_question_id;
                $question = ShadowBoardQuestion::find($question_id);
                $answer_text = $item->answer_text;
                $object = (object)['question_number' => $question->question_number,'question_type'=> $question->question_type,
                    'question_text' => $question->question_text, 'answer_text' => $answer_text,
                    'question_id' => $question_id];
                array_push($question_array, $object);
            }

        } else {
            $question_category = ShadowBoardQuestionCategory::where('category_name', 'mentor')->firstOrFail();
            $question_category->load('shadowBoardQuestion');
            $question = $question_category->shadowBoardQuestion->sortBy('question_number');
            $g_question_category = $question_category;
            $g_questions = $question;
            $g_questions->load('shadowBoardQuestionSubQuestionText');
        }

        if ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.mentors.admin-mentors.show',compact('venture','user','user_mentor','question_category','question','g_questions','answered_questions','question_array'));
        }
    }

    public function showVentureForm(Venture $venture){
        $user = Auth::user()->load('roles');
        $venture->load('mentor','incubatee');
        $user_mentor = $venture->mentor;


        $question_category = ShadowBoardQuestionCategory::where('category_name', 'venture')->firstOrFail();
        $question_category->load('shadowBoardQuestion');
        $question = $question_category->shadowBoardQuestion->sortBy('question_number');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.venture.show-venture-form',compact('question','venture','user_mentor'));
        }elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.mentors.show-venture-form',compact('question','venture'));
        }elseif ($user->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.venture.show-venture-form',compact('question','venture'));
        }
    }

    //Function to store Questions and Answers
    public function storeVentureShadowboardAnswers(Request $request)
    {
        $input = $request->all();

        try {
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $mentorVentureShadowboard_id = $input['mentorVentureShadowboard'];
            $mentorVentureShadowboard = MentorVentureShadowboard::find($mentorVentureShadowboard_id);
            $venture_id = $questions_answers[0]->venture_id;
            $venture = Venture::find($venture_id);
            $venture->load('questionAnswers', 'mentor','ventureShadow');



            if (count($venture->questionAnswers) > 0) {
                foreach ($questions_answers as $question_answer) {
                    $shadow_board_question_id = $question_answer->shadow_board_question_id;
                    $testing = DB::table('venture_question_answers')->where('shadow_board_question_id', $shadow_board_question_id)
                        ->where('venture_id', $venture->id)->where('mentor_venture_id', $mentorVentureShadowboard->id)->first();

                    if (isset($testing)) {
                        DB::table('venture_question_answers')->where('shadow_board_question_id', $shadow_board_question_id)
                            ->where('venture_id', $venture_id)->where('mentor_venture_id', $mentorVentureShadowboard->id)
                            ->update(['answer_text' => $question_answer->answer_text]);
                    } else {
                        $create_question_answer = VentureQuestionAnswer::create(['venture_id' => $venture_id, 'shadow_board_question_id' => $question_answer->shadow_board_question_id,
                            'answer_text' => $question_answer->answer_text, 'mentor_venture_id' => $mentorVentureShadowboard->id]);
                    }
                }
            } else {
                foreach ($questions_answers as $question_answer) {
                    $create_question_answer = VentureQuestionAnswer::create(['venture_id' => $venture_id, 'shadow_board_question_id' => $question_answer->shadow_board_question_id,
                        'answer_text' => $question_answer->answer_text, 'mentor_venture_id' => $mentorVentureShadowboard->id]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Answers Saved successfully.', 'venture' => $venture], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Function to store Questions and Answers
    public function storeMentorShadowBoardAnswers(Request $request)
    {
        $input = $request->all();

        try {
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $mentor_id = $questions_answers[0]->mentor_id;
            $venture_id = $questions_answers[0]->venture_id;
            $mentorVentureShadowboard_id = $input['mentorVentureShadowboard'];
            $mentorVentureShadowboard = MentorVentureShadowboard::find($mentorVentureShadowboard_id);
            $mentor = Mentor::find($mentor_id);
            $mentor->load('questionAnswers', 'venture','user');

            if (count($mentor->questionAnswers) > 0) {
                foreach ($questions_answers as $question_answer) {
                    $shadow_board_question_id = $question_answer->shadow_board_question_id;
                    $testing = DB::table('shadow_board_question_answers')->where('shadow_board_question_id', $shadow_board_question_id)
                        ->where('mentor_id', $mentor->id)->where('mentor_venture_id', $mentorVentureShadowboard->id)->first();

                    if (isset($testing)) {
                        DB::table('shadow_board_question_answers')->where('shadow_board_question_id', $shadow_board_question_id)
                            ->where('mentor_id', $mentor->id)->where('mentor_venture_id', $mentorVentureShadowboard->id)
                            ->update(['answer_text' => $question_answer->answer_text]);
                    } else {
                        $create_question_answer = ShadowBoardQuestionAnswer::create(['mentor_id' => $mentor->id, 'shadow_board_question_id' => $question_answer->shadow_board_question_id,
                            'answer_text' => $question_answer->answer_text, 'mentor_venture_id' => $mentorVentureShadowboard->id]);
                    }
                }
            } else {
                foreach ($questions_answers as $question_answer) {
                    $create_question_answer = ShadowBoardQuestionAnswer::create(['mentor_id' => $mentor->id, 'shadow_board_question_id' => $question_answer->shadow_board_question_id,
                        'answer_text' => $question_answer->answer_text, 'mentor_venture_id' => $mentorVentureShadowboard->id]);
                }
            }
            DB::commit();
            return response()->json(['message' => 'Answers Saved successfully.', 'mentor' => $mentor], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    // REFERRED
    public function referredApplicantCategory(){
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/referred-applicant-category');
        }else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/referred-applicant-category');
        }else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/referred-applicant-category');
        }
    }

    public function getReferredApplicantCategories(){
        $logged_in_user = Auth::user()->load('roles');
        $categories = ReferredApplicantCategory::all();
        $categories_array = [];
        foreach ($categories as $category){
            $category->load('referred_applicants');
            $applicant_count = 0;
            if (count($category->referred_applicants) > 0){
                $applicant_count = count($category->referred_applicants);
            }

            $object = (object)['category_name' => $category->category_name, 'applicant_count' => $applicant_count,
                'id' => $category->id];
            array_push($categories_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'advisor'
            or $logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($categories_array)->addColumn('action', function ($categories) {
                $sh = '/referred-applicant-category-edit/' .$categories->id;
                $del = '/delete-referred-applicant/' .$categories->id;
                return '<a href=' . $sh . ' title="Edit Category"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete_users(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })


                ->make(true);
        } elseif ($logged_in_user->roles[0]->name == 'marketing'){
            return Datatables::of($categories_array)
//                ->addColumn('action', function ($applicant) {
//                $sh = '/applicant-show/' . $applicant->id;
//                return '<a href=' . $sh . ' title="View Applicant"><i class="material-icons">remove_red_eye</i></a>';
//            })
                ->make(true);
        } else {
            return response('You are not authorized!');
        }
    }

    public function destroyReferredApplicantCategory($id){

        try{
            DB::beginTransaction();
            $referredApplicantCategory = ReferredApplicantCategory::find($id);
            $referredApplicantCategory->delete();
            DB::commit();
            return response()->json(['message' => 'Deleted .']);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }
    //==================================================================//
    //Applicants section

    public function declinedApplicantCategoryIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/declined-applicant-categories-index');
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/declined-applicant-categories-index');
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/declined-applicant-categories-index');
        } elseif($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/applicants/declined-applicant-categories-index');
        } else {
            return response("You are not authorized!");
        }
    }
    //Referred applicant category edit
    public function referredApplicantCategoryEdit(ReferredApplicantCategory $referredApplicantCategory){
        $logged_in_user = Auth::user()->load('roles');
        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/referred-applicant-category-edit',compact('referredApplicantCategory'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/referred-applicant-category-edit',compact('referredApplicantCategory'));
        }
    }

    //Declined applicnt category edit
    public function declinedApplicantCategoryEdit(DeclinedApplicantCategory $declinedApplicantCategory){
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/declined-applicant-category-edit',compact('declinedApplicantCategory'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/declined-applicant-category-edit',compact('declinedApplicantCategory'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/declined-applicant-category-edit',compact('declinedApplicantCategory'));
        }  else {
            return response("You are not authorized!");
        }
    }

    public function updateReferredApplicantCategory(Request $request, ReferredApplicantCategory $referredApplicantCategory){
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'administrator') {
            try {
                $referredApplicantCategory->update(['category_name' => $input['category_name']]);

                DB::commit();
                return response()->json(['message' => 'Category updated successfully.', 'referredApplicantCategory' => $referredApplicantCategory], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Category could not be saved at the moment ' . $e->getMessage(),'referredApplicantCategory'=>$referredApplicantCategory], 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Update Declined applicant category
    public function updateDeclineApplicabrCategory(Request $request, DeclinedApplicantCategory $declinedApplicantCategory)
    {
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'administrator') {
            try {
                $declinedApplicantCategory->update(['category_name' => $input['category_name']]);

                DB::commit();
                return response()->json(['message' => 'Category updated successfully.', 'declinedApplicantCategory' => $declinedApplicantCategory], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Category could not be saved at the moment ' . $e->getMessage(),'declinedApplicantCategory'=>$declinedApplicantCategory], 400);
            }
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getDeclinedApplicantCategories(){
        $logged_in_user = Auth::user()->load('roles');
        $categories = DeclinedApplicantCategory::all();
        $categories_array = [];
        foreach ($categories as $category){
            $category->load('declined_applicants');
            $applicant_count = 0;
            if (count($category->declined_applicants) > 0){
                $applicant_count = count($category->declined_applicants);
            }

            $object = (object)['category_name' => $category->category_name, 'applicant_count' => $applicant_count,
                'id' => $category->id];
            array_push($categories_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'advisor'
            or $logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($categories_array)->addColumn('action', function ($categories) {
                $sh = '/declined-applicant-category-edit/' .$categories->id;
                $del = '/delete-declined-applicant-category/' .$categories->id;
                return '<a href=' . $sh . ' title="Edit Category"><i class="material-icons">create</i></a>
                <a href=' . $del . ' onclick="confirm_delete_users(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($logged_in_user->roles[0]->name == 'marketing'){
            return Datatables::of($categories_array)
                ->make(true);
        } else {
            return response('You are not authorized!');
        }
    }
    public function destroyDeclinedApplicantCategory($id){

        try{
            DB::beginTransaction();
            $declinedApplicantCategory = DeclinedApplicantCategory::find($id);
            $declinedApplicantCategory->delete();
            DB::commit();
            return response()->json(['message' => 'Deleted.']);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }


    public function applicantsIndex(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/applicant-index');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/applicant-index');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/applicant-index');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('/users/mentor-dashboard/applicants/applicant-index');
        } elseif($user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/applicants/applicant-index');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function getApplicants(){
        $logged_in_user = Auth::user()->load('roles');
        $applicant_array = [];
        $applicants = Applicant::with('user')
            ->where('is_bootcamper', '=', false)
            ->where('is_incubatee', '=', false)
            ->get();


        foreach($applicants as $applicant){
            if(isset($applicant->user)){
                $object = (object)[
                    'name' => $applicant->user->name,
                    'surname' => $applicant->user->surname,
                    'email' => $applicant->user->email,
                    'phone' => $applicant->user->contact_number,
                    'category' => isset($applicant->chosen_category) ? $applicant->chosen_category : 'Not chosen',
                    'date_time_applied' => $applicant->user->created_at->toDateString(),
                    'completed_form' => $applicant->application_completed ? 'Yes' : 'No',
                    'id' => $applicant->user->id,
                    'status'=> isset($applicant->status) ? $applicant->status : 'Pending',
                    'popi_act_agreement'=> isset($applicant->popi_act_agreement) ? $applicant->popi_act_agreement : 'No',
                    'data_protected'=> isset($applicant->data_protected) ? $applicant->data_protected : 'No'
                ];
                array_push($applicant_array, $object);
            }
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'advisor'
            or $logged_in_user->roles[0]->name == 'administrator'
            or $logged_in_user->roles[0]->name == 'marketing') {
            return Datatables::of($applicant_array)->addColumn('action', function ($applicant) {
                $sh = '/applicant-show/' . $applicant->id;
               /* $paw = '/applicant-panel-access-window/' . $applicant->id;
                $apw = '/show-applicant-panelists/' . $applicant->id;
                $nsb = '/applicant-next-steps/' . $applicant->id;*/
                $del = '/delete-applicants/'. $applicant->id;
                return '<a href=' . $sh . ' title="View Applicant"><i class="material-icons">remove_red_eye</i></a>
           <a href=' . $del . ' title="Delete Applicant" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } else {
            return response('You are not authorized!');
        }
    }

    //DELETE APPLICANTS
    public function deleteApplicants($id){
        DB::beginTransaction();
        $user = User::find($id);
        $user->load('applicant','userQuestionAnswers');

        try {
            if(isset($user->applicant)){
                $user->applicant()->forceDelete();
            }
            if(isset($user->userQuestionAnswers)){
                $user->userQuestionAnswers()->forceDelete();
            }

             $user->forceDelete();

            DB::commit();
            return response()->json(['message' => 'Applicant deleted successfully'], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    //Referred applicants
    public function referredApplicantsIndex(){
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/referred-applicants-index');
        }elseif ($user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/referred-applicants-index');
        }elseif ($user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/referred-applicants-index');
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function declinedApplicantsIndex(){
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/declined-applicants-index');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/declined-applicants-index');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/declined-applicants-index');
        } elseif ($user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/applicants/declined-applicants-index');
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //PANEL DECLINED APPLICANTS
    public function panelDeclinedApplicants(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/panel-declined-applicants');
        }
    }

    //GET PANEL DECLINED APPLICANTS
    public function getPanelDeclinedApplicants(){
        $logged_in_user = Auth::user()->load('roles');
        $applicants = DeclinedApplicant::with('declined_applicant_category')->where('result_of_contact','Panel decision declined')->get();
        $declined_applicants_array = [];

        foreach($applicants as $declined_applicants){
            $declined_applicants->load('declined_applicant_category');
            $declined_category = "None";

            if($declined_applicants->declined_applicant_category != null){
                $declined_category = $declined_applicants->declined_applicant_category->category_name;
            }

            $object = (object)['name' => $declined_applicants->name, 'surname' => $declined_applicants->surname,
                'email' => $declined_applicants->email, 'contact_number' => $declined_applicants->contact_number,
                'id' => $declined_applicants->id,
                'date_declined' => $declined_applicants->created_at->toDateString(),
                'declined_reason'=>$declined_applicants->declined_reason,
                'referred_company'=>$declined_applicants->referred_company,
                'contacted_via'=>$declined_applicants->contacted_via,
                'category'=> $declined_category];

            array_push($declined_applicants_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($declined_applicants_array)->addColumn('action', function($applicant){
                $sh = '/show-declined-applicant-application/' . $applicant->id;
                $edit= '/declined-applicant-info/' . $applicant->id;
                $delete = $applicant->id;
                return '<a href=' . $sh . ' title="Show Application" style="color:blue!important;"><i class="material-icons">visibility</i></a><a href=' . $edit . ' title="Edit Declined Applicant Info" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $delete . ' onclick="confirm_delete_declined_applicant(this)" title="Delete Applicant"><i class="material-icons" style="color:red">delete_forever</i></a>';

            })->make(true);
        }
    }

    //PANEL REFERRED APPLICANTS
    public function panelReferredApplicants(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/panel-referred-applicants');
        }
    }

    //GET PANEL REFERRED APPLICANTS
    public function getPanelReferredApplicants(){
        $logged_in_user = Auth::user()->load('roles');
        $referred = PropellaReferredApplicant::with('referred_applicant_category')->where('result_of_contact','Panel decision referred')->get();
        $referred_applicant_array = [];

        foreach($referred as $referred_applicants){
            $referred_category = 'None';
            if($referred_applicants->referred_applicant_category != null){
                $referred_category = $referred_applicants->referred_applicant_category->category_name;
            }
            $object = (object)['name' => $referred_applicants->name, 'surname' => $referred_applicants->surname,
                'email' => $referred_applicants->email, 'contact_number' => $referred_applicants->contact_number,
                'id' => $referred_applicants->id,
                'created_at' => $referred_applicants->created_at->toDateString(),
                'referred_reason'=>$referred_applicants->referred_reason,
                'referred_company'=>$referred_applicants->referred_company,
                'contacted_via'=>$referred_applicants->contacted_via,
                'category'=> $referred_category];

            array_push($referred_applicant_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return Datatables::of($referred_applicant_array)->addColumn('action', function($applicant){
                $sh = '/show-referred-applicant/' . $applicant->id;
                $edit= '/referred-applicant-info/' . $applicant->id;
                $delete = $applicant->id;
                return '<a href=' . $sh . ' title="Show Application" style="color:blue!important;"><i class="material-icons">visibility</i></a><a href=' . $edit . ' title="Edit Declined Applicant Info" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $delete . ' onclick="confirm_delete_declined_applicant(this)" title="Delete Applicant"><i class="material-icons" style="color:red">delete_forever</i></a>';

            })->make(true);
        } else {
            return response('You are not authorized!');
        }
    }

    //REFERRED APPLICANTS
    public function getReferredApplicants(){
        $logged_in_user = Auth::user()->load('roles');
        $referred = PropellaReferredApplicant::all();
        $referred_applicant_array = [];

        foreach($referred as $referred_applicants){
            $referred_applicants->load('referred_applicant_category');
            $referred_category = 'None';
            if($referred_applicants->referred_applicant_category != null){
                $referred_category = $referred_applicants->referred_applicant_category->category_name;
            }
            $object = (object)['name' => $referred_applicants->name, 'surname' => $referred_applicants->surname,
                'email' => $referred_applicants->email, 'contact_number' => $referred_applicants->contact_number,
                'id' => $referred_applicants->id,
                'created_at' => $referred_applicants->created_at->toDateString(),
                'referred_reason'=>$referred_applicants->referred_reason,
                'referred_company'=>$referred_applicants->referred_company,
                'contacted_via'=>$referred_applicants->contacted_via,
                'category'=> $referred_category];

            array_push($referred_applicant_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'administrator'
            or $logged_in_user->roles[0]->name == 'advisor') {
            return Datatables::of($referred_applicant_array)->addColumn('action', function($applicant){
                $sh = '/show-referred-applicant/' . $applicant->id;
                $edit= '/referred-applicant-info/' . $applicant->id;
                $delete = $applicant->id;
                return '<a href=' . $sh . ' title="Show Application" style="color:blue!important;"><i class="material-icons">visibility</i></a><a href=' . $edit . ' title="Edit Declined Applicant Info" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $delete . ' onclick="confirm_delete_declined_applicant(this)" title="Delete Applicant"><i class="material-icons" style="color:red">delete_forever</i></a>';
            })->make(true);
        } else {
            return response('You are not authorized!');
        }
    }

    public function getDelinedApplicants(){
        $logged_in_user = Auth::user()->load('roles');
        $applicants = DeclinedApplicant::all();
        $applicants->load('declined_applicant_category');
        $declined_applicants_array = [];

        foreach($applicants as $declined_applicants){
            $declined_applicants->load('declined_applicant_category');
            $declined_category = "None";

            if($declined_applicants->declined_applicant_category != null){
                $declined_category = $declined_applicants->declined_applicant_category->category_name;
            }

            $object = (object)['name' => $declined_applicants->name, 'surname' => $declined_applicants->surname,
                'email' => $declined_applicants->email, 'contact_number' => $declined_applicants->contact_number,
                'id' => $declined_applicants->id,
                'date_declined' => $declined_applicants->created_at->toDateString(),
                'declined_reason'=>$declined_applicants->declined_reason,
                'referred_company'=>$declined_applicants->referred_company,
                'contacted_via'=>$declined_applicants->contacted_via,
                'category'=> $declined_category];

            array_push($declined_applicants_array, $object);
        }

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'administrator') {
            return Datatables::of($declined_applicants_array)->addColumn('action', function($applicant){
                $acc = '/declined-applicant-account-overview/' . $applicant->id;
                $edit= '/declined-applicant-info/' . $applicant->id;
                $delete = $applicant->id;
                return '<a href=' . $acc . ' title="Account Overview" style="color:green!important;"><i class="material-icons" style="color: green;">visibility</i></a> <a href=' . $edit . ' title="Edit Declined Applicant Info" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $delete . ' onclick="confirm_delete_declined_applicant(this)" title="Delete Applicant"><i class="material-icons" style="color:red">delete_forever</i></a>';
            })->make(true);
        } elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return Datatables::of($declined_applicants_array)->addColumn('action', function($applicant){
                $acc = '/declined-applicant-account-overview/' . $applicant->id;
                $edit= '/declined-applicant-info/' . $applicant->id;
                return '<a href=' . $acc . ' title="Account Overview" style="color:green!important;"><i class="material-icons" style="color: green;">visibility</i></a><a href=' . $edit . ' title="Edit Declined Applicant Info" style="color:green!important;"><i class="material-icons">create</i></a>';
            })->make(true);
        } elseif ($logged_in_user->roles[0]->name == 'marketing'){
            return Datatables::of($declined_applicants_array)->addColumn('action', function($applicant){
                $acc = '/declined-applicant-account-overview/' . $applicant->id;
                $edit= '/declined-applicant-info/' . $applicant->id;
                return '<a href=' . $acc . ' title="Account Overview" style="color:green!important;"><i class="material-icons" style="color: green;">visibility</i></a><a href=' . $edit . ' title="Edit Declined Applicant Info" style="color:green!important;"><i class="material-icons">create</i></a>';
            })->make(true);
        }elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return Datatables::of($declined_applicants_array)->addColumn('action', function($applicant){
                $acc = '/declined-applicant-account-overview/' . $applicant->id;
                $edit= '/declined-applicant-info/' . $applicant->id;
                return '<a href=' . $acc . ' title="Account Overview" style="color:green!important;"><i class="material-icons" style="color: green;">visibility</i></a><a href=' . $edit . ' title="Edit Declined Applicant Info" style="color:green!important;"><i class="material-icons">create</i></a>';
            })->make(true);
        }
        else {
            return response('You are not authorized!');
        }
    }

    public function adminCreateReferredApplicantCategory(Request $request){
        $input = $request->all();
        $category_name = $input['category_name'];
        try{
            DB::beginTransaction();

            $create_referred_applicant_category = ReferredApplicantCategory::create(['category_name' => $category_name]);
            DB::commit();
            return response()->json(['message' => 'Category created.'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Get Declined Applicant account Overview
    public function adminViewDeclinedApplicantAccountOverview(DeclinedApplicant $declinedApplicant)
    {
        $logged_in_user = Auth::user()->load('roles');
        $declinedApplicant->load('question_answers', 'declined_applicant_category','contactLog');
        $declinedApplicantQuestionAnswers = [];

        foreach ($declinedApplicant->question_answers as $userQuestionAnswer) {
            $question_id = $userQuestionAnswer->question_id;
            $question = Question::find($question_id);
            $answer_text = $userQuestionAnswer->answer_text;
            $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                'answer_text' => $answer_text];

            array_push($declinedApplicantQuestionAnswers, $object);
        }
        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.app-admin.applicants.declined-applicant-account-overview',compact('declinedApplicant','declinedApplicantQuestionAnswers'));
        }elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrator.applicants.declined-applicant-account-overview',compact('declinedApplicant','declinedApplicantQuestionAnswers'));
        }elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.applicants.declined-applicant-account-overview',compact('declinedApplicant','declinedApplicantQuestionAnswers'));
        }elseif ($logged_in_user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.applicants.declined-applicant-account-overview',compact('declinedApplicant','declinedApplicantQuestionAnswers'));
        }
    }

    public function editDeclinedApplicantBasic(DeclinedApplicant $declinedApplicant){
        $category = DeclinedApplicantCategory::orderBy('category_name', 'asc')->get();
        $logged_in_user = Auth::user()->load('roles');
        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.declined-applicants.edit-declined-applicants-info',compact('declinedApplicant','category'));
        }elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.declined-applicants.edit-declined-applicants-info',compact('declinedApplicant'));
        }elseif ($logged_in_user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.declined-applicants.edit-declined-applicants-info',compact('declinedApplicant'));
        }elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.declined-applicants.edit-declined-applicants-info',compact('declinedApplicant'));
        }
        else {
            return response("You are not authorized!");
        }
    }

    //Edit referred Info
    public function editReferredpplicantBasic(PropellaReferredApplicant $propellaReferredApplicant){
        $referred_category = ReferredApplicantCategory::orderBy('category_name', 'asc')->get();
        $logged_in_user = Auth::user()->load('roles');
        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.app-admin.applicants.edit-referred-applicant-info',compact('propellaReferredApplicant','referred_category'));
        } else {
            return response("You are not authorized!");
        }
    }
    //Update Referred
    public function referredApplicantUpdateBasic(Request $request, PropellaReferredApplicant $propellaReferredApplicant){
        $input = $request->all();
        $propellaReferredApplicant->load('referred_applicant_category');
        try {
            DB::beginTransaction();
            $propellaReferredApplicant->update(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'],'race'=> $input['race'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'],
                'gender' => $input['gender'],'referred_reason'=>$input['referred_reason'],
                'contacted_via'=>$input['contacted_via'],'referred_company'=> $input['referred_company'],
                'referred_applicant_category_id'=>$input['referred_applicant_category_id']]);
            DB::commit();
            return response()->json(['message' => 'Your details have successfully been updated ']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    public function declinedApplicantUpdateBasic(Request $request, DeclinedApplicant $declinedApplicant){
        $input = $request->all();
        $declinedApplicant->load('declined_applicant_category');
        try {
            DB::beginTransaction();
            $declinedApplicant->update(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'],'race'=> $input['race'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'],
                'gender' => $input['gender'],'declined_reason'=>$input['declined_reason'],
                'contacted_via'=>$input['contacted_via'],
                'declined_applicant_category_id'=>$input['declined_applicant_category_id'],'referred_company'=>$input['referred_company']]);


            DB::commit();
            return response()->json(['message' => 'Your details have successfully been updated ']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Show Referred Applicants
    public function showReferredApplicantApplication(PropellaReferredApplicant $propellaReferredApplicant){
        $logged_in_user = Auth::user()->load('roles');
        $propellaReferredApplicant->load('question_answers');
        $r_a_QuestionAnswersArray = [];

        foreach($propellaReferredApplicant->question_answers as $r_a_QuestionAnswers){
            $question_id = $r_a_QuestionAnswers->question_id;
            $question = Question::find($question_id);
            $answer_text = $r_a_QuestionAnswers->answer_text;
            $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                'answer_text' => $answer_text];

            array_push($r_a_QuestionAnswersArray, $object);
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/applicants/referred-applicant-application', compact('r_a_QuestionAnswersArray', 'propellaReferredApplicant'));
        } else {
            return response("You are not authorized!!");
        }
    }

    public function showDeclinedApplicantApplication(DeclinedApplicant $declinedApplicant){
        $logged_in_user = Auth::user()->load('roles');
        $declinedApplicant->load('question_answers');
        $d_a_QuestionAnswersArray = [];

        foreach($declinedApplicant->question_answers as $d_a_QuestionAnswers){
            $question_id = $d_a_QuestionAnswers->question_id;
            $question = Question::find($question_id);
            $answer_text = $d_a_QuestionAnswers->answer_text;
            $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                'answer_text' => $answer_text];

            array_push($d_a_QuestionAnswersArray, $object);
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/applicants/declined-applicant-application', compact('d_a_QuestionAnswersArray', 'declinedApplicant'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/applicants/declined-applicant-application', compact('d_a_QuestionAnswersArray', 'declinedApplicant'));
        } else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/applicants/declined-applicant-application', compact('d_a_QuestionAnswersArray', 'declinedApplicant'));
        } else if ($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/applicants/declined-applicant-application', compact('d_a_QuestionAnswersArray', 'declinedApplicant'));
        } else {
            return response("You are not authorized!!");
        }
    }

    public function adminDeleteDeclinedApplicant(DeclinedApplicant $declinedApplicant){
        try{
            $declinedApplicant->load('question_answers');
            DB::beginTransaction();
            if(count($declinedApplicant->question_answers) > 0){
                foreach ($declinedApplicant->question_answers as $d_qa){
                    $d_qa->forceDelete();
                }
            }

            $declinedApplicant->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Applicant deleted.'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    public function adminCreateDeclinedApplicantCategory(Request $request){
        $input = $request->all();
        $category_name = $input['category_name'];
        try{
            DB::beginTransaction();

            $create_declined_applicant_category = DeclinedApplicantCategory::create(['category_name' => $category_name]);
            DB::commit();
            return response()->json(['message' => 'Category created.'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //End of declined applicants
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Applicant Panel Access Window
    public function showApplicantPanelAccessWindow(User $user)
    {
        $logged_in_user = Auth::user()->load('roles');

        $user->load('applicant');
        $applicant = $user->applicant;
        $applicant->load('panelists');
        $applicant_panelists = [];
        $question_categories = QuestionsCategory::all();
        $declined_applicant_categories = DeclinedApplicantCategory::all();

        if(count($applicant->panelists) > 0){
            foreach($applicant->panelists as $applicant_panelist){
                $applicant_panelist->load('panelist');
                $applicant_panelist_question_category_id = $applicant_panelist->question_category_id;
                $applicant_panelist_question_category = QuestionsCategory::find($applicant_panelist_question_category_id);
                $panelist = $applicant_panelist->panelist;
                $panelist->load('user');
                $panelist_user = $panelist->user;



                $object = (object)['name' => $panelist_user->name, 'surname' => $panelist_user->surname,
                    'title' => $panelist_user->title, 'initials' => $panelist_user->initials,
                    'company_name' => $panelist_user->company_name, 'position' => $panelist_user->position,
                    'email' => $panelist_user->email, 'contact_number' => $panelist_user->contact_number,
                    'applicant_panelist_id' => $applicant_panelist->id, 'question_category_id' => $applicant_panelist_question_category->id,
                    'question_category' => $applicant_panelist_question_category->category_name];


                array_push($applicant_panelists, $object);

            }
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/applicant-panel-access-window', compact('user', 'applicant', 'applicant_panelists', 'question_categories', 'declined_applicant_categories'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/applicant-panel-access-window', compact('user','applicant', 'applicant_panelists', 'question_categories', 'declined_applicant_categories'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/applicants/applicant-panel-access-window', compact('user','applicant', 'applicant_panelists', 'question_categories', 'declined_applicant_categories'));
        } else if ($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/applicants/applicant-panel-access-window', compact('user','applicant', 'applicant_panelists', 'question_categories', 'declined_applicant_categories'));
        }
        else {
            return response("You are not authorized!");
        }
    }

    //Admin approve applicant panel interview
    public function adminApproveApplicantPanelInterview(Request $request, User $user)
    {
        $input = $request->all();
        $selected_panelists = json_decode($input['selected_panelists']);
        $guest_panelists = json_decode($input['guest_panelists']);
        $panelist_role = Role::where('name', 'panelist')->first();
        $question_category_id = $input['question_category_id'];

        try {
            DB::beginTransaction();

            $user->applicant()->update(['contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact']]);

            foreach ($selected_panelists as $selected_panelist) {
                //See if the actual user exists
                $check_user = User::where('email', $selected_panelist)->first();

                //if user does exist
                if (isset($check_user)) {
                    //load relationships to be used
                    $check_user->load('roles');
                    //boolean to see if the user has panelist role
                    $check_role = false;

                    //check if user has role panelist
                    foreach ($check_user->roles as $user_role) {
                        if ($user_role->name == 'panelist') {
                            $check_role = true;
                        }
                    }

                    //if user has role panelist, do nothing, else attach role
                    if ($check_role == false) {
                        $check_user->roles()->attach($panelist_role->id);
                    }

                    //Variable to see if the panelist object with the same user id already exists
                    $check_panelist = $check_user->panelist()->exists();;

                    //Do if statement to see if the panelist with user id exists, else create
                    if ($check_panelist == true) {
                        $cur_panelist = $check_user->panelist;
                        $check_panelist_applicants = $cur_panelist->applicants()->exists();

                        if ($check_panelist_applicants == true) {
                            $cur_panelist_applicants = $cur_panelist->applicants;

                            //boolean to see if panelist has applicant
                            $has_applicant = false;

                            //loop through panelist applicants to see if the applicant id matches the current applicant id
                            foreach ($cur_panelist_applicants as $panelist_applicant) {
                                if ($panelist_applicant->applicant_id == $user->applicant->id) {
                                    $has_applicant = true;
                                }
                            }

                            //if panelist does not have current applicant, create applicantpanelist object
                            if ($has_applicant == false) {
                                $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            } else {
                                $get_object = DB::table('applicant_panelists')->where('applicant_id', $user->applicant->id)
                                    ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                        'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            }
                        } else {
                            $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $cur_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                        }
                    } else {
                        $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                        $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                    }
                } else {
                    $create_user = User::create(['email' => $selected_panelist, 'password' => Hash::make('secret')]);
                    $create_user->roles()->attach($panelist_role->id);
                    $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                    $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $create_panelist->id,
                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                }
            }

            foreach ($guest_panelists as $guest_panelist) {
                $check_user = User::where('email', $guest_panelist->email)->first();

                //if user does exist
                if (isset($check_user)) {
                    //load relationships to be used
                    $check_user->load('roles');
                    //boolean to see if the user has panelist role
                    $check_role = false;

                    //check if user has role panelist
                    foreach ($check_user->roles as $user_role) {
                        if ($user_role->name == 'panelist') {
                            $check_role = true;
                        }
                    }

                    //if user has role panelist, do nothing, else attach role
                    if ($check_role == false) {
                        $check_user->roles()->attach($panelist_role->id);
                    }

                    //Variable to see if the panelist object with the same user id already exists
                    $check_panelist = $check_user->panelist()->exists();;

                    //Do if statement to see if the panelist with user id exists, else create
                    if ($check_panelist == true) {
                        $cur_panelist = $check_user->panelist;
                        $check_panelist_applicants = $cur_panelist->applicants()->exists();

                        if ($check_panelist_applicants == true) {
                            $cur_panelist_applicants = $cur_panelist->applicants;
                            //boolean to see if panelist has applicant
                            $has_applicant = false;

                            //loop through panelist applicants to see if the applicant id matches the current applicant id
                            foreach ($cur_panelist_applicants as $panelist_applicant) {
                                if ($panelist_applicant->applicant_id == $user->applicant->id) {
                                    $has_applicant = true;
                                }
                            }

                            //if panelist does not have current applicant, create applicantpanelist object
                            if ($has_applicant == false) {
                                $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            } else {
                                $get_object = DB::table('applicant_panelists')->where('applicant_id', $user->applicant->id)
                                    ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                        'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            }
                        } else {
                            $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $cur_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                        }
                    } else {
                        $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                        $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                    }
                } else {
                    $create_user = User::create(['name' => $guest_panelist->name, 'surname' => $guest_panelist->surname,
                        'contact_number' => $guest_panelist->contact_number, 'email' => $guest_panelist->email, 'password' => Hash::make('secret'),
                        'company_name' => $guest_panelist->company, 'position' => $guest_panelist->position, 'title' => $guest_panelist->title,
                        'initials' => $guest_panelist->initials]);
                    $create_user->roles()->attach($panelist_role->id);
                    $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                    $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $create_panelist->id,
                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                }
            }
            DB::commit();
            return response()->json(['message' => 'Successfully created panelists.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //Admin decline applicant panel interview
    public function adminDeclineApplicantPanelInterview(Request $request, User $user)
    {
        $user->load('applicant', 'userQuestionAnswers');
        $input = $request->all();
        $user_applicant = $user->applicant;
        $user_applicant->load('contactLog');
        $contact_log = $user_applicant->contactLog;
        $user_question_answers = $user->userQuestionAnswers;
        $applicant_venture_name = "No name";
        $applicant_chosen_category = "Not Chosen";
        $declined_applicant_category_id = null;

        if($input['declined_applicant_category_id'] != null){
            $declined_applicant_category_id = $input['declined_applicant_category_id'];
        }


        try {
            DB::beginTransaction();

            //Check if applicant category has been selected
            if(isset($user_applicant->chosen_category)){
                $applicant_chosen_category = $user_applicant->chosen_category;
            }

            //Check if applicant venture name has been selected
            if(isset($user_applicant->venture_name)){
                $applicant_venture_name = $user_applicant->venture_name;
            }

            if($request->has('referred_company')){
                $create_declined_applicant = DeclinedApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city ,'race' => $user->race,
                    'code' => $user->code, 'declined' => false, 'referred' => true, 'chosen_category' => $applicant_chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'],
                    'venture_name' => $applicant_venture_name, 'referred_company' => $input['referred_company'], 'declined_applicant_category_id' => $declined_applicant_category_id,
                    'declined_reason_two'=> $input['declined_reason_two']]);
            } else {
                $create_declined_applicant = DeclinedApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,'race' => $user->race,
                    'code' => $user->code, 'declined' => true, 'chosen_category' => $applicant_chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'],
                    'venture_name' => $applicant_venture_name, 'declined_applicant_category_id' => $declined_applicant_category_id, 'declined_reason_two'=> $input['declined_reason_two']]);
            }
            if(count($user_question_answers) > 0){
                foreach ($user_question_answers as $question_answer) {
                    $create_declined_applicant_question_answer = DeclinedApplicantQuestionAnswer::create(['d_a_id' => $create_declined_applicant->id,
                        'question_id' => $question_answer->question_id, 'answer_text' => $question_answer->answer_text]);
                    $question_answer->forceDelete();
                }
            }
            if(count($contact_log) > 0){
                foreach ($contact_log as $contact_logs) {
                    $contact_log = DeclinedApplicantContactLog::create(['declined_applicant_id' => $create_declined_applicant->id,
                        'comment' => $contact_logs->comment,
                        'date' => $contact_logs->date,
                        'applicant_contact_results' => $contact_logs->applicant_contact_results]);
                }
            }


            //Automated email
           /* $email = $user->email;
            $name = $user->name;
            $data = array(
                'name' => $name,
                'email' => $email,
                'declined_reason_two' => $input['declined_reason_two']
            );
            $email_field = $user->email;
            Mail::send('emails.declined-applicant-emailer', $data, function ($message) use ($email_field) {
                $message->to($email_field)
                    ->subject('Propella Application Unsuccessful .');
                $message->from('propellabusinessincubator@gmail.com', 'Propella Business Incubator');
            });*/


            $user_applicant->forceDelete();
            $user->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Applicant was removed from the applicants list and set as declined applicant.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()], 200);
        }
    }

    //Admin Refer applicant panel interview
    public function adminReferApplicantPanelInterview(Request $request, User $user)
    {
        $user->load('applicant', 'userQuestionAnswers');
        $input = $request->all();
        $user_applicant = $user->applicant;
        $user_question_answers = $user->userQuestionAnswers;
        $applicant_venture_name = "No name";
        $applicant_chosen_category = "Not Chosen";
        $referred_applicant_category_id = null;

        if($input['referred_applicant_category_id'] != null){
            $referred_applicant_category_id = $input['referred_applicant_category_id'];
        }

        try {
            DB::beginTransaction();

            //Check if applicant category has been selected
            if(isset($user_applicant->chosen_category)){
                $applicant_chosen_category = $user_applicant->chosen_category;
            }

            //Check if applicant venture name has been selected
            if(isset($user_applicant->venture_name)){
                $applicant_venture_name = $user_applicant->venture_name;
            }

            if($request->has('referred_reason')){
                $create_declined_applicant = PropellaReferredApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,
                    'code' => $user->code, 'declined' => true, 'referred' => true, 'chosen_category' => $applicant_chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'referred_reason' => $input['referred_reason'],
                    'venture_name' => $applicant_venture_name, 'referred_company' => $input['referred_company'], 'referred_applicant_category_id' => $referred_applicant_category_id]);
            } else {
                $create_declined_applicant = PropellaReferredApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                    'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                    'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                    'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,
                    'code' => $user->code, 'declined' => true, 'chosen_category' => $applicant_chosen_category,
                    'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'referred_reason' => $input['referred_reason'],
                    'referred_company' => $input['referred_company'],'venture_name ' => $applicant_venture_name, 'referred_applicant_category_id' => $referred_applicant_category_id]);
            }


            if(count($user_question_answers) > 0){
                foreach ($user_question_answers as $question_answer) {
                    $create_declined_applicant_question_answer = ReferredApplicantQuestionAnswer::create(['p_r_a_id' => $create_declined_applicant->id,
                        'question_id' => $question_answer->question_id, 'answer_text' => $question_answer->answer_text]);
                    $question_answer->forceDelete();
                }
            }

            $user_applicant->forceDelete();
            $user->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Applicant was removed from the applicants list and set as referred applicant.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()], 200);
        }
    }

    //Admin decline but refer applicant panel interview
    public function adminDeclineButReferApplicantPanelInterview(Request $request, User $user)
    {
        $user->load('applicant', 'userQuestionAnswers');
        $input = $request->all();
        $user_applicant = $user->applicant;
        $user_question_answers = $user->userQuestionAnswers;
        $applicant_venture_name = "No name";
        $applicant_chosen_category = "Not Chosen";
        $declined_applicant_category_id = null;

        if($input['declined_applicant_category_id'] != null) {
            $declined_applicant_category_id = $input['declined_applicant_category_id'];
        }

        try {
            DB::beginTransaction();

            //Check if applicant category has been selected
            if(isset($user_applicant->chosen_category)){
                $applicant_chosen_category = $user_applicant->chosen_category;
            }

            //Check if applicant venture name has been selected
            if(isset($user_applicant->venture_name)){
                $applicant_venture_name = $user_applicant->venture_name;
            }

            $create_declined_applicant = DeclinedApplicant::create(['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                'contact_number' => $user->contact_number, 'id_number' => $user->id_number,
                'address_one' => $user->address_one, 'address_two' => $user->address_two, 'address_three' => $user->address_three, 'title' => $user->title,
                'initials' => $user->initials, 'dob' => $user->dob, 'age' => $user->age, 'gender' => $user->gender, 'city' => $user->city,'race'=> $user->race,
                'code' => $user->code, 'declined' => true, 'referred' => true, 'chosen_category' => $user_applicant->chosen_category,
                'contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact'], 'declined_reason' => $input['declined_reason_text'],
                'venture_name' => $user_applicant->venture_name, 'referred_company' => $input['referred_company'], 'declined_applicant_category_id' => $declined_applicant_category_id]);

            if(count($user_question_answers) > 0){
                foreach ($user_question_answers as $question_answer) {
                    $create_declined_applicant_question_answer = DeclinedApplicantQuestionAnswer::create(['d_a_id' => $create_declined_applicant->id,
                        'question_id' => $question_answer->question_id, 'answer_text' => $question_answer->answer_text]);
                    $question_answer->forceDelete();
                }
            }

            $user_applicant->forceDelete();
            $user->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Applicant was removed from the applicants list and set as declined applicant.'], 200);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()], 200);
        }
    }

    public function applicantNextStepsView(User $user){
        $logged_in_user = Auth::user()->load('roles');
        $today = Carbon::now()->toDateString();
        $venture_categories = VentureCategory::all();
        $incubatee_stages = IncubateeStage::all();
        $events = Event::where([['type','Private'], ['start', '>=', $today]])->get();
        $declined_applicant_categories = DeclinedApplicantCategory::all();
        //Add
        $referred_applicant_categories = ReferredApplicantCategory::all();
        $user->load('applicant');
        $user_applicant = $user->applicant;
        $user_applicant->load('contactLog');

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/applicants/applicant-next-steps', compact('user', 'venture_categories', 'incubatee_stages', 'events',
                'declined_applicant_categories','referred_applicant_categories','user_applicant'));
        } else if($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/applicants/applicant-next-steps', compact('user', 'venture_categories', 'incubatee_stages', 'events',
                'declined_applicant_categories','referred_applicant_categories','user_applicant'));
        } else if($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/applicants/applicant-next-steps', compact('user', 'venture_categories', 'incubatee_stages', 'events',
                'declined_applicant_categories','referred_applicant_categories','user_applicant'));
        } else if($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/applicants/applicant-next-steps', compact('user', 'venture_categories', 'incubatee_stages', 'events',
                'declined_applicant_categories','referred_applicant_categories','user_applicant'));
        } else {
            return response('You are not authorized!!');
        }
    }

    public function setApplicantToIncubatee(Request $request, User $user){
        $input = $request->all();
        $category_id = $input['category_id'];
        $stage_id = $input['stage_id'];
        $venture_category = VentureCategory::find($category_id);
        $incubatee_stage = IncubateeStage::find($stage_id);
        $user->load('applicant');
        $applicant = $user->applicant;
        $incubatee_role = Role::where('name', 'incubatee')->first();
        $bootcamper_role = Role::where('name', 'bootcamper')->first();
        $today = Carbon::today()->toDateString();

        try{
            DB::beginTransaction();
            $applicant->update(['is_incubatee' => true]);
            $applicant->save();

            $applicant->update(['is_bootcamper' => true]);
            $applicant->save();
            $applicant_venture_name = "No name";

            //Check if applicant venture name is set
            if(isset($applicant->venture_name)){
                $applicant_venture_name = $applicant->venture_name;
            }

            if($venture_category->category_name == 'Industrial'){
                $user->roles()->sync($incubatee_role->id);

                $create_incubatee = Incubatee::create(['user_id' => $user->id, 'startup_name' => $applicant_venture_name, 'company_name' => $applicant_venture_name,
                    'contact_number' => $user->contact_number, 'physical_address_1' => $user->address_one, 'physical_address_2' => $user->address_two,
                    'physical_address_3' => $user->address_three, 'physical_city' => $user->city, 'physical_code' => $user->code, 'incubatee_start_date' => $today,
                    'venture_category_id' => $venture_category->id]);

                $create_incubatee->save();

                $create_incubatee->stages()->attach([$incubatee_stage->id]);
            } else {
                $user->roles()->sync($bootcamper_role->id);
                $create_bootcamper = Bootcamper::create(['user_id' => $user->id, 'venture_category_id' => $venture_category->id]);

                $selected_events = json_decode($input['events']);
                $event_dates = [];

                if ($request->has('events')) {
                    foreach ($selected_events as $s_event) {
                        $event = Event::find($s_event);
                        $create_bootcamper_event = EventBootcamper::create(['event_id' => $event->id, 'bootcamper_id' => $create_bootcamper->id,
                            'accepted' => false, 'attended' => false, 'date_registered' => $today]);
                        array_push($event_dates, $event->start->toDateString());
                    }
                }

                $email = $user->email;
                $password = 'Password_1234';
                $link = 'https://thepropella.co.za';
                $data = array(
                   'name' => $user->name,
                    'email' => $email,
                    'password' => $password,
                    'link' => $link,
                    'event_dates' => $event_dates
                );
                $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                $fromEmail = "dev@propellaincubator.co.za";
                $toEmail = $email;
                $subject = "Propella Start Up Bootcamp";
                $htmlBody = "<p>Dear <b>$user->name</b></p>
                <p>Congratulations, it is with great pleasure that we wish to inform you that your entry to the Start Up Bootcamp has been successful.</p>
                <br>
                <p>The Boot Camp is facilitated via Zoom over 2 days (Monday’s and Tuesday’s).  Dates have been allocated to you.  To find out what they are login to the Propella Platform… BUT WAIT, please read on first….</p>
                <br>
                <p>Day 1 starts at 08h30 sharp for registration and 09h00 on Day 2.  It runs until 16h00 on both days, therefore if you are working full time, this programme is unfortunately not for you</p>
                <br>
                <p>During the Bootcamp we will assist you in refining your business idea, address business fundamentals, customer validation amongst others and get you pitch ready.</p>
                <br>
                <p>Have a pen and paper handy, ensure you have a quiet place, a reliable laptop, phone or tablet to access the zoom workshop and of course bring your “A” game.  </p>
                <br>
                <p>We will be providing data for duration of Bootcamp <span>(only if you log into the Propella Platform and provide us with all the information that we need)</span> but you will need to have your own data to join us on Day 1.</p>
                <br>
                <p>All work and no play is not allowed so there will be some fun interactions and comfort breaks.</p>
                <br>
                <p>Please confirm that you are willing and available to join us for the journey that lies ahead:</p>
                <br>
                <p><span><b>Login to the platform Username: <span><b>Your email address</b></span> and your password: <span><b>Password_1234</b></span> </p>
                <p> •	Accept your Bootcamp </p>
                <p > •	Next confirm your cell phone number and service provider on the data button – for data </p >
                <p > •	Then upload a copy of your id </p >
                <p > •	We will also need your proof of address not older than 3 months </p >
                <p > •	Read and accept that Non - Disclosure Agreement that protects each other’s ideas </p >
                <p > •	For those with existing companies upload your CIPC documents onto the platform . Having a registered company is not a pre - requisite </p >
                <br >
                <p >If the dates don’t suit you, then please call us on 041 502 3700 so that we can re - assign you .</p >
                <br >
                <p >If you are no longer interested in attending then please email  mailto:reception@propellaincubator . co . za </p >
                <br >
                <p >For any queries contact  reception@propellaincubator . co . za </p >
                <br >
                <p > To log into your profile please follow this link . $link</a ></p >
                <br >
                <br >
                <p ><b > Log in details </b ></p >
                <p > User name:  Please use the email address that you applied with </p >
                <p > Password: Password_1234 </p >
                <br >
                <p > Congrats again!</p >
                <br >
                <p > Kind regards,</p >
                <br >
                <p ><b > Team at propella </b ></p >
                <br >";
                $textBody = "";
                $tag = "example-email-tag";
                $trackOpens = true;
                $trackLinks = "None";
                $messageStream = "broadcast";

                // Send an email:
                $sendResult = $client->sendEmail(
                    $fromEmail,
                    $toEmail,
                    $subject,
                    $htmlBody,
                    $textBody,
                    $tag,
                    $trackOpens,
                    NULL, // Reply To
                    NULL, // CC
                    NULL, // BCC
                    NULL, // Header array
                    NULL, // Attachment array
                    $trackLinks,
                    NULL, // Metadata array
                    $messageStream
                );

                /*$email_field = $email;
                Mail::send('emails.bootcamper-acceptance-letter', $data, function ($message) use ($email_field) {
                    $message->to($email_field)
                        ->subject('Propella Start Up Bootcamp');
                    $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

                });*/
            }

            DB::commit();
            return response()->json(['message' => 'Applicant now given permission into the system.'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Edit bootcamper Basic info
    public function editBootcamperBasicInfo(User $user){
        $logged_in_user = Auth::user()->load('roles');
        if($logged_in_user->roles[0]->name == 'bootcamper'){
            $user->load('bootcamper');
            return view('users.bootcampers.edit-bootcamper-basic-info', compact('user'));
        }elseif($logged_in_user->roles[0]->name == 'app-admin'){
            $user->load('bootcamper');
            return view('users.app-admin.bootcampers.edit-bootcamper-basic-info', compact('user'));
        } else {
            return response("You are not authorized!");
        }
    }

    public function editBootcamperBasic(Bootcamper $bootcamper){
        $logged_in_user = Auth::user()->load('roles');
        $bootcamper->load('user');
       if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.app-admin.bootcampers.edit-bootcamper-basic-info',compact('bootcamper'));
        } elseif($logged_in_user->roles[0]->name == 'administrator'){
           return view('users.administrators.bootcampers.edit-bootcamper-basic-info', compact('bootcamper'));
       } else {
            return response("You are not authorized!");
        }
    }

    //Bootcamper Update basic info
    public function bootcamperUpdateBasic(Request $request, Bootcamper $bootcamper){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $bootcamper->user()->update(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'],'race'=> $input['race'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'], 'gender' => $input['gender'],
                'service_provider_network' => $input['service_provider_network'],
                'data_cellnumber' => $input['data_cellnumber']]);


            DB::commit();

            return response()->json(['bootcamper' => $bootcamper, 'message' => 'Your details have successfully been updated ' . $bootcamper->user->name . '.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Bootcamper Update basic info
    public function bootcamperUpdateBasicInfo(Request $request, User $user){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $user->update(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'],'race'=> $input['race'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'], 'gender' => $input['gender'],
                'email' => $input['email'],'service_provider_network' =>$input['service_provider_network'],
                'data_cellnumber' => $input['data_cellnumber']]);

            $user->applicant()->update(['venture_name' => $input['venture_name']]);

            DB::commit();

            return response()->json(['user' => $user, 'message' => 'Your details have successfully been updated ' . $user->name . '.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Edit application that is not complete
    public function applicantEditApplication(User $user)
    {
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'applicant') {
            $user->load('applicant', 'userQuestionAnswers');
            $g_question_category = "";
            $g_questions = "";
            $applicant_array = [];

            if(count($user->userQuestionAnswers) > 0){
                foreach ($user->userQuestionAnswers as $item) {
                    $question_id = $item->question_id;
                    $question = Question::find($question_id);
                    $answer_text = $item->answer_text;
                    $object = (object)['question_number' => $question->question_number,
                        'question_text' => $question->question_text, 'answer_text' => $answer_text,
                        'question_id' => $question_id];
                    array_push($applicant_array, $object);
                }
            } else {
                $applicant_category = $user->applicant->chosen_category;
                $question_category = QuestionsCategory::where('category_name', $applicant_category)->firstOrFail();
                $question_category->load('questions');
                $g_question_category = $question_category;
                $questions = $question_category->questions->sortBy('question_number');
                $g_questions = $questions;
            }




            return view('users/applicants/edit-application', compact('user', 'applicant_array', 'g_question_category', 'g_questions'));
        }
    }

    //Applicant edit basic information view
    public function applicantEditBasicInfo(User $user){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'applicant'){
            $user->load('applicant');
            return view('users/applicants/edit-basic-info', compact('user'));
        } else {
            return response("You are not authorized!");
        }
    }

    //Applicant update basic information
    public function applicantUpdateBasicInfo(Request $request, User $user){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $user->update(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'], 'gender' => $input['gender'],
                'email' => $input['email']]);

            $user->applicant()->update(['venture_name' => $input['venture_name']]);

            DB::commit();

            return response()->json(['user' => $user, 'message' => 'Your details have successfully been updated ' . $user->name . '.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Applicant Delete Application
    public function applicantDeleteApplication(User $user)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        try {
            $user->load('userQuestionAnswers');
            foreach ($user->userQuestionAnswers as $item) {
                $item->forceDelete();
            }

            $user->applicant->forceDelete();
            $user->forceDelete();

            DB::statement('SET FOREIGN_KEY_CHECKS=1');

            return response()->json(['message' => 'Application deleted successfully'], 200);


        } catch (\Exception $e) {
            return response()->json(['message' => 'Application can not be deleted ' . $e->getMessage(), 'user' => $user->load('userQuestionAnswers')], 500);
        }
    }

    //Panelist show industrial applicants view
    public function panelistShowIndustrialApplicants()
    {
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/show-panel-applicants', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/show-panel-applicants', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/show-panel-applicants', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'incubatee') {
            return view('/users/incubatee-dashboard/applicants/show-applicants', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'mentor') {
            return view('/users/mentor-dashboard/applicants/show-applicants', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'marketing') {
            return view('/users/marketing-dashboard/applicants/show-applicants', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'panelist') {
            return view('/users/panelist/applicants/show-applicants', compact('logged_in_user'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Panelist show ict applicants view
    public function panelistShowICTApplicants(){
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/bootcampers/show-panel-bootcampers', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/bootcampers/show-panel-bootcampers', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/bootcampers/show-panel-bootcampers', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'incubatee') {
            return view('/users/incubatee-dashboard/bootcampers/show-bootcampers', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'mentor') {
            return view('/users/mentor-dashboard/bootcampers/show-panel-bootcampers', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'marketing') {
            return view('/users/marketing-dashboard/bootcampers/show-panel-bootcampers', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'panelist') {
            return view('/users/panelist/bootcampers/show-panel-bootcampers', compact('logged_in_user'));
        } else {
            return response('You are not authorized!');
        }
    }

    public function adminShowICTPanelInterviews(){
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/bootcampers/show-bootcamper-panel-interviews', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/bootcampers/show-bootcamper-panel-interviews', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/bootcampers/show-bootcamper-panel-interviews', compact('logged_in_user'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function adminGetICTPanelInterviews(){
        $logged_in_user = Auth::user()->load('roles');
        $bootcamper_panelists = BootcamperPanelist::all();
        $temp_bootcamper_id = '';
        $interview_array = [];

        foreach($bootcamper_panelists as $bootcamper_panelist){
            if($temp_bootcamper_id != $bootcamper_panelist->bootcamper_id){
                $bootcamper_id = $bootcamper_panelist->bootcamper_id;
                $bootcamper = Bootcamper::find($bootcamper_id);
                if($bootcamper->pitch_video_link != null){
                    $bootcamper->load('user');
                    $bootcamper_user = $bootcamper->user;
                    $bootcamper_average = 0;
                    if($bootcamper->average_panel_score != null or $bootcamper->average_panel_score != 0){
                        $bootcamper_average = $bootcamper->average_panel_score;
                    }
                    //Query db to get the count of bootcamper_panelists to display on datatable
                    $panelist_assigned_to_bootcamper = DB::table('bootcamper_panelists')
                        ->where('bootcamper_id', '=', $bootcamper_panelist->bootcamper_id)
                        ->get();
                    //Assign a variable of the count of the above query result
                    $panelist_count = count($panelist_assigned_to_bootcamper);
                }
                $object = (object)['name' => $bootcamper_user->name, 'surname' => $bootcamper_user->surname, 'panel_selection_date' => $bootcamper_panelist->panel_selection_date];
                array_push($interview_array, $object);
                $temp_bootcamper_id = $bootcamper_panelist->bootcamper_id;
            }
        }

        if($logged_in_user->roles[0]->name == 'app-admin' or
            $logged_in_user->roles[0]->name == 'administrator' or
            $logged_in_user->roles[0]->name == 'advisor'){
            return Datatables::of(array_unique($interview_array))->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Get the data for the datatable
    public function panelistGetIndustrialApplicants(User $user)
    {
        $user->load('panelist');
        $user_panelist = $user->panelist;
        $user_panelist->load('applicants');
        $panelist_applicants = $user_panelist->applicants;
        $panelist_applicants_array = [];
        $today = Carbon::now()->toDateString();

        foreach ($panelist_applicants as $panelist_applicant) {
            if ($today <= $panelist_applicant->panel_selection_date) {
                $applicant_id = $panelist_applicant->applicant_id;
                $panel_selection_date = $panelist_applicant->panel_selection_date;
                $searched_applicant = Applicant::find($applicant_id);
                $applicant_user_id = $searched_applicant->user_id;

                $object = (object)['name' => $searched_applicant->user->name, 'surname' => $searched_applicant->user->surname,
                    'email' => $searched_applicant->user->email, 'chosen_category' => $searched_applicant->chosen_category,
                    'selection_date' => $panel_selection_date, 'applicant_id' => $searched_applicant->id,
                    'panelist_id' => $panelist_applicant->panelist_id, 'applicant_user_id' => $applicant_user_id];
                array_push($panelist_applicants_array, $object);
            }
        }

        return Datatables::of($panelist_applicants_array)->addColumn('action', function ($panelist_applicant) {
            $applicant_questions = '/panelist-view-applicant-questions/' . $panelist_applicant->applicant_user_id;
            $applicant_score_sheet = '/panelist-view-applicant-score-sheet/' . $panelist_applicant->applicant_id;
            return '<a href=' . $applicant_questions . ' title="Show Applicant Questions" style="color:blue!important;"><i class="material-icons">remove_red_eye</i></a><a href=' . $applicant_score_sheet . ' title="Show Applicant Score Sheet" style="color:orange!important;"><i class="material-icons">assignment_ind</i></a>';
        })
            ->make(true);
    }

    public function panelistGetICTApplicants(User $user){
        $user->load('panelist');
        $user_panelist = $user->panelist;
        $user_panelist->load('bootcamper');
        $panelist_applicants = $user_panelist->bootcamper;
        $panelist_applicants_array = [];
        $today = Carbon::now()->toDateString();

        foreach ($panelist_applicants as $panelist_applicant) {
            if ($today <= $panelist_applicant->panel_selection_date) {
                $bootcamper_id = $panelist_applicant->bootcamper_id;
                $panel_selection_date = $panelist_applicant->panel_selection_date;
                $searched_bootcamper = Bootcamper::find($bootcamper_id);
                $bootcamper_user_id = $searched_bootcamper->user_id;

                $object = (object)['name' => $searched_bootcamper->user->name, 'surname' => $searched_bootcamper->user->surname,
                    'email' => $searched_bootcamper->user->email, 'chosen_category' => $searched_bootcamper->user->applicant->chosen_category,
                    'selection_date' => $panel_selection_date, 'bootcamper_id' => $searched_bootcamper->id,
                    'panelist_id' => $panelist_applicant->panelist_id, 'bootcamper_user_id' => $bootcamper_user_id];
                array_push($panelist_applicants_array, $object);
            }
        }

        return Datatables::of($panelist_applicants_array)->addColumn('action', function ($panelist_applicant) {
            $applicant_questions = '/panelist-view-ict-applicant-questions/' . $panelist_applicant->bootcamper_user_id;
            $applicant_score_sheet = '/panelist-view-ict-applicant-score-sheet/' . $panelist_applicant->bootcamper_id;
            return '<a href=' . $applicant_questions . ' title="Show Applicant Questions" style="color:blue!important;"><i class="material-icons">remove_red_eye</i></a><a href=' . $applicant_score_sheet . ' title="Show Applicant Score Sheet" style="color:orange!important;"><i class="material-icons">assignment_ind</i></a>';
        })
            ->make(true);
    }

    public function panelistShowVentureInterviews(){
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/venture/show-venture-interviews', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/ventures/show-venture-interviews', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/ventures/show-venture-interviews', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'incubatee') {
            return view('/users/incubatee-dashboard/venture/show-venture-interviews', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'mentor') {
            return view('/users/mentor-dashboard/ventures/show-venture-interviews', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'marketing') {
            return view('/users/marketing-dashboard/ventures/show-venture-interviews', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'panelist') {
            return view('/users/panelist/ventures/show-venture-interviews', compact('logged_in_user'));
        } else {
            return response('You are not authorized!');
        }
    }

    public function getPanelistVentureInterviews(User $user){
        $user->load('panelist');
        $user_panelist = null;
        $panelist_venture_interviews = null;
        $panelist_venture_interview_array = [];
        $today = Carbon::now()->toDateString();

        if(isset($user->panelist)){
            $user_panelist = $user->panelist;
            $user_panelist->load('venturePanelInterviews');

            if(isset($user_panelist->venturePanelInterviews)){
                $panelist_venture_interviews = $user_panelist->venturePanelInterviews;
            }
        }

        if($panelist_venture_interviews != null){
            if(count($panelist_venture_interviews) > 0){
                foreach ($panelist_venture_interviews as $p_interview) {
                    if ($today <= $p_interview->panel_selection_date) {
                        $venture_id = $p_interview->venture_id;
                        $panel_selection_date = $p_interview->panel_selection_date;
                        $searched_venture = Venture::find($venture_id);
                        $venture_panel_interview_category_id = $p_interview->venture_panel_interview_category_id;
                        $venture_panel_interview_category = null;

                        if(isset($p_interview->venture_panel_interview_category_id)){
                            $venture_panel_interview_category = VenturePanelInterviewCategory::findOrFail($venture_panel_interview_category_id);
                        }

                        if($venture_panel_interview_category != null){
                            $object = (object)['venture_name' => $searched_venture->company_name, 'panel_selection_date' => $p_interview->panel_selection_date,
                                'panel_selection_time' => $p_interview->panel_selection_time, 'interview_category' => $venture_panel_interview_category->category_name
                                ,'venture_interview_id' => $p_interview->id];
                            array_push($panelist_venture_interview_array, $object);
                        } else {
                            $object = (object)['venture_name' => $searched_venture->company_name, 'panel_selection_date' => $p_interview->panel_selection_date,
                                'panel_selection_time' => $p_interview->panel_selection_time, 'interview_category' => 'No category chosen'
                                ,'venture_interview_id' => $p_interview->id];
                            array_push($panelist_venture_interview_array, $object);
                        }
                    }
                }
            }
        }


        return Datatables::of($panelist_venture_interview_array)
            ->addColumn('action', function ($panelist_interview) {
            $venture_interview_score_sheet = '/panelist-view-venture-interview-score-sheet/' . $panelist_interview->venture_interview_id;
            return '<a href=' . $venture_interview_score_sheet . ' title="Show Venture Score Sheet" style="color:orange!important;"><i class="material-icons">assignment_ind</i></a>';
        })
            ->make(true);
    }

    public function showVentureInterviewPanelScoreSheet(VenturePanelInterview $venturePanelInterview){
        $venturePanelInterview->load('venturePanelInterviewQuestionAnswers','venture');
        $ven = $venturePanelInterview->venture;
        $ven->load('ventureUploads');

        $logged_in_user = Auth::user()->load('roles', 'panelist');
        $question_category_id = $venturePanelInterview->question_category_id;
        $question_category = QuestionsCategory::find($question_category_id);
        $questions = $question_category->questions->sortBy('question_number');
        $questions->load('questionSubTexts');
        $cur_panelist_question_answers = [];

        if(count($venturePanelInterview->venturePanelInterviewQuestionAnswers) > 0){
            foreach($venturePanelInterview->venturePanelInterviewQuestionAnswers as $pqa){
                $question_id = $pqa->question_id;
                $question = Question::find($question_id);
                $question_number = $question->question_number;
                $question_text = $question->question_text;
                $question_sub_texts = [];
                $question->load('questionSubTexts');
                if(count($question->questionSubTexts) > 0){
                    foreach($question->questionSubTexts as $qst){
                        $object = (object)['question_sub_text' => $qst->question_sub_text];
                        array_push($question_sub_texts, $object);
                    }
                }
                $score = $pqa->score;
                $comment = $pqa->comment;

                $object = (object)['question_id' => $question_id, 'question_sub_texts' => $question_sub_texts,
                    'score' => $score, 'comment' => $comment, 'question_number' => $question_number, 'question_text' => $question_text];
                array_push($cur_panelist_question_answers, $object);
            }
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/venture/show-venture-interview-score-sheet', compact('venturePanelInterview',
                'question_category', 'questions', 'cur_panelist_question_answers','ven'));
        } else if ($logged_in_user->roles[0]->name == 'panelist') {
            return view('/users/panelist/ventures/show-venture-interview-score-sheet', compact('venturePanelInterview',
                'question_category', 'questions', 'cur_panelist_question_answers','ven'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/ventures/show-venture-interview-score-sheet', compact('venturePanelInterview',
                'question_category', 'questions', 'cur_panelist_question_answers','ven'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/ventures/show-venture-interview-score-sheet', compact('venturePanelInterview',
                'question_category', 'questions', 'cur_panelist_question_answers','ven'));
        } else if ($logged_in_user->roles[0]->name == 'incubatee') {
            return view('/users/incubatee-dashboard/venture/show-venture-interview-score-sheet', compact('venturePanelInterview',
                'question_category', 'questions', 'cur_panelist_question_answers','ven'));
        } else if ($logged_in_user->roles[0]->name == 'mentor') {
            return view('/users/mentor-dashboard/ventures/show-venture-interview-score-sheet', compact('venturePanelInterview',
                'question_category', 'questions', 'cur_panelist_question_answers','ven'));
        } else if($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/ventures/show-venture-interview-score-sheet', compact('venturePanelInterview',
                'question_category', 'questions', 'cur_panelist_question_answers','ven'));
        } else {
            return response("You are not authorized!!");
        }
    }

    public function panelistSaveVentureInterviewQuestionAnswerComments(Request $request){
        $input = $request->all();
        $questions_answers_comments_array = json_decode($input['questions_answers_comments']);
        $venture_interview_id = $questions_answers_comments_array[0]->venture_interview_id;
        $venture_interview = VenturePanelInterview::find($venture_interview_id);
        $venture_interview->load('venturePanelInterviewQuestionAnswers');

        try {
            if(isset($venture_interview->venturePanelInterviewQuestionAnswers)){
                foreach ($questions_answers_comments_array as $item) {
                    $find_question_answer = DB::table('venture_panelist_question_answers')->where('venture_panel_interview_id', $venture_interview->id)
                        ->where('question_id', $item->question_id)->first();

                    if(isset($find_question_answer)){
                        $score = (integer)$item->answer_score;
                        DB::table('venture_panelist_question_answers')->where('venture_panel_interview_id', $venture_interview->id)
                            ->where('question_id', $item->question_id)->update(['score' => $score, 'comment' => $item->comment]);
                    } else {
                        $score = (integer)$item->answer_score;
                        $create_object = VenturePanelistQuestionAnswer::create(['question_id' => $item->question_id, 'venture_panel_interview_id' => $venture_interview->id,
                            'score' => $score, 'comment' => $item->comment]);
                    }
                }
            } else {
                foreach ($questions_answers_comments_array as $item) {
                    $score = (integer)$item->answer_score;
                    $create_object = VenturePanelistQuestionAnswer::create(['question_id' => $item->question_id, 'venture_panel_interview_id' => $venture_interview->id,
                        'score' => $score, 'comment' => $item->comment]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Completed! Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function adminViewVenturePanelInterviewOverview(Venture $venture){
        $logged_in_user = Auth::user()->load('roles');
        $venture_interview_categories = VenturePanelInterviewCategory::all();

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/venture/show-venture-interview-overview', compact('logged_in_user',
                'venture_interview_categories', 'venture'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/ventures/show-venture-interview-overview', compact('logged_in_user'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/ventures/show-venture-interview-overview', compact('logged_in_user'));
        } else {
            return response('You are not authorized!');
        }
    }

    public function adminGetVentureInterviewOverviewInformation(Request $request, VenturePanelInterviewCategory $venturePanelInterviewCategory)
    {
        $venturePanelInterviewCategory->load('venturePanelInterviews');
        $input = $request->all();
        $venture_id = $input['venture_id'];
        $venture = Venture::find($venture_id);
        $venture_interviews = $venturePanelInterviewCategory->venturePanelInterviews()->where('venture_id', $venture_id)->get();
        $venture_interview_results_array = [];
        $panelists_question_score_collection = [];
        $final_question_numbers = [];
        $total_interview_score = 0;
        $average_interview_score = 0;
        $panelist_count = 0;

        if (count($venture_interviews) > 0) {
            foreach ($venture_interviews as $venture_interview) {
                $venture_interview->load('venture', 'panelist', 'venturePanelInterviewQuestionAnswers');
                $venture_interview_panelist = null;
                $panelist_full_name = "";
                $panelist_questions_scores = [];
                $panelist_interview_score = 0;

                if (isset($venture_interview->panelist)) {
                    $venture_interview_panelist = $venture_interview->panelist;
                    $venture_interview_panelist->load('user');
                    $panelist_user = null;

                    if (isset($venture_interview_panelist->user)) {
                        $panelist_user = $venture_interview_panelist->user;
                    }

                    if ($panelist_user != null) {
                        $panelist_full_name = $panelist_user->name . ' ' . $panelist_user->surname;
                    } else {
                        $panelist_full_name = "user name not set";
                    }
                }

                $panelist_question_answers = VenturePanelistQuestionAnswer::where('venture_panel_interview_id', $venture_interview->id)->get();

                if (count($panelist_question_answers) > 0) {
                    foreach ($panelist_question_answers as $panelist_question_answer) {
                        $question_id = $panelist_question_answer->question_id;
                        $cur_question = Question::find($question_id);
                        $cur_question_number = $cur_question->question_number;
                        $cur_score = $panelist_question_answer->score;
                        $panelist_interview_score += $cur_score;
                        $object = (object)['question_number' => $cur_question_number,
                            'question_score' => $cur_score];
                        array_push($panelist_questions_scores, $object);
                        array_push($panelists_question_score_collection, $object);
                        array_push($final_question_numbers, $cur_question_number);
                    }

                    $object = (object)['panelist_full_name' => $panelist_full_name,
                        'user_questions_scores' => $panelist_questions_scores, 'panelist_applicant_score' => $panelist_interview_score,
                        'venture_interview_id' => $venture_interview->id];
                    array_push($venture_interview_results_array, $object);

                    $total_interview_score += $panelist_interview_score;

                    $panelist_questions_scores = [];
                    $panelist_applicant_score = 0;
                }
            }
        }

        foreach ($venture_interview_results_array as $pqsa){
            if(count($pqsa->user_questions_scores)){
                $panelist_count += 1;
            }
        }

        if($panelist_count > 0){
            $average_interview_score = $total_interview_score / $panelist_count;
        } else {
            $average_interview_score = 0;
        }

        //Store total panel score and average bootcamp score to the database for further use
        if(isset($venture_interviews)){
            DB::beginTransaction();
            foreach($venture_interviews as $venture_interview) {
                $venture_interview->total_panel_score = $total_interview_score;
                $venture_interview->average_panel_score = $average_interview_score;
                $venture_interview->save();
            }
            DB::commit();
        }

        $question_scores_array = [];
        $question_score = 0;

        foreach (array_unique($final_question_numbers) as $q_number){
            foreach ($venture_interviews as $venture_interview) {
                $venture_interview->load('venturePanelInterviewQuestionAnswers');
                $applicant_panelist_questions = $venture_interview->venturePanelInterviewQuestionAnswers;
                foreach ($applicant_panelist_questions as $applicant_panelist_question){
                    $question_id = $applicant_panelist_question->question_id;
                    $cur_question = Question::find($question_id);
                    if($cur_question->question_number == $q_number){
                        $question_score += $applicant_panelist_question->score;
                    }
                }
            }
            $object = (object)['question_number' => $q_number, 'question_score' => $question_score];
            array_push($question_scores_array, $object);
            $question_score = 0;
        }

        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'administrator'
            or $logged_in_user->roles[0]->name == 'advisor'){
            $data = (object)[
                'venture' => $venture,
                'venture_interview_results_array' => $venture_interview_results_array,
                'question_scores_array' => $question_scores_array,
                'total_interview_score' => $total_interview_score,
                'average_interview_score' => $average_interview_score
            ];

            $html = view('/dynamic-pages/venture-interview-overview', compact('data'))->render();

            return response()->json(['view_data' => $html]);
        } else {
            return response("You are not authorized!!");
        }
    }

    public function adminGetPanelistVentureInterviewScoreSheet(VenturePanelInterview $venturePanelInterview){
        $venturePanelInterview->load('venturePanelInterviewQuestionAnswers');
        $panelist_question_answers = [];
        $logged_in_user = Auth::user()->load('roles');
        $panelist = $venturePanelInterview->panelist;
        $panelist->load('user');
        $panelist_user = $panelist->user;
        $user_object = (object)['panelist_full_name' => $panelist_user->name . ' ' . $panelist_user->surname];

        if(isset($venturePanelInterview->venturePanelInterviewQuestionAnswers)){
            if(count($venturePanelInterview->venturePanelInterviewQuestionAnswers) > 0){
                foreach($venturePanelInterview->venturePanelInterviewQuestionAnswers as $pqa){
                    $question_id = $pqa->question_id;
                    $question = Question::find($question_id);
                    $question->load('questionSubTexts');
                    $question_sub_texts = [];

                    if(isset($question->questionSubTexts)){
                        foreach ($question->questionSubTexts as $questionSubText){
                            $question_sub_text = $questionSubText->question_sub_text;
                            array_push($question_sub_texts, $question_sub_text);
                        }
                    }

                    $question_text = $question->question_text;
                    $question_number = $question->question_number;
                    $score = $pqa->score;
                    $comment = $pqa->comment;

                    $object = (object)['question_text' => $question_text, 'question_number' => $question_number, 'score' => $score,
                        'comment' => $comment, 'question_sub_texts' => $question_sub_texts];
                    array_push($panelist_question_answers, $object);
                }

            }
        }

        if($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'administrator'
            or $logged_in_user->roles[0]->name == 'advisor'){
            $html = view('/dynamic-pages/venture-interview-panelist-score-sheet', compact('panelist_question_answers',
            'user_object'))->render();

            return response()->json(['view_data' => $html]);
        } else {
            return response("You are not authorized!!");
        }
    }


    //Show Panelist Score Sheet
    public function showPanelScoreSheet(Applicant $applicant)
    {
        $logged_in_user = Auth::user()->load('roles', 'panelist');
        $applicant->load('user');
        $applicant_panelist = DB::table('applicant_panelists')->where('applicant_id', $applicant->id)
            ->where('panelist_id', $logged_in_user->panelist->id)->first();
        $question_category_id = $applicant_panelist->question_category_id;
        $question_category = QuestionsCategory::find($question_category_id);
        $questions = $question_category->questions->sortBy('question_number');
        $questions->load('questionSubTexts');
        $cur_applicant_panelist = ApplicantPanelist::find($applicant_panelist->id);
        $cur_applicant_panelist->load('panelistQuestionAnswers');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist','applicant'));
        } else if ($logged_in_user->roles[0]->name == 'panelist') {
            return view('/users/panelist/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist','applicant'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist','applicant'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist','applicant'));
        } else if ($logged_in_user->roles[0]->name == 'incubatee') {
            return view('/users/incubatee-dashboard/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist','applicant'));
        } else if ($logged_in_user->roles[0]->name == 'mentor') {
            return view('/users/mentor-dashboard/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist','applicant'));
        }

        if (end($user_roles_last_item) == 'panelist') {
            if (end($user_roles_last_item) == 'app-admin') {
                return view('/users/app-admin/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                    'question_category', 'questions', 'cur_applicant_panelist'));
            } else if (end($user_roles_last_item) == 'panelist') {
                return view('/users/panelist/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                    'question_category', 'questions', 'cur_applicant_panelist'));
            } else if (end($user_roles_last_item) == 'administrator') {
                return view('/users/administrators/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                    'question_category', 'questions', 'cur_applicant_panelist'));
            } else if (end($user_roles_last_item) == 'advisor') {
                return view('/users/advisor-dashboard/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                    'question_category', 'questions', 'cur_applicant_panelist'));
            } else if (end($user_roles_last_item) == 'incubatee') {
                return view('/users/incubatee-dashboard/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                    'question_category', 'questions', 'cur_applicant_panelist'));
            } else if (end($user_roles_last_item) == 'mentor') {
                return view('/users/mentor-dashboard/applicants/show-applicant-score-sheet', compact('applicant_panelist',
                    'question_category', 'questions', 'cur_applicant_panelist'));
            }
        }
    }

    public function showICTPanelScoreSheet(Bootcamper $bootcamper){
        $logged_in_user = Auth::user()->load('roles', 'panelist');
        $applicant_panelist = DB::table('bootcamper_panelists')->where('bootcamper_id', $bootcamper->id)
            ->where('panelist_id', $logged_in_user->panelist->id)->first();
        $question_category_id = $applicant_panelist->question_category_id;
        $question_category = QuestionsCategory::find($question_category_id);
        $questions = $question_category->questions->sortBy('question_number');
        $questions->load('questionSubTexts');
        $cur_applicant_panelist = BootcamperPanelist::find($applicant_panelist->id);
        $cur_applicant_panelist->load('panelistQuestionAnswers');
        $cur_panelist_question_answers = [];

        if(count($cur_applicant_panelist->panelistQuestionAnswers) > 0){
            foreach($cur_applicant_panelist->panelistQuestionAnswers as $pqa){
                $question_id = $pqa->question_id;
                $question = Question::find($question_id);
                $question_number = $question->question_number;
                $question_text = $question->question_text;
                $question_sub_texts = [];
                $question->load('questionSubTexts');
                if(count($question->questionSubTexts) > 0){
                    foreach($question->questionSubTexts as $qst){
                        $object = (object)['question_sub_text' => $qst->question_sub_text];
                        array_push($question_sub_texts, $object);
                    }
                }
                $score = $pqa->score;
                $comment = $pqa->comment;

                $object = (object)['question_id' => $question_id, 'question_sub_texts' => $question_sub_texts,
                    'score' => $score, 'comment' => $comment, 'question_number' => $question_number, 'question_text' => $question_text];
                array_push($cur_panelist_question_answers, $object);
            }
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if ($logged_in_user->roles[0]->name == 'panelist') {
            return view('/users/panelist/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if ($logged_in_user->roles[0]->name == 'incubatee') {
            return view('/users/incubatee-dashboard/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if ($logged_in_user->roles[0]->name == 'mentor') {
            return view('/users/mentor-dashboard/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else {
            return response("You are not authorized!!");
        }
    }

    public function panelistSaveQuestionAnswerComments(Request $request)
    {
        $input = $request->all();
        $questions_answers_comments_array = json_decode($input['questions_answers_comments']);
        $applicant_panelist_id = $questions_answers_comments_array[0]->applicant_panelist_id;
        $applicant_panelist = ApplicantPanelist::find($applicant_panelist_id);
        $applicant_panelist->load('panelistQuestionAnswers');

        try {
            if(isset($applicant_panelist->panelistQuestionAnswers)){
                foreach ($questions_answers_comments_array as $item) {
                    $find_question_answer = DB::table('panelist_question_answers')->where('applicant_panelist_id', $applicant_panelist_id)
                        ->where('question_id', $item->question_id)->first();

                    if(isset($find_question_answer)){
                        $score = (integer)$item->answer_score;
                        DB::table('panelist_question_answers')->where('applicant_panelist_id', $applicant_panelist_id)
                            ->where('question_id', $item->question_id)->update(['score' => $score, 'comment' => $item->comment]);
                    } else {
                        $score = (integer)$item->answer_score;
                        $create_object = PanelistQuestionAnswer::create(['question_id' => $item->question_id, 'applicant_panelist_id' => $item->applicant_panelist_id,
                            'score' => $score, 'comment' => $item->comment]);
                    }
                }
            } else {
                foreach ($questions_answers_comments_array as $item) {
                    $score = (integer)$item->answer_score;
                    $create_object = PanelistQuestionAnswer::create(['question_id' => $item->question_id, 'applicant_panelist_id' => $item->applicant_panelist_id,
                        'score' => $score, 'comment' => $item->comment]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Completed!'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function panelistSaveIctQuestionAnswerComments(Request $request){
        $input = $request->all();
        $questions_answers_comments_array = json_decode($input['questions_answers_comments']);
        $applicant_panelist_id = $questions_answers_comments_array[0]->applicant_panelist_id;
        $applicant_panelist = BootcamperPanelist::find($applicant_panelist_id);
        $applicant_panelist->load('panelistQuestionAnswers');

        try {
            if(isset($applicant_panelist->panelistQuestionAnswers)){
                foreach ($questions_answers_comments_array as $item) {
                    $find_question_answer = DB::table('ict_panelist_question_answers')->where('bootcamper_panelist_id', $applicant_panelist_id)
                        ->where('question_id', $item->question_id)->first();

                    if(isset($find_question_answer)){
                        $score = (integer)$item->answer_score;
                        DB::table('ict_panelist_question_answers')->where('bootcamper_panelist_id', $applicant_panelist_id)
                            ->where('question_id', $item->question_id)->update(['score' => $score, 'comment' => $item->comment]);
                    } else {
                        $score = (integer)$item->answer_score;
                        $create_object = IctPanelistQuestionAnswer::create(['question_id' => $item->question_id, 'bootcamper_panelist_id' => $item->applicant_panelist_id,
                            'score' => $score, 'comment' => $item->comment]);
                    }
                }
            } else {
                foreach ($questions_answers_comments_array as $item) {
                    $score = (integer)$item->answer_score;
                    $create_object = IctPanelistQuestionAnswer::create(['question_id' => $item->question_id, 'bootcamper_panelist_id' => $item->applicant_panelist_id,
                        'score' => $score, 'comment' => $item->comment]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Completed!'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    public function showApplicantPanelists(User $user)
    {
        $user->load('applicant');
        $applicant = $user->applicant;
        $applicant->load('panelists');
        $applicant_panelists = $applicant->panelists;
        $final_panelist_question_score_array = [];
        $panelists_question_score_collection = [];
        $final_question_numbers = [];
        $total_panelist_score = 0;
        $panelist_count = 0;

        foreach($applicant_panelists as $applicant_panelist){
            $applicant_panelist->load('panelist');
            $cur_panelist = $applicant_panelist->panelist;
            $cur_panelist->load('user');
            $cur_user = $cur_panelist->user;
            $cur_user_name = $cur_user->name;
            $cur_user_surname = $cur_user->surname;
            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;

            $panelist_question_answers = PanelistQuestionAnswer::where('applicant_panelist_id', $applicant_panelist->id)->get();

            foreach ($panelist_question_answers as $panelist_question_answer){
                $question_id = $panelist_question_answer->question_id;
                $cur_question = Question::find($question_id);
                $cur_question_number = $cur_question->question_number;
                $cur_score = $panelist_question_answer->score;
                $panelist_applicant_score += $cur_score;
                $object = (object)['question_number' => $cur_question_number,
                    'question_score' => $cur_score];
                array_push($panelist_questions_scores, $object);
                array_push($panelists_question_score_collection, $object);
                array_push($final_question_numbers, $cur_question_number);
            }

            $object = (object)['name' => $cur_user_name, 'surname' => $cur_user_surname,
                'user_questions_scores' => $panelist_questions_scores, 'panelist_applicant_score' => $panelist_applicant_score,
                'applicant_panelist_id' => $applicant_panelist->id];
            array_push($final_panelist_question_score_array, $object);

            $total_panelist_score += $panelist_applicant_score;

            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;
        }

        foreach ($final_panelist_question_score_array as $pqsa){
            if(count($pqsa->user_questions_scores)){
                $panelist_count += 1;
            }
        }

        if($panelist_count > 0){
            $average_panelist_score = $total_panelist_score / $panelist_count;
        } else {
            $average_panelist_score = 0;
        }

        //Store total panel score and average applicant score to the database for further use
        DB::beginTransaction();
        $applicant->total_panel_score = $total_panelist_score;
        $applicant->average_panel_score = $average_panelist_score;
        $applicant->save();
        DB::commit();

        $question_scores_array = [];
        $question_score = 0;

        foreach (array_unique($final_question_numbers) as $q_number){
            foreach ($applicant_panelists as $applicant_panelist) {
                $applicant_panelist->load('panelistQuestionAnswers');
                $applicant_panelist_questions = $applicant_panelist->panelistQuestionAnswers;
                foreach ($applicant_panelist_questions as $applicant_panelist_question){
                    $question_id = $applicant_panelist_question->question_id;
                    $cur_question = Question::find($question_id);
                    if($cur_question->question_number == $q_number){
                        $question_score += $applicant_panelist_question->score;
                    }
                }
            }
            $object = (object)['question_number' => $q_number, 'question_score' => $question_score];
            array_push($question_scores_array, $object);
            $question_score = 0;
        }

        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/applicants/show-applicant-panelists', compact('user', 'applicant', 'final_panelist_question_score_array', 'question_scores_array', 'applicant',
                'total_panelist_score', 'average_panelist_score'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/applicants/show-applicant-panelists', compact('user', 'applicant', 'final_panelist_question_score_array', 'question_scores_array', 'applicant_panelist',
                'total_panelist_score', 'average_panelist_score'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/applicants/show-applicant-panelists',compact('user', 'applicant', 'final_panelist_question_score_array', 'question_scores_array', 'applicant_panelist',
                'total_panelist_score', 'average_panelist_score'));
        } else if ($logged_in_user->roles[0]->name == 'marketing') {
            return view('/users/marketing-dashboard/applicants/show-applicant-panelists',compact('user', 'applicant', 'final_panelist_question_score_array', 'question_scores_array', 'applicant_panelist',
                'total_panelist_score', 'average_panelist_score'));
        } else {
            return response("You are not authorized!!");
        }
    }

    public function showIctApplicantPanelists(Bootcamper $bootcamper){
        $bootcamper->load('user', 'panelists');
        $bootcamper_user = $bootcamper->user;
        $bootcamper_user->load('applicant');
        $applicant = $bootcamper_user->applicant;
        $applicant_panelists = $bootcamper->panelists;
        $final_panelist_question_score_array = [];
        $panelists_question_score_collection = [];
        $final_question_numbers = [];
        $total_panelist_score = 0;
        $panelist_count = 0;

        foreach($applicant_panelists as $applicant_panelist){
            $applicant_panelist->load('panelist');
            $cur_panelist = $applicant_panelist->panelist;
            $cur_panelist->load('user');
            $cur_user = $cur_panelist->user;
            $cur_user_name = $cur_user->name;
            $cur_user_surname = $cur_user->surname;
            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;

            $panelist_question_answers = IctPanelistQuestionAnswer::where('bootcamper_panelist_id', $applicant_panelist->id)->get();

            foreach ($panelist_question_answers as $panelist_question_answer){
                $question_id = $panelist_question_answer->question_id;
                $cur_question = Question::find($question_id);
                $cur_question_number = $cur_question->question_number;
                $cur_score = $panelist_question_answer->score;
                $panelist_applicant_score += $cur_score;
                $object = (object)['question_number' => $cur_question_number,
                    'question_score' => $cur_score];
                array_push($panelist_questions_scores, $object);
                array_push($panelists_question_score_collection, $object);
                array_push($final_question_numbers, $cur_question_number);
            }

            $object = (object)['name' => $cur_user_name, 'surname' => $cur_user_surname,
                'user_questions_scores' => $panelist_questions_scores, 'panelist_applicant_score' => $panelist_applicant_score,
                'applicant_panelist_id' => $applicant_panelist->id];
            array_push($final_panelist_question_score_array, $object);

            $total_panelist_score += $panelist_applicant_score;

            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;
        }

        foreach ($final_panelist_question_score_array as $pqsa){
            if(count($pqsa->user_questions_scores)){
                $panelist_count += 1;
            }
        }

        if($panelist_count > 0){
            $average_panelist_score = $total_panelist_score / $panelist_count;
        } else {
            $average_panelist_score = 0;
        }

        //Store total panel score and average bootcamp score to the database for further use
        DB::beginTransaction();
        $bootcamper->total_panel_score = $total_panelist_score;
        $bootcamper->average_panel_score = $average_panelist_score;
        $bootcamper->save();
        DB::commit();

        $question_scores_array = [];
        $question_score = 0;

        foreach (array_unique($final_question_numbers) as $q_number){
            foreach ($applicant_panelists as $applicant_panelist) {
                $applicant_panelist->load('panelistQuestionAnswers');
                $applicant_panelist_questions = $applicant_panelist->panelistQuestionAnswers;
                foreach ($applicant_panelist_questions as $applicant_panelist_question){
                    $question_id = $applicant_panelist_question->question_id;
                    $cur_question = Question::find($question_id);
                    if($cur_question->question_number == $q_number){
                        $question_score += $applicant_panelist_question->score;
                    }
                }
            }
            $object = (object)['question_number' => $q_number, 'question_score' => $question_score];
            array_push($question_scores_array, $object);
            $question_score = 0;
        }

        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/app-admin/bootcampers/show-applicant-panelists', compact('bootcamper_user',
                'bootcamper', 'final_panelist_question_score_array', 'question_scores_array', 'total_panelist_score', 'average_panelist_score'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/bootcampers/show-applicant-panelists', compact('bootcamper_user',
                'bootcamper', 'final_panelist_question_score_array', 'question_scores_array', 'total_panelist_score', 'average_panelist_score'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/bootcampers/show-applicant-panelists',compact('bootcamper_user',
                'bootcamper', 'final_panelist_question_score_array', 'question_scores_array', 'total_panelist_score', 'average_panelist_score'));
        } else {
            return response("You are not authorized!!");
        }
    }

    public function getApplicantPanelists(Applicant $applicant)
    {
        $applicant->load('panelists');
        $panelists = [];

        foreach ($applicant->panelists as $applicant_panelist) {
            $panelist_id = $applicant_panelist->panelist_id;
            $panelist = Panelist::find($panelist_id);
            $panelist_user_id = $panelist->user_id;
            $user = User::find($panelist_user_id);

            $object = (object)['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                'selection_date' => $applicant_panelist->panel_selection_date, 'panelist_id' => $panelist_id,
                'applicant_panelist_id' => $applicant_panelist->id];
            array_push($panelists, $object);
        }

        return Datatables::of($panelists)->addColumn('action', function ($panelist) {
            $sh = '/admin-show-panelist-score-sheet/' . $panelist->applicant_panelist_id;
            return '<a href=' . $sh . ' title="View Panelist Score Sheet"><i class="material-icons" style="color: darkred">assignment_turned_in</i></a>';
        })
            ->make(true);
    }

    public function getIctApplicantPanelists(Bootcamper $bootcamper){
        $bootcamper->load('panelists');
        $panelists = [];

        foreach ($bootcamper->panelists as $applicant_panelist) {
            $panelist_id = $applicant_panelist->panelist_id;
            $panelist = Panelist::find($panelist_id);
            $panelist_user_id = $panelist->user_id;
            $user = User::find($panelist_user_id);

            $object = (object)['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                'selection_date' => $applicant_panelist->panel_selection_date, 'panelist_id' => $panelist_id,
                'bootcamper_panelist_id' => $applicant_panelist->id];
            array_push($panelists, $object);
        }

        return Datatables::of($panelists)->addColumn('action', function ($panelist) {
            $sh = '/admin-show-ict-panelist-score-sheet/' . $panelist->bootcamper_panelist_id;
            return '<a href=' . $sh . ' title="View Panelist Score Sheet"><i class="material-icons" style="color: darkred">assignment_turned_in</i></a>';
        })
            ->make(true);
    }

    public function getDeclinedBootcamperPanelists(DeclinedBootcamper $declinedBootcamper){
        $declinedBootcamper->load('declined_bootcamper_panelists');
        $panelists = [];

        foreach ($declinedBootcamper->declined_bootcamper_panelists as $applicant_panelist) {
            $panelist_id = $applicant_panelist->panelist_id;
            $panelist = Panelist::find($panelist_id);
            $panelist_user_id = $panelist->user_id;
            $user = User::find($panelist_user_id);

            $object = (object)['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                'selection_date' => $applicant_panelist->panel_selection_date, 'panelist_id' => $panelist_id,
                'bootcamper_panelist_id' => $applicant_panelist->id];
            array_push($panelists, $object);
        }

        return Datatables::of($panelists)->addColumn('action', function ($panelist) {
            $sh = '/declined-bootcamper-panelist-score-sheet/' . $panelist->bootcamper_panelist_id;
            return '<a href=' . $sh . ' title="View Panelist Score Sheet"><i class="material-icons" style="color: darkred">assignment_turned_in</i></a>';
        })
            ->make(true);
    }

    public function adminShowPanelistScoreSheet(ApplicantPanelist $applicantPanelist){
        $applicantPanelist->load('panelistQuestionAnswers');
        $panelist_question_answers = [];
        $logged_in_user = Auth::user()->load('roles');
        $panelist = $applicantPanelist->panelist;
        $user_object = (object)['name' => $panelist->user->name, 'surname' => $panelist->user->surname];

        foreach($applicantPanelist->panelistQuestionAnswers as $pqa){
            $question_id = $pqa->question_id;
            $question = Question::find($question_id);
            $question->load('questionSubTexts');
            $question_sub_texts = [];

            if(isset($question->questionSubTexts)){
                foreach ($question->questionSubTexts as $questionSubText){
                    $question_sub_text = $questionSubText->question_sub_text;
                    array_push($question_sub_texts, $question_sub_text);
                }
            }

            $question_text = $question->question_text;
            $question_number = $question->question_number;
            $score = $pqa->score;
            $comment = $pqa->comment;

            $object = (object)['question_text' => $question_text, 'question_number' => $question_number, 'score' => $score,
                'comment' => $comment, 'question_sub_texts' => $question_sub_texts];
            array_push($panelist_question_answers, $object);
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/applicants/show-panelist-score-sheet', compact('panelist_question_answers', 'user_object'));
        } else if($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/applicants/show-panelist-score-sheet', compact('panelist_question_answers', 'user_object'));
        } else if($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/applicants/show-panelist-score-sheet', compact('panelist_question_answers', 'user_object'));
        } else {
            return response("You are not authorized!");
        }
    }

    public function adminShowIctPanelistScoreSheet(BootcamperPanelist $bootcamperPanelist){
        $bootcamperPanelist->load('panelistQuestionAnswers');
        $panelist_question_answers = [];
        $logged_in_user = Auth::user()->load('roles');
        $panelist = $bootcamperPanelist->panelist;
        $user_object = (object)['name' => $panelist->user->name, 'surname' => $panelist->user->surname];

        foreach($bootcamperPanelist->panelistQuestionAnswers as $pqa){
            $question_id = $pqa->question_id;
            $question = Question::find($question_id);
            $question->load('questionSubTexts');
            $question_sub_texts = [];

            if(isset($question->questionSubTexts)){
                foreach ($question->questionSubTexts as $questionSubText){
                    $question_sub_text = $questionSubText->question_sub_text;
                    array_push($question_sub_texts, $question_sub_text);
                }
            }

            $question_text = $question->question_text;
            $question_number = $question->question_number;
            $score = $pqa->score;
            $comment = $pqa->comment;

            $object = (object)['question_text' => $question_text, 'question_number' => $question_number, 'score' => $score,
                'comment' => $comment, 'question_sub_texts' => $question_sub_texts];
            array_push($panelist_question_answers, $object);
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/bootcampers/show-panelist-score-sheet', compact('panelist_question_answers', 'user_object'));
        } else if($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrators/bootcampers/show-panelist-score-sheet', compact('panelist_question_answers', 'user_object'));
        } else if($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/bootcampers/show-panelist-score-sheet', compact('panelist_question_answers', 'user_object'));
        } else {
            return response("You are not authorized!");
        }
    }

    //SHOW DECLINED BOOTCAMPER PANELIST SCORE SHEET
    public function adminShowDeclinedBootcamperPanelistScoreSheet(DeclinedBootcamperPanelist $declinedBootcamperPanelist){
        $declinedBootcamperPanelist->load('declined_bootcamper_panelist_question_answers','declined_bootcamper');
        $panelist_question_answers = [];
        $logged_in_user = Auth::user()->load('roles');
        $panelist = $declinedBootcamperPanelist->panelist;
        $declined_b = $declinedBootcamperPanelist->declined_bootcamper;
        $user_object = (object)['name' => $declined_b->name, 'surname' => $declined_b->surname];

        foreach($declinedBootcamperPanelist->declined_bootcamper_panelist_question_answers as $pqa){
            $question_id = $pqa->question_id;
            $question = Question::find($question_id);
            $question->load('questionSubTexts');
            $question_sub_texts = [];

            if(isset($question->questionSubTexts)){
                foreach ($question->questionSubTexts as $questionSubText){
                    $question_sub_text = $questionSubText->question_sub_text;
                    array_push($question_sub_texts, $question_sub_text);
                }
            }

            $question_text = $question->question_text;
            $question_number = $question->question_number;
            $score = $pqa->score;
            $comment = $pqa->comment;

            $object = (object)['question_text' => $question_text, 'question_number' => $question_number, 'score' => $score,
                'comment' => $comment, 'question_sub_texts' => $question_sub_texts];
            array_push($panelist_question_answers, $object);
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.app-admin.bootcampers.show-declined-bootcamper-score-sheet', compact('panelist_question_answers','user_object'));
        }
    }


    public function adminRemoveApplicantPanelistFromPanel(ApplicantPanelist $applicantPanelist){
        try{
            DB::beginTransaction();
            $applicantPanelist->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Successfully removed panelist.'], 200);
        } catch (\Exception $e){
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    public function adminAddIndustrialPanelistFromPanelAccessWindow(Request $request){
    $input = $request->all();
    $user_id = $input['user_id'];
    $panelist_role = Role::where('name', 'panelist')->first();
    $user = User::find($user_id);
    $user->load('applicant');
    $question_category_id = $input['question_category_id'];

    try{
        DB::beginTransaction();
        //See if the actual user exists
        $check_user = User::where('email', $input['email'])->first();

        //if user does exist
        if (isset($check_user)) {
            //load relationships to be used
            $check_user->load('roles');
            //boolean to see if the user has panelist role
            $check_role = false;

            //check if user has role panelist
            foreach ($check_user->roles as $user_role) {
                if ($user_role->name == 'panelist') {
                    $check_role = true;
                }
            }

            //if user has role panelist, do nothing, else attach role
            if ($check_role == false) {
                $check_user->roles()->attach($panelist_role->id);
            }

            //Variable to see if the panelist object with the same user id already exists
            $check_panelist = $check_user->panelist()->exists();;

            //Do if statement to see if the panelist with user id exists, else create
            if ($check_panelist == true) {
                $cur_panelist = $check_user->panelist;
                $check_panelist_applicants = $cur_panelist->applicants()->exists();

                if ($check_panelist_applicants == true) {
                    $cur_panelist_applicants = $cur_panelist->applicants;

                    //boolean to see if panelist has applicant
                    $has_applicant = false;

                    //loop through panelist applicants to see if the applicant id matches the current applicant id
                    foreach ($cur_panelist_applicants as $panelist_applicant) {
                        if ($panelist_applicant->applicant_id == $user->applicant->id) {
                            $has_applicant = true;
                        }
                    }

                    //if panelist does not have current applicant, create applicantpanelist object
                    if ($has_applicant == false) {
                        $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $cur_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                            'question_category_id' => $question_category_id]);
                    } else {
                        $get_object = DB::table('applicant_panelists')->where('applicant_id', $user->applicant->id)
                            ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                'panel_selection_time' => $input['panel_selection_time'],
                                'question_category_id' => $question_category_id]);
                    }
                } else {
                    $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $cur_panelist->id,
                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                        'question_category_id' => $question_category_id]);
                }
            } else {
                $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $create_panelist->id,
                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                    'question_category_id' => $question_category_id]);
            }
        } else {
            $create_user = User::create(['email' => $input['email'], 'password' => Hash::make('secret')]);
            $create_user->roles()->attach($panelist_role->id);
            $create_panelist = Panelist::create(['user_id' => $create_user->id]);
            $create_panelist_applicant = ApplicantPanelist::create(['applicant_id' => $user->applicant->id, 'panelist_id' => $create_panelist->id,
                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                'question_category_id' => $question_category_id]);
        }
        DB::commit();
        return response()->json(['message' => 'Successfully added panelist'], 200);
    } catch (\Exception $e){
        return response()->json(['message' => 'Something went wrong: '. $e->getMessage()], 400);
    }
}

    public function adminUpdatePanelInterviewDetails(Request $request, User $user){
        $input = $request->all();
        $user->load('applicant');
        $user_applicant = $user->applicant;
        $user_applicant->load('panelists');
        $applicant_panelists = $user_applicant->panelists;

        try{
            DB::beginTransaction();
            $user->applicant()->update(['contacted_via' => $input['contact'],
                'result_of_contact' => $input['contact_result']]);

            if(count($applicant_panelists) > 0){
                foreach ($applicant_panelists as $applicant_panelist){
                    $applicant_panelist->panel_selection_date = $input['date'];
                    $applicant_panelist->panel_selection_time = $input['time'];
                    $applicant_panelist->question_category_id = $input['question_category_id'];
                    $applicant_panelist->save();
                }
            }

            DB::commit();
            return response()->json(['message' => 'Updated successfully!']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: '.$e->getMessage()], 400);
        }
    }

    public function panelistEditScoresheet(ApplicantPanelist $applicantPanelist){
        $applicantPanelist->load('panelistQuestionAnswers');
        $question_answers_array = [];
        $logged_in_user = Auth::user()->load('roles');

        foreach($applicantPanelist->panelistQuestionAnswers as $pqa){
            $question_id = $pqa->question_id;
            $cur_question = Question::find($question_id);
            $question_text = $cur_question->question_text;
            $question_number = $cur_question->question_number;
            $score = $pqa->score;
            $comment = $pqa->comment;

            $object = (object)['question_number' => $question_number, 'question_text' => $question_text,
                'question_id' => $question_id, 'score' => $score, 'comment' => $comment];
            array_push($question_answers_array, $object);
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('/users/app-admin/applicants/edit-applicant-score-sheet', compact('question_answers_array', 'applicantPanelist'));
        } else if ($logged_in_user->roles[0]->name == 'administrator'){
            return view('/users/administrator/applicants/edit-applicant-score-sheet', compact('question_answers_array', 'applicantPanelist'));
        } else if ($logged_in_user->roles[0]->name == 'advisor'){
            return view('/users/advisor-dashboard/applicants/edit-applicant-score-sheet', compact('question_answers_array', 'applicantPanelist'));
        } else if ($logged_in_user->roles[0]->name == 'incubatee'){
            return view('/users/incubatee-dashboard/applicants/edit-applicant-score-sheet', compact('question_answers_array', 'applicantPanelist'));
        } else if ($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/applicants/edit-applicant-score-sheet', compact('question_answers_array', 'applicantPanelist'));
        } else if ($logged_in_user->roles[0]->name == 'mentor'){
            return view('/users/mentor-dashboard/applicants/edit-applicant-score-sheet', compact('question_answers_array', 'applicantPanelist'));
        } else if ($logged_in_user->roles[0]->name == 'panelist'){
            return view('/users/panelist/applicants/edit-applicant-score-sheet', compact('question_answers_array', 'applicantPanelist'));
        } else {
            return response("You are not authorized!!");
        }
    }

    public function panelistUpdateQuestionAnswerComments(Request $request)
    {
        $input = $request->all();
        $questions_answers_comments_array = json_decode($input['questions_answers_comments']);
        $applicant_panelist_id = $questions_answers_comments_array[0]->applicant_panelist_id;
        $applicant_panelist = ApplicantPanelist::find($applicant_panelist_id);
        $applicant_panelist->load('panelistQuestionAnswers');

        try {
            if(isset($applicant_panelist->panelistQuestionAnswers)){
                foreach ($questions_answers_comments_array as $item) {
                    $find_question_answer = DB::table('panelist_question_answers')->where('applicant_panelist_id', $applicant_panelist_id)
                        ->where('question_id', $item->question_id)->first();

                    if(isset($find_question_answer)){
                        $score = (integer)$item->answer_score;
                        DB::table('panelist_question_answers')->where('applicant_panelist_id', $applicant_panelist_id)
                            ->where('question_id', $item->question_id)->update(['score' => $score, 'comment' => $item->comment]);
                    } else {
                        $score = (integer)$item->answer_score;
                        $create_object = PanelistQuestionAnswer::create(['question_id' => $item->question_id, 'applicant_panelist_id' => $item->applicant_panelist_id,
                            'score' => $score, 'comment' => $item->comment]);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Updated Successfully!'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //========================================================================================================================

    public function getAllApplicants()
    {
        $applicants_count = User::whereHas('roles', function ($q) {
            $q->whereIn('display_name', ['Applicant']);
        })->count();
        $user = Auth::user()->load('roles');


        if ($user->roles[0]->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.applicant-report', compact('applicants_count'));
        } else {
            return response("You are not authorized!");
        }
    }

    public function destroyUsers($id)
    {
        DB::beginTransaction();
        $user = User::find($id);
        $user->load('mentor');
        try {
            foreach($user->mentor as $mentor_user){
                $mentor_user->load('questionAnswers','venture');
                $mentor_user->forceDelete();

            }
            if (isset($user)) {
                $user->delete();
            }
            DB::commit();
            return response()->json(['message' => 'User deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'User can not be deleted ' . $e->getMessage(), 'user' => $user->load('answers')], 500);
        }
    }

    public function destroy(Incubatee $incubatee)
    {
        $incubatee->load('user', 'ownership', 'uploads', 'testimonials', 'incubateeGallery', 'incubateeEmployee',
            'venture', 'events', 'incubateeDeregisters');

        try {
            DB::beginTransaction();

            if(isset($incubatee->uploads)){
                $incubatee->uploads()->forceDelete();
            }

            if(isset($incubatee->ownership)){
                $incubatee->ownership()->forceDelete();
            }

            if(isset($incubatee->testimonials)){
                foreach ($incubatee->testimonials as $testimonial){
                    $testimonial->forceDelete();
                }
            }

            if(isset($incubatee->incubateeGallery)){
                foreach($incubatee->incubateeGallery as $incubatee_gallery){
                    $incubatee_gallery->forceDelete();
                }
            }

            if(isset($incubatee->incubateeEmployee)){
                foreach ($incubatee->incubateeEmployee as $incubatee_employee){
                    $incubatee_employee->forceDelete();
                }
            }

            if(isset($incubatee->events)){
                foreach ($incubatee->events as $incubatee_event){
                    $incubatee_event->forceDelete();
                }
            }

            if(isset($incubatee->incubateeDeregisters)){
                foreach ($incubatee->incubateeDeregisters as $incubatee_deregister){
                    $incubatee_deregister->forceDelete();
                }
            }

            if(isset($incubatee->venture)){
                $incubatee->venture()->forceDelete();
            }

            if (isset($incubatee)) {
                $incubatee->forceDelete();
            }

            if(isset($incubatee->user)){
                $incubatee->user()->forceDelete();
            }

            DB::commit();
            return redirect('incubatees');

        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    //==================================================================//
    //User Event Functions
    public function storeUserEventViaPublicEventPage(Request $request)
    {
        $input = $request->all();
        $event_id = $input['event_id'];
        $user_id = $input['user_id'];
        $cur_user = User::find($user_id);
        $cur_event = Event::find($event_id);
        $date = Carbon::now();

        if (isset($cur_user) and isset($cur_event)) {
            $cur_user->load('events');

            foreach ($cur_user->events as $user_event) {
                if ($user_event->event_id == $event_id) {
                    return response()->json(['message' => 'You are already registered for this event.']);
                }
            }

            try {
                DB::beginTransaction();

                $event_user = EventInternalUser::create(['user_id' => $user_id, 'event_id' => $event_id,
                    'registered' => true, 'date_time_registered' => $date, 'found_out_via' => $input['found_out_via']]);

                $event_user->save();

                DB::commit();

                return response()->json(['message' => 'Event now added to your registered list.']);

            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
            }
        } else {
            return response()->json(['message' => 'The system cannot pick up this event.']);
        }
    }

    public function userEvents(User $user)
    {
        $events = Event::with('EventVenue')->orderBy('start', 'desc')->get();
        $user_events = [];
        $user_event_start_time = "";
        $all_events_array = [];
        $event_start_time = "";

        $logged_in_user = Auth::user()->load('roles');


        if ($logged_in_user->roles[0]->name == 'app-admin') {
            $user->load('events');
            foreach ($user->events as $user_event) {
                $event_id = $user_event->event_id;
                $cur_event = Event::find($event_id);

                if(isset($cur_event->start_time)){
                    for ($i = 0; $i < count_chars($cur_event->start_time); $i++) {
                        $user_event_start_time = $user_event_start_time . $cur_event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                if (isset($cur_event)) {
                    $object = (object)['user_event_registered' => $user_event, 'user_events_by_event_id' => $cur_event, 'user_event_start_time' => $user_event_start_time];
                    $user_event_start_time = "";
                    array_push($user_events, $object);
                }
            }

            foreach ($events as $event) {
                if(isset($event->start_time)){
                    for ($i = 0; $i < count_chars($event->start_time); $i++) {
                        $event_start_time = $event_start_time . $event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                $object = (object)['event_start_time' => $event_start_time, 'event' => $event];
                $event_start_time = "";
                array_push($all_events_array, $object);
            }
            return view('/users/app-admin/events', compact('user', 'all_events_array', 'user_events'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator') {
            $user->load('events');
            foreach ($user->events as $user_event) {
                $event_id = $user_event->event_id;
                $cur_event = Event::find($event_id);

                if(isset($cur_event->start_time)){
                    for ($i = 0; $i < count_chars($cur_event->start_time); $i++) {
                        $user_event_start_time = $user_event_start_time . $cur_event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                if (isset($cur_event)) {
                    $object = (object)['user_event_registered' => $user_event, 'user_events_by_event_id' => $cur_event, 'user_event_start_time' => $user_event_start_time];
                    $user_event_start_time = "";
                    array_push($user_events, $object);
                }
            }

            foreach ($events as $event) {
                if (isset($event->start_time)) {
                    for ($i = 0; $i < count_chars($event->start_time); $i++) {
                        $event_start_time = $event_start_time . $event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }


                $object = (object)['event_start_time' => $event_start_time, 'event' => $event];
                $event_start_time = "";
                array_push($all_events_array, $object);
            }
            return view('/users/administrator/events', compact('user', 'all_events_array', 'user_events'));
        } elseif ($logged_in_user->roles[0]->name == 'marketing') {
            $user->load('events');
            foreach ($user->events as $user_event) {
                $event_id = $user_event->event_id;
                $cur_event = Event::find($event_id);

                if(isset($cur_event->start_time)){
                    for ($i = 0; $i < count_chars($cur_event->start_time); $i++) {
                        $user_event_start_time = $user_event_start_time . $cur_event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                if (isset($cur_event)) {
                    $object = (object)['user_event_registered' => $user_event, 'user_events_by_event_id' => $cur_event, 'user_event_start_time' => $user_event_start_time];
                    $user_event_start_time = "";
                    array_push($user_events, $object);
                }
            }

            foreach ($events as $event) {
                if (isset($event->start_time)) {
                    for ($i = 0; $i < count_chars($event->start_time); $i++) {
                        $event_start_time = $event_start_time . $event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                $object = (object)['event_start_time' => $event_start_time, 'event' => $event];
                $event_start_time = "";
                array_push($all_events_array, $object);
            }
            return view('/users/marketing-dashboard/events', compact('user', 'all_events_array', 'user_events'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor') {
            $user->load('events');
            foreach ($user->events as $user_event) {
                $event_id = $user_event->event_id;
                $cur_event = Event::find($event_id);

                if(isset($cur_event->start_time)){
                    for ($i = 0; $i < count_chars($cur_event->start_time); $i++) {
                        $user_event_start_time = $user_event_start_time . $cur_event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                if (isset($cur_event)) {
                    $object = (object)['user_event_registered' => $user_event, 'user_events_by_event_id' => $cur_event, 'user_event_start_time' => $user_event_start_time];
                    $user_event_start_time = "";
                    array_push($user_events, $object);
                }
            }

            foreach ($events as $event) {
                if (isset($event->start_time)) {
                    for ($i = 0; $i < count_chars($event->start_time); $i++) {
                        $event_start_time = $event_start_time . $event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                $object = (object)['event_start_time' => $event_start_time, 'event' => $event];
                $event_start_time = "";
                array_push($all_events_array, $object);
            }
            return view('/users/advisor-dashboard/events', compact('user', 'all_events_array', 'user_events'));
        } elseif ($logged_in_user->roles[0]->name == 'mentor') {
            $user->load('events');
            foreach ($user->events as $user_event) {
                $event_id = $user_event->event_id;
                $cur_event = Event::find($event_id);

                if(isset($cur_event->start_time)){
                    for ($i = 0; $i < count_chars($cur_event->start_time); $i++) {
                        $user_event_start_time = $user_event_start_time . $cur_event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                if (isset($cur_event)) {
                    $object = (object)['user_event_registered' => $user_event, 'user_events_by_event_id' => $cur_event, 'user_event_start_time' => $user_event_start_time];
                    $user_event_start_time = "";
                    array_push($user_events, $object);
                }
            }

            foreach ($events as $event) {
                if (isset($event->start_time)) {
                    for ($i = 0; $i < count_chars($event->start_time); $i++) {
                        $event_start_time = $event_start_time . $event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                $object = (object)['event_start_time' => $event_start_time, 'event' => $event];
                $event_start_time = "";
                array_push($all_events_array, $object);
            }
            return view('/users/mentor-dashboard/events', compact('user', 'all_events_array', 'user_events'));
        } elseif ($logged_in_user->roles[0]->name == 'tenant') {
            $user->load('events');
            foreach ($user->events as $user_event) {
                $event_id = $user_event->event_id;
                $cur_event = Event::find($event_id);

                if(isset($cur_event->start_time)){
                    for ($i = 0; $i < count_chars($cur_event->start_time); $i++) {
                        $user_event_start_time = $user_event_start_time . $cur_event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                if (isset($cur_event)) {
                    $object = (object)['user_event_registered' => $user_event, 'user_events_by_event_id' => $cur_event, 'user_event_start_time' => $user_event_start_time];
                    $user_event_start_time = "";
                    array_push($user_events, $object);
                }
            }

            foreach ($events as $event) {
                if (isset($event->start_time)) {
                    for ($i = 0; $i < count_chars($event->start_time); $i++) {
                        $event_start_time = $event_start_time . $event->start_time[$i];
                        if ($i == 4) {
                            break;
                        }
                    }
                }

                $object = (object)['event_start_time' => $event_start_time, 'event' => $event];
                $event_start_time = "";
                array_push($all_events_array, $object);
            }
            return view('/users/tenant-dashboard/events', compact('user', 'all_events_array', 'user_events'));
        }
    }

    public function deregisterUser(Request $request)
    {
        $input = $request->all();
        $user_id = $input['user_id'];
        $event_id = $input['event_id'];
        $cur_user = User::find($user_id);
        $cur_event = Event::find($event_id);
        $cur_user->load('events');
        $date = Carbon::now();

        try {
            if (isset($cur_user)) {

                DB::beginTransaction();

                foreach ($cur_user->events as $user_events) {
                    if ($user_events->event_id == $event_id) {
                        $create_user_deregister = UserEventDeregister::create(['event_id' => $event_id, 'user_id' => $cur_user->id,
                            'de_registered' => true, 'de_register_reason' => $input['deregister_reason'], 'date_time_de_register' => $date,
                            'event_name' => $cur_event->title]);

                        $user_events->forceDelete();

                        DB::commit();
                        return response()->json(['message' => 'You are now deregistered for that event.']);
                    }
                }
            } else {
                return response()->json(['message' => 'The system could not pick up the visitor.']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    public function storeUserEventViaUserEventPage(Request $request)
    {
        $input = $request->all();
        $event_id = $input['event_id'];
        $user_id = $input['user_id'];
        $cur_user = User::find($user_id);
        $cur_event = Event::find($event_id);
        $date = Carbon::now();

        if (isset($cur_user) and isset($cur_event)) {
            $cur_user->load('events');

            foreach ($cur_user->events as $user_event) {
                if ($user_event->event_id == $event_id) {
                    return response()->json(['message' => 'You are already registered for this event.']);
                }
            }

            try {
                DB::beginTransaction();

                $event_user = EventInternalUser::create(['user_id' => $user_id, 'event_id' => $event_id,
                    'registered' => true, 'date_time_registered' => $date, 'found_out_via' => $input['found_out_via']]);

                $event_user->save();

                DB::commit();

                return response()->json(['message' => 'Event now added to your registered list.']);

            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
            }
        } else {
            return response()->json(['message' => 'The system cannot pick up this event.']);
        }
    }

    public function updateUserEvent(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();
        $user_id = $input['user_id'];
        $cur_user = User::find($user_id);
        $cur_user->load('events');
        $logged_in_user = User::find(Auth::user()->id);

        if ($logged_in_user != null) {
            $logged_in_user->load('roles');

            if ($logged_in_user->roles[0]->name == 'clerk') {
                DB::beginTransaction();
                try {
                    if ($request->has('event_id')) {
                        $event_id = $input['event_id'];
                        $check_event = Event::find($event_id);
                        $user_events = EventInternalUser::where('user_id', $user_id)->get();

                        $bool_found = false;
                        foreach ($user_events as $user_event) {
                            if ($user_event->event_id == $event_id) {
                                $bool_found = true;
                                $user_event->attended = true;
                                $user_event->save();
                                DB::commit();
                                return response()->json(['visitor' => $cur_user, 'message' => 'Enjoy your event / workshop!'], 200);
                            }
                        }

                        if (!$bool_found) {
                            $create_user_event = EventInternalUser::create(['event_id' => $event_id, 'user_id' => $user_id,
                                'attended' => true, 'registered' => true, 'de_registered' => false, 'date_time_registered' => $date]);
                            DB::commit();
                            return response()->json(['user' => $cur_user, 'message' => 'Enjoy your event / workshop!'], 200);
                        }

                    } else {
                        return response()->json(['message', 'Are you sure you chose and event?']);
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 500);
                }
            } else {
                return response('You are not authorized for this action.');
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }


    //EVENT INVITE EMAILER FOR ALL THE USERS
    public function eventInviteEmailerIndex(){
        $users = User::all();
        $visitors = Visitor::all();
        $companiesEmployees = CompanyEmployee::all();
        $allUsersArray = [];

        //ALL USERS
        foreach ($users as $user) {
            $object = (object)['name' => isset($user->name) ? $user->name : "No name",
                'surname' => isset($user->surname) ? $user->surname : "No surname",
                'email' => isset($user->email) ? $user->email : "No email",
                'contact_number' => isset($user->contact_number) ? $user->contact_number : "No cell",
                'id' => $user->id];
            array_push($allUsersArray, $object);
        }

        //ALL VISITORS
        foreach ($visitors as $visitor) {
            $object = (object)['first_name' => isset($visitor->first_name) ? $visitor->first_name : "No name",
                'last_name' => isset($visitor->last_name) ? $visitor->last_name : "No surname",
                'email' => isset($visitor->email) ? $visitor->email : "No email",
                'cell_number' => isset($visitor->cell_number) ? $visitor->cell_number : "No cell",
                'id' => $visitor->id];
            array_push($allUsersArray, $object);
        }

        //ALL COMPANY EMPLOYEES
        foreach ($companiesEmployees as $companiesEmployee) {
            $object = (object)['name' => isset($companiesEmployee->name) ? $companiesEmployee->name : "No name",
                'surname' => isset($companiesEmployee->surname) ? $companiesEmployee->surname : "No surname",
                'email' => isset($companiesEmployee->email) ? $companiesEmployee->email : "No email",
                'contact_number' => isset($companiesEmployee->contact_number) ? $companiesEmployee->contact_number : "No cell",
                'id' => $companiesEmployee->id];
            array_push($allUsersArray, $object);
        }

        return view('users.app-admin.event-emailer.event-send-email',compact('users','visitors','companiesEmployees',
        'allUsersArray'));
    }


    //SEND EMAIL TO ALL USERS
    public function sendEmailToAllUsers(Request $request)
    {
        $input = $request->all();
        $selected_users = json_decode($input['selected_users']);

        try {
            DB::beginTransaction();

            foreach ($selected_users as $selected_users_in_db) {
                if ($selected_users_in_db) {
                    $users = User::find($selected_users_in_db);

                    $data = array(
                        'name' => $users->name,
                        'email' => $users->email,
                    );
                    $email_field = $users->email;
                    Mail::send('emails.send-email-to-all-users', $data, function ($message) use ($email_field) {
                        $message->to($email_field)
                            ->subject('Event invite');
                        $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                    });

                    return response()->json(['message' => 'Email sent']);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully sent email.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //Get covid forms
    public function covidFormsIndex()
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.covid-forms.covid-forms-index');
        } else if ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.covid-forms.covid-forms-index');
        } else if ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.covid-forms.covid-forms-index');
        }
    }
    public function getCovidForms()
    {
        $forms = SafetyForm::all();


        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($forms)->addColumn('action', function ($question) {
                $del = $question->id;
                return '<a id=' . $del . ' onclick="confirm_delete_form(this)" title="Delete Form" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif($user->roles[0]->name == 'administrator'){
            return Datatables::of($forms)->addColumn('action', function ($question) {
                $del = $question->id;
                return '<a id=' . $del . ' onclick="confirm_delete_form(this)" title="Delete Form" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif($user->roles[0]->name == 'advisor'){
            return Datatables::of($forms)->addColumn('action', function ($question) {
                $del = $question->id;
                return '<a id=' . $del . ' onclick="confirm_delete_form(this)" title="Delete Form" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
        else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //Delete Role
    public function adminDeleteForm(SafetyForm $safetyForm){

        try {
            DB::beginTransaction();
            $safetyForm->forceDelete();
            DB::commit();

            return response()->json(['message' => 'Successfully deleted form.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 400);
        }

    }

    public function qrCodeGenerator(){
        return view('users.QR-code.qr-code-generator');
    }

    //UPLOAD PROPELLA VIDEOS
    public function uploadVideos(){
        $logged_in_user = Auth::user()->load('roles');
        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.videos.upload-videos');
        } elseif($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.videos.upload-videos');
        } elseif($logged_in_user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.videos.upload-videos');
        } elseif($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.videos.upload-videos');
        } else {
            return response("You are not authorized!");
        }

    }

    public function storeVideo(Request $request){

        $input = $request->all();
        DB::beginTransaction();

        try {

            $video = Video::create(['video' => $input['video'], 'video_title' => $input['video_title'],'date' => $input['date']]);


            DB::commit();
            return response()->json(['message' => 'Video added successfully.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'User could not be saved  ' . $e->getMessage()], 400);
        }

    }

    public function videoIndex(){
        $logged_in_user = Auth::user()->load('roles');
        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.videos.video-index');
        } elseif($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.videos.video-index');
        } elseif($logged_in_user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.videos.video-index');
        } elseif($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.videos.video-index');
        } else {
            return response("You are not authorized!");
        }

    }

    public function getVideos()
    {
        $videos = Video::all();

        return Datatables::of($videos)->addColumn('action', function ($video) {
            $re = '/edit-video/' . $video->id;
            $del = '/delete-video/' . $video->id;
            return '<a href=' . $re . ' title="Edit user" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete_users(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
        })
            ->ToJson();

    }

    //UPLOAD PROPELLA VIDEOS
    public function editVideos(Video $video){
        $logged_in_user = Auth::user()->load('roles');
        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.videos.edit-video',compact('video'));
        } elseif($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.videos.edit-video', compact('video'));
        } elseif($logged_in_user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.videos.edit-video', compact('video'));
        } elseif($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.videos.edit-video', compact('video'));
        } else {
            return response("You are not authorized!");
        }

    }

    public function updateVideo(Request $request, Video $video)
    {
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'administrator') {
            try {
                $video->update(['video' => $input['video'],'video_title' => $input['video_title'],'date' => $input['date']]);

                $video->save();

                DB::commit();
                return response()->json(['message' => 'Video updated successfully.', 'video' => $video], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Video could not be saved at the moment ' . $e->getMessage(),'video'=>$video], 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function deleteVideo(Video $video){
        try{
            $video->forceDelete();
            return response()->json(['message' => 'Deleted video'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Function to store Shadowboard comments

    public function storeMentorShadowBoardComments(Request $request,MentorVentureShadowboard $mentorVentureShadowboard)
    {
        $input = $request->all();
        $mentorVentureShadowboard->load('mentor', 'venture', 'mentorComments');
        $mentor = $mentorVentureShadowboard->mentor;
        $venture = $mentorVentureShadowboard->venture;

        try {
            DB::beginTransaction();

            $create_question_answer = ShadowBoardComment::create(['mentor_id' => $mentor->id,
                'comment_section' => $input['comment_section'], 'mentor_venture_id' => $mentorVentureShadowboard->id,
                'venture_id'=>$venture->id]);
            DB::commit();

            return response()->json(['message' => 'Answers Saved successfully.', 'mentor' => $mentorVentureShadowboard], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Create presentation
    public function createPresentation(){
        $user = Auth::user()->load('roles','incubatee');
        $presentations  = Presentation::all();
        if($user->roles[0]->name=='app-admin') {
            return view('users.app-admin.Presentations.create-presentations', compact('presentations'));
        }elseif($user->roles[0]->name=='administrator') {
            return view('users.administrators.Presentations.create-presentations', compact('presentations'));
        } elseif($user->roles[0]->name=='advisor') {
            return view('users.advisor-dashboard.Presentations.create-presentations', compact('presentations'));
        }
        else {
            return response("You are not authorized!");
        }
    }

    //Store presentations
    public function storePresentation(Request $request){
        DB::beginTransaction();
        $input = $request->all();
        // validation
        $this->validate($request, [
            'handout' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf,docx,xlsx|max:2048',
            'presentation' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf,docx,xlsx|max:2048',
        ]);
        try{
            $presentation = new Presentation();
            if ($request->hasFile('handout')) {
                $handout_path = $request->file('handout')->store('presentation');
                $presentation->handout = $handout_path;
            }

            if ($request->hasFile('presentation')) {
                $presentation_path = $request->file('presentation')->store('presentation');
                $presentation->presentation = $presentation_path;
            }


            $presentation->title = $request->title;
            $presentation->status = $request->status;
            $presentation->video = $request->video;

            $presentation->save();


            DB::commit();
            return response()->json(['message'=>'Presentation added successfully']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }
    }

    //Update presentations
    public function updatePresentation(Request $request, Presentation $presentation){
        DB::beginTransaction();
        $input = $request->all();
        $this->validate($request, [
            'handout' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf,docx,xlsx|max:2048',
            'presentation' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf,docx,xlsx|max:2048',
        ]);
        try{
            $presentation->update(['title'=>$input['title'],'video'=>$input['video']]);

            $presentation->save();


            DB::commit();
            return response()->json(['message'=>'Presentation updated successfully']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }
    }


    //View presentations
    public function viewPresentationUpload($id){
        $user = Auth::user()->load('roles','incubatee');
        $date = Presentation::find($id);
        $date->load('handouts_uploads','presentation_uploads');

        if($user->roles[0]->name=='app-admin') {
            return view('users.app-admin.Presentations.view-presentation', compact('date'));
        }elseif($user->roles[0]->name=='administrator') {
            return view('users.administrators.Presentations.view-presentation', compact('date'));
        }elseif($user->roles[0]->name=='advisor') {
            return view('users.advisor-dashboard.Presentations.view-presentation', compact('date'));
        } else {
            return response("You are not authorized!");
        }
    }

    //Index presentation
    public function indexPresentation()
    {
        $user = Auth::user()->load('roles', 'incubatee');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Presentations.presentation-index');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.Presentations.presentation-index');
        } elseif($user->roles[0]->name=='advisor') {
            return view('users.advisor-dashboard.Presentations.presentation-index');
        } else {
            return response("You are not authorized!");
        }
    }

    //Get all presentations
    public function getPresentation(){

        $presentation = Presentation::all();
        $user = Auth::user()->load('roles');

        if($user->roles[0]->name=='app-admin' or $user->roles[0]->name == 'administrator' or $user->roles[0]->name=='advisor') {
            return Datatables::of($presentation)->addColumn('action', function ($presentation) {
                $show = '/view-presentation/' . $presentation->id;
                $assign= '/assign-incubatees-to-presentation/' . $presentation->id;
                $edit = '/edit-workshop-presentation/' . $presentation->id;
                $del = '/destroy-presentation/' . $presentation->id;
                return '<a href=' . $edit . ' title="Edit" style="color:green!important;"><i class="material-icons">create</i></a>
                <a href=' . $show . ' title="View" style="color:orange!important;"><i class="material-icons">remove_red_eye</i></a>
                <a href=' . $assign . ' title="Assign" style="color:purple!important;"><i class="material-icons">person_outline</i></a>
                <a href=' . $del . ' onclick="confirm_delete_presentation(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    //Assign presentation to incubatees
    public function assignIncubateesToPresentation(Presentation $presentation){
        $incubatees = Incubatee::with('uploads', 'user','venture','venture_category')->get();
        $presentation->load('handouts_uploads','presentation_uploads');

        $incubatee_array = [];

        foreach($incubatees as $incubatee){
            $object = (object)[
                'id' => $incubatee->id,
                'name' => isset($incubatee->user->name) ? $incubatee->user->name : 'No name set',
                'surname' => isset($incubatee->user->surname) ? $incubatee->user->surname : 'No surname set',
                'email' => isset($incubatee->user->email) ? $incubatee->user->email : 'No email set',
                'contact_number' => isset($incubatee->user->contact_number) ? $incubatee->user->contact_number : 'No numbers set',
                'category_name' => isset($incubatee->venture_category) ? $incubatee->venture_category->category_name : 'No category',
                'stage' => isset($incubatee->venture) ? $incubatee->venture->stage : 'No stage',
                'status' => isset($incubatee->venture) ? $incubatee->venture->status : 'No status',
                'cohort' => isset($incubatee->venture) ? $incubatee->venture->cohort : 'No cohort',
                'date_joined' => $incubatee->created_at->toDateString(),
                'incubatee_popi_act_agreement'=> isset($incubatee->incubatee_popi_act_agreement) ? $incubatee->incubatee_popi_act_agreement : 'No'
            ];
            array_push($incubatee_array, $object);
        }

        $user = Auth::user()->load('roles', 'incubatee');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Presentations.assign-incubatees', compact('presentation', 'incubatee_array'));
        }elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.Presentations.assign-incubatees', compact('presentation','incubatee_array'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.Presentations.assign-incubatees', compact('presentation','incubatee_array'));
        } else {
                return response("You are not authorized!");
            }
    }

    //Edit wordkshop presentation
    public function editWorkshopPresentation(Presentation $presentation){
        $user = Auth::user()->load('roles', 'incubatee');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Presentations.edit-workshop-presentation', compact('presentation'));
        }elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.Presentations.edit-workshop-presentation', compact('presentation'));
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.Presentations..edit-workshop-presentation', compact('presentation'));
        } else {
            return response("You are not authorized!");
        }
    }

    //Get assigned presentations
    public function getAssignIncubateesToPresentation(Presentation $presentation){
        $incubatees = Incubatee::with('uploads', 'user','venture','venture_category')->get();


        $incubatee_array = [];

        foreach($incubatees as $incubatee){
            $object = (object)[
                'id' => $incubatee->id,
                'name' => isset($incubatee->user->name) ? $incubatee->user->name : 'No name set',
                'surname' => isset($incubatee->user->surname) ? $incubatee->user->surname : 'No surname set',
                'email' => isset($incubatee->user->email) ? $incubatee->user->email : 'No email set',
                'contact_number' => isset($incubatee->user->contact_number) ? $incubatee->user->contact_number : 'No numbers set',
                'category_name' => isset($incubatee->venture_category) ? $incubatee->venture_category->category_name : 'No category',
                'stage' => isset($incubatee->venture) ? $incubatee->venture->stage : 'No stage',
                'status' => isset($incubatee->venture) ? $incubatee->venture->status : 'No status',
                'cohort' => isset($incubatee->venture) ? $incubatee->venture->cohort : 'No cohort',
                'date_joined' => $incubatee->created_at->toDateString(),
                'incubatee_popi_act_agreement'=> isset($incubatee->incubatee_popi_act_agreement) ? $incubatee->incubatee_popi_act_agreement : 'No'
            ];
            array_push($incubatee_array, $object);
        }

        return Datatables::of($incubatee_array)->addColumn('action', function ($presentation) {

        })
            ->make(true);


    }

    //Store assigned workshops of incubatees
    public function assignPresentationToIncubatee(Request $request)
    {
        $input = $request->all();
        $selected_ventures = json_decode($input['selected_ventures']);
        $presentation_id = $input['presentation_id'];
        $ven = Presentation::find($presentation_id);

        try {
            DB::beginTransaction();

            foreach ($selected_ventures as $selected_venture) {
                $incubatee_id = $selected_venture;
                $venture = Incubatee::find($incubatee_id);

                $inc = IncubateePresentation::create(['incubatee_id'=>$venture->id,'presentation_id'=>$ven->id,
                    'start_date' => $input['start_date'],'end_date' => $input['end_date']]);

                $inc->save();
            }

            DB::commit();
            return response()->json(['message' => 'Successfully assigned incubatee(s).'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //Incubatee workshop index
    public function incubateeWorkshops(){
        $user = Auth::user()->load('roles','incubatee');
        $incubatee = $user->incubatee;
        $incubatee->load('presentations');
        $presentation = $incubatee->presentations;
        $presentation->load('presentation');
        $presentation_array = [];


        foreach($presentation as $present){
            $present->load('presentation');

            $object = (object)[
                'id' => $present->id,
                'title' => isset($present->title) ? $present->title : 'No title',
                'handout' => isset($present->handout) ? $present->handout : 'No handout',
                'presentation' => isset($present->presentation) ? $present->presentation : 'No presentation',
                'audio' => isset($present->audio) ? $present->audio : 'No audio',
            ];
            array_push($presentation_array, $object);
        }


        if($user->roles[0]->name == 'incubatee'){
            return view('users.incubatee-dashboard.presentations.incubatee-workshops',compact('user','presentation_array','presentation'));
        }
    }

    //Get incubatee workshop presentations
    public function getIncubateePresentation(){

        $user = Auth::user()->load('roles','incubatee');
        $incubatee = $user->incubatee;
        $incubatee->load('presentations');
        $presentation = $incubatee->presentations;
        $presentation->load('presentation');
        $presentation_array = [];
        $date = Carbon::now()->toDateString();
        foreach ($presentation as $present) {
            $present->load('presentation');
            if($present->end_date <= $date) {
            }else{
                $object = (object)[
                    'id' => $present->id,
                    'title' => isset($present->presentation->title) ? $present->presentation->title : 'No title',
                    'date' => $present->created_at->toDateString(),
                ];
                array_push($presentation_array, $object);
            }
        }


        if($user->roles[0]->name=='incubatee') {
            return Datatables::of($presentation_array)->addColumn('action', function ($presentation) {
                $show = '/view-inc-presentation/' . $presentation->id;
                return '<a href=' . $show . ' title="View presentation" style="color:green!important;"><i class="material-icons">remove_red_eye</i></a';
            })
                ->make(true);
        }
    }

    //Incubatee view workshop presentations
    public function viewIncubateePresentationUpload($id){
        $user = Auth::user()->load('roles','incubatee');

        $date = IncubateePresentation::find($id);
        $date->load('presentation');
        $presentations = $date->presentation;
        $presentations->load('handouts_uploads','presentation_uploads');

        if($user->roles[0]->name == 'incubatee'){
            return view('users.incubatee-dashboard.presentations.view-presentation', compact('date','presentations'));
        } else {
            return response("You are not authorized!");
        }
    }

    //GetIncubatees
    public function getICTAndIndIncubatees(){
        $incubatees = Incubatee::with( 'user','venture')->get();
        $incubatee_array = [];

        foreach($incubatees as $incubatee){
            $object = (object)[
                'id' => $incubatee->id,
                'name' => isset($incubatee->user->name) ? $incubatee->user->name : 'No name set',
                'surname' => isset($incubatee->user->surname) ? $incubatee->user->surname : 'No surname set',
                'email' => isset($incubatee->user->email) ? $incubatee->user->email : 'No email set',
                'contact_number' => isset($incubatee->user->contact_number) ? $incubatee->user->contact_number : 'No numbers set',
                'category_name' => isset($incubatee->venture_category) ? $incubatee->venture_category->category_name : 'No category',
                'stage' => isset($incubatee->venture) ? $incubatee->venture->stage : 'No stage',
                'cohort' => isset($incubatee->venture) ? $incubatee->venture->cohort : 'No cohort',
                'status' => isset($incubatee->venture) ? $incubatee->venture->status : 'No status',
                'date_joined' => $incubatee->created_at->toDateString(),
                'incubatee_popi_act_agreement'=> isset($incubatee->incubatee_popi_act_agreement) ? $incubatee->incubatee_popi_act_agreement : 'No'
            ];
            array_push($incubatee_array, $object);
        }

        return Datatables::of($incubatee_array)->addColumn('action', function ($incubatee) {
            return '<input type="checkbox" class="check" id='.$incubatee->id.' value='.$incubatee->id.' name='.$incubatee->email.' />';
        })
            ->make(true);
    }

    //Delete presentation
    public function destroyPresentation($id){
        DB::beginTransaction();
        $presentation = Presentation::find($id);
        $user = Auth::user()->load('roles', 'incubatee');
        try {
            if (isset($presentation)) {
                $presentation->delete();
            }
            DB::commit();

            if ($user->roles[0]->name == 'app-admin') {
                return view('users.app-admin.Presentations.presentation-index');
            } elseif ($user->roles[0]->name == 'administrator') {
                return view('users.administrators.Presentations.presentation-index');
            } elseif($user->roles[0]->name=='advisor') {
                return view('users.advisor-dashboard.Presentations.presentation-index');
            } else {
                return response("You are not authorized!");
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Presentation can not be deleted '], 500);
        }
    }

    //Upload multiple handout
    public function multipleHandoutUpload(Request $request, Presentation $presentation)
    {
        $input = $request->all();
        $this->validate($request, [
            'handout_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf,docx,xlsx|max:2048',
        ]);
        try{
            DB::beginTransaction();
            if ($request->hasFile('handout_url')) {
                $handout_path = $request->file('handout_url')->store('presentation');
                $handouts = HandoutUpload::create(['presentation_id' => $presentation->id,
                    'handout_url'=>$handout_path]);
            }

            DB::commit();
            return response()->json(['message' => 'Handout uploaded.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    //Upload multiple presentation
    public function multiplePresentationUpload(Request $request, Presentation $presentation)
    {
        $input = $request->all();
        $this->validate($request, [
            'presentation_url' => 'mimes:png,jpg,jiff,jfif,jpeg,csv,txt,xlx,xls,pdf,docx,xlsx|max:2048',
        ]);
        try{
            DB::beginTransaction();
            if ($request->hasFile('presentation_url')) {
                $presentation_url_path = $request->file('presentation_url')->store('presentation');
                $handouts = PresentationUpload::create(['presentation_id' => $presentation->id,
                    'presentation_url'=>$presentation_url_path]);
            }

            DB::commit();
            return response()->json(['message' => 'Presentation uploaded.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    public function deletePresentationHandout($id){
        $handout = HandoutUpload::find($id);
        $presentation = PresentationUpload::find($id);

        try{
            if(isset($handout)){
                $handout->forceDelete();
                return response()->json(['message' => 'success'], 200);
            }
            if(isset($presentation)){
                $presentation->forceDelete();
                return response()->json(['message' => 'success'], 200);
            }
        } catch (\Exception $e){
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    //Donation index
    public function donationIndex(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.donation.donation-index');
        } elseif($user->roles[0]->name=='advisor') {
            return view('users.advisor-dashboard.donation.donation-index');
        }
    }

    public function getDonations()
    {
        $logged_in_user = Auth::user()->load('roles');
        $donations = Donation::all();

        if ($logged_in_user->roles[0]->name == 'app-admin' or $logged_in_user->roles[0]->name=='advisor') {
            return Datatables::of($donations)->addColumn('action', function ($user) {

            })
                ->ToJson();
        }
    }

    //GET TIA Applications

    public function tiaIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.applicants.tia-application-index');
        }elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.applicants.tia-application-index');
        }

    }

    public function getTiaApplicants(){
        $logged_in_user = Auth::user()->load('roles');
        $tiaApplicants = TiaApplication::all();

        if ($logged_in_user->roles[0]->name == 'app-admin'
            or $logged_in_user->roles[0]->name == 'advisor') {
            return Datatables::of($tiaApplicants)->addColumn('action', function ($applicant) {
                $sh = '/get-tia-overview/' . $applicant->id;
                return '<a href=' . $sh . ' title="View Applicant"><i class="material-icons">remove_red_eye</i></a>';
            })
                ->make(true);
        } else {
            return response('You are not authorized!');
        }
    }

    public function viewTiaApplicationOverview(TiaApplication $tiaApplication){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.applicants.view-tia-application-overview',compact('tiaApplication'));
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.applicants.view-tia-application-overview',compact('tiaApplication'));
        }
    }

    //Store Appointments
    public function storeMentorAppointments(Request $request)
    {

        $input = $request->all();
        DB::beginTransaction();

        $selected_incubatees = json_decode($input['selected_incubatees']);
        try {
            foreach ($selected_incubatees as $selected_venture) {
                $user_id = $selected_venture;
                $user = User::find($user_id);

                $users = Auth::user()->load('roles');
                $appointment = Appointment::create(['title' => $input['title'], 'mentor_name' => $users->name,
                    'date' => $input['date'], 'time' => $input['time'],
                    'message' => $input['message'], 'user_id' => $user->id]);

                $appointment->save();

                $user_email = $user->email;
                $name = $user->name;

                $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                $fromEmail = "dev@propellaincubator.co.za";
                $toEmail = $user_email;
                $subject = "Mentorship Appointment";
                $htmlBody = "<p>Dear <b>$name</b></p>
                        <br>
                         <p>$appointment->message</p>

                         <p> Warm regards</p>
                          <p>$users->name</p>";
                $textBody = "";
                $tag = "example-email-tag";
                $trackOpens = true;
                $trackLinks = "None";
                $messageStream = "broadcast";

                // Send an email:
                $sendResult = $client->sendEmail(
                    $fromEmail,
                    $toEmail,
                    $subject,
                    $htmlBody,
                    $textBody,
                    $tag,
                    $trackOpens,
                    NULL, // Reply To
                    NULL, // CC
                    NULL, // BCC
                    NULL, // Header array
                    NULL, // Attachment array
                    $trackLinks,
                    NULL, // Metadata array
                    $messageStream
                );


            }

            DB::commit();
            return response()->json(['message' => 'Appointment created.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'User could not be saved  ' . $e->getMessage()], 400);
        }

    }

    //Update venture stage and send email
    public function updateVentureStage(Request $request,Venture $venture)
    {
        DB::beginTransaction();
        $input = $request->all();
        try {

            $venture->update(['stage' => $input['stage']]);

            $venture->save();

            $venture->load('incubatee');

            if (count($venture->incubatee) > 0) {
                foreach ($venture->incubatee as $ven_inc) {
                    $ven_inc->load('user');
                    $inc_user = $ven_inc->user;
                    $user_email = $inc_user->email;
                    $name = $inc_user->name;
                    $stage = $venture->stage;

                    $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                    $fromEmail = "dev@propellaincubator.co.za";
                    $toEmail = $user_email;
                    $subject = "You are now in $stage";
                    $htmlBody = "<p>Dear <b>$name</b></p>
                        <p>Congratulations on moving to the next stage  of the programme</p>

                        <p>With many thanks.</p>
                        The Propella Team";
                    $textBody = "";
                    $tag = "example-email-tag";
                    $trackOpens = true;
                    $trackLinks = "None";
                    $messageStream = "broadcast";

                    // Send an email:
                    $sendResult = $client->sendEmail(
                        $fromEmail,
                        $toEmail,
                        $subject,
                        $htmlBody,
                        $textBody,
                        $tag,
                        $trackOpens,
                        NULL, // Reply To
                        NULL, // CC
                        NULL, // BCC
                        NULL, // Header array
                        NULL, // Attachment array
                        $trackLinks,
                        NULL, // Metadata array
                        $messageStream
                    );
                }
            }


            DB::commit();
            return response()->json(['message' => 'Stage updated successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    //Update venture status and send email
    public function updateVentureStatus(Request $request,Venture $venture)
    {
        DB::beginTransaction();
        $input = $request->all();
        try {

            $venture->update(['status' => $input['status']]);

            $venture->save();

            $venture->load('incubatee');

            if (count($venture->incubatee) > 0) {
                foreach ($venture->incubatee as $ven_inc) {
                    $ven_inc->load('user');
                    $inc_user = $ven_inc->user;
                    $user_email = $inc_user->email;
                    $name = $inc_user->name;
                    $status = $venture->status;

                    $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                    $fromEmail = "dev@propellaincubator.co.za";
                    $toEmail = $user_email;
                    $subject = "Your Venture status is $status";
                    $htmlBody = "<p>Dear <b>$name</b></p>
                        <p>Your status have changed to $status</p>

                        <p>With many thanks.</p>
                        The Propella Team";
                    $textBody = "";
                    $tag = "example-email-tag";
                    $trackOpens = true;
                    $trackLinks = "None";
                    $messageStream = "broadcast";

                    // Send an email:
                    $sendResult = $client->sendEmail(
                        $fromEmail,
                        $toEmail,
                        $subject,
                        $htmlBody,
                        $textBody,
                        $tag,
                        $trackOpens,
                        NULL, // Reply To
                        NULL, // CC
                        NULL, // BCC
                        NULL, // Header array
                        NULL, // Attachment array
                        $trackLinks,
                        NULL, // Metadata array
                        $messageStream
                    );
                }
            }


            DB::commit();
            return response()->json(['message' => 'Stage updated successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }


    //Newsletter index
    public function newsletterIndex(){
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.newsletter.newsletter');
        }

    }

    //Get newsletter
    public function getNewsletter()
    {
        $news = Newsletter::all();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($news)->addColumn('action', function ($blog) {
                })
                ->make(true);
        }
    }
}
