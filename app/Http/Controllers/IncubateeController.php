<?php

namespace App\Http\Controllers;

use App\Bootcamper;
use App\BootcamperPanelist;
use App\DeclinedApplicantCategory;
use App\DeclinedBootcamper;
use App\DeclinedBootcamperEvent;
use App\DeclinedBootcamperPanelist;
use App\DeclinedBootcamperPanelistQuestionAnswers;
use App\DeclinedBootcamperQuestionAnswer;
use App\Event;
use App\EventIncubatee;
use App\EventVisitor;
use App\IctPanelistQuestionAnswer;
use App\IncPanelistQuestionAnswer;
use App\Incubatee;
use App\IncubateeContactLog;
use App\IncubateeEventDeregister;
use App\IncubateeOwnership;
use App\IncubateePanelist;
use App\IncubateeStage;
use App\IncubateeUpload;
use App\Panelist;
use App\PanelistQuestionAnswer;
use App\Question;
use App\QuestionsCategory;
use App\Role;
use App\User;
use App\UserQuestionAnswer;
use App\Venture;
use App\VentureBbbeeCertificate;
use App\VentureCategory;
use App\VentureCkDocument;
use App\VentureClientContract;
use App\VentureDormantAffidavit;
use App\VentureManagementAccount;
use App\VentureOwnership;
use App\VentureTaxClearance;
use App\VentureUpload;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tightenco\Collect\Support\Arr;
use Yajra\DataTables\DataTables;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class IncubateeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.create');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.incubatees.create');
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.incubatees.create');
        } elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.incubatees.create');
        } elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.incubatees.create');
        }
    }

    public function incCreateVenture(Incubatee $incubatee)
    {
        $current_user = $incubatee->user;
        $venture_categories = VentureCategory::all();
        $incubatee_stages = IncubateeStage::all();

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users/incubatees/admin-incubatees/incubatee-create-venture', compact('incubatee',
                'current_user', 'venture_categories', 'incubatee_stages'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users/administrators/incubatees/incubatee-create-venture', compact('incubatee',
                'current_user', 'venture_categories', 'incubatee_stages'));
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.incubatees.incubatee-create-venture', compact('incubatee',
                'current_user', 'venture_categories', 'incubatee_stages'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.incubatee.incubatee-create-venture', compact('incubatee',
                'current_user', 'venture_categories', 'incubatee_stages'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.incubatees.incubatee-create-venture', compact('incubatee',
                'current_user', 'venture_categories', 'incubatee_stages'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.incubatees.incubatee-create-venture', compact('incubatee',
                'current_user', 'venture_categories', 'incubatee_stages'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function store(Request $request)
    {

        DB::beginTransaction();
        try {
            $input = $request->all();


            $incubatee_role = Role::where('name', 'incubatee')->first();

            $user = User::create(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'], 'email' => $input['email'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'dob' => $input['dob'], 'age' => $input['age'],
                'password' => Hash::make('secret')]);

            $user->roles()->attach($incubatee_role->id);

            $incubatee = Incubatee::create(['user_id' => $user->id,
                'nmu_graduate' => $input['nmu_graduate'],
                'disabled' => $input['disabled'],
                'home_language' => $input['home_language'],
                'residence' => $input['residence'],
                'short_bio' => $input['short_bio'],
                'personal_facebook_url' => $input['personal_facebook_url'],
                'personal_linkedIn_url' => $input['personal_linkedIn_url'],
                'personal_twitter_url' => $input['personal_twitter_url'],
                'personal_instagram_url' => $input['personal_instagram_url']]);

            DB::commit();

            return response()->json(['message' => 'Incubatee has been successfully saved, please proceed to media tab', 'user' => $user, 'incubatee' => $incubatee]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function storeMedia(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();

        try {
            if ($request->hasFile('profile_picture_url')) {
                $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                $create = IncubateeUpload::create(['profile_picture_url' => $profile_picture_path, 'incubatee_id' => $input['incubatee_id']]);
            }

            if ($request->hasFile('id_document') and $request->has('profile_picture_url')) {
                $profile_picture_path = $request->file('id_document')->store('incubatee_uploads');

                $venture_uploads = IncubateeUpload::with('incubatee')->where('incubatee_id', $input['incubatee_id'])
                    ->update(['id_document' => $profile_picture_path]);

            }

            DB::commit();

            return response()->json(['message' => 'Incubatee Media has been saved.']);

        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    /*Storing Venture Information*/
    public function storeVenture(Request $request, Incubatee $incubatee)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            /*$venture_category_id = $input['hub'];
            $incubatee_stage_id = $input['stage'];
            $venture_category = VentureCategory::find($venture_category_id);
            $incubatee_stage = IncubateeStage::find($incubatee_stage_id);

            $current_user = $incubatee->load('user');
            $incubatee->update(['venture_category_id' => $venture_category->id]);
            $incubatee->stages()->sync($incubatee_stage->id);
            $incubatee->save();*/


            $venture = Venture::create([
                'hub' =>  $input['hub'],
                'smart_city_tags' => $input['smart_city_tags'],
                'business_type' => $input['business_type'],
                'company_name' => $input['company_name'],
                'slug' => Str::slug($input['company_name'], '-'),
                'stage' => $input['stage'],
                'cohort' => $input['cohort'],
                'contact_number' => $input['contact_number'],
                'physical_address_one' => $input['physical_address_one'],
                'physical_address_two' => $input['physical_address_two'],
                'physical_address_three' => $input['physical_address_three'],
                'physical_city' => $input['physical_city'],
                'physical_postal_code' => $input['physical_postal_code'],
                'postal_address_one' => $input['postal_address_one'],
                'postal_address_two' => $input['postal_address_two'],
                'postal_address_three' => $input['postal_address_three'],
                'postal_city' => $input['postal_city'],
                'postal_code' => $input['postal_code'],
                'virtual_or_physical' => $input['virtual_or_physical'],
                'pricinct_telephone_code' => $input['pricinct_telephone_code'],
                'pricinct_keys' => $input['pricinct_keys'],
                'pricinct_office_position' => $input['pricinct_office_position'],
                'pricinct_printing_code' => $input['pricinct_printing_code'],
                'pricinct_warehouse_position' => $input['pricinct_warehouse_position'],
                'rent_turnover' => $input['rent_turnover'],
                'rent_date' => $input['rent_date'],
                'rent_amount' => $input['rent_amount'],
                'status' => $input['status'],
                'alumni_date' => $input['alumni_date'],
                'exit_date' => $input['exit_date'],
                'resigned_date' => $input['resigned_date'],
                'mentor' => $input['mentor'],
                'advisor' => $input['advisor'],
                'venture_funding' => $input['venture_funding'],
                'date_awarded' => $input['date_awarded'],
                'fund_value' => $input['fund_value'],
                'date_closedout' => $input['date_closedout'],
                'venture_type' => $input['venture_type']]);

            $ownership = VentureOwnership::create(['venture_id' => $venture->id,
                'ownership' => $input['ownership'],
                'registration_number' => $input['registration_number'],
                'BBBEE_level' => $input['BBBEE_level']]);


            $create_venture_upload = VentureUpload::create(['venture_id' => $venture->id]);

            if ($request->has('vat_registration_number_url')) {
                if (isset($create_venture_upload)) {
                    $create_venture_upload->update(['vat_registration_number_url' => $input['vat_registration_number_url']]);
                }
            }

            if ($request->hasFile('client_contract')) {
                $client_contract_path = $request->file('client_contract')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_client_contract = VentureClientContract::create(['client_contract_url' => $client_contract_path,
                        'date_of_signature' => $input['date_of_signature'], 'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('tax_clearance_url')) {
                $tax_clearance_path = $request->file('tax_clearance_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_tax_clearance = VentureTaxClearance::create(['tax_clearance_url' => $tax_clearance_path,
                        'date_of_tax_clearance' => $input['date_of_clearance_tax'], 'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('bbbee_certificate_url')) {
                $bbbee_certificate_path = $request->file('bbbee_certificate_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_bbbee_certificate = VentureBbbeeCertificate::create(['bbbee_certificate_url' => $bbbee_certificate_path,
                        'date_of_bbbee_certificate' => $input['date_of_bbbee'], 'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('ckDocument_url')) {
                $ck_document_path = $request->file('ckDocument_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_ck_documents = VentureCkDocument::create(['ck_documents_url' => $ck_document_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('management_account_url')) {
                $management_account_path = $request->file('management_account_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_management_account = VentureManagementAccount::create(['management_account_url' => $management_account_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            if ($request->hasFile('dormant_affidavit_url')) {
                $dormant_affidavit_path = $request->file('dormant_affidavit_url')->store('incubatee_uploads');
                if (isset($create_venture_upload)) {
                    $create_venture_dormant_affidavit = VentureDormantAffidavit::create(['dormant_affidavit_url' => $dormant_affidavit_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }
            }

            $incubatee->update(['venture_id' => $venture->id]);

            DB::commit();
            return response()->json(['message' => 'Venture details has been saved, please update Media Tab.', 'venture' => $venture, 'venture_upload' => $create_venture_upload]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    /*Storing Venture Media*/
    public function storeincVentureMedia(Request $request, Venture $venture)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();

            $venture->update(['venture_profile_information' => $input['venture_profile_information'],
                'venture_email' => $input['venture_email'],
                'company_website' => $input['company_website'],
                'business_facebook_url' => $input['business_facebook_url'],
                'business_twitter_url' => $input['business_twitter_url'],
                'business_linkedin_url' => $input['business_linkedin_url'],
                'business_instagram_url' => $input['business_instagram_url'],
                'elevator_pitch' => $input['elevator_pitch']]);

            $venture->load('ventureUploads');

            //See if the Venture Upload relationship has been set before
            if (isset($venture->ventureUploads)) {
                $venture_upload = $venture->ventureUploads;
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                }

                if ($request->hasFile('company_logo_url')) {
                    $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                    $venture_upload->update(['company_logo_url' => $company_logo_path]);
                }

                if ($request->hasFile('video_shot_url')) {
                    $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                    $venture_upload->update(['video_shot_url' => $video_shot_path]);
                }

                if ($request->hasFile('infographic_url')) {
                    $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                    $venture_upload->update(['infographic_url' => $infographic_path]);
                }

                if ($request->hasFile('crowdfunding_url')) {
                    $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                    $venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                }

                if ($request->hasFile('product_shot_url')) {
                    $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                    $venture_upload->update(['product_shot_url' => $product_shot_path]);
                }
            } else {
                //If Venture Upload has not been set. Create it based on file coming in.
                //Then update the same instance if there are any other files incoming.
                //There is a LOT of if statements here to check different file types and to update the same
                //instance of Venture Uploads that we create.
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['profile_picture_url' => $profile_picture_path, 'venture_id'
                    => $venture->id]);

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                    }

                    if ($request->hasFile('infographic_url')) {
                        $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['infographic_url' => $infographic_path]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if ($request->hasFile('company_logo_url')) {
                    $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['company_logo_url' => $company_logo_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                    }

                    if ($request->hasFile('infographic_url')) {
                        $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['infographic_url' => $infographic_path]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if ($request->hasFile('video_shot_url')) {
                    $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['video_shot_url' => $video_shot_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('infographic_url')) {
                        $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['infographic_url' => $infographic_path]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if ($request->hasFile('infographic_url')) {
                    $infographic_path = $request->file('infographic_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['infographic_url' => $infographic_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if ($request->hasFile('crowdfunding_url')) {
                    $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['crowdfunding_url' => $crowdfunding_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                } else if ($request->hasFile('product_shot_url')) {
                    $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                    $create_venture_upload = VentureUpload::create(['product_shot_url' => $product_shot_path,
                        'venture_id' => $venture->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['profile_picture_url' => $profile_picture_path]);
                    }

                    if ($request->hasFile('company_logo_url')) {
                        $company_logo_path = $request->file('company_logo_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['company_logo_url' => $company_logo_path]);
                    }

                    if ($request->hasFile('video_shot_url')) {
                        $video_shot_path = $request->file('video_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['video_shot_url' => $video_shot_path]);
                    }

                    if ($request->hasFile('crowdfunding_url')) {
                        $crowdfunding_path = $request->file('crowdfunding_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['crowdfunding_url' => $crowdfunding_path]);
                    }

                    if ($request->hasFile('product_shot_url')) {
                        $product_shot_path = $request->file('product_shot_url')->store('incubatee_uploads');
                        $create_venture_upload->update(['product_shot_url' => $product_shot_path]);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Venture media has been saved.', 'venture' => $venture]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured: ' . $e->getMessage()]);
        }
    }

    public function adminViewIncubateeAccountOverview(Incubatee $incubatee)
    {
        $logged_in_user = Auth::user()->load('roles');
        $incubatee->load('user', 'events', 'incubateeDeregisters', 'uploads');
        $incubatee_user = $incubatee->user;
        $incubatee_user->load('applicant', 'userQuestionAnswers', 'bootcamper');

        //The incubatee relationship details will be stored in these variables
        $incubatee_uploads = null;
        $incubatee_events_array = [];
        $incubatee_deregistered_events_array = [];

        //If the incubatee is a applicant or has the relationship, the details will be stored here
        $incubatee_applicant = null;
        $incubatee_application_question_answers = null;
        $incubatee_application_question_answers_array = [];
        $incubatee_applicant_panelists = null;
        $inucbatee_applicant_panelists_array = [];
        $incubatee_applicant_panel_selection_time = null;
        $incubatee_applicant_panel_selection_date = null;
        $incubatee_applicant_total_panel_score = null;
        $incubatee_applicant_average_panel_score = null;

        //If the incubatee is a bootcamper or has the relationship, the details will be stored here
        $incubatee_bootcamper = null;
        $incubatee_bootcamper_events = null;
        $incubatee_bootcamper_events_array = [];
        $incubatee_bootcamper_panelists = null;
        $inucbatee_bootcamper_panelists_array = [];
        $incubatee_bootcamper_panel_selection_time = null;
        $incubatee_bootcamper_panel_selection_date = null;
        $incubatee_bootcamper_total_panel_score = null;
        $incubatee_bootcamper_average_panel_score = null;

        //Check if the incubatee has any uploades
        if(isset($incubatee->uploads)){
            $incubatee_uploads = $incubatee->uploads;
        }

        //Check if the incubatee has any events
        if(isset($incubatee->events)){
            $incubatee_events = $incubatee->events;
            if(count($incubatee_events) > 0){
                foreach($incubatee_events as $i_event){
                    $event_id = $i_event->event_id;
                    $event = Event::find($event_id);

                    $object = (object)[
                        'event_title' => isset($event->title) ? $event->title : 'No title set',
                        'event_date' => isset($event->start) ? $event->start : 'No date set',
                        'attended' => isset($i_event->attended) ? $i_event->attended : 'No attendance set',
                        'date_time_registered' => isset($i_event->date_time_registered) ? $i_event->date_time_registered : 'No date time set',
                        'found_out_via' => isset($i_event->found_out_via) ? $i_event->found_out_via : 'Not set'];
                    array_push($incubatee_events_array, $object);
                }
            }
        }

        //Check if the incubatee has any de-registered events
        if(isset($incubatee->incubateeDeregisters)){
            $incubatee_deregistered_events = $incubatee->incubateeDeregisters;
            if(count($incubatee_deregistered_events) > 0){
                foreach($incubatee_deregistered_events as $i_d_event){
                    $event_id = $i_d_event->event_id;
                    $event = Event::find($event_id);

                    $object = (object)['event_title' => $i_d_event->event_name, 'event_date' => isset($event->start),
                        'de_register_reason' => $i_d_event->de_register_reason, 'date_time_de_register' => $i_d_event->date_time_de_register];
                    array_push($incubatee_deregistered_events_array, $object);
                }
            }
        }

        //Check if the incubatee has application question and answers.
        if(isset($incubatee_user->userQuestionAnswers)){
            $incubatee_application_question_answers = $incubatee_user->userQuestionAnswers;
            //If the incubatee does have question and answers, store in to an array that has been created by objects
            //Push the array to the view
            if(count($incubatee_application_question_answers) > 0){
                foreach ($incubatee_application_question_answers as $application_question_answer){
                    $question_id = $application_question_answer->question_id;
                    $question = Question::find($question_id);
                    if(isset($question)){
                        $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                            'answer_text' => $application_question_answer->answer_text];
                        array_push($incubatee_application_question_answers_array, $object);
                    }
                }
            }
        }

        //Check if the incubatee has application relationship
        if(isset($incubatee_user->applicant)){
            $incubatee_applicant = $incubatee_user->applicant;
            //load all applicant relationships
            $incubatee_applicant->load('panelists');

            //Check if the applicant has panelists,
            //If they do, set a global variable = to the applicant panelists
            if(count($incubatee_applicant->panelists) > 0){
                $incubatee_applicant_panelists = $incubatee_applicant->panelists;
                $incubatee_applicant_total_panel_score = $incubatee_applicant->total_panel_score;
                $incubatee_applicant_average_panel_score = $incubatee_applicant->average_panel_score;

                if(count($incubatee_applicant_panelists) > 0){
                    $panel_selection_time = $incubatee_applicant_panelists[0]->panel_selection_time;
                    $incubatee_applicant_panel_selection_time = $panel_selection_time;
                    $panel_selection_date = $incubatee_applicant_panelists[0]->panel_selection_date;
                    $incubatee_applicant_panel_selection_date = $panel_selection_date;

                    //Loop through applicant panelist and store them in an array,
                    //then push that newly created array to a view
                    foreach($incubatee_applicant_panelists as $a_panelist){
                        $a_panelist->load('panelistQuestionAnswers');
                        $panelist_id = $a_panelist->panelist_id;
                        $panelist = Panelist::find($panelist_id);
                        $panelist->load('user');
                        $panelist_user = $panelist->user;
                        $incubatee_applicant_panelist_question_answer_array = [];

                        if(count($a_panelist->panelistQuestionAnswers) > 0){
                            foreach($a_panelist->panelistQuestionAnswers as $p_question_answer){
                                $question_id = $p_question_answer->question_id;
                                $question = Question::find($question_id);
                                if(isset($question)){
                                    $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                                        'panelist_score' => $p_question_answer->score, 'panelist_comment' => $p_question_answer->comment];
                                    array_push($incubatee_applicant_panelist_question_answer_array, $object);
                                }
                            }
                        }

                        //Return a sorted version of the incubatee_bootcamper_panelist_question_answer_array Array -> using Arr::sort() as per laravel requests
                        $sortedApplicantPanelistQuestionAnswersArray = array_values(Arr::sort($incubatee_bootcamper_panelist_question_answer_array, function ($value) {
                            return $value->question_number;
                        }));

                        $object = (object)['panelist_details' => $panelist_user->name . ' ' . $panelist_user->surname, 'panelist_question_answers' => $sortedApplicantPanelistQuestionAnswersArray];
                        array_push($inucbatee_applicant_panelists_array, $object);
                    }
                }
            }
        }

        //Check if the incubatee has been through the bootcamper journey,
        if(isset($incubatee_user->bootcamper)){
            $incubatee_bootcamper = $incubatee_user->bootcamper;
            //load all bootcamper relationships
            if(Cache::has('incubatee_bootcamper'))
                $incubatee_bootcamper = Cache::get('incubatee_bootcamper');
            else {
                $incubatee_bootcamper->load('ventureCategory', 'events', 'panelists');
                Cache::forever('incubatee_bootcamper', $incubatee_bootcamper);
            }

            //Check if the bootcamper has events,
            //If they do, set a global variable = to the bootcamper events
            if(count($incubatee_bootcamper->events) > 0){
                $incubatee_bootcamper_events = $incubatee_bootcamper->events;

                //Loop through bootcamper events and store them in an array,
                //then push that newly created array to a view
                foreach ($incubatee_bootcamper_events as $b_event){
                    $event_id = $b_event->event_id;
                    $event = Event::find($event_id);
                    $object = (object)['event_title' => $event->title,
                        'accepted' => $b_event->accepted, 'date_registered' => $b_event->date_registered,
                        'attended' => $b_event->attended, 'declined' => $b_event->declined, 'event_date' => $b_event->start];
                    array_push($incubatee_bootcamper_events_array, $object);
                }
            }

            //Check if the bootcamper has panelists,
            //If they do, set a global variable = to the bootcamper panelists
            if(count($incubatee_bootcamper->panelists) > 0){
                $incubatee_bootcamper_panelists = $incubatee_bootcamper->panelists;
                $incubatee_bootcamper_average_panel_score = $incubatee_bootcamper->average_panel_score;
                $incubatee_bootcamper_total_panel_score = $incubatee_bootcamper->total_panel_score;

                if(count($incubatee_bootcamper_panelists) > 0){
                    $panel_selection_time = $incubatee_bootcamper_panelists[0]->panel_selection_time;
                    $incubatee_bootcamper_panel_selection_time = $panel_selection_time;
                    $panel_selection_date = $incubatee_bootcamper_panelists[0]->panel_selection_date;
                    $incubatee_bootcamper_panel_selection_date = $panel_selection_date;

                    //Loop through bootcamper panelist and store them in an array,
                    //then push that newly created array to a view
                    foreach($incubatee_bootcamper_panelists as $b_panelist){
                        if(Cache::has('b_panelist'))
                            $b_panelist = Cache::get('b_panelist');
                        else {
                            $b_panelist->load('panelistQuestionAnswers');
                            Cache::forever('b_panelist', $b_panelist);
                        }

                        $panelist_id = $b_panelist->panelist_id;
                        $panelist = Panelist::find($panelist_id);
                        $panelist->load('user');
                        $panelist_user = $panelist->user;
                        $incubatee_bootcamper_panelist_question_answer_array = [];

                        if(count($b_panelist->panelistQuestionAnswers) > 0){
                            foreach($b_panelist->panelistQuestionAnswers as $p_question_answer){
                                $question_id = $p_question_answer->question_id;
                                $question = Question::find($question_id);
                                if(isset($question)){
                                    $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                                        'panelist_score' => $p_question_answer->score, 'panelist_comment' => $p_question_answer->comment];
                                    array_push($incubatee_bootcamper_panelist_question_answer_array, $object);
                                }
                            }
                        }

                        //Return a sorted version of the incubatee_bootcamper_panelist_question_answer_array Array -> using Arr::sort() as per laravel requests
                        $sortedBootcamperPanelistQuestionAnswersArray = array_values(Arr::sort($incubatee_bootcamper_panelist_question_answer_array, function ($value) {
                            return $value->question_number;
                        }));

                        $object = (object)['panelist_details' => $panelist_user->name . ' ' . $panelist_user->surname, 'panelist_question_answers' => $sortedBootcamperPanelistQuestionAnswersArray];
                        array_push($inucbatee_bootcamper_panelists_array, $object);
                    }
                }
            }
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users/incubatees/admin-incubatees/incubatee-account-overview', compact('incubatee', 'incubatee_user', 'incubatee_applicant',
            'incubatee_application_question_answers_array', 'inucbatee_applicant_panelists_array', 'incubatee_applicant_panel_selection_time',
            'incubatee_applicant_panel_selection_date', 'incubatee_applicant_total_panel_score', 'incubatee_applicant_average_panel_score',
            'incubatee_bootcamper', 'incubatee_bootcamper_events_array', 'inucbatee_bootcamper_panelists_array',
            'incubatee_bootcamper_panel_selection_date', 'incubatee_bootcamper_panel_selection_time', 'incubatee_bootcamper_total_panel_score',
            'incubatee_bootcamper_average_panel_score', 'incubatee_events_array', 'incubatee_deregistered_events_array', 'incubatee_uploads'));
        } elseif ($logged_in_user->roles[0]->name == 'administrator'){
            return view('users/administrators/incubatees/incubatee-account-overview', compact('incubatee', 'incubatee_user', 'incubatee_applicant',
                'incubatee_application_question_answers_array', 'inucbatee_applicant_panelists_array', 'incubatee_applicant_panel_selection_time',
                'incubatee_applicant_panel_selection_date', 'incubatee_applicant_total_panel_score', 'incubatee_applicant_average_panel_score',
                'incubatee_bootcamper', 'incubatee_bootcamper_events_array', 'inucbatee_bootcamper_panelists_array',
                'incubatee_bootcamper_panel_selection_date', 'incubatee_bootcamper_panel_selection_time', 'incubatee_bootcamper_total_panel_score',
                'incubatee_bootcamper_average_panel_score', 'incubatee_events_array', 'incubatee_deregistered_events_array', 'incubatee_uploads'));
        } elseif ($logged_in_user->roles[0]->name == 'advisor'){
            return view('users/advisor-dashboard/incubatees/incubatee-account-overview', compact('incubatee', 'incubatee_user', 'incubatee_applicant',
                'incubatee_application_question_answers_array', 'inucbatee_applicant_panelists_array', 'incubatee_applicant_panel_selection_time',
                'incubatee_applicant_panel_selection_date', 'incubatee_applicant_total_panel_score', 'incubatee_applicant_average_panel_score',
                'incubatee_bootcamper', 'incubatee_bootcamper_events_array', 'inucbatee_bootcamper_panelists_array',
                'incubatee_bootcamper_panel_selection_date', 'incubatee_bootcamper_panel_selection_time', 'incubatee_bootcamper_total_panel_score',
                'incubatee_bootcamper_average_panel_score', 'incubatee_events_array', 'incubatee_deregistered_events_array', 'incubatee_uploads'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function adminShowIncubateePanelistScoreSheet(BootcamperPanelist $bootcamperPanelist){
        $bootcamperPanelist->load('panelistQuestionAnswers');
        $panelist_question_answers = [];
        $logged_in_user = Auth::user()->load('roles');
        $panelist = $bootcamperPanelist->panelist;
        $user_object = (object)['name' => $panelist->user->name, 'surname' => $panelist->user->surname];

        foreach($bootcamperPanelist->panelistQuestionAnswers as $pqa){
            $question_id = $pqa->question_id;
            $question = Question::find($question_id);
            $question->load('questionSubTexts');
            $question_sub_texts = [];

            if(isset($question->questionSubTexts)){
                foreach ($question->questionSubTexts as $questionSubText){
                    $question_sub_text = $questionSubText->question_sub_text;
                    array_push($question_sub_texts, $question_sub_text);
                }
            }

            $question_text = $question->question_text;
            $question_number = $question->question_number;
            $score = $pqa->score;
            $comment = $pqa->comment;

            $object = (object)['question_text' => $question_text, 'question_number' => $question_number, 'score' => $score,
                'comment' => $comment, 'question_sub_texts' => $question_sub_texts];
            array_push($panelist_question_answers, $object);
        }

        if($logged_in_user->roles[0]->name == 'app-admin'){
            return view('users.incubatees.admin-incubatees.show-incubatee-panelist-score-sheet', compact('panelist_question_answers','bootcamperPanelist','user_object'));
        } else if($logged_in_user->roles[0]->name == 'administrator'){
            return view('users.administrators.incubatees.show-incubatee-panelist-score-sheet', compact('panelist_question_answers', 'user_object'));
        } else if($logged_in_user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.incubatees.show-incubatee-panelist-score-sheet', compact('panelist_question_answers', 'user_object'));
        } else {
            return response("You are not authorized!");
        }
    }


    //SHOW ALL INCUBATEE DETAILS
    public function showIncubateeDetails(Incubatee $incubatee)
    {
        $incubatee->with('user', 'events', 'incubateeDeregisters', 'uploads', 'venture_category', 'panelists', 'incubateeContactLog', 'venture');
        $incubatee_uploads = $incubatee->uploads;
        $role = Auth::user()->roles()->first();
        $incubatee_user = $incubatee->user;
        $incubatee_user->load('applicant', 'userQuestionAnswers', 'bootcamper')->get();
        $bootcamper = $incubatee_user->bootcamper;
        $applicant_user = $incubatee_user->applicant;
        $applicant_user->load('contactLog');

        //APPLICATION FORM
        $incubatee_application_question_answers_array = [];

        foreach ($incubatee_user->userQuestionAnswers as $userQuestionAnswer) {
            $question_id = $userQuestionAnswer->question_id;
            $question = Question::find($question_id);
            $answer_text = $userQuestionAnswer->answer_text;
            if (isset($question)) {
                if (isset($question->category->category_name)) {
                    if ($question->category->category_name == 'AA Application form - ICT Bootcamp' or
                        $question->category->category_name == 'AA Application form - Propella Satellite') {
                        $object = (object)['question_number' => $question->question_number, 'question_text' => $question->question_text,
                            'answer_text' => $answer_text];

                        array_push($incubatee_application_question_answers_array, $object);
                    }
                }
            }
        }


        if ($role->name == 'app-admin' and $incubatee->venture_category->category_name == 'ICT' and $incubatee->venture_category->category_name == 'PSI') {
            return view('users/incubatees/admin-incubatees/show-incubatee', compact('incubatee', 'incubatee_application_question_answers_array',
            'bootcamper','incubatee_user','incubatee_uploads'));
        }elseif ($role->name == 'administrator' and $incubatee->venture_category->category_name == 'ICT' and $incubatee->venture_category->category_name == 'PSI'){
            return view('users/administrators/incubatees/show-incubatee',compact('incubatee', 'incubatee_application_question_answers_array',
                'bootcamper','incubatee_user','incubatee_uploads'));
        } elseif ($role->name == 'advisor' and $incubatee->venture_category->category_name == 'ICT' and $incubatee->venture_category->category_name == 'PSI'){
            return view('users/advisor-dashboard/incubatees/show-incubatee', compact('incubatee', 'incubatee_application_question_answers_array',
                'bootcamper','incubatee_user','incubatee_uploads'));
        } else {
            return view('users/incubatees/admin-incubatees/show-incubatee',compact('incubatee', 'incubatee_application_question_answers_array',
            'bootcamper','incubatee_user','incubatee_uploads'));

        }
    }


    //Download File
    public function downloadIdDocument($id)
    {
        $file = IncubateeUpload::find($id);

        return Storage::download($file->id_document,$file->file_description.'.pdf');
    }

    public function edit(Incubatee $incubatee)
    {
        $current_user = $incubatee->user;
        $incubatee_ownership = $incubatee->ownership;
        $incubatee_uploads = $incubatee->uploads;
        $current_venture = $incubatee->venture;


        if (Auth::user()->load('roles')->roles[0]->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.edit', compact('current_user', 'incubatee', 'incubatee_ownership', 'incubatee_uploads','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'administrator') {
            return view('users.administrators.incubatees.edit', compact('current_user', 'incubatee', 'incubatee_ownership', 'incubatee_uploads','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'incubatee') {
            return view('users.incubatee-dashboard.incubatee.edit', compact('current_user', 'incubatee', 'incubatee_ownership', 'incubatee_uploads','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.incubatees.edit', compact('current_user', 'incubatee', 'incubatee_ownership', 'incubatee_uploads','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.incubatees.edit', compact('current_user', 'incubatee', 'incubatee_ownership', 'incubatee_uploads','current_venture'));
        } elseif (Auth::user()->load('roles')->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.incubatees.edit', compact('current_user', 'incubatee', 'incubatee_ownership', 'incubatee_uploads','current_venture'));
        }
    }

    public function editIncVenture(Venture $venture)
    {
        $venture->load('ventureOwnership', 'ventureUploads', 'incubatee');
        $current_venture_ownership = $venture->ventureOwnership;
        $venture_upload = $venture->ventureUploads;

        if (Auth::user()->load('roles')->roles[0]->name == 'app-admin') {
            return view('users/incubatees/admin-incubatees/incubatee-edit-venture', compact('venture', 'current_venture_ownership', 'venture_upload', 'incubatee'));
        } else {
            return view('users.venture.edit-venture', compact('venture_ownership', 'venture_upload', 'current_venture'));
        }
    }

    public function updateVenture(Request $request, Venture $venture)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $venture->load('ventureOwnership', 'ventureUploads');

            if ($request->has('company_name')) {
                $venture->update(['hub' => $input['hub'],
                    'smart_city_tags' => $input['smart_city_tags'],
                    'business_type' => $input['business_type'],
                    'company_name' => $input['company_name'],
                    'stage' => $input['stage'],
                    'cohort' => $input['cohort'],
                    'contact_number' => $input['contact_number'],
                    'physical_address_one' => $input['physical_address_one'],
                    'physical_address_two' => $input['physical_address_two'],
                    'physical_address_three' => $input['physical_address_three'],
                    'physical_city' => $input['physical_city'],
                    'physical_postal_code' => $input['physical_postal_code'],
                    'postal_address_one' => $input['postal_address_one'],
                    'postal_address_two' => $input['postal_address_two'],
                    'postal_address_three' => $input['postal_address_three'],
                    'postal_city' => $input['postal_city'],
                    'postal_code' => $input['postal_code'],
                    'virtual_or_physical' => $input['virtual_or_physical'],
                    'pricinct_telephone_code' => $input['pricinct_telephone_code'],
                    'pricinct_keys' => $input['pricinct_keys'],
                    'pricinct_office_position' => $input['pricinct_office_position'],
                    'pricinct_printing_code' => $input['pricinct_printing_code'],
                    'pricinct_warehouse_position' => $input['pricinct_warehouse_position'],
                    'rent_turnover' => $input['rent_turnover'],
                    'rent_date' => $input['rent_date'],
                    'rent_amount' => $input['rent_amount'],
                    'status' => $input['status'],
                    'alumni_date' => $input['alumni_date'],
                    'exit_date' => $input['exit_date'],
                    'resigned_date' => $input['resigned_date'],
                    'mentor' => $input['mentor'],
                    'advisor' => $input['advisor'],
                    'venture_type' => $input['venture_type'],
                    'venture_funding' => $input['venture_funding'],
                    'date_awarded' => $input['date_awarded'],
                    'fund_value' => $input['fund_value'],
                    'date_closedout' => $input['date_closedout'],
                    'venture_type' => $input['venture_type']]);
                $venture->save();
            }

            /*Ownership*/
            if (isset($venture->ventureOwnership)) {
                $venture_ownership = $venture->ventureOwnership;
                if ($request->has('ownership')) {
                    $venture_ownership->update(['ownership' => $input['ownership']]);
                }

                if ($request->has('registration_number')) {
                    $venture_ownership->update(['registration_number' => $input['registration_number']]);
                }

                if ($request->has('BBBEE_level')) {
                    $venture_ownership->update(['BBBEE_level' => $input['BBBEE_level']]);
                }
            } else {
                if ($request->has('ownership')) {
                    $create_venture_ownership = VentureOwnership::create(['ownership' => $input['ownership'],
                        'venture_id' => $venture->id]);

                    if ($request->has('registration_number')) {
                        $create_venture_ownership->update(['registration_number' => $input['registration_number']]);
                    }

                    if ($request->has('BBBEE_level')) {
                        $create_venture_ownership->update(['BBBEE_level' => $input['BBBEE_level']]);
                    }
                } else if ($request->has('registration_number')) {
                    $create_venture_ownership = VentureOwnership::create(['registration_number' => $input['registration_number'],
                        'venture_id' => $venture->id]);

                    if ($request->has('ownership')) {
                        $create_venture_ownership->update(['ownership' => $input['ownership']]);
                    }

                    if ($request->has('BBBEE_level')) {
                        $create_venture_ownership->update(['BBBEE_level' => $input['BBBEE_level']]);
                    }
                } else if ($request->has('BBBEE_level')) {
                    $create_venture_ownership = VentureOwnership::create(['BBBEE_level' => $input['BBBEE_level'],
                        'venture_id' => $venture->id]);

                    if ($request->has('ownership')) {
                        $create_venture_ownership->update(['ownership' => $input['ownership']]);
                    }

                    if ($request->has('registration_number')) {
                        $create_venture_ownership->update(['registration_number' => $input['registration_number']]);
                    }
                }
            }


            /*uploads*/
            if (isset($venture->ventureUploads)) {
                $venture_upload = $venture->ventureUploads;

                //Check the venture uploads' client contracts
                if ($request->hasFile('client_contract')) {
                    $client_contract_path = $request->file('client_contract')->store('incubatee_uploads');
                    $date_of_signature = $input['date_of_signature'];
                    $create_venture_client_contract = VentureClientContract::create(['client_contract_url' => $client_contract_path,
                        'date_of_signature' => $date_of_signature, 'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' tax clearances
                if ($request->hasFile('tax_clearance_url')) {
                    $tax_clearance_path = $request->file('tax_clearance_url')->store('incubatee_uploads');
                    $date_of_clearance_tax = $input['date_of_clearance_tax'];
                    $create_venture_tax_clearance = VentureTaxClearance::create(['tax_clearance_url' => $tax_clearance_path,
                        'date_of_tax_clearance' => $date_of_clearance_tax, 'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' bbbee certificates
                if ($request->hasFile('bbbee_certificate_url')) {
                    $bbbee_certificate_path = $request->file('bbbee_certificate_url')->store('incubatee_uploads');
                    $date_of_bbbee_certificate = $input['date_of_bbbee'];
                    $create_venture_bbbee_certificate = VentureBbbeeCertificate::create(['bbbee_certificate_url' => $bbbee_certificate_path,
                        'date_of_bbbee_certificate' => $date_of_bbbee_certificate, 'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' ck documents
                if ($request->hasFile('ck_document')) {
                    $ck_document_path = $request->file('ck_documents_url')->store('incubatee_uploads');
                    $create_venture_ck_document = VentureCkDocument::create(['ck_documents_url' => $ck_document_path,
                        'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' management account
                if ($request->hasFile('management_account_url')) {
                    $management_account_path = $request->file('management_account_url')->store('incubatee_uploads');
                    $create_venture_management_account = VentureManagementAccount::create(['management_account_url' => $management_account_path,
                        'venture_upload_id' => $venture_upload->id]);
                }

                //Check the venture uploads' dormant affidavit
                if ($request->hasFile('dormant_affidavit_url')) {
                    $dormant_affidavi_path = $request->file('dormant_affidavit_url')->store('incubatee_uploads');
                    $create_venture_dormant_affidavit = VentureDormantAffidavit::create(['dormant_affidavit_url' => $dormant_affidavi_path,
                        'venture_upload_id' => $venture_upload->id]);
                }

                //Vat registration number
                if ($request->has('vat_registration_number_url')) {
                    $venture_upload->update(['vat_registration_number_url' => $input['vat_registration_number_url']]);
                }
            } else {
                $create_venture_upload = VentureUpload::create(['venture_id' => $venture->id]);

                //Check the venture uploads' client contracts
                if ($request->hasFile('client_contract')) {
                    $client_contract_path = $request->file('client_contract')->store('incubatee_uploads');
                    $date_of_signature = $input['date_of_signature'];
                    $create_venture_client_contract = VentureClientContract::create(['client_contract_url' => $client_contract_path,
                        'date_of_signature' => $date_of_signature, 'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' tax clearances
                if ($request->hasFile('tax_clearance_url')) {
                    $tax_clearance_path = $request->file('tax_clearance_url')->store('incubatee_uploads');
                    $date_of_clearance_tax = $input['date_of_clearance_tax'];
                    $create_venture_tax_clearance = VentureTaxClearance::create(['tax_clearance_url' => $tax_clearance_path,
                        'date_of_tax_clearance' => $date_of_clearance_tax, 'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' bbbee certificates
                if ($request->hasFile('bbbee_certificate_url')) {
                    $bbbee_certificate_path = $request->file('bbbee_certificate_url')->store('incubatee_uploads');
                    $date_of_bbbee_certificate = $input['date_of_bbbee'];
                    $create_venture_bbbee_certificate = VentureBbbeeCertificate::create(['bbbee_certificate_url' => $bbbee_certificate_path,
                        'date_of_bbbee_certificate' => $date_of_bbbee_certificate, 'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' ck documents
                if ($request->hasFile('ck_document')) {
                    $ck_document_path = $request->file('ck_documents_url')->store('incubatee_uploads');
                    $create_venture_ck_document = VentureCkDocument::create(['ck_documents_url' => $ck_document_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' management account
                if ($request->hasFile('management_account_url')) {
                    $management_account_path = $request->file('management_account_url')->store('incubatee_uploads');
                    $create_venture_management_account = VentureManagementAccount::create(['management_account_url' => $management_account_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }

                //Check the venture uploads' dormant affidavit
                if ($request->hasFile('dormant_affidavit_url')) {
                    $dormant_affidavi_path = $request->file('dormant_affidavit_url')->store('incubatee_uploads');
                    $create_venture_dormant_affidavit = VentureDormantAffidavit::create(['dormant_affidavit_url' => $dormant_affidavi_path,
                        'venture_upload_id' => $create_venture_upload->id]);
                }

                //Vat registration number
                if ($request->has('vat_registration_number_url')) {
                    $create_venture_upload->update(['vat_registration_number_url' => $input['vat_registration_number_url']]);
                }
            }


            DB::commit();
            return response()->json(['message' => 'Venture Business has been successfully updated.']);

        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function updatePersonal(Request $request, User $user)
    {
        $this->validate($request, [
            'short_bio' => 'max:2000'
        ]);

        DB::beginTransaction();
        $input = $request->all();
        try {
            $current_user = $user->load('incubatee');
            $current_incubatee = $user->incubatee;

            $current_user->update([
                'title' => $input['title'],
                'initials' => $input['initials'],
                'name' => $input['name'],
                'surname' => $input['surname'],
                'contact_number' => $input['contact_number'],
                'email' => $input['email'],
                'id_number' => $input['id_number'],
                'address_one' => $input['address_one'],
                'address_two' => $input['address_two'],
                'address_three' => $input['address_three'],
                'city' => $input['city'],
                'code' => $input['code'],
                'dob' => $input['dob'],
                'age' => $input['age'],
                'gender' => $input['gender']
            ]);


            $current_incubatee->update(['nmu_graduate' => $input['nmu_graduate'],
                'disabled' => $input['disabled'],
                'home_language' => $input['home_language'],
                'residence' => $input['residence'],
                'short_bio' => $input['short_bio'],
                'personal_facebook_url' => $input['personal_facebook_url'],
                'personal_linkedIn_url' => $input['personal_linkedIn_url'],
                'personal_twitter_url' => $input['personal_twitter_url'],
                'personal_instagram_url' => $input['personal_instagram_url'],
                'status' => $input['status'],]);


            DB::commit();
            $current_user->fresh()->load('incubatee');
            return response()->json(['message' => 'Incubatee Details has been successfully updated.']);
        } catch (Exception $e) {
            return response()->json(['message' => 'Something went wrong!']);
        }
    }

    public function updateIncubateeMedia(Request $request, User $user)
    {
        DB::beginTransaction();
        $input = $request->all();

        try {
            $user->load('incubatee');
            $current_incubatee = $user->incubatee;
            $current_incubatee->load('uploads');
            $current_incubatee_uploads = $current_incubatee->uploads;

            if (isset($current_incubatee_uploads)) {
                $current_incubatee_uploads->update(['stage_2_pitch_video' => $input['stage_2_pitch_video'],
                    'stage_2_pitch_video_title' => $input['stage_2_pitch_video_title']]);
            }else{
                $create_file_description = IncubateeUpload::create(['stage_2_pitch_video' => $input['stage_2_pitch_video'],
                    'incubatee_id' => $current_incubatee->id,'stage_2_pitch_video_title' => $input['stage_2_pitch_video_title']]);
            }

            if (isset($current_incubatee_uploads)) {
                $current_incubatee_uploads->update(['founder_video_shot' => $input['founder_video_shot'],
                    'founder_video_title'=>$input['founder_video_title']]);
            }else{
                $create_founder_video_shot = IncubateeUpload::create(['founder_video_shot' => $input['founder_video_shot'],
                    'founder_video_title'=>$input['founder_video_title']]);
            }


            if (isset($current_incubatee_uploads)) {
                if ($request->hasFile('proof_of_address')) {
                    $proof_of_address_path = $request->file('proof_of_address')->store('incubatee_uploads');
                    $current_incubatee_uploads->update(['proof_of_address' => $proof_of_address_path]);
                }
            } else {
                if ($request->hasFile('proof_of_address')) {
                    $proof_of_address_path = $request->file('proof_of_address')->store('incubatee_uploads');
                    $create_incubatee_uploads = IncubateeUpload::create(['proof_of_address' => $proof_of_address_path,
                        'incubatee_id' => $current_incubatee->id]);

                }
            }

            if (isset($current_incubatee_uploads)) {
                if ($request->hasFile('disc_assessment_report')) {
                    $disc_assessment_path = $request->file('disc_assessment_report')->store('incubatee_uploads');
                    $current_incubatee_uploads->update(['disc_assessment_report' => $disc_assessment_path]);
                }
            } else {
                if ($request->hasFile('disc_assessment_report')) {
                    $disc_assessment_path = $request->file('disc_assessment_report')->store('incubatee_uploads');
                    $create_incubatee_uploads = IncubateeUpload::create(['disc_assessment_report' => $disc_assessment_path,
                        'incubatee_id' => $current_incubatee->id]);

                }
            }
            if (isset($current_incubatee_uploads)) {
                if ($request->hasFile('strength_upload')) {
                    $strength_upload_path = $request->file('strength_upload')->store('incubatee_uploads');
                    $current_incubatee_uploads->update(['strength_upload' => $strength_upload_path]);
                }
            } else {
                if ($request->hasFile('strength_upload')) {
                    $strength_upload_path = $request->file('strength_upload')->store('incubatee_uploads');
                    $create_incubatee_uploads = IncubateeUpload::create(['strength_upload' => $strength_upload_path,
                        'incubatee_id' => $current_incubatee->id]);

                }
            }
            if (isset($current_incubatee_uploads)) {
                if ($request->hasFile('talent_dynamics_upload')) {
                    $talent_dynamic_path = $request->file('talent_dynamics_upload')->store('incubatee_uploads');
                    $current_incubatee_uploads->update(['talent_dynamics_upload' => $talent_dynamic_path]);
                }
            } else {
                if ($request->hasFile('talent_dynamics_upload')) {
                    $talent_dynamic_path = $request->file('talent_dynamics_upload')->store('incubatee_uploads');
                    $create_incubatee_uploads = IncubateeUpload::create(['talent_dynamics_upload' => $talent_dynamic_path,
                        'incubatee_id' => $current_incubatee->id]);

                }
            }

            if (isset($current_incubatee_uploads)) {
                $current_incubatee_uploads->update(['file_description' => $input['file_description']]);
            }else {
                if ($current_incubatee_uploads) {
                    $create_file_description = IncubateeUpload::create(['file_description' => $input['file_description'],
                        'incubatee_id' => $current_incubatee->id]);
                }
            }

            if (isset($current_incubatee_uploads)) {
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $current_incubatee_uploads->update(['profile_picture_url' => $profile_picture_path]);
                }

                if ($request->hasFile('id_document')) {
                    $id_document_path = $request->file('id_document')->store('incubatee_uploads');
                    $current_incubatee_uploads->update(['id_document' => $id_document_path]);
                }
            } else {
                if ($request->hasFile('profile_picture_url')) {
                    $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                    $create_incubatee_uploads = IncubateeUpload::create(['profile_picture_url' => $profile_picture_path,
                        'incubatee_id' => $current_incubatee->id]);

                    if ($request->hasFile('id_document')) {
                        $id_document_path = $request->file('id_document')->store('incubatee_uploads');
                        $create_incubatee_uploads->update(['id_document' => $id_document_path]);
                    }

                } else if ($request->hasFile('id_document')) {
                    $id_document_path = $request->file('id_document')->store('incubatee_uploads');
                    $create_incubatee_uploads = IncubateeUpload::create(['id_document' => $id_document_path,
                        'incubatee_id' => $current_incubatee->id]);

                    if ($request->hasFile('profile_picture_url')) {
                        $profile_picture_path = $request->file('profile_picture_url')->store('incubatee_uploads');
                        $create_incubatee_uploads->update(['profile_picture_url' => $profile_picture_path]);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Incubatee Media has been successfully updated.']);

        } catch (\Exception $e) {
            return response()->json(['message' => 'Media could not be updated at the moment ' . $e->getMessage()], 400);

        }
    }

    //Update disk assessment results
    public function updateDeskAssessmentResults(Request $request, User $user)
    {
        $input = $request->all();
        try{
            DB::beginTransaction();
            $user->load('incubatee');
            $current_incubatee = $user->incubatee;
            $current_incubatee->load('uploads');
            $current_incubatee_uploads = $current_incubatee->uploads;

            if (isset($current_incubatee_uploads)) {
                $current_incubatee_uploads->update(['disk_results' => $input['disk_results']]);
            }else{
                $create_current_incubatee_uploads = IncubateeUpload::create(['disk_results' => $input['disk_results']]);
            }


            DB::commit();
            return response()->json(['message' => 'Updated disk assessment.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

       //Update strength results
    public function updateStrengthResults(Request $request, User $user)
    {
        $input = $request->all();
        try{
            DB::beginTransaction();
            $user->load('incubatee');
            $current_incubatee = $user->incubatee;
            $current_incubatee->load('uploads');
            $current_incubatee_uploads = $current_incubatee->uploads;

            if (isset($current_incubatee_uploads)) {
                $current_incubatee_uploads->update(['strength_results' => $input['strength_results']]);
            }else{
                $create_current_incubatee_uploads = IncubateeUpload::create(['strength_results' => $input['strength_results']]);
            }


            DB::commit();
            return response()->json(['message' => 'Updated strength results.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }

    //Update talent dynamics results
    public function updateTalentDynamicsResults(Request $request, User $user)
    {
        $input = $request->all();
        try{
            DB::beginTransaction();
            $user->load('incubatee');
            $current_incubatee = $user->incubatee;
            $current_incubatee->load('uploads');
            $current_incubatee_uploads = $current_incubatee->uploads;

            if (isset($current_incubatee_uploads)) {
                $current_incubatee_uploads->update(['talent_results' => $input['talent_results']]);
            }else{
                $create_current_incubatee_uploads = IncubateeUpload::create(['talent_results' => $input['talent_results']]);
            }


            DB::commit();
            return response()->json(['message' => 'Updated talent dynamics results.'], 200);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' .$e->getMessage()], 400);
        }
    }


    //Incubatee Events Functions
    public function storeIncubateeEventViaPublicEventPage(Request $request)
    {
        $input = $request->all();
        $event_id = $input['event_id'];
        $incubatee_id = $input['incubatee_id'];
        $cur_incubatee = Incubatee::find($incubatee_id);
        $cur_event = Event::find($event_id);
        $date = Carbon::now();

        if (isset($cur_incubatee) and isset($cur_event)) {
            $cur_incubatee->load('events');

            foreach ($cur_incubatee->events as $visitor_event) {
                if ($visitor_event->event_id == $event_id) {
                    return response()->json(['message' => 'You are already registered for this event.']);
                }
            }

            try {
                DB::beginTransaction();

                $event_visitor = EventIncubatee::create(['incubatee_id' => $incubatee_id, 'event_id' => $event_id,
                    'registered' => true, 'date_time_registered' => $date, 'found_out_via' => $input['found_out_via']]);

                $event_visitor->save();

                DB::commit();

                return response()->json(['message' => 'Event now added to your registered list.']);

            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
            }
        } else {
            return response()->json(['message' => 'The system cannot pick up this event.']);
        }
    }

    public function incubateeEvents(Incubatee $incubatee)
    {
        $user = Auth::user()->load('roles');
        $incubatee->load('events');
        $events = Event::with('EventVenue')->orderBy('start', 'asc')->get();
        $incubatee_events = [];
        $incubatee_event_start_time = "";
        $all_events_array = [];
        $event_start_time = "";

        foreach ($incubatee->events as $incubatee_event) {
            $event_id = $incubatee_event->event_id;
            $cur_event = Event::find($event_id);

            if (isset($cur_event->start_time)) {
                for ($i = 0; $i < count_chars($cur_event->start_time); $i++) {
                    $user_event_start_time = $incubatee_event_start_time . $cur_event->start_time[$i];
                    if ($i == 4) {
                        break;
                    }
                }
            }

            if (isset($cur_event)) {
                $object = (object)['incubatee_event_registered' => $incubatee_event,'accepted' => 'No',
                    'attended' => 'No', 'incubatee_events_by_event_id' => $cur_event, 'incubatee_event_start_time' => $incubatee_event_start_time];
                $incubatee_event_start_time = "";
                array_push($incubatee_events, $object);
            }
        }

        foreach ($events as $event) {
            if (isset($event->start_time)) {
                for ($i = 0; $i < count_chars($event->start_time); $i++) {
                    $event_start_time = $event_start_time . $event->start_time[$i];
                    if ($i == 4) {
                        break;
                    }
                }
            }


            $object = (object)['event_start_time' => $event_start_time, 'event' => $event,'accepted' => 'No',
                'attended' => 'No',];
            $event_start_time = "";
            array_push($all_events_array, $object);
        }

        if ($user->roles[0]->name == 'incubatee') {
            return view('/users/incubatee-dashboard/full-calendar/show-events',
                compact('incubatee', 'events', 'incubatee_events', 'all_events_array','event'));
        } else {
            return response('You are not authorized!');
        }
    }

    public function deregisterIncubatee(Request $request)
    {
        $input = $request->all();
        $incubatee_id = $input['incubatee_id'];
        $event_id = $input['event_id'];
        $cur_incubatee = Incubatee::find($incubatee_id);
        $cur_incubatee->load('events');
        $date = Carbon::now();

        try {
            if (isset($cur_incubatee)) {

                DB::beginTransaction();

                foreach ($cur_incubatee->events as $incubatee_events) {
                    if ($incubatee_events->event_id == $event_id) {
                        $cur_event = Event::find($event_id);
                        $create_incubatee_deregister = IncubateeEventDeregister::create(['event_id' => $event_id, 'incubatee_id' => $cur_incubatee->id,
                            'de_registered' => true, 'de_register_reason' => $input['deregister_reason'], 'date_time_de_register' => $date, 'event_name' => $cur_event->title]);

                        $incubatee_events->forceDelete();

                        DB::commit();
                        return response()->json(['message' => 'You are now deregistered for that event.']);
                    }
                }
            } else {
                return response()->json(['message' => 'The system could not pick up the visitor.']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    public function storeIncubateeEventViaIncubateeEventPage(Request $request)
    {
        $input = $request->all();
        $event_id = $input['event_id'];
        $incubatee_id = $input['incubatee_id'];
        $cur_incubatee = Incubatee::find($incubatee_id);
        $cur_event = Event::find($event_id);
        $date = Carbon::now();

        if (isset($cur_incubatee) and isset($cur_event)) {
            $cur_incubatee->load('events');

            foreach ($cur_incubatee->events as $incubatee_event) {
                if ($incubatee_event->event_id == $event_id) {
                    return response()->json(['message' => 'You are already registered for this event.']);
                }
            }

            try {
                DB::beginTransaction();

                $event_visitor = EventIncubatee::create(['incubatee_id' => $incubatee_id, 'event_id' => $event_id,
                    'registered' => true, 'date_time_registered' => $date, 'found_out_via' => $input['found_out_via']]);

                $event_visitor->save();

                DB::commit();

                return response()->json(['message' => 'Event now added to your registered list.']);

            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
            }
        } else {
            return response()->json(['message' => 'The system cannot pick up this event.']);
        }
    }

    public function updateIncubateeEvent(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();
        $incubatee_id = $input['incubatee_id'];
        $cur_incubatee = Incubatee::find($incubatee_id);
        $cur_incubatee->load('events');
        $user = User::find(Auth::user()->id);

        if ($user != null) {
            $user->load('roles');

            if ($user->roles[0]->name == 'clerk') {
                DB::beginTransaction();
                try {
                    if ($request->has('event_id')) {
                        $event_id = $input['event_id'];
                        $check_event = Event::find($event_id);
                        $incubatee_events = EventIncubatee::where('incubatee_id', $incubatee_id)->get();

                        $bool_found = false;
                        foreach ($incubatee_events as $incubatee_event) {
                            if ($incubatee_event->event_id == $event_id) {
                                $bool_found = true;
                                $incubatee_event->attended = true;
                                $incubatee_event->save();
                                DB::commit();
                                return response()->json(['visitor' => $cur_incubatee, 'message' => 'Enjoy your event / workshop!'], 200);
                            }
                        }

                        if (!$bool_found) {
                            $create_incubatee_event = EventIncubatee::create(['event_id' => $event_id, 'incubatee_id' => $incubatee_id,
                                'attended' => true, 'registered' => true, 'de_registered' => false, 'date_time_registered' => $date]);
                            DB::commit();
                            response()->json(["hit" => $create_incubatee_event]);
                            return response()->json(['incubatee' => $cur_incubatee, 'message' => 'Enjoy your event / workshop!'], 200);
                        }

                    } else {
                        return response()->json(['message', 'Are you sure you chose and event?']);
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 500);
                }
            } else {
                return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function updateEventIncubatee(Request $request, EventIncubatee $eventIncubatee){
        $input = $request->all();
        $eventIncubatee->load('events');

        try {
            DB::beginTransaction();

            $eventIncubatee->update([
                'attended' => $input['attended'], 'registered' => $input['registered'],
                'de_register_reason'=> $input['de_register_reason'],'date_time_de_register'=> $input['date_time_de_register']
                ]);
            $eventIncubatee->save();
            DB::commit();
            return response()->json(['message' => 'Event Incubatee updated successfully.', 'eventIncubatee$eventIncubatee' => $eventIncubatee], 200);


        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Event incubatee could not be saved ' . $e->getMessage()], 400);
        }
    }

    //STORE INCUBATEE EVALUATION
    public function storeIncubateeEvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $incubatee_user = User::find($user_id);
            $incubatee_user->load('userQuestionAnswers', 'incubatee');


            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $incubatee_user->incubatee->update(['workshop_evaluation_complete' => true]);
                $incubatee_user->save();
            }


            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //STORE STAGE 3 INCUBATEE EVALUATION
    public function storeStage3IncubateeEvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $incubatee_user = User::find($user_id);
            $incubatee_user->load('userQuestionAnswers', 'incubatee');


            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $incubatee_user->incubatee->update(['stage3_workshop_complete' => true]);
                $incubatee_user->save();
            }


            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //STORE END OF STAGE 3 INCUBATEE EVALUATION
    public function storeEndOfStage3IncubateeEvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $incubatee_user = User::find($user_id);
            $incubatee_user->load('userQuestionAnswers', 'incubatee');


            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $incubatee_user->incubatee->update(['end_of_stage3_complete' => true]);
                $incubatee_user->save();
            }


            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //STORE STAGE 4 INCUBATEE EVALUATION
    public function storeStage4IncubateeEvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $incubatee_user = User::find($user_id);
            $incubatee_user->load('userQuestionAnswers', 'incubatee');


            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $incubatee_user->incubatee->update(['stage4_workshop_complete' => true]);
                $incubatee_user->save();
            }


            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //STORE STAGE 5 INCUBATEE EVALUATION
    public function storeStage5IncubateeEvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $incubatee_user = User::find($user_id);
            $incubatee_user->load('userQuestionAnswers', 'incubatee');


            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $incubatee_user->incubatee->update(['stage5_workshop_complete' => true]);
                $incubatee_user->save();
            }


            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //STORE STAGE 6 INCUBATEE EVALUATION
    public function storeStage6IncubateeEvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $incubatee_user = User::find($user_id);
            $incubatee_user->load('userQuestionAnswers', 'incubatee');


            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $incubatee_user->incubatee->update(['stage6_workshop_complete' => true]);
                $incubatee_user->save();
            }


            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }



    //Admin approve bootcamper panel interview
    public function adminApproveIncubateePanelInterview(Request $request, Incubatee $incubatee)
    {
        $input = $request->all();
        $selected_panelists = json_decode($input['selected_panelists']);
        $guest_panelists = json_decode($input['guest_panelists']);
        $panelist_role = Role::where('name', 'panelist')->first();
        $question_category_id = $input['question_category_id'];

        try {
            DB::beginTransaction();

            $incubatee->update(['contacted_via' => $input['contacted_via'], 'result_of_contact' => $input['result_of_contact']]);

            if(count($selected_panelists) > 0){
                foreach ($selected_panelists as $selected_panelist) {
                    //See if the actual user exists
                    $check_user = User::where('email', $selected_panelist)->first();

                    //if user does exist
                    if (isset($check_user)) {
                        //load relationships to be used
                        $check_user->load('roles');
                        //boolean to see if the user has panelist role
                        $check_role = false;

                        //check if user has role panelist
                        foreach ($check_user->roles as $user_role) {
                            if ($user_role->name == 'panelist') {
                                $check_role = true;
                            }
                        }

                        //if user has role panelist, do nothing, else attach role
                        if ($check_role == false) {
                            $check_user->roles()->attach($panelist_role->id);
                        }

                        //Variable to see if the panelist object with the same user id already exists
                        $check_panelist = $check_user->panelist()->exists();;

                        //Do if statement to see if the panelist with user id exists, else create
                        if ($check_panelist == true) {
                            $cur_panelist = $check_user->panelist;
                            $check_panelist_applicants = $cur_panelist->incubatee()->exists();

                            if ($check_panelist_applicants == true) {
                                $cur_panelist_applicants = $cur_panelist->applicants;

                                //boolean to see if panelist has applicant
                                $has_applicant = false;

                                //loop through panelist applicants to see if the applicant id matches the current applicant id
                                foreach ($cur_panelist_applicants as $panelist_applicant) {
                                    if ($panelist_applicant->bootcamper_id == $incubatee->id) {
                                        $has_applicant = true;
                                    }
                                }

                                //if panelist does not have current applicant, create applicantpanelist object
                                if ($has_applicant == false) {
                                    $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $cur_panelist->id,
                                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                                } else {
                                    $get_object = DB::table('incubatee_panelists')->where('incubatee_id', $incubatee->id)
                                        ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                            'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                                }
                            } else {
                                $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            }
                        } else {
                            $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                            $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $create_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                        }
                    } else {
                        $create_user = User::create(['email' => $selected_panelist, 'password' => Hash::make('secret')]);
                        $create_user->roles()->attach($panelist_role->id);
                        $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                        $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                    }
                }
            }


            if(count($guest_panelists) > 0){
                foreach ($guest_panelists as $guest_panelist) {
                    $check_user = User::where('email', $guest_panelist->email)->first();

                    //if user does exist
                    if (isset($check_user)) {
                        //load relationships to be used
                        $check_user->load('roles');
                        //boolean to see if the user has panelist role
                        $check_role = false;

                        //check if user has role panelist
                        foreach ($check_user->roles as $user_role) {
                            if ($user_role->name == 'panelist') {
                                $check_role = true;
                            }
                        }

                        //if user has role panelist, do nothing, else attach role
                        if ($check_role == false) {
                            $check_user->roles()->attach($panelist_role->id);
                        }

                        //Variable to see if the panelist object with the same user id already exists
                        $check_panelist = $check_user->panelist()->exists();;

                        //Do if statement to see if the panelist with user id exists, else create
                        if ($check_panelist == true) {
                            $cur_panelist = $check_user->panelist;
                            $check_panelist_applicants = $cur_panelist->incubatee()->exists();

                            if ($check_panelist_applicants == true) {
                                $cur_panelist_applicants = $cur_panelist->applicants;
                                //boolean to see if panelist has applicant
                                $has_applicant = false;

                                //loop through panelist applicants to see if the applicant id matches the current applicant id
                                foreach ($cur_panelist_applicants as $panelist_applicant) {
                                    if ($panelist_applicant->bootcamper_id == $incubatee->id) {
                                        $has_applicant = true;
                                    }
                                }

                                //if panelist does not have current applicant, create applicantpanelist object
                                if ($has_applicant == false) {
                                    $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $cur_panelist->id,
                                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                                } else {
                                    $get_object = DB::table('incubatee_panelists')->where('incubatee_id', $incubatee->id)
                                        ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                            'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                                }
                            } else {
                                $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $cur_panelist->id,
                                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                            }
                        } else {
                            $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                            $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $create_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                        }
                    } else {
                        $create_user = User::create(['name' => $guest_panelist->name, 'surname' => $guest_panelist->surname,
                            'contact_number' => $guest_panelist->contact_number, 'email' => $guest_panelist->email, 'password' => Hash::make('secret'),
                            'company_name' => $guest_panelist->company, 'position' => $guest_panelist->position, 'title' => $guest_panelist->title,
                            'initials' => $guest_panelist->initials]);
                        $create_user->roles()->attach($panelist_role->id);
                        $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                        $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $create_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'], 'question_category_id' => $question_category_id]);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully created panelists.'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }

    //Update panel interview
    public function adminUpdatePanelInterviewDetailsOfIncubatee(Request $request, Incubatee $incubatee){
        $input = $request->all();
        $incubatee->load('panelists');
        $incubatee_panelists = $incubatee->panelists;

        try{
            DB::beginTransaction();
            $incubatee->update(['contacted_via' => $input['contact'],
                'result_of_contact' => $input['contact_result']]);

            if(count($incubatee_panelists) > 0){
                foreach ($incubatee_panelists as $applicant_panelist){
                    $applicant_panelist->panel_selection_date = $input['date'];
                    $applicant_panelist->panel_selection_time = $input['time'];
                    $applicant_panelist->question_category_id = $input['question_category_id'];
                    $applicant_panelist->save();
                }
            }

            DB::commit();
            return response()->json(['message' => 'Updated successfully!']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: '.$e->getMessage()], 400);
        }
    }

    //Append Panel
    public function adminAddPanelistFromPanelAccessWindowFoIncubatee(Request $request){
        $input = $request->all();
        $incubatee_id = $input['incubatee_id'];
        $panelist_role = Role::where('name', 'panelist')->first();
        $incubatee = Incubatee::find($incubatee_id);
        $incubatee->load('panelists');

        try{
            DB::beginTransaction();
            //See if the actual user exists
            $check_user = User::where('email', $input['email'])->first();

            //if user does exist
            if (isset($check_user)) {
                //load relationships to be used
                $check_user->load('roles');
                //boolean to see if the user has panelist role
                $check_role = false;

                //check if user has role panelist
                foreach ($check_user->roles as $user_role) {
                    if ($user_role->name == 'panelist') {
                        $check_role = true;
                    }
                }

                //if user has role panelist, do nothing, else attach role
                if ($check_role == false) {
                    $check_user->roles()->attach($panelist_role->id);
                }

                //Variable to see if the panelist object with the same user id already exists
                $check_panelist = $check_user->panelist()->exists();;

                //Do if statement to see if the panelist with user id exists, else create
                if ($check_panelist == true) {
                    $cur_panelist = $check_user->panelist;
                    $check_panelist_applicants = $cur_panelist->incubatee()->exists();

                    if ($check_panelist_applicants == true) {
                        $cur_panelist_applicants = $cur_panelist->incubatee;

                        //boolean to see if panelist has applicant
                        $has_applicant = false;

                        //loop through panelist applicants to see if the applicant id matches the current applicant id
                        foreach ($cur_panelist_applicants as $panelist_applicant) {
                            if ($panelist_applicant->bootcamper_id == $incubatee->id) {
                                $has_applicant = true;
                            }
                        }

                        //if panelist does not have current applicant, create applicantpanelist object
                        if ($has_applicant == false) {
                            $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $cur_panelist->id,
                                'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                                'question_category_id' => $incubatee->panelists[0]->question_category_id]);
                        } else {
                            $get_object = DB::table('incubatee_panelists')->where('incubatee_id', $incubatee->id)
                                ->where('panelist_id', $cur_panelist->id)->update(['panel_selection_date' => $input['panel_selection_date'],
                                    'panel_selection_time' => $input['panel_selection_time'],
                                    'question_category_id' => $incubatee->panelists[0]->question_category_id]);
                        }
                    } else {
                        $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $cur_panelist->id,
                            'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                            'question_category_id' => $incubatee->panelists[0]->question_category_id]);
                    }
                } else {
                    $create_panelist = Panelist::create(['user_id' => $check_user->id]);
                    $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $create_panelist->id,
                        'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                        'question_category_id' => $incubatee->panelists[0]->question_category_id]);
                }
            } else {
                $create_user = User::create(['email' => $input['email'], 'password' => Hash::make('secret')]);
                $create_user->roles()->attach($panelist_role->id);
                $create_panelist = Panelist::create(['user_id' => $create_user->id]);
                $create_panelist_applicant = IncubateePanelist::create(['incubatee_id' => $incubatee->id, 'panelist_id' => $create_panelist->id,
                    'panel_selection_date' => $input['panel_selection_date'], 'panel_selection_time' => $input['panel_selection_time'],
                    'question_category_id' => $incubatee->panelists[0]->question_category_id]);
            }
            DB::commit();
            return response()->json(['message' => 'Successfully added panelist'], 200);
        } catch (\Exception $e){
            return response()->json(['message' => 'Something went wrong: '. $e->getMessage()], 400);
        }
    }

    //Bootcamper Contact Log
    public function storeIncubateeContactLog(Request $request,Incubatee $incubatee)
    {
        DB::beginTransaction();
        $input = $request->all();

        try {
            $contact_log = IncubateeContactLog::create(['comment' => $input['comment'], 'date' => $input['date'],
                'incubatee_contact_results' => $input['incubatee_contact_results'],
                'incubatee_id' => $incubatee->id]);

            $contact_log->save();


            DB::commit();
            return response()->json(['message' => 'Log added successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    //Panelist show ict applicants view
    public function panelistShowIncubatees(){
        $logged_in_user = Auth::user()->load('roles');

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.show-panel-incubatee',compact('logged_in_user'));
        }else if ($logged_in_user->roles[0]->name == 'panelist') {
            return view('users.panelist.show-panel-incubatee',compact('logged_in_user'));
        }
        else {
            return response('You are not authorized!');
        }
    }

    //Get ICT Incubatee panelist
    public function panelistGetICTIncubatees(User $user){
        $user->load('panelist');
        $user_panelist = $user->panelist;
        $user_panelist->load('incubatee');
        $panelist_incubatee = $user_panelist->incubatee;
        $panelist_incubatee_array = [];
        $today = Carbon::now()->toDateString();

        foreach ($panelist_incubatee as $panelist_incubatees) {
            if ($today <= $panelist_incubatees->panel_selection_date) {
                $incubatee_id = $panelist_incubatees->incubatee_id;
                $panel_selection_date = $panelist_incubatees->panel_selection_date;
                $searched_incubatee = Incubatee::find($incubatee_id);
                $incubatee_user_id = $searched_incubatee->user_id;

                $object = (object)['name' => $searched_incubatee->user->name, 'surname' => $searched_incubatee->user->surname,
                    'email' => $searched_incubatee->user->email, 'chosen_category' => $searched_incubatee->user->applicant->chosen_category,
                    'selection_date' => $panel_selection_date, 'incubatee_id' => $searched_incubatee->id,
                    'panelist_id' => $panelist_incubatees->panelist_id, 'incubatee_user_id' => $incubatee_user_id];
                array_push($panelist_incubatee_array, $object);
            }
        }


        return Datatables::of($panelist_incubatee_array)->addColumn('action', function ($panelist_applicant) {
            $applicant_questions = '/panelist-view-ict-applicant-questions/' . $panelist_applicant->incubatee_user_id;
            $applicant_score_sheet = '/show-incubatee-score-sheet/' . $panelist_applicant->incubatee_id;
            return '<a href=' . $applicant_questions . ' title="Show Applicant Questions" style="color:blue!important;"><i class="material-icons">remove_red_eye</i></a><a href=' . $applicant_score_sheet . ' title="Show Applicant Score Sheet" style="color:orange!important;"><i class="material-icons">assignment_ind</i></a>';
        })
            ->make(true);
    }

    //Show incubatee score sheet
    public function showIncubateePanelScoreSheet(Incubatee $incubatee){
        $incubatee->load('user');
        $logged_in_user = Auth::user()->load('roles', 'panelist');
        $applicant_panelist = DB::table('incubatee_panelists')->where('incubatee_id', $incubatee->id)
            ->where('panelist_id', $logged_in_user->panelist->id)->first();
        $question_category_id = $applicant_panelist->question_category_id;
        $question_category = QuestionsCategory::find($question_category_id);
        $questions = $question_category->questions->sortBy('question_number');
        $questions->load('questionSubTexts');
        $cur_applicant_panelist = IncubateePanelist::find($applicant_panelist->id);
        $cur_applicant_panelist->load('panelistQuestionAnswers');
        $cur_panelist_question_answers = [];

        if(count($cur_applicant_panelist->panelistQuestionAnswers) > 0){
            foreach($cur_applicant_panelist->panelistQuestionAnswers as $pqa){
                $question_id = $pqa->question_id;
                $question = Question::find($question_id);
                $question_number = $question->question_number;
                $question_text = $question->question_text;
                $question_sub_texts = [];
                $question->load('questionSubTexts');
                if(count($question->questionSubTexts) > 0){
                    foreach($question->questionSubTexts as $qst){
                        $object = (object)['question_sub_text' => $qst->question_sub_text];
                        array_push($question_sub_texts, $object);
                    }
                }
                $score = $pqa->score;
                $comment = $pqa->comment;

                $object = (object)['question_id' => $question_id, 'question_sub_texts' => $question_sub_texts,
                    'score' => $score, 'comment' => $comment, 'question_number' => $question_number, 'question_text' => $question_text];
                array_push($cur_panelist_question_answers, $object);
            }
        }

        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('/users/incubatees/admin-incubatees/show-incubatee-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'incubatee'));
        } else if ($logged_in_user->roles[0]->name == 'panelist') {
            return view('users.panelist.show-incubatee-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'incubatee'));
        } else if ($logged_in_user->roles[0]->name == 'administrator') {
            return view('/users/administrators/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('/users/advisor-dashboard/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if ($logged_in_user->roles[0]->name == 'incubatee') {
            return view('/users/incubatee-dashboard/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if ($logged_in_user->roles[0]->name == 'mentor') {
            return view('/users/mentor-dashboard/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else if($logged_in_user->roles[0]->name == 'marketing'){
            return view('/users/marketing-dashboard/bootcampers/show-applicant-score-sheet', compact('applicant_panelist',
                'question_category', 'questions', 'cur_applicant_panelist', 'cur_panelist_question_answers', 'bootcamper'));
        } else {
            return response("You are not authorized!!");
        }
    }

    //Get applicant and bootcamp interview
    public function getApplicantAndIncubateeInterview(Incubatee $incubatee){
        $logged_in_user = Auth::user()->load('roles', 'panelist');
        $incubatee->load('user');
        $user = $incubatee->user;
        $user->load('applicant', 'userQuestionAnswers','bootcamper');
        $bootcamper = $user->bootcamper;
        $user_applicant = $user->applicant;
        $applicant = $user->applicant;
        $applicant->load('panelists');
        $applicantion_panelists = [];
        $question_categories = QuestionsCategory::all();

        $applicant_panelists = $applicant->panelists;
        $final_panelist_question_score_array = [];
        $panelists_question_score_collection = [];
        $final_question_numbers = [];
        $total_panelist_score = 0;
        $panelist_count = 0;
        foreach($applicant_panelists as $applicant_panelist){
            $applicant_panelist->load('panelist');
            $cur_panelist = $applicant_panelist->panelist;
            $cur_panelist->load('user');
            $cur_user = $cur_panelist->user;
            $cur_user_name = $cur_user->name;
            $cur_user_surname = $cur_user->surname;
            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;

            $panelist_question_answers = PanelistQuestionAnswer::where('applicant_panelist_id', $applicant_panelist->id)->get();

            foreach ($panelist_question_answers as $panelist_question_answer){
                $question_id = $panelist_question_answer->question_id;
                $cur_question = Question::find($question_id);
                $cur_question_number = $cur_question->question_number;
                $cur_score = $panelist_question_answer->score;
                $panelist_applicant_score += $cur_score;
                $object = (object)['question_number' => $cur_question_number,
                    'question_score' => $cur_score];
                array_push($panelist_questions_scores, $object);
                array_push($panelists_question_score_collection, $object);
                array_push($final_question_numbers, $cur_question_number);
            }

            $object = (object)['name' => $cur_user_name, 'surname' => $cur_user_surname,
                'user_questions_scores' => $panelist_questions_scores, 'panelist_applicant_score' => $panelist_applicant_score,
                'applicant_panelist_id' => $applicant_panelist->id];
            array_push($final_panelist_question_score_array, $object);

            $total_panelist_score += $panelist_applicant_score;

            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;
        }

        foreach ($final_panelist_question_score_array as $pqsa){
            if(count($pqsa->user_questions_scores)){
                $panelist_count += 1;
            }
        }

        if($panelist_count > 0){
            $average_panelist_score = $total_panelist_score / $panelist_count;
        } else {
            $average_panelist_score = 0;
        }
        $question_scores_array = [];
        $question_score = 0;

        foreach (array_unique($final_question_numbers) as $q_number){
            foreach ($applicant_panelists as $applicant_panelist) {
                $applicant_panelist->load('panelistQuestionAnswers');
                $applicant_panelist_questions = $applicant_panelist->panelistQuestionAnswers;
                foreach ($applicant_panelist_questions as $applicant_panelist_question){
                    $question_id = $applicant_panelist_question->question_id;
                    $cur_question = Question::find($question_id);
                    if($cur_question->question_number == $q_number){
                        $question_score += $applicant_panelist_question->score;
                    }
                }
            }
            $object = (object)['question_number' => $q_number, 'question_score' => $question_score];
            array_push($question_scores_array, $object);
            $question_score = 0;
        }

        //Get applicant Panelist on incubatee account
        $panelists = [];
        foreach ($applicant->panelists as $applicant_panelist) {
            $panelist_id = $applicant_panelist->panelist_id;
            $panelist = Panelist::find($panelist_id);
            $panelist_user_id = $panelist->user_id;
            $user = User::find($panelist_user_id);

            $object = (object)['name' => $user->name, 'surname' => $user->surname, 'email' => $user->email,
                'selection_date' => $applicant_panelist->panel_selection_date, 'panelist_id' => $panelist_id,
                'applicant_panelist_id' => $applicant_panelist->id];
            array_push($panelists, $object);
        }


        //BOOTCAMPER PANEL SCRORE
        $bootcamper->load('user', 'panelists');
        $bootcamper_user = $bootcamper->user;
        $bootcamper_user->load('applicant');
        $applicant = $bootcamper_user->applicant;
        $bootcamp_panelists = $bootcamper->panelists;
        $bootcamp_final_panelist_question_score_array = [];
        $panelists_question_score_collection = [];
        $final_question_numbers = [];
        $bootcamp_total_panelist_score = 0;
        $bootcamp_panelist_count = 0;

        foreach($bootcamp_panelists as $applicant_panelist){
            $applicant_panelist->load('panelist');
            $cur_panelist = $applicant_panelist->panelist;
            $cur_panelist->load('user');
            $cur_user = $cur_panelist->user;
            $cur_user_name = $cur_user->name;
            $cur_user_surname = $cur_user->surname;
            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;

            $panelist_question_answers = IctPanelistQuestionAnswer::where('bootcamper_panelist_id', $applicant_panelist->id)->get();

            foreach ($panelist_question_answers as $panelist_question_answer){
                $question_id = $panelist_question_answer->question_id;
                $cur_question = Question::find($question_id);
                $cur_question_number = $cur_question->question_number;
                $cur_score = $panelist_question_answer->score;
                $panelist_applicant_score += $cur_score;
                $object = (object)['question_number' => $cur_question_number,
                    'question_score' => $cur_score];
                array_push($panelist_questions_scores, $object);
                array_push($panelists_question_score_collection, $object);
                array_push($final_question_numbers, $cur_question_number);
            }

            $object = (object)['name' => $cur_user_name, 'surname' => $cur_user_surname,
                'user_questions_scores' => $panelist_questions_scores, 'panelist_applicant_score' => $panelist_applicant_score,
                'applicant_panelist_id' => $applicant_panelist->id];
            array_push($bootcamp_final_panelist_question_score_array, $object);

            $bootcamp_total_panelist_score += $panelist_applicant_score;

            $panelist_questions_scores = [];
            $panelist_applicant_score = 0;
        }

        foreach ($bootcamp_final_panelist_question_score_array as $pqsa){
            if(count($pqsa->user_questions_scores)){
                $bootcamp_panelist_count += 1;
            }
        }

        if($bootcamp_panelist_count > 0){
            $bootcamp_average_panelist_score = $bootcamp_total_panelist_score / $bootcamp_panelist_count;
        } else {
            $bootcamp_average_panelist_score = 0;
        }
        $bootcamp_question_scores_array = [];
        $bootcamp_question_score = 0;

        foreach (array_unique($final_question_numbers) as $q_number){
            foreach ($applicant_panelists as $applicant_panelist) {
                $applicant_panelist->load('panelistQuestionAnswers');
                $applicant_panelist_questions = $applicant_panelist->panelistQuestionAnswers;
                foreach ($applicant_panelist_questions as $applicant_panelist_question){
                    $question_id = $applicant_panelist_question->question_id;
                    $cur_question = Question::find($question_id);
                    if($cur_question->question_number == $q_number){
                        $bootcamp_question_score += $applicant_panelist_question->score;
                    }
                }
            }
            $object = (object)['question_number' => $q_number, 'question_score' => $bootcamp_question_score];
            array_push($bootcamp_question_scores_array, $object);
            $bootcamp_question_score = 0;
        }


        if ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.incubatees.admin-incubatees.applicant-and-bootcamp-interviews', compact('incubatee', 'applicant', 'user_applicant',
                'question_categories', 'applicantion_panelists', 'final_panelist_question_score_array', 'applicant',
                'total_panelist_score', 'average_panelist_score', 'question_scores_array', 'question_score', 'panelists',
                'bootcamp_panelists', 'bootcamp_final_panelist_question_score_array', 'bootcamper', 'bootcamp_total_panelist_score',
                'bootcamp_average_panelist_score', 'bootcamp_question_scores_array', 'bootcamp_question_score'));
        }elseif  ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.incubatees.applicant-and-bootcamp-interviews', compact('incubatee', 'applicant', 'user_applicant',
                'question_categories', 'applicantion_panelists', 'final_panelist_question_score_array', 'applicant',
                'total_panelist_score', 'average_panelist_score', 'question_scores_array', 'question_score', 'panelists',
                'bootcamp_panelists', 'bootcamp_final_panelist_question_score_array', 'bootcamper', 'bootcamp_total_panelist_score',
                'bootcamp_average_panelist_score', 'bootcamp_question_scores_array', 'bootcamp_question_score'));
        }else if ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.incubatees.applicant-and-bootcamp-interviews', compact('incubatee', 'applicant', 'user_applicant',
                'question_categories', 'applicantion_panelists', 'final_panelist_question_score_array', 'applicant',
                'total_panelist_score', 'average_panelist_score', 'question_scores_array', 'question_score', 'panelists',
                'bootcamp_panelists', 'bootcamp_final_panelist_question_score_array', 'bootcamper', 'bootcamp_total_panelist_score',
                'bootcamp_average_panelist_score', 'bootcamp_question_scores_array', 'bootcamp_question_score'));
        }

    }


    //Save panelist incubatee question and answers
    public function panelistSaveIncubateeQuestionAnswerComments(Request $request){
        $input = $request->all();
        $questions_answers_comments_array = json_decode($input['questions_answers_comments']);
        $incubatee_panelist_id = $questions_answers_comments_array[0]->incubatee_panelist_id;
        $applicant_panelist = IncubateePanelist::find($incubatee_panelist_id);
        $applicant_panelist->load('panelistQuestionAnswers');

        try {
            if(isset($applicant_panelist->panelistQuestionAnswers)){
                foreach ($questions_answers_comments_array as $item) {
                    $find_question_answer = DB::table('inc_panelist_question_answers')->where('incubatee_panelist_id', $incubatee_panelist_id)
                        ->where('question_id', $item->question_id)->first();

                    if(isset($find_question_answer)){
                        $score = (integer)$item->answer_score;
                        DB::table('inc_panelist_question_answers')->where('incubatee_panelist_id', $incubatee_panelist_id)
                            ->where('question_id', $item->question_id)->update(['score' => $score, 'comment' => $item->comment]);
                    } else {
                        $score = (integer)$item->answer_score;
                        $create_object = IncPanelistQuestionAnswer::create(['question_id' => $item->question_id, 'incubatee_panelist_id' => $item->incubatee_panelist_id,
                            'score' => $score, 'comment' => $item->comment]);
                    }
                }
            } else {
                foreach ($questions_answers_comments_array as $item) {
                    $score = (integer)$item->answer_score;
                    $create_object = IncPanelistQuestionAnswer::create(['question_id' => $item->question_id, 'incubatee_panelist_id' => $item->incubatee_panelist_id,
                        'score' => $score, 'comment' => $item->comment]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Completed!'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => 'Something went wrong: ' . $e->getMessage()]);
        }
    }


    //Update incubatee POPI Compliance
    public function updateIncubateePopiCompliance(Request $request,Incubatee $incubatee)
    {
        DB::beginTransaction();

        try {
            $incubatee->incubatee_popi_act_agreement = 'Yes';
            $incubatee->popi_is_agreed = true;

            $incubatee->save();

            DB::commit();
            return  'Thank you !' . $incubatee->user->name;
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    //Update NDA agreement
    public function IncubateeNDAagreement(Request $request,Incubatee $incubatee)
    {
        DB::beginTransaction();

        try {
            $incubatee->nda_agreement = 'Yes';
            $incubatee->nda_agreed = true;

            $incubatee->save();

            DB::commit();
            return  'Thank you !' . $incubatee->user->name;

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }


    /*STORE PRE STAGE 2 EVALUATION FORM*/
    public function storePreStage2EvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'incubatee');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->incubatee->update(['pre_stage_two_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    /*STORE POST STAGE 2 EVALUATION FORM*/
    public function storePostStage2EvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'incubatee');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->incubatee->update(['post_stage_two_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    /*STORE PRE STAGE 3 EVALUATION FORM*/
    public function storePreStage3EvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'incubatee');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->incubatee->update(['pre_stage_three_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    /*STORE POST STAGE 3 EVALUATION FORM*/
    public function storePostStage3EvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'incubatee');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->incubatee->update(['post_stage_three_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    /*STORE PRE STAGE 4 EVALUATION FORM*/
    public function storePreStage4EvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'incubatee');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->incubatee->update(['pre_stage_four_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    /*STORE POST STAGE 4 EVALUATION FORM*/
    public function storePostStage4EvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'incubatee');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->incubatee->update(['post_stage_four_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }


    /*STORE PRE STAGE 5 EVALUATION FORM*/
    public function storePreStage5EvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'incubatee');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->incubatee->update(['pre_stage_five_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    /*STORE POST STAGE 4 EVALUATION FORM*/
    public function storePostStage5EvaluationAnswers(Request $request)
    {
        try {
            $input = $request->all();

            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'incubatee');

            foreach ($questions_answers as $question_answer) {
                $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                    'answer_text' => $question_answer->answer_text]);
                $create_question_answer->save();
                $applicant_user->incubatee->update(['post_stage_five_complete' => true]);
                $applicant_user->save();
            }

            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //==========================Incubatee Go /  No Go section
    //This is where the admin declines a bootcamper and sets them to the declined bootcamper table
    // ---- This is quite a big function as there are many relationships that need to be deleted / created
}

