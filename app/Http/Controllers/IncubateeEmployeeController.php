<?php

namespace App\Http\Controllers;

use App\Event;
use App\Incubatee;
use App\IncubateeEmployee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;

class IncubateeEmployeeController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('/users/incubatees/admin-job/index');
        } /*else {
            return view('users.incubatees.create');
        }*/
    }



    public function getIncubateeEmployee()
    {
        /*$incubatee = Incubatee::find($id);
        $incubateeEmployee = Incubatee::with('incubateeEmployee')->where('id', $incubatee)->get();*/
        $incubateeEmployee = IncubateeEmployee::with('incubatee')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($incubateeEmployee)->addColumn('action', function ($incubateeEmployee) {
                $re = 'users.incubatees.admin-job.edit/' . $incubateeEmployee->id;
                $del = '/delete/incubateeEmployee/' . $incubateeEmployee->id;
                return '<a href=' . $re . ' title="Edit Employee" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Employee" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function createIncubateeEmployee(Incubatee $incubatee)
    {
        $incubatee->where('user_id', $incubatee->id)->first();
        /*dd($incubatee);*/
        $incubateeEmployee = Incubatee::with('incubateeEmployee');
        return view('/users/incubatees/admin-job/create', compact('incubateeEmployee', 'incubatee'));
    }

    public function storeIncubateeEmployee(Request $request, Incubatee $incubatee)
    {

        DB::beginTransaction();
        try {
            $input = $request->all();
            if ($request->has('user_id')) {
                $incubatee->where('user_id', $incubatee->id)->first();

                $save_incEmployee = IncubateeEmployee::create([/*'incubatee_id' => $incubatee->id,*/ 'employee_name' => $input['employee_name'], 'position' => $input['position'],
                    'start_date' => $input['start_date'], 'end_date' => $input['end_date']]);

                if ($request->hasFile('contract_url')) {
                    $contact_url_path = $request->file('contract_url')->store('incubatee_uploads');
                    if (isset($incubatee)) {
                        $save_incEmployee->update(['contract_url' => $contact_url_path]
                        );
                    } else {
                        $save_incEmployee->incubateeEmployee()->create(['contract_url' => $contact_url_path]
                        );
                    }
                }
                /*$save_incEmployee->incubatee()->attach($input['incubatee_id']);
                $save_incEmployee = $save_incEmployee->load('incubatee');*/

                DB::commit();
                return response()->json(['message'=>'Successfully added Employee.','save_incEmployee'=>$save_incEmployee, 'incubatee'=>$incubatee],200);
            }
           /* else{
                return ('Not working');
            }*/
        }
        catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function editIncubateeEmployee()
    {

        $incubateeEmployee = Incubatee::with('incubateeEmployee');
        return view('/users/incubatees/admin-job/edit', compact('incubateeEmployee'));
    }

    public function updateIncubateeEmployee(Request $request, IncubateeEmployee $incubateeEmployees){
        DB::beginTransaction();
        $cur = $incubateeEmployees;
        try {
            $input = $request->all();


            $incubateeEmployees->update(['employee_name'=>$input['employee_name'], 'position'=>$input['position'],
                'start_date'=>$input['start_date'], 'end_date'=>$input['end_date']]);
            if ($request->hasFile('contract_url')) {
                $contact_url_path = $request->file('contract_url')->store('incubatee_uploads');
                if (isset($incubatee)) {
                    $cur->update(['contract_url' => $contact_url_path]
                    );
                } else {
                    $cur->incubateeEmployee()->create(['contract_url' => $contact_url_path]
                    );
                }
            }
            DB::commit();
            return response()->json(['message'=>'Employee successfully updated', 'save_incEmployee'=>$incubateeEmployees]);
        }
        catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function destroy($id){
        $incEmployee = IncubateeEmployee::find($id);
//        dd($event);
        $incEmployee->delete();

        return view('users.incubatees.admin-job.index');
    }


}
