<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Incoming\Answer;


class BotManController extends Controller
{

    public function handle()
    {

        $botman = app('botman');

        $botman->hears('{message}', function ($botman, $message) {

            if ($message == 'hi' || $message == 'Hi' || $message =='Hi Bulb' || $message =='hi Bulb') {
                $this->askName($botman);
            } elseif ($message == 'hey'|| $message == 'Hey') {
                $this->askName($botman);
            } elseif ($message == 'hello' || $message == 'Hello') {
                $this->askName($botman);
            };


            if ($message == '1') {
                $this->Q1($botman);
            }else if($message == '2'){
                $this->Q2($botman);
            }else if($message == '3'){
                $this->Q3($botman);
            }else if($message == '4'){
                $this->Q4($botman);
            } else if($message == '5'){
                $this->Q5($botman);
            } else if($message == '6'){
                $this->Q6($botman);
            }else if($message == '7'){
                $this->Q7($botman);
            }else if($message == '8'){
                $this->Q8($botman);
            } else if($message == '9'){
                $this->Q9($botman);
            } else if($message == '10'){
                $this->Q10($botman);
            }else if($message == '11'){
                $this->Q11($botman);
            }else if($message == '12'){
                $this->Q12($botman);
            }else if($message == '13'){
                $this->Q13($botman);
            } else if($message == '14'){
                $this->Q14($botman);
            } else if($message == '15'){
                $this->Q15($botman);
            }else if($message == '16'){
                $this->Q16($botman);
            }else if($message == '17'){
                $this->Q17($botman);
            } else if($message == '18'){
                $this->Q18($botman);
            } else if($message == '19'){
                $this->Q19($botman);
            }else if($message == '20'){
                $this->Q20($botman);
            }
        });
        $botman->listen();
    }

    public function askName($botman)
    {

        $botman->ask('Hello! What is your Name?', function (Answer $answer) {

            $name = $answer->getText();

            $this->say('Nice to meet you '  . $name .' 👍' );
            $this->say('How may i help you? :(Please reply with a Question number
                    <br><br> 1. What is an incubator? <br>
                    <br>2. If I have just an idea, will I be considered for incubation? <br>
                    <br> 3. Do you provide funding?
                    <br><br>4. What types of businesses do you help?
                    <br><br>5. Do you do business plans?
                    <br><br>6. Can you help with my business plan?
                    <br><br>7. Do you provide equipment i.e. computers/laptop?
                    <br><br>8. Do you provide office space?
                    <br><br>9. Do you sign NDA’s?
                    <br>10. What if you steal my business idea?
                    <br><br>11. How do I apply?
                    <br><br>12. When will I know if my application is successful?
                    <br><br>13. Can you help me develop (software) my product?
                    <br><br>14. Do you support existing businesses?
                    <br><br>15. How long is the programme?
                    <br><br>16. How much does the programme cost?
                    <br><br>17. Where are you located?
                    <br><br>18. Who funds the programmes?
                    <br><br>19. Will you take a stake in my business?
                    <br><br>20. Are you hiring? ');
        });
    }


    public function Q1($botman)
    {
        $botman->say('<b>What is an incubator?</b> <br><br> A business incubator is a company that assists start-up companies to develop their products, businesses and markets by providing a range of shared services and infrastructure.', function (Answer $answer) {
        });

    }
    public function Q2($botman)
    {
        $botman->say('<b>If I have just an idea, will I be considered for incubation?</b><br><br>When running ICT cohorts, software apps and tech-related ideas will be considered. When wanting to develop a product or technology, only a proof of concept or early prototype will be considered.', function (Answer $answer) {
        });

    }
    public function Q3($botman)
    {
        $botman->say('<b>Do you provide funding?</b><br><br>Propella does have access to grant funding streams. ', function (Answer $answer) {
        });

    }
    public function Q4($botman)
    {
        $botman->say('<b>What types of businesses do you help?</b><br><br>We assist business in the focus areas listed below:<br>
                <br><br>• Renewable energy – Renewable energy is energy that is collected from renewable resources, which are naturally replenished on a human timescale, such as sunlight, wind, rain, tides, waves, and geothermal heat
                <br><br>•	Energy efficiency – targets projects that reduces the amount of energy required to deliver on a particular outcome, thus reducing carbon emissions and greenhouse gases
                <br><br>• Industry 4.0 and advanced manufacturing – Advanced manufacturing is the use of innovative technology and processes to improve products or processes. Industry 4.0 related to the merging of the physical and virtual environments by deploying a range of advanced technologies
                <br><br>• Internet of Things – IoT is simply the network of interconnected things/devices which are embedded with sensors, software, network connectivity and necessary electronics that enables them to collect and exchange data
                <br><br>• Smart City solutions – any technologies that will make the city a cleaner, better, safer and more efficient place in which to live
                <br><br>• Information & Communication Technology (ICT) - relates to software development and apps that are aligned to smart cities and the Industry 4.0 environment', function (Answer $answer) {
        });

    }
    public function Q5($botman)
    {
        $botman->say('<b>Do you do business plans?</b><br><br>We won’t write your business plan for you, but if you are part of one of our programmes, we will take you through a development
           programme that will equip you to thoroughly understand the ins and outs of your business, placing you at a point where
           you will be empowered to write your own business plan.. ', function (Answer $answer) {
        });

    }

    public function Q6($botman)
    {
        $botman->say('<b>Can you help with my business plan?</b><br><br>See above. ', function (Answer $answer) {
        });

    }
    public function Q7($botman)
    {
        $botman->say('<b>Do you provide equipment i.e. computers/laptop?</b><br><br>No. Please ensure you have your own computer equipment. We can however offer you connectivity in the form of uncapped Wi-Fi, telephone access and printing services.. ', function (Answer $answer) {
        });

    }
    public function Q8($botman)
    {
        $botman->say('<b>Do you provide office space?</b><br><br>We offer co-working spaces in the form of a hot desking area which is open to
 all our Incubatees, and a clean production environment for those people who are developing a new technology or product. There may be nominal costs attached to this.There may be nominal costs attached to this. ', function (Answer $answer) {
        });

    }
    public function Q9($botman)
    {
        $botman->say('<b>Do you sign NDA’s?</b><br><br>Yes.. ', function (Answer $answer) {
        });

    }
    public function Q10($botman)
    {
        $botman->say('<b>What if you steal my business idea?</b><br><br>We have been exposed to hundreds of new ideas. We assure you we won’t steal your idea. Therefore, we don’t mind signing a non-disclosure agreement.. ', function (Answer $answer) {
        });

    }
    public function Q11($botman)
    {
        $botman->say('<b>How do I apply?</b><br><br>All applications are done online. Click the “Apply” tab on the top right-hand corner and follow the prompts. ', function (Answer $answer) {
        });

    }
    public function Q12($botman)
    {
        $botman->say('<b>When will I know if my application is successful?</b><br><br>You will be notified via email and then called to come in for a panel interview where you will pitch your business idea in person.. ', function (Answer $answer) {
        });

    }
    public function Q13($botman)
    {
        $botman->say('<b>Can you help me develop (software) my product?</b><br><br>It would be dependent on the nature of the venture, equipment required and the cost.. ', function (Answer $answer) {
        });

    }
    public function Q14($botman)
    {
        $botman->say('<b>Do you support existing businesses?</b><br><br>Yes, as long as it falls within our scope of business and they are working on a new product or technology. ', function (Answer $answer) {
        });

    }
    public function Q15($botman)
    {
        $botman->say('<b>How long is the programme?</b><br><br>We have an ICT and Industrial programme running concurrently.
        The industrial programme takes a period of 3years and the ICT programme runs for 2 days to 12 months depending on how you progress within the programme. ', function (Answer $answer) {
        });

    }
    public function Q16($botman)
    {
        $botman->say('<b>How much does the programme cost?</b><br><br>There are Incubation and rental fees. This is dependent on the programme that you select. ', function (Answer $answer) {
        });

    }
    public function Q17($botman)
    {
        $botman->say('<b>Where are you located?</b><br><br>Our building is located at 07 Oakworth Road, South End. We’re right behind Virgin Active in Humewood. ', function (Answer $answer) {
        });

    }
    public function Q18($botman)
    {
        $botman->say('<b>Who funds the programmes?</b><br><br>We have a variety of funders to developing technology solutions. ', function (Answer $answer) {
        });

    }
    public function Q19($botman)
    {
        $botman->say('<b>Will you take a stake in my business?</b><br><br>Propella will not take direct equity in any business, but make take a royalty proportion of turnover, should you make use of any of its proprietary technology when prototyping. ', function (Answer $answer) {
        });

    }
    public function Q20($botman)
    {
        $botman->say('<b>Are you hiring?</b><br><br>Industrial incubation is always open for Entrepreneurs, but a cohort for ICT will commence from February 2020. We unfortunately dont have any permanent office positions available at Propella at the moment.  ', function (Answer $answer) {
        });

    }



}
