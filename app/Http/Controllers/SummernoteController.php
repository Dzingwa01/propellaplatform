<?php

namespace App\Http\Controllers;

use App\Career;
use App\CareerSummernote;
use App\LatestInfo;
use App\LatestInfoSummernote;
use App\LibraryPdf;
use App\PropellaNew;
use App\PropellaNewsletterContent;
use App\Summernote;
use App\Blog;
use App\Role;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;



class SummernoteController extends Controller{


    //create blog summernote
    public function summernote()
    {

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users/app-admin/Summernote/summernote');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users/app-admin/Summernote/summernote');
        }elseif ($user->roles[0]->name == 'administrator'){
            return view('users/app-admin/Summernote/summernote');
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users/advisor-dashboard/Summernote/summernote');
        }elseif ($user->roles[0]->name == 'marketing'){
            return view('users/marketing-dashboard/Summernote/summernote');
        } else {
            return response('You are not authorized.');
        }
    }

    public function createBlog(){

        return view('users/app-admin/Summernote/create-blog');
    }

    public function downloadFilePdf($id)
    {
        $blogPdf = Blog::find($id);

        return Storage::download($blogPdf->pdf_url,$blogPdf->pdf_description.'.pdf');
    }

   //Store blog
    public function storeBlog(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::today();


        try {
            $blog = Blog::create(['title' => $input['title'],'slug' => Str::slug($input['title'],'-'),'description' => $input['description'],'author' => $input['author'],
                'blog_date' => $date,'pdf_description'=>$input['pdf_description']
            ]);
            if ($request->hasFile('pdf_url')) {
                $pdf_url_path = $request->file('pdf_url')->store('incubatees_uploads');
                if (isset($blog)) {
                    $blog->update(['pdf_url' => $pdf_url_path]
                    );
                } else {
                    $blog->create(['blog_image_url' => $pdf_url_path]
                    );
                }
            }

            if ($request->hasFile('blog_image_url')) {
                $image_url_path = $request->file('blog_image_url')->store('incubatees_uploads');
                if (isset($blog)) {
                    $blog->update(['blog_image_url' => $image_url_path]
                    );
                } else {
                    $blog->create(['blog_image_url' => $image_url_path]
                    );
                }
            }

            $blog->save();
            DB::commit();
            return response()->json(['message' => 'Your Blog has been successfully saved.', 'blog_id' => $blog->id]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function index()
    {
        $blogs = Blog::orderBy('blog_date', 'desc')->get();

        return view('users/app-admin/Summernote/summernote-index', compact('blogs'));

    }

    //Store summernote
    public function store(Request $request)
    {
        $detail = $request->summernoteInput;
        $blog = $request->blog_id;

        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

//        $images = $dom->getelementsbytagname('img');
//
//        //loop over img elements, decode their base64 src and save them to public folder,
//        //and then replace base64 src with stored image URL.
//        foreach($images as $k => $img){
//            $data = $img->getattribute('src');
//
//            list($type, $data) = explode(';', $data);
//            list(, $data)      = explode(',', $data);
//
//            $data = base64_decode($data);
//            $image_name= time().$k.'.jpg';
//            $path = public_path() .'/'. $image_name;
//
//            file_put_contents($path, $data);
//
//            $img->removeattribute('src');
//            $img->setattribute('src', $image_name);
//        }

        $detail = $dom->savehtml();


        $summernote = new Summernote;

        $summernote->content = $detail;
        $summernote->blog_id = $blog;



        $summernote->save();
        return view('summernote_display',compact('summernote'));
    }

    public function showBlogSummernote($id)
    {
        $blogs = Blog::all()->take(4);
        $blog = Blog::where('slug', $id)->first();
        $blog->load('summernote');
        return view('users/app-admin/Summernote/show-blog-summernote', compact('blog','blogs'));
    }

    //Blog and summernote  get index
    public function indexBlog()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users/app-admin/Summernote/summernote-blog-index');
        }elseif ($user->roles[0]->name == 'advisor') {
            return view('users/advisor-dashboard/Summernote/summernote-blog-index');
        }elseif ($user->roles[0]->name == 'marketing') {
            return view('users/marketing-dashboard/Summernote/summernote-blog-index');
        }else{
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }

    }

    //Get blog and summernote
    public function getBlogs()
    {
        $blogs = Blog::with('summernote')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-blog/' . $blog->id;
                $del = '/blog-delete/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Blog" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Blog" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif  ($user->roles[0]->name == 'advisor') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-blog/' . $blog->id;
                $del = '/blog-delete/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Blog" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Blog" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif  ($user->roles[0]->name == 'marketing') {
            return Datatables::of($blogs)->addColumn('action', function ($blog) {
                $re = '/edit-blog/' . $blog->id;
                $del = '/blog-delete/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Blog" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Blog" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }


    public function show(Summernote $summernote)
    {
        dd($summernote);

    }


    public function editBlog($id)
    {
        $blog = Blog::where('id',$id)->first();
        $blog->load('BlogMedia','Summernote');

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users/app-admin/Summernote/edit-blog', compact('blog'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users/app-admin/Summernote/edit-blog', compact('blog'));
        }elseif ($user->roles[0]->name == 'marketing') {
            return view('users/marketing-dashboard/Summernote/edit-blog', compact('blog'));
        }elseif ($user->roles[0]->name == 'advisor') {
            return view('users/advisor-dashboard/Summernote/edit-blog', compact('blog'));
        }
    }

    public function updateBlogOne(Request $request, Blog $blog)
    {
        $blog->load('summernote')->get();

        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::today();


        try {
            $blog->update(['title' => $input['title'],
                'description' => $input['description'],
                'slug' => Str::slug($input['title'],'-'),
                'author' => $input['author'],
                'pdf_description' => $input['pdf_description'],
                'blog_date' => $date]);
            $blog->save();

            if ($request->hasFile('blog_image_url')) {
                $image_url_path = $request->file('blog_image_url')->store('incubatee_uploads');
                if (isset($blog)) {
                    $blog->update(['blog_image_url' => $image_url_path]
                    );
                } else {
                    $blog->create(['blog_image_url' => $image_url_path]
                    );
                }
            }
            if ($request->hasFile('pdf_url')) {
                $pdf_url_path = $request->file('pdf_url')->store('incubatees_uploads');
                if (isset($blog)) {
                    $blog->update(['pdf_url' => $pdf_url_path]
                    );
                } else {
                    $blog->create(['blog_image_url' => $pdf_url_path]
                    );
                }
            }
            DB::commit();
            return response()->json(['message' => 'Successfully updated Blog']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    public function updateBlog(Request $request, Blog $blog)
    {
        $blog->load('summernote')->get();

        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::today();


        try {
            $blog->save();
            if(isset($blog->summernote)) {
                $summernote_content = $blog->summernote;
                if ($request->has('content')) {
                    $summernote_content->update(['content' => $input['content']]);
                }
            }

            if ($request->hasFile('blog_image_url')) {
                $image_url_path = $request->file('blog_image_url')->store('incubatee_uploads');
                if (isset($blog)) {
                    $blog->update(['blog_image_url' => $image_url_path]
                    );
                } else {
                    $blog->create(['blog_image_url' => $image_url_path]
                    );
                }
            }
            DB::commit();
            return response()->json(['message' => 'Successfully updated Blog']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function destroyBlog($id)
    {
        try {
            DB::beginTransaction();
            $blog = Blog::find($id);

            $blog->load('summernote');
            if (isset($blog->summernote)){
                $blog->summernote->forceDelete();
            }else {
                $blog->forceDelete();
            }

            DB::commit();
            return view('users/app-admin/Summernote/summernote-blog-index');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }

    }


    //CREATE CAREERS PAGE
    public function createCareersPage(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.careers.create-careers');
        } elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.careers.create-careers');
        } elseif ($user->roles[0]->name == 'app-admin'){
            return view('users.app-admin.careers.create-careers');
        }
        else {
            return response('You are not authorized.');
        }
    }

    //STORE CAREERS
    public function storeCareers(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();

        try {
            $career = Career::create(['title' => $input['title'],'description' => $input['description']]);

            $career->save();
            DB::commit();
            return response()->json(['message' => 'Career has been successfully saved.', 'career_id' => $career->id]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //STORE CAREER SUMMERNOTE
    public function storeCareerSummernote(Request $request)
    {
        $detail = $request->summernoteInput;
        $career = $request->career_id;

        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $detail = $dom->savehtml();


        $careerSummernote = new CareerSummernote();

        $careerSummernote->content = $detail;
        $careerSummernote->career_id = $career;



        $careerSummernote->save();
        return response()->json(['message' => 'Career has been successfully saved.', $careerSummernote]);
    }
    //CAREERS INDEX
    public function careersIndex(){
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.careers.careers-index');
        } elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.careers.careers-index');
        }  elseif ($user->roles[0]->name == 'app-admin'){
            return view('users.app-admin.careers.careers-index');
        }
        else {
            return response('You are not authorized.');
        }
    }

    //GET CAREERS
    public function getCareers(){
        $careers = Career::with('careersSummernote')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'administrator'
            or $user->roles[0]->name == 'marketing'
            or $user->roles[0]->name == 'app-admin') {
            return Datatables::of($careers)->addColumn('action', function ($career) {
                $re = '/edit-careers/' . $career->id;
                $del = '/careers-delete/' . $career->id;
                return '<a href=' . $re . ' title="Edit Careers" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Career" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }

    }

    //DELETE CAREERS
    public function destroyCareers($id)
    {
        try {
            DB::beginTransaction();
            $careers = Career::find($id);

            $careers->load('careersSummernote');
            if (isset($careers->careersSummernote)){
                $careers->careersSummernote->forceDelete();
            }else {
                $careers->forceDelete();
            }

            DB::commit();
            return view('users.administrators.careers.careers-index');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }

    }

    //EDIT CAREERS
    public function editCareers(Career $career){
        $career->load('careersSummernote');

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.careers.edit-careers', compact('career'));
        } elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.careers.edit-careers',compact('career'));
        } elseif ($user->roles[0]->name == 'app-admin'){
            return view('users.app-admin.careers.edit-careers',compact('career'));
        }
        else {
            return response('You are not authorized.');
        }
    }

    //UPDATE CAREERS
    public function updateCareers(Request $request, Career $career)
    {
        $career->load('careersSummernote')->get();

        DB::beginTransaction();
        $input = $request->all();

        try {
            $career->update(['title' => $input['title'],
                'description' => $input['description']]);
            $career->save();

            DB::commit();
            return response()->json(['message' => 'Successfully updated Careers']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //UPDATE CAREER SUMMERNOTE
    public function updateCareerSummernote(Request $request, Career $career)
    {
        $career->load('careersSummernote')->get();
        DB::beginTransaction();
        $input = $request->all();

        try {
            if(isset($career->careersSummernote)) {
                $summernote_content = $career->careersSummernote;
                if ($request->has('content')) {
                    $summernote_content->update(['content' => $input['content']]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully updated Careers']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }


    //create news
    public function createNews()
    {

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.propella-news.create-news');
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.propella-news.create-news');
        }elseif ($user->roles[0]->name == 'advisor'){
            return view('users.advisor-dashboard.propella-news.create-news');
        }elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.propella-news.create-news');
        } else {
            return response('You are not authorized.');
        }
    }

    //Store news
    public function storeNews(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();


        try {
            $news = PropellaNew::create(['title' => $input['title'],'slug' => Str::slug($input['description'], '-'),'description' => $input['description'],
                'added_date' => $input['added_date']
            ]);

            if ($request->hasFile('news_image_url')) {
                $image_url_path = $request->file('news_image_url')->store('incubatees_uploads');
                if (isset($news)) {
                    $news->update(['news_image_url' => $image_url_path]
                    );
                } else {
                    $news->create(['news_image_url' => $image_url_path]
                    );
                }
            }

            $news->save();
            DB::commit();
            return response()->json(['message' => 'Your news has been successfully saved.', 'propella_news_id' => $news->id]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //STORE news content
    public function storeNewsContent(Request $request)
    {
        $detail = $request->summernoteInput;
        $news = $request->propella_news_id;

        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $detail = $dom->savehtml();


        $newsContent = new PropellaNewsletterContent();

        $newsContent->content = $detail;
        $newsContent->propella_news_id = $news;

        $newsContent->save();
        return response()->json(['message' => 'News content has been successfully saved.']);
    }

    //News
    public function newsIndex(){
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.propella-news.propella-news-index');
        }elseif ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.propella-news.propella-news-index');
        }elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.propella-news.propella-news-index');
        }elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.propella-news.propella-news-index');
        }
    }

    public function getNews()
    {
        $news = PropellaNew::with('propellaNewsContent')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'marketing' or
            $user->roles[0]->name == 'app-admin' or
            $user->roles[0]->name == 'administrator' or
            $user->roles[0]->name == 'advisor') {
            return Datatables::of($news)->addColumn('action', function ($learning) {
                $re = '/edit-news/' . $learning->id;
                $del = '/destroy-news/' . $learning->id;
                return '<a href=' . $re . ' title="Edit Blog" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Blog" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    //Edit news
    public function editNews(PropellaNew $propellaNew){
        $propellaNew->load('propellaNewsContent');
        $logged_in_user = Auth::user()->load('roles');

        if($logged_in_user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.propella-news.edit-news',compact('propellaNew'));
        }elseif ($logged_in_user->roles[0]->name == 'app-admin') {
            return view('users.propella-news.edit-news',compact('propellaNew'));
        }elseif ($logged_in_user->roles[0]->name == 'administrator') {
            return view('users.administrators.propella-news.edit-news',compact('propellaNew'));
        }elseif ($logged_in_user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.propella-news.edit-news',compact('propellaNew'));
        }
    }

    //Update news
    public function updateNews(Request $request, PropellaNew $propellaNew)
    {
        $propellaNew->load('propellaNewsContent')->get();

        DB::beginTransaction();
        $input = $request->all();


        try {
            $propellaNew->update(['title' => $input['title'],
                'slug' => Str::slug($input['description'],'-'),
                'description' => $input['description'],
                'added_date' => $input['added_date']]);
            $propellaNew->save();

            if ($request->hasFile('news_image_url')) {
                $image_url_path = $request->file('news_image_url')->store('incubatee_uploads');
                if (isset($propellaNew)) {
                    $propellaNew->update(['news_image_url' => $image_url_path]
                    );
                } else {
                    $propellaNew->create(['news_image_url' => $image_url_path]
                    );
                }
            }
            DB::commit();
            return response()->json(['message' => 'Successfully updated news']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Update news content
    public function updateNewsContent(Request $request, PropellaNew $propellaNew)
    {
        $propellaNew->load('propellaNewsContent')->get();

        DB::beginTransaction();
        $input = $request->all();

        try {
            $propellaNew->save();
            if(isset($propellaNew->propellaNewsContent)) {
                $news_content = $propellaNew->propellaNewsContent;
                if ($request->has('content')) {
                    $news_content->update(['content' => $input['content']]);
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully updated news content']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Delete news with its relationships
    public function destroyNews($id)
    {
        try {
            DB::beginTransaction();
            $news = PropellaNew::find($id);
            $news->load('propellaNewsContent');
            if (isset($news->propellaNewsContent)){
                $news->propellaNewsContent->forceDelete();
            }else {
                $news->forceDelete();
            }

            DB::commit();
            return response()->json(['message' => 'News deleted.']);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    //Create latest news
    //create blog summernote
    public function createLatestInfo()
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.latest-info.create-latest-info');
        } elseif ($user->roles[0]->name == 'marketing'){
            return view('users.marketing-dashboard.latest-info.create-latest-info');
        } else {
            return response('You are not authorized.');
        }
    }

    //Store latest info
    public function storeLatestInfo(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();


        try {
            $latestInfo = LatestInfo::create(['description' => $input['description'],
                'date' => $input['date'],'title'=>$input['title']
            ]);
            if ($request->hasFile('info_image_url')) {
                $image_url_path = $request->file('info_image_url')->store('incubatees_uploads');
                if (isset($latestInfo)) {
                    $latestInfo->update(['info_image_url' => $image_url_path]
                    );
                } else {
                    $latestInfo->create(['info_image_url' => $image_url_path]
                    );
                }
            }
            if ($request->hasFile('pdf_url')) {
                $pdf_url_path = $request->file('pdf_url')->store('incubatees_uploads');
                if (isset($latestInfo)) {
                    $latestInfo->update(['pdf_url' => $pdf_url_path]
                    );
                } else {
                    $latestInfo->create(['blog_image_url' => $pdf_url_path]
                    );
                }
            }
            if ($request->hasFile('word_doc_url')) {
                $pdf_url_path = $request->file('word_doc_url')->store('incubatees_uploads');
                if (isset($latestInfo)) {
                    $latestInfo->update(['word_doc_url' => $pdf_url_path]
                    );
                } else {
                    $latestInfo->create(['word_doc_url' => $pdf_url_path]
                    );
                }
            }
            $latestInfo->save();
            DB::commit();
            return response()->json(['message' => 'Latest info has been successfully saved.', 'latest_info_id' => $latestInfo->id]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Store summernote
    public function storeLatestInfoSummernote(Request $request)
    {
        $detail = $request->summernoteInput;
        $latestInfo = $request->latest_info_id;

        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $detail = $dom->savehtml();

        $summernote = new LatestInfoSummernote();

        $summernote->content = $detail;
        $summernote->latest_info_id = $latestInfo;

        $summernote->save();

        return response()->json(['message' => 'Successfully created latest posts']);
    }

    //DETAILED LATEST INFO
    public function showDetailedLatestInfo(LatestInfo $latestInfo){
        $latest = LatestInfo::all();
        $latest->load('latestInfoSummernote');
        return view('users.app-admin.latest-info.detailed-latest-info',compact('latestInfo','latest'));
    }

    //Download File
    public function downloadLatestInfoPDF($id)
    {
        $file = LatestInfo::find($id);

        return Storage::download($file->pdf_url,$file->title.'.pdf');
    }

    //Download File
    public function downloadLatestInfoWordDoc($id)
    {
        $file = LatestInfo::find($id);

        return Storage::download($file->pdf_url,$file->title.'.doc');
    }

    //Latest post index
    public function indexLatestPost()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.latest-info.latest-post-index');
        }elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.latest-info.latest-post-index');
        }else{
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }

    }

    //Get latest posts
    public function getLatestInfo()
    {
        $latestInfo = LatestInfo::with('latestInfoSummernote')->get();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'marketing') {
            return Datatables::of($latestInfo)->addColumn('action', function ($blog) {
                $re = '/edit-latest-info/' . $blog->id;
                $del = '/delete-latest-post/' . $blog->id;
                return '<a href=' . $re . ' title="Edit Blog" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Blog" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    //Edit latest posts
    public function editLatestPosts(LatestInfo $latestInfo)
    {
        $latestInfo->load('latestInfoSummernote');

        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.latest-info.edit-latest-info', compact('latestInfo'));
        } elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.latest-info.edit-latest-info', compact('latestInfo'));
        }
    }

    //Update latest posts
    public function updateLatestPosts(Request $request, LatestInfo $latestInfo)
    {
        $latestInfo->load('latestInfoSummernote')->get();

        DB::beginTransaction();
        $input = $request->all();


        try {
            $latestInfo->update(['title' => $input['title'],
                'description' => $input['description'],
                'date' => $input['date']]);
            $latestInfo->save();

            if ($request->hasFile('info_image_url')) {
                $image_url_path = $request->file('info_image_url')->store('incubatee_uploads');
                if (isset($latestInfo)) {
                    $latestInfo->update(['info_image_url' => $image_url_path]
                    );
                } else {
                    $latestInfo->create(['info_image_url' => $image_url_path]
                    );
                }
            }
            if ($request->hasFile('pdf_url')) {
                $pdf_url_path = $request->file('pdf_url')->store('incubatees_uploads');
                if (isset($latestInfo)) {
                    $latestInfo->update(['pdf_url' => $pdf_url_path]
                    );
                } else {
                    $latestInfo->create(['blog_image_url' => $pdf_url_path]
                    );
                }
            }

            if ($request->hasFile('word_doc_url')) {
                $pdf_url_path = $request->file('word_doc_url')->store('incubatees_uploads');
                if (isset($latestInfo)) {
                    $latestInfo->update(['word_doc_url' => $pdf_url_path]
                    );
                } else {
                    $latestInfo->create(['word_doc_url' => $pdf_url_path]
                    );
                }
            }
            DB::commit();
            return response()->json(['message' => 'Successfully updated latest posts']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Update latest post summernote
    public function updateLatestPostSummernote(Request $request, LatestInfo $latestInfo)
    {
        $latestInfo->load('latestInfoSummernote')->get();

        DB::beginTransaction();
        $input = $request->all();

        try {
            $latestInfo->save();
            if(isset($latestInfo->latestInfoSummernote)) {
                $summernote_content = $latestInfo->latestInfoSummernote;
                if ($request->has('content')) {
                    $summernote_content->update(['content' => $input['content']]);
                }
            }


            DB::commit();
            return response()->json(['message' => 'Successfully updated latest posts']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Delete latest post
    public function destroyLatestPost($id)
    {
        try {
            DB::beginTransaction();
            $latestPost = LatestInfo::find($id);

            $latestPost->load('latestInfoSummernote');
            if (isset($latestPost->latestInfoSummernote)){
                $latestPost->latestInfoSummernote->forceDelete();
            }else {
                $latestPost->forceDelete();
            }

            DB::commit();
            return response()->json(['message' => 'Successfully deleted latest posts']);
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }

    }

}
