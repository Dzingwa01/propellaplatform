<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogMedia;
use App\BlogSection;
use App\Career;
use App\Donation;
use App\EventIncubatee;
use App\EventMediaImage;
use App\Incubatee;
use App\IncubateeOwnership;
use App\IncubateeUpload;
use App\Event;
use App\LatestInfo;
use App\Mail\PartnerEmail;
use App\Newsletter;
use App\PropellaLearningCurve;
use App\PropellaNew;
use App\PropellaVenue;
use App\User;
use App\Video;
use Carbon\Carbon;
use App\Venture;
use App\VentureOwnership;
use App\VentureUpload;
use App\Visitor;
//use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\EventVenue;
use App\EventsMedia;
use Calendar;
use App\GeneralAssignedEnquiry;
use App\SafetyForm;
use Postmark\PostmarkClient;


class WelcomeController extends Controller

{

    //Hedge sa page
    public function salutarisPage(){
        $enquiry_categories = GeneralAssignedEnquiry::all();
        return view('home.salutaris-page',compact('enquiry_categories'));
    }

    public function updateIncubateeEvent(Request $request)
    {
        $input = $request->all();
        $date = Carbon::now();
        $incubatee_id = $input['incubatee_id'];
        $cur_incubatee = Incubatee::find($incubatee_id);
        $cur_incubatee->load('events');

                DB::beginTransaction();
                try {
                    if(isset($cur_incubatee)){
                    if ($request->has('event_id')) {
                        $event_id = $input['event_id'];
                        $check_event = Event::find($event_id);
                        $incubatee_events = EventIncubatee::where('incubatee_id', $incubatee_id)->get();

                        $bool_found = false;
                        foreach ($incubatee_events as $incubatee_event) {
                            if ($incubatee_event->event_id == $event_id) {
                                $bool_found = true;
                                $incubatee_event->attended = true;
                                $incubatee_event->save();
                                DB::commit();
                                return response()->json(['visitor' => $cur_incubatee, 'message' => 'Enjoy your event / workshop!'], 200);
                            }
                        }

                        if (!$bool_found) {
                            $create_incubatee_event = EventIncubatee::create(['event_id' => $event_id, 'incubatee_id' => $incubatee_id,
                                'attended' => true, 'registered' => true, 'de_registered' => false, 'date_time_registered' => $date]);
                            DB::commit();
                            response()->json(["hit" => $create_incubatee_event]);
                            return response()->json(['incubatee' => $cur_incubatee, 'message' => 'Enjoy your event / workshop!'], 200);
                        }

                    } else {
                        return response()->json(['message', 'Are you sure you chose and event?']);
                    }
                    }else{
                        return response()->json(['message', 'Not part of incubatee?']);
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 500);
                }

    }

    public function incubateeAttendance(){
        return view('home.incubatee-attendance');
    }

    public function newPage(){
        return view('zane_project.newPage');
    }

    public function landingPage(){
        return view('home.landingPage');
    }

    public function bookingSystem(){
        $venues = PropellaVenue::all();

        return view('home.bookingSystem',compact('venues'));
    }

    public function getBookingSystem(){
        $venues =PropellaVenue::all();
        $date = Carbon::today()->toDateString();
        $time = Carbon::now()->toTimeString();
        $rooms_occupied =[];
        $indexed_time = substr($time, 0, 5);
        $first_two_indexes_time = "$indexed_time[0]" . "$indexed_time[1]";
        $first_two_indexes_int_plus_2 = (int)$first_two_indexes_time + 2;
        $fixed_current_time = "";

        foreach ($venues as $venue) {
            $venue->load('venueBooking');

            //Convert the time to +2GMT
            if ($first_two_indexes_int_plus_2 < 10) {
                $fixed_current_time = "0" . (string)$first_two_indexes_int_plus_2 . ":" . substr($indexed_time, 3, 5);
            } else {
                $fixed_current_time = (string)$first_two_indexes_int_plus_2 . ":" . substr($indexed_time, 3, 5);
            }

            foreach ($venue->venueBooking as $book) {
                if ($book->venue_date == $date) {
                    if ($fixed_current_time >= $book->venue_start_time) {
                        if($fixed_current_time <= $book->venue_end_time){
                            $object = (object)['venue_name' => $venue->venue_name, 'venue_start_time' => $book->venue_start_time, 'venue_end_time' => $book->venue_end_time];
                            array_push($rooms_occupied, $object);
                        }
                    }
                }
            }
        }

        return $rooms_occupied;
    }


    public function getIncubatees()
    {

        $venture = Venture::with('ventureOwnership', 'ventureUploads')
            ->where('hub', 'ICT')
            ->orderBy('stage', 'DESC')
            ->orderBy('company_name', 'ASC')
            ->get();

        $current_venture_ownership = Venture::with('ventureOwnership')->get();
        $venture_upload = Venture::with('ventureUploads')->get();

        return view('home.Incubatees', compact('venture', 'current_venture_ownership', 'venture_upload'));
    }

    public function getIndustrial()
    {

        $venture = Venture::with('ventureOwnership', 'ventureUploads')
            ->where('hub', 'Industrial')
            ->orderBy('stage', 'DESC')
            ->orderBy('company_name', 'ASC')
            ->get();

        return view('home.Industrial', compact('venture'));
    }

    public function getAlumni()
    {
        $ventures = Venture::with('ventureOwnership', 'ventureUploads')
            ->where('status', 'Alumni')
            ->where('Hub','ICT')
            ->orderBy('company_name','ASC')
            ->get();

        return view('home.alumni', compact('ventures'));
    }

    public function getExitedVentures()
    {
        $ventures = Venture::with('ventureOwnership', 'ventureUploads')->where('status', 'Exit')->orderBy('company_name','asc')->get();

        return view('home.exited-ventures', compact('ventures'));
    }

    //Get exited industrial ventures
    public function getExitedIndustrialVentures()
    {
        $ventures = Venture::with('ventureOwnership', 'ventureUploads')->where('status', 'Exit')->orderBy('company_name','asc')->get();

        return view('home.exited-industrial-ventures', compact('ventures'));
    }

    public function getExited(){
        $venture = Venture::with('ventureOwnership', 'ventureUploads')
            ->orderBy('cohort','DESC')
            ->orderBy('company_name','ASC')
            ->get();

        return view('home.cohorts', compact('venture'));
    }



    public function getFounder($slug)
    {
        $venture = Venture::where('slug',$slug)->first();
        $venture->load('incubatee', 'ventureOwnership', 'ventureUploads')->get()->first();
        $current_venture_ownership = VentureOwnership::with('venture')->where('venture_id', $venture->id)->get()->first();
        $venture_upload = VentureUpload::with('venture')->where('venture_id', $venture->id)->get()->first();
        /*dd($venture);*/

        return view('home.founders', compact('venture', 'venture_upload', 'current_venture_ownership'));
    }

    public function getSmartCity1()
    {
        $smart_mobility = Venture::with('ventureOwnership', 'ventureUploads')->where('smart_city_tags', 'like','%Smart Mobility%')->get();

        $smart_infrastructure = Venture::with('ventureOwnership', 'ventureUploads')->where('smart_city_tags', 'like','%Smart Infrastructure%')->get();

        $smart_healthcare = Venture::with('ventureOwnership', 'ventureUploads')->where('smart_city_tags', 'like','%Smart Healthcare%')->get();

        $smart_governance = Venture::with('ventureOwnership', 'ventureUploads')->where('smart_city_tags', 'like','%Smart Governance%')->get();
        /*dd($smart_governance);*/
        $smart_environment = Venture::with('ventureOwnership', 'ventureUploads')->where('smart_city_tags', 'like','%Smart Environment%')->get();
        $smart_education = Venture::with('ventureOwnership', 'ventureUploads')->where('smart_city_tags', 'like','%Smart Education%')->get();
        $smart_community = Venture::with('ventureOwnership', 'ventureUploads')->where('smart_city_tags', 'like','%Smart Community%')->get();
        $smart_building = Venture::with('ventureOwnership', 'ventureUploads')->where('smart_city_tags', 'like','%Smart Building%')->get();
        $smart_business = Venture::with('ventureOwnership', 'ventureUploads')->where('smart_city_tags', 'like','%Smart Business%')->get();


        return view('home.smartcity', compact('smart_mobility', 'smart_infrastructure', 'smart_healthcare',
            'smart_governance',
            'smart_environment',
            'smart_education',
            'smart_community',
            'smart_building',
            'smart_business'));
    }

    public function getSDG()
    {
        $no_poverty = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%No poverty%')->get();

        $zero_hunger = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Zero hunger%')->get();

        $good_health = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like','%Good Health and well-being%')->get();

        $quality_education = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like','Quality education%')->get();

        $gender_equality = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Gender quality%')->get();
        $clean_water = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Clean water & sanitation%')->get();
        $clean_energy = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Affordable & clean energy%')->get();
        $decent_work = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Decent work & economic growth%')->get();
        $industry = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Industry , innovation & infastructure%')->get();
        $inequalities = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Reduced inequalities%')->get();
        $communities = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Sustainable cities and communities%')->get();
        $production = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Responsible consumption and production%')->get();
        $climate = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Climate action%')->get();
        $below_water = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like','%Life below water%')->get();
        $on_land = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Life on land%')->get();
        $peace = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Promote just, peaceful and inclusive societies%')->get();
        $partnerships = Venture::with('ventureOwnership', 'ventureUploads')->where('sustainable_development_goals', 'like', '%Partnership for the goals%')->get();


        return view('home.sdg', compact('no_poverty', 'zero_hunger', 'good_health',
            'quality_education',
            'gender_equality',
            'clean_water',
            'clean_energy',
            'decent_work',
            'industry',
            'inequalities',
            'communities',
            'production',
            'climate',
            'below_water',
            'on_land',
            'peace',
            'partnerships'));
    }

    //Show all the events when 'view event' button is clicked on calendar view
    /*public function showEvents()
    {
        $events = Event::with('EventVenue', 'EventsMedia')
            ->whereDate('start', '>', Carbon::now()->subDay(30))
            ->orderBy('start', 'DESC')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $object = (object)['event' => $event, 'start_time' => $start_time];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('/full_calendar/show-events', compact('events_array'));
    }*/

    //Show all the events when 'view event' button is clicked on calendar view
    public function showEvents()
    {
        //Hello

        $events = Event::with('EventVenue', 'EventsMedia')->where('eventStatus','Active')->orderBy('start', 'asc')->get();


        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('/full_calendar/show-events', compact('events_array'));
    }

    public function complianceEvents()
    {
        //Hello
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','Preparing for Compliance with the POPI Act: SME and start-up businesses')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('/full_calendar/compliance-event', compact('events_array'));
    }

    //FUN 2 FAIL
    public function funToFail()
    {
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','Fun2Fail')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('/full_calendar/fun-to-fail-event', compact('events_array'));
    }

    //Showing all the events in the platform
    public function showAllEvents(){
        $events = Event::with('EventVenue', 'EventsMedia')
            /*->whereDate('start', '>', Carbon::now()->subDay(30))*/
            ->orderBy('start', 'desc')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('/full_calendar/show-all-events', compact('events_array'));
    }

    /*Show gallery when 'view photo gallery' is clicked*/
    public function showGallery($event)
    {
        $event = Event::where('slug',$event)->first();
        $event->load('EventVenue', 'EventsMedia');
        $event_media = $event->EventsMedia;
        $event_media_images = [];

        if(isset($event_media)){
            $event_media->load('eventsMediaImage');

            if(isset($event_media->eventsMediaImage)){
                foreach($event_media->eventsMediaImage as $media_image){
                    array_push($event_media_images, $media_image);
                }
            }
        }

        return view('/full_calendar/show-gallery', compact('event','event_media_images'));
    }

    /*Show gallery when 'gallery' is clicked*/
    public function eventGallery()
    {
        $events = Event::with('EventsMedia')->orderBy('start', 'DESC')->get();

        return view('/home/gallery', compact('events'));
    }



    /*PUBLIC EVENTS METHOD*/
    public function publicEvent()
    {
        $events = Event::all();
        $venueName = EventVenue::orderBy('venue_name', 'asc')->get();

        $calendar = Calendar::addEvents($events)->setCallbacks(['eventClick' => 'function(calEvent, jsEvent, view){
            alert("Please click View Events on top of page for more details!!");
        }']);
        return view('/full_calendar/public-event', compact('calendar', 'venueName'));
    }

    public function getEventMedia(Event $event)
    {
        return view('/home/event-show-media', compact('event'));
    }

    public function deleteApplicants(){
        $applicants = User::whereHas('roles',function($query){
            $query->where('name','applicant');
            return $query;
        })->get();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        foreach ($applicants as $applicant){
            foreach ($applicant->userQuestionAnswers as $question){
                $question->forceDelete();
            }
            $applicant->forceDelete();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }

    public function blogIndex()
    {

        $blogs = Blog::all();

        $blog_array = [];

        foreach ($blogs as $blog){
            $description = htmlspecialchars($blog->description, ENT_QUOTES, 'UTF-8');
            $title = $blog->title;

            $object = (object)['title' => $title, 'description' => $description];

            array_push($blog_array, $object);
        }


        return view('Blogs/blog-index', compact('blogs','blog_array'));
    }

    public function showBlog(Blog $blog)
    {
        $blogs = Blog::all();
        $blog->load('blogSection');
        $blog_section_paragraph_array = [];
        $temp_section_array = [];
        $temp_paragraph_array = [];
        $date = Carbon::today();

        foreach($blog->blogSection as $blog_section){
            $blog_section->load('blogParagraph');
            array_push($temp_section_array, $blog_section);

            foreach ($blog_section->blogParagraph as $section_paragraph){
                array_push($temp_paragraph_array, $section_paragraph);
            }
        }

        $object = (object)[
            'blog_title' => $blog->title,
            'blog_description' => $blog->description,
            'blog_date' => $date,
            'blog_image_url' => $blog->blog_image_url,
            'blog_sections' => $temp_section_array,
            'section_paragraphs' => $temp_paragraph_array
        ];

        return view('/Blogs/show-blog', compact('object','blogs'));
    }


    public function bootcampAttendance(){
        return view('home.bootcamp-attendance');
    }

    public function getPsiIncubatees()
    {

        $venture = Venture::with('ventureOwnership', 'ventureUploads')
            ->where('hub', 'PSI')
            ->orderBy('stage', 'DESC')
            ->orderBy('company_name', 'ASC')
            ->get();

        $current_venture_ownership = Venture::with('ventureOwnership')->get();
        $venture_upload = Venture::with('ventureUploads')->get();

        return view('home.psi-ventures', compact('venture', 'current_venture_ownership', 'venture_upload'));
    }

    public function alumniCategories(){
        return view('home.alumni-categories');
    }

    public function exitedCategories(){
        return view('home.exited-categories');
    }


    public function getIndustrialAlumni()
    {
        $ventures = Venture::with('ventureOwnership', 'ventureUploads')
            ->where('status', 'Alumni')
            ->where('Hub','Industrial')
            ->orderBy('company_name', 'ASC')
            ->get();

        return view('home.industrial-alumni', compact('ventures'));
    }

    public function getPTIAlumni()
    {
        $ventures = Venture::with('ventureOwnership', 'ventureUploads')->where('status', 'Alumni')->where
        ('Hub','PTI')->get();

        return view('home.township-hub-alumni', compact('ventures'));
    }

    public function getCreativeAlumni()
    {
        $ventures = Venture::with('ventureOwnership', 'ventureUploads')->where('status', 'Alumni')->where
        ('Hub','Creative')->get();

        return view('home.creative-alumni', compact('ventures'));
    }

    //COVID 19
    public function covidQuestion(){

        return view('home.covid-19-safety-form');
    }
    //Store user roles
    public function storeCovidForm(Request $request)
    {
        $input = $request->all();
        DB::beginTransaction();

        try {
            $save_form = SafetyForm::create(['fist_and_last_name' => $input['fist_and_last_name'], 'id_number' => $input['id_number'],
                'contact_number'=>$input['contact_number'],'street_address'=>$input['street_address'],
                'employer'=>$input['employer'], 'age'=>$input['age'],'temperature'=>$input['temperature'],
                'medical_history'=>$input['medical_history'],'travel'=>$input['travel'],
                'recent_care'=>$input['recent_care'],'signs_symptoms'=>$input['signs_symptoms'],
                'signature'=>$input['signature'],'date'=>$input['date']
                ]);
            $save_form->save();
            DB::commit();
            return response()->json(['message' => 'Thank you.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'User could not be saved  ' . $e->getMessage()], 400);
        }
    }

    public function propellaDocs(){
        return view('home.propella-docs');
    }


    //Franchising 101 event
    public function franchisingEvents()
    {
        //Hello
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','Franchising 101 Workshop')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('full_calendar.franchising-event', compact('events_array'));
    }

    //VIDEO GALLERY
    public function viewVideoGallery(){
        $video = Video::orderBy('created_at', 'asc')->get();
        return view('home.video-gallery',compact('video'));
    }

    //NEWS
    public function newsPage(){
        $news = PropellaNew::orderBy('added_date', 'desc')->get();
        $news->load('propellaNewsContent');
        return view('home.news',compact('news'));
    }


    //DETAILED NEWS
    public function showDetailedNews($slug){

        $propellaNew = PropellaNew::where('slug',$slug)->first();
        $propellaNew->load('propellaNewsContent');
        $news = PropellaNew::orderBy('added_date', 'desc')->get();
        return view('home.detailed-news',compact('propellaNew','news'));

    }

    //Franchising 101 event
    public function storyTellingEvent()
    {
        //Hello
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','Story telling is the best Marketing')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('home.story-telling', compact('events_array'));
    }

    public function careersView(){
        $careers = Career::with('careersSummernote')->get();
        return view('home.careers',compact('careers'));
    }

    //TIA APPLICATION
    public function TIAView(){
        return view('home.TIA');
    }

    //DEMO DAY EXHIBITION
    public function demoDayExhibition()
    {
        //Hello
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','DEMO DAY - Exhibition')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('home.demo-day', compact('events_array'));
    }

    //4IR readiness questionair
    public function readinessQuestionairs(){
        return view('home.4ir-readiness-questionair');
    }

    //DESIGN THINKING
    public function designThinking()
    {
        //Hello
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','Design Thinking Workshop')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('home.design-thinking', compact('events_array'));
    }

    //Combined media page
    public function mediaPage(){
        $blogs = Blog::orderBy('blog_date', 'desc')->get();
        $news = PropellaNew::orderBy('added_date', 'desc')->get();
        $news->load('propellaNewsContent');

        $Galleryevents = Event::with('EventsMedia')->orderBy('start', 'DESC')->get();

        $Allevents = Event::with('EventVenue', 'EventsMedia')->whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->get();

        $start_time = "";
        $events = [];
        $events_array = [];

        foreach ($Allevents->take(3) as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        foreach ($Galleryevents->take(3) as $event){


                    $object = (object)['title' => $event->title, 'created_at' => $event->created_at, 'event_image_url' => $event->event_image_url,
                        'start' => $event->start, 'slug' => $event->slug];

                    array_push($events, $object);




        }



        $video = Video::orderBy('date', 'desc')->get();
        $learning_curves = PropellaLearningCurve::orderBy('added_date', 'desc')->get();

        return view('home.media',compact('blogs','news','events','video','learning_curves', 'events_array'));
    }

    public function welcome(){
       $events = Event::with('EventVenue', 'EventsMedia')->where('eventShow','Yes')->orderBy('start', 'asc')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);

        }

        return view('welcome',compact('events_array'));
    }

    public function talkChangeMeEvent()
    {
        //Hello
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','Talk : Change me')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('/full_calendar/talk-change-me', compact('events_array'));
    }

    //Application quiz
    public function applicationQuiz(){
        return view('home.application-quiz');
    }

    //ICT 21 Virtual Showcase
    public function ICT21VirtualShowcase()
    {
        //Hello
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','ICT 21 Virtual Showcase')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('/full_calendar/ict-21-virtual-showcase', compact('events_array'));
    }

    //Virtual lean cofee
    public function virtualLeanCoffee()
    {
        //Hello
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','Virtual Lean Coffee')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('/full_calendar/virtual-lean-coffee', compact('events_array'));
    }

    /*ict apply link*/
    public function ictAppyLink(){
        return view('Application-Process.ICT-application-link');
    }

    //Donation
    public function donation(){
        return view('home.donation');
    }

    //Ivest in our Ventures
    public function investInVentures(){
        return view('home.invest-in-our-venture');
    }


    //Store presentations
    public function storeDonationForm(Request $request){
        DB::beginTransaction();
        $input = $request->all();
        try{
           $donation = Donation::create(['name'=>$input['name'],'email'=>$input['email'],'contact_number'=>$input['contact_number'],
               'message'=>$input['message']]);
            $donation->save();

            DB::commit();
            return response()->json(['message'=>'Thank you']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }
    }

    //Testimonials
    public function testimonialsPage(){
        return view('home.testimonials');
    }

    //pitching101
    public function pitching101()
    {
        //Hello
        $events = Event::with('EventVenue', 'EventsMedia')->where('title','Pitching 101')->get();
        $start_time = "";
        $events_array = [];

        foreach ($events as $event){
            $event->load('visitors', 'incubatees', 'internalUsers', 'bootcampers');
            for($i = 0; $i < count_chars($event->start_time); $i++){
                $start_time = $start_time . $event->start_time[$i];
                if($i == 4){
                    break;
                }
            }
            $registered_participants = 0;

            if(isset($event->visitors)){
                if(count($event->visitors) > 0){
                    $registered_participants += count($event->visitors);
                }
            }

            if(isset($event->incubatees)){
                if(count($event->incubatees) > 0){
                    $registered_participants += count($event->incubatees);
                }
            }

            if(isset($event->internalUsers)){
                if(count($event->internalUsers) > 0){
                    $registered_participants += count($event->internalUsers);
                }
            }

            if(isset($event->bootcampers)){
                if(count($event->bootcampers) > 0){
                    $registered_participants += count($event->bootcampers);
                }
            }

            $object = (object)['event' => $event, 'start_time' => $start_time, 'participant_count' => (int)$event->participants,
                'registered_participants' => $registered_participants];
            $start_time = "";
            array_push($events_array, $object);
        }

        return view('/full_calendar/pitching101', compact('events_array'));
    }

    //News letter with emailer
    public function storeNewsletter(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();

        $createNewsletter = Newsletter::create(['email'=>$input['email'],'fullname'=>$input['fullname']]);
        try {
            $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
            $fromEmail = "dev@propellaincubator.co.za";
            $toEmail = "dev@propellaincubator.co.za";
            $subject = "Newsletter subscriber";
            $htmlBody = "<p>Hi</p>
                       <p>A user with $createNewsletter->email just subscribed on Propella newsletter</p>

                       <p>With many thanks.</p>
                        The Propella Team";
            $textBody = "";
            $tag = "example-email-tag";
            $trackOpens = true;
            $trackLinks = "None";
            $messageStream = "broadcast";

            // Send an email:
            $sendResult = $client->sendEmail(
                $fromEmail,
                $toEmail,
                $subject,
                $htmlBody,
                $textBody,
                $tag,
                $trackOpens,
                NULL, // Reply To
                NULL, // CC
                NULL, // BCC
                NULL, // Header array
                NULL, // Attachment array
                $trackLinks,
                NULL, // Metadata array
                $messageStream
            );


            DB::commit();
            return response()->json(['message' => 'Thank you']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }
}
