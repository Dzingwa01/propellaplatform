<?php

namespace App\Http\Controllers;

use App\SmartTags;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class SmartTagsController extends Controller
{
    //returns table of smart tags
    public function smartTagIndex(){
        return view('users.smart-tag.smart-tag-index');
    }
    //add smart tags
    public function createSmartTag(){
        return view('users.smart-tag.create-smart-tag');
    }
    //store data on database
    public function storeSmartTag(Request $request){
        $input = $request->all();

        try {
            DB::beginTransaction();
            $smart_tag = SmartTags::create(['smart_tag_name' => $input['smart_tag_name']]);
            DB::commit();

            return response()->json(['message' => 'Smart City tag added.']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }
    //calls all smart tags to display on index
    public function getSmartTags(){
        $smart_tags = SmartTags::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($smart_tags)->addColumn('action', function ($smart_tag) {
            $re = '/smart-tag-edit/' . $smart_tag->id;
            $del = $smart_tag->id;
            return '<a href=' . $re . ' title="Edit Smart Tag" style="color:green!important;"><i class="material-icons">create</i></a><a id=' . $del . ' onclick="confirm_smart_tag(this)" title="Delete Smart Tag" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($smart_tags)->addColumn('action', function ($smart_tag) {
                $re = '/smart-tag-edit/' . $smart_tag->id;
                return '<a href=' . $re . ' title="Edit Smart Tag" style="color:green!important;"><i class="material-icons">create</i></a>';
            })
                ->make(true);
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }
    //updates selected records on database
    public function updateSmartTag(Request $request, SmartTags $smartTag){
        $input = $request->all();

        try{
            DB::beginTransaction();
            $smartTag->update(['smart_tag_name' => $input['smart_tag_name']]);
            $smartTag->save();
            DB::commit();
            return response()->json(['message' => 'Smart Tag updated.']);

        } catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }
    //makes changes to selected smart tags
    public function editSmartTag(SmartTags $smartTag){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.smart-tag.edit-smart-tag', compact('smartTag'));
        } elseif ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.smart-tag.edit-smart-tag', compact('smartTag'));
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    //deletes selected record
    public function deleteSmartTag(SmartTags $smartTag){
        try{
            $smartTag->forceDelete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occurred, please contact your IT Admin ' . $e->getMessage()]);
        }
    }
}
