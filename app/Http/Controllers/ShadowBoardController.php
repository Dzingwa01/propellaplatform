<?php

namespace App\Http\Controllers;

use App\ShadowBoardQuestion;
use App\ShadowBoardQuestionAnswer;
use App\ShadowBoardQuestionCategory;
use App\ShadowBoardSubQuestionText;
use App\WorkshopEvaluation;
use App\WorkshopEvaluationForm;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class ShadowBoardController extends Controller
{
    //Shadow Board Category

    public function createQuestionCategory(){

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.ShadowBoard.create_question_category');
        }elseif($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ShadowBoard.create_question_category');
        }elseif($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ShadowBoard.create_question_category');
        }
    }

    public function storeCategory(Request $request)
    {
        $input = $request->all();
        DB::beginTransaction();

        try {
            $create_category = ShadowBoardQuestionCategory::create(['category_name' => $input['category_name']]);

            DB::commit();
            return response()->json(['message'=>'Category added successfully','create_category'=>$create_category]);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function indexShadowCategory()
    {
        $category = ShadowBoardQuestionCategory::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.ShadowBoard.shadow-category-index', compact('category'));
        }elseif($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ShadowBoard.shadow-category-index', compact('category'));
        }elseif($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ShadowBoard.shadow-category-index', compact('category'));
        }
    }

    public function getShadowCategory(){

        $category = ShadowBoardQuestionCategory::all();
        $user = Auth::user()->load('roles');

        if($user->roles[0]->name=='app-admin') {
            return Datatables::of($category)->addColumn('action', function ($category) {
                $del = '/categoryDel/' . $category->id;
                $edit = '/editBoardCategory/' . $category->id;
                return '<a href=' . $edit . ' title="Edit Board Category" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name=='mentor') {
            return Datatables::of($category)->addColumn('action', function ($category) {
                $del = '/categoryDel/' . $category->id;
                $edit = '/editBoardCategory/' . $category->id;
                return '<a href=' . $edit . ' title="Edit Board Category" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name=='advisor') {
            return Datatables::of($category)->addColumn('action', function ($category) {
                $del = '/categoryDel/' . $category->id;
                $edit = '/editBoardCategory/' . $category->id;
                return '<a href=' . $edit . ' title="Edit Board Category" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function editShadowCategory(ShadowBoardQuestionCategory $boardQuestionCategory)
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.ShadowBoard.edit-category', compact('boardQuestionCategory'));
        }elseif  ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ShadowBoard.edit-category', compact('boardQuestionCategory'));
        }elseif  ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ShadowBoard.edit-category', compact('boardQuestionCategory'));
        }
    }

    public function updateCategory(Request $request, ShadowBoardQuestionCategory $boardQuestionCategory)
    {
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'advisor' or $user->roles[0]->name == 'mentor') {
            try {
                $boardQuestionCategory->update(['category_name' => $input['category_name']]);

                $boardQuestionCategory->save();

                DB::commit();
                return response()->json(['message' => 'Category updated successfully.', 'ShadowBoardQuestionCategory' => $boardQuestionCategory], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Category could not be saved at the moment ' . $e->getMessage()], 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }
    }

    public function categoryDestroy($id)
    {
        $category = ShadowBoardQuestionCategory::find($id);
        try {
            DB::beginTransaction();

            if (isset($category)) {
                $category->forceDelete();
            }
            DB::commit();
            return view('users.app-admin.ShadowBoard.shadow-category-index');
        } catch (\Exception $e) {
            return response()->json(['message' => "Something went wrong : " . $e->getMessage()]);
        }
    }

    //Shadow Board Questions

    public function createShadowQuestions()
    {
        $questionCategories = ShadowBoardQuestionCategory::orderBy('category_name', 'asc')->get();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.ShadowBoard.create-question', compact('questionCategories'));
        }elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ShadowBoard.create-question', compact('questionCategories'));
        }elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ShadowBoard.create-question', compact('questionCategories'));
        }
    }

    public function storeShadowQuestions(Request $request)
    {
        $input = $request->all();
        $sub_text_array = json_decode($input['sub_text_array']);

        DB::beginTransaction();

        try {
            $question = ShadowBoardQuestion::create(['question_text' => $input['question_text'],
                'question_category_id' => $input['question_category_id'],
                'question_number' => $input['question_number'],'question_type' => $input['question_type']]);


            if(count($sub_text_array) > 0){
                foreach ($sub_text_array as $sub_text){
                    $sub_shadow_text = ShadowBoardSubQuestionText::create(['question_sub_text' => $sub_text->question_sub_text, 'shadow_board_question_id' => $question->id]);
                }
            }
            DB::commit();
            return response()->json(['message' => 'Shadow board question added successfully', 'questions' => $question]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function shadowQuestionIndex()
    {
        $question = ShadowBoardQuestion::all()->load('shadowBoardQuestionCategory');
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.ShadowBoard.shadow-board-question-index', compact('question'));
        }elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ShadowBoard.shadow-board-question-index', compact('question'));
        }elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ShadowBoard.shadow-board-question-index', compact('question'));
        }
    }

    public function getShadowQuestions()
    {
        $questions = ShadowBoardQuestion::with('shadowBoardQuestionCategory')->get();

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($questions)->addColumn('action', function ($question) {
                $re = '/editBoardQuestions/' . $question->id;
                $del = '/destroyQuestions/' . $question->id;
                return '<a href=' . $re . ' title="Edit Question" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete Question" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'mentor') {
            return Datatables::of($questions)->addColumn('action', function ($question) {
                $re = '/editBoardQuestions/' . $question->id;
                $del = '/destroyQuestions/' . $question->id;
                return '<a href=' . $re . ' title="Edit Question" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete Question" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($questions)->addColumn('action', function ($question) {
                $re = '/editBoardQuestions/' . $question->id;
                $del = '/destroyQuestions/' . $question->id;
                return '<a href=' . $re . ' title="Edit Question" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete Question" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function editShadowQuestion(ShadowBoardQuestion $shadowBoardQuestion){
        $user = Auth::user()->load('roles');
        $shadowBoardQuestion->load('shadowBoardQuestionCategory','shadowBoardQuestionSubQuestionText');
        $questionCategories = ShadowBoardQuestionCategory::orderBy('category_name', 'asc')->get();

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.ShadowBoard.edit-shadow-questions', compact('shadowBoardQuestion','questionCategories'));
        }elseif ($user->roles[0]->name == 'mentor') {
            return view('users.mentor-dashboard.ShadowBoard.edit-shadow-questions', compact('shadowBoardQuestion','questionCategories'));
        }elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.ShadowBoard.edit-shadow-questions', compact('shadowBoardQuestion','questionCategories'));
        }
    }

    public function updateShadowQuestion(Request $request, ShadowBoardQuestion $shadowBoardQuestion){
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();
        $shadowBoardQuestion->load('shadowBoardQuestionCategory','shadowBoardQuestionSubQuestionText');

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'mentor' or $user->roles[0]->name == 'advisor') {
            try {
                $shadowBoardQuestion->update(['question_text' => $input['question_text'],
                    'question_number' => $input['question_number'],'question_category_id' => $input['question_category_id']]);

                if(isset($shadowBoardQuestion->shadowBoardQuestionSubQuestionText)){
                    foreach($shadowBoardQuestion->shadowBoardQuestionSubQuestionText as $questionSubText){
                        $questionSubText->update(['question_sub_text' =>$input['question_sub_text']]);
                    }
                }

                $shadowBoardQuestion->save();

                DB::commit();
                return response()->json(['message' => 'Question updated successfully.', 'shadowBoardQuestion' => $shadowBoardQuestion], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Question could not be saved at the moment ' . $e->getMessage(),'shadowBoardQuestion'=>$shadowBoardQuestion], 400);
            }
        } else {
            return response('<img style="margin-left: 500px;margin-top: 20vh" src="images/UNAUTHORIZED_ENTRY_L-500x353.png">');
        }

    }

    public function destroyShadowQuestion($id){
        try {
            DB::beginTransaction();
            $question = ShadowBoardQuestion::find($id);

            $question->forceDelete();
            DB::commit();
            return view('users.app-admin.ShadowBoard.shadow-board-question-index');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }
    }

    public function workshopIndex(){
        $question = WorkshopEvaluation::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Workshops.evaluation-index', compact('question'));
        }elseif($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.Workshops.evaluation-index', compact('question'));
        }
    }
    public function getWorkshopEvaluation()
    {
        $questions = WorkshopEvaluation::all();

        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($questions)->addColumn('action', function ($question) {
                $re = '/show-all-evaluation-info/' . $question->id;
                $del = '/delete-user-workshop/' . $question->id;
                return '<a href=' . $re . ' title="Edit Question" style="color:green!important;"><i class="material-icons">person</i></a><a href=' . $del . ' title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }elseif ($user->roles[0]->name == 'advisor') {
            return Datatables::of($questions)->addColumn('action', function ($question) {
                $re = '/show-all-evaluation-info/' . $question->id;
                $del = '/delete-user-workshop/' . $question->id;
                return '<a href=' . $re . ' title="Edit Question" style="color:green!important;"><i class="material-icons">person</i></a><a href=' . $del . ' title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function viewAllInfo(WorkshopEvaluation $workshopEvaluation){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.Workshops.view-all-info', compact('workshopEvaluation'));
        }elseif ($user->roles[0]->name == 'advisor') {
            return view('users.advisor-dashboard.Workshops.view-all-info', compact('workshopEvaluation'));
        }
    }

}
