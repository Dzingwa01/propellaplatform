<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == "app-admin") {
            return view('/auth/passwords/change');
        }
        elseif ($user->roles[0]->name == "administrator"){
            return view('users.administrators.passwords.change');
        }
        elseif ($user->roles[0]->name == 'incubatee'){
            return view('users.incubatee-dashboard.password.change');
        }
        elseif ($user->roles[0]->name == "marketing") {
            return view('users.marketing-dashboard.password.change');
        }
        elseif ($user->roles[0]->name == "advisor") {
            return view('users.advisor-dashboard.passwords.change');
        }
        elseif ($user->roles[0]->name == "mentor") {
            return view('users.mentor-dashboard.passwords.change');
        }
    }

    public function updatePassword(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current_password'), $request->get('password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $this->validate($request, [
            'current_password'     => 'required',
            'password'     => 'required|min:8',
            'password-confirm' => 'required|same:password',
        ]);

        $data = $request->all();
        $user = User::find(auth()->user()->id);

        if(!Hash::check($data['current_password'], $user->password)){
            return back()
                ->with('error','The specified password does not match the database password');
        }
        else{


            $user = User::find(Auth::id());
            /*dd($user);*/
            $user->password = bcrypt($request->get('password'));
            $user->save();
            Auth::logout();
            return redirect()->back()->with("success","Password changed successfully !");
        }


    }

}
