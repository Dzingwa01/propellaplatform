<?php

namespace App\Http\Controllers;

use App\Visitor;
use Illuminate\Http\Request;
use App\Notifications\CustomEmail;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    //
    public function getContact()
    {
        return view('contact');
    }

    /*public function postContact(Request $request){
        $this->validate($request, ['email' => 'required|email']);

        $data = array(
            'email' = $request->email,
            'surname' = $request->surname,
            'name' = $request->name
        );

        Mail::send('emails.contact', $data, function (){

        });
    }*/


    public function customEmail(Request $request)
    {
        if ($request->isMethod('get'))
            return view('custom_email');
        else {
            $rules = [
                'to_email' => 'required|email',
                'subject' => 'required',
                'message' => 'required',
            ];
            $this->validate($request, $rules);
            $visitor = new Visitor();
            $visitor->email = $request->to_email;
            $visitor->notify(new CustomEmail($request->subject, $request->message));
            $request->session()->put('status', true);
            return back();
        }
    }

    public function send(Request $request)
    {
        //Logic will go here
        $title = $request->input('title');
        $content = $request->input('content');

        Mail::send('emails.send', ['title' => $title, 'content' => $content], function ($message) {

            $message->from('khanyiso@propellaicubator.co.za', 'Khanyi');

            $message->to('chrisn@scotch.io');

        });


        return response()->json(['message' => 'Request completed']);
    }

    public function Mail()
    {
        $data = array(
            'name' => 'first_name',
            'email' => 'khuselwalukwe@gmail.com'

        );

        Mail::send('emails.contact', $data, function ($message) use ($data) {
            $message->from('noreply@propelladev.co.za', 'Propella Business Incubator')
                ->to($data['email'])
                ->subject('Propella Test Mail');

        });
    }
}
