<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Company;
use App\ApplicantPanelistExport;
use App\CompanyEmployee;
use App\Exports\CompaniesExport;
use App\Exports\CompanyDetailsExport;
use App\Exports\CompanyEmployeesExport;
use App\Exports\PanelistQuestionsExport;
use App\Exports\UsersExport;
use App\Exports\VentureEmployeesExport;
use App\Exports\VentureExport;
use App\Exports\VentureIncubateesExport;
use App\Exports\VisitorDeregisteredExport;
use App\Exports\VisitorDetailsExport;
use App\Exports\VisitorEventsExport;
use App\Exports\VisitorsExport;
use App\Exports\VisitorSummaryExport;
use App\Visitor;
use App\PanelistQuestionAnswer;
use App\Question;
use App\Venture;
use Illuminate\Http\Request;
use App\ApplicantPanelist;
use Maatwebsite\Excel\Facades\Excel;

class DownloadExcelController extends Controller
{

    public function downloadData($type)
    {
        $data = Venture::get()->toArray();

        Excel::create('users', function ($excel) use ($data) {
            $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export('xls');
    }

    public function downloadExcel(){
        return Excel::download(new VentureExport(), 'venture.xlsx');
    }

    public function downlaodApplicantResults(Applicant $applicant){
        return Excel::download(new PanelistQuestionsExport($applicant), 'Applicant.xlsx');
    }

    public function downloadVentureInformation(Venture $venture){
        return Excel::download(new VentureExport($venture), 'Venture.xlsx');
    }

    public function downloadVentureIncubatees(Venture $venture){
        return Excel::download(new VentureIncubateesExport($venture), 'VentureIncubatees.xlsx');
    }

    public function downloadVentureEmployees(Venture $venture){
        return Excel::download(new VentureEmployeesExport($venture), 'VentureEmployees.xlsx');
    }

    public function downloadAllVisitors(){
        return Excel::download(new VisitorsExport(), 'visitors.xlsx');
    }

    public function downloadVisitorDetails(Visitor $visitor){
        return Excel::download(new VisitorDetailsExport($visitor), 'visitordetails.xlsx');
    }

    public function downloadVisitorSummary(Visitor $visitor){
        return Excel::download(new VisitorSummaryExport($visitor), 'visitorsummary.xlsx');
    }

    public function downloadVisitorEvents(Visitor $visitor){
        return Excel::download(new VisitorEventsExport($visitor), 'visitorevents.xlsx');
    }

    public function downloadVisitorDeregisteredEvents(Visitor $visitor){
        return Excel::download(new VisitorDeregisteredExport($visitor), 'visitorderegisteredevents.xlsx');
    }

    public function downloadUsers(){
        return Excel::download(new UsersExport(), 'users.xlsx');
    }

    public function downloadCompanies(){
        return Excel::download(new CompaniesExport(), 'companies.xlsx');
    }

    public function downloadCompanyDetails(Company $company){
        return Excel::download(new CompanyDetailsExport($company), 'companydetails.xlsx');
    }

    public function downloadCompanyEmployees(Company $company){
        return Excel::download(new CompanyEmployeesExport($company), 'companyemployees.xlsx');
    }

    public function downloadCompanyEmployeeDetails(CompanyEmployee $companyEmployee){
        return Excel::download(new CompanyEmployeeDetailsExport($companyEmployee), 'companyemployeedetails.xlsx');
    }

    public function downloadCompanyEmployeeContactLog(CompanyEmployee $companyEmployee){
        return Excel::download(new CompanyEmployeeContactLogExport($companyEmployee), 'companyemployeedetails.xlsx');
    }
}
