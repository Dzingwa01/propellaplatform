<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\EventMediaImage;
use App\Library;
use App\LibraryImage;
use App\PropellaHubGallery;
use App\QuestionsCategory;
use App\SatelliteGallery;
use App\SatelliteHubMedia;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use function GuzzleHttp\Promise\all;

class PTIController extends Controller
{
    //
    public function ptiPage(){
        return view('home.PTIWebsite.pti_webpage');
    }

    //PTI Applicants
    public function ptiAapplicantsIndex()
    {
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'PTI Admin') {
            return view('users.pti-dashboard.applicant-index');
        }
    }

    //Services
    public function ptiServices(){
        return view('home.PTIWebsite.pti-services');
    }
    //Programme
    public function ptiProgramme(){
        return view('home.PTIWebsite.pti-programme');
    }
    //Mentors
    public function ptiMentors(){
        return view('home.PTIWebsite.pti-mentors');
    }

    //Apply
    public function ptiApply(){
        $category = QuestionsCategory::with('questions')->where('category_name','Application form - Propella Satellite')->get();

        return view('home.PTIWebsite.pti-apply',compact('category'));
    }

    public function ptiContact(){
        return view('home.PTIWebsite.pti-contact');
    }

    public function getPTIApplicants()
    {
        $logged_in_user = Auth::user()->load('roles');

        $applicants = User::whereHas('roles', function ($query) {$query->where('name', 'applicant');
        })->get();
        $applicant_array = [];

        foreach($applicants as $applicant){

            $applicant->load('userQuestionAnswers', 'applicant');

            if(count($applicant->userQuestionAnswers) > 0){
                if(($applicant->applicant->chosen_category == 'Township Hub Application')){
                    $object = (object)['name' => $applicant->name, 'surname' => $applicant->surname,
                        'email' => $applicant->email, 'phone' => $applicant->contact_number,
                        'category' => $applicant->applicant->chosen_category, 'date_time_applied' => $applicant->created_at->toDateString(),
                        'completed_form' => 'Yes', 'id' => $applicant->id];
                    array_push($applicant_array, $object);
                }
            }
        }

        if ($logged_in_user->roles[0]->name == 'PTI Admin') {
            return Datatables::of($applicant_array)->addColumn('action', function ($applicant) {
                $sh = '/applicant-show/' . $applicant->id;
                return '<a href=' . $sh . ' title="View Applicant"><i class="material-icons">remove_red_eye</i></a>';
            })
                ->make(true);
        }
    }

    //Gallery
    public function ptiGallery(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.pti.pti-gallery');
        }elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.pti.pti-gallery');
        }elseif($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.pti.pti-gallery');
        }
    }

    //Store gallery
    public function storeGallery(Request $request){
        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::now();
        $images_count = $input['image_array_count'];
        try{
            $create_gallery = PropellaHubGallery::create(['description' => $input['description'],'date_added'=>$date]);
            $create_gallery->save();
            if ($request->hasFile('single_image')) {
                $single_image_path = $request->file('single_image')->store('gallery_uploads');
                if (isset($create_gallery)) {
                    $create_gallery->update(['single_image' => $single_image_path]
                    );
                } else {
                    $create_gallery->create(['single_image' => $single_image_path]
                    );
                }
            }

            for ($i = 0; $i < $images_count; $i++) {
                $cur_image_url = $request->file('image_' . $i)->store('gallery_uploads');

                if ($cur_image_url) {
                    $create_media_images = SatelliteGallery::create(['image_url' => $cur_image_url, 'propella_hub_id' => $create_gallery->id]);
                }
            }


            DB::commit();
            return response()->json(['message'=>'File uploaded successfully']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }

    }

    //Index
    public function galleryIndex(){
        return view('users.administrators.pti.all-media-gallery');
    }
    //Get Gallery
    public function getGallery()
    {
        $gallery = PropellaHubGallery::all();
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($gallery)->addColumn('action', function ($media) {
                $re = '/edit-media-gallery/' . $media->id;
                $del = '/destroy-gallery/' . $media->id;
                return '<a href=' . $re . ' title="Edit Media" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Media" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'administrator') {
            return Datatables::of($gallery)->addColumn('action', function ($media) {
                $re = '/edit-media-gallery/' . $media->id;
                $del = '/destroy-gallery/' . $media->id;
                return '<a href=' . $re . ' title="Edit Media" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Media" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        } elseif ($user->roles[0]->name == 'marketing') {
            return Datatables::of($gallery)->addColumn('action', function ($media) {
                $re = '/edit-media-gallery/' . $media->id;
                $del = '/destroy-gallery/' . $media->id;
                return '<a href=' . $re . ' title="Edit Media" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete Media" style="color:red; cursor: pointer"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    //Show Gallery
    public function showPtiGallery(){
        $gallery = PropellaHubGallery::orderBy('date_added', 'desc')->get();
        return view('home.satellite-hub-gallery',compact('gallery'));
    }

    public function fullGallery(PropellaHubGallery $propellaHubGallery){
        $propellaHubGallery->load('satelliteMedia');
        $satellite_media = $propellaHubGallery->satelliteMedia;
        $media_images_array = [];

        if(isset($satellite_media)){
            if(isset($satellite_media)){
                foreach($satellite_media as $media_image){
                    array_push($media_images_array, $media_image);
                }
            }
        }
        return view('home.full-gallery-images',compact('propellaHubGallery','media_images_array'));
    }

    public function editGallery(PropellaHubGallery $propellaHubGallery){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'administrator') {
            return view('users.administrators.pti.edit-media-gallery', compact('propellaHubGallery'));
        }elseif ($user->roles[0]->name == 'app-admin') {
            return view('users.app-admin.pti.edit-media-gallery', compact('propellaHubGallery'));
        }elseif ($user->roles[0]->name == 'marketing') {
            return view('users.marketing-dashboard.pti.edit-media-gallery', compact('propellaHubGallery'));
        }
    }

    public function updateMediaGallery(Request $request, PropellaHubGallery $propellaHubGallery)
    {
        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::now();
        $images_count = $input['image_array_count'];

        $propellaHubGallery->load('satelliteMedia');
        try {
            $propellaHubGallery->update(['description' => $input['description'],
                'date_added' => $date]);
            $propellaHubGallery->save();
            if ($request->hasFile('single_image')) {
                $image_url_path = $request->file('single_image')->store('gallery_uploads');
                if (isset($image_url_path)) {
                    $propellaHubGallery->update(['single_image' => $image_url_path]
                    );
                } else {
                    $propellaHubGallery->create(['single_image' => $image_url_path]
                    );
                }
            }
            if(isset($propellaHubGallery->satelliteMedia)){
                for ($i = 0; $i < $images_count; $i++) {
                    $cur_image_url = $request->file('image_' . $i)->store('gallery_uploads');

                    if ($cur_image_url) {
                        $create_media_images = SatelliteGallery::create(['image_url' => $cur_image_url, 'propella_hub_id' => $propellaHubGallery->id]);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'Successfully updated Media']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function destroyGallery(PropellaHubGallery $propellaHubGallery)
    {
        try {
            DB::beginTransaction();
            $propellaHubGallery->forceDelete();
            DB::commit();
            return redirect('gallery-index');
        } catch (\Exception $e) {
            return response('Something went wrong while deleting: ' . $e->getMessage());
        }

    }
}
