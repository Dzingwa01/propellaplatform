<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Applicant;
use App\ApplicantContactLog;
use App\DeclinedApplicant;
use App\DeclinedApplicantContactLog;
use App\PropellaReferredApplicant;
use App\Question;
use App\ReferredApplicantCategory;
use App\Role;
use App\TiaApplication;
use App\UserQuestionAnswer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\QuestionsCategory;
use Illuminate\Support\Facades\Mail;
use App\Mail\applicationMail;
use Postmark\PostmarkClient;



class ApplicationProcessController extends Controller
{

    //Return tioa appli
    public function tiaApplication(){
        return view('Application-Process.tia-application');
    }

    //Store tia applications
    public function storeTiaApplication(Request $request)
    {
        $input = $request->all();

        try {
            DB::beginTransaction();
            $storeTia = TiaApplication::create(['full_name' => $input['full_name'], 'contact_number' => $input['contact_number'],
                'email' => $input['email'], 'innovation_description' => $input['innovation_description'],
                'proof_of_concept' => $input['proof_of_concept'], 'if_yes' => $input['if_yes'],
                'competitors' => $input['competitors'], 'market_size' => $input['market_size']]
            );

            DB::commit();

            return response()->json(['message' => 'Thank your for your application '], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }


    //Return the basic info page
    public function index(){
        $role = Role::where('name', 'applicant')->firstOrFail();

        return view('Application-Process.Basic-info', compact('role'));
    }

    public function rapApplication(){
        $role = Role::where('name', 'applicant')->firstOrFail();

        return view('Application-Process.rap-application', compact('role'));
    }
    public function indApplication(){
        $role = Role::where('name', 'applicant')->firstOrFail();

        return view('Application-Process.ind-application', compact('role'));
    }
    public function ictApplication(){
        $role = Role::where('name', 'applicant')->firstOrFail();

        return view('Application-Process.pti-ict-application', compact('role'));
    }
    public function pbiApplication(){
        $role = Role::where('name', 'applicant')->firstOrFail();

        return view('Application-Process.pbi-ict-application', compact('role'));
    }

    public function indexBootcamp(){
        $role = Role::where('name', 'applicant')->firstOrFail();

        return view('Application-Process.Basic-info-bootcamp', compact('role'));
    }

    //Return the 'choose category' page
    public function category(){
        $questionCategories = QuestionsCategory::where([['category_name', '!=', 'Propella Stage 1 Judging Criteria'],
            ['category_name', '!=', 'Propella Stage 2 Judging Criteria'],['category_name', '!=', 'Propella Industrial Judging Criteria II'],
            ['category_name', '!=', 'ICT BOOT CAMP (STAGE 1) JUDGING CRITERIA'], ['category_name', '!=', 'ICT STAGE 2 JUDGING CRITERIA'],
            ['category_name', '!=','Industrial Onboarding Panel Questions v1'], ['category_name', '!=', 'Industrial Onboarding Panel Questions v2'],
            ['category_name', '!=','Bootcamp evaluation form'],['category_name', '!=','Workshop Evaluation Form'],
            ['category_name', '!=','RAP - Judging Criteria'],['category_name','!=','Stage 2 Evaluation Form'],
            ['category_name','!=','ICT STAGE 2 JUDGING CRITERIA 2020'],['category_name','!=','ICT STAGE 2 JUDGING CRITERIA 2021'],
            ['category_name','!=','Stage 3 workshop evaluation form'],['category_name','!=','Stage 4 workshop evaluation form'],
            ['category_name','!=','Stage 5 workshop evaluation form'],['category_name','!=','Stage 6 workshop evaluation form'],
            ['category_name','!=','End of Stage 3 Workshop Evaluation Form']])->get();

        return view('Application-Process.question-category', compact('questionCategories'));
    }

    //Return the 'choose category'
    public function rapPtiCategory(){
        $questionCategorie = QuestionsCategory::where([['category_name', '!=', 'Propella Stage 1 Judging Criteria'],
            ['category_name', '!=', 'Propella Stage 2 Judging Criteria'],['category_name', '!=', 'Propella Industrial Judging Criteria II'],
            ['category_name', '!=', 'ICT BOOT CAMP (STAGE 1) JUDGING CRITERIA'], ['category_name', '!=', 'ICT STAGE 2 JUDGING CRITERIA'],
            ['category_name', '!=','Industrial Onboarding Panel Questions v1'], ['category_name', '!=', 'Industrial Onboarding Panel Questions v2'],
            ['category_name', '!=','Bootcamp evaluation form'],['category_name', '!=','Workshop Evaluation Form'],
            ['category_name', '!=','RAP - Judging Criteria'],['category_name','!=','Stage 2 Evaluation Form'],
            ['category_name','!=','ICT STAGE 2 JUDGING CRITERIA 2020'],['category_name','!=','ICT STAGE 2 JUDGING CRITERIA 2021'],
            ['category_name','!=','Application form - ICT Bootcamp'],['category_name','!=','Application form - Industrial'],
            ['category_name','!=','Stage 3 workshop evaluation form'],['category_name','!=','Stage 4 workshop evaluation form'],
            ['category_name','!=','Stage 5 workshop evaluation form'],['category_name','!=','Stage 6 workshop evaluation form'],
            ['category_name','!=','End of Stage 3 Workshop Evaluation Form']
        ])->get();

        return view('Application-Process.rap-and-pti-category', compact('questionCategorie'));
    }

    //Return only Industrial Application
    public function industrialCategory(){
        $questionCategorie = QuestionsCategory::where([['category_name', '!=', 'Propella Stage 1 Judging Criteria'],
            ['category_name', '!=', 'Propella Stage 2 Judging Criteria'],['category_name', '!=', 'Propella Industrial Judging Criteria II'],
            ['category_name', '!=', 'ICT BOOT CAMP (STAGE 1) JUDGING CRITERIA'], ['category_name', '!=', 'ICT STAGE 2 JUDGING CRITERIA'],
            ['category_name', '!=','Industrial Onboarding Panel Questions v1'], ['category_name', '!=', 'Industrial Onboarding Panel Questions v2'],
            ['category_name', '!=','Bootcamp evaluation form'],['category_name', '!=','Workshop Evaluation Form'],
            ['category_name', '!=','RAP - Judging Criteria'],['category_name','!=','Stage 2 Evaluation Form'],
            ['category_name','!=','ICT STAGE 2 JUDGING CRITERIA 2020'],['category_name','!=','ICT STAGE 2 JUDGING CRITERIA 2021'],
            ['category_name','!=','Application form - ICT Bootcamp'], ['category_name','!=','Stage 3 workshop evaluation form'],
            ['category_name','!=','Stage 4 workshop evaluation form'],['category_name','!=','Stage 5 workshop evaluation form'],
            ['category_name','!=','Stage 6 workshop evaluation form'],['category_name','!=','Application form - Propella Township Incubator'],
            ['category_name','!=','Application form - RAP Programme'],
            ['category_name','!=','End of Stage 3 Workshop Evaluation Form']
        ])->get();

        return view('Application-Process.industrial-application', compact('questionCategorie'));
    }

    public function deleteDeclinedApplicant(DeclinedApplicant $declinedApplicant){
        try{
            $declinedApplicant->forceDelete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    public function deleteReferredApplicants(PropellaReferredApplicant $propellaReferredApplicant)
    {
        $propellaReferredApplicant->load('question_answers');
        try {
            DB::beginTransaction();

            if(isset($propellaReferredApplicant->question_answers)){
                $propellaReferredApplicant->question_answers()->forceDelete();
            }
            $propellaReferredApplicant->forceDelete();
            DB::commit();
            return response()->json(['message' => 'Application deleted successfully'], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occured, please contact your IT Admin ' . $e->getMessage()]);
        }
    }

    //Function to load questions dynamically
    public function loadQuestions(User $user)
    {
        $user->load('applicant');
        $applicant_category = $user->applicant->chosen_category;
        $question_category = QuestionsCategory::where('category_name', $applicant_category)->firstOrFail();
        $question_category->load('questions');
        $questions = $question_category->questions->sortBy('question_number');

        return view('Application-Process.questions', compact('question_category', 'questions', 'user'));
    }

    //Function to upload Basic Info to Db
    public function storeBasicInfo(Request $request)
    {
        $users_in_db = User::all();
        $input = $request->all();
        $category_id = $input['category_id'];
        $category = QuestionsCategory::find($category_id);

        foreach ($users_in_db as $user_in_db) {
            if ($user_in_db->email == $input['email']) {
                return response()->json(['message' => 'Email']);
            }
        }

        try {
            DB::beginTransaction();
            $user = User::create(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'], 'email' => $input['email'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'], 'gender' => $input['gender'],'whatsapp_number' => $input['whatsapp_number'],
                'password' => Hash::make('Password_1234'), 'race' => $input['race']], 'role_id', $input['role_id']

            );

            $user->roles()->attach($input['role_id']);

            $applicant = Applicant::create(['venture_name' => $input['venture_name'], 'user_id' => $user->id,
                'chosen_category' => $category->category_name, 'popi_act_agreement'=> $input['popi_act_agreement'],
                'data_protected'=> $input['data_protected']]);
            

            DB::commit();

            return response()->json(['user' => $user, 'message' => 'Welcome to the system ' . $user->name . '. Onwards and Upwards!.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    public function storeBasicInfoBootcamp(Request $request)
    {
        $cat3_id = "c5673ef0-6aaf-11eb-99c2-bb714b6c5453";
        $users_in_db = User::all();
        $input = $request->all();
        $category_id = $cat3_id;
        $category = QuestionsCategory::find($category_id);

        foreach ($users_in_db as $user_in_db) {
            if ($user_in_db->email == $input['email']) {
                return response()->json(['message' => 'Email']);
            }
        }

        try {
            DB::beginTransaction();
            $user = User::create(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'], 'email' => $input['email'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'], 'gender' => $input['gender'],
                'password' => Hash::make('Password_1234'), 'race' => $input['race']], 'role_id', $input['role_id']);

            $user->roles()->attach($input['role_id']);

            $applicant = Applicant::create(['venture_name' => $input['venture_name'], 'user_id' => $user->id,
                'chosen_category' => $category->category_name]);

            $email = $input['email'];
            $password = 'Password_1234';
            $link = 'https://thepropella.co.za/login';
            $data = array(
                'name' => $input['name'],
                'email' => $email,
                'password' => $password,
                'link' => $link
            );

            $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
            $fromEmail = "dev@propellaincubator.co.za";
            $toEmail = $email;
            $subject = "THANK YOU FOR APPLYING";
            $htmlBody = "<p>Dear <b>applicant</b></p>
            <br>
            <p>Thank you for applying to our program, your application is being processed you can use your email $email.
              and password Password_1234 to login and check your application status.</p>

                 <p> Warm regards</p>
                  <p> The Propella Team</p>";
            $textBody = "";
            $tag = "example-email-tag";
            $trackOpens = true;
            $trackLinks = "None";
            $messageStream = "broadcast";

            // Send an email:
            $sendResult = $client->sendEmail(
                $fromEmail,
                $toEmail,
                $subject,
                $htmlBody,
                $textBody,
                $tag,
                $trackOpens,
                NULL, // Reply To
                NULL, // CC
                NULL, // BCC
                NULL, // Header array
                NULL, // Attachment array
                $trackLinks,
                NULL, // Metadata array
                $messageStream
            );


           /*
            $email_field = $email;
            Mail::send('emails.application', $data, function ($message) use ($email_field) {
                $message->to($email_field)
                    ->subject('THANK YOU FOR APPLYING');
                $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

            });*/

            DB::commit();

            return response()->json(['user' => $user, 'message' => 'Welcome to the system ' . $user->name . '. Onwards and Upwards!.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }
    public function storeIctApplication(Request $request)
    {
        $cat3_id = "37ca6bc0-bbd7-11ea-be56-195655914985";
        $users_in_db = User::all();
        $input = $request->all();
        $category_id = $cat3_id;
        $category = QuestionsCategory::find($category_id);

        foreach ($users_in_db as $user_in_db) {
            if ($user_in_db->email == $input['email']) {
                return response()->json(['message' => 'Email']);
            }
        }

        try {
            DB::beginTransaction();
            $user = User::create(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'], 'email' => $input['email'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'], 'gender' => $input['gender'],
                'password' => Hash::make('Password_1234'), 'race' => $input['race']], 'role_id', $input['role_id']);

            $user->roles()->attach($input['role_id']);

            $applicant = Applicant::create(['venture_name' => $input['venture_name'], 'user_id' => $user->id,
                'chosen_category' => $category->category_name,'popi_act_agreement' => $input['popi_act_agreement'],
                'data_protected' => $input['data_protected']]);


            DB::commit();

            return response()->json(['user' => $user, 'message' => 'Welcome to the system ' . $user->name . '. Onwards and Upwards!.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }
    public function storePbiApplication(Request $request)
    {
        $cat3_id = "f5c0d620-8dee-11ea-8e8c-8d76e789ba2d";
        $users_in_db = User::all();
        $input = $request->all();
        $category_id = $cat3_id;
        $category = QuestionsCategory::find($category_id);

        foreach ($users_in_db as $user_in_db) {
            if ($user_in_db->email == $input['email']) {
                return response()->json(['message' => 'Email']);
            }
        }

        try {
            DB::beginTransaction();
            $user = User::create(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'], 'email' => $input['email'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'], 'gender' => $input['gender'],
                'password' => Hash::make('Password_1234'), 'race' => $input['race'], 'role_id', $input['role_id'],
                'whatsapp_number'=>$input['whatsapp_number'],'service_provider_network' => $input['service_provider_network'],
                'data_cellnumber' => $input['data_cellnumber']]);

            $user->roles()->attach($input['role_id']);

            $applicant = Applicant::create(['venture_name' => $input['venture_name'], 'user_id' => $user->id,
                'chosen_category' => $category->category_name,'popi_act_agreement' => $input['popi_act_agreement'],
                'data_protected' => $input['data_protected']]);

            DB::commit();

            return response()->json(['user' => $user, 'message' => 'Welcome to the system ' . $user->name . '. Onwards and Upwards!.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }
    public function storeIndApplication(Request $request)
    {
        $cat3_id = "ca6f6c10-8dee-11ea-a709-158a52ddef29";
        $users_in_db = User::all();
        $input = $request->all();
        $category_id = $cat3_id;
        $category = QuestionsCategory::find($category_id);

        foreach ($users_in_db as $user_in_db) {
            if ($user_in_db->email == $input['email']) {
                return response()->json(['message' => 'Email']);
            }
        }

        try {
            DB::beginTransaction();
            $user = User::create(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'], 'email' => $input['email'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'], 'gender' => $input['gender'],
                'password' => Hash::make('Password_1234'), 'race' => $input['race']], 'role_id', $input['role_id']);

            $user->roles()->attach($input['role_id']);

            $applicant = Applicant::create(['venture_name' => $input['venture_name'], 'user_id' => $user->id,
                'chosen_category' => $category->category_name,'popi_act_agreement' => $input['popi_act_agreement'],
                'data_protected' => $input['data_protected']]);


            DB::commit();

            return response()->json(['user' => $user, 'message' => 'Welcome to the system ' . $user->name . '. Onwards and Upwards!.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    public function storeRapApplication(Request $request)
    {
        $cat3_id = "c5673ef0-6aaf-11eb-99c2-bb714b6c5453";
        $users_in_db = User::all();
        $input = $request->all();
        $category_id = $cat3_id;
        $category = QuestionsCategory::find($category_id);

        foreach ($users_in_db as $user_in_db) {
            if ($user_in_db->email == $input['email']) {
                return response()->json(['message' => 'Email']);
            }
        }

        try {
            DB::beginTransaction();
            $user = User::create(['title' => $input['title'], 'initials' => $input['initials'],
                'name' => $input['name'], 'surname' => $input['surname'],
                'contact_number' => $input['contact_number'], 'email' => $input['email'],
                'id_number' => $input['id_number'], 'address_one' => $input['address_one'],
                'address_two' => $input['address_two'], 'address_three' => $input['address_three'],
                'city' => $input['city'], 'code' => $input['code'], 'age' => $input['age'], 'gender' => $input['gender'],
                'password' => Hash::make('Password_1234'), 'race' => $input['race']], 'role_id', $input['role_id']);

            $user->roles()->attach($input['role_id']);

            $applicant = Applicant::create(['venture_name' => $input['venture_name'], 'user_id' => $user->id,
                'chosen_category' => $category->category_name,'popi_act_agreement' => $input['popi_act_agreement'],
                'data_protected'=> $input['data_protected']]);


            DB::commit();

            return response()->json(['user' => $user, 'message' => 'Welcome to the system ' . $user->name . '. Onwards and Upwards!.'], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }
    //Function to store Questions and Answers to a User
    public function storeAnswers(Request $request)
    {
        $input = $request->all();

        try {
            DB::beginTransaction();
            $questions_answers = json_decode($input['questions_answers']);
            $user_id = $questions_answers[0]->user_id;
            $applicant_user = User::find($user_id);
            $applicant_user->load('userQuestionAnswers', 'applicant');

            if (count($applicant_user->userQuestionAnswers) > 0) {
                foreach ($questions_answers as $question_answer) {
                    $question_id = $question_answer->question_id;
                    $testing = DB::table('user_question_answers')->where('question_id', $question_id)
                        ->where('user_id', $user_id)->first();

                    if (isset($testing)) {
                        DB::table('user_question_answers')->where('question_id', $question_id)
                            ->where('user_id', $user_id)->update(['answer_text' => $question_answer->answer_text]);
                        $applicant_user->applicant->application_completed = true;
                        $applicant_user->applicant->save();
                    } else {
                        $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                            'answer_text' => $question_answer->answer_text]);
                        $applicant_user->applicant->application_completed = false;
                        $applicant_user->applicant->save();
                    }
                }

            }else{
                foreach ($questions_answers as $question_answer) {
                    $question_id = $question_answer->question_id;
                    $testing = DB::table('user_question_answers')->where('question_id', $question_id)
                        ->where('user_id', $user_id)->first();

                    if (isset($testing)) {
                        DB::table('user_question_answers')->where('question_id', $question_id)
                            ->where('user_id', $user_id)->update(['answer_text' => $question_answer->answer_text]);
                        $applicant_user->applicant->application_completed = true;
                        $applicant_user->applicant->save();
                    } else {
                        $create_question_answer = UserQuestionAnswer::create(['user_id' => $user_id, 'question_id' => $question_answer->question_id,
                            'answer_text' => $question_answer->answer_text]);
                        $applicant_user->applicant->application_completed = true;
                        $applicant_user->applicant->save();
                    }
                }

            }


           /* EMAILER*/
            $name = $applicant_user->name;
            $email = $applicant_user->email;
            $password = 'Password_1234';
            $link = 'https://thepropella.co.za/login';


           $data = array(
                'email' => $email,
                'name' => $name,
                'password'=>$password,
                 'link' => $link
            );


            $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
            $fromEmail = "dev@propellaincubator.co.za";
            $toEmail = $email;
            $subject = "Your Application at Propella Business Incubator";
            $htmlBody = "<p>Dear <b>$name</b></p>
            <br>
            <p>Thank you for applying to our program, your application is being processed you can use your email $email.
              and password Password_1234 to login and check your application status.</p>

                 <p> Warm regards</p>
                  <p> The Propella Team</p>";
            $textBody = "Hello dear Postmark user.";
            $tag = "example-email-tag";
            $trackOpens = true;
            $trackLinks = "None";
            $messageStream = "broadcast";

            // Send an email:
            $sendResult = $client->sendEmail(
                $fromEmail,
                $toEmail,
                $subject,
                $htmlBody,
                $textBody,
                $tag,
                $trackOpens,
                NULL, // Reply To
                NULL, // CC
                NULL, // BCC
                NULL, // Header array
                NULL, // Attachment array
                $trackLinks,
                NULL, // Metadata array
                $messageStream
            );

            /*$email_field = $email;
            Mail::send('emails.application', $data, function ($message) use ($email_field) {
                $message->to($email_field)
                    ->subject('Your Application at Propella Business Incubator');
                $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');

            });*/

            DB::commit();

            return response()->json(['message' => 'Thank you.', 'user' => $applicant_user], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Oops, something went wrong.' . $e->getMessage()], 400);
        }
    }

    //Applicant Contact Log
    public function storeApplicantContactLog(Request $request,User $user)
    {
        DB::beginTransaction();
        $input = $request->all();

        $user->load('applicant');
        $applicant = $user->applicant;
        $date = Carbon::today();

        try {
            if ($input['applicant_contact_results'] == 'Email') {
                $contact_log = ApplicantContactLog::create(['email_comment' => $input['email_comment'],'comment' => $input['comment'], 'date' => $date,
                    'applicant_contact_results' => $input['applicant_contact_results'],
                    'applicant_id' => $applicant->id]);


                $email =$user->email;
                $name = $user->name;
                $link = 'https://thepropella.co.za/login';
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'email_comment' => $input['email_comment']
                );

                $client = new PostmarkClient("29b4173d-3508-48ca-a67c-936a449d6892");
                $fromEmail = "dev@propellaincubator.co.za";
                $toEmail = $email;
                $subject = "Application to Propella Business Incubator";
                $htmlBody = "<p>Dear <b>$name</b></p>
                <p> You applied to Propella Business Incubator ICT Program  .
                Make contact with Propella by with emailing us on reception@propellaincubator.co.za or calling us on 041 502 3700.
                <br/>

                <p>To log into your profile please follow this link.</p>
                <br>
                <p>$link</a></p>
                <br>
                <p><b>Log in details</b></p>
                <p>User name:  Please use the email address that you applied with</p>
                <p>Password: Password_1234</p>
                <br/>
                <p>With many thanks.</p>
                The Propella Team";
                $textBody = "";
                $tag = "example-email-tag";
                $trackOpens = true;
                $trackLinks = "None";
                $messageStream = "broadcast";

                // Send an email:
                $sendResult = $client->sendEmail(
                    $fromEmail,
                    $toEmail,
                    $subject,
                    $htmlBody,
                    $textBody,
                    $tag,
                    $trackOpens,
                    NULL, // Reply To
                    NULL, // CC
                    NULL, // BCC
                    NULL, // Header array
                    NULL, // Attachment array
                    $trackLinks,
                    NULL, // Metadata array
                    $messageStream
                );

               /* $email_field = $email;
                Mail::send('emails.applicant-status-emailer', $data, function ($message) use ($email_field) {
                $message->to($email_field)
                    ->subject('Application to Propella Business Incubator');
                $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                });*/

                DB::commit();
                return response()->json(['message' => 'Log added successfully']);

            }else{
                $contact_log = ApplicantContactLog::create(['comment' => $input['comment'], 'date' => $date,
                    'applicant_contact_results' => $input['applicant_contact_results'],
                    'applicant_id' => $applicant->id]);


                DB::commit();
                return response()->json(['message' => 'Log added successfully']);
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    //store status
    public function updateApplicantStatus(Request $request,User $user)
    {
        DB::beginTransaction();
        $input = $request->all();
        $user->load('applicant');
        $user_applicant = $user->applicant;
        try {
            $user_applicant->update(['status' => $input['status']]);

            $user_applicant->save();


            DB::commit();
            return response()->json(['message' => 'Status updated successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    //Update applicant chosen category
    public function updateApplicantChosenCategory(Request $request,User $user)
    {
        DB::beginTransaction();
        $input = $request->all();
        $user->load('applicant');
        $user_applicant = $user->applicant;
        try {

            $user_applicant->update(['chosen_category' => $input['chosen_category']]);

            $user_applicant->save();


            DB::commit();
            return response()->json(['message' => 'Chosen category updated successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    //Declined Applicant Contact Log
    public function storeDeclinedApplicantContactLog(Request $request,DeclinedApplicant $declinedApplicant)
    {
        DB::beginTransaction();
        $input = $request->all();

        $date = Carbon::today();

        try {
            if ($input['applicant_contact_results'] == 'Email') {
                $contact_log = DeclinedApplicantContactLog::create(['comment' => $input['comment'], 'date' => $date,
                    'applicant_contact_results' => $input['applicant_contact_results'],
                    'declined_applicant_id' => $declinedApplicant->id]);


              /*  $email =$declinedApplicant->email;
                $name = $declinedApplicant->name;
                $data = array(
                    'name' => $name,
                    'email' => $email,
                    'comment' => $input['comment']
                );


               mail::send('emails.declined-applicant-contact-log-emailer', $data, function ($message) use ($email_field) {
                    $message->to($email_field)
                        ->subject('Application to Propella Business Incubator');
                    $message->from('propellabusiness@gmail.com', 'Propella Business Incubator');
                });*/

                DB::commit();
                return response()->json(['message' => 'Log added successfully']);

            }else{
                $contact_log = DeclinedApplicantContactLog::create(['comment' => $input['comment'], 'date' => $date,
                    'applicant_contact_results' => $input['applicant_contact_results'],
                    'declined_applicant_id' => $declinedApplicant->id]);


                DB::commit();
                return response()->json(['message' => 'Log added successfully']);
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }


}
