<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Applicant extends BaseModel
{
    use Notifiable;

    protected $fillable = ['user_id', 'referred', 'declined',
        'venture_name', 'chosen_category', 'contacted_via', 'result_of_contact',
        'declined_reason', 'referred_company', 'is_incubatee','status','is_bootcamper','popi_act_agreement','data_protected'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function panelists(){
        return $this->hasMany(ApplicantPanelist::class, 'applicant_id');
    }

    //Contact Log
    public function contactLog(){
        return $this->hasMany(ApplicantContactLog::class, 'applicant_id');
    }
}
