<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShadowBoardQuestionCategory extends BaseModel
{
    protected $fillable =['category_name'];

    public function shadowBoardQuestion(){
        return $this->hasMany(ShadowBoardQuestion::class,'question_category_id');
    }
}
