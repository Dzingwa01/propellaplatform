<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureQuestionAnswer extends BaseModel
{
    protected $fillable = ['shadow_board_question_id','answer_text','venture_id','mentor_venture_id'];

    public function shadowQuestion(){
        return $this->belongsTo(ShadowBoardQuestion::class, 'shadow_board_question_id');
    }
    public function venture(){
        return $this->belongsTo(Venture::class,'venture_id');
    }

    public function mentorVentureShadowboard(){
        return $this->belongsTo(MentorVentureShadowboard::class,'mentor_venture_id');
    }
}
