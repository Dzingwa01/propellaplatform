<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $super_admin = [
            'super-delete' => true,
            'super-add' => true,
            'super-update' => true,
            'super-view' => true,
        ];

        $office_clerk = [
            'clerk-delete' => true,
            'clerk-add' => true,
            'clerk-update' => true,
            'clerk-view' => true,
        ];

        $incubatee = [
            'incubatee-delete' => true,
            'incubatee-add' => true,
            'incubatee-update' => true,
            'incubatee-view' => true,
        ];

        $partner = [
            'partner-delete' => true,
            'partner-add' => true,
            'partner-update' => true,
            'partner-view' => true,
        ];

        $applicant = [
            'applicant-view' => true,
        ];

        $administrator = [
            'administrator-delete' => true,
            'administrator-add' => true,
            'administrator-update' => true,
            'administrator-view' => true,
        ];

        $marketing = [
            'marketing-delete' => true,
            'marketing-add' => true,
            'marketing-update' => true,
            'marketing-view' => true,
        ];

        $advisor = [
            'advisor-delete' => true,
            'advisor-add' => true,
            'advisor-update' => true,
            'advisor-view' => true,
        ];

        $mentor = [
            'mentor-delete' => true,
            'mentor-add' => true,
            'mentor-update' => true,
            'mentor-view' => true,
        ];

        $tenant = [
            'tenant-delete' => true,
            'tenant-add' => true,
            'tenant-update' => true,
            'tenant-view' => true,
        ];

        $super_user = Role::create([
            'name' => 'app-admin',
            'display_name'=>'App Admin',
            'permissions' =>$super_admin,
            'guard_name'=>'web'
        ]);

        $clerk_user1 = Role::create([
            'name' => 'incubatee',
            'display_name'=>'Incubatee',
            'permissions' =>$incubatee,
            'guard_name'=>'web'
        ]);

        $partner_1 = Role::create([
            'name' => 'partner',
            'display_name'=>'Partner',
            'permissions' =>$partner,
            'guard_name'=>'web'
        ]);

        $clerk_user = Role::create([
            'name' => 'clerk',
            'display_name'=>'PR & Marketing',
            'permissions' =>$office_clerk,
            'guard_name'=>'web'
        ]);

        $applicant_user = Role::create([
            'name' => 'applicant',
            'display_name' => 'Applicant',
            'permissions' => $applicant,
            'guard_name' => 'web'
        ]);

        $administrator_user = Role::create([
            'name' => 'administrator',
            'display_name' => 'Administrator',
            'permissions' => $administrator,
            'guard_name' => 'web'
        ]);

        $marketing_user = Role::create([
            'name' => 'marketing',
            'display_name' => 'Marketing',
            'permissions' => $marketing,
            'guard_name' => 'web'
        ]);

        $advisor_user = Role::create([
            'name' => 'advisor',
            'display_name' => 'Advisor',
            'permissions' => $advisor,
            'guard_name' => 'web'
        ]);

        $mentor_user = Role::create([
            'name' => 'mentor',
            'display_name' => 'Mentor',
            'permissions' => $mentor,
            'guard_name' => 'web'
        ]);

       /* $tenant_user = Role::create([
            'name' => 'tenant',
            'display_name' => 'Tenant',
            'permissions' => $tenant,
            'guard_name' => 'web'
        ]);*/
    }
}
