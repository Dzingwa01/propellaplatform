<?php

use Illuminate\Database\Seeder;
use App\QuestionType;

class QuestionsDatatypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datatype1 = QuestionType::create(["datatype"=>"string"]);
        $datatype2 = QuestionType::create(["datatype"=>"boolean"]);
    }
}
