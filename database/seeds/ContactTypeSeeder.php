<?php

use Illuminate\Database\Seeder;
use App\ContactType;

class ContactTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $sector_1 = ContactType::create(["type_name"=>"We mail"]);
        $sector_2 = ContactType::create(["type_name"=>"We phone"]);
        $sector_3 = ContactType::create(["type_name"=>"We visit"]);
        $sector_4 = ContactType::create(["type_name"=>"Client emailed"]);
        $sector_5 = ContactType::create(["type_name"=>"Client phoned"]);
        $sector_6 = ContactType::create(["type_name"=>"Client phoned"]);
        $sector_7 = ContactType::create(["type_name"=>"Client visited"]);
    }
}
