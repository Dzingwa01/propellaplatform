<?php

use Illuminate\Database\Seeder;
use App\ResourceCategory;

class ResourceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_1 = ResourceCategory::create(["category_name"=>"Grant"]);
        $category_2 = ResourceCategory::create(["category_name"=>"Funding"]);
        $category_3 = ResourceCategory::create(["category_name"=>"Competition"]);
        $category_4 = ResourceCategory::create(["category_name"=>"Crowd Funding"]);

    }
}
