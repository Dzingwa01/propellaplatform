<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $password = \Illuminate\Support\Facades\Hash::make('secret');
        $verification_time = \Carbon\Carbon::now();

        $super = User::create(['name'=>'Anita','surname'=>'User','email'=>'anita@propellaincubator.co.za'
            ,'contact_number'=>'076677777','email_verified_at'=>$verification_time,'password'=>$password]);

        $super_role = Role::where('name','app-admin')->first();
        $super->roles()->attach($super_role->id);

        $clerk = User::create(['name'=>'Clerk','surname'=>'User','email'=>'clerk@propellaincubator.co.za'
            ,'contact_number'=>'076677777','email_verified_at'=>$verification_time,'password'=>$password]);

        $clerk_role = Role::where('name','clerk')->first();
        $clerk->roles()->attach($clerk_role->id);

        $administrator = User::create(['name'=>'Mara','surname'=>'User','email'=>'mara@propellaincubator.co.za'
            ,'contact_number'=>'076677777','email_verified_at'=>$verification_time,'password'=>$password]);

        $administrator_role = Role::where('name','administrator')->first();
        $administrator->roles()->attach($administrator_role->id);

        $incubatee = User::create(['name'=>'Kaizer','surname'=>'User','email'=>'kaizer@propellaincubator.co.za'
            ,'contact_number'=>'076677777','email_verified_at'=>$verification_time,'password'=>$password]);

        $incubatee_role = Role::where('name','incubatee')->first();
        $incubatee->roles()->attach($incubatee_role->id);

        $marketing = User::create(['name'=>'Aphelele','surname'=>'User','email'=>'aphelele@propellaincubator.co.za'
            ,'contact_number'=>'076677777','email_verified_at'=>$verification_time,'password'=>$password]);

        $marketing_role = Role::where('name','marketing')->first();
        $marketing->roles()->attach($marketing_role->id);

        /*$tenant = User::create(['name'=>'John','surname'=>'Kani','email'=>'kani@gmail.comn'
            ,'contact_number'=>'076677777','email_verified_at'=>$verification_time,'password'=>$password]);

        $tenant_role = Role::where('name','tenant')->first();
        $tenant->roles()->attach($tenant_role->id);*/
    }
}
