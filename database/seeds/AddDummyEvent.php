<?php

use Illuminate\Database\Seeder;
use App\Event;

class AddDummyEvent extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [

            ['title'=>'Rom Event', 'start'=>'2019-02-21', 'end'=>'2019-02-22', 'all_day'=>'1'],

            ['title'=>'Coyala Event', 'start'=>'2019-02-22', 'end'=>'2019-02-23', 'all_day'=>'2'],

            ['title'=>'Lara Event', 'start'=>'2019-02-23', 'end'=>'2019-02-24', 'all_day'=>'3'],

        ];

        foreach ($data as $key => $value) {

            Event::create($value);

        }
    }
}
