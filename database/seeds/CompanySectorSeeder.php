<?php

use Illuminate\Database\Seeder;
use App\CompanySector;

class CompanySectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $sector_1 = CompanySector::create(["sector_name"=>"Agriculture"]);
        $sector_2 = CompanySector::create(["sector_name"=>"Arts"]);
        $sector_3 = CompanySector::create(["sector_name"=>"Automotive"]);
        $sector_4 = CompanySector::create(["sector_name"=>"Accounting"]);
        $sector_5 = CompanySector::create(["sector_name"=>"Consulting"]);
        $sector_6 = CompanySector::create(["sector_name"=>"IT"]);
        $sector_7 = CompanySector::create(["sector_name"=>"Health"]);
        $sector_8 = CompanySector::create(["sector_name"=>"Engineering"]);
        $sector_9 = CompanySector::create(["sector_name"=>"Telecommunication"]);
        $sector_10 = CompanySector::create(["sector_name"=>"Manufacturing"]);
        $sector_11= CompanySector::create(["sector_name"=>"Real Estate"]);
        $sector_12 = CompanySector::create(["sector_name"=>"Hospitality"]);
        $sector_13 = CompanySector::create(["sector_name"=>"FMCG"]);
        $sector_14 = CompanySector::create(["sector_name"=>"Chemical"]);
        $sector_15 = CompanySector::create(["sector_name"=>"Investment"]);
        $sector_16 = CompanySector::create(["sector_name"=>"Business"]);
        $sector_17 = CompanySector::create(["sector_name"=>"Retail"]);
        $sector_18 = CompanySector::create(["sector_name"=>"Logistics"]);
        $sector_19 = CompanySector::create(["sector_name"=>"Sales"]);
        $sector_20 = CompanySector::create(["sector_name"=>"Services"]);
        $sector_21 = CompanySector::create(["sector_name"=>"Construction"]);
        $sector_22 = CompanySector::create(["sector_name"=>"Marine"]);
        $sector_23 = CompanySector::create(["sector_name"=>"Government"]);
        $sector_24 = CompanySector::create(["sector_name"=>"Other"]);


    }
}
