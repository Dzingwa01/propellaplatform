<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(UsersSeeder::class);
       $this->call(CategorySeeder::class);
        $this->call(QuestionsCategorySeeder::class);
        // $this->call(QuestionsDatatypeSeeder::class);
        $this->call(CompanySectorSeeder::class);
        $this->call(AddVenueName::class);
        $this->call(ContactTypeSeeder::class);
        $this->call(ResourceCategorySeeder::class);

    }
}
