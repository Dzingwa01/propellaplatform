<?php

use Illuminate\Database\Seeder;
use App\QuestionsCategory;

class QuestionsCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category1 = QuestionsCategory::create(["category_name"=>"ICT Questions"]);
        $category2 = QuestionsCategory::create(["category_name"=>"Industrial Questions"]);
        $category3 = QuestionsCategory::create(["category_name"=>"TIA Fund Questions"]);
    }
}
