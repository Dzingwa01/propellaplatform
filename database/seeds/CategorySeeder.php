<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        //
        $category_1 = Category::create(["category_name"=>"Media"]);
        $category_2 = Category::create(["category_name"=>"Propella Board Member"]);
        $category_3 = Category::create(["category_name"=>"Supporters"]);
        $category_4 = Category::create(["category_name"=>"Team"]);
        $category_5 = Category::create(["category_name"=>"Industrial Incubatee"]);
        $category_6 = Category::create(["category_name"=>"ICT Incubatee"]);
        $category_7 = Category::create(["category_name"=>"Supplier"]);
        $category_8 = Category::create(["category_name"=>"Applications"]);
        $category_9 = Category::create(["category_name"=>"Visitors"]);
        $category_10 = Category::create(["category_name"=>"Potential Funders"]);
        $category_11 = Category::create(["category_name"=>"Campaign 1"]);
        $category_12 = Category::create(["category_name"=>"RIF"]);
        $category_13 = Category::create(["category_name"=>"Government"]);
        $category_14 = Category::create(["category_name"=>"International"]);
        $category_15 = Category::create(["category_name"=>"Ecosystem"]);
        $category_16 = Category::create(["category_name"=>"Stakeholder"]);
        $category_17 = Category::create(["category_name"=>"Highlight Reel"]);
    }
}
