<?php

use Illuminate\Database\Seeder;
use App\EventVenue;
class AddVenueName extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $venue_name1= EventVenue::create(["venue_name"=>"Propella Mezz Level"]);
        $venue_name2 = EventVenue::create(["venue_name"=>"4IR Meeting Room (ground floor)"]);
        $venue_name3 = EventVenue::create(["venue_name"=>"IoT Meeting room (2)"]);
        $venue_name4 = EventVenue::create(["venue_name"=>"Propella Downstairs Training Venue"]);
        $venue_name5 = EventVenue::create(["venue_name"=>"Propella Board Room"]);
        $venue_name6 = EventVenue::create(["venue_name"=>"AI Meeting Room"]);

    }
}
