<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedBootcamperPanelistQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_bootcamper_panelist_question_answers', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('question_id')->nullable();
            $table->uuid('declined_bootcamper_panelist_id')->nullable();
            $table->float('score')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_bootcamper_panelist_question_answers');
    }
}
