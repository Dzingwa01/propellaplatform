<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncubateeEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incubatee_employees', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('employee_name')->nullable();
            $table->string('position')->nullable();
            $table->DateTime('start_date')->nullable();
            $table->DateTime('end_date')->nullable();
            $table->string('contract_url')->nullable();
            $table->string('incubatee_id');
            $table->timestamps();

            $table->foreign('incubatee_id')->references('id')->on('incubatees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incubatee_employees');
    }
}
