<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncubateePanelistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incubatee_panelists', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('incubatee_id');
            $table->uuid('panelist_id');
            $table->date('panel_selection_date')->nullable();
            $table->time('panel_selection_time')->nullable();
            $table->uuid('question_category_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('incubatee_id')->references('id')->on('incubatees')->onDelete('cascade');
            $table->foreign('panelist_id')->references('id')->on('panelists')->onDelete('cascade');
            $table->foreign('question_category_id')->references('id')->on('questions_categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incubatee_panelists');
    }
}
