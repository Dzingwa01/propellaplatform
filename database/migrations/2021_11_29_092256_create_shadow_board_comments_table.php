<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShadowBoardCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shadow_board_comments', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->longText('comment_section')->nullable();
            $table->uuid('mentor_id')->nullable();
            $table->uuid('venture_id')->nullable();
            $table->uuid('mentor_venture_id')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shadow_board_comments');
    }
}
