<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFebToDecSpreadsheetOnVentureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venture', function (Blueprint $table) {
            $table->boolean('feb2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('feb2022_date_submitted')->nullable();
            $table->boolean('march2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('march2022_date_submitted')->nullable();
            $table->boolean('april2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('april2022_date_submitted')->nullable();
            $table->boolean('may2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('may2022_date_submitted')->nullable();
            $table->boolean('june2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('june2022_date_submitted')->nullable();
            $table->boolean('july2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('july2022_date_submitted')->nullable();
            $table->boolean('august2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('august2022_date_submitted')->nullable();
            $table->boolean('sept2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('sept2022_date_submitted')->nullable();
            $table->boolean('oct2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('oct2022_date_submitted')->nullable();
            $table->boolean('nov2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('nov2022_date_submitted')->nullable();
            $table->boolean('dec2022_spreadsheet_uploaded')->default(false);
            $table->dateTime('dec2022_date_submitted')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venture', function (Blueprint $table) {
            //
        });
    }
}
