<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureManagementAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_management_accounts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('venture_upload_id');
            $table->string('management_account_url')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('venture_upload_id')->references('id')
                ->on('venture_uploads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_management_accounts');
    }
}
