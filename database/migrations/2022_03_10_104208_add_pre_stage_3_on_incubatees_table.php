<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreStage3OnIncubateesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            $table->boolean('pre_stage_three_complete')->default(false);
            $table->boolean('post_stage_three_complete')->default(false);
            $table->boolean('pre_stage_four_complete')->default(false);
            $table->boolean('post_stage_four_complete')->default(false);
            $table->boolean('pre_stage_five_complete')->default(false);
            $table->boolean('post_stage_five_complete')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            //
        });
    }
}
