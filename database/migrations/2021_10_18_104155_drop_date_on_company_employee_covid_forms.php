<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropDateOnCompanyEmployeeCovidForms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_employee_covid_forms', function (Blueprint $table) {
            $table->dropColumn('date_of_vaccine');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_employee_covid_forms', function (Blueprint $table) {
            //
        });
    }
}
