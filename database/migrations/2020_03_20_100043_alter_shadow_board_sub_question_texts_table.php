<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterShadowBoardSubQuestionTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shadow_board_sub_question_texts', function (Blueprint $table) {
            $table->uuid('shadow_board_question_id')->nullable();
            $table->foreign('shadow_board_question_id')->references('id')->on('shadow_board_questions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
