<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionsModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('question_text');
            $table->string('percentage');
            $table->string('question_category_id');
            $table->string('question_type_id');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('question_category_id')->references('id')->on('questions_categories');
            $table->foreign('question_type_id')->references('id')->on('question_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
