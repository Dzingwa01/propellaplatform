<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureContactLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_contact_logs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('venture_id');
            $table->longText('comment')->nullable();
            $table->date('date')->nullable();
            $table->string('venture_contact_results')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_contact_logs');
    }
}
