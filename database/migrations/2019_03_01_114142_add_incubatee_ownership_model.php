<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncubateeOwnershipModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('incubatee_ownerships', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('registration_number')->nullable();
            $table->string('BBBEE_level')->nullable();
            $table->string('ownership')->nullable();
            $table->string('incubatee_id')->nullable();

            $table->engine = 'InnoDB';
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('incubatee_id')->references('id')->on('incubatees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
