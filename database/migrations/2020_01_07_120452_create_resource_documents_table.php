<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_documents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('fundingResource_id');
            $table->string('documents')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('fundingResource_id')->references('id')->on('funding_resources')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_documents');
    }
}
