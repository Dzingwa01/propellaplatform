<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorCovidSymptomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitor_covid_symptoms', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('visitor_id')->nullable();
            $table->string('covid_symptoms_one')->nullable();
            $table->string('covid_symptoms_two')->nullable();
            $table->string('health_declaration')->nullable();
            $table->string('declaration')->nullable();
            $table->string('digitally_signed_declaration')->nullable();
            $table->dateTime('date_visited')->nullable();
            $table->double('temperature')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitor_covid_symptoms');
    }
}
