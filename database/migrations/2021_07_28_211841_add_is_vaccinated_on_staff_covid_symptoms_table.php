<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsVaccinatedOnStaffCovidSymptomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staff_covid_symptoms', function (Blueprint $table) {
            $table->string('is_vaccinated')->nullable();
            $table->DateTime('date_of_vaccine')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_covid_symptoms', function (Blueprint $table) {
            //
        });
    }
}
