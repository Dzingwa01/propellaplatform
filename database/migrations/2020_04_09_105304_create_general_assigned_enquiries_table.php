<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralAssignedEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_assigned_enquiries', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('user_id')->nullable();
            $table->uuid('general_enquiry_id')->nullable();
            $table->string('initial_contact_person')->nullable();
            $table->string('initial_contact_method')->nullable();
            $table->text('initial_report')->nullable();
            $table->string('enquiry_next_step')->nullable();
            $table->string('enquiry_assigned_person')->nullable();
            $table->date('date_assigned')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_assigned_enquiries');
    }
}
