<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryCompanyEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_company_employee', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->uuid('company_employee_id');
            $table->uuid('category_id');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['category_id','company_employee_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_company_employee');
    }
}
