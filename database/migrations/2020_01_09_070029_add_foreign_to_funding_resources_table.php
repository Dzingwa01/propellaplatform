<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToFundingResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('funding_resources', function (Blueprint $table) {
            $table->uuid('resource_category_id')->nullable();
            $table->foreign('resource_category_id')->references('id')->on('resource_categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('funding_resources', function (Blueprint $table) {
            //
        });
    }
}
