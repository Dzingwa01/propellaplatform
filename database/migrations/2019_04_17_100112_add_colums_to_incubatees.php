<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsToIncubatees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            $table->string('contact_number')->nullable();
            $table->string('physical_address_1')->nullable();
            $table->string('physical_address_2')->nullable();
            $table->string('physical_address_3')->nullable();
            $table->string('physical_city')->nullable();
            $table->string('physical_code')->nullable();
            $table->string('postal_address_1')->nullable();
            $table->string('postal_address_2')->nullable();
            $table->string('postal_address_3')->nullable();
            $table->string('postal_city')->nullable();
            $table->string('postal_code')->nullable();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            //
        });
    }
}
