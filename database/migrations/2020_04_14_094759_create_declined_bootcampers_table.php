<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedBootcampersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_bootcampers', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('venture_category_id')->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('email')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('contact_number_two')->nullable();
            $table->string('id_number')->nullable();
            $table->text('address_one')->nullable();
            $table->text('address_two')->nullable();
            $table->text('address_three')->nullable();
            $table->string('title')->nullable();
            $table->string('initials')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('city')->nullable();
            $table->string('code')->nullable();
            $table->string('race')->nullable();
            $table->boolean('referred')->nullable();
            $table->boolean('declined')->nullable();
            $table->string('venture_name')->nullable();
            $table->string('chosen_category')->nullable();
            $table->string('contacted_via')->nullable();
            $table->string('result_of_contact')->nullable();
            $table->text('declined_reason')->nullable();
            $table->string('referred_company')->nullable();
            $table->string('pitch_video_link')->nullable();
            $table->dateTime('pitch_video_date_time')->nullable();
            $table->string('pitch_video_link_two')->nullable();
            $table->float('total_panel_score')->nullable();
            $table->float('average_panel_score')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_bootcampers');
    }
}
