<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyReferredApplicantQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('referred_applicant_question_answers', function (Blueprint $table) {
            $table->uuid('p_r_a_id');
            $table->foreign('p_r_a_id')->references('id')->on('propella_referred_applicants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referred_applicant_question_answers', function (Blueprint $table) {
            //
        });
    }
}
