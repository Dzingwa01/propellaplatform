<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RinpApplicantPanelistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rinp_applicant_panelists', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('rinp_applicant_id')->nullable();
            $table->uuid('panelist_id')->nullable();
            $table->date('panel_selection_date')->nullable();
            $table->time('panel_selection_time')->nullable();
            $table->uuid('question_category_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rinp_applicant_panelists');
    }
}
