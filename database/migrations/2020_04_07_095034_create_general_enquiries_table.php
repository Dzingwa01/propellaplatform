<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_enquiries', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->uuid('id');
            $table->uuid('general_enquiry_category_id')->nullable();
            $table->string('title')->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('company')->nullable();
            $table->string('email')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('city')->nullable();
            $table->string('heard_about_us')->nullable();
            $table->text('enquiry_message')->nullable();
            $table->date('date_enquired')->nullable();
            $table->string('initial_contact_person')->nullable();
            $table->string('initial_contact_method')->nullable();
            $table->text('initial_report')->nullable();
            $table->text('enquiry_next_step')->nullable();
            $table->string('enquiry_assigned_person')->nullable();
            $table->date('date_assigned')->nullable();
            $table->timestamps();
            $table->softDeletes();

           // $table->foreign('general_enquiry_category_id')->references('id')->on('general_enquiry_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_enquiries');
    }
}
