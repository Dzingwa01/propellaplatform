<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificationOnEventVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event_visitors', function (Blueprint $table) {
            $table->text('de_register_reason')->change();
            $table->string('found_out_via')->nullable();
            $table->dateTime('date_time_registered')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_visitors', function (Blueprint $table) {
            //
        });
    }
}
