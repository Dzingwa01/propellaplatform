<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifVentureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venture', function (Blueprint $table) {
            $table->uuid('mentor_venture_id')->nullable();
            $table->foreign('mentor_venture_id')->references('id')->on('mentor_venture_shadowboards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venture', function (Blueprint $table) {
            //
        });
    }
}
