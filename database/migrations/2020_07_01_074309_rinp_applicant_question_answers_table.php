<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RinpApplicantQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rinp_applicant_question_answers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('rinp_applicant_id')->nullable();
            $table->text('question_text')->nullable();
            $table->text('answer_text')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rinp_applicant_question_answers');
    }
}
