<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedApplicantContactLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_applicant_contact_logs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('declined_applicant_id');
            $table->longText('comment')->nullable();
            $table->date('date')->nullable();
            $table->string('applicant_contact_results')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_applicant_contact_logs');
    }
}
