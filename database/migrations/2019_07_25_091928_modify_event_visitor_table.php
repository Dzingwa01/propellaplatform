<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEventVisitorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event_visitors', function (Blueprint $table) {
            $table->dropColumn('de_registered');
            $table->dropColumn('de_register_reason');
            $table->dropColumn('date_time_de_register');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('event_visitors', function (Blueprint $table) {
            //
        });
    }
}
