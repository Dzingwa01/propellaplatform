<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_applicants', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('surname');
            $table->string('email');
            $table->string('contact_number');
            $table->string('contact_number_two');
            $table->string('id_number');
            $table->string('address_one');
            $table->string('address_two');
            $table->string('address_three');
            $table->string('title');
            $table->string('initials');
            $table->string('dob');
            $table->string('age');
            $table->string('gender');
            $table->string('city');
            $table->string('code');
            $table->boolean('declined');
            $table->boolean('referred');
            $table->string('chosen_category');
            $table->string('contacted_via');
            $table->string('result_of_contact');
            $table->text('declined_reason');
            $table->string('venture_name');
            $table->string('referred_company');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_applicants');
    }
}
