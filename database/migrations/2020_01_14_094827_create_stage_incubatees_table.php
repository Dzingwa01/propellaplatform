<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageIncubateesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stage_incubatees', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->uuid('incubatee_id');
            $table->uuid('incubatee_stage_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['incubatee_id','incubatee_stage_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stage_incubatees');
    }
}
