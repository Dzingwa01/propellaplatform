<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SecondaryInterests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secondary_interests', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->uuid('company_id');
            $table->uuid('category_id');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['category_id','company_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secondary_interests');
    }
}
