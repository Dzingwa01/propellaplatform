<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->uuid('id')->primary();
            $table->string('elevator_pitch')->nullable();
            $table->string('hub')->nullable();
            $table->string('smart_city_tags')->nullable();
            $table->string('business_type')->nullable();
            $table->string('company_name')->nullable();
            $table->string('stage')->nullable();
            $table->string('cohort')->nullable();
            $table->string('venture_profile_information')->nullable();
            $table->string('business_linkedin_url')->nullable();
            $table->string('business_facebook_url')->nullable();
            $table->string('business_twitter_url')->nullable();
            $table->string('business_instagram_url')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('physical_address_one')->nullable();
            $table->string('physical_address_two')->nullable();
            $table->string('physical_address_three')->nullable();
            $table->string('physical_city')->nullable();
            $table->string('physical_postal_code')->nullable();
            $table->string('postal_address_one')->nullable();
            $table->string('postal_address_two')->nullable();
            $table->string('postal_address_three')->nullable();
            $table->string('postal_city')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('virtual_or_physical')->nullable();
            $table->string('pricinct_telephone_code')->nullable();
            $table->string('pricinct_keys')->nullable();
            $table->string('pricinct_office_position')->nullable();
            $table->string('pricinct_printing_code')->nullable();
            $table->string('pricinct_warehouse_position')->nullable();
            $table->string('rent_turnover')->nullable();
            $table->DateTime('rent_date')->nullable();
            $table->string('rent_amount')->nullable();
            $table->string('status')->nullable();
            $table->DateTime('alumni_date')->nullable();
            $table->DateTime('exit_date')->nullable();
            $table->DateTime('resigned_date')->nullable();
            $table->string('mentor')->nullable();
            $table->string('advisor')->nullable();
            $table->string('venture_funding')->nullable();
            $table->DateTime('date_awarded')->nullable();
            $table->string('fund_value')->nullable();
            $table->DateTime('date_closedout')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture');
    }
}
