<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RinpApplicantPanelistQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rinp_applicant_panelist_question_answers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('rinp_applicant_panelist_id')->nullable();
            $table->uuid('question_id')->nullable();
            $table->integer('score')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rinp_applicant_panelist_question_answers');
    }
}
