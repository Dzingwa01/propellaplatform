<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherPitchVideosOnVentureUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venture_uploads', function (Blueprint $table) {
            $table->string('stage_three_to_four_video')->nullable();
            $table->string('stage_four_to_five_video')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venture_uploads', function (Blueprint $table) {
            //
        });
    }
}
