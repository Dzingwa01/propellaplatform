<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropellaReferredApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propella_referred_applicants', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('referred_applicant_category_id')->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('email')->nullable();
            $table->string('contact_number');
            $table->string('contact_number_two')->nullable();
            $table->string('id_number')->nullable();
            $table->string('address_one')->nullable();
            $table->string('address_two')->nullable();
            $table->string('address_three')->nullable();
            $table->string('title')->nullable();
            $table->string('initials')->nullable();
            $table->string('dob')->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('city')->nullable();
            $table->string('code')->nullable();
            $table->boolean('declined')->nullable();
            $table->boolean('referred')->nullable();
            $table->string('chosen_category')->nullable();
            $table->string('contacted_via')->nullable();
            $table->string('result_of_contact')->nullable();
            $table->string('venture_name')->nullable();
            $table->string('referred_company')->nullable();
            $table->string('referred_reason')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propella_referred_applicants');
    }
}
