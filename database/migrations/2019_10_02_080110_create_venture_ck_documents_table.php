<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureCkDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_ck_documents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('venture_upload_id');
            $table->string('ck_documents_url')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('venture_upload_id')->references('id')
                ->on('venture_uploads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_ck_documents');
    }
}
