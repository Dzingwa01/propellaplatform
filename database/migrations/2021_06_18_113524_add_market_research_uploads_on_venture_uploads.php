<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarketResearchUploadsOnVentureUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venture_uploads', function (Blueprint $table) {
            $table->string('market_research_upload')->nullable();
            $table->string('project_plan_upload')->nullable();
            $table->string('business_plan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venture_uploads', function (Blueprint $table) {
            //
        });
    }
}
