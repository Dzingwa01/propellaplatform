<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStage3WorkshopCompleteOnIncubatees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            $table->boolean('stage3_workshop_complete')->default(false);
            $table->boolean('stage4_workshop_complete')->default(false);
            $table->boolean('stage5_workshop_complete')->default(false);
            $table->boolean('stage6_workshop_complete')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            //
        });
    }
}
