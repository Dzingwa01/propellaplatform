<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenturePanelInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_panel_interviews', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('venture_id')->nullable();
            $table->uuid('venture_panel_interview_category_id')->nullable();
            $table->uuid('panelist_id')->nullable();
            $table->uuid('question_category_id')->nullable();
            $table->float('total_panel_score')->nullable();
            $table->float('average_panel_score')->nullable();
            $table->date('panel_selection_date')->nullable();
            $table->time('panel_selection_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_panel_interviews');
    }
}
