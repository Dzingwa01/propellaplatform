<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyIncubateeModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('incubatees', function(Blueprint $table){
           $table->dropColumn('linked_in',
               'facebook',
               'twitter',
               'ownership',
               'logo_url',
               'profile_picture_url',
               'registration_number',
               'product_platform');

            $table->string('personal_linkedIn_url')->nullable();
            $table->string('personal_facebook_url')->nullable();
            $table->string('personal_instagram_url')->nullable();
            $table->string('personal_twitter_url')->nullable();

            $table->string('business_linkedIn_url')->nullable();
            $table->string('business_facebook_url')->nullable();
            $table->string('business_instagram_url')->nullable();
            $table->string('business_twitter_url')->nullable();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
