<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadsResultsOnIncubateeUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incubatee_uploads', function (Blueprint $table) {
            $table->longText('disk_results')->nullable();
            $table->longText('talent_results')->nullable();
            $table->longText('strength_results')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incubatee_uploads', function (Blueprint $table) {
            //
        });
    }
}
