<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedBootcamperPanelistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_bootcamper_panelists', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('declined_bootcamper_id')->nullable();
            $table->uuid('panelist_id')->nullable();
            $table->uuid('question_category_id')->nullable();
            $table->date('panel_selection_date')->nullable();
            $table->time('panel_selection_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_bootcamper_panelists');
    }
}
