<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnOnReferredApplicantQuestionAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('referred_applicant_question_answers', function (Blueprint $table) {
//            $table->dropForeign('referred_applicant_question_answers_r_a_id_foreign');
//            $table->dropColumn('r_a_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referred_applicant_question_answers', function (Blueprint $table) {

        });
    }
}
