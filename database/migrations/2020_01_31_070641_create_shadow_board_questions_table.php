<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShadowBoardQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shadow_board_questions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('question_category_id')->nullable();
            $table->integer('question_number');
            $table->text('question_text');
            $table->string('question_type');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('question_category_id')->references('id')->on('shadow_board_question_categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shadow_board_questions');
    }
}
