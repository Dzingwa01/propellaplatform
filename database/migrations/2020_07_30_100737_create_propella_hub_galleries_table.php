<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropellaHubGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propella_hub_galleries', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('single_image')->nullable();
            $table->string('image_url')->nullable();
            $table->string('description');
            $table->dateTime('date_added')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propella_hub_galleries');
    }
}
