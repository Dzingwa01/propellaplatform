<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShadowBoardQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shadow_board_question_answers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('shadow_board_question_id');
            $table->text('answer_text')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('shadow_board_question_id')->references('id')->on('shadow_board_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shadow_board_question_answers');
    }
}
