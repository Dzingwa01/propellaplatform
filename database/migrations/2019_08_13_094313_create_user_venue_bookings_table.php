<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVenueBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_venue_bookings', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->date('venue_date')->nullable();
            $table->string('venue_start_time')->nullable();
            $table->string('venue_end_time')->nullable();
            $table->string('booking_reason')->nullable();
            $table->string('booking_person')->nullable();
            $table->string('user_id')->nullable();
            $table->string('propella_venue_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_venue_bookings');
    }
}
