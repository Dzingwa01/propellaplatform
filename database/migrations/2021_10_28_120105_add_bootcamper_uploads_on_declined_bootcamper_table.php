<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBootcamperUploadsOnDeclinedBootcamperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('declined_bootcampers', function (Blueprint $table) {
            $table->string('id_document_url')->nullable();
            $table->string('proof_of_address')->nullable();
            $table->string('cipc')->nullable();
            $table->string('bootcamper_contract_upload')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('declined_bootcampers', function (Blueprint $table) {
            //
        });
    }
}
