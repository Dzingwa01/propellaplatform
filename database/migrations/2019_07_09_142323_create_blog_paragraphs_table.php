<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogParagraphsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_paragraphs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('paragraph_number')->nullable();
            $table->string('paragraph_title')->nullable();
            $table->string('paragraph_text')->nullable();
            $table->string('blog_section_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_paragraphs');
    }
}
