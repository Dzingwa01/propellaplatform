<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureSpreadsheetUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_spreadsheet_uploads', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('venture_upload_id')->nullable();
            $table->string('spreadsheet_upload')->nullable();
            $table->boolean('spreadsheet_uploaded')->default(false);
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->dateTime('date_submitted')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_spreadsheet_uploads');
    }
}
