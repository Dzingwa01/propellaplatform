<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadsFieldsOnBootcampersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bootcampers', function (Blueprint $table) {
            $table->boolean('cipc_uploaded')->default(false);
            $table->boolean('proof_of_address_uploaded')->default(false);
            $table->boolean('id_copy_uploaded')->default(false);
            $table->boolean('evaluation_form_completed')->default(false);
            $table->boolean('pitch_video_uploaded')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bootcampers', function (Blueprint $table) {
            //
        });
    }
}
