<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMentorVentureIdToShadowBoardQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shadow_board_question_answers', function (Blueprint $table) {
            $table->uuid('mentor_venture_id')->nullable();
            $table->foreign('mentor_venture_id')->references('id')->on('mentor_venture_shadowboards');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shadow_board_question_answers', function (Blueprint $table) {
            //
        });
    }
}
