<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafetyFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safety_forms', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('fist_and_last_name')->nullable();
            $table->string('id_number')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('street_address')->nullable();
            $table->string('employer')->nullable();
            $table->string('age')->nullable();
            $table->string('temperature')->nullable();
            $table->string('medical_history')->nullable();
            $table->string('travel')->nullable();
            $table->string('recent_care')->nullable();
            $table->string('signs_symptoms')->nullable();
            $table->string('signature')->nullable();
            $table->string('date')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safety_forms');
    }
}
