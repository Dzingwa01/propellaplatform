<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffCovidSymptomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_covid_symptoms', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id')->nullable();
            $table->string('staff_covid_symptoms_one')->nullable();
            $table->string('staff_covid_symptoms_two')->nullable();
            $table->string('staff_health_declaration')->nullable();
            $table->string('staff_declaration')->nullable();
            $table->string('staff_digitally_signed_declaration')->nullable();
            $table->dateTime('date')->nullable();
            $table->double('staff_temperature')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_covid_symptoms');
    }
}
