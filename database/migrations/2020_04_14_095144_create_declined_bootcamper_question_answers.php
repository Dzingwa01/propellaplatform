<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedBootcamperQuestionAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_bootcamper_question_answers', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('question_id')->nullable();
            $table->uuid('d_b_id')->nullable();
            $table->text('answer_text')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_bootcamper_question_answers');
    }
}
