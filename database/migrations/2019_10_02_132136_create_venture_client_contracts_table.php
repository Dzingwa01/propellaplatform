<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureClientContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_client_contracts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('venture_upload_id');
            $table->string('client_contract_url')->nullable();
            $table->date('date_of_signature')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('venture_upload_id')->references('id')
                ->on('venture_uploads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_client_contracts');
    }
}
