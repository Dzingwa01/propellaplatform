<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyShadowBoardSubQuestionTexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shadow_board_sub_question_texts', function (Blueprint $table) {
            $table->renameColumn('question_text', 'question_sub_text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shadow_board_sub_question_texts', function (Blueprint $table) {
            //
        });
    }
}
