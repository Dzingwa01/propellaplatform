<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_gallery', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('image_url')->nullable();
            $table->string('video_url')->nullable();
            $table->string('venture_id');
            $table->timestamps();

            $table->foreign('venture_id')->references('id')->on('venture');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_gallery');
    }
}
