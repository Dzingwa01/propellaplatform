<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_uploads', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->uuid('id')->primary();
            $table->string('company_logo_url')->nullable();
            $table->string('profile_picture_url')->nullable();
            $table->string('video_shot_url')->nullable();
            $table->string('product_shot_url')->nullable();
            $table->string('infographic_url')->nullable();
            $table->string('crowdfunding_url')->nullable();
            $table->string('client_contract')->nullable();
            $table->DateTime('date_of_signature')->nullable();
            $table->string('tax_clearance_url')->nullable();
            $table->DateTime('date_of_clearance_tax')->nullable();
            $table->string('bbbee_certificate_url')->nullable();
            $table->DateTime('date_of_bbbee')->nullable();
            $table->string('ck_documents_url')->nullable();
            $table->string('vat_registration_number_url')->nullable();
            $table->string('cipc_document_url')->nullable();
            $table->string('management_account_url')->nullable();
            $table->string('dormant_affidavit_url')->nullable();
            $table->string('video_links_id')->nullable();
            $table->string('testimonials_id')->nullable();
            $table->string('gallery_id')->nullable();
            $table->string('venture_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('venture_id')->references('id')->on('venture');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_uploads');
    }
}
