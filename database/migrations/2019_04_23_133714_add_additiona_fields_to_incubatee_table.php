<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionaFieldsToIncubateeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            $table->string('client_contract')->nullable();
            $table->DateTime('date_of_signature')->nullable();
            $table->string('id_document')->nullable();
            $table->string('tax_clearance')->nullable();
            $table->string('bbbee_certificate')->nullable();
            $table->DateTime('date_of_clearance')->nullable();
            $table->string('ck_document')->nullable();
            $table->string('vat_reg')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            //
        });
    }
}
