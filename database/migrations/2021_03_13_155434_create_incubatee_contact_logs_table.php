<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncubateeContactLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incubatee_contact_logs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('incubatee_id');
            $table->longText('comment')->nullable();
            $table->date('date')->nullable();
            $table->string('incubatee_contact_results')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incubatee_contact_logs');
    }
}
