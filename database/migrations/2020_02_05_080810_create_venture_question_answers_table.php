<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_question_answers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('venture_id');
            $table->text('answer_text')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('venture_id')->references('id')->on('venture');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_question_answers');
    }
}
