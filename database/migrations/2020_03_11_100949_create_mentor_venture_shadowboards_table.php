<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMentorVentureShadowboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mentor_venture_shadowboards', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('mentor_id');
            $table->uuid('venture_id');
            $table->date('shadow_board_date')->nullable();
            $table->time('shadow_board_time')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('mentor_id')->references('id')->on('mentors')->onDelete('cascade');
            $table->foreign('venture_id')->references('id')->on('venture')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mentor_venture_shadowboards');
    }
}
