<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDeclinedApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('declined_applicants', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('surname')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('contact_number')->nullable()->change();
            $table->string('contact_number_two')->nullable()->change();
            $table->string('id_number')->nullable()->change();
            $table->string('address_one')->nullable()->change();
            $table->string('address_two')->nullable()->change();
            $table->string('address_three')->nullable()->change();
            $table->string('title')->nullable()->change();
            $table->string('initials')->nullable()->change();
            $table->string('dob')->nullable()->change();
            $table->string('age')->nullable()->change();
            $table->string('gender')->nullable()->change();
            $table->string('city')->nullable()->change();
            $table->string('code')->nullable()->change();
            $table->boolean('declined')->nullable()->change();
            $table->boolean('referred')->nullable()->change();
            $table->string('chosen_category')->nullable()->change();
            $table->string('contacted_via')->nullable()->change();
            $table->string('result_of_contact')->nullable()->change();
            $table->text('declined_reason')->nullable()->change();
            $table->string('venture_name')->nullable()->change();
            $table->string('referred_company')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
