<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventIncubateesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_incubatees', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->uuid('id')->primary();
            $table->uuid('event_id');
            $table->uuid('incubatee_id');
            $table->boolean('attended')->nullable();
            $table->boolean('registered')->nullable();
            $table->boolean('de_registered')->nullable();
            $table->string('de_register_reason')->nullable();
            $table->dateTime('date_time_registered')->nullable();
            $table->dateTime('date_time_de_register')->nullable();
            $table->string('found_out_via')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('incubatee_id')->references('id')->on('incubatees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_incubatees');
    }
}
