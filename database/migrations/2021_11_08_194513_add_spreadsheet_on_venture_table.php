<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpreadsheetOnVentureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venture', function (Blueprint $table) {
            $table->boolean('oct_spreadsheet_uploaded')->default(false);
            $table->boolean('nov_spreadsheet_uploaded')->default(false);
            $table->boolean('dec_spreadsheet_uploaded')->default(false);
            $table->boolean('jan_spreadsheet_uploaded')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venture', function (Blueprint $table) {
            //
        });
    }
}
