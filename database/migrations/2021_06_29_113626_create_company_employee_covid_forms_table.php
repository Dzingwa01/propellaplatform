<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyEmployeeCovidFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_employee_covid_forms', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('company_employee_id')->nullable();
            $table->string('employee_covid_symptoms_one')->nullable();
            $table->string('employee_covid_symptoms_two')->nullable();
            $table->string('employee_health_declaration')->nullable();
            $table->string('employee_declaration')->nullable();
            $table->string('employee_digitally_signed_declaration')->nullable();
            $table->dateTime('date')->nullable();
            $table->double('employee_temperature')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_employee_covid_forms');
    }
}
