<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffidavityExpiryDateOnVentureDormantAffidavits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venture_dormant_affidavits', function (Blueprint $table) {
            $table->date('affidavity_expiry_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venture_dormant_affidavits', function (Blueprint $table) {
            //
        });
    }
}
