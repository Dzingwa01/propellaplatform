<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedApplicantQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_applicant_question_answers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('d_a_id');
            $table->uuid('question_id');
            $table->text('answer_text')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('d_a_id')->references('id')->on('declined_applicants');
            $table->foreign('question_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_applicant_question_answers');
    }
}
