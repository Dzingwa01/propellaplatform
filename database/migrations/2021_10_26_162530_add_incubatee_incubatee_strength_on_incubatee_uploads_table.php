<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncubateeIncubateeStrengthOnIncubateeUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incubatee_uploads', function (Blueprint $table) {
            $table->string('strength_upload')->nullable();
            $table->string('talent_dynamics_upload')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incubatee_uploads', function (Blueprint $table) {
            //
        });
    }
}
