<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPanelScoreOnIncubatees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            $table->double('total_panel_score')->nullable();
            $table->double('average_panel_score')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incubatees', function (Blueprint $table) {
            //
        });
    }
}
