<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedBootcamperContactLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_bootcamper_contact_logs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('declined_bootcamper_id');
            $table->longText('comment')->nullable();
            $table->date('date')->nullable();
            $table->string('bootcamper_contact_results')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_bootcamper_contact_logs');
    }
}
