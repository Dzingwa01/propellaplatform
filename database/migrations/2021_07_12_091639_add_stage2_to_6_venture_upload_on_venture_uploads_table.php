<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStage2To6VentureUploadOnVentureUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venture_uploads', function (Blueprint $table) {
            $table->string('stage3_client_contract')->nullable();
            $table->DateTime('stage3_date_of_signature')->nullable();
            $table->string('stage4_client_contract')->nullable();
            $table->DateTime('stage4_date_of_signature')->nullable();
            $table->string('stage5_client_contract')->nullable();
            $table->DateTime('stage5_date_of_signature')->nullable();
            $table->string('stage6_client_contract')->nullable();
            $table->DateTime('stage6_date_of_signature')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venture_uploads', function (Blueprint $table) {
            //
        });
    }
}
