<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('visitors', function(Blueprint $table){
           $table->string('designation')->nullable();
           $table->dropColumn('external_company');
           $table->dropColumn('visit_reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('visitors', function(Blueprint $table){
            //
        });
    }
}
