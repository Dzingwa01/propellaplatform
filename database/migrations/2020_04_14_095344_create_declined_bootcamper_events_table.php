<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclinedBootcamperEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declined_bootcamper_events', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('event_id')->nullable();
            $table->uuid('declined_bootcamper_id')->nullable();
            $table->boolean('accepted')->nullable();
            $table->date('date_registered')->nullable();
            $table->boolean('attended')->nullable();
            $table->boolean('declined')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declined_bootcamper_events');
    }
}
