<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncubateeUploadModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('incubatee_uploads', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('company_logo_url')->nullable();
            $table->string('profile_picture_url')->nullable();
            $table->string('video_shot_url')->nullable();
            $table->string('product_shot_url')->nullable();
            $table->string('infographic_url')->nullable();
            $table->string('crowdfunding_url')->nullable();
            $table->string('video_links_id')->nullable();
            $table->string('testimonials_id')->nullable();
            $table->string('gallery_id')->nullable();
            $table->string('incubatee_id');
            $table->engine = 'InnoDB';
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('incubatee_id')->references('id')->on('incubatees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('incubatee_uploads');
    }
}
