<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentureEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venture_employees', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('employee_name')->nullable();
            $table->string('position')->nullable();
            $table->DateTime('start_date')->nullable();
            $table->DateTime('end_date')->nullable();
            $table->string('contract_url')->nullable();
            $table->string('employee_surname')->nullable();
            $table->string('title')->nullable();
            $table->string('initials')->nullable();
            $table->string('id_number')->nullable();
            $table->string('gender')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('email')->nullable();
            $table->string('status')->nullable();
            $table->date('resigned_date')->nullable();
            $table->string('venture_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venture_employees');
    }
}
