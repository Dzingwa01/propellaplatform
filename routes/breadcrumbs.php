<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Home > About
Breadcrumbs::for('about', function ($trail) {
    $trail->parent('home');
    $trail->push('About', url('/home-about'));
});

//Home > Partners
Breadcrumbs::for('partners', function ($trail) {
    $trail->parent('home');
    $trail->push('Partners', url('/home-partners'));
});

//Home > Innovators
Breadcrumbs::for('innovators', function ($trail) {
    $trail->parent('home');
    $trail->push('Innovators', url('/home-innovators'));
});

//Home > Ventures
Breadcrumbs::for('ventures', function ($trail) {
    $trail->parent('home');
    $trail->push('Ventures', url('/home-categories'));

});

//Admin functions

Breadcrumbs::for('admin', function ($trail) {
    $trail->push('Home', route('home'));
});

//Home > View users
Breadcrumbs::for('index', function ($trail) {
    $trail->parent('admin');
    $trail->push('Users', url('/users-index'));
});

//Home > Users > Edit user
Breadcrumbs::for('edit-user', function ($trail) {
    $trail->parent('index');
    $trail->push('Edit user', url('/users-edit-user'));
});

//Home > Users > Add user
Breadcrumbs::for('add-user', function ($trail) {
    $trail->parent('index');
    $trail->push('Add user', url('/users-create'));
});

//Home > Ventures
Breadcrumbs::for('admin-ventures', function ($trail) {
    $trail->parent('admin');
    $trail->push('Ventures', url('/venture'));
});

//Home > Ventures > Add venture
Breadcrumbs::for('create-ventures', function ($trail) {
    $trail->parent('admin-ventures');
    $trail->push('Add Ventures', url('/venture-create-venture'));
});

//Home > Applicants
Breadcrumbs::for('applicants', function ($trail) {
    $trail->parent('admin');
    $trail->push('Applicants', url('/applicants'));
});

//Home > ICT-Venture
Breadcrumbs::for('ict-venture', function ($trail) {
    $trail->parent('admin');
    $trail->push('ICT-Ventures', url('/venture-ict-venture-index'));
});

//Home > ICT-Venture > Edit
Breadcrumbs::for('edit-venture', function ($trail) {
    $trail->parent('admin');
    $trail->push('Edit-Venture', url('/venture-edit'));
});

//Home > ICT-Venture > Edit > Add Incubatee
Breadcrumbs::for('venture-create-incubatee', function ($trail) {
    $trail->parent('edit-venture');
    $trail->push('Add- Incubatee', url('/venture-venture-create-incubatee'));
});

//Home > Industrial Venture
Breadcrumbs::for('ind-venture', function ($trail) {
    $trail->parent('admin');
    $trail->push('Industrial- Ventures', url('/venture-ind-venture-index'));
});

//Home > Events
Breadcrumbs::for('admin-events', function ($trail) {
    $trail->parent('admin');
    $trail->push('Events', url('/tenant-dashboard-events'));
});

//Home > Venues
Breadcrumbs::for('admin-venues', function ($trail,$user) {
    $trail->parent('admin');
    $trail->push('Venues', url('/show-booking',$user));
});

//Home > Venues> View venue bookings
Breadcrumbs::for('admin-venue-bookings', function ($trail,$users,$venue) {
    $trail->parent('admin-venues',$users);
    $trail->push('Venue Bookings', url('/venue-booking',$venue));
});

//Home > Venues> View venue bookings > Book venue
Breadcrumbs::for('admin-booking-venue', function ($trail,$users,$venue) {
    $trail->parent('admin-venue-bookings',$users,$venue);
    $trail->push('Book Venue ', url('/book-venue'));
});

//Home > Venues> Edit Venue Booking
Breadcrumbs::for('admin-edit-venue-bookings', function ($trail,$users,$venues) {
    $trail->parent('admin-venues',$users);
    $trail->push('Edit Venue Booking', url('/book-venue',$venues));
});

//Home > Shadow board> Create question category
Breadcrumbs::for('ques-cat', function ($trail) {
    $trail->parent('admin');
    $trail->push('Create Question Categories', url('/createShadowQuestionCategory'));
});

//Home > Shadow board> category index
Breadcrumbs::for('shadow-index', function ($trail) {
    $trail->parent('admin');
    $trail->push('Question Categories', url('/getShadowIndex'));
});

//Home > Shadow board> questions
Breadcrumbs::for('quest-index', function ($trail) {
    $trail->parent('admin');
    $trail->push('Shadow board Questions', url('/getShadowQuestionIndex'));
});


//Home > Shadow board> Create question
Breadcrumbs::for('shadow-quest', function ($trail) {
    $trail->parent('admin');
    $trail->push('Shadow board Question', url('/uploadShadowQuestion'));
});

//Home > Shadow board> Edit question
Breadcrumbs::for('quest-edit', function ($trail) {
    $trail->parent('quest-index');
    $trail->push('Edit Questions', url('/editBoardQuestions'));
});
//Home > Shadow board> Edit question category
Breadcrumbs::for('cat-edit', function ($trail) {
    $trail->parent('shadow-index');
    $trail->push('Edit Categories', url('/editBoardCategory'));
});

//Home > Emailer> Companies
Breadcrumbs::for('camp-email', function ($trail) {
    $trail->parent('admin');
    $trail->push('Company e-Mail', url('/bulk-mailer'));
});

//Home > Emailer> Visitor
Breadcrumbs::for('visi-email', function ($trail) {
    $trail->parent('admin');
    $trail->push('Visitor e-Mail', url('/visitor-bulk-mailer'));
});

//Home > Emailer> All users
Breadcrumbs::for('all-email', function ($trail) {
    $trail->parent('admin');
    $trail->push('All Users e-Mail', url('/incubatee-bulk-mailer-index'));
});

//Home > Emailer> All Ventures
Breadcrumbs::for('venture-email', function ($trail) {
    $trail->parent('admin');
    $trail->push('All Venture e-Mail', url('/emailerVentureIndex'));
});

//Home > Propella Emailer> Propella Users
Breadcrumbs::for('users-email', function ($trail) {
    $trail->parent('admin');
    $trail->push('Propella e-Mail', url('/propella-user-emailer'));
});

//Home > Resources> Venture email
Breadcrumbs::for('venture-emails', function ($trail) {
    $trail->parent('admin');
    $trail->push('Venture e-Mail', url('/send-email'));
});

//Home > Resources> Funding categories
Breadcrumbs::for('fund-cat', function ($trail) {
    $trail->parent('admin');
    $trail->push('Resource Categories', url('/indexCategory'));
});

//Home > Resources> Funding categories> Edit Category
Breadcrumbs::for('edit-fund-cat', function ($trail) {
    $trail->parent('fund-cat');
    $trail->push('Edit Resource Categories', url('/editCategory'));
});

//Home > Resources> Funding categories> Add Category
Breadcrumbs::for('add-fund-cat', function ($trail) {
    $trail->parent('fund-cat');
    $trail->push('Add new Resource Categories', url('/create-category'));
});

//Home > Resources> Grants
Breadcrumbs::for('grant', function ($trail) {
    $trail->parent('admin');
    $trail->push('Grant', url('/grant'));
});

//Home > Resources> Funds
Breadcrumbs::for('funds', function ($trail) {
    $trail->parent('admin');
    $trail->push('Funding', url('/fundings'));
});

//Home > Resources> Competition
Breadcrumbs::for('competition', function ($trail) {
    $trail->parent('admin');
    $trail->push('Competition', url('/competition'));
});

//Home > Resources> Crowd
Breadcrumbs::for('crowd', function ($trail) {
    $trail->parent('admin');
    $trail->push('Crowd funding', url('/crowd'));
});

//Home > Resources> Create Resource
Breadcrumbs::for('create-resource', function ($trail) {
    $trail->parent('admin');
    $trail->push('Create Resource', url('/addResources'));
});


//Home > Resources> Index
Breadcrumbs::for('resource-index', function ($trail) {
    $trail->parent('admin');
    $trail->push('All resources', url('/resourceIndex'));
});

//Home > Resources> Show resource details
Breadcrumbs::for('show-resource', function ($trail) {
    $trail->parent('admin');
    $trail->push('Details', url('/show-all-info'));
});

//Home > Resources> Incubatee Emailer
Breadcrumbs::for('resource-emailer', function ($trail) {
    $trail->parent('admin');
    $trail->push('All Incubatees', url('/AllIncubatees'));
});

//Home > Blog> View blog
Breadcrumbs::for('blog-index', function ($trail) {
    $trail->parent('admin');
    $trail->push('All Blogs', url('/blog-index'));
});

//Home > Learning Curve > View Learning Curves
Breadcrumbs::for('admin-curve', function ($trail) {
    $trail->parent('admin');
    $trail->push('All Learning Curves', url('/learning-curve'));
});

//Home > Venues> View venues
Breadcrumbs::for('admin-venueindex', function ($trail) {
    $trail->parent('admin');
    $trail->push('All venues', url('/get-venues'));
});

//Home > Venues> View venue booking
Breadcrumbs::for('admin-venue-booking', function ($trail) {
    $trail->parent('admin-venueindex');
    $trail->push('Venue Bookings', url('/show-venue-bookings'));
});

//Home > Venues> Edit View venues
Breadcrumbs::for('edit-admin-venue', function ($trail) {
    $trail->parent('admin-venueindex');
    $trail->push('Edit venue', url('/edit-venue'));
});

//Home > Venues> add View venues
Breadcrumbs::for('add-admin-venue', function ($trail) {
    $trail->parent('admin-venueindex');
    $trail->push('Add venue', url('/create-venue'));
});

//Home > Calendar> Events
Breadcrumbs::for('admin-calendar', function ($trail) {
    $trail->parent('admin');
    $trail->push('Calendar', url('/full_calendar/events'));
});

//Home > Calendar> View all Events
Breadcrumbs::for('admin-calendar-events', function ($trail) {
    $trail->parent('admin-calendar');
    $trail->push('Calendar Events', url('/full_calendar/view-events'));
});

//Home > Calendar> View all Events
Breadcrumbs::for('admin-edit-event', function ($trail,$event) {
    $trail->parent('admin-calendar-events');
    $trail->push('Edit Event', url('/event_edit',$event));
});


//Home > Companies Database> Companies
Breadcrumbs::for('admin-companies', function ($trail) {
    $trail->parent('admin');
    $trail->push('All Companies', url('/companies'));
});

//Home > Companies Database> Companies> Create Company
Breadcrumbs::for('create-companies', function ($trail) {
    $trail->parent('admin-companies');
    $trail->push('Create Company', url('/company-create'));
});

//Home > Companies Database> Companies > Edit
Breadcrumbs::for('edit-companies', function ($trail, $company) {
    $trail->parent('admin-companies');
    $trail->push('Edit Company', url('/companies',$company));
});

//Home > Companies Database>  Companies > View Companies
Breadcrumbs::for('view-companies', function ($trail,$company) {
    $trail->parent('admin-companies');
    $trail->push('Company Details', url('/company/show',$company));
});

//Home > Companies Database>  Companies > View Companies > Create Employee
Breadcrumbs::for('view-employees-create', function ($trail,$company) {
    $trail->parent('view-companies',$company);
    $trail->push('Add New Employee', url('/company-employee-create'));
});

//Home > Companies Database> Employees
Breadcrumbs::for('admin-employees', function ($trail) {
    $trail->parent('admin');
    $trail->push('All Employees', url('/company-employees'));
});

//Home > Companies Database>  Employees > Edit Employee
Breadcrumbs::for('edit-employees', function ($trail,$companyEmployee) {
    $trail->parent('admin-employees');
    $trail->push('Edit Employee', url('/company-employee',$companyEmployee));
});

//Home > Companies Database>  Employees > View Employee
Breadcrumbs::for('view-employees', function ($trail,$companyEmployee) {
    $trail->parent('admin-employees');
    $trail->push('Employee Details', url('/company-employee/show',$companyEmployee));
});

//Home > Companies Database>  Employees > View Employee > Add Contact Log
Breadcrumbs::for('add-employees-log', function ($trail,$companyEmployee) {
    $trail->parent('view-employees',$companyEmployee);
    $trail->push('Contact Log', url('/add-activity-log-for-contact'));
});

//Home > Companies Database> Contact Logs
Breadcrumbs::for('admin-logs', function ($trail) {
    $trail->parent('admin');
    $trail->push('Contact Log', url('/contact-logs'));
});

//Home > Visitors
Breadcrumbs::for('admin-visitors', function ($trail) {
    $trail->parent('admin');
    $trail->push('Visitors', url('/visitors/visitors-index'));
});

//Home > Visitors > Edit Visitor
Breadcrumbs::for('admin-visitors-edit', function ($trail,$visitor) {
    $trail->parent('admin-visitors');
    $trail->push('Edit Visitor', url('/visitor-edit',$visitor));
});

//Home > Visitors > Add Visitor
Breadcrumbs::for('admin-add-visitors', function ($trail) {
    $trail->parent('admin-visitors');
    $trail->push('Add New Visitor', url('/visitors/create-visitor'));
});

//Home > Questions
Breadcrumbs::for('admin-questions', function ($trail) {
    $trail->parent('admin');
    $trail->push('Questions', url('/questionIndex'));
});

//Home > Questions > Add Questions
Breadcrumbs::for('admin-add-questions', function ($trail) {
    $trail->parent('admin-questions');
    $trail->push('Add Questions', url('/upload-questions'));
});

//Home > Questions > Edit Questions
Breadcrumbs::for('admin-edit-questions', function ($trail,$Question) {
    $trail->parent('admin-questions');
    $trail->push('Edit Question', url('/editQuestions',$Question));
});

//Home > Question Category
Breadcrumbs::for('admin-question-category', function ($trail) {
    $trail->parent('admin');
    $trail->push('Question Categories', url('/categoryIndex'));
});


//Home > Questions > Add Question Category
Breadcrumbs::for('admin-add-category', function ($trail) {
    $trail->parent('admin-question-category');
    $trail->push('Add Category', url('/questionCategory'));
});

//Home > Questions > Edit Question Category
Breadcrumbs::for('admin-edit-category', function ($trail,$QuestionsCategory) {
    $trail->parent('admin-question-category');
    $trail->push('Edit Question', url('/editQuestions',$QuestionsCategory));
});

//Home > Venture Interview > Interview Category
Breadcrumbs::for('admin-interview-category', function ($trail) {
    $trail->parent('admin');
    $trail->push('Interview Category', url('/venture-panel-interview-category-index'));
});

//Home > Propella Platform > Manage Categories
Breadcrumbs::for('admin-venture-category', function ($trail) {
    $trail->parent('admin');
    $trail->push('Venture Category', url('/venture-category'));
});

//Home > Propella Platform > Create Venture Categories
Breadcrumbs::for('admin-create-vcategory', function ($trail) {
    $trail->parent('admin-venture-category');
    $trail->push('Create Venture Category', url('/create-venture-category'));
});

//Home > Propella Platform > Edit Venture Categories
Breadcrumbs::for('admin-edit-vcategory', function ($trail,$ventureCategory) {
    $trail->parent('admin-venture-category');
    $trail->push('Edit Venture Category', url('/venture-category-edit/',$ventureCategory));
});

//Home > Propella Platform > Manage Stages
Breadcrumbs::for('admin-incubatee-stages', function ($trail) {
    $trail->parent('admin');
    $trail->push('Incubatee Stages', url('/incubatee-stages'));
});

//Home > Propella Platform > Create Incubatee Stages
Breadcrumbs::for('admin-create-stages', function ($trail) {
    $trail->parent('admin-incubatee-stages');
    $trail->push('Create Incubatee Stages', url('/create-incubatee-stage'));
});

//Home > Propella Platform > Edit Incubatee Stages
Breadcrumbs::for('admin-edit-stages', function ($trail) {
    $trail->parent('admin-incubatee-stages');
    $trail->push('Edit Incubatee Stage', url('/incubatee-stage-edit'));
});

//Home > Propella Platform > Manage Bootcampers
Breadcrumbs::for('admin-bootcampers', function ($trail) {
    $trail->parent('admin');
    $trail->push('Bootcampers', url('/bootcampers'));
});

//Home > Propella Platform > Manage Bootcampers > Show Bootcamper Details
Breadcrumbs::for('admin-bootcamper-details', function ($trail) {
    $trail->parent('admin-bootcampers');
    $trail->push('Bootcamper Details', url('/bootcamper-show'));
});

//Home > Propella Platform > Manage Bootcampers > Show Bootcamper Details > Bootcamper Events
Breadcrumbs::for('admin-bootcampr-events', function ($trail,$bootcamper) {
    $trail->parent('admin-bootcampers');
    $trail->push('Bootcamper Events', url('/bootcamper-events/',$bootcamper));
});

//Home > Propella Platform > Manage Bootcampers > Show Bootcamper Details > Bootcamper Events > Edit Event
Breadcrumbs::for('admin-bootcamper-editevent', function ($trail,$eventBootcamper) {
    $trail->parent('admin-bootcamper-events');
    $trail->push('Edit Bootcamper Event', url('/bootcamper-events/',$eventBootcamper));
});

//Home > Propella Platform > Manage Bootcampers > Show Bootcamper Details > Bootcamper to Declined  Declined Applicant
Breadcrumbs::for('admin-bootcamper-to-declined', function ($trail,$bootcamper) {
    $trail->parent('admin-bootcampers');
    $trail->push('Bootcamper to Declined  Declined Applicant', url('/bootcamper-to-declined-applicant-view/',$bootcamper));
});

//Home > Propella Platform > Manage Bootcampers > Show Bootcamper Details > Bootcamper panel access window
Breadcrumbs::for('admin-bootcamper-panel-window', function ($trail,$bootcamper) {
    $trail->parent('admin-bootcampers');
    $trail->push('Bootcamper Panel Access Window', url('/bootcamper-panel-access-window/',$bootcamper));
});

//Home > Propella Platform > Manage Bootcampers > Show Bootcamper Details > View Assigned panelist
Breadcrumbs::for('admin-bootcamper-view-panelist', function ($trail,$bootcamper) {
    $trail->parent('admin-bootcampers');
    $trail->push('Assigned Panelists', url('/show-ict-applicant-panelists/',$bootcamper));
});

//Home > Propella Platform > Manage Bootcampers > Show Bootcamper Details > Bootcamper next step
Breadcrumbs::for('admin-bootcamper-next-step', function ($trail,$bootcamper) {
    $trail->parent('admin-bootcampers');
    $trail->push('Bootcamper Next Step', url('/bootcamper-next-step/',$bootcamper));
});

//Home > Propella Platform > Manage Bootcampers > Show Bootcamper Details > Edit Bootcamper Info
Breadcrumbs::for('admin-edit-bootcamper-info', function ($trail,$bootcamper) {
    $trail->parent('admin-bootcampers');
    $trail->push('Edit Bootcamper Information', url('/bootcamper-edit-basic/',$bootcamper));
});

//Home > Propella Platform > Bootcamper Interview > Bootcampers Panel Interviews
Breadcrumbs::for('admin-bootcamper-panelists', function ($trail) {
    $trail->parent('admin');
    $trail->push('Bootcampers Panel Interviews', url('/bootcamper-panel-interviews'));
});

//Home > Propella Platform > Bootcamper Interview > Declined Bootcampers
Breadcrumbs::for('admin-declined-bootcamper', function ($trail) {
    $trail->parent('admin');
    $trail->push('Declined Bootcampers', url('/declined-bootcampers'));
});

//Home > Propella Platform > Bootcamper Interview > Declined Bootcampers > Account Overview
Breadcrumbs::for('admin-declined-bootcamperAC', function ($trail,$bootcamper) {
    $trail->parent('admin');
    $trail->push('Account Overview', url('/declined-bootcampers',$bootcamper));
});


//Home > Propella Platform > Manage Incubatees
Breadcrumbs::for('admin-incubatees', function ($trail) {
    $trail->parent('admin');
    $trail->push('Incubatees', url('/incubatees'));
});

//Home > Propella Platform > Manage Incubatees > Create Incubatee
Breadcrumbs::for('admin-create-incubatees', function ($trail) {
    $trail->parent('admin-incubatees');
    $trail->push('Create Incubatees', url('/create-incubatees'));
});

//Home > Propella Platform > Manage Incubatees > Account overview
Breadcrumbs::for('admin-incubateesAO', function ($trail) {
    $trail->parent('admin-incubatees');
    $trail->push('Account Overview', url('/incubatee-account-overview'));
});

//Home > Propella Platform > Manage Incubatees > Edit Incubatee
Breadcrumbs::for('admin-edit-incubatees', function ($trail,$incubatee) {
    $trail->parent('admin-incubatees');
    $trail->push('Edit Incubatee Details', url('/incubatee-edit', $incubatee));
});

//Home > Propella Platform > Manage Incubatees > Edit Incubatee > Create Incubatee Venture
Breadcrumbs::for('admin-create-incubatee-venture', function ($trail,$incubatee) {
    $trail->parent('admin-edit-incubatees',$incubatee);
    $trail->push('Incubatee Venture', url('/incubatee-create-venture',$incubatee));
});


//Home > Propella Platform > Manage Incubatees > Incubatee Events
Breadcrumbs::for('admin-incubatee-events', function ($trail,$incubatee) {
    $trail->parent('admin-incubatees');
    $trail->push('Incubatee Events', url('/incubatee-events', $incubatee));
});

//Home > Propella Platform > Manage Incubatees > Incubatee Events Edit
Breadcrumbs::for('admin-incubatee-events-edit', function ($trail,$eventIncubatee) {
    $trail->parent('admin-incubatee-events');
    $trail->push('Incubatee Events', url('/incubatee-registered-event-edit',$eventIncubatee));
});

//Home > Propella Platform > Manage Alumni Ventures
Breadcrumbs::for('admin-alumni-venture', function ($trail) {
    $trail->parent('admin');
    $trail->push('Alumni Ventures', url('/alumni-venture'));
});

//Home > Propella Platform > Manage Employeess
Breadcrumbs::for('admin-prop-employees', function ($trail) {
    $trail->parent('admin');
    $trail->push('Employees', url('/users'));
});

//Home > Propella Platform > Manage Mentors
Breadcrumbs::for('admin-prop-mentors', function ($trail) {
    $trail->parent('admin');
    $trail->push('Mentors', url('/mentors'));
});

//Home > Propella Platform > Manage Mentors > Show Mentor info
Breadcrumbs::for('admin-prop-mentor-show', function ($trail,$user) {
    $trail->parent('admin-prop-mentors');
    $trail->push('Mentor Information', url('/mentor-show',$user));
});


//Home > Propella Platform > Manage Mentors > Show Mentor Info > Mentor Venture
Breadcrumbs::for('admin-prop-mentor-show-venture', function ($trail,$venture) {
    $trail->parent('admin-prop-mentor-show',$venture);
    $trail->push('Mentor Venture Information', url('/show-mentor-venture-content',$venture));
});

//Home > Propella Platform > Propella Ventures
Breadcrumbs::for('admin-prop-ventures', function ($trail) {
    $trail->parent('admin');
    $trail->push('Ventures', url('/propella-ventures'));
});

//Home > Propella Platform > Propella Ventures > Panel Access Window
Breadcrumbs::for('admin-prop-ventures-panel', function ($trail) {
    $trail->parent('admin-prop-ventures');
    $trail->push('Venture Panel Access Window', url('/venture-panel-access-window'));
});

//Home > Propella Platform > Propella Ventures > Panel Interview
Breadcrumbs::for('admin-prop-panel-int', function ($trail) {
    $trail->parent('admin-prop-ventures');
    $trail->push('Venture Panel Interview', url('/show-venture-interviews-overview'));
});

//Home > Exited Ventures >Manage Exited Ventures
Breadcrumbs::for('admin-exit-venture', function ($trail) {
    $trail->parent('admin');
    $trail->push('Exited Ventures', url('/exited-venture'));
});

//Home > Reporting > Applicant Report
Breadcrumbs::for('admin-app-report', function ($trail) {
    $trail->parent('admin');
    $trail->push('Applicant Reports', url('/reports'));
});

//Home > Content Portal > View Questions
Breadcrumbs::for('admin-view-quest', function ($trail) {
    $trail->parent('admin');
    $trail->push('View Questions', url('/view-questions'));
});

//Home > Reporting > Applicant Report > Edit Question
Breadcrumbs::for('admin-edit-questt', function ($trail) {
    $trail->parent('admin-view-quest');
    $trail->push('Edit Question', url('/editQuestions/'));
});

//Home > Enquiries > Manage enquiries
Breadcrumbs::for('admin-enquiries', function ($trail) {
    $trail->parent('admin');
    $trail->push('Manage Enquiries ', url('/enquiry-category-index'));
});

//Home > Enquiries > Manage enquiries > Show
Breadcrumbs::for('admin-show-enquiries', function ($trail) {
    $trail->parent('admin-enquiries');
    $trail->push('Show Enquiries ', url('/show-enquiry-category-enquiries'));
});

//Home > Enquiries > Manage enquiries > Edit
Breadcrumbs::for('admin-edit-enquiry-cat', function ($trail) {
    $trail->parent('admin-enquiries');
    $trail->push('Edit Enquiries Category ', url('/edit-enquiry-category'));
});

//Home > Enquiries > Unassigned enquiries
Breadcrumbs::for('admin-unassigned-enquiries', function ($trail) {
    $trail->parent('admin');
    $trail->push('Unassigned enquiries ', url('/unassigned-enquiry-index'));
});

//Home > Enquiries > Pre-assigned enquiries
Breadcrumbs::for('admin-preassigned-enquiries', function ($trail) {
    $trail->parent('admin');
    $trail->push('Pre-assigned enquiries ', url('/pre-assigned-enquiry-index'));
});

//Home > Enquiries > Pre-assigned enquiries
Breadcrumbs::for('admin-assigned-enquiries', function ($trail) {
    $trail->parent('admin');
    $trail->push('Assigned enquiries ', url('/assigned-enquiry-index'));
});

//Home > Enquiries > Overview
Breadcrumbs::for('admin-enquiry-overview', function ($trail) {
    $trail->parent('admin');
    $trail->push('Enquiry Overview ', url('/show-enquiry'));
});

//Home > Enquiries > Overview
Breadcrumbs::for('admin-enquiry-followup', function ($trail) {
    $trail->parent('admin-unassigned-enquiries');
    $trail->push('Enquiry Follow up ', url('/pre-assign-enquiry'));
});

//ADMINISTRATOR BREADCRUMBS

Breadcrumbs::for('administrator', function ($trail) {
    $trail->push('Home', route('home'));
});

//Home > View Applications
Breadcrumbs::for('applicant-index', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Applicants', url('/applicants'));
});

//Home > View Incubatees
Breadcrumbs::for('incubatee-index', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Ventures', url('/venture'));
});

//Home > Users > Edit Venture
Breadcrumbs::for('edit-incubatee', function ($trail,$venture) {
    $trail->parent('incubatee-index');
    $trail->push('Edit Venture', url('/venture-edit',$venture));
});

//Home > Users  > Edit Venture> Add Venture Incubatee
Breadcrumbs::for('add-venture-incubatee', function ($trail,$venture) {
   $trail->parent('edit-incubatee',$venture);
   $trail->push('Add Incubatee', url('/venture-incubatee'));
});

//Home > Users  > Edit Venture> Edit Incubatee
Breadcrumbs::for('venture-edit-incubatee', function ($trail,$incubatee,$venture) {
    $trail->parent('edit-incubatee',$venture);
    $trail->push('Edit Incubatee', url('/venture-edit-incubatee',$incubatee));
});

//Home > Users > Add Venture
Breadcrumbs::for('create-venture', function ($trail) {
    $trail->parent('incubatee-index');
    $trail->push('Create Venture', url('/create-venture'));
});

//Home > Emailer > Database Users
Breadcrumbs::for('emailer-index', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Propella Contacts', url('/bulk-mailer'));
});

//Home > Emailer > Visitors
Breadcrumbs::for('visitor-emailer-index', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Propella Visitor Contacts', url('/visitor-bulk-mailer'));
});
//Home > Resources > Send email
Breadcrumbs::for('venture-email-index', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Send email to Venture', url('/send-email'));
});

//home > Resources > Funding Categories
Breadcrumbs::for('funding-category', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Funding Categories', url('/indexCategory'));
});

//home > Resources > Funding Categories
Breadcrumbs::for('edit-funding-category', function ($trail) {
    $trail->parent('funding-category');
    $trail->push('Edit Funding Categories', url('/editCategory'));
});

//Home > Resources > Funding Categories
Breadcrumbs::for('create-funding-category', function ($trail) {
    $trail->parent('funding-category');
    $trail->push('Create Funding Categories', url('/editCategory'));
});

//Home > Application Process>  Manage Category > Edit Question Category
Breadcrumbs::for('edit-question-category', function ($trail) {
    $trail->parent('admin-question-category');
    $trail->push('Edit Question Category', url('/edit-question-category'));
});

//Home > Application Process> Manage Category >Create Category
Breadcrumbs::for('create-question-category', function ($trail) {
    $trail->parent('admin-question-category');
    $trail->push('Create Question Category', url('/questionCategory'));
});

//Home > Venues > View Venue
Breadcrumbs::for('venue-index', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Venues', url('/get-venues'));
});

//Home > Venues > View Venue > View Bookings
Breadcrumbs::for('venue-bookings', function ($trail) {
    $trail->parent('venue-index');
    $trail->push('Venue Bookings', url('/show-venue-bookings'));
});

//Home > Venues > View Venue > Edit Venue
Breadcrumbs::for('edit-venue', function ($trail) {
    $trail->parent('venue-index');
    $trail->push('Edit Venue', url('/edit-venue'));
});

//Home > Book Venue
Breadcrumbs::for('book-venue', function ($trail,$user) {
    $trail->parent('administrator');
    $trail->push('Book Venue', url('/show-booking',$user));
});

//Home > Book Venue > Book a Vanue
Breadcrumbs::for('book-a-venue', function ($trail,$users,$venue) {
    $trail->parent('book-venue',$users);
    $trail->push('All Bookings', url('/venue-booking',$venue));
});

//Home > Book Venue > Book a Vanue > Create booking
Breadcrumbs::for('add-venue-booking', function ($trail,$users,$venue) {
    $trail->parent('book-a-venue',$users,$venue);
    $trail->push('Create Booking', url('/book-venue',$venue));
});

//Home > Satellite Hub Gallery > Upload Media
Breadcrumbs::for('pti-gallery', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Upload Images', url('/create-gallery'));
});

//Home > Satellite Hub Gallery > All Media Gallery
Breadcrumbs::for('pti-all-gallery', function ($trail) {
    $trail->parent('administrator');
    $trail->push('All Media Gallery', url('/gallery-index'));
});

//Home > Satellite Hub Gallery > All Media Gallery > Edit Media
Breadcrumbs::for('pti-edit-media', function ($trail) {
    $trail->parent('pti-all-gallery');
    $trail->push('Edit Image', url('/edit-media-gallery'));
});

//Home >Learning Curves > Get Learning curves
Breadcrumbs::for('learning-curves', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Learning Curves', url('/get-learning-curves'));
});

//Home >Venues> View Venues
Breadcrumbs::for('administrator-venues', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Venues', url('/get-venues'));
});

//Home >Venues> View Venues > Edit Venue
Breadcrumbs::for('administrator-edit-venues', function ($trail) {
    $trail->parent('administrator-venues');
    $trail->push('Edit Venue', url('/edit-venue'));
});

//Home >Venues> View Venues > Create Venue
Breadcrumbs::for('administrator-create-venues', function ($trail) {
    $trail->parent('administrator-venues');
    $trail->push('Create Venue', url('/create-venue'));
});

//Home >Calendar > Create Event Venue
Breadcrumbs::for('administrator-event-venue', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Create Event Venue', url('/create-event-venue'));
});

//Home >Calendar > All Event Venue
Breadcrumbs::for('administrator-all-event-venue', function ($trail) {
    $trail->parent('administrator');
    $trail->push('All Event Venue', url('/get-venues-index'));
});

//Home >Calendar > All Event Venue > Edit Event Venue
Breadcrumbs::for('administrator-edit-event-venue', function ($trail) {
    $trail->parent('administrator-all-event-venue');
    $trail->push('Edit Event Venue', url('/edit-event-venue'));
});

//Home >Calendar > All Event Venue > View calendar
Breadcrumbs::for('administrator-calendar', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Event Calendar', url('/full_calendar/events'));
});

//Home >Calendar > All Event Venue > View calendar > Events
Breadcrumbs::for('administrator-view-calendar', function ($trail) {
    $trail->parent('administrator-calendar');
    $trail->push('Calendar Events', url('/full_calendar/view-events'));
});

//Home >Calendar > All Event Venue > View calendar > Events > Edit Event
Breadcrumbs::for('administrator-edit-event-calendar', function ($trail) {
    $trail->parent('administrator-view-calendar');
    $trail->push('Edit Event Details', url('/event-edit'));
});

//Home >Calendar > All Event Venue > View calendar > Events > Event Visitor
Breadcrumbs::for('administrator-event-visitor', function ($trail) {
    $trail->parent('administrator-view-calendar');
    $trail->push('Event Visitors', url('/event-show'));
});

//Home > Calendar > All Events
Breadcrumbs::for('administrator-all-events', function ($trail) {
    $trail->parent('administrator');
    $trail->push('All Event ', url('/events'));
});

//Home >Calendar > Private Event
Breadcrumbs::for('administrator-private-event', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Private Events', url('/private-events'));
});

//Home >Calendar > Private Event > Private Event Visitor
Breadcrumbs::for('administrator-private-event-visitor', function ($trail) {
    $trail->parent('administrator-private-event');
    $trail->push('Private Event Visitors', url('/private-event-show'));
});

//Home >Companies Database> Companies
Breadcrumbs::for('administrator-companies', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Companies', url('/companies'));
});

//Home >Companies Database> Companies > View Company Information
Breadcrumbs::for('administrator-view-company', function ($trail,$company) {
    $trail->parent('administrator-companies');
    $trail->push('Company Information', url('/company/show',$company));
});

//Home >Companies Database> Companies > View Company Information > View Employee Details
Breadcrumbs::for('administrator-view-employee', function ($trail,$company,$companyEmployee) {
    $trail->parent('administrator-view-company',$company);
    $trail->push('Employee Information', url('/company-employee/show',$companyEmployee));
});

//Home >Companies Database> Companies > View Company Information > Edit Employee Details
Breadcrumbs::for('administrator-edit-employee', function ($trail,$company,$companyEmployee) {
    $trail->parent('administrator-view-company',$company);
    $trail->push('Edit Employee Information', url('/company-employee/',$companyEmployee));
});

//Home >Companies Database> Companies > View Company Information > Add Employee Details
Breadcrumbs::for('administrator-add-employee', function ($trail,$company) {
    $trail->parent('administrator-view-company',$company);
    $trail->push('Add Employee Information', url('/company-employee/'));
});

//Home >Companies Database> People
Breadcrumbs::for('administrator-all-employee', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Employees', url('/company-employees'));
});

//Home >Visitor > Manage Visitor
Breadcrumbs::for('administrator-visitor', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Visitors', url('/visitors/visitors-index'));
});

//Home >Visitor > Manage Visitor > Edit Visitor
Breadcrumbs::for('administrator-edit-visitor', function ($trail,$visitor) {
    $trail->parent('administrator-visitor');
    $trail->push('Edit Visitors', url('/visitor-edit',$visitor));
});

//Home >Application Process > Manage Questions
Breadcrumbs::for('administrator-edit-questions', function ($trail) {
    $trail->parent('admin-questions');
    $trail->push('Edit Question', url('/editQuestions'));
});

//Home >Application Process > Upload Questions
Breadcrumbs::for('administrator-upload-questions', function ($trail) {
    $trail->parent('admin-questions');
    $trail->push('Upload Question', url('/upload-question'));
});

//Home >Propella Platform > Manage categories
Breadcrumbs::for('administrator-venture-category', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Venture Categories', url('/venture-category'));
});

//Home >Propella Platform > Manage categories > Edit Category
Breadcrumbs::for('administrator-edit-category', function ($trail) {
    $trail->parent('administrator-venture-category');
    $trail->push('Edit Venture Categories', url('/venture-category-edit'));
});

//Home >Propella Platform > Manage categories > Add Category
Breadcrumbs::for('administrator-add-category', function ($trail) {
    $trail->parent('administrator-venture-category');
    $trail->push('Add Venture Categories', url('/create-venture-category'));
});

//Home >Propella Platform > Manage Stages
Breadcrumbs::for('administrator-incubatee-stage', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Incubatee Stages', url('/incubatee-stages'));
});

//Home >Propella Platform > Manage Stages > Add Stage
Breadcrumbs::for('administrator-add-stage', function ($trail) {
    $trail->parent('administrator-incubatee-stage');
    $trail->push('Add Incubatee Stages', url('/create-incubatee-stage'));
});

//Home >Propella Platform > Manage Alumni
Breadcrumbs::for('administrator-alumni-venture', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Alumni Ventures', url('/alumni-venture'));
});

//Home >Propella Platform > Manage Incubatees
Breadcrumbs::for('administrator-incubatees', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Incubatees', url('/incubatees'));
});

//Home >Propella Platform > Manage Incubatees > Account Overview
Breadcrumbs::for('administrator-incubatee-account', function ($trail) {
    $trail->parent('administrator-incubatees');
    $trail->push('Incubatee Account', url('/incubatee-account-overview'));
});

//Home >Propella Platform > Manage Incubatees > Edit Account
Breadcrumbs::for('administrator-edit-incubatee', function ($trail,$incubatee) {
    $trail->parent('administrator-incubatees');
    $trail->push('Edit Incubatee', url('/incubatee-edit',$incubatee));
});

//Home >Propella Platform > Manage Incubatees > Edit Account > Create Venture
Breadcrumbs::for('administrator-create-venture', function ($trail,$incubatee) {
    $trail->parent('administrator-edit-incubatee',$incubatee);
    $trail->push('Create Venture', url('/incubatee-create-venture'));
});

//Home >Propella Platform > Manage Incubatees > Registered Events
Breadcrumbs::for('administrator-events-incubatee', function ($trail,$incubatee) {
    $trail->parent('administrator-incubatees');
    $trail->push('Incubatee Registered Events', url('/incubatee-events',$incubatee));
});

//Home >Propella Platform > Manage Incubatees > Registered Events > Edit Event
Breadcrumbs::for('administrator-events-incubatee-edit', function ($trail,$incubatee,$eventIncubatee) {
    $trail->parent('administrator-events-incubatee',$incubatee);
    $trail->push('Edit Incubatee Event', url('/incubatee-edit',$eventIncubatee));
});

//Home >Propella Platform > Manage Vantures
Breadcrumbs::for('administrator-all-ventures', function ($trail) {
    $trail->parent('administrator');
    $trail->push('All Ventures', url('/propella-ventures'));
});

//Home >Propella Alumni > Alumni Incubatees
Breadcrumbs::for('administrator-alumni-incubatee', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Alumni Incubatees', url('/alumni-incubatees'));
});

//Home >Exited Ventures> Manage Exited
Breadcrumbs::for('administrator-exited-ventures', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Exited Ventures', url('/exited-venture'));
});

//Home >Enquiries > Manage Categories
Breadcrumbs::for('administrator-enquiry-category', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Enquiry Categories', url('/enquiry-category-index'));
});

//Home >Enquiries > Manage Categories > Show Category Enquiry
Breadcrumbs::for('administrator-show-category-enquiry', function ($trail) {
    $trail->parent('administrator-enquiry-category');
    $trail->push('Category Enquiry', url('/show-enquiry-category-enquiries'));
});

//Home >Enquiries > Manage Categories > Edit Category Enquiry
Breadcrumbs::for('administrator-edit-category-enquiry', function ($trail) {
    $trail->parent('administrator-enquiry-category');
    $trail->push('Edit Enquiry Category', url('/edit-enquiry-category'));
});

//Home >Enquiries > Pending Enquiries
Breadcrumbs::for('administrator-unassigned-enquiry', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Unassigned Enquiry', url('/unassigned-enquiry-index'));
});

//Home >Enquiries > Pending Enquiries > Show
Breadcrumbs::for('administrator-show-enquiry', function ($trail) {
    $trail->parent('administrator-unassigned-enquiry');
    $trail->push('Enquiry Overview', url('/show-enquiry'));
});

//Home >Enquiries > Pending Enquiries > Assign
Breadcrumbs::for('administrator-assign-enquiry', function ($trail) {
    $trail->parent('administrator-unassigned-enquiry');
    $trail->push('Assign Enquiry Follow Up', url('/pre-assign-enquiry'));
});

//Home >Enquiries >  Pre-Assign Enquiry
Breadcrumbs::for('administrator-preassigned-enquiry', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Pre Assigned Enquiries', url('/pre-assigned-enquiry-index'));
});


//Home >Enquiries > Pre-Assigned Enquiry > Show
Breadcrumbs::for('administrator-show-preassigned-enquiry', function ($trail) {
    $trail->parent('administrator-preassigned-enquiry');
    $trail->push('Pre Assigned Enquiries Overview', url('/show-enquiry'));
});

//Home >Enquiries > Pre-Assign Enquiry > Assign
Breadcrumbs::for('administrator-assign-preassigned-enquiry', function ($trail) {
    $trail->parent('administrator-preassigned-enquiry');
    $trail->push('Assign Enquiry Follow Up', url('/show-enquiry'));
});

//Home >Enquiries >  Assigned Enquiries
Breadcrumbs::for('administrator-assigned-enquiries', function ($trail) {
    $trail->parent('administrator');
    $trail->push('Assigned Enquiries', url('/assigned-enquiry-index'));
});

//Home
Breadcrumbs::for('marketing', function ($trail) {
    $trail->push('Home', route('home'));
});

//Home > View Incubatees
Breadcrumbs::for('marketing-venture', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Ventures', url('/venture'));
});

//Home > View Incubatees > Add Venture
Breadcrumbs::for('marketing-new-venture', function ($trail) {
    $trail->parent('marketing-venture');
    $trail->push('Add Venture', url('/create-venture'));
});

//Home > View Incubatees > Edit Venture
Breadcrumbs::for('marketing-edit-venture', function ($trail,$venture) {
    $trail->parent('marketing-venture');
    $trail->push('Edit Venture', url('/venture-edit',$venture));
});

//Home > View Incubatees > Add Venture Incubatee
Breadcrumbs::for('marketing-add-venture', function ($trail,$venture) {
    $trail->parent('marketing-edit-venture',$venture);
    $trail->push('Add Venture Incubatee', url('/venture-incubatee'));
});

//Home > View Incubatees > Edit Venture Incubatee
Breadcrumbs::for('marketing-edit-venture-inc', function ($trail,$incubatee,$ventureIncubatee) {
    $trail->parent('marketing-edit-venture',$ventureIncubatee);
    $trail->push('Edit Venture Incubatee', url('/venture-edit-incubatee',$incubatee));
});

//Home > Applicants
Breadcrumbs::for('marketing-applicants', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Applicants', url('/applicants'));
});

//Home > Applicants > View Details
Breadcrumbs::for('marketing-applicants-view', function ($trail) {
    $trail->parent('marketing-applicants');
    $trail->push('Applicant Details', url('/applicant-show'));
});

//Home > Applicants > Access Window
Breadcrumbs::for('marketing-applicants-window', function ($trail) {
    $trail->parent('marketing-applicants');
    $trail->push('Applicant Panel Access Window', url('/applicant-panel-access-window'));
});

//Home > Applicants > View Assigned Panelist
Breadcrumbs::for('marketing-applicants-panelist', function ($trail) {
    $trail->parent('marketing-applicants');
    $trail->push('Assigned Panelist', url('/show-applicant-panelists'));
});

//Home > Applicants > View Next Step
Breadcrumbs::for('marketing-applicants-next', function ($trail) {
    $trail->parent('marketing-applicants');
    $trail->push('Applicant next Step', url('/applicant-next-steps'));
});

//Home > ICT Ventures
Breadcrumbs::for('marketing-ict-venture', function ($trail) {
    $trail->parent('marketing');
    $trail->push('ICT Ventures', url('/ict-venture'));
});

//Home > Industrial Ventures
Breadcrumbs::for('marketing-ind-venture', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Industrial Ventures', url('/ind-only-venture'));
});

//Home > Events
Breadcrumbs::for('marketing-events', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Events', url('/users/show-events'));
});

//Home > Book Venue
Breadcrumbs::for('marketing-venues', function ($trail,$user) {
    $trail->parent('marketing');
    $trail->push('Book Venue', url('/show-booking',$user));
});

//Home > Book Venue > Venue Bookings
Breadcrumbs::for('marketing-book-venue', function ($trail,$user,$venue) {
    $trail->parent('marketing-venues',$user);
    $trail->push('Venue Bookings', url('/venue-booking',$venue));
});

//Home > Book Venue > Venue Bookings > Book a Venue
Breadcrumbs::for('marketing-book-a-venue', function ($trail,$user,$venue) {
    $trail->parent('marketing-book-venue',$user,$venue);
    $trail->push('Book A Venue', url('/venue-booking'));
});

//Home > Emailer > Companies
Breadcrumbs::for('marketing-company', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Propella Contacts', url('/bulk-mailer'));
});

//Home > Emailer > Visitors
Breadcrumbs::for('marketing-visitor', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Propella Visitor Contacts', url('/visitor-bulk-mailer'));
});

//Home > Emailer > All users
Breadcrumbs::for('marketing-users', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Propella Users', url('/incubatee-bulk-mailer-index'));
});

//Home > Emailer > All Ventures
Breadcrumbs::for('marketing-all-ventures', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Ventures', url('/emailerVentureIndex'));
});

//Home > Blogs > View Blogs
Breadcrumbs::for('marketing-blogs', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Blogs', url('/blog-index'));
});


//Home > Satelite Hub Gallery > Upload Media
Breadcrumbs::for('marketing-create-gallery', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Upload Media', url('/create-gallery'));
});

//Home > Satelite Hub Gallery > All Media
Breadcrumbs::for('marketing-all-gallery', function ($trail) {
    $trail->parent('marketing');
    $trail->push('All Media Gallery', url('/gallery-index'));
});

//Home > Satelite Hub Gallery > All Media> Edit
Breadcrumbs::for('marketing-edit-gallery', function ($trail) {
    $trail->parent('marketing-all-gallery');
    $trail->push('Edit Media Gallery', url('/edit-media-gallery'));
});

//Home > Learning Curves > Get Learning Curves
Breadcrumbs::for('marketing-learning', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Learning Curves', url('/get-learning-curves'));
});

//Home > Venues > view Venues
Breadcrumbs::for('marketing-all-venues', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Venues', url('/get-venues'));
});

//Home > Venues > view Venues > Create Venue
Breadcrumbs::for('marketing-create-venues', function ($trail) {
    $trail->parent('marketing-all-venues');
    $trail->push('Create Venue', url('/create-venue'));
});

//Home > Calendar > View Calendar
Breadcrumbs::for('marketing-calendar', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Event Calendar', url('/full_calendar/events'));
});

//Home > Calendar > View Calendar > View Events
Breadcrumbs::for('marketing-calendar-events', function ($trail) {
    $trail->parent('marketing-calendar');
    $trail->push('Calendar Event', url('/full_calendar/view-events'));
});

//Home > Calendar > View Calendar > View Events > Edit
Breadcrumbs::for('marketing-edit-calendar-events', function ($trail) {
    $trail->parent('marketing-calendar-events');
    $trail->push('Calendar Event', url('/event-edit'));
});

//Home > Calendar > View Calendar > View Events > Visitors
Breadcrumbs::for('marketing-calendar-events-visitors', function ($trail) {
    $trail->parent('marketing-calendar-events');
    $trail->push('Event Visitors', url('/event-show'));
});

//Home > Calendar > All Events
Breadcrumbs::for('marketing-events-calendar', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Events', url('/events'));
});

//Home > Calendar > Private Events
Breadcrumbs::for('marketing-private-events', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Private Events', url('/events'));
});

//Home > Calendar > Private Events > Visitor
Breadcrumbs::for('marketing-private-event-visitor', function ($trail) {
    $trail->parent('marketing-private-events');
    $trail->push('Private Event Visitors', url('/private-event-show'));
});

//Home > Companies Database > Companies
Breadcrumbs::for('marketing-companies', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Company', url('/companies'));
});

//Home > Companies Database > Companies > Edit
Breadcrumbs::for('marketing-edit-companies', function ($trail) {
    $trail->parent('marketing-companies');
    $trail->push('Edit Company', url('/company'));
});

//Home > Companies Database > Companies > Create
Breadcrumbs::for('marketing-create-companies', function ($trail) {
    $trail->parent('marketing-companies');
    $trail->push('Create Company', url('/company-create'));
});

//Home > Companies Database > People
Breadcrumbs::for('marketing-company-employee', function ($trail) {
    $trail->parent('marketing');
    $trail->push('All Contacts', url('/company-employees'));
});

//Home > Companies Database > People > View Information
Breadcrumbs::for('marketing-employee-info', function ($trail) {
    $trail->parent('marketing-company-employee');
    $trail->push('Personal Information', url('/company-employees/show'));
});

//Home > Companies Database > People > Edit
Breadcrumbs::for('marketing-employee-edit', function ($trail) {
    $trail->parent('marketing-company-employee');
    $trail->push('Edit Personal Information', url('/company-employee'));
});

//Home >Propella Platform > Manage Alumni
Breadcrumbs::for('marketing-alumni', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Alumni Ventures', url('/alumni-venture'));
});

//Home >Propella Platform > Manage Incubatees
Breadcrumbs::for('marketing-incubatees', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Incubatees ', url('/incubatees'));
});

//Home >Propella Platform > Manage Incubatees > Edit
Breadcrumbs::for('marketing-edit-incubatees', function ($trail) {
    $trail->parent('marketing-incubatees');
    $trail->push('Edit Incubatee ', url('/incubatee-edit'));
});

//Home >Propella Platform > Manage Incubatees > Create
Breadcrumbs::for('marketing-create-incubatees', function ($trail) {
    $trail->parent('marketing-incubatees');
    $trail->push('Create Incubatee ', url('/create-incubatee'));
});

//Home >Propella Platform > Declined Applicants
Breadcrumbs::for('marketing-declined-applicants', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Declined Applicants ', url('/declined-applicants'));
});

//Home >Propella Platform > Declined Applicants > Account Overview
Breadcrumbs::for('marketing-declined-applicant-acc', function ($trail) {
    $trail->parent('marketing-declined-applicants');
    $trail->push('Account Overview ', url('/declined-applicant-account-overview'));
});

//Home >Propella Platform > Declined Applicants > Application
Breadcrumbs::for('marketing-declined-applicant-app', function ($trail) {
    $trail->parent('marketing-declined-applicants');
    $trail->push('Application ', url('/show-declined-applicant-application'));
});

//Home >Propella Platform > Propella Ventures
Breadcrumbs::for('marketing-prop-ventures', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Propella Ventures', url('/propella-venture'));
});

//Home >Exited Ventures > Manage Exited
Breadcrumbs::for('marketing-exited-ventures', function ($trail) {
    $trail->parent('marketing');
    $trail->push('Propella Exited Ventures', url('/propella-venture'));
});

//Home
Breadcrumbs::for('advisor', function ($trail) {
    $trail->push('Home', route('home'));
});

//Home >Applicants
Breadcrumbs::for('advisor-applicants', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Applicants', url('/applicants'));
});

//Home >Applicants > Show
Breadcrumbs::for('advisor-applicant-details', function ($trail) {
    $trail->parent('advisor-applicants');
    $trail->push('Applicant Details', url('/applicant-show'));
});

//Home >Applicants > Access Window
Breadcrumbs::for('advisor-applicant-access', function ($trail) {
    $trail->parent('advisor-applicants');
    $trail->push('Applicant Access Window', url('/applicant-panel-access-window'));
});

//Home >Applicants > View Assigned Panelist
Breadcrumbs::for('advisor-applicant-panelist', function ($trail) {
    $trail->parent('advisor-applicants');
    $trail->push('Assigned Panelist', url('/show-applicant-panelists'));
});

//Home >ICT Ventures
Breadcrumbs::for('advisor-ict-ventures', function ($trail) {
    $trail->parent('advisor');
    $trail->push('ICT Ventures', url('/ict-venture'));
});

//Home >ICT Ventures > Edit
Breadcrumbs::for('advisor-ict-ventures-edit', function ($trail) {
    $trail->parent('advisor-ict-ventures');
    $trail->push('Edit Venture', url('/venture-edit'));
});

//Home >Industrial Ventures
Breadcrumbs::for('advisor-ind-ventures', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Industrial Ventures', url('/ind-only-venture'));
});

//Home > Venue Booking
Breadcrumbs::for('advisor-venues', function ($trail,$users) {
    $trail->parent('advisor');
    $trail->push('Book Venue', url('/show-booking', $users));
});

//Home > Venue Booking > Time slots
Breadcrumbs::for('advisor-venues-slots', function ($trail,$users,$venue) {
    $trail->parent('advisor-venues',$users);
    $trail->push('Venue Booked Times', url('/venue-booking',$venue));
});

//Home > Venue Booking > Time slots > Create Booking
Breadcrumbs::for('advisor-add-venue-booking', function ($trail,$users,$venue) {
    $trail->parent('advisor-venues-slots',$users,$venue);
    $trail->push('Book A Venue', url('/book-venue',$venue));
});

//Home > Venue Booking > Time slots > Edit Booking
Breadcrumbs::for('advisor-edit-venue-booking', function ($trail,$users,$venue) {
    $trail->parent('advisor-venues',$users);
    $trail->push('Edit A Venue Booking', url('/book-venue',$venue));
});

//Home > Events
Breadcrumbs::for('advisor-events', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Events', url('/users/show-events'));
});

//Home > Emailer > Companies
Breadcrumbs::for('advisor-emailer', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Propella Contacts', url('/bulk-mailer'));
});

//Home > Emailer > Venture
Breadcrumbs::for('advisor-venture-emailer', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Ventures Email', url('/emailerVentureIndex'));
});

//Home > Workshop > Workshop Evaluations
Breadcrumbs::for('advisor-evaluations', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Workshop Evaluations', url('/workshop-index'));
});

//Home > Resources > Send Email
Breadcrumbs::for('advisor-send-email', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Ventures', url('/send-email'));
});

//Home > Resources > Funding
Breadcrumbs::for('advisor-funding', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Funding Category', url('/indexCategory'));
});

//Home > Resources > Funding > Edit
Breadcrumbs::for('advisor-funding-edit', function ($trail) {
    $trail->parent('advisor-funding');
    $trail->push('Edit Funding Category', url('/editCategory'));
});

//Home > Resources > Funding > Create
Breadcrumbs::for('advisor-funding-create', function ($trail) {
    $trail->parent('advisor-funding');
    $trail->push('Create Funding Category', url('/create-category'));
});

//Home > Resources > Trade Show
Breadcrumbs::for('advisor-trade-show', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Trade Show', url('/tradeShow'));
});

//Home > Resources > Grant
Breadcrumbs::for('advisor-grant', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Grant', url('/grant'));
});

//Home > Resources > Funding
Breadcrumbs::for('advisor-fund', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Funding', url('/fundings'));
});

//Home > Resources > Events
Breadcrumbs::for('advisor-event', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Events', url('/events'));
});

//Home > Resources > Competition
Breadcrumbs::for('advisor-competition', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Competition', url('/competition'));
});

//Home > Resources > Crowd
Breadcrumbs::for('advisor-crowd', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Crowd Funding', url('/crowd'));
});

//Home > Resources > All Resources
Breadcrumbs::for('advisor-all-resources', function ($trail) {
    $trail->parent('advisor');
    $trail->push('All Resources', url('/resourceIndex'));
});

//Home > Resources > Emailer
Breadcrumbs::for('advisor-emailers', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Ventures', url('/allIncubatees'));
});

//Home > Blog > View Blog
Breadcrumbs::for('advisor-blogs', function ($trail) {
    $trail->parent('advisor');
    $trail->push('All Blogs', url('/blog-index'));
});

//Home > Learning Curves > Get learning Curves
Breadcrumbs::for('advisor-learning-curves', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Learning Curves', url('/get-learning-curves'));
});

//Home > Venues > View Venues
Breadcrumbs::for('advisor-get-venues', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Venues', url('/get-venues'));
});

//Home > Venues > View Venues > View Bookings
Breadcrumbs::for('advisor-get-venue-booking', function ($trail) {
    $trail->parent('advisor-get-venues');
    $trail->push('Venue Booking', url('/show-venue-bookings'));
});

//Home > Calendar > View Calendar
Breadcrumbs::for('advisor-event-calendar', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Event Calendar', url('/full_calendar/events'));
});

//Home > Calendar > View Calendar > Calendar Events
Breadcrumbs::for('advisor-calendar-events', function ($trail) {
    $trail->parent('advisor-event-calendar');
    $trail->push('Calendar Events', url('/full_calendar/view-events'));
});

//Home > Calendar > View Calendar > Calendar Events > Edit event
Breadcrumbs::for('advisor-edit-calendar-events', function ($trail) {
    $trail->parent('advisor-calendar-events');
    $trail->push('Edit Calendar Event', url('/event-edit'));
});

//Home > Calendar > View Calendar > Calendar Events > Visitors
Breadcrumbs::for('advisor-calendar-event-visitors', function ($trail) {
    $trail->parent('advisor-calendar-events');
    $trail->push('Event Visitors', url('/event-show'));
});

//Home > Calendar > All Events
Breadcrumbs::for('advisor-event-index', function ($trail) {
    $trail->parent('advisor');
    $trail->push('All Events', url('/events'));
});

//Home > Calendar > Private Events
Breadcrumbs::for('advisor-private-event', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Private Events', url('/private-events'));
});

//Home > Calendar > Private Events > Visitors
Breadcrumbs::for('advisor-private-event-visitors', function ($trail) {
    $trail->parent('advisor-private-event');
    $trail->push('Private Event Visitors', url('/private-event-show'));
});

//Home > Companies Database > Companies
Breadcrumbs::for('advisor-company', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Companies', url('/companies'));
});

//Home > Companies Database>  Companies > View Companies
Breadcrumbs::for('advisor-view-companies', function ($trail,$company) {
    $trail->parent('advisor-company');
    $trail->push('Company Details', url('/company/show',$company));
});

//Home > Companies Database>  Companies > View Companies > Create Employee
Breadcrumbs::for('advisor-view-employees-create', function ($trail,$company) {
    $trail->parent('advisor-view-companies',$company);
    $trail->push('Add New Employee', url('/company-employee-create'));
});

//Home > Companies Database> Employees
Breadcrumbs::for('advisor-employees', function ($trail) {
    $trail->parent('advisor');
    $trail->push('All Employees', url('/company-employees'));
});

//Home > Companies Database>  Employees > View Employee
Breadcrumbs::for('advisor-view-employees', function ($trail,$companyEmployee) {
    $trail->parent('advisor-employees');
    $trail->push('Employee Details', url('/company-employee/show',$companyEmployee));
});

//Home > Companies Database>  Employees > Edit Employee
Breadcrumbs::for('advisor-edit-employees', function ($trail,$companyEmployee) {
    $trail->parent('advisor-employees');
    $trail->push('Edit Employee', url('/company-employee',$companyEmployee));
});

//Home > Companies Database>  Employees > View Employee > Add Contact Log
Breadcrumbs::for('advisor-add-employees-log', function ($trail,$companyEmployee) {
    $trail->parent('advisor-view-employees',$companyEmployee);
    $trail->push('Contact Log', url('/add-activity-log-for-contact'));
});


//Home > Companies Database> Contact Logs
Breadcrumbs::for('advisor-logs', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Contact Log', url('/contact-logs'));
});

//Home > Companies Database> Contact Logs > Add Contact Log
Breadcrumbs::for('advisor-add-logs', function ($trail) {
    $trail->parent('advisor-logs');
    $trail->push('Add Contact Log', url('/add-activity-logs'));
});

//Home >Propella Platform > Manage Ventures
Breadcrumbs::for('advisor-ventures', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Ventures', url('/venture'));
});

//Home >Propella Platform > Manage Ventures > Create ventures
Breadcrumbs::for('advisor-create-ventures', function ($trail) {
    $trail->parent('advisor-ventures');
    $trail->push('Create Venture', url('/create-venture'));
});

//Home >Propella Platform > Manage Alumni
Breadcrumbs::for('advisor-alumni', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Alumni', url('/alumni-venture'));
});

//Home >Propella Platform > Manage Incubatees
Breadcrumbs::for('advisor-incubatee', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Incubatees', url('/incubatees'));
});

//Home >Propella Platform > Manage Incubatees > Account Overview
Breadcrumbs::for('advisor-incubatee-account', function ($trail) {
    $trail->parent('advisor-incubatee');
    $trail->push('Account Overview', url('/incubatee-account-overview'));
});

//Home >Propella Platform > Manage Incubatees > Edit
Breadcrumbs::for('advisor-incubatee-edit', function ($trail) {
    $trail->parent('advisor-incubatee');
    $trail->push('Incubatee Edit', url('/incubatee-edit'));
});

//Home >Propella Platform > Manage Incubatees > Create
Breadcrumbs::for('advisor-incubatee-create', function ($trail) {
    $trail->parent('advisor-incubatee');
    $trail->push('Create Incubatee', url('/create-incubatee'));
});

//Home >Propella Platform > Manage Bootcamper
Breadcrumbs::for('advisor-bootcamper', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Bootcampers', url('/bootcampers'));
});

//Home > Propella Platform > Manage Bootcampers > Show Bootcamper Details
Breadcrumbs::for('advisor-bootcamper-details', function ($trail) {
    $trail->parent('advisor-bootcamper');
    $trail->push('Bootcamper Details', url('/bootcamper-show'));
});

//Home > Propella Platform > Manage Bootcampers > Bootcamper Events
Breadcrumbs::for('advisor-bootcamper-events', function ($trail,$bootcamper) {
    $trail->parent('advisor-bootcamper');
    $trail->push('Bootcamper Events', url('/bootcamper-events/',$bootcamper));
});

//Home > Propella Platform > Manage Bootcampers > Bootcamper to Declined Applicant
Breadcrumbs::for('advisor-bootcamper-to-declined', function ($trail,$bootcamper) {
    $trail->parent('advisor-bootcamper');
    $trail->push('Bootcamper to Declined  Declined Applicant', url('/bootcamper-to-declined-applicant-view/',$bootcamper));
});

//Home > Propella Platform > Manage Bootcampers > View Assigned panelist
Breadcrumbs::for('advisor-bootcamper-view-panelist', function ($trail,$bootcamper) {
    $trail->parent('Advisor-bootcamper');
    $trail->push('Assigned Panelists', url('/show-ict-applicant-panelists/',$bootcamper));
});

//Home >Propella Platform > Declined Bootcamper
Breadcrumbs::for('advisor-declined-bootcamper', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Declined Bootcampers', url('/declined-bootcampers'));
});

//Home >Propella Platform > Declined Bootcamper > Account Overview
Breadcrumbs::for('advisor-declined-bootcamper-account', function ($trail) {
    $trail->parent('advisor-declined-bootcamper');
    $trail->push('Declined Bootcamper Details', url('/declined-bootcamper-account-overview'));
});

//Home >Propella Platform > Declined Bootcamper > Edit
Breadcrumbs::for('advisor-edit-declined-bootcamper', function ($trail) {
    $trail->parent('advisor-declined-bootcamper');
    $trail->push('Edit Declined Bootcamper', url('/edit-declined-bootcamper-info'));
});

//Home >Applicants > Next Step
Breadcrumbs::for('advisor-next-step', function ($trail) {
    $trail->parent('advisor-applicants');
    $trail->push('Applicant Next Step', url('/applicant-next-step'));
});

//Home >Propella Platform > Declined Applicants
Breadcrumbs::for('advisor-declined-applicants', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Declined Applicants', url('/declined-applicants'));
});

//Home >Propella Platform > Declined Applicants > Account Overview
Breadcrumbs::for('advisor-declined-applicant-account', function ($trail) {
    $trail->parent('advisor-declined-applicants');
    $trail->push('Declined Applicant Overview', url('/declined-applicant-account-overview'));
});

//Home >Propella Platform > Declined Applicants > Show Details
Breadcrumbs::for('advisor-declined-applicant-details', function ($trail) {
    $trail->parent('advisor-declined-applicants');
    $trail->push('Declined Applicant Details', url('/show-declined-applicant-application'));
});

//Home >Propella Platform > Propella Ventures
Breadcrumbs::for('advisor-prop-venture', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Propella Ventures', url('/propella-ventures'));
});

//Home >Propella Platform > Manage Mentors
Breadcrumbs::for('advisor-mentors', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Mentors', url('/mentors'));
});

//Home >Propella Platform > Manage Mentors > Mentor Details
Breadcrumbs::for('advisor-mentor-details', function ($trail,$user_mentor) {
    $trail->parent('advisor-mentors');
    $trail->push('Mentor Details', url('/mentor-show',$user_mentor));
});

//Home >Propella Platform > Manage Mentors > Mentor Details > Venture
Breadcrumbs::for('advisor-mentor-venture', function ($trail,$user_mentor,$venture) {
    $trail->parent('advisor-mentor-details',$user_mentor);
    $trail->push('Venture Content', url('/show-mentor-venture-content',$venture));
});

//Home >Propella Platform > Manage Mentors > Mentor Details > Venture > Shadowboard Results
Breadcrumbs::for('advisor-mentor-venture-results', function ($trail,$user_mentor,$venture) {
    $trail->parent('advisor-mentor-venture',$user_mentor,$venture);
    $trail->push('Venture Shadowboard Results', url('/view-mentor-venture-shadowboard-results',$user_mentor,$venture));
});

//Home >Propella Alumni > Alumni Incubatees
Breadcrumbs::for('advisor-alumni-incubatees', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Alumni Incubatees', url('alumni-incubatees'));
});

//Home > Exited Ventures > Manage Exited
Breadcrumbs::for('advisor-exited-venture', function ($trail) {
    $trail->parent('advisor');
    $trail->push('Exited Ventures', url('exited-venture'));
});

//INCUBATEE
//Home
Breadcrumbs::for('inc-home', function ($trail) {
    $trail->push('Home', route('home'));
});

//Home > Manage Your Shadowboards
Breadcrumbs::for('inc-shadowboard', function ($trail,$venture) {
    $trail->parent('inc-home');
    $trail->push('Shadow Board Meetings', url('venture-shadow',$venture));
});

//Home > Manage Your Venture
Breadcrumbs::for('inc-venture', function ($trail,$venture) {
    $trail->parent('inc-home');
    $trail->push('Edit Venture', url('venture-edit',$venture));
});

//Home > Manage Your Venture > Create Incubatee
Breadcrumbs::for('inc-venture-incubatee', function ($trail,$venture) {
    $trail->parent('inc-venture',$venture);
    $trail->push('Assign Incubatee to Venture', url('venture-incubatee',$venture));
});

//Home >Edit Your Details
Breadcrumbs::for('inc-edit-incubatee', function ($trail) {
    $trail->parent('inc-home');
    $trail->push('Edit Incubatee', url('incubatee-edit'));
});

//Home >Events
Breadcrumbs::for('inc-events', function ($trail,$incubatee) {
    $trail->parent('inc-home');
    $trail->push('Incubatee Events', url('/users/incubatee-dashboard/full-calendar/show-events',$incubatee));
});


//Home >Events >Edit Incubatee Registered Event
Breadcrumbs::for('inc-edit-incubatee-event', function ($trail,$incubatee,$eventIncubatee) {
    $trail->parent('inc-events',$incubatee);
    $trail->push('Edit Incubatee Events', url('/incubatee-registered-event-edit',$eventIncubatee));
});

//Home > Book Venue
Breadcrumbs::for('inc-book-venue', function ($trail,$user) {
    $trail->parent('inc-home');
    $trail->push('Book Venue', url('/show-booking',$user));
});

//Home > Book Venue > Book a Vanue
Breadcrumbs::for('inc-book-a-venue', function ($trail,$users,$venue) {
    $trail->parent('inc-book-venue',$users);
    $trail->push('All Bookings', url('/venue-booking',$venue));
});

//Home > Book Venue > Book a Vanue > Create booking
Breadcrumbs::for('inc-add-venue-booking', function ($trail,$users,$venue) {
    $trail->parent('inc-book-a-venue',$users,$venue);
    $trail->push('Create Booking', url('/book-venue',$venue));
});

//Home > Book Venue > Edit Venue Booking
Breadcrumbs::for('inc-edit-venue-booking', function ($trail,$users,$venue) {
    $trail->parent('inc-book-venue',$users);
    $trail->push('Edit Venue Booking', url('/edit-venue-booking',$venue));
});

//Home > Manage Your Employees
Breadcrumbs::for('inc-employees', function ($trail,$venture) {
    $trail->parent('inc-home');
    $trail->push('All Employees', url('employee-index',$venture));
});


//Home > Manage Your Employees > Add Employee
Breadcrumbs::for('inc-create-employee', function ($trail,$venture) {
    $trail->parent('inc-employees',$venture);
    $trail->push('Create Employee', url('/create-employee'));
});

//Home > Manage Your Employees > Update Employee
Breadcrumbs::for('inc-edit-employee', function ($trail,$venture) {
    $trail->parent('inc-employees',$venture);
    $trail->push('Update Employee', url('/edit-employee'));
});

//Home > Resources > Trade Show
Breadcrumbs::for('inc-trade', function ($trail) {
    $trail->parent('inc-home');
    $trail->push('Trade Show', url('tradeShow'));
});

//Home > Resources > Grant
Breadcrumbs::for('inc-grant', function ($trail) {
    $trail->parent('inc-home');
    $trail->push('Grant', url('grant'));
});

//Home > Resources > Funding
Breadcrumbs::for('inc-funding', function ($trail) {
    $trail->parent('inc-home');
    $trail->push('Funding', url('fundings'));
});

//Home > Resources > Competition
Breadcrumbs::for('inc-competition', function ($trail) {
    $trail->parent('inc-home');
    $trail->push('Competition', url('competition'));
});

//Home > Resources > Crowd Funding
Breadcrumbs::for('inc-crowd', function ($trail) {
    $trail->parent('inc-home');
    $trail->push('Crowd Funding', url('crowd'));
});

//Home > Resources > Events
Breadcrumbs::for('inc-event', function ($trail) {
    $trail->parent('inc-home');
    $trail->push('Events', url('events'));
});

//APPLICANT
//Home
Breadcrumbs::for('applicant', function ($trail) {
    $trail->push('Home', route('home'));
});

//Home > Edit Application
Breadcrumbs::for('applicant-edit-app', function ($trail) {
    $trail->parent('applicant');
    $trail->push('Edit Application', url('events'));
});

//Home > Edit Applicant Details
Breadcrumbs::for('applicant-edit-details', function ($trail) {
    $trail->parent('applicant');
    $trail->push('Edit Applicant Details', url('events'));
});

//MENTOR
//Home
Breadcrumbs::for('mentor', function ($trail) {
    $trail->push('Home', route('home'));
});

//Home > View Ventures
Breadcrumbs::for('mentor-venture-to-mentor', function ($trail) {
    $trail->parent('mentor');
    $trail->push('Mentor Ventures', url('mentor-show'));
});

//Home > View Shadow Board
Breadcrumbs::for('mentor-shadow-board', function ($trail,$mentor) {
    $trail->parent('mentor');
    $trail->push('Shadow Board', url('mentor-venture-shadow',$mentor));
});

//Home > View Shadow Board > View
Breadcrumbs::for('mentor-venture-evaluation', function ($trail,$mentor) {
    $trail->parent('mentor-shadow-board',$mentor);
    $trail->push('Venture Evaluation', url('mentor-shadow-board-form'));
});

//Home > Venues
Breadcrumbs::for('mentor-venues', function ($trail,$user) {
    $trail->parent('mentor');
    $trail->push('Venues', url('/show-booking',$user));
});

//Home > Venues> View venue bookings
Breadcrumbs::for('mentor-venue-bookings', function ($trail,$users,$venue) {
    $trail->parent('mentor-venues',$users);
    $trail->push('Venue Bookings', url('/venue-booking',$venue));
});

//Home > Venues> View venue bookings > Book venue
Breadcrumbs::for('mentor-booking-venue', function ($trail,$users,$venue) {
    $trail->parent('mentor-venue-bookings',$users,$venue);
    $trail->push('Book Venue ', url('/book-venue'));
});

//Home > Venues> Edit Venue Booking
Breadcrumbs::for('mentor-edit-venue-bookings', function ($trail,$users,$venues) {
    $trail->parent('mentor-venues',$users);
    $trail->push('Edit Venue Booking', url('/book-venue',$venues));
});
