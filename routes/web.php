<?php

use App\Events\StatusLiked;
/*
|--------------------------------------------------------------------------
| Web Routes`
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|





*/

//Newsletter
Route::post('store-newsletter','WelcomeController@storeNewsletter')->name('store-newsletter');
Route::get('newsletter-index','UsersController@newsletterIndex')->name('newsletter-index');
Route::get('get-newsletter','UsersController@getNewsletter')->name('get-newsletter');

//Venture stage update
Route::post('/update-stage/{venture}','UsersController@updateVentureStage')->name('update-stage');
Route::post('/update-status/{venture}','UsersController@updateVentureStatus')->name('update-status');

//Make appointment
Route::post('store-appointment','UsersController@storeMentorAppointments');

//TIA Application
Route::get('tia-application','ApplicationProcessController@tiaApplication')->name('tia-application');
Route::post('store-tia-application','ApplicationProcessController@storeTiaApplication')->name('store-tia-application');
Route::get('tia-index','UsersController@tiaIndex')->name('tia-index');
Route::get('get-tia-index','UsersController@getTiaApplicants')->name('get-tia-index');
Route::get('/get-tia-overview/{tiaApplication}','UsersController@viewTiaApplicationOverview')->name('get-tia-overview');

//Pitching101
Route::get('pitching101','WelcomeController@pitching101')->name('pitching101');

//Testimonials
Route::get('testimonials','WelcomeController@testimonialsPage')->name('testimonials');


//Donation
Route::get('donation-page','WelcomeController@donation');
Route::post('store-donation','WelcomeController@storeDonationForm')->name('store-donation');
Route::get('donation-index','UsersController@donationIndex')->name('donation-index');
Route::get('get-donation-index','UsersController@getDonations')->name('get-donation-index');

//VENTURE PANEL
Route::get('/edit-venture-panel-interview/{venturePanelInterview}','VentureController@adminEditVenturePanelInterview')->name('edit-venture-panel-interview');

//SHOW VENTURE INCUBATEE
Route::get('/show-venture-incubatee/{user}','VentureController@showVentureIncubateeOnVenturesSide')->name('show-venture-incubatee');

//Presentation
Route::get('create-presentation','UsersController@createPresentation')->name('create-presentation');
Route::post('store-presentation','UsersController@storePresentation')->name('store-presentation');
Route::get('/view-presentation/{id}','UsersController@viewPresentationUpload')->name('view-presentation');
Route::get('/view-inc-presentation/{id}','UsersController@viewIncubateePresentationUpload')->name('view-inc-presentation');
Route::get('index-presentation','UsersController@indexPresentation')->name('index-presentation');
Route::get('get-presentation','UsersController@getPresentation')->name('get-presentation');
Route::get('/assign-incubatees-to-presentation/{presentation}','UsersController@assignIncubateesToPresentation')->name('assign-incubatees-to-presentation');
Route::get('/edit-workshop-presentation/{presentation}','UsersController@editWorkshopPresentation')->name('edit-workshop-presentation');
Route::post('assign','UsersController@assignPresentationToIncubatee')->name('assign');
Route::get('incubatee-workshops','UsersController@incubateeWorkshops')->name('incubatee-workshops');
Route::get('get-incubatee-workshops','UsersController@getIncubateePresentation')->name('get-incubatee-workshops');
Route::post('/update-presentation/{presentation}','UsersController@updatePresentation')->name('update-presentation');
Route::get('get-incubatees-to-assign-to-presentation','UsersController@getAssignIncubateesToPresentation')->name('get-incubatees-to-assign-to-presentation');
Route::get('get-ict-and-industrial', 'UsersController@getICTAndIndIncubatees')->name('get-ict-and-industrial');
Route::get('/destroy-presentation/{id}', 'UsersController@destroyPresentation');
Route::post('/upload-multiple-handout/{presentation}','UsersController@multipleHandoutUpload')->name('upload-multiple-presentation');
Route::post('/upload-multiple-presentation/{presentation}','UsersController@multiplePresentationUpload')->name('upload-multiple-handout');
Route::get('/delete-presentation-document/{id}', 'UsersController@deletePresentationHandout');

//STORE INCUBATEE STAGE 2 PRE AND POST
Route::post('store-pre-stage2-evaluation-form','IncubateeController@storePreStage2EvaluationAnswers');
Route::post('store-post-stage2-evaluation-form','IncubateeController@storePostStage2EvaluationAnswers');

//STORE INCUBATEE STAGE 3 PRE AND POST
Route::post('store-pre-stage3-evaluation-form','IncubateeController@storePreStage3EvaluationAnswers');
Route::post('store-post-stage3-evaluation-form','IncubateeController@storePostStage3EvaluationAnswers');

//STORE INCUBATEE STAGE 4 PRE AND POST
Route::post('store-pre-stage4-evaluation-form','IncubateeController@storePreStage4EvaluationAnswers');
Route::post('store-post-stage4-evaluation-form','IncubateeController@storePostStage4EvaluationAnswers');

//STORE INCUBATEE STAGE 5 PRE AND POST
Route::post('store-pre-stage5-evaluation-form','IncubateeController@storePreStage5EvaluationAnswers');
Route::post('store-post-stage5-evaluation-form','IncubateeController@storePostStage5EvaluationAnswers');

Route::get('ict-apply','WelcomeController@ictAppyLink');
//Update venture panel interview
Route::post('/update-venture-panel-interview/{venturePanelInterview}','VentureController@adminUpdatePanelInterviewDetailsVenture');

//Venture panel interview category
Route::get('/edit-venture-panel-interview-category/{venturePanelInterviewCategory}','VentureController@editVentureInterviewCategory')->name('edit-venture-panel-interview-category');
Route::post('/update-panel-interview-category/{venturePanelInterviewCategory}','VentureController@updatePanelInterviewCategory');
Route::get('/delete-venture-interview-category/{venturePanelInterviewCategory}','VentureController@deletePanelInterviewCategory');
Route::get('/show-score-sheet/{bootcamperPanelist}','IncubateeController@adminShowIncubateePanelistScoreSheet');

//Virtual lean coffee
Route::get('virtual-lean-coffee','WelcomeController@virtualLeanCoffee');

//Client conract
Route::get('/edit-venture-client-contract/{venture}','VentureController@editVentureContract');
Route::post('/update-venture-client-contract-stage/{venture}','VentureController@updateVentureContract');

Route::get('ict-21-virtual-showcase','WelcomeController@ICT21VirtualShowcase');

//User events delete
Route::get('/delete-user-event/{id}','UsersController@destroyUserEvents');

//Application quiz
Route::get('application-quiz','WelcomeController@applicationQuiz');

//LATEST INFO
Route::get('create-latest-info','SummernoteController@createLatestInfo');
Route::post('store-latest-info','SummernoteController@storeLatestInfo')->name('store-latest-info');
Route::post('store-latest-info-summernote','SummernoteController@storeLatestInfoSummernote')->name('store-latest-info-summernote');
Route::get('/latest-info/{latestInfo}','SummernoteController@showDetailedLatestInfo')->name('latest-info');
Route::get('/download-latest-info-pdf/{id}','SummernoteController@downloadLatestInfoPDF');
Route::get('latest-info-index','SummernoteController@indexLatestPost');
Route::get('get-latest-info','SummernoteController@getLatestInfo')->name('get-latest-info');
Route::get('/edit-latest-info/{latestInfo}','SummernoteController@editLatestPosts');
Route::post('/update-latest-posts/{latestInfo}','SummernoteController@updateLatestPosts');
Route::post('/update-latest-info-summernote/{latestInfo}','SummernoteController@updateLatestPostSummernote');
Route::get('/download-latest-info-doc/{id}','SummernoteController@downloadLatestInfoWordDoc');
Route::get('/delete-latest-post/{id}','SummernoteController@destroyLatestPost');



Route::get('talk-change-me','WelcomeController@talkChangeMeEvent');
//MEDIA
Route::get('media','WelcomeController@mediaPage');

//DESIGN THINKING WORKSHOP
Route::get('design-thinking-workshop','WelcomeController@designThinking');


//4IR readiness programme
Route::get('4IR-readiness-questionnaire','WelcomeController@readinessQuestionairs');

//DEMO DAY EXHIBITION
Route::get('demo-day','WelcomeController@demoDayExhibition');

//TIA Application
Route::get('/TIA','WelcomeController@TIAView');

//View forms on tablet
Route::get('view-covid-forms','TabletController@viewDailyCovidForms');

//VENTURE ACCOUNT OVERVIEW
Route::get('/venture-account-overview/{venture}','VentureController@ventureAccountOverview');
Route::get('industrial-application','ApplicationProcessController@industrialCategory');

//Exited incubatees
Route::get('exited-incubatees','UsersController@exitedIncubatees');
Route::get('get-exited-incubatees','UsersController@getExitedIncubatees')->name('get-exited-incubatees');
Route::get('/get-declined-bootcamper-panelist/{declinedBootcamper}','UsersController@getDeclinedBootcamperPanelists');
Route::get('/declined-bootcamper-panelist-score-sheet/{declinedBootcamperPanelist}','UsersController@adminShowDeclinedBootcamperPanelistScoreSheet');

Route::get('/delete-declined-applicant-category/{id}','UsersController@destroyDeclinedApplicantCategory');
Route::get('/delete-referred-applicant/{id}','UsersController@destroyReferredApplicantCategory');

Route::get('create-news','SummernoteController@createNews')->name('create-news');
Route::post('store-news','SummernoteController@storeNews')->name('store-news');
Route::post('store-news-content','SummernoteController@storeNewsContent')->name('store-news-content');
Route::get('propella-news-index','SummernoteController@newsIndex')->name('propella-news-index');
Route::get('get-propella-news','SummernoteController@getNews')->name('get-propella-news');
Route::get('/edit-news/{propellaNew}','SummernoteController@editNews')->name('edit-news');
Route::post('/update-news/{propellaNew}','SummernoteController@updateNews')->name('update-news');
Route::post('/update-news-content/{propellaNew}','SummernoteController@updateNewsContent')->name('update-news-content');
Route::get('news','WelcomeController@newsPage');
Route::get('/destroy-news/{id}','SummernoteController@destroyNews');
Route::get('/detailed-news/{propellaNew}','WelcomeController@showDetailedNews')->name('detailed-news');

//UPLOAD VIDEOS
Route::get('upload-videos','UsersController@uploadVideos');
Route::post('store-video','UsersController@storeVideo')->name('store-video');
Route::get('video-index','UsersController@videoIndex');
Route::get('get-videos','UsersController@getVideos')->name('get-videos');
Route::get('video-gallery','WelcomeController@viewVideoGallery');
Route::get('/edit-video/{video}','UsersController@editVideos')->name('edit-video');
Route::post('/updateVideo/{video}','UsersController@updateVideo')->name('updateVideo');
Route::get('/delete-video/{video}','UsersController@deleteVideo');

//WORKSHOP EVALUATION
Route::post('store-stage3-evaluation-form','IncubateeController@storeStage3IncubateeEvaluationAnswers')->name('store-stage3-evaluation-form');
Route::post('store-stage4-evaluation-form','IncubateeController@storeStage4IncubateeEvaluationAnswers')->name('store-stage4-evaluation-form');
Route::post('store-stage5-evaluation-form','IncubateeController@storeStage5IncubateeEvaluationAnswers')->name('store-stage5-evaluation-form');
Route::post('store-stage6-evaluation-form','IncubateeController@storeStage6IncubateeEvaluationAnswers')->name('store-stage6-evaluation-form');
Route::post('store-end-of-stage-3-evaluation','IncubateeController@storeEndOfStage3IncubateeEvaluationAnswers')->name('store-end-of-stage-3-evaluation');

//COVID LOGIN REDIRECT
Route::get('visitor-covid-login','VisitorController@visitorCovidLogin');
Route::get('/get-visitor-covid-dashboard/{visitor}','VisitorController@covidVisitorDashboard');
Route::post('/store-visitors-covid-form','VisitorController@visitorStoreCovidForms')->name('store-visitors-covid-form');
Route::get('visitor-covid-index','VisitorController@visitorCovidFormsIndex')->name('visitor-covid-index');
Route::get('get-visitor-covid-forms','VisitorController@getVisitorCovidForms')->name('get-visitor-covid-forms');
Route::get('/get-all-users-covid-dashboard/{user}','VisitorController@covidAllUsersDashboard');
Route::get('staff-login','VisitorController@staffCovidLogin');
Route::post('search-staff','VisitorController@searchStaff')->name('search-staff');
Route::post('/store-staff-covid-form','VisitorController@staffStoreCovidForms')->name('store-staff-covid-form');
Route::get('staff-covid-index','VisitorController@staffCovidFormsIndex')->name('staff-covid-index');
Route::get('get-staff-covid-forms','VisitorController@getStaffCovidForms')->name('get-staff-covid-forms');
Route::get('employees-covid-login','VisitorController@employeesCovidLogin');
Route::post('employee-search','VisitorController@searchEmployees')->name('employee-search');
Route::get('/get-employees-dashboard/{companyEmployee}','VisitorController@covidEmployeesDashboard');
Route::post('/store-employee-covid-form','VisitorController@employeesStoreCovidForms')->name('store-employee-covid-form');
Route::get('company-emplyees-covid-index','VisitorController@employeesCovidFormsIndex')->name('company-emplyees-covid-index');
Route::get('get-employees-covid-forms','VisitorController@getEmployeesCovidForms')->name('get-employees-covid-forms');
Route::get('/delete-visitor-covid-form/{visitorCovidSymptom}','VisitorController@adminDeleteVisitorForm');
Route::get('/delete-staff-covid-form/{staffCovidSymptom}','VisitorController@adminDeleteStaffForm');
Route::get('/delete-employee-covid-form/{companyEmployeeCovidForm}','VisitorController@adminDeleteCompanyEmployeesForm');

//QR CODE
Route::get('qr-code-generator','UsersController@qrCodeGenerator');
Route::get('pti-application-category','ApplicationProcessController@rapPtiCategory');

//INCUBATEE POPI ACT
Route::post('/update-incubatee-popi-act/{incubatee}','IncubateeController@updateIncubateePopiCompliance')->name('update-incubatee-popi-act');
Route::post('/update-incubatee-nda-agreement/{incubatee}','IncubateeController@IncubateeNDAagreement')->name('update-incubatee-nda-agreement');

//DOCS
Route::get('funders-page','WelcomeController@propellaDocs');

//Admin approve incubatee panelist
Route::post('/incubatee-panel-interview-approve/{incubatee}','IncubateeController@adminApproveIncubateePanelInterview');
Route::post('/update-incubatee-panel-interview/{incubatee}','IncubateeController@adminUpdatePanelInterviewDetailsOfIncubatee');
Route::post('/admin-add-panel-for-panel-access-window-for-incubatees','IncubateeController@adminAddPanelistFromPanelAccessWindowFoIncubatee');
Route::get('/get-incubatee-ict-score-sheet/{incubatee}','IncubateeController@etIctIncubateePanelists');
Route::post('/store-incubatee-contact-log/{incubatee}','IncubateeController@storeIncubateeContactLog')->name('store-incubatee-contact-log');
Route::get('/show-panel-incubatees','IncubateeController@panelistShowIncubatees');
Route::get('/panelist-get-ict-incubatees/{user}','IncubateeController@panelistGetICTIncubatees');
Route::get('/show-incubatee-score-sheet/{incubatee}','IncubateeController@showIncubateePanelScoreSheet');
Route::post('/panelist-save-incubatee-question-answers','IncubateeController@panelistSaveIncubateeQuestionAnswerComments');
Route::get('/pti-ventures','WelcomeController@getPsiIncubatees');
Route::get('alumni-categories','WelcomeController@alumniCategories');
Route::get('exited-categories','WelcomeController@exitedCategories');
Route::get('industrial-alumni','WelcomeController@getIndustrialAlumni');
Route::get('township-hub-alumni','WelcomeController@getPTIAlumni');
Route::get('creative-alumni','WelcomeController@getCreativeAlumni');


//SHOW INCUBATEE ACCOUNT OVERVIEW
Route::get('/show-incubatee-details/{incubatee}','IncubateeController@showIncubateeDetails');
Route::get('/panel/{incubatee}','IncubateeController@showIncubateePanelResults');

/*Route::get('/test','WelcomeController@test');*/
//STORE BOOTCAMPER CONTRACT
Route::post('/store-bootcamper-contract/{bootcamper}','BootcamperController@bootcamperUploadContract');

Route::post('store-workshop-incubatee-form','IncubateeController@storeIncubateeEvaluationAnswers');

Route::get('/delete-event/{id}','EventController@deleteEvents');

Route::get('join-event','WelcomeController@bootcampAttendance');


//Bootcamper evaluaton
Route::get('/bootcaper-evaluation-index/{bootcamper}','BootcamperController@bootcamperEvaluationIndex');

//Store bootcamp evaluatiojn answers
Route::post('store-bootcamp-evaluation-answers','BootcamperController@storeBootcampEvaluationAnswers');
Route::post('store-beginning-of-bootcamp-evaluation','BootcamperController@storeBeginningBootcampEvaluationAnswers');
Route::post('store-post-of-bootcamp-evaluation','BootcamperController@storePostBootcampEvaluationAnswers');

//Bootcamp agreement
Route::post('/update-bootcamp-agreement/{bootcamper}','BootcamperController@updateBootcampAgreement');

//BOOTCAPER DATA PACKAGEs
Route::post('/bootcamper-store-data-network/{user}','BootcamperController@storeBootcamperDataUpload');

//BUSINESS CATEGORY
Route::get('create-business-category','BusinessCategoryController@createBusinessCategory')->name('create-business-category');
Route::get('business-category-index','BusinessCategoryController@businessCategoryIndex')->name('business-category-index');
Route::post('store-business-category','BusinessCategoryController@storeBusinessCategory')->name('store-business-category');
Route::get('business-category', 'BusinessCategoryController@businessCategoryIndex');
Route::get('get-business-category', 'BusinessCategoryController@getBusinessCategories')->name('get-business-category');
Route::get('edit-business-category', 'BusinessCategoryController@editBusinessCategory')->name('edit-business-category');
Route::get('/business-category-edit/{businessCategory}', 'BusinessCategoryController@editBusinessCategory');
Route::post('/update-business-category/{businessCategory}', 'BusinessCategoryController@updateBusinessCategory');
Route::get('/business-category-delete-check/{businessCategory}', 'BusinessCategoryController@deleteBusinessCategory');


//SMART TAG
Route::get('create-smart-tag','SmartTagsController@createSmartTag')->name('create-smart-tag');
Route::get('smart-tag-index','SmartTagsController@smartTagIndex')->name('venture-status-index');
Route::post('store-smart-tag','SmartTagsController@storeSmartTag')->name('store-smart-tag');
Route::get('smart-tag', 'SmartTagsController@smartTagIndex');
Route::get('get-smart-tag', 'SmartTagsController@getSmartTags')->name('get-smart-tag');
Route::get('/smart-tag-edit/{smartTag}', 'SmartTagsController@editSmartTag');
Route::post('/update-smart-tag/{smartTag}', 'SmartTagsController@updateSmartTag');
Route::get('/smart-tag-delete-check/{smartTag}', 'SmartTagsController@deleteSmartTag');

//VENTURE STATUSES
Route::get('create-venture-status','VentureStatusController@createVentureStatus')->name('create-venture-status');
Route::get('venture-status-index','VentureStatusController@ventureStatusIndex')->name('venture-status-index');
Route::post('store-venture-status','VentureStatusController@storeVentureStatus')->name('store-venture-status');
Route::get('venture-status', 'VentureStatusController@ventureStatusIndex')->name('venture-status');
Route::get('get-venture-status', 'VentureStatusController@getVentureStatuses')->name('get-venture-status');
Route::get('/venture-status-edit/{ventureStatus}', 'VentureStatusController@editVentureStatus');
Route::post('/update-venture-status/{ventureStatus}', 'VentureStatusController@updateVentureStatus');
Route::get('/venture-status-delete-check/{ventureStatus}', 'VentureStatusController@deleteVentureStatus');

//VENTURE TYPES
Route::get('create-venture-type','VentureTypeController@createVentureType')->name('create-venture-type');
Route::get('venture-type-index','VentureTypeController@ventureTypeIndex')->name('venture-type-index');
Route::post('store-venture-type','VentureTypeController@storeVentureType')->name('store-venture-type');
Route::get('venture-type', 'VentureTypeController@ventureTypeIndex');
Route::get('get-venture-type', 'VentureTypeController@getVentureTypes')->name('get-venture-type');
Route::get('/venture-type-edit/{ventureType}', 'VentureController@editVentureType');
Route::post('/update-venture-type/{ventureType}', 'VentureController@updateVentureType');
Route::get('/venture-type-delete-check/{ventureType}', 'VentureController@deleteVentureType');

//Bootcamper proof of address
Route::post('/store-proof-of-address/{bootcamper}','BootcamperController@bootcamperUploadProofOfAddress');
//Bootcamper upload CIPC
Route::post('/store-cipc/{bootcamper}','BootcamperController@bootcamperUploadCIPC');

//DELETE APPLICANTS
Route::get('/delete-applicants/{id}','UsersController@deleteApplicants');

//CAREERS
Route::get('create-careers-page','SummernoteController@createCareersPage')->name('create-careers-page');
Route::post('store-career','SummernoteController@storeCareers')->name('store-career');
Route::post('store-career-summernote','SummernoteController@storeCareerSummernote')->name('store-career-summernote');
Route::get('/Home/Careers','WelcomeController@careersView');
Route::get('career-index','SummernoteController@careersIndex')->name('career-index');
Route::get('get-careers','SummernoteController@getCareers')->name('get-careers');
Route::get('/careers-delete/{id}','SummernoteController@destroyCareers');
Route::get('/edit-careers/{career}','SummernoteController@editCareers');
Route::post('/update-careers/{career}','SummernoteController@updateCareers');
Route::post('/update-career-summernote/{career}','SummernoteController@updateCareerSummernote');


//BOOTCAMPER CONTACT LOG
Route::post('store-bootcamper-contact-log/{bootcamper}','BootcamperController@storeBootcamperContactLog')->name('store-bootcamper-contact-log');
Route::post('update-bootcamper-status/{bootcamper}','BootcamperController@updateBootcamperStatus')->name('update-bootcamper-status');

//REFERRED BOOTCAMPERS
Route::get('referred-bootcamper-index','BootcamperController@referredBootcampersIndex')->name('referred-bootcamper-index');
Route::get('get-referred-bootcampers','BootcamperController@getReferredBootcampers')->name('get-referred-bootcampers');

//PANEL DECLINED APPLICANTS
Route::get('panel-declined-applicant-index','UsersController@panelDeclinedApplicants');
Route::get('get-panel-declined-applicants','UsersController@getPanelDeclinedApplicants')->name('get-panel-declined-applicants');

//PANEL REFERRED APPLICANTS
Route::get('panel-referred-applicant-index','UsersController@panelReferredApplicants');
Route::get('get-panel-decision-referred-applicants','UsersController@getPanelReferredApplicants')->name('get-panel-decision-referred-applicants');

//Workshop
Route::get('workshop-evaluation-form','WelcomeController@createWorkshopQuestions');
Route::post('upload-workshop-questions','WelcomeController@storeWorkshopQuestions')->name('upload-workshop-questions');
Route::get('workshop-index','ShadowBoardController@workshopIndex');
Route::get('get-workshop','ShadowBoardController@getWorkshopEvaluation')->name('get-workshop');
Route::get('/show-all-evaluation-info/{workshopEvaluation}','ShadowBoardController@viewAllInfo');
Route::get('/delete-user-workshop/{id}','WelcomeController@destroyUserWorkshop');

//PTI
Route::get('create-gallery','PTIController@ptiGallery');
Route::post('gallery-store','PTIController@storeGallery')->name('gallery.store');
Route::get('gallery-index','PTIController@galleryIndex');
Route::get('get-gallery', 'PTIController@getGallery')->name('get-gallery');
Route::get('/home/show-satellite-gallery','PTIController@showPtiGallery');
Route::get('/gallery/{propellaHubGallery}','PTIController@fullGallery');
Route::get('/edit-media-gallery/{propellaHubGallery}','PTIController@editGallery');
Route::post('/update-media-gallery/{propellaHubGallery}','PTIController@updateMediaGallery');
Route::get('/destroy-gallery/{propellaHubGallery}','PTIController@destroyGallery');



//Contact Log
Route::post('/store-contact-log/{user}','ApplicationProcessController@storeApplicantContactLog')->name('store-contact-log');
Route::post('/update-applicant-status/{user}','ApplicationProcessController@updateApplicantStatus');
Route::post('/store-declined-applicant-contact-log/{declinedApplicant}','ApplicationProcessController@storeDeclinedApplicantContactLog')->name('store-declined-applicant-contact-log');
Route::post('/store-declined-bootcamper-contact-log/{declinedBootcamper}','BootcamperController@storeDeclinedBootcamperContactLog')->name('store-declined-bootcamper-contact-log');
//Update applicant chosen category
Route::post('/update-applicant-chosen-category/{user}','ApplicationProcessController@updateApplicantChosenCategory')->name('update-applicant-chosen-category');

//PTI Website
Route::get('/home/AboutUs','PTIController@ptiPage');
Route::get('pti-application-index','PTIController@ptiAapplicantsIndex');
Route::get('get-pti-applicants','PTIController@getPTIApplicants')->name('get-pti-applicants');
Route::get('/home/Services','PTIController@ptiServices');
Route::get('/home/Programme','PTIController@ptiProgramme');
Route::get('/home/Mentors','PTIController@ptiMentors');
Route::get('/home/Apply','PTIController@ptiApply');
Route::get('/home/Contact','PTIController@ptiContact');


//LEARNING CURVES
Route::get('create-learning-curves','LearningCurveController@createLearningCurve');
Route::post('store-learning-curve','LearningCurveController@storeLearningCurve')->name('store-learning-curve');
Route::post('store-learning-curve-summernote','LearningCurveController@storeLearningCurveSummernote')->name('store-learning-curve-summernote');
Route::get('/learning-curves','LearningCurveController@showLearningCurve');
Route::get('/get-detailed-learning-curve/{propellaLearningCurve}','LearningCurveController@showDetailedLearningCurve');
Route::get('get-learning-curves','LearningCurveController@indexLearning');
Route::get('learning-curve-index','LearningCurveController@getLearningCurves')->name('learning-curve-index');
Route::get('/edit-learning-curves/{propellaLearningCurve}','LearningCurveController@editLearningCurves');
Route::post('/update-learning-curves/{propellaLearningCurve}','LearningCurveController@updateLearningCurve');
Route::post('/update-learning-curve-summernote/{propellaLearningCurve}','LearningCurveController@updateLearningSummernote');
Route::get('/destroy-learning-curves/{id}','LearningCurveController@destroyLearningCurve');

//HEDGE SA
Route::get('/hedgeSa','WelcomeController@salutarisPage');

//PROPELLA EMAILER
Route::get('/propella-user-emailer','PropellaUsersEmailerController@propellaUsersIndex');
//Route::get('/all-emailer-categories','PropellaUsersEmailerController@allEmailerCategories');
Route::post('/send-email-to-users','PropellaUsersEmailerController@sendMailToUsers');
Route::post('/send-email-to-visitors','PropellaUsersEmailerController@sendMailToVisitors');
Route::post('/send-email-to-venture','PropellaUsersEmailerController@sendMailToVentures');
Route::post('/send-email-to-company','PropellaUsersEmailerController@sendMailToCompany');





Route::post('/send-email-to-ventures', 'ResourceController@sendMailToVentures');

Route::get('/mentorFormApp/{venture}','UsersController@showMentorOnApp');


//Send email to incubatees
Route::get('send-email', 'ResourceController@sendEmailToSelectedIncubatees');
Route::get('get-all', 'ResourceController@getMailIncubatees')->name('get-mail-incubatees');



Route::get('/feedback-edit/{venture}', 'UsersController@editFeedback');
Route::post('/editFeedback', 'UsersController@updateFeedback');


//Bootcamper Event
Route::get('/bootcamperEventEdit/{eventBootcamper}','BootcamperController@bootcamperEventEdit');

Route::post('/updateBootcaperEvent/{eventBootcamper}','BootcamperController@updateEventBootcamper');

//Edit incubatee event
Route::post('/updateIncubateeEvent/{eventIncubatee}','IncubateeController@updateEventIncubatee');

//Incubatee de registered events
Route::post('/update-deregistered-events/{incubateeEventDeregister}','IncubateeController@updateEventDeRegisterIncubatee');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Enquiries
Route::get('/enquiry-category-index', 'EnquiryController@enquiryCategoryIndex');
Route::get('get-enquiry-categories', 'EnquiryController@getEnquiryCategories')->name('get-enquiry-categories');
Route::get('/unassigned-enquiry-index', 'EnquiryController@unassignedEnquiryIndex');
Route::get('get-unassigned-enquiries', 'EnquiryController@getUnassignedEnquiries')->name('get-unassigned-enquiries');
Route::get('/pre-assigned-enquiry-index', 'EnquiryController@preAssignedEnquiryIndex');
Route::get('get-pre-assigned-enquiries', 'EnquiryController@getPreAssignedEnquiries')->name('get-pre-assigned-enquiries');
Route::get('/assigned-enquiry-index', 'EnquiryController@assignedEnquiryIndex');
Route::get('get-assigned-enquiries', 'EnquiryController@getAssignedEnquiries')->name('get-assigned-enquiries');
Route::post('store-enquiry-category', 'EnquiryController@storeEnquiryCategory');
Route::post('send-enquiry', 'EnquiryController@sendEnquiry');
Route::get('/show-enquiry-category-enquiries/{enquiryCategory}', 'EnquiryController@adminViewEnquiryCategoryEnquiries');
Route::get('/edit-enquiry-category/{enquiryCategory}', 'EnquiryController@adminEditEnquiryCategory');
Route::post('/update-enquiry-category/{enquiryCategory}', 'EnquiryController@adminUpdateEnquiryCategory');
Route::get('/show-enquiry/{enquiry}', 'EnquiryController@adminViewEnquiry');
Route::get('/pre-assign-enquiry/{enquiry}', 'EnquiryController@adminPreAssignEnquiryView');
Route::post('/admin-store-pre-assigned-enquiry/{enquiry}', 'EnquiryController@adminStorePreAssignedEnquiry');
Route::get('/assign-enquiry-view/{enquiry}', 'EnquiryController@adminAssignEnquiryView');
Route::post('/admin-assign-enquiry/{enquiry}', 'EnquiryController@adminAssignGeneralEnquiry');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Venture
Route::post('/storeVentureShadowboardAnswers', 'UsersController@storeVentureShadowboardAnswers');
//Mentor
Route::post('/storeMentorShadowBoardAnswers', 'UsersController@storeMentorShadowBoardAnswers');
Route::post('/storeMentorShadowComment/{mentorVentureShadowboard}','UsersController@storeMentorShadowBoardComments');

Route::get('/venture-form-show/{venture}','UsersController@showVentureForm');

Route::get('/venture-shadowboard-form/{mentorVentureShadowboard}','UsersController@ventureShadowboardForm');

Route::get('/mentor-shadow-board-form/{mentorVentureShadowboard}','UsersController@mentorShadowboardForm');




Route::get('/venture-feedback-show/{venture}','UsersController@showVentureFeedback');

Route::get('/mentor-feedback/{mentor}','UsersController@showMentorFeedback');


//Get Mentor Form
Route::get('/mentor-venture-shadow/{user}','UsersController@mentorVentureShadowIndex');
Route::get('/get-venture-mentor-shadow/{mentor}', 'UsersController@getMentorVentureShadow');
Route::get('/delete-shadow-board/{mentorVentureShadowboard}','UsersController@destroyShadowBoard');

//Venture shadow
Route::get('/venture-shadow/{venture}','UsersController@ventureShadowboardIndex');
Route::get('/get-venture-shadowboards/{venture}', 'UsersController@getVentureShadowboards');

Route::get('/delete-incubatees/{id}','UsersController@deleteIncubatees');

Route::get('/mentor-form-show/{venture}','UsersController@showMentorForm');

Route::get('/mentorForm/{venture}','UsersController@showFormMentor');

Route::get('get-mentors', 'UsersController@getMentors')->name('get-mentors');

Route::get('/get-venture-assigned-to-mentor/{mentor}', 'UsersController@getVentureAssigned');
Route::get('/show-mentor-venture-content/{venture}', 'UsersController@showMentorVentureContent');
Route::get('/view-mentor-venture-shadowboard-results/{mentorVentureShadowboard}', 'UsersController@adminViewMentorVentureShadowBoardResults');


Route::get('/delete-mentor/{id}','UsersController@deleteMentor');
//Shadow board
Route::get('createShadowQuestionCategory', 'ShadowBoardController@createQuestionCategory');
Route::post('storeShadowCategory','ShadowBoardController@storeCategory')->name('store');
Route::get('getShadowIndex','ShadowBoardController@indexShadowCategory');
Route::get('getShadowCategory','ShadowBoardController@getShadowCategory')->name('getShadowCategory');
Route::get('editBoardCategory/{boardQuestionCategory}','ShadowBoardController@editShadowCategory');
Route::post('/updateShadowCategory/{boardQuestionCategory}','ShadowBoardController@updateCategory');
Route::get('/categoryDel/{id}','ShadowBoardController@categoryDestroy');

Route::get('uploadShadowQuestion','ShadowBoardController@createShadowQuestions');
Route::post('upload-shadow-questions','ShadowBoardController@storeShadowQuestions')->name('upload-shadow-questions');
Route::get('getShadowQuestionIndex','ShadowBoardController@shadowQuestionIndex');
Route::get('get-shadow-questions','ShadowBoardController@getShadowQuestions')->name('get-shadow-questions');
Route::get('editBoardQuestions/{shadowBoardQuestion}','ShadowBoardController@editShadowQuestion');
Route::post('/updateShadowQuestion/{shadowBoardQuestion}','ShadowBoardController@updateShadowQuestion');
Route::get('/destroyQuestions/{id}','ShadowBoardController@destroyShadowQuestion');


Route::post('/assign-venture-to-mentor', 'UsersController@assignVenturesToMentor');
Route::post('/assign-shadow-boards', 'UsersController@adminAssignShadowBoard');
Route::get('/mentor-show/{user}', 'UsersController@showMentorInfo');

//Assign incubatee to venture
Route::post('/assign-incubatee-to-venture','VentureController@assignIncubateeToVenture');


Route::get('/mentorVenture', 'UsersController@getVentureMentors');
Route::get('/show/{venture}','UsersController@incMentorIndex');
Route::get('/mentor/{venture}','UsersController@getIncubateesMentor');


Route::get('create-event-venue', 'EventController@createEventVenue')->name('create-event-venue');
Route::post('/venue-event-store', 'EventController@storeEventVenue');
Route::get('/get-venues-index', 'EventController@indexVenue');
Route::get('/get-all-event-venues', 'EventController@getVenues');
Route::get('/edit-venue-event/{eventVenue}', 'EventController@editEventVenue');
Route::post('/update-edit-venue/{eventVenue}', 'EventController@updateVenueEvent');
Route::get('/delete-event-venue/{eventVenue}','EventController@deleteEventVenue');




Route::get('create-category', 'ResourceController@createResourceCategory')->name('create-category');
Route::post('store-resource-category','ResourceController@storeCategory')->name('store-resource-category');
Route::get('indexCategory', 'ResourceController@indexCategory');
Route::get('getCategory', 'ResourceController@getCategory')->name('category-name');
Route::get('/destroyRC/{resourceCategory}', 'ResourceController@destroyResourceCategory');
Route::get('editCategory/{resourceCategory}', 'ResourceController@editCategory');
Route::post('/updateCategory/{resourceCategory}', 'ResourceController@updateCategory');

//Resources




Route::post('send-bulk-email-to-ventures', 'ResourceController@sendBulkEmailToVenture');
Route::post('embed-image-for-venture', 'ResourceController@uploadEmbedsForVenture');

Route::get('addResources','ResourceController@createResource');
Route::post('storeResourceFunding', 'ResourceController@storeResourceFunding')->name('store-resource-funding');
Route::post('storeResource', 'ResourceController@storeDoc')->name('storeResource');

Route::get('resourceIndex', 'ResourceController@index');
Route::get('/get-funding', 'ResourceController@getIndex');

Route::get('/edit-resource/{fundingResource}', 'ResourceController@edit');

Route::post('/update-resource/{fundingResource}', 'ResourceController@updateFunding');

Route::post('/update-resource-funding/{fundingResource}', 'ResourceController@updateResourceFunding');

Route::get('/funding-del/{id}','ResourceController@fundingDestroy');

Route::post('/update-funding-summernote/{fundingResource}', 'ResourceController@updateFundingSummernote');

//Route::get('/get-documents', 'ResourceController@documentsIndex')->name('getDoc');

Route::get('/get-documents/{fundingResource}', 'ResourceController@documentsIndex');
Route::get('/document-del/{id}','ResourceController@documentDestroy');

//Trade Show
Route::get('tradeShow', 'ResourceController@tradeShowIndex');
// Funding
Route::get('fundings', 'ResourceController@fundingsIndex');
//Grant
Route::get('grant', 'ResourceController@grantIndex');
//Competition
Route::get('competition', 'ResourceController@competitionIndex');
//Crowd
Route::get('crowd', 'ResourceController@crowdIndex');

Route::get('/download-file/{id}','ResourceController@downloadFile');

Route::get('/show-all-info/{fundingResource}', 'ResourceController@showAll');
//Events
Route::get('resource-events','ResourceController@eventsIndex');


//emailer
Route::get('allIncubatees', 'ResourceController@allIncubatees');

Route::get('get-all-incubatees', 'ResourceController@getAllIncubatees')->name('get-all-incubatees');

//Route::post('pdf-store','ResourceController@storePDF')->name('pdf.store');





//BotMan

Route::get('newPage','WelcomeController@newPage');


Route::get('/Home/botManChat', function () {
    return view('/home.botManChat');
});

Route::match(['get', 'post'], '/botman', 'BotManController@handle');


Route::get('landingPage','WelcomeController@landingPage');

Route::get('bookingSystem', 'WelcomeController@bookingSystem');
Route::get('/get-booking-system/', 'WelcomeController@getBookingSystem');



//Tablet
Route::get('/show-venue-bookings-on-tablet', 'TabletController@showVenueBookingsOnTablet');
Route::get('/get-venue-all-bookings-on-tablet', 'TabletController@geAllVenuesOnTablet');
Route::get('/show-venue-bookings-on-tablet', 'TabletController@indexTablet');




//summernote form

Route::get('/summernote', 'SummernoteController@summernote');
Route::get('/blog/create', 'SummernoteController@CreateBlog');


Route::get('/edit-blog/{blog}', 'SummernoteController@editBlog');


Route::post('/update-blog/{blog}', 'SummernoteController@updateBlog');
Route::post('/update-blog-one/{blog}', 'SummernoteController@updateBlogOne');

Route::get('/download-pdf/{id}','SummernoteController@downloadFilePdf');

Route::get('/download-id-document/{id}','IncubateeController@downloadIdDocument');


Route::get('/blog/{title}', ['as'=>'blog.show', 'uses' => 'SummernoteController@showBlogSummernote']);

//Get blog summernote
Route::get('get-blog', 'SummernoteController@getBlogs')->name('get-blog');

//Delet blog
Route::get('/blog-delete/{id}','SummernoteController@destroyBlog');

//
Route::get('blog-index', 'SummernoteController@indexBlog');

//summernote store route
Route::post('/summernote_display','SummernoteController@store')->name('summernotePersist');

//summernote display route
Route::get('/summernote_display','SummernoteController@show')->name('summernoteDispay');

//VIEW
Route::get('/blogs', 'SummernoteController@index');


Route::post('blog-store', 'SummernoteController@storeBlog')->name('blog.store');


Route::get('blog', 'BlogController@create');






Route::get('ict-venture', 'VentureController@ictOnlyIndex');
Route::get('get-ICT', 'VentureController@getICTOnly')->name('get-ICT');

Route::get('ind-only-venture', 'VentureController@indOnlyIndex');
Route::get('get-IND', 'VentureController@getINDOnly')->name('get-IND');



//Route::get('/delete-applicants','WelcomeController@deleteApplicants');
Route::group(['middleware' => 'web'], function () {


        Route::get('/reports', 'HomeController@reports');


    Route::get('/question/delete/{question}', 'QuestionController@destroyQuestion');

    //QuestionCategory
    Route::get('questionCategory', 'QuestionController@createQuestionCategory');
    Route::post('storeCategory', 'QuestionController@storeQuestionCategory')->name('store-category');
    Route::get('edit-question-category/{QuestionsCategory}', 'QuestionController@editQuestionCategory');
    Route::get('categoryIndex', 'QuestionController@categoryIndex');
    Route::get('get-category', 'QuestionController@getCategory')->name('get-categories');
    Route::post('/update-application-category/{QuestionsCategory}', 'QuestionController@updateCategory');
    Route::get('/category/delete/{id}', 'QuestionController@destroyCategory');
    Route::get('upload-questions', 'QuestionController@createQuestions');

    //Application Process

    Route::post('upload-questions', 'QuestionController@storeQuestions')->name('store-questions');
    Route::get('get-questions', 'QuestionController@getQuestions')->name('get-questions');
    Route::get('questionIndex', 'QuestionController@questionIndex');
    Route::get('editQuestions/{Question}', 'QuestionController@editQuestions');
    Route::post('/updateQuestion/{Question}', 'QuestionController@updateQuestions');
    Route::get('/question/delete/{id}', 'QuestionController@destroyQuestion');

    //Roles
    Route::get('createRole', 'RoleController@createRole');
    Route::post('storeRole', 'RoleController@storeRole')->name('store-role');
    Route::get('indexRole', 'RoleController@indexRole');
    Route::get('get-roles', 'RoleController@getRoles')->name('get-roles');
    Route::get('/role/delete/{role}', 'RoleController@destroyRole');
    Route::get('editRole/{role}', 'RoleController@editRole');
    Route::post('/updateRole/{role}', 'RoleController@updateRole');

    //EDIT DECLINED INFO
    Route::get('/declined-applicant-info/{declinedApplicant}','UsersController@editDeclinedApplicantBasic');
    Route::post('/update-declined-applicant-update-info/{declinedApplicant}','UsersController@declinedApplicantUpdateBasic');

    //Edit referred info
    Route::get('/referred-applicant-info/{propellaReferredApplicant}','UsersController@editReferredpplicantBasic');
    Route::post('/update-referred-applicant-info/{propellaReferredApplicant}','UsersController@referredApplicantUpdateBasic');

    //Bootcamper
    Route::get('/bootcamper-edit-basic-info/{user}', 'UsersController@editBootcamperBasicInfo');

    Route::get('/bootcamper-edit-basic/{bootcamper}', 'UsersController@editBootcamperBasic');
    Route::post('/bootcamper-update-basic/{bootcamper}', 'UsersController@bootcamperUpdateBasic');



    Route::post('/bootcamper-update-basic-info/{user}', 'UsersController@bootcamperUpdateBasicInfo');

    Route::get('/applicant-edit-application/{user}', 'UsersController@applicantEditApplication');
    Route::get('/applicant-edit-basic-info/{user}', 'UsersController@applicantEditBasicInfo');
    Route::post('/applicant-update-basic-info/{user}', 'UsersController@applicantUpdateBasicInfo');
    Route::get('/destroyOwnApplication/{user}', 'UsersController@applicantDeleteApplication');


    Route::get('/home', 'HomeController@index')->name('home');
    Auth::routes(['verify' => true]);
    /**
     * Bulk Mailer Routes
     */
    Route::get('incubatee-bulk-mailer-index', 'BulkMailerController@allUsersIndex');
    Route::get('get-all-users-mailer', 'BulkMailerController@getAllUsersMailer');
    Route::get('send-email-to-venture','BulkMailerController@sendEmailToVentures');


    Route::get('emailerVentureIndex', 'BulkMailerController@allVentureIndex');

    Route::get('get-AllVenture', 'BulkMailerController@getAllVentures')->name('get-allVentures');


    Route::post('send-bulk-email-to-visitors','BulkMailerController@sendBulkEmailToVisitor');

    Route::get('bulk-mailer', 'BulkMailerController@index');
    Route::get('get-company-employees-mailer', 'BulkMailerController@getContacts');
    Route::post('send-bulk-email', 'BulkMailerController@sendBulkEmail');
    Route::post('embed-image', 'BulkMailerController@uploadEmbeds');
    Route::get('visitor-bulk-mailer', 'BulkMailerController@visitorIndex');
    Route::get('get-company-visitors-mailer', 'BulkMailerController@getVisitorsContacts');
    Route::post('send-visitor-bulk-email', 'BulkMailerController@sendVisitorBulkEmail');
    Route::post('visitor-embed-image', 'BulkMailerController@uploadVisitorEmbeds');
    Route::post('send-bulk-email-to-ventures', 'BulkMailerController@sendBulkEmailToVenture');
    Route::post('embed-image-for-venture', 'BulkMailerController@uploadEmbedsForVenture');
    Route::post('send-users-bulk-email', 'BulkMailerController@sendUsersBulkEmail');
    Route::post('users-embed-image', 'BulkMailerController@uploadUsersEmbeds');



    /*System Users Routes*/
    Route::get('/user-del/{id}','UsersController@userDestroy');
    Route::get('users-index', 'UsersController@indexUser');
    Route::get('get-users', 'UsersController@getUsers')->name('get-users');
    Route::get('create', 'UsersController@createUser');
    Route::post('/users/create-user', 'UsersController@store')->name('add-users');
    Route::get('edit-user/{user}', 'UsersController@editUser');
    Route::post('update-edit-user/{user}', 'UsersController@updateUser')->name('update-user');
    Route::post('/user-store-event-via-public-event-page', 'UsersController@storeUserEventViaPublicEventPage')->name('user.storeUserEventViaPublicEventPage');
    Route::get('/users/show-events/{user}', 'UsersController@userEvents');
    Route::post('/users/event-deregister', 'UsersController@deregisterUser')->name('user.deregister');
    Route::post('/users/store-event-via-user-event-page', 'UsersController@storeUserEventViaUserEventPage')->name('user.storeEventViaUserEventPage');
    Route::get('/visitors/clerk/user-event-edit/{user}', 'VisitorController@editUserEventView');
    Route::post('/visitors/clerk/update-user-event', 'VisitorController@updateUserEvent')->name('update-user-event');
    Route::get('/export-users', 'DownloadExcelController@downloadUsers');


    /**
     * Application Process Routes
     */
    Route::resource('question-category', 'ApplicationProcessController');
    //Questions Routes
    Route::get('view-questions', 'HomeController@questionsIndex');
    Route::get('/question-edit/{question}', 'HomeController@editQuestions');
    Route::post('update-question-edit/{question}', 'HomeController@updateQuestions');
    Route::get('/question-delete/{question}', 'HomeController@destroyQuestions');

    //Basic Info page
    Route::get('/Application-Process/Basic-info', 'ApplicationProcessController@index');
    Route::post('/Application-Process/Basic-info', 'ApplicationProcessController@storeBasicInfo')->name('Application-Process.storeBasicInfo');

    Route::get('/Application-Process/rap-application', 'ApplicationProcessController@rapApplication');
    Route::post('/Application-Process/rap-application', 'ApplicationProcessController@storeRapApplication')->name('Application-Process.storeRapApplication');

    Route::get('/Application-Process/ind-application', 'ApplicationProcessController@indApplication');
    Route::post('/Application-Process/ind-application', 'ApplicationProcessController@storeIndApplication')->name('Application-Process.storeIndApplication');

    Route::get('/Application-Process/pti-ict-application', 'ApplicationProcessController@ictApplication');
    Route::post('/Application-Process/pti-ict-application', 'ApplicationProcessController@storeIctApplication')->name('Application-Process.storeIctApplication');

    Route::get('/Application-Process/pbi-ict-application', 'ApplicationProcessController@pbiApplication');
    Route::post('/Application-Process/pbi-ict-application', 'ApplicationProcessController@storePbiApplication')->name('Application-Process.storePbiApplication');

    Route::get('/Application-Process/Basic-info-bootcamp', 'ApplicationProcessController@indexBootcamp');
    Route::post('/Application-Process/Basic-info-bootcamp', 'ApplicationProcessController@storeBasicInfoBootcamp')->name('Application-Process.storeBasicInfoBootcamp');

    //Choose Category page
    Route::get('/Application-Process/question-category', 'ApplicationProcessController@category');

    //View Questions page
    Route::get('/Application-Process/questions/{user}', 'ApplicationProcessController@loadQuestions');

    //Answers Upload Route
    Route::post('/Application-Process/questions', 'ApplicationProcessController@storeAnswers');


    //Thank you page
    Route::get('/thank-you-guest', function () {
        return view('Application-Process.Thanks-page');
    });


    /**
     * Contact Logs
     */
    Route::resource('contact-logs', 'ContactLogController');
    Route::get('get-contact-logs', 'ContactLogController@getContactLogs')->name('get-contact-logs');
    Route::get('/contact-logs/delete/{contact_log}', 'ContactLogController@destroy');
    Route::get('/contact-logs/show/{contact_log}', 'ContactLogController@show');
    Route::get('add-activity-log', 'ContactLogController@addActivityLog');
    Route::get('add-activity-log-for-contact/{companyEmployee}', 'ContactLogController@addActivityLogCompanyEmployee');
    /**
     * Campaign Manager routes
     */
    Route::resource('companies', 'CompanyController');
    Route::get('get-companies', 'CompanyController@getCompanies')->name('get-companies');
    Route::get('/company/delete/{company}', 'CompanyController@destroy');
    Route::get('/company/show/{company}', 'CompanyController@show');
    Route::get('/company/{company}', 'CompanyController@edit');
    Route::get('get-specific-company-employees/{company}', 'CompanyController@getSpecificCompanyEmployees');
    Route::post('company/{company}/update', 'CompanyController@update');
    Route::get('company-create', 'CompanyController@create');
    Route::get('/export-companies', 'DownloadExcelController@downloadCompanies');
    Route::get('/export-company-details/{company}', 'DownloadExcelController@downloadCompanyDetails');
    Route::get('/export-company-employees/{company}', 'DownloadExcelController@downloadCompanyEmployees');

    /**
     * Company employees routes
     */
    Route::resource('company-employees', 'CompanyEmployeeController');
    Route::get('get-company-employees', 'CompanyEmployeeController@getCompanyEmployee')->name('get-company-employees');
    Route::get('/company-employee/delete/{companyEmployee}', 'CompanyEmployeeController@destroy');
    Route::get('company-employee/show/{companyEmployee}', 'CompanyEmployeeController@show');
    Route::get('/company-employee/{companyEmployee}', 'CompanyEmployeeController@edit');
    Route::post('/company-employee/{companyEmployee}/update', 'CompanyEmployeeController@update');
    Route::get('company-employee-create/{company}', 'CompanyEmployeeController@create');


    /*Booking Venue Routes*/
    Route::get('/get-venues', 'BookingVenueController@index');
    Route::get('/get-all-venues', 'BookingVenueController@getVenues');
    Route::get('create-venue', 'BookingVenueController@createVenue')->name('create-venue');
    Route::post('/venue-store', 'BookingVenueController@storeVenue');
    Route::get('/edit-venue/{propellaVenue}', 'BookingVenueController@editVenue');
    Route::post('/update-venue/{propellaVenue}', 'BookingVenueController@updateVenue');
    Route::get('/venue-delete-check/{propellaVenue}', 'BookingVenueController@destroyVenue');
    Route::get('booking-calendar', 'BookingVenueController@bookingCalendar');

    /*this routes for user booking*/
    Route::get('/check-venue-bookings/{venue}', 'BookingVenueController@checkVenueBookingTablet');

    Route::get('/show-booking/{user}', 'BookingVenueController@showVenues');
    Route::get('/venue-booking/{venue}', 'BookingVenueController@venueBooking');
    Route::get('/get-venue-bookings/{venue}', 'BookingVenueController@getVenueBooking');
    Route::get('/book-venue/{venue}', 'BookingVenueController@createVenueBooking');
    Route::post('/venue-book-store', 'BookingVenueController@storeVenueBooking');
    Route::get('/show-venue-bookings/{venue}', 'BookingVenueController@showVenueBookings');
    Route::get('/get-venue-all-bookings/{venue}', 'BookingVenueController@getAllVenueBooking');
    Route::get('/edit-venue-booking/{userBooking}', 'BookingVenueController@editVenueBooking');
    Route::post('/update-edit-venue-booking/{userBooking}', 'BookingVenueController@updateVenueBooking');
    Route::get('/cancel-delete-check/{id}', 'BookingVenueController@cancelBooking');

    /*Blog Route*/
    Route::post('section-number-store', 'BlogController@storeSectionNumber')->name('section.number.store');
    Route::post('paragraph-with-section-number-store', 'BlogController@storeParagraphAndSection')->name('paragraph.section.number.store');
    Route::post('paragraph-store', 'BlogController@storeParagraph')->name('paragraph.store');
    Route::post('section-store', 'BlogController@storeSection')->name('section.store');

    /*Section and Paragraph Routes*/
    Route::get('get-section/{blog}', 'BlogController@getSection');
    Route::get('/edit-section/{blog}', 'BlogController@editSection');
    Route::post('/update-section/{blog}', 'BlogController@updateSection');
    Route::get('get-paragraph/{blog}', 'BlogController@getParagraph');
    Route::get('/edit-paragraph/{blog}', 'BlogController@editSection');
    Route::post('/update-paragraph/{blogParagraph}', 'BlogController@updateParagraph');
    Route::get('/Blogs/show-blog/{blog}', 'WelcomeController@showBlog');


    //ALUMNI
    Route::get('alumni-incubatees', 'UsersController@alumniIncubatees');
    Route::get('get-alumni-incubatees', 'UsersController@getAlumniIncubatees')->name('get-alumni-incubatees');

    /*Incubatee Routes*/
    Route::get('incubatee-stages', 'UsersController@incubateeStagesIndex');
    Route::get('get-incubatee-stages', 'UsersController@getIncubateeStages')->name('get-incubatee-stages');
    Route::get('create-incubatee-stage', 'UsersController@createIncubateeStage');
    Route::post('store-incubatee-stage', 'UsersController@storeIncubateeStage');
    Route::get('/incubatee-stage-edit/{incubateeStage}', 'UsersController@editIncubateeStage');
    Route::post('/update-incubatee-stage/{incubateeStage}', 'UsersController@updateIncubateeStage');
    Route::get('incubatees', 'UsersController@incubateesIndex');
    Route::get('create-incubatee', 'IncubateeController@create');
    Route::post('incubatee-store', 'IncubateeController@store')->name('incubatee.store');
    /*venture in incubateeController*/
    Route::get('/incubatee-create-venture/{incubatee}', 'IncubateeController@incCreateVenture');
    Route::post('/venture-store-business/{incubatee}', 'IncubateeController@storeVenture');
    Route::post('/venture-store-media/{venture}', 'IncubateeController@storeincVentureMedia');
    Route::get('users/incubatees/admin-incubatees/incubatee-edit-venture/{venture}', 'IncubateeController@editIncVenture');
    Route::post('/incubatee-update-venture/{venture}', 'IncubateeController@updateVenture');
    /*/venture in incubateeController*/
    Route::post('incubatee-store-media/{incubatee}', 'IncubateeController@storeMedia');
    Route::get('/incubatee-edit/{incubatee}', 'IncubateeController@edit');
    Route::get('/incubatee-events/{incubatee}', 'UsersController@adminViewIncubateeEvents');
    Route::get('/incubatee-evaluations/{incubatee}', 'UsersController@adminViewIncubateeEvaluations');
    Route::get('/get-incubatee-registered-events-via-edit/{incubatee}', 'UsersController@adminGetIncubateeRegisteredEvents');
    Route::get('/get-incubatee-de-registered-events-via-edit/{incubatee}', 'UsersController@adminGetIncubateeDeRegisteredEvents');
    Route::get('/admin/incubatee-registered-event-edit/{eventIncubatee}', 'UsersController@adminEditIncubateeRegisteredEvent');
    Route::post('/incubatee-update-personal/{user}', 'IncubateeController@updatePersonal');
    Route::post('/incubatee-update-media/{user}', 'IncubateeController@updateIncubateeMedia');
    Route::get('/incubatee/delete/{incubatee}', 'UsersController@destroy');
    Route::get('/incubatee-account-overview/{incubatee}', 'IncubateeController@adminViewIncubateeAccountOverview');
    Route::post('/store-disk-results/{user}','IncubateeController@updateDeskAssessmentResults');
    Route::post('/store-strength-results/{user}','IncubateeController@updateStrengthResults');
    Route::post('/store-talent-dynamics-results/{user}','IncubateeController@updateTalentDynamicsResults');
    /*Administrator routes*/

    //Declined applicant account overview
    Route::get('/declined-applicant-account-overview/{declinedApplicant}','UsersController@adminViewDeclinedApplicantAccountOverview');

    //EXITED
    Route::get('exited-venture', 'VentureController@exitedIndex');
    Route::get('get-exited', 'VentureController@getExited')->name('get-exited');


    Route::get('propella-ventures','VentureController@allPropellaVentures');
    Route::get('all-propella-ventures','VentureController@gePropellatVenture')->name('all-propella-ventures');

    Route::get('/delete-declined-app/{declinedApplicant}','ApplicationProcessController@deleteDeclinedApplicant');

    Route::get('/delete-referred-app/{propellaReferredApplicant}','ApplicationProcessController@deleteReferredApplicants');

    Route::get('/delete-declined-bootcamper/{declinedBootcamper}','BootcamperController@deleteDeclinedBootcamper');
    /*Create venture*/
    Route::get('venture-category', 'VentureController@ventureCategoryIndex');
    Route::get('get-venture-category', 'VentureController@getVentureCategories')->name('get-venture-category');
    Route::get('create-venture-category', 'VentureController@createVentureCategory');
    Route::post('store-venture-category', 'VentureController@storeVentureCategory');
    Route::get('/venture-category-edit/{ventureCategory}', 'VentureController@editVentureCategory');
    Route::post('/update-venture-category/{ventureCategory}', 'VentureController@updateVentureCategory');
    Route::get('/venture-category-delete-check/{ventureCategory}', 'VentureController@deleteVentureCategory');
    Route::get('venture', 'VentureController@ventureIndex');
    Route::get('/incubatee-index/{venture}', 'VentureController@incubateeIndex');
    Route::get('alumni-venture', 'VentureController@alumniIndex');
    Route::get('get-venture', 'VentureController@getVenture')->name('get-venture');
    Route::get('/get-venture-incubatees/{venture}', 'VentureController@getIncubatee');
    Route::get('get-alumni', 'VentureController@getAlumni')->name('get-alumni');
    Route::get('create-venture', 'VentureController@createVenture');
    Route::post('incubatee-store-business/', 'VentureController@storeBusiness');
    Route::post('/venture-store-media/{venture}', 'VentureController@storeVentureMedia');
    Route::get('/venture-edit/{venture}', 'VentureController@editVenture');
    Route::post('/venture-update/{venture}', 'VentureController@updateBusiness');
    Route::post('/venture-update-media/{venture}', 'VentureController@updateMedia');
    Route::get('/Venture-delete-check/{id}', 'VentureController@destroyVenture');
    Route::get('/download-venture-document/{id}', 'VentureController@downloadVentureDocument');
    Route::post('/upload-venture-client-contract/{venture}', 'VentureController@uploadVentureClientContract');
    Route::post('/upload-venture-tax-clearance/{venture}', 'VentureController@uploadVentureTaxClearance');
    Route::post('/upload-venture-bbbee-certificate/{venture}', 'VentureController@uploadVentureBbbeeCertificate');
    Route::post('/upload-venture-ck-document/{venture}', 'VentureController@uploadVentureCkDocuments');
    Route::post('/upload-venture-management-account/{venture}', 'VentureController@uploadVentureManagementAccount');
    Route::post('/upload-venture-dormant-affidavit/{venture}', 'VentureController@uploadVentureDormantAffidavit');
    Route::post('/upload-venture-infographic/{venture}', 'VentureController@uploadVentureInfographic');
    Route::get('/delete-venture-document/{id}', 'VentureController@deleteVentureDocument');
    Route::get('/export-venture-details/{venture}', 'DownloadExcelController@downloadVentureInformation');
    Route::get('/export-venture-incubatees/{venture}', 'DownloadExcelController@downloadVentureIncubatees');
    Route::get('/export-venture-employees/{venture}', 'DownloadExcelController@downloadVentureEmployees');
    Route::get('/venture-panel-interview-category-index', 'VentureController@venturePanelInterviewCategoryIndex');
    Route::get('get-venture-panel-interview-categories', 'VentureController@getVenturePanelInterviewCategories')->name('get-venture-panel-interview-categories');
    Route::post('/admin-create-venture-panel-interview-category', 'VentureController@adminCreateVenturePanelInterviewCategory');
    Route::get('/venture-panel-access-window/{venture}', 'VentureController@adminViewVenturePanelAccessWindow');
    Route::post('/admin-create-venture-panel-interview/{venture}', 'VentureController@adminStoreVenturePanelInterview');
    Route::post('/upload-venture-spreadsheet/{venture}','VentureController@uploadVentureSpreadsheet');
    Route::post('/incubatee-upload-venture-spreadsheet/{venture}','VentureController@incubateeuploadVentureSpreadsheet');
    Route::get('ventures_oct_2021','VentureController@meVenturesOctober2021Index')->name('ventures_oct_2021');
    Route::get('get-venture-oct-2021','VentureController@getOctoberMEVenture2021')->name('get-venture-oct-2021');
    Route::get('/view-spreadsheet/{venture}','VentureController@viewSpreadsheet')->name('view-spreadsheet');
    Route::get('ventures_nov_2021','VentureController@meVenturesNovember2021Index')->name('ventures_nov_2021');
    Route::get('get-venture-nov-2021','VentureController@getNovemberMEVenture2021')->name('get-venture-nov-2021');
    Route::get('ventures_dec_2021','VentureController@meVenturesDec2021Index')->name('ventures_dec_2021');
    Route::get('get-venture-dec-2021','VentureController@getDecMEVenture2021')->name('get-venture-dec-2021');
    Route::get('ventures_jan_2022','VentureController@meVenturesJan2022Index')->name('ventures_jan_2022');
    Route::get('get-venture-jan-2022','VentureController@getJanMEVenture2022')->name('get-venture-jan-2022');
    Route::get('/venture-contact-log/{venture}','VentureController@ventureContactLog')->name('venture-contact-log');
    Route::post('/store-venture-contact-log/{venture}','VentureController@storeVentureContactLog')->name('store-venture-contact-log');
    Route::get('/view-nov-spreadsheet/{venture}','VentureController@viewSpreadsheetNov')->name('view-nov-spreadsheet');
    Route::get('/view-dec-spreadsheet/{venture}','VentureController@viewSpreadsheetDec')->name('view-dec-spreadsheet');
    Route::get('/view-jan-spreadsheet/{venture}','VentureController@viewSpreadsheetJan')->name('view-jan-spreadsheet');
    Route::get('/delete-venture-spreadsheet/{id}','VentureController@deleteVentureSpreadsheet');
    Route::get('venture-feb-2022','VentureController@meVenturesFeb2022Index')->name('venture-feb-2022');
    Route::get('get-venture-feb-2022','VentureController@getFebMEVenture2022')->name('get-venture-feb-2022');
    Route::get('venture-march-2022','VentureController@meVenturesMarch2022Index')->name('venture-march-2022');
    Route::get('get-venture-march-2022','VentureController@getMarchMEVenture2022')->name('get-venture-march-2022');
    Route::get('venture-april-2022','VentureController@meVenturesApril2022Index')->name('venture-april-2022');
    Route::get('get-venture-april-2022','VentureController@getAprilMEVenture2022')->name('get-venture-april-2022');
    Route::get('venture-may-2022','VentureController@meVenturesMay2022Index')->name('venture-may-2022');
    Route::get('get-venture-may-2022','VentureController@getMayMEVenture2022')->name('get-venture-may-2022');
    Route::get('venture-june-2022','VentureController@meVenturesJune2022Index')->name('venture-june-2022');
    Route::get('get-venture-june-2022','VentureController@getJuneMEVenture2022')->name('get-venture-june-2022');
    Route::get('venture-july-2022','VentureController@meVenturesJuly2022Index')->name('venture-jult-2022');
    Route::get('get-venture-july-2022','VentureController@getJulyMEVenture2022')->name('get-venture-july-2022');
    Route::get('venture-aug-2022','VentureController@meVenturesAug2022Index')->name('venture-aug-2022');
    Route::get('get-venture-august-2022','VentureController@getJulyMEVenture2022')->name('get-venture-august-2022');
    Route::get('venture-sept-2022','VentureController@meVentureSept2022Index')->name('venture-sept-2022');
    Route::get('get-venture-sept-2022','VentureController@getSeptMEVenture2022')->name('get-venture-sept-2022');
    Route::get('venture-oct-2022','VentureController@meVentureOct2022Index')->name('venture-oct-2022');
    Route::get('get-venture-oct-2022','VentureController@getOctMEVenture2022')->name('get-venture-oct-2022');
    Route::get('venture-nov-2022','VentureController@meVentureNov2022Index')->name('venture-nov-2022');
    Route::get('get-venture-nov-2022','VentureController@getNovMEVenture2022')->name('get-venture-nov-2022');
    Route::get('venture-dec-2022','VentureController@meVentureDec2022Index')->name('venture-dec-2022');
    Route::get('get-venture-dec-2022','VentureController@getDecMEVenture2022')->name('get-venture-dec-2022');
    Route::get('/view-feb-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetFeb2022')->name('view-feb-2022-spreadsheet');
    Route::get('/view-march-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetMarch2022')->name('view-march-2022-spreadsheet');
    Route::get('/view-april-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetApril2022')->name('view-april-2022-spreadsheet');
    Route::get('/view-may-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetMay2022')->name('view-may-2022-spreadsheet');
    Route::get('/view-june-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetJune2022')->name('view-june-2022-spreadsheet');
    Route::get('/view-july-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetJuly2022')->name('view-july-2022-spreadsheet');
    Route::get('/view-august-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetAugust2022')->name('view-august-2022-spreadsheet');
    Route::get('/view-september-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetSeptember2022')->name('view-september-2022-spreadsheet');
    Route::get('/view-october-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetOctober2022')->name('view-october-2022-spreadsheet');
    Route::get('/view-november-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetNovember2022')->name('view-november-2022-spreadsheet');
    Route::get('/view-december-2022-spreadsheet/{venture}','VentureController@viewSpreadsheetDec2022')->name('view-december-2022-spreadsheet');


    /*Incubatee in Venture*/
    Route::get('venture-incubatee/{venture}', 'VentureController@createIncubatee');
    Route::post('/venture-store-incubatee', 'VentureController@StoreIncubatee');
    Route::post('/incubatee-store-media-venture/{incubatee}', 'VentureController@ventureIncStoreMedia');
    Route::get('/venture-edit-incubatee/{user}', 'VentureController@editIncubatee');
    Route::post('/venture-incubatee-update-personal/{user}', 'VentureController@updateVenturePersonal');
    Route::post('/venture-incubatee-update-media/{user}', 'VentureController@updateVentureIncubateeMedia');
    Route::get('/incubatee-delete/{incubatee}', 'VentureController@destroyIncubatee');
    Route::get('/applicants/application-reporting-info', 'UsersController@applicantRep');
    /*
    * Incubatee Employee
    * */
    Route::get('/users/incubatees/admin-job/index', 'IncubateeEmployeeController@index');
    Route::get('/users/incubatees/admin-job/index-get-incEmployee', 'IncubateeEmployeeController@getIncubateeEmployee')->name('get-incEmployee');
    Route::get('/users/incubatees/admin-job/create/{incubatee}', 'IncubateeEmployeeController@createIncubateeEmployee');
    Route::post('/users/incubatees/admin-job/create-add-incEmployee/{incubatee}', 'IncubateeEmployeeController@storeIncubateeEmployee')->name('add-incEmployee');
    Route::get('/users/incubatees/admin-job/edit/{incubateeEmployee}', 'IncubateeEmployeeController@editIncubateeEmployee');
    Route::post('update-edit/{incubateeEmployee}', 'IncubateeEmployeeController@updateIncubateeEmployee');
    Route::get('/delete/incubateeEmployee/{id}', 'IncubateeEmployeeController@destroy');
    Route::post('/incubatee.home/store-event-via-public-event-page', 'IncubateeController@storeIncubateeEventViaPublicEventPage')->name('incubatee.storeIncubateeEventViaPublicEventPage');
    Route::get('/users/incubatee-dashboard/full-calendar/show-events/{incubatee}', 'IncubateeController@incubateeEvents');
    Route::post('/users/incubatee-dashboard/full-calendar/show-events/incubatee-deregister', 'IncubateeController@deregisterIncubatee')->name('incubatee.deregister');
    Route::post('/users/incubatee-dashboard/full-calendar/show-events/store-event-via-incubatee-event-page', 'IncubateeController@storeIncubateeEventViaIncubateeEventPage')->name('incubatee.storeEventViaIncubateeEventPage');
    Route::post('/visitors/clerk/update-incubatee-event', 'WelcomeController@updateIncubateeEvent')->name('update-incubatee-event');
    Route::post('/visitors/clerk/update-bootcamper-event', 'BootcamperController@updateBootcamperEvent')->name('update-bootcamper-event');
    Route::get('/export-all-visitors', 'DownloadExcelController@downloadAllVisitors');


    Route::resource('users', 'UsersController');

    Route::get('mentors', 'UsersController@mentorsIndex');


    Route::get('get-incubatees', 'UsersController@getIncubatees')->name('get-incubatees');
    Route::get('get-mentors', 'UsersController@getMentors')->name('get-mentors');


    //Referred Applicants
    Route::get('referred-applicants','UsersController@referredApplicantsIndex');
    Route::get('get-referred-applicants','UsersController@getReferredApplicants')->name('get-referred-applicants');
    Route::get('referred-applicant-categories','UsersController@referredApplicantCategory')->name('referred-applicant-categories');
    Route::get('get-referred-applicant-categories','UsersController@getReferredApplicantCategories')->name('get-referred-applicant-categories');
    Route::get('/referred-applicant-category-edit/{referredApplicantCategory}','UsersController@referredApplicantCategoryEdit');
    Route::post('/update-referred-applicant-category/{referredApplicantCategory}','UsersController@updateReferredApplicantCategory');
    Route::get('/show-referred-applicant/{propellaReferredApplicant}','UsersController@showReferredApplicantApplication');
    /**
     * Manage Applicants
     */
    Route::post('/update-declined-applicant-category/{declinedApplicantCategory}','UsersController@updateDeclineApplicabrCategory');
    Route::get('/declined-applicant-category-edit/{declinedApplicantCategory}','UsersController@declinedApplicantCategoryEdit');
    Route::get('declined-applicant-categories', 'UsersController@declinedApplicantCategoryIndex');
    Route::get('get-declined-applicant-categories', 'UsersController@getDeclinedApplicantCategories')->name('get-declined-applicant-categories');
    Route::get('applicants', 'UsersController@applicantsIndex');
    Route::get('declined-applicants', 'UsersController@declinedApplicantsIndex');
    Route::get('get-applicants', 'UsersController@getApplicants')->name('get-applicants');
    Route::get('get-declined-applicants', 'UsersController@getDelinedApplicants')->name('get-declined-applicants');
    Route::get('/show-declined-applicant-application/{declinedApplicant}', 'UsersController@showDeclinedApplicantApplication');
    Route::get('/applicant/show/{user}', 'UsersController@showApplicant');
    Route::get('/applicant-approve/{user}', 'UsersController@approveApplicant')->name('applicant-approved');
    Route::get('/applicant-delete/{user}', 'UsersController@deleteApplicant');
    Route::get('/applicant-show/{user}', 'UsersController@showApplicant');
    Route::get('/applicant/show/{applicant}', 'UsersController@applicantRep');
    Route::post('/set-applicant-to-incubatee/{user}', 'UsersController@setApplicantToIncubatee');
    Route::get('/applicant-panel-access-window/{user}', 'UsersController@showApplicantPanelAccessWindow');
    Route::post('/applicant-panel-interview-decline/{user}', 'UsersController@adminDeclineApplicantPanelInterview');
    Route::get('/applicant-and-bootcamp-interviews/{incubatee}','IncubateeController@getApplicantAndIncubateeInterview');

    //Referred applicants
    Route::post('/applicant-panel-interview-referred/{user}','UsersController@adminReferApplicantPanelInterview');

    Route::post('/applicant-panel-interview-decline-but-refer/{user}', 'UsersController@adminDeclineButReferApplicantPanelInterview');
    Route::post('/applicant-panel-interview-approve/{user}', 'UsersController@adminApproveApplicantPanelInterview');
    Route::get('/panelist-show-industrial-applicants', 'UsersController@panelistShowIndustrialApplicants');
    Route::get('/panelist-show-ict-applicants', 'UsersController@panelistShowICTApplicants');
    Route::get('bootcamper-panel-interviews', 'UsersController@adminShowICTPanelInterviews')->name('bootcamper-panel-interviews');
    Route::get('get-bootcamper-panel-interviews', 'UsersController@adminGetICTPanelInterviews')->name('get-bootcamper-panel-interviews');
    Route::get('/get-panelist-applicants/{user}', 'UsersController@panelistGetIndustrialApplicants');
    Route::get('/get-panelist-ict-applicants/{user}', 'UsersController@panelistGetICTApplicants');
    Route::get('/panelist-view-applicant-questions/{user}', 'UsersController@showApplicant');
    Route::get('panelist-view-ict-applicant-questions/{user}', 'UsersController@panelistShowIctApplicant');
    Route::get('/panelist-view-applicant-score-sheet/{applicant}', 'UsersController@showPanelScoreSheet');
    Route::get('/panelist-view-ict-applicant-score-sheet/{bootcamper}', 'UsersController@showICTPanelScoreSheet');
    Route::post('/panelist-save-question-answer-comments', 'UsersController@panelistSaveQuestionAnswerComments');
    Route::post('/panelist-save-ict-question-answer-comments', 'UsersController@panelistSaveIctQuestionAnswerComments');
    Route::post('/panelist-update-question-answer-comments', 'UsersController@panelistUpdateQuestionAnswerComments');
    Route::get('/show-applicant-panelists/{user}', 'UsersController@showApplicantPanelists');
    Route::get('/show-ict-applicant-panelists/{bootcamper}', 'UsersController@showIctApplicantPanelists');
    Route::get('/get-applicant-panelists/{applicant}', 'UsersController@getApplicantPanelists');
    Route::get('/get-ict-applicant-panelists/{bootcamper}', 'UsersController@getIctApplicantPanelists');
    Route::get('/admin-show-ict-panelist-score-sheet/{bootcamperPanelist}', 'UsersController@adminShowIctPanelistScoreSheet');
    Route::get('/admin-show-panelist-score-sheet/{applicantPanelist}', 'UsersController@adminShowPanelistScoreSheet');
    Route::get('/admin-remove-panelist-from-applicant/{applicantPanelist}', 'UsersController@adminRemoveApplicantPanelistFromPanel');
    Route::post('/admin-add-industrial-panelist-from-panel-access-window', 'UsersController@adminAddIndustrialPanelistFromPanelAccessWindow');
    Route::post('/admin-update-panel-interview-details/{user}', 'UsersController@adminUpdatePanelInterviewDetails');
    Route::get('/applicant-next-steps/{user}', 'UsersController@applicantNextStepsView');
    Route::get('/panelist-edit-scoresheet/{applicantPanelist}', 'UsersController@panelistEditScoresheet');
    Route::get('/extract-applicant-panelists-data/{applicant}', 'DownloadExcelController@downlaodApplicantResults');
    Route::get('/declined-applicant-delete/{declinedApplicant}', 'UsersController@adminDeleteDeclinedApplicant');
    Route::post('/admin-create-declined-applicant-category', 'UsersController@adminCreateDeclinedApplicantCategory');
    Route::get('/panelist-show-ventures', 'UsersController@panelistShowVentureInterviews');
    Route::get('/panelist-get-venture-interviews/{user}', 'UsersController@getPanelistVentureInterviews');
    Route::get('/panelist-view-venture-interview-score-sheet/{venturePanelInterview}', 'UsersController@showVentureInterviewPanelScoreSheet');
    Route::post('/panelist-save-venture-interview-question-answer-comments', 'UsersController@panelistSaveVentureInterviewQuestionAnswerComments');
    Route::get('/show-venture-interviews-overview/{venture}', 'UsersController@adminViewVenturePanelInterviewOverview');
    Route::post('/admin-get-venture-panel-interview-overview/{venturePanelInterviewCategory}', 'UsersController@adminGetVentureInterviewOverviewInformation');
    Route::get('/admin-get-panelist-venture-interview-score-sheet/{venturePanelInterview}', 'UsersController@adminGetPanelistVentureInterviewScoreSheet');

    Route::post('/create-referred-applicant-category','UsersController@adminCreateReferredApplicantCategory');

    /*Bootcampers*/
    Route::get('/edit-declined-bootcamper-info/{declinedBootcamper}','BootcamperController@editDeclinedBootcamperInfo');
    Route::post('/update-declined-bootcamper-info/{declinedBootcamper}','BootcamperController@declinedBootcamperUpdateBasicInfo');
    Route::get('bootcampers', 'BootcamperController@bootcampersIndex');
    Route::get('get-bootcampers', 'BootcamperController@getBootcampers')->name('get-bootcampers');
    Route::get('declined-bootcampers', 'BootcamperController@declinedBootcampersIndex');
    Route::get('get-declined-bootcampers', 'BootcamperController@getDeclinedBootcampers')->name('get-declined-bootcampers');
    Route::get('/bootcamper-events/{bootcamper}', 'BootcamperController@showBootcamperEvents');
    Route::get('/get-bootcamper-events/{bootcamper}', 'BootcamperController@getBootcamperEvents');
    Route::post('/admin-register-bootcamper-event', 'BootcamperController@adminRegisterBootcamperEvent');
    Route::post('/bootcamper-accept-event', 'BootcamperController@bootcamperAcceptEvent');
    Route::post('/bootcamper-decline-event', 'BootcamperController@bootcamperDeclineEvent');
    Route::get('/bootcamper-delete/{bootcamper}', 'BootcamperController@deleteBootcamper');
    Route::get('/bootcamper-event-delete/{eventBootcamper}', 'BootcamperController@deleteBootcamperEventRegistration');
    Route::get('/bootcamper-show/{bootcamper}', 'BootcamperController@showBootcamperDetails');
    Route::post('/bootcamper-upload-pitch-video/{bootcamper}', 'BootcamperController@bootcamperUploadPitchVideo');
    Route::get('/declined-bootcamper-account-overview/{declinedBootcamper}', 'BootcamperController@declinedBootcamperAccountOverview');

    //Bootcamper panel access window
    Route::get('/bootcamper-panel-access-window/{bootcamper}','BootcamperController@showBootcamperPanelAccessWindow');
    Route::post('/bootcamper-panel-interview-approve/{bootcamper}','BootcamperController@adminApproveBootcamperPanelInterview');
    Route::post('/bootcamper-panel-interview-decline/{user}','BootcamperController@adminDeclineBootcamperPanelInterview');
    Route::post('/update-panel-interview-details/{bootcamper}','BootcamperController@adminUpdatePanelInterviewDetails');
    Route::post('/admin-add-panelist-from-panel-access-window', 'BootcamperController@adminAddPanelistFromPanelAccessWindow');
    Route::get('/admin-remove-panelist-from-ict-applicant/{bootcamperPanelist}','BootcamperController@adminRemoveBootcamperPanelistFromPanel');

    //Admin Regidter Incubatee to an Even
    Route::post('/admin-regiter-incubtee-event','UsersController@adminRegisterIncubateeEvent');



    //Bootcamper next step
    Route::get('/bootcamper-next-step/{bootcamper}','BootcamperController@bootcamperNextStepsView');
    Route::post('/set-bootcamper-to-incubatee/{bootcamper}', 'BootcamperController@setBootcamperToIncubatee');
    Route::post('/set-bootcamper-to-declined-bootcamper/{bootcamper}', 'BootcamperController@setBootcamperToDeclinedBootcamper');


    //Second Pitch Video
    Route::post('/bootcamper-upload-second-pitch-video/{bootcamper}','BootcamperController@bootcamperSecondUploadPitchVideo');


    Route::post('/bootcamper-upload-id-document/{bootcamper}', 'BootcamperController@bootcamperUploadIDDocument');
    Route::get('/bootcamper-to-declined-applicant-view/{bootcamper}', 'BootcamperController@bootcamperToDeclinedApplicantView');
    Route::post('/set-bootcamper-to-declined-applicant/{bootcamper}', 'BootcamperController@setBootcamperToDeclinedApplicant');


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /*Reporting*/
    Route::get('users.incubatees.admin-incubatees.applicant-report', 'UsersController@getAllApplicants');


    Route::get('/user-incubatee-delete/{incubatee}', 'UsersController@destroy');
    Route::get('/user-delete/{user}', 'UsersController@destroyUsers');
    Route::get('account-activation/{user}', 'RegisterController@verifyEmail');
    Route::get('user-profile', 'UsersController@getUserProfile');
    Route::get('clerk-profile', 'UsersController@getUserProfile');
    Route::post('profile-update/{user}', 'UsersController@updateProfile');

    /**
     * Calendar route
     */
    Route::get('/full_calendar/events', 'EventController@index');
    Route::post('/full_calendar/create', 'EventController@create')->name('events.create');
    Route::get('/events', 'EventController@eventIndex');
    Route::get('/private-events', 'EventController@privateEventIndex');
    Route::get('get-events', 'EventController@getEvents')->name('get-events');
    Route::get('get-private-events', 'EventController@getPrivateEvents')->name('get-private-events');
    Route::get('/event-show/{event}', 'EventController@eventShow');
    Route::get('/private-event-show/{event}', 'EventController@privateEventShow');
    Route::get('/get-event-visitors-and-incubatees/{event}', 'EventController@getEventVisitorsAndIncubatees');
    Route::get('/get-private-event-visitors/{event}', 'EventController@getPrivateEventVisitors');
    Route::post('/update-event-edit/{event}', 'EventController@updateEvents');
    Route::get('/event-edit/{event}', 'EventController@editEvents');
    Route::get('/full_calendar/view-events', 'EventController@display')->name('events.display');
    Route::get('/full_calendar/view-media', 'EventController@showMedia')->name('eventMedia.showMedia');
    Route::get('/show-events', 'WelcomeController@showEvents');
    Route::get('/show-all-events', 'WelcomeController@showAllEvents');
    Route::get('/home/gallery', 'WelcomeController@eventGallery');
    Route::get('/delete-events-images/{eventMediaImage}', 'EventController@deleteImage');
    Route::get('/full_calendar/show-gallery/{event}', 'WelcomeController@showGallery');
    Route::get('/event-delete-check/{id}', 'EventController@destroyEvents');
    Route::get('/POPI-event','WelcomeController@complianceEvents');
    Route::get('/fun2fail','WelcomeController@funToFail');
    Route::get('/Franchising-101-workshop','WelcomeController@franchisingEvents');
    Route::get('/story-telling','WelcomeController@storyTellingEvent');


    /**
     * public calendar route
     */
    Route::get('/full_calendar/public-event', 'WelcomeController@publicEvent');


    Route::get('/Home/Incubatees', 'WelcomeController@getIncubatees');
    Route::get('/Home/ICTVentures', 'WelcomeController@getIncubatees');
    Route::get('/Home/INDVentures', 'WelcomeController@getIndustrial');

    Route::get('/Home/Cohorts', 'WelcomeController@getExited');

    Route::get('/Home/Incubatees', 'WelcomeController@getIncubatees');
    Route::get('/Home/ICTVentures', 'WelcomeController@getIncubatees');
    Route::get('/Home/INDVentures', 'WelcomeController@getIndustrial');

    Route::get('/Home/contactUs', 'HomeController@contact');
    Route::get('/visitors/visitors-index', 'VisitorController@visitorsIndex');
    Route::get('/visitors/visitors-index-get', 'VisitorController@getVisitors')->name('get-visitors');
    Route::get('/visitors/create-visitor', 'VisitorController@createVisitor');
    Route::post('/visitors/create-visitor', 'VisitorController@visitorStore')->name('visitor.store');
    Route::get('/visitor-edit/{visitor}', 'VisitorController@adminEditVisitor');
    Route::get('/visitor-event-edit/{eventVisitor}', 'VisitorController@adminEditVisitorEvent');
    Route::get('/get-visitor-events-via-edit/{visitor}', 'VisitorController@getVisitorEventsViaEditView');
    Route::get('/get-visitor-de-registered-events-via-edit/{visitor}', 'VisitorController@getVisitorDeRegisteredEventsViaEditView');
    Route::post('/admin-update-visitor-details/{visitor}', 'VisitorController@adminUpdateVisitorDetails');
    Route::post('/admin-update-visitor-registered-event/{eventVisitor}', 'VisitorController@adminUpdateVisitorRegisteredEvent');
    Route::get('/visitor-registered-event-index/{visitor}', 'VisitorController@adminGetVisitorRegisteredEventsIndex');
    Route::get('/visitor-de-registered-event-index/{visitor}', 'VisitorController@adminGetVisitorDeRegisteredEventsIndex');
    Route::get('/visitor-de-registered-event-edit/{visitorDeregister}', 'VisitorController@adminEditVisitorDeregisteredEvent');
    Route::post('/admin-update-visitor-deregistered-event/{visitorDeregister}', 'VisitorController@adminUpdateVisitorDeregisteredEvent');
    Route::get('/visitor-deregistered-event-delete/{visitorDeregister}', 'VisitorController@adminDeleteVisitorDeregisteredEvent');
    Route::post('/admin-register-visitor-event', 'VisitorController@adminRegisterVisitorEvent');
    Route::get('/visitor-visit-summary-index/{visitor}', 'VisitorController@adminGetVisitorSummaryIndex');
    Route::get('/admin-get-visitor-summary/{visitor}', 'VisitorController@adminGetVisitorSummary');
    Route::get('/admin-delete-visitor-backup/{visitorBackup}', 'VisitorController@adminDeleteVisitorSummary');
    Route::post('/update-visitor/{visitor}', 'VisitorController@updateVisitor');
    Route::get('/visitors/visitor-login', 'VisitorController@visitorLogin');
    Route::post('/visitors/search-visitor', 'VisitorController@searchVisitor')->name('visitor.search');
    Route::get('/visitors/visitor-dashboard/{visitor}', 'VisitorController@visitorDashboard');
    Route::post('/visitors/visitor-update-basic-details', 'VisitorController@updateVisitorBasicDetails')->name('visitor.updateBasicDetails');
    Route::get('/visitors/visitor-summary/{visitor}', 'VisitorController@visitorSummary');
    Route::get('/visitors/visitor-get-summary/{visitor}', 'VisitorController@getVisitorSummary')->name('get-visitor-summary');
    Route::get('/visitors/visitor-events/{visitor}', 'VisitorController@visitorEvents');
    Route::get('/users/incubatee-dashboard/full-calendar/visitor-events/{visitor}', 'VisitorController@visitorEvents');
    Route::post('/visitors/store-event-via-visitor-event-page', 'VisitorController@storeVisitorEventViaVisitorEventPage')->name('visitor.storeEventViaVisitorEventPage');
    Route::post('/visitors/visitor-deregister', 'VisitorController@deregisterVisitor')->name('visitor.deregister');
    Route::get('/visitors/create-visitor-via-event', 'VisitorController@createVisitorViaEvent');
    Route::post('visitors/store-visitor-via-event-register', 'VisitorController@storeVisitorViaEventRegister')->name('visitor.storeVisitorViaEventRegister');
    Route::get('/visitors/clerk/create-visitor', 'VisitorController@createVisitorViaClerk');
    Route::post('/visitors/store-visitor-via-clerk', 'VisitorController@visitorStoreViaClerk')->name('visitor.storeViaClerk');
    Route::get('/visitor-delete-check/{visitor}', 'VisitorController@deleteVisitor');
    Route::get('/export-visitor-details/{visitor}', 'DownloadExcelController@downloadVisitorDetails');
    Route::get('/export-visitor-summary/{visitor}', 'DownloadExcelController@downloadVisitorSummary');
    Route::get('/export-visitor-events/{visitor}', 'DownloadExcelController@downloadVisitorEvents');
    Route::get('/export-visitor-deregistered-events/{visitor}', 'DownloadExcelController@downloadVisitorDeregisteredEvents');


    //Clerk Visitor Routes
    Route::get('/visitors/clerk/visitor-login', 'VisitorController@visitorLogin');
    Route::get('/visitors/clerk/visitor-edit/{visitor}', 'VisitorController@editVisitorBackUpView');
    Route::get('/visitors/clerk/incubatee-visit-edit/{incubatee}', 'VisitorController@editIncubateeEventView');
    Route::get('/visitors/clerk/bootcamper-visit-edit/{bootcamper}', 'VisitorController@editBootcamperEventView');
    Route::post('/visitors/clerk/visitor-update-visit', 'VisitorController@updateVisitorBackUp')->name('visitor-update-visit');

    /*Change password route*/
    Route::get('change', 'ChangePasswordController@index');
    Route::post('/auth/passwords/change-updatePassword', 'ChangePasswordController@updatePassword')->name('updatePassword');
    Route::post('/users/administrators/passwords/change-updatePassword', 'ChangePasswordController@updatePassword')->name('updatePassword');

    /*Venture Employee Routes*/
    Route::get('create-employee/{venture}', 'VentureEmployeeController@createEmployee');
    Route::get('/employee-index/{venture}', 'VentureEmployeeController@index');
    Route::get('/get-employee/{venture}', 'VentureEmployeeController@getVentureEmployee');
    Route::post('store-employee/{venture}', 'VentureEmployeeController@storeEmployee')->name('store-employee');
    Route::get('/edit-employee/{venture_employee}', 'VentureEmployeeController@editEmployee');
    Route::post('update-employee/{venture_employee}', 'VentureEmployeeController@updateEmployee');
    Route::get('/delete-employee/{employee}', 'VentureEmployeeController@destroyEmployee');

    /*Download route to down excel from the table*/
    Route::get('downloadExcel', 'DownloadExcelController@downloadExcel');


    //Regional Innovation Networking Platform - RINP routes
    Route::get('/rinp-applicant-categories-index', 'RegionalInnovationController@rinpApplicantCategoriesIndex');
    Route::get('/get-rinp-applicant-categories', 'RegionalInnovationController@getRinpApplicantCategories')->name('get-rinp-applicant-categories');
    Route::get('/rinp-applicants-index', 'RegionalInnovationController@rinpApplicantIndex');
    Route::get('/get-rinp-applicants', 'RegionalInnovationController@getRinpApplicants')->name('get-rinp-applicants');
    Route::post('/store-rinp-applicant-application', 'RegionalInnovationController@storeApplicantApplication');
    Route::get('/view-rinp-applicant/{rinpApplicant}', 'RegionalInnovationController@adminViewRinpApplicant');
    Route::get('/delete-rinp-applicant/{rinpApplicant}', 'RegionalInnovationController@adminDeleteRinpApplicant');
    Route::get('/view-rinp-applicant-panel-access-window/{rinpApplicant}', 'RegionalInnovationController@adminViewRinpApplicantPanelAccessWindow');
    Route::post('/rinp-applicant-panel-interview-approve/{rinpApplicant}', 'RegionalInnovationController@adminSetRinpApplicantPanelInterview');
    Route::post('/admin-update-rinp-applicant-panel-interview-details/{rinpApplicant}', 'RegionalInnovationController@adminUpdateRinpApplicantPanelInterview');

    /*Testing Email Route*/
    Route::match(['get', 'post'], 'laravel-send-custom-email', 'EmailController@customEmail');
    Route::post('/send', 'EmailController@send');

    Route::get('/contact', 'EmailController@getContact');
    Route::post('contact', 'EmailController@postContact');

    Route::get('/contact', 'EmailController@Mail');

    /* REPORT ROUTE SECTION */
    Route::get('/show-reports', 'ReportController@showReports');
    Route::get('/view-bootcamper-reports', 'ReportController@showBootcamperReports');
    Route::get('/download-detailed-bootcamper-report/{event}', 'ReportController@downloadDetailedBootcamperReport');
    Route::get('/send-personalized-sql-script/{script}', 'ReportController@sendPersonalizedScript');

    //COVID 19
    Route::get('covid-19','WelcomeController@covidQuestion');
    Route::post('store-covid-form','WelcomeController@storeCovidForm')->name('store-covid-form');
    Route::get('covid-forms-index','UsersController@covidFormsIndex');
    Route::get('get-forms','UsersController@getCovidForms')->name('get-forms');
    Route::get('/delete-form/{safetyForm}','UsersController@adminDeleteForm');
    /* ---------------------------------------------------------------------------------------------- */



    Route::get('/test-notification/welcome', function () {
        event(new App\Events\StatusLiked('Someone'));
        return "Event has been sent!";
    });

    Route::get('/test-notification/welcome', function () {
        return view('test-notification.welcome');
    });

    Route::get('/test-notification/sender', function () {
        return view('test-notification.sender');
    });
    Route::post('/test-notification/sender', function () {
        $text = request()->text;
        event(new StatusLiked($text));
    });

    Route::get('/Home/visitors', function () {
        return view('home.visitors');
    });


    Route::get('/Home/contactUs', function () {
        return view('home.contactUs');
    });
    Route::get('/Home/contactUs', function () {
        return view('home.contactUs');
    });
    Route::get('/','WelcomeController@welcome');

   /* Route::get('/', function () {
        return view('welcome');
    });*/
    Route::get('/Home/About', function () {
        return view('home.about');
    });
    Route::get('/Home/Mentors', function () {
        return view('home.mentors');
    });
    Route::get('/Home/Partners', function () {
        return view('home.partners');
    });

    Route::post('/Home/Partners/form-submit', 'WelcomeController@sendPartnerMail');

    Route::get('/Home/Calendar', function () {
        return view('home.calendar');
    });

    Route::get('/Home/Innovators', function () {
        return view('home.innovators');
    });

    Route::get('/Home/Categories', function () {
        return view('home.categories');
    });

    Route::get('/Home/Programs', function () {
        return view('home.programs');
    });

//    Route::get('/Home/SmartCity',function(){
//        return view('home.smart_city');
//    });


    Route::get('/Home/Founders/{venture}', 'WelcomeController@getFounder');


    Route::get('/Home/Funding', function () {
        return view('home.funding');
    });


    Route::get('/Home/Innovators', function () {
        return view('home.innovators');
    });


    Route::get('/Home/Partners', function () {
        return view('home.partners');
    });


    Route::get('Home/page-loader', function () {
        return view('home.page-loader');
    });

    Route::get('/Home/Opportunity', function () {
        return view('home.opportunity');
    });


    Route::get('/home/SDG', 'WelcomeController@getSDG');
    Route::get('/home/SmartCity', 'WelcomeController@getSmartCity1');
    Route::get('/Incubatees/List/{venture}', 'WelcomeController@getIncubatees');
    Route::get('/home/alumni', 'WelcomeController@getAlumni');
    Route::get('/home/event-show-media/{event}', 'WelcomeController@getEventMedia');
    Route::get('/home/exited', 'WelcomeController@getExitedVentures');
    Route::get('/home/exited/industrial', 'WelcomeController@getExitedIndustrialVentures');

    Route::get('/Home/FreeCourse', function () {
        return view('home.smartcity');
    });
});
